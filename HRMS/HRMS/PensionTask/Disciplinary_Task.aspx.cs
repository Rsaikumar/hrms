﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace HRMS
{
    public partial class Disciplinary_Task : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SetInitialRow();
        }
        public void SetInitialRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("pension ID", typeof(string)));
            dt.Columns.Add(new DataColumn("Emp_ID", typeof(string)));
            dt.Columns.Add(new DataColumn("Employee_name", typeof(string)));
            dt.Columns.Add(new DataColumn("Date_of_Retirement", typeof(string)));
            dt.Columns.Add(new DataColumn("Status", typeof(string)));

            dr = dt.NewRow();

            dr["Pension ID"] = string.Empty;
            dr["Emp_ID"] = string.Empty;
            dr["Employee_name"] = string.Empty;
            dr["Date_of_Retirement"] = string.Empty;
            dr["Status"] = string.Empty;

            dt.Rows.Add(dr);

            GridView1.DataSource = dt;
            GridView1.DataBind();

            //int columncount = 7;
            //grdDefineLeaveTypesList.Rows[0].Cells.Clear();
            //grdDefineLeaveTypesList.Rows[0].Cells.Add(new TableCell());
            //grdDefineLeaveTypesList.Rows[0].Cells[0].ColumnSpan = columncount;
            //grdDefineLeaveTypesList.Rows[0].Cells[0].Text = "No Records Found";
            ////  grdStorageLocation.HeaderStyle.HorizontalAlign = "center";
            //grdDefineLeaveTypesList.Rows[0].Cells[0].ForeColor = System.Drawing.Color.Red;
        }
    }
}