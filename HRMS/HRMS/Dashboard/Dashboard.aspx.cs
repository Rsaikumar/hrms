﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BAL;
using DAL;
using System.Data.SqlClient;
using System.Configuration;

namespace HRMS.Dashboard
{
        #region Declaration

    public partial class Dashboard : System.Web.UI.Page
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();
        //DefineLeavesBAL objSCM_LeaveBAL = new DefineLeavesBAL();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Dbconnection"].ConnectionString);
        ERP_DatabaseDataContext objdal = new ERP_DatabaseDataContext();

        #endregion

        #region EVENTS

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["ClientRegistrationNo"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                //gpftemporarydetails();
                //gpfpartfinalapplication();
                //gpffinalapplication();
                leavebind();
                bindgridloandetails();
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Employee/Add_events.aspx");
        }

        protected void gdvempleave_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(gdvempleave, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = "View Applicant Details";
                e.Row.Attributes["style"] = "cursor:pointer";
            }
        }

        protected void gdvempleave_SelectedIndexChanged(object sender, EventArgs e)
        {
            string Id = gdvempleave.SelectedRow.Cells[0].Text;
            string path = "~/Payroll/LeaveApproval.aspx";
            Session["LeaveapplicationNo"] = Id;
            Response.Redirect(path);
        }

        protected void gdvemploanapp_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(gdvemploanapp, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = "View Loan Applicant Details";
                e.Row.Attributes["style"] = "cursor:pointer";
            }
        }

        protected void gdvemploanapp_SelectedIndexChanged(object sender, EventArgs e)
        {
            string Loanid = gdvemploanapp.SelectedRow.Cells[0].Text;
            string path = "~/Payroll/LoanApproval.aspx";
            Session["LoanApplicationNo"] = Loanid;
            Response.Redirect(path);
        }
        #endregion

        #region Methods

        public int empid;

        public void leavebind()
        {
            empid = Convert.ToInt32(Session["Emp_Code"]);
            var objbindleavegrid = (from ST in objDAL.USP_gettingleavegrid(empid)
                                    select ST).ToList();
            if (objbindleavegrid.Count() >= 0)
            {
                gdvempleave.DataSource = objbindleavegrid;
                gdvempleave.DataBind();
            }

            else
            {
                gdvempleave.DataSource = objbindleavegrid;
                gdvempleave.DataBind();
            }
        }

        public void bindgridloandetails()
        {
            int empcode = Convert.ToInt32(Session["Emp_Code"].ToString());
            var bindgrid = (from item in objdal.Usp_GettingEmployeeDetailsInLoanGrid(empcode)
                            select item).ToList();
            if (bindgrid.Count() >= 0)
            {
                gdvemploanapp.DataSource = bindgrid;
                gdvemploanapp.DataBind();

            }
            else
            {
                gdvemploanapp.DataSource = bindgrid;
                gdvemploanapp.DataBind();
            }
        }

        #endregion

     
    }
}


