﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="HRMS.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!--webfonts-->
    <link rel='stylesheet prefetch' href='https://fonts.googleapis.com/icon?family=Material+Icons' />
    <link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Roboto:300' />
    <!--//Styles-->
    <link href="css/Login/style.css" rel="stylesheet" />

    <script type="text/javascript" lang="javascript">

        function validation() {
            if (document.getElementById('<%=txtUserName.ClientID%>').value == "") {
                alert("Please Enter UserName");

                document.getElementById('<%=txtUserName.ClientID%>').focus();
                return false;
            }
            if (document.getElementById('<%=txtPassword.ClientID%>').value == "") {
                alert("Please Enter Password");
                document.getElementById('<%=txtPassword.ClientID%>').focus();
                return false;
            }
          

            //this will return true
            return true;
        }
    </script>
</head>
<body>

    <form id="form1" runat="server" align="center">
        <asp:ScriptManager runat="server"></asp:ScriptManager>

        <asp:UpdatePanel runat="server" align="center">
            <ContentTemplate>
                <div class="mediahawkz-login">
                    <div class="div-left">
                        <div class="state">

                        </div>
                    </div>
                    <div class="div-right">
                        <div class="rkmd-form login">
                     <img src="css/Login/hrms_icon.png" />
                            <hr />
                            <div id="login">
                                <div class="form-field">
                                    <label class="field-label" for="emailid">Username</label>
                                    <asp:TextBox ID="txtUserName" Text="sai" runat="server" TextMode="SingleLine"
                                        class="field-input"></asp:TextBox>

                                    <i class="material-icons md-18">error_outline</i>
                                </div>
                                <div class="form-field">
                                    <label class="field-label" for="pass">Password</label>
                                    <asp:TextBox ID="txtPassword" Text="123" runat="server" TextMode="Password" class="field-input"></asp:TextBox>

                                    <i class="material-icons md-18">error_outline</i>
                                </div>
                                <div class="form-field" id="ddlcompany" runat="server" visible="false">
                                    <asp:DropDownList ID="ddlCompanyName" runat="server">
                                        <asp:ListItem Text="Select Company Name" Value="Select Company Name"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="form-row clearfix">
                                    <div class="remember float-left">
                                        <%--<asp:CheckBox ID="chkUserType" CssClass="radioButtonList" runat="server" Text="Employee" />--%>
                                    </div>

                                    <asp:Button ID="btnLogin" runat="server" Text="Login" OnClick="btnLogin_Click"  class="rkmd-btn btn-lightBlue ripple-effect float-right" />
                                </div>
                                <div class="form-row clearfix">
                                    <a href="#" class="link float-right">Forgot your password?</a>

                                </div>
                            </div>
                        </div>


                        <div class="copyright">
                            <p>&copy; 2017, Designed by <a href="" class="link">ThinkLogic IT Solutions</a>  </p>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>

    <!-----start-copyright---->
    <%-- <div class="copy-right">
        
    </div>--%>
    <!-----//end-copyright---->


    <!-- Main content -->



    <script src="css/Login/jquery.min.js"></script>
    <script src="css/Login/index.js"></script>


</body>
</html>
