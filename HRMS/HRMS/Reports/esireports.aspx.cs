﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BAL;
using DAL;
using System.Data.Linq.SqlClient;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using System.IO;
namespace HRMS.Reports
{
    public partial class esireports : System.Web.UI.Page
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();
        DefineLeavesBAL objSCM_LeaveBAL = new DefineLeavesBAL();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Dbconnection"].ConnectionString);
        ERP_DatabaseDataContext objdal = new ERP_DatabaseDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            Bind_Grid();
        }
        public void Bind_Grid()
        {


            string Cmpy = (string)Session["ClientRegistrationNo"];
            //string Brnach = (string)Session["BranchID"];
            //string UserType = (string)Session["User_Type"];
            //string UserName = (string)Session["User_Name"];
            //string Cration_Company = (string)Session["CreationCompany"]; //"000001";
            ////if (UserType == "User")
            //{


            // string EMP_Code = (from EM in objDAL.HRMS_Employee_Master_PersonalDetails where EM.User_Name == UserName select EM.Emp_Code).FirstOrDefault();
            var objHeaderInfo = (from header in objDAL.HRMS_Employee_Official_Info_Head_Details
                                 where header.Creation_Company == Cmpy
                                 orderby header.Emp_Code ascending
                                 select header).ToList();
            //// List<Administrator_UserRegistration> ObjuserInfo = objSCM_userRegistration.Get_userRegDetails(Session["ClientRegistrationNo"].ToString());
            if (objHeaderInfo.Count > 0)
            {

                grdleaves.DataSource = objHeaderInfo;
                grdleaves.DataBind();
            }
            else
            {
                // // // Grid view using setting initial rows displaying Empty Header Displying
                setinitialrow();
            }
        }


        public void setinitialrow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("Sno", typeof(string)));

            dt.Columns.Add(new DataColumn("Emp_Code", typeof(string)));

            dt.Columns.Add(new DataColumn("Salery", typeof(string)));

            dt.Rows.Add(dr);
            ViewState["CurrentTable"] = dt;
            grdleaves.DataSource = dt;
            grdleaves.DataBind();

        }
    }
}