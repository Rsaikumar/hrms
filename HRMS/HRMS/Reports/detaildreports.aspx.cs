﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BAL;
using DAL;
using System.Data.Linq.SqlClient;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using System.IO;

namespace HRMS.Reports
{
    public partial class detaildreports : System.Web.UI.Page
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();
        DefineLeavesBAL objSCM_LeaveBAL = new DefineLeavesBAL();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Dbconnection"].ConnectionString);
        ERP_DatabaseDataContext objdal = new ERP_DatabaseDataContext();
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();
        SqlConnection objSqlConnection;
        SqlCommand objSqlCommand;
        SqlDataAdapter da;
        protected void Page_Load(object sender, EventArgs e)
        {
          
            if (!IsPostBack)
            {
                Panel1.Visible = false;
                Panel2.Visible = false;
                Panel3.Visible = false;
                Panel4.Visible = false;
                Panel5.Visible = false;
                Panel6.Visible = false;
                Panel7.Visible = false;
                Panel8.Visible = false;
                Panel9.Visible = false;
                
                txtEmpCode.Focus();
                Emp_NameBinding();
            }
        }
        protected void txtEmpCode_TextChanged(object sender, EventArgs e)
        {
            string CreationCompany = Session["ClientRegistrationNo"].ToString();
            string Emp_Code = (txtEmpCode.Text.ToLower().Trim() == "") ? "" : txtEmpCode.Text.ToLower().Trim();
            var objEmPCodeDUP = (from itmes in ObjDAL.HRMS_Employee_Master_PersonalDetails
                                 where itmes.Creation_Company == CreationCompany && itmes.Emp_Code.ToLower().Trim() == Emp_Code
                                 select new
                                 {
                                     Emp_name = itmes.Emp_First_Name + " " + itmes.Emp_Last_Name + " " + itmes.Emp_Middle_Name,
                                     Emp_Code = itmes.Emp_Code
                                 }).ToList();
            if (objEmPCodeDUP.Count > 0)
            {

                ddlEmpName.SelectedItem.Text = objEmPCodeDUP[0].Emp_name;

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Emp Code is Not Valid');", true);
                txtEmpCode.Text = "";
                txtEmpCode.Focus();
                ddlEmpName.SelectedValue = "0";


            }
        }
        protected void ddlEmpName_SelectedIndexChanged(object sender, EventArgs e)
        {
            string CreationCompany = (string)Session["ClientRegistrationNo"].ToString();
            txtEmpCode.Text = ddlEmpName.SelectedValue;

        }
        public void Emp_NameBinding()
        {
            string CreationCompany = Session["ClientRegistrationNo"].ToString();

            var objEmPCodeDUP = (from itmes in ObjDAL.HRMS_Employee_Master_PersonalDetails
                                 where itmes.Creation_Company == CreationCompany
                                 select new
                                 {
                                     Emp_name = itmes.Emp_Last_Name + " " + itmes.Emp_First_Name + " " + itmes.Emp_Middle_Name,
                                     Emp_Code = itmes.Emp_Code
                                 }).ToList();
            if (objEmPCodeDUP.Count > 0)
            {
                ddlEmpName.DataSource = objEmPCodeDUP;
                ddlEmpName.DataTextField = "Emp_name";
                ddlEmpName.DataValueField = "Emp_Code";
                ddlEmpName.DataBind();
                ddlEmpName.Items.Insert(0, new ListItem("--Select--", "0"));
            }

        }
        public void Bind_Gridapgli()
        {


            //string Cmpy = (string)Session["ClientRegistrationNo"];

            // string EMP_Code = (from EM in objDAL.HRMS_Employee_Master_PersonalDetails where EM.User_Name == UserName select EM.Emp_Code).FirstOrDefault();
            var objHeaderInfo = (from header in objDAL.HRMS_Employee_Official_Info_Head_Details
                                 
                                 orderby header.Emp_Code ascending
                                 select header).ToList();
            //// List<Administrator_UserRegistration> ObjuserInfo = objSCM_userRegistration.Get_userRegDetails(Session["ClientRegistrationNo"].ToString());
            if (objHeaderInfo.Count > 0)
            {

                grdapgli.DataSource = objHeaderInfo;
                grdapgli.DataBind();
            }
            else
            {
                // // // Grid view using setting initial rows displaying Empty Header Displying
                setinitialrowapgli();
            }
        }

        public void setinitialrowapgli()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("Sno", typeof(string)));

            dt.Columns.Add(new DataColumn("Emp_Code", typeof(string)));

            dt.Columns.Add(new DataColumn("Salery", typeof(string)));

            dt.Rows.Add(dr);
            ViewState["CurrentTable"] = dt;
            grdapgli.DataSource = dt;
            grdapgli.DataBind();

        }
        public void Bind_Gridpf()
        {


            string Cmpy = (string)Session["ClientRegistrationNo"];
         

            // string EMP_Code = (from EM in objDAL.HRMS_Employee_Master_PersonalDetails where EM.User_Name == UserName select EM.Emp_Code).FirstOrDefault();
            var objHeaderInfo = (from header in objDAL.HRMS_Employee_Official_Info_Head_Details
                                 where header.Creation_Company == Cmpy
                                 orderby header.Emp_Code ascending
                                 select header).ToList();
            //// List<Administrator_UserRegistration> ObjuserInfo = objSCM_userRegistration.Get_userRegDetails(Session["ClientRegistrationNo"].ToString());
            if (objHeaderInfo.Count > 0)
            {

                grdpf.DataSource = objHeaderInfo;
                grdpf.DataBind();
            }
            else
            {
                // // // Grid view using setting initial rows displaying Empty Header Displying
                setinitialrowpf();
            }
        }


        public void setinitialrowpf()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("Sno", typeof(string)));

            dt.Columns.Add(new DataColumn("Emp_Code", typeof(string)));

            dt.Columns.Add(new DataColumn("Salery", typeof(string)));

            dt.Rows.Add(dr);
            ViewState["CurrentTable"] = dt;
            grdpf.DataSource = dt;
            grdpf.DataBind();

        }
        public void Bind_Gridesi()
        {


            string Cmpy = (string)Session["ClientRegistrationNo"];
            //string Brnach = (string)Session["BranchID"];
            //string UserType = (string)Session["User_Type"];
            //string UserName = (string)Session["User_Name"];
            //string Cration_Company = (string)Session["CreationCompany"]; //"000001";
            ////if (UserType == "User")
            //{


            // string EMP_Code = (from EM in objDAL.HRMS_Employee_Master_PersonalDetails where EM.User_Name == UserName select EM.Emp_Code).FirstOrDefault();
            var objHeaderInfo = (from header in objDAL.HRMS_Employee_Official_Info_Head_Details
                                 where header.Creation_Company == Cmpy
                                 orderby header.Emp_Code ascending
                                 select header).ToList();
            //// List<Administrator_UserRegistration> ObjuserInfo = objSCM_userRegistration.Get_userRegDetails(Session["ClientRegistrationNo"].ToString());
            if (objHeaderInfo.Count > 0)
            {

                grdesi.DataSource = objHeaderInfo;
                grdesi.DataBind();
            }
            else
            {
                // // // Grid view using setting initial rows displaying Empty Header Displying
                setinitialrowesi();
            }
        }


        public void setinitialrowesi()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("Sno", typeof(string)));

            dt.Columns.Add(new DataColumn("Emp_Code", typeof(string)));

            dt.Columns.Add(new DataColumn("Salery", typeof(string)));

            dt.Rows.Add(dr);
            ViewState["CurrentTable"] = dt;
            grdesi.DataSource = dt;
            grdesi.DataBind();

        }
        public void Bind_Gridsalary()
        {


            string Cmpy = (string)Session["ClientRegistrationNo"];
            //string Brnach = (string)Session["BranchID"];
            //string UserType = (string)Session["User_Type"];
            //string UserName = (string)Session["User_Name"];
            //string Cration_Company = (string)Session["CreationCompany"]; //"000001";
            ////if (UserType == "User")
            //{


            // string EMP_Code = (from EM in objDAL.HRMS_Employee_Master_PersonalDetails where EM.User_Name == UserName select EM.Emp_Code).FirstOrDefault();
            var objHeaderInfo = (from header in objDAL.HRMS_Employee_Official_Info_Head_Details
                                 where header.Creation_Company == Cmpy
                                 orderby header.Emp_Code ascending
                                 select header).ToList();
            //// List<Administrator_UserRegistration> ObjuserInfo = objSCM_userRegistration.Get_userRegDetails(Session["ClientRegistrationNo"].ToString());
            if (objHeaderInfo.Count > 0)
            {

                grdsaleryreport.DataSource = objHeaderInfo;
                grdsaleryreport.DataBind();
            }
            else
            {
                // // // Grid view using setting initial rows displaying Empty Header Displying
               
            }
        }
        public void Bind_Gridemploye()
        {


            string Cmpy = (string)Session["ClientRegistrationNo"];
            //string Brnach = (string)Session["BranchID"];
            //string UserType = (string)Session["User_Type"];
            //string UserName = (string)Session["User_Name"];
            //string Cration_Company = (string)Session["CreationCompany"]; //"000001";
            ////if (UserType == "User")
            //{


            // string EMP_Code = (from EM in objDAL.HRMS_Employee_Master_PersonalDetails where EM.User_Name == UserName select EM.Emp_Code).FirstOrDefault();
            var objHeaderInfo = (from header in objDAL.HRMS_Employee_Master_PersonalDetails
                                 where header.Creation_Company == Cmpy
                                 orderby header.Emp_Code ascending
                                 select header).ToList();
            //// List<Administrator_UserRegistration> ObjuserInfo = objSCM_userRegistration.Get_userRegDetails(Session["ClientRegistrationNo"].ToString());
            if (objHeaderInfo.Count > 0)
            {

                grdemploye.DataSource = objHeaderInfo;
                grdemploye.DataBind();
            }
            else
            {
                // // // Grid view using setting initial rows displaying Empty Header Displying
                setinitialrowemploye();
            }
        }


        public void setinitialrowemploye()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("Sno", typeof(string)));
            dt.Columns.Add(new DataColumn("Leave_Code", typeof(string)));
            dt.Columns.Add(new DataColumn("Emp_Code", typeof(string)));
            dt.Columns.Add(new DataColumn("Emp_Name", typeof(string)));
            dt.Columns.Add(new DataColumn("TypeOfLeaveApple", typeof(string)));
            dt.Columns.Add(new DataColumn("Noof_Days", typeof(string)));
            dt.Columns.Add(new DataColumn("Leave_Status", typeof(string)));

            dr = dt.NewRow();
            dr["Sno"] = string.Empty;
            dr["Leave_Code"] = string.Empty;
            dr["Emp_Code"] = string.Empty;
            dr["Emp_Name"] = string.Empty;
            dr["TypeOfLeaveApple"] = string.Empty;
            // dr["Prod_Spec"] = string.Empty;
            dr["Noof_Days"] = string.Empty;

            dt.Rows.Add(dr);
            ViewState["CurrentTable"] = dt;
            grdemploye.DataSource = dt;
            grdemploye.DataBind();

        }
        public void Bind_Gridgis()
        {


            //string Cmpy = (string)Session["ClientRegistrationNo"];
            //string Brnach = (string)Session["BranchID"];
            //string UserType = (string)Session["User_Type"];
            //string UserName = (string)Session["User_Name"];
            //string Cration_Company = (string)Session["CreationCompany"]; //"000001";
            ////if (UserType == "User")
            //{


            // string EMP_Code = (from EM in objDAL.HRMS_Employee_Master_PersonalDetails where EM.User_Name == UserName select EM.Emp_Code).FirstOrDefault();
            var objHeaderInfo = (from header in objDAL.HRMS_Employee_Official_Info_Head_Details
                                 
                                 orderby header.Emp_Code ascending
                                 select header).ToList();
            //// List<Administrator_UserRegistration> ObjuserInfo = objSCM_userRegistration.Get_userRegDetails(Session["ClientRegistrationNo"].ToString());
            if (objHeaderInfo.Count > 0)
            {

                grdgis.DataSource = objHeaderInfo;
                grdgis.DataBind();
            }
            else
            {
                // // // Grid view using setting initial rows displaying Empty Header Displying
                setinitialrowgis();
            }
        }


        public void setinitialrowgis()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("Sno", typeof(string)));

            dt.Columns.Add(new DataColumn("Emp_Code", typeof(string)));

            dt.Columns.Add(new DataColumn("Salery", typeof(string)));

            dt.Rows.Add(dr);
            ViewState["CurrentTable"] = dt;
            grdgis.DataSource = dt;
            grdgis.DataBind();

        }
        private void bindgridleave()
        {
            try
            {




                //#region Display Code




                string objConfig = ConfigurationManager.ConnectionStrings["Dbconnection"].ToString();

                objSqlConnection = new SqlConnection(objConfig);
                objSqlCommand = new SqlCommand("select Emp_Code,Emp_Name from HRMS_Leave_Allotment", objSqlConnection);
                objSqlCommand.Connection = objSqlConnection;

                objSqlConnection.Open();
               

                da = new SqlDataAdapter(objSqlCommand);
                DataTable ds = new DataTable();
                da.Fill(ds);
                var distinctrows = ds.AsEnumerable()
                        .Select(s => new
                        {
                            Emp_Code = s.Field<string>("Emp_Code"),
                            Emp_Name = s.Field<string>("Emp_Name"),
                        }).Distinct().ToList();

                grd_LEave_Balance.Visible = true;

                if (ds.Rows.Count > 0)
                {
                    grd_LEave_Balance.DataSource = distinctrows;
                    grd_LEave_Balance.DataBind();
                    ViewState["CurrentTable"] = ds;
                    Session["DS"] = ds;
                    //Iscroll.Attributes.Add("style", "overflow-x:scroll;width:99%;");
                }
                else
                {
                    #region header with no record
                    ds.Rows.Add(ds.NewRow());
                    grd_LEave_Balance.DataSource = ds;
                    grd_LEave_Balance.DataBind();
                    int columncount = grd_LEave_Balance.Rows[0].Cells.Count;
                    grd_LEave_Balance.Rows[0].Cells.Clear();
                    grd_LEave_Balance.Rows[0].Cells.Add(new TableCell());
                    grd_LEave_Balance.Rows[0].Cells[0].ColumnSpan = columncount;
                    grd_LEave_Balance.Rows[0].Cells[0].Text = "No Records Found";
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Session["Error"] = ex.ToString();
                //Server.Transfer("~/Modules/Error_Page.aspx");
            }
            finally
            {

            }
        }
        public void grd2Bind(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowIndex != -1)
                {
                    string EmpNo = grd_LEave_Balance.DataKeys[e.Row.RowIndex].Value.ToString();
                    GridView grdstc = (GridView)e.Row.FindControl("grd2");
                    string objConfig = ConfigurationManager.ConnectionStrings["Dbconnection"].ToString();

                    objSqlConnection = new SqlConnection(objConfig);
                    objSqlCommand = new SqlCommand("select Emp_Code,Emp_Name,Leave_Name,Noof_Leavs from HRMS_Leave_Allotment", objSqlConnection);
                    objSqlCommand.Connection = objSqlConnection;

                    objSqlConnection.Open();
                    // objSqlCommand.CommandType = CommandType.StoredProcedure;
                    // objSqlCommand.CommandText = "Sp_HRMS_Emppoyee_Leave_Details";

                    //objSqlCommand.Parameters.AddWithValue("@Year", ddlYear.SelectedValue);
                    //objSqlCommand.Parameters.AddWithValue("@Emp_Code", EmpNo);
                    //objSqlCommand.Parameters.AddWithValue("@Department", "");
                    //objSqlCommand.Parameters.AddWithValue("@Creation_Company", (string)Session["ClientRegistrationNo"]);
                    //objSqlCommand.Parameters.AddWithValue("@Parameter", 2);

                    da = new SqlDataAdapter(objSqlCommand);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    var distinctrows = dt.AsEnumerable()
                       .Select(s => new
                       {
                           Leave_Name = s.Field<string>("Leave_Name"),
                           Noof_Leavs = s.Field<decimal>("Noof_Leavs"),

                       }).Distinct().ToList();

                    //DataTable dt = (DataTable)ViewState["CurrentTable"];
                    grdstc.DataSource = dt;
                    grdstc.DataBind();
                    if (dt.Rows.Count > 0)
                    {
                        grdstc.DataSource = distinctrows;
                        grdstc.DataBind();
                        ViewState["CurrentTable"] = dt;
                        Session["DS"] = dt;
                        //Iscroll.Attributes.Add("style", "overflow-x:scroll;width:99%;");
                    }
                    else
                    {
                        #region header with no record
                        dt.Rows.Add(dt.NewRow());
                        grdstc.DataSource = dt;
                        grdstc.DataBind();
                        int columncount = grdstc.Rows[0].Cells.Count;
                        grd_LEave_Balance.Rows[0].Cells.Clear();
                        grd_LEave_Balance.Rows[0].Cells.Add(new TableCell());
                        grd_LEave_Balance.Rows[0].Cells[0].ColumnSpan = columncount;
                        grd_LEave_Balance.Rows[0].Cells[0].Text = "No Records Found";
                        #endregion
                    }



                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
        }
        private void bindgridloan()
        {
            try
            {

                string objConfig = ConfigurationManager.ConnectionStrings["Dbconnection"].ToString();

                objSqlConnection = new SqlConnection(objConfig);
                objSqlCommand = new SqlCommand("select Emp_Code,Emp_Name from HRMS_Loan_Application", objSqlConnection);
                objSqlCommand.Connection = objSqlConnection;

                objSqlConnection.Open();
                

                da = new SqlDataAdapter(objSqlCommand);
                DataTable ds = new DataTable();
                da.Fill(ds);
                var distinctrows = ds.AsEnumerable()
                        .Select(s => new
                        {
                            Emp_Code = s.Field<string>("Emp_Code"),
                            Emp_Name = s.Field<string>("Emp_Name"),


                        }).Distinct().ToList();

                grd_LEave_Balance.Visible = true;



                if (ds.Rows.Count > 0)
                {
                    grd_LEave_Balance.DataSource = distinctrows;
                    grd_LEave_Balance.DataBind();
                    ViewState["CurrentTable"] = ds;
                    Session["DS"] = ds;
                    //Iscroll.Attributes.Add("style", "overflow-x:scroll;width:99%;");
                }
                else
                {
                    #region header with no record
                    ds.Rows.Add(ds.NewRow());
                    grd_LEave_Balance.DataSource = ds;
                    grd_LEave_Balance.DataBind();
                    int columncount = grd_LEave_Balance.Rows[0].Cells.Count;
                    grd_LEave_Balance.Rows[0].Cells.Clear();
                    grd_LEave_Balance.Rows[0].Cells.Add(new TableCell());
                    grd_LEave_Balance.Rows[0].Cells[0].ColumnSpan = columncount;
                    grd_LEave_Balance.Rows[0].Cells[0].Text = "No Records Found";
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Session["Error"] = ex.ToString();
                //Server.Transfer("~/Modules/Error_Page.aspx");
            }
            finally
            {

            }
        }
        public void grd22Bind(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowIndex != -1)
                {
                    string EmpNo = grd_LEave_Balance.DataKeys[e.Row.RowIndex].Value.ToString();
                    GridView grdstc = (GridView)e.Row.FindControl("grd2");
                    string objConfig = ConfigurationManager.ConnectionStrings["Dbconnection"].ToString();

                    objSqlConnection = new SqlConnection(objConfig);
                    objSqlCommand = new SqlCommand("select Emp_Code,Emp_Name,Type_Of_Loan_Apply,Basic_Amount,Loan_amount_Required from HRMS_Loan_Application", objSqlConnection);
                    objSqlCommand.Connection = objSqlConnection;

                    objSqlConnection.Open();
                    da = new SqlDataAdapter(objSqlCommand);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    var distinctrows = dt.AsEnumerable()
                       .Select(s => new
                       {
                           Type_Of_Loan_Apply = s.Field<string>("Type_Of_Loan_Apply"),
                           Basic_Amount = s.Field<decimal>("Basic_Amount"),
                           Loan_amount_Required = s.Field<decimal>("Loan_amount_Required"),

                       }).Distinct().ToList();

                    //DataTable dt = (DataTable)ViewState["CurrentTable"];
                    grdstc.DataSource = dt;
                    grdstc.DataBind();
                    if (dt.Rows.Count > 0)
                    {
                        grdstc.DataSource = distinctrows;
                        grdstc.DataBind();
                        ViewState["CurrentTable"] = dt;
                        Session["DS"] = dt;
                        //Iscroll.Attributes.Add("style", "overflow-x:scroll;width:99%;");
                    }
                    else
                    {
                        #region header with no record
                        dt.Rows.Add(dt.NewRow());
                        grdstc.DataSource = dt;
                        grdstc.DataBind();
                        int columncount = grdstc.Rows[0].Cells.Count;
                        grd_LEave_Balance.Rows[0].Cells.Clear();
                        grd_LEave_Balance.Rows[0].Cells.Add(new TableCell());
                        grd_LEave_Balance.Rows[0].Cells[0].ColumnSpan = columncount;
                        grd_LEave_Balance.Rows[0].Cells[0].Text = "No Records Found";
                        #endregion
                    }



                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
        }
       

        protected void ddlfeature_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlfeature.SelectedItem.Text == "APGLI")
            {
                Panel1.Visible = false;
                Panel2.Visible = false;
                Panel3.Visible = false;
                Panel4.Visible = true;
                Panel5.Visible = false;
                Panel6.Visible = false;
                Panel5.Visible = false;
                Panel7.Visible = false;
                Panel8.Visible = false;
                Panel9.Visible = false;

                Bind_Gridapgli();
            }
            else if (ddlfeature.SelectedItem.Text == "PF")
            {
                Panel1.Visible = true;
                Panel2.Visible = false;
                Panel3.Visible = false;
                Panel4.Visible = false;
                Panel5.Visible = false;
                Panel6.Visible = false;
                Panel5.Visible = false;
                Panel7.Visible = false;
                Panel8.Visible = false;
                Panel9.Visible = false;
                Bind_Gridpf();
            }
            else if (ddlfeature.SelectedItem.Text == "ESI")
            {
                Panel1.Visible = false;
                Panel2.Visible = true;
                Panel3.Visible = false;
                Panel4.Visible = false;
                Panel5.Visible = false;
                Panel6.Visible = false;
                Panel5.Visible = false;
                Panel7.Visible = false;
                Panel8.Visible = false;
                Panel9.Visible = false;
                Bind_Gridesi();
            }
            else if (ddlfeature.SelectedItem.Text == "salary Report")
            {
                Panel1.Visible = false;
                Panel2.Visible = false;
                Panel3.Visible = true;
                Panel4.Visible = false;
                Panel5.Visible = false;
                Panel6.Visible = false;
                Panel5.Visible = false;
                Panel7.Visible = false;
                Panel8.Visible = false;
                Panel9.Visible = false;
                Bind_Gridsalary();
            }
            else if (ddlfeature.SelectedItem.Text == "GIS")
            {
                Panel1.Visible = false;
                Panel2.Visible = false;
                Panel3.Visible = false;
                Panel4.Visible = false;
                Panel5.Visible = true;
                Panel6.Visible = false;
                Panel5.Visible = false;
                Panel7.Visible = false;
                Panel8.Visible = false;
                Panel9.Visible = false;
                Bind_Gridgis();
            }
            else if (ddlfeature.SelectedItem.Text == "Leave Report")
            {
                Panel1.Visible = false;
                Panel2.Visible = false;
                Panel3.Visible = false;
                Panel4.Visible = false;
                Panel5.Visible = false;
                Panel6.Visible = true;
                Panel5.Visible = false;
                Panel7.Visible = false;
                Panel8.Visible = false;
                Panel9.Visible = false;
                bindgridleave();
            }
            else if (ddlfeature.SelectedItem.Text == "Loan Report")
            {
                Panel1.Visible = false;
                Panel2.Visible = false;
                Panel3.Visible = false;
                Panel4.Visible = false;
                Panel5.Visible = false;
                Panel6.Visible = false;
                Panel5.Visible = false;
                Panel7.Visible = true;
                Panel8.Visible = false;
                Panel9.Visible = false;
                bindgridloan();
            }
            else if (ddlfeature.SelectedItem.Text == "Employee Report")
            {
                Panel1.Visible = false;
                Panel2.Visible = false;
                Panel3.Visible = false;
                Panel4.Visible = false;
                Panel5.Visible = false;
                Panel6.Visible = false;
                Panel5.Visible = false;
                Panel7.Visible = false;
                Panel8.Visible = true;
                Panel9.Visible = false;
                Bind_Gridemploye();
            }
            else if (ddlfeature.SelectedItem.Text == "monthly payslip")
            {
                Panel1.Visible = true;
                Panel2.Visible = false;
                Panel3.Visible = false;
                Panel4.Visible = false;
                Panel5.Visible = false;
                Panel6.Visible = false;
                Panel5.Visible = false;
                Panel7.Visible = false;
                Panel8.Visible = false;
                Panel9.Visible = false;

            }

        }
}
}

        
        