﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="pfreports.aspx.cs" Inherits="HRMS.Reports.pfreports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div class="container">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">Report</a><i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="EmployeeReport.aspx">PF Report</a><i class="fa fa-angle-right"></i>
                    </li>

                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>


        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                    <div class="box-title">
                        <h3>
                            <i class="fa fa-th-list"></i>Pf Report</h3>
                    </div>
                    <div class="box-content">
                        <div class="col-md-12">

                          
                                <asp:GridView ID="grdleaves" class="table table-bordered CustomerAddress"
                                    Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%;" runat="server"
                                    ShowFooter="True" AutoGenerateColumns="False" DataKeyNames="Sno">

                                    <Columns>
                                        <asp:BoundField ReadOnly="true" HeaderText="Sno" DataField="Sno" />

                                        <asp:TemplateField HeaderText="Employee Code">
                                            <ItemTemplate>
                                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("Emp_Code") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Employe Salery">
                                            <ItemTemplate>
                                                <asp:Label ID="lblbasicamount" runat="server" Text='<%# Eval("Basic_Amount") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PF Amount">
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("PF") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Month">
                                            <ItemTemplate>
                                                <asp:Label ID="lblmonth" runat="server" Text='<%# Eval("MONTHS") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>

                            </div>
                        </div>
                    </div>
               
            </div>
        </div>
</asp:Content>
