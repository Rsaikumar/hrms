﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Linq.SqlClient;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using System.IO;
using BAL;
using DAL;

namespace HRMS.Reports
{
    public partial class services_records : System.Web.UI.Page
    {
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();
        Leave_AllotmentBLL objLeaveAllotmentBLL = new Leave_AllotmentBLL();
        EmployeeOfficialInfoBAL objeoinfo = new EmployeeOfficialInfoBAL();
        SqlConnection objSqlConnection;
        SqlCommand objSqlCommand;
        SqlDataAdapter da;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                pnlincrement.Visible = false;
                pnlleave.Visible = false;
                txtEmpCode.Focus();
                Emp_NameBinding();

            }
        }

        protected void btnleave_Click(object sender, EventArgs e)
        {
            grd2Bind();
            grdleave.Visible = true;
            grdincrement.Visible = false;
            pnlincrement.Visible = false;
            pnlleave.Visible = true;
        }
        protected void txtEmpCode_TextChanged(object sender, EventArgs e)
        {
            string CreationCompany = Session["ClientRegistrationNo"].ToString();
            string Emp_Code = (txtEmpCode.Text.ToLower().Trim() == "") ? "" : txtEmpCode.Text.ToLower().Trim();
            var objEmPCodeDUP = (from itmes in ObjDAL.HRMS_Employee_Master_PersonalDetails
                                 where itmes.Creation_Company == CreationCompany && itmes.Emp_Code.ToLower().Trim() == Emp_Code
                                 select new
                                 {
                                     Emp_name = itmes.Emp_First_Name + " " + itmes.Emp_Last_Name + " " + itmes.Emp_Middle_Name,
                                     Emp_Code = itmes.Emp_Code
                                 }).ToList();
            if (objEmPCodeDUP.Count > 0)
            {

                ddlEmpName.SelectedItem.Text = objEmPCodeDUP[0].Emp_name;

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Emp Code is Not Valid');", true);
                txtEmpCode.Text = "";
                txtEmpCode.Focus();
                ddlEmpName.SelectedValue = "0";


            }
        }
        protected void ddlEmpName_SelectedIndexChanged(object sender, EventArgs e)
        {
            string CreationCompany = (string)Session["ClientRegistrationNo"].ToString();
            txtEmpCode.Text = ddlEmpName.SelectedValue;

        }
        public void Emp_NameBinding()
        {
            string CreationCompany = Session["ClientRegistrationNo"].ToString();

            var objEmPCodeDUP = (from itmes in ObjDAL.HRMS_Employee_Master_PersonalDetails
                                 where itmes.Creation_Company == CreationCompany
                                 select new
                                 {
                                     Emp_name = itmes.Emp_Last_Name + " " + itmes.Emp_First_Name + " " + itmes.Emp_Middle_Name,
                                     Emp_Code = itmes.Emp_Code
                                 }).ToList();
            if (objEmPCodeDUP.Count > 0)
            {
                ddlEmpName.DataSource = objEmPCodeDUP;
                ddlEmpName.DataTextField = "Emp_name";
                ddlEmpName.DataValueField = "Emp_Code";
                ddlEmpName.DataBind();
                ddlEmpName.Items.Insert(0, new ListItem("--Select--", "0"));
            }

        }
        //public void payslipbindempdetails()
        //{

        //    string Cmpy = (string)Session["ClientRegistrationNo"];
        //    string Brnach = (string)Session["BranchID"];
        //    string UserType = (string)Session["User_Type"];
        //    string UserName = (string)Session["User_Name"];
        //    string Cration_Company = (string)Session["CreationCompany"]; //"000001";

        //    if (Session["User_Type"].ToString() == "CCF")
        //    {
        //        string EMP_Code = (from EM in ObjDAL.HRMS_Employee_Master_PersonalDetails where EM.User_Name == UserName select EM.Emp_Code).FirstOrDefault();
        //        var objHeaderInfo1 = (from header in ObjDAL.HRMS_Employee_Official_Info_Head_Details
        //                              where header.Creation_Company == Cmpy && header.Emp_Code == EMP_Code
        //                              select header).ToList();
        //        if (objHeaderInfo1.Count > 0)
        //        {

        //        }
        //    }
        //}
        public void grd2Bind()
        {
            try
            {
                string objConfig = ConfigurationManager.ConnectionStrings["Dbconnection"].ToString();
                objSqlConnection = new SqlConnection(objConfig);
                objSqlCommand = new SqlCommand("select LeaveFrom,TypeOfLeaveApple,Noof_Days,Leave_Status,Modified_By,Balances_Available from HRMS_Leave_Application where Emp_Code='" + txtEmpCode.Text + "' ", objSqlConnection);
                objSqlCommand.Connection = objSqlConnection;
                objSqlConnection.Open();
                da = new SqlDataAdapter(objSqlCommand);
                DataTable dt = new DataTable();
                da.Fill(dt);
                grdleave.DataSource = dt;
                grdleave.DataBind();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
        }
        public void grdBind()
        {
            try
            {
                string objConfig = ConfigurationManager.ConnectionStrings["Dbconnection"].ToString();
                objSqlConnection = new SqlConnection(objConfig);
                objSqlCommand = new SqlCommand("select Created_Date,Designation,pay_scale,Basic_salary,Approved_By from HRMS_Employee_Increments where Emp_Code='" + txtEmpCode.Text + "' ", objSqlConnection);
                objSqlCommand.Connection = objSqlConnection;
                objSqlConnection.Open();
                da = new SqlDataAdapter(objSqlCommand);
                DataTable dt = new DataTable();
                da.Fill(dt);
                grdincrement.DataSource = dt;
                grdincrement.DataBind();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
        }

        protected void btnincerments_Click(object sender, EventArgs e)
        {
            grdBind();
            grdincrement.Visible = true;
            grdleave.Visible = false;
            pnlleave.Visible = false;
            pnlincrement.Visible = true;

        }
    }
}