﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="gisreports.aspx.cs" Inherits="HRMS.Reports.gisreports" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div id="main">
        <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">Report</a><i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="EmployeeReport.aspx">GIS Report</a><i class="fa fa-angle-right"></i>
                    </li>
                   
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>

                 
                   </div>

                 <div> <h1 class="auto-style1"> <em>GIS Report:</em></h1></div>
               <div class="col-sm-12">
                        
                   <div class="col-lg-6">
                       <asp:GridView ID="grdleaves" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                                                    Style="margin-bottom: 0.83%; margin-top: 0.83%; width:100%;" runat="server"
                                                                    ShowFooter="True" AutoGenerateColumns="False" DataKeyNames="Sno">
                                                                    <AlternatingRowStyle BackColor="#EEEEEE" />
                                                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                                    <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                                    <SortedDescendingHeaderStyle BackColor="#000065" />
                                                                    <Columns>
                                                                        <asp:BoundField ReadOnly="true" HeaderText="Sno" DataField="Sno" />
                                                                       
                                                                        <asp:TemplateField HeaderText="Employee Code">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("Emp_Code") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Employe Salery">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("Basic_Amount") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="GIS Amount">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("GIS") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                                                                                          </Columns>
                                                                </asp:GridView>

                    </div>
                   </div>
         </div>
</asp:Content>
