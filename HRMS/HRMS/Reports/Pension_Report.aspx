﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="Pension_Report.aspx.cs" Inherits="HRMS.Reports.Pension_Report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="main">
        <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">Report</a><i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="Increment.aspx">Pension Report</a><i class="fa fa-angle-right"></i>
                    </li>

                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>

        </div>

        <div class="box box-bordered box-color" style="margin-bottom: 25px;">
            <div class="box-title">
                <h3>
                    <i class="fa fa-th-list"></i>Pension</h3>
            </div>
            <div class="box-content">


                <div class="row">
                    <div class="col-md-11">

                        <div class="form-group col-md-3">
                            <label for="textfield" class="control-label col-sm-3" style="">
                                Circle</label>
                            <div class="col-sm-9" style="" id="Div39">
                                <asp:DropDownList ID="ddlcircle" runat="server" class="form-control DropdownCss" AutoPostBack="true" OnSelectedIndexChanged="ddlcircle_SelectedIndexChanged">
                                    <asp:ListItem Text="---Select---" Value="Select"></asp:ListItem>
                                    <asp:ListItem Text="Adilabad" Value="Adilabad"></asp:ListItem>
                                    <asp:ListItem Text="Kawal Tiger Reserve" Value="Kawal Tiger Reserve"></asp:ListItem>
                                    <asp:ListItem Text="Kothagudem" Value="Kothagudem"></asp:ListItem>
                                    <asp:ListItem Text="Khammam" Value="Khammam"></asp:ListItem>
                                    <asp:ListItem Text="Nizamabad" Value="Nizamabad"></asp:ListItem>
                                    <asp:ListItem Text="Amrabada Tiger Reserve" Value="Amrabada Tiger Reserve"></asp:ListItem>
                                    <asp:ListItem Text="Hyderabad" Value="Hyderabad"></asp:ListItem>
                                    <asp:ListItem Text="Karimnagar" Value="Karimnagar"></asp:ListItem>
                                    <asp:ListItem Text="Warangal" Value="Warangal"></asp:ListItem>
                                    <asp:ListItem Text="Medak" Value="Medak"></asp:ListItem>
                                    <asp:ListItem Text="Mahabubnagar" Value="Mahabubnagar"></asp:ListItem>
                                    <asp:ListItem Text="Ranga Reddy" Value="Ranga Reddy"></asp:ListItem>
                                    <asp:ListItem Text="R & D Circle" Value="R & D Circle"></asp:ListItem>
                                    <asp:ListItem Text="STC Circle" Value="STC Circle"></asp:ListItem>
                                    <asp:ListItem Text="Director Zoo Parks" Value="Director Zoo Parks"></asp:ListItem>

                                </asp:DropDownList>


                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="textfield" class="control-label col-sm-3" style="">
                                District</label>
                            <div class="col-sm-9" style="" id="Div40">
                                <asp:DropDownList ID="ddldistrictforestofficers" runat="server" class="form-control DropdownCss" AutoPostBack="true" OnSelectedIndexChanged="ddldistrictforestofficers_SelectedIndexChanged">
                                    <asp:ListItem Text="---Select---" Value="---Select---"></asp:ListItem>



                                </asp:DropDownList>


                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="textfield" class="control-label col-sm-3" style="">
                                Divisions</label>
                            <div class="col-sm-9" style="" id="Div41">
                                <asp:DropDownList ID="ddlforestdivisions" runat="server" class="form-control DropdownCss" OnSelectedIndexChanged="ddlforestdivisions_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Text="---Select---" Value="---Select---"></asp:ListItem>

                                </asp:DropDownList>


                            </div>
                        </div>
                        <div class="form-group col-md-3" id="Div1" runat="server">
                            <label for="textfield" class="control-label col-sm-5" style="">
                                Department<span id="Span2" runat="server" style="color: Red; display: inline;">*</span></label>
                            <div class="col-sm-7" style="" id="Div2">
                                <asp:DropDownList ID="ddldepartment" runat="server" class="form-control DropdownCss" OnSelectedIndexChanged="ddldepartment_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem>---Select---</asp:ListItem>
                                    <asp:ListItem>DFO</asp:ListItem>
                                    <asp:ListItem>FDO</asp:ListItem>
                                    <asp:ListItem>PCCF</asp:ListItem>
                                </asp:DropDownList>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                    <div class="box-title">
                        <h3>
                            <i class="fa fa-th-list"></i>Pension Report</h3>
                    </div>
                    <div class="box-content">
                        <div class="col-md-12 table-responsive">
                            <asp:GridView ID="grdincrementreport" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%;" runat="server"
                                ShowFooter="True" AutoGenerateColumns="False" DataKeyNames="Sno">
                                <AlternatingRowStyle BackColor="#EEEEEE" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#000065" />
                                <Columns>
                                    <asp:BoundField ReadOnly="true" HeaderText="Sno" DataField="Sno" />
                                    <asp:TemplateField HeaderText="Employee ID">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("Employee_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee Name">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("Employee_Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Department">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("Name_Of_Circle") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name Of The Employee Retired & retiring before (18) Months">
                                        <ItemTemplate>
                                            <asp:Label ID="Label15" runat="server" ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Designation">
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Date Of Birth">
                                        <ItemTemplate>
                                              <asp:Label ID="Label5" runat="server" Text='<%# Eval("DOb", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Date Of Entry Into Service">
                                        <ItemTemplate>
                                             <asp:Label ID="Label6" runat="server" Text='<%# Eval("date_of_entry_service", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Palce of Last Station">
                                        <ItemTemplate>
                                            <asp:Label ID="Label7" runat="server" Text='<%# Eval("Date_retirement_death", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Date Of Retirement/Death">
                                        <ItemTemplate>
                                            <asp:Label ID="Label8" runat="server" Text='<%# Eval("Date_retirement_death", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date Of Receipt PPs">
                                        <ItemTemplate>
                                             <asp:Label ID="Label10" runat="server" Text='<%# Eval("date_of_receipt_pps") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status On Processing PPs">
                                        <ItemTemplate>
                                              <asp:Label ID="Label11" runat="server" Text='<%# Eval("receipt_pps") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date Of Forwarding PPs to The AG/AD">
                                        <ItemTemplate>
                                            <asp:Label ID="Label12" runat="server" Text='<%# Eval("date_of_forwarding_pension_to_ag") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date Of Receipt of PVR">
                                        <ItemTemplate>
                                            <asp:Label ID="Label13" runat="server" Text='<%# Eval("date_pvr", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date Of Sending for authorisation of PBs">
                                        <ItemTemplate>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date Of Receipt of authorisation">
                                        <ItemTemplate>
                                             <asp:Label ID="Label15" runat="server" Text='<%# Eval("date_sending_auterisation") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Resons Not Sending of authorisation">
                                        <ItemTemplate>
                                               <asp:Label ID="Label16" runat="server" Text='<%# Eval("reason_not_sending_autherisation") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Provisional">
                                        <ItemTemplate>
                                            <asp:Label ID="Label17" runat="server" Text='<%# Eval("due_to_disc_cases") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Anticipatory">
                                        <ItemTemplate>
                                            <asp:Label ID="Label18" runat="server" Text='<%# Eval("non_finalisation_ndc") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Family">
                                        <ItemTemplate>
                                            <asp:Label ID="Label19" runat="server" Text='<%# Eval("death_employee") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action Taken On Conclusion of Deptl.proc and Issue Of Autherisation">
                                        <ItemTemplate>
                                            <asp:Label ID="Label20" runat="server" Text='<%# Eval("forward_ag_autherization") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks">
                                        <ItemTemplate>
                                            <asp:Label ID="Label21" runat="server" Text='<%# Eval("autherisation_recd") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



</asp:Content>
