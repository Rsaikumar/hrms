﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using DAL;
//using BAL;
//using System.Data.Linq.SqlClient;

//namespace HRMS.Reports
//{
//    public partial class Pension_Report : System.Web.UI.Page
//    {
//        ERP_DatabaseDataContext objdb = new ERP_DatabaseDataContext();
//        protected void Page_Load(object sender, EventArgs e)
//        {
//            if (!IsPostBack)
//            {
//                bindtotal();
//            }

//        }
//        protected void ddlcircle_SelectedIndexChanged(object sender, EventArgs e)
//        {
//            if (ddlcircle.SelectedItem.Text == "Adilabad")
//            {
//                ddldistrictforestofficers.Items.Clear();
//                ddldistrictforestofficers.Items.Add("---Select---");
//                ddldistrictforestofficers.Items.Add("Adilabad");
//                ddldistrictforestofficers.Items.Add("Komarambheem");


//            }
//            else if (ddlcircle.SelectedItem.Text == "Kawal Tiger Reserve")
//            {
//                ddldistrictforestofficers.Items.Clear();
//                ddldistrictforestofficers.Items.Add("---Select---");
//                ddldistrictforestofficers.Items.Add("Mancherial");
//                ddldistrictforestofficers.Items.Add("Nirmal(New)");

//            }
//            else if (ddlcircle.SelectedItem.Text == "Kothagudem")
//            {
//                ddldistrictforestofficers.Items.Clear();
//                ddldistrictforestofficers.Items.Add("---Select---");
//                ddldistrictforestofficers.Items.Add("Kothagudem");
//                ddldistrictforestofficers.Items.Add("Mahabubabad");

//            }
//            else if (ddlcircle.SelectedItem.Text == "Khammam")
//            {
//                ddldistrictforestofficers.Items.Clear();
//                ddldistrictforestofficers.Items.Add("---Select---");
//                ddldistrictforestofficers.Items.Add("Khammam");
//                ddldistrictforestofficers.Items.Add("Suryapet");

//            }
//            else if (ddlcircle.SelectedItem.Text == "Nizamabad")
//            {
//                ddldistrictforestofficers.Items.Clear();
//                ddldistrictforestofficers.Items.Add("---Select---");
//                ddldistrictforestofficers.Items.Add("Nizamabad");
//                ddldistrictforestofficers.Items.Add("Kamareddy");

//            }
//            else if (ddlcircle.SelectedItem.Text == "Amrabada Tiger Reserve")
//            {
//                ddldistrictforestofficers.Items.Clear();
//                ddldistrictforestofficers.Items.Add("---Select---");
//                ddldistrictforestofficers.Items.Add("Nagarkarnool");
//                ddldistrictforestofficers.Items.Add("Nalgonda");

//            }
//            else if (ddlcircle.SelectedItem.Text == "Hyderabad")
//            {
//                ddldistrictforestofficers.Items.Clear();
//                ddldistrictforestofficers.Items.Add("---Select---");
//                ddldistrictforestofficers.Items.Add("Hyderabad");
//                ddldistrictforestofficers.Items.Add("Medchal");
//                ddldistrictforestofficers.Items.Add("Yadadri");

//            }
//            else if (ddlcircle.SelectedItem.Text == "Karimnagar")
//            {
//                ddldistrictforestofficers.Items.Clear();
//                ddldistrictforestofficers.Items.Add("---Select---");
//                ddldistrictforestofficers.Items.Add("Jagital(New)");
//                ddldistrictforestofficers.Items.Add("Karimnagar + Sircilla");
//                ddldistrictforestofficers.Items.Add("Peddapally");

//            }
//            else if (ddlcircle.SelectedItem.Text == "Warangal")
//            {

//                ddldistrictforestofficers.Items.Add("Bhupalpally");
//                ddldistrictforestofficers.Items.Add("Warngal(U) + Jangoan");
//                ddldistrictforestofficers.Items.Add("Warngal(R)");

//            }
//            else if (ddlcircle.SelectedItem.Text == "Medak")
//            {
//                ddldistrictforestofficers.Items.Clear();
//                ddldistrictforestofficers.Items.Add("---Select---");
//                ddldistrictforestofficers.Items.Add("Siddipet");
//                ddldistrictforestofficers.Items.Add("Medak");
//                ddldistrictforestofficers.Items.Add("Sangareddy");

//            }
//            else if (ddlcircle.SelectedItem.Text == "Mahabubnagar")
//            {
//                ddldistrictforestofficers.Items.Clear();
//                ddldistrictforestofficers.Items.Add("---Select---");
//                ddldistrictforestofficers.Items.Add("Mahabubnagar");
//                ddldistrictforestofficers.Items.Add("Gadwal + Wanaparthy");

//            }
//            else if (ddlcircle.SelectedItem.Text == "Ranga Reddy")
//            {
//                ddldistrictforestofficers.Items.Clear();
//                ddldistrictforestofficers.Items.Add("---Select---");

//                ddldistrictforestofficers.Items.Add("Ranga Reddy");
//                ddldistrictforestofficers.Items.Add("Vikarabad");

//            }
//            else
//            {

//            }

//        }

//        protected void ddldistrictforestofficers_SelectedIndexChanged(object sender, EventArgs e)
//        {
//            if (ddldistrictforestofficers.SelectedItem.Text == "Adilabad")
//            {
//                ddlforestdivisions.Items.Clear();
//                ddlforestdivisions.Items.Add("---Select---");
//                ddlforestdivisions.Items.Add("Adilabad");
//                ddlforestdivisions.Items.Add("Utnoor FDPT");
//                ddlforestdivisions.Items.Add("Echoda");
//            }
//            else if (ddldistrictforestofficers.SelectedItem.Text == "Komarambheem")
//            {
//                ddlforestdivisions.Items.Clear();
//                ddlforestdivisions.Items.Add("---Select---");
//                ddlforestdivisions.Items.Add("Asifabad");
//                ddlforestdivisions.Items.Add("Khagaznagar");

//            }
//            else if (ddldistrictforestofficers.SelectedItem.Text == "Mancherial")
//            {
//                ddlforestdivisions.Items.Clear();
//                ddlforestdivisions.Items.Add("---Select---");
//                ddlforestdivisions.Items.Add("Jannaram FDPT");
//                ddlforestdivisions.Items.Add("Chennur");
//                ddlforestdivisions.Items.Add("Bellampally");
//                ddlforestdivisions.Items.Add("Mancherial");
//            }
//            else if (ddldistrictforestofficers.SelectedItem.Text == "Nirmal(New)")
//            {
//                ddlforestdivisions.Items.Clear();
//                ddlforestdivisions.Items.Add("---Select---");
//                ddlforestdivisions.Items.Add("Nirmal");

//                ddlforestdivisions.Items.Add("Khanapur");
//            }
//            else if (ddldistrictforestofficers.SelectedItem.Text == "Kothagudem")
//            {
//                ddlforestdivisions.Items.Clear();
//                ddlforestdivisions.Items.Add("---Select---");
//                ddlforestdivisions.Items.Add("Kothagudem");
//                ddlforestdivisions.Items.Add("Yellanadu");
//                ddlforestdivisions.Items.Add("Paloncha");
//                ddlforestdivisions.Items.Add("Mangur");
//                ddlforestdivisions.Items.Add("WLM Kinnerasani");
//                ddlforestdivisions.Items.Add("Bhadrachalam");
//            }
//            else if (ddldistrictforestofficers.SelectedItem.Text == "Mahabubabad")
//            {
//                ddlforestdivisions.Items.Clear();
//                ddlforestdivisions.Items.Add("---Select---");
//                ddlforestdivisions.Items.Add("Mahabubabad");
//                ddlforestdivisions.Items.Add("WL Gudur");

//            }
//            else if (ddldistrictforestofficers.SelectedItem.Text == "Khammam")
//            {
//                ddlforestdivisions.Items.Clear();
//                ddlforestdivisions.Items.Add("---Select---");
//                ddlforestdivisions.Items.Add("Khammam");

//                ddlforestdivisions.Items.Add("Sathupally");
//            }
//            else if (ddldistrictforestofficers.SelectedItem.Text == "Nizamabad")
//            {
//                ddlforestdivisions.Items.Clear();
//                ddlforestdivisions.Items.Add("---Select---");
//                ddlforestdivisions.Items.Add("Nizamabad");

//                ddlforestdivisions.Items.Add("Armoor");
//            }
//            else if (ddldistrictforestofficers.SelectedItem.Text == "Kamareddy")
//            {
//                ddlforestdivisions.Items.Clear();
//                ddlforestdivisions.Items.Add("---Select---");
//                ddlforestdivisions.Items.Add("Kamareddy");

//                ddlforestdivisions.Items.Add("Banswada");
//            }
//            else if (ddldistrictforestofficers.SelectedItem.Text == "Nagarkarnool")
//            {
//                ddlforestdivisions.Items.Clear();
//                ddlforestdivisions.Items.Add("---Select---");
//                ddlforestdivisions.Items.Add("Achampet");

//                ddlforestdivisions.Items.Add("Amarbad");
//            }
//            else if (ddldistrictforestofficers.SelectedItem.Text == "Nalgonda")
//            {
//                ddlforestdivisions.Items.Clear();
//                ddlforestdivisions.Items.Add("---Select---");
//                ddlforestdivisions.Items.Add("Nalgonda");

//                ddlforestdivisions.Items.Add("Nagarjunsagar WL");
//            }
//            else if (ddldistrictforestofficers.SelectedItem.Text == "Bhupalpally")
//            {
//                ddlforestdivisions.Items.Clear();
//                ddlforestdivisions.Items.Add("---Select---");
//                ddlforestdivisions.Items.Add("Bhupalpally");
//                ddlforestdivisions.Items.Add("Mahadevpur");
//                ddlforestdivisions.Items.Add("Mulugu");
//                ddlforestdivisions.Items.Add("Tadval");
//                ddlforestdivisions.Items.Add("WLM Eturungaram");
//                ddlforestdivisions.Items.Add("Venkatapuram");
//            }
//            else if (ddldistrictforestofficers.SelectedItem.Text == "Ranga Reddy")
//            {
//                ddlforestdivisions.Items.Clear();
//                ddlforestdivisions.Items.Add("---Select---");
//                ddlforestdivisions.Items.Add("Shamshabad");

//                ddlforestdivisions.Items.Add("Amangal");
//            }

//            else
//            {

//            }

//        }
//        public void bindtotal()
//        {
//            var j = (from inmt in objdb.pens
//                     select inmt).ToList();
//             if (j.Count > 0)
//            {
//                grdincrementreport.DataSource = j;
//                grdincrementreport.DataBind();
//            }
//            else
//            {
//                grdincrementreport.EmptyDataText = "No Records Recornd";
//            }

//        }
//        public void bind()
//        {
            

//            var i = (from inmt in objdb.pension_forms
//                     where  inmt.circle==ddlcircle.SelectedItem.Text && inmt.distric==ddldistrictforestofficers.SelectedItem.Text  && inmt.sub_distric==ddlforestdivisions.SelectedItem.Text && inmt.Designation==ddldepartment.SelectedItem.Text
//                     select inmt).ToList();

             
//            if (i.Count > 0)
//            {

//                grdincrementreport.DataSource = i;
//                grdincrementreport.DataBind();
//            }
//            else
//            {
//                grdincrementreport.EmptyDataText = "No Records Recornd";
//            }
//        }

//        protected void ddlforestdivisions_SelectedIndexChanged(object sender, EventArgs e)
//        {
//            bind();
//        }

//        protected void ddldepartment_SelectedIndexChanged(object sender, EventArgs e)
//        {
//            bind();

//        }
//    }
//}