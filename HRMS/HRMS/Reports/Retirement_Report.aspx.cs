﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BAL;
using DAL;

namespace HRMS.Reports
{
    public partial class Retirement_Report : System.Web.UI.Page
    {
        ERP_DatabaseDataContext objdb = new ERP_DatabaseDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            bind();
            Emp_NameBinding();
        }
        public void bind()
        {

            var i = (from inmt in objdb.employee_detail_informations
                     select inmt).ToList();
            if (i.Count > 0)
            {
                grdincrementreport.DataSource = i;
                grdincrementreport.DataBind();


            }
        }
        public void Emp_NameBinding()
        {
            string CreationCompany = Session["ClientRegistrationNo"].ToString();

            var objEmPCodeDUP = (from itmes in objdb.HRMS_Employee_Master_PersonalDetails
                                 where itmes.Creation_Company == CreationCompany
                                 select new
                                 {
                                     Emp_name = itmes.Emp_Last_Name + " " + itmes.Emp_First_Name + " " + itmes.Emp_Middle_Name,
                                     Emp_Code = itmes.Emp_Code
                                 }).ToList();
            if (objEmPCodeDUP.Count > 0)
            {
                ddlEmpName.DataSource = objEmPCodeDUP;
                ddlEmpName.DataTextField = "Emp_name";
                ddlEmpName.DataValueField = "Emp_Code";
                ddlEmpName.DataBind();
                ddlEmpName.Items.Insert(0, new ListItem("--Select--", "0"));
            }

        }

        protected void ddlEmpName_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtEmpCode.Text = ddlEmpName.SelectedValue;
        }
    }
}