﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="Saleryreport.aspx.cs" Inherits="HRMS.Reports.Saleryreport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div class="container">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">Report</a><i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="EmployeeReport.aspx">Salery Report</a><i class="fa fa-angle-right"></i>
                    </li>

                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>


        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                    <div class="box-title">
                        <h3>
                            <i class="fa fa-th-list"></i>salary Report</h3>
                    </div>
                    <div class="box-content">
                        <div class="col-md-12">

                            <div class="col-md-12">
                                <asp:GridView ID="grdleaves" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                    Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%;" runat="server"
                                    ShowFooter="True" AutoGenerateColumns="False" DataKeyNames="Sno">
                                    <AlternatingRowStyle BackColor="#EEEEEE" />
                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#000065" />
                                    <Columns>
                                        <asp:BoundField ReadOnly="true" HeaderText="Sno" DataField="Sno" />

                                        <asp:TemplateField HeaderText="Employee Code">
                                            <ItemTemplate>
                                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("Emp_Code") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Employe Salary">
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("Basic_Amount") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Credited Date">
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("Created_Date") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                        <div class="box-title">
                            <h3>
                                <i class="fa fa-th-list"></i>Details Salary Report</h3>
                        </div>
                        <div class="box-content">
                            <div class="col-md-12">
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-md-5" style="">
                                        Emp Code
                                    </label>
                                    <div class="col-md-7" style="">
                                        <asp:TextBox ID="txtEmpCode" runat="server" placeholder="Emp Code" MaxLength="50"
                                            class="form-control" AutoPostBack="true" OnTextChanged="txtEmpCode_TextChanged"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-md-5" style="">
                                        Employee Name<span id="Span1" runat="server" style="color: Red; display: inline;">*</span></label>
                                    <div class="col-md-7" style="" id="">
                                        <asp:DropDownList ID="ddlEmpName" runat="server" class="form-control DropdownCss" TabIndex="1" AutoPostBack="True" OnSelectedIndexChanged="ddlEmpName_SelectedIndexChanged">
                                        </asp:DropDownList>

                                    </div>
                                </div>


                                <div class="col-md-10" align="center">
                                    <asp:Button ID="btnSave" runat="server" Text="Show" class="btn btn-primary" OnClick="btnSave_Click" />
                                    <%--OnClientClick="return fileUploadValidation()"--%>
                                    <%--<asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-primary" OnClick="btnUpdate_Click" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Reset" class="btn" OnClick="btnCancel_Click" />--%>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="Div4" style="width: 100%; height: auto; overflow: auto" runat="server">
                                <div class="col-md-12">
                                    <asp:GridView ID="grdpayroll" class="table table-bordered CustomerAddress" runat="server"
                                        ShowFooter="True" AutoGenerateColumns="False" DataKeyNames="Sno">

                                        <Columns>
                                            <asp:BoundField ReadOnly="true" HeaderText="Sno" DataField="Sno" />

                                            <asp:TemplateField HeaderText="Employee Code">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtProdCode" runat="server" placeholder="Employee Code" class="form-control"
                                                        Text='<%# Eval("Emp_Code")%>' MaxLength="10" AutoPostBack="true" Height="24px" Width="60px" ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Month">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtMonth" runat="server" placeholder="Month"
                                                        class="form-control" MaxLength="10" Height="24px" Width="86px" ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Year">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtYear" runat="server" placeholder="Year"
                                                        class="form-control" MaxLength="10" Height="24px" Width="86px" ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Present Days">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtpdays" runat="server" placeholder="Present Days"
                                                        class="form-control" MaxLength="10" Height="24px" Width="86px" ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Basic">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtBasic" runat="server" placeholder="Basic" class="form-control"
                                                        Text='<%# Eval("Basic_Amount")%>' MaxLength="10" Width="58px" ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Gross">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtgross" runat="server" placeholder="Gross" class="form-control"
                                                        Text='<%# Eval("Gross_Amount")%>' MaxLength="10" Width="58px" ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="DA">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtDA" runat="server" placeholder="DA"
                                                        class="form-control" Text='<%# Eval("DA")%>' MaxLength="10" Height="24px" Width="86px" ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="HRA">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtHRA" runat="server" placeholder="HRA"
                                                        class="form-control" Text='<%# Eval("HRA")%>' MaxLength="10" Height="24px" Width="86px" ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="CCA">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtCCA" runat="server" placeholder="CCA"
                                                        class="form-control" Text='<%# Eval("CCA")%>' MaxLength="10" Height="24px" Width="86px" ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="OA">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtOA" runat="server" placeholder="OA"
                                                        class="form-control" Text='<%# Eval("OA")%>' MaxLength="10" Height="24px" Width="86px" ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PF">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtPF" runat="server" placeholder="PF"
                                                        class="form-control" Text='<%# Eval("PF")%>' MaxLength="10" Height="24px" Width="86px" ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ESI">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtESI" runat="server" placeholder="ESI"
                                                        class="form-control" Text='<%# Eval("ESI")%>' MaxLength="10" Height="24px" Width="86px" ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="EESI">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtEESI" runat="server" placeholder="EESI"
                                                        class="form-control" Text='<%# Eval("EESI")%>' MaxLength="10" Height="24px" Width="86px" ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PTAX">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtPTAX" runat="server" placeholder="PTAX"
                                                        class="form-control" Text='<%# Eval("PTAX")%>' MaxLength="10" Height="24px" Width="86px" ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="TDS">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtTDS" runat="server" placeholder="TDS"
                                                        class="form-control" Text='<%# Eval("TDS")%>' MaxLength="10" Height="24px" Width="86px" ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="APGLI">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtAPGLI" runat="server" placeholder="APGLI"
                                                        class="form-control" Text='<%# Eval("APGLI")%>' MaxLength="10" Height="24px" Width="86px" ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="GIS">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtGIS" runat="server" placeholder="GIS"
                                                        class="form-control" Text='<%# Eval("GIS")%>' MaxLength="10" Height="24px" Width="86px" ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="MCA">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtMCA" runat="server" placeholder="MCA"
                                                        class="form-control" Text='<%# Eval("MCA")%>' MaxLength="10" Height="24px" Width="86px" ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="BA">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtBA" runat="server" placeholder="BA" class="form-control"
                                                        Text='<%# Eval("BA")%>' MaxLength="10" Width="58px" ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

