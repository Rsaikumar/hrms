﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BAL;
using DAL;
using System.Data.Linq.SqlClient;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using System.IO;

namespace HRMS.Reports
{

    public partial class EmployesReport : System.Web.UI.Page
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();
        DefineLeavesBAL objSCM_LeaveBAL = new DefineLeavesBAL();
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();
         SqlConnection objSqlConnection;
        SqlCommand objSqlCommand;
        SqlDataAdapter da;
        protected void Page_Load(object sender, EventArgs e)
        {
            Bind_Grid();
            setinitialrow();
        }
         
        public void Bind_Grid()
        {
            try
            {
                string Cmpy = (string)Session["ClientRegistrationNo"];
                string Brnach = (string)Session["BranchID"];
                string UserType = (string)Session["User_Type"];
                string UserName = (string)Session["User_Name"];
                string Cration_Company = (string)Session["CreationCompany"]; //"000001";
                //if (UserType == "User")
                //{


                //string EMP_Code = (from EM in objDAL.HRMS_Employee_Master_PersonalDetails where EM.User_Name == UserName select EM.Emp_Code).FirstOrDefault();
                var objHeaderInfo = (from header in objDAL.HRMS_Leave_Allotments
                                     where header.Creation_Company == Cmpy
                                     
                                     select header).ToList();
                //// List<Administrator_UserRegistration> ObjuserInfo = objSCM_userRegistration.Get_userRegDetails(Session["ClientRegistrationNo"].ToString());
                if (objHeaderInfo.Count > 0)
                {

                    grdleaves.DataSource = objHeaderInfo;
                    grdleaves.DataBind();
                }
                else
                {
                    // // // Grid view using setting initial rows displaying Empty Header Displying
                    setinitialrow();
                }
            }


            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }
          
        }
          

        
        public void setinitialrow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("Sno", typeof(string)));
            dt.Columns.Add(new DataColumn("Emp_Code", typeof(string)));
            dt.Columns.Add(new DataColumn("Emp_Name", typeof(string)));
            dt.Columns.Add(new DataColumn("Emp_DOB", typeof(string)));
            dt.Columns.Add(new DataColumn("Emp_Age", typeof(string)));
            dt.Columns.Add(new DataColumn("Gender", typeof(string)));
            dt.Columns.Add(new DataColumn("Telephone", typeof(string)));
            dt.Columns.Add(new DataColumn("Permanent_Email", typeof(string)));
            dt.Columns.Add(new DataColumn("Educate", typeof(string)));
           
           

            dr = dt.NewRow();
            dr["Sno"] = string.Empty;
            dr["Leave_Code"] = string.Empty;
            dr["Emp_Code"] = string.Empty;
            dr["Emp_Name"] = string.Empty;
            dr["TypeOfLeaveApple"] = string.Empty;
            // dr["Prod_Spec"] = string.Empty;
            dr["Noof_Days"] = string.Empty;
            // dt.Columns.Add(new DataColumn("Leave_Status", typeof(string)));
            //dt.Columns.Add(new DataColumn("Created_Date", typeof(string)));


            dt.Rows.Add(dr);
            ViewState["CurrentTable"] = dt;
            grdleaves.DataSource = dt;
            grdleaves.DataBind();
        }
    }
}

    

    