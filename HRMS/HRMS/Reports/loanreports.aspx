﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="loanreports.aspx.cs" Inherits="HRMS.Reports.loanreports" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div id="main">
     <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">Report</a><i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="LeaveReport.aspx">Loan Report</a><i class="fa fa-angle-right"></i>
                    </li>
                   
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>

                 
                   </div>
         <div class="row">
                <div class="col-sm-12">
                    <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                        <div class="box-title">
                            <h3>
                                <i class="fa fa-th-list"></i>Loan Report</h3>
                        </div>
                        <div class="box-content">
    <div class="col-sm-12">
                        
                   <div class="col-lg-6">
                                                <asp:GridView ID="grd_LEave_Balance" class="table table-bordered CustomerAddress" runat="server"
                                                    ShowFooter="true" AutoGenerateColumns="False" 
                                                    AllowPaging="true" PageSize="3" AllowSorting="true"  OnRowDataBound="grd2Bind"
                                                    DataKeyNames="Emp_Code">
                                                    <AlternatingRowStyle BackColor="#EEEEEE" />
                                                    <FooterStyle BackColor="#EEEEEE" ForeColor="Black" />
                                                    <PagerStyle BackColor="#EEEEEE" ForeColor="Black" HorizontalAlign="Center" />
                                                    <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                    <SortedDescendingHeaderStyle BackColor="#000065" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Emp Code" SortExpression="Emp_Code">
                                                            <ItemTemplate>
                                                                <%--<asp:LinkButton ID="lblEmpCode" runat="server" Text='<%#Eval("Po_No") %>' Style="text-align: center;"
                                                                CausesValidation="false" CommandArgument='<%# Eval("Po_No")%>' CommandName="Vch_No" ></asp:LinkButton>--%>
                                                                <asp:Label ID="lblEmpCode" runat="server" Text='<%# Eval("Emp_Code")%>' Style="text-align: center;"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Emp Name" SortExpression="Emp_Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEmpName" runat="server" Text='<%# Eval("Emp_Name") %>' Style="text-align: center;"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                      <%--  <asp:TemplateField HeaderText="Department" SortExpression="Department">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("Department")%>' Style="text-align: center;"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                       <asp:TemplateField ItemStyle-Width="50%">
                                                            <HeaderTemplate>
                                                                <table class="table table-hover table-nomargin table-bordered CustomerAddress" >
                                                                    <tr>
                                                                        <td style="width: 39%;">
                                                                           Loan Name
                                                                        </td>
                                                                       <%-- <td style="">
                                                                            Leave Type
                                                                        </td>--%>
                                                                        <td style="">
                                                                           Basic Salery
                                                                        </td>
                                                                         <td style="">
                                                                           Loan Amount
                                                                        </td>
                                                                       <%-- <td style="">
                                                                            Used Leaves
                                                                        </td>
                                                                         <td style="">
                                                                            Balance Leaves
                                                                        </td>--%>
                                                                    </tr>
                                                                </table>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:GridView ID="grd2" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                                                    Style="margin-bottom: 0.83%; margin-top: 0.83%;" runat="server"
                                                                    AutoGenerateColumns="false" ShowHeader="false">
                                                                    <AlternatingRowStyle BackColor="#EEEEEE" />
                                                    <FooterStyle BackColor="#EEEEEE" ForeColor="Black" />
                                                    <PagerStyle BackColor="#EEEEEE" ForeColor="Black" HorizontalAlign="Center" />
                                                    <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                    <SortedDescendingHeaderStyle BackColor="#000065" />
                                                                    <Columns>
                                                                        <asp:BoundField DataField="Type_Of_Loan_Apply" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemStyle Width="39%" />
                                                                        </asp:BoundField>
                                                                        <%--<asp:BoundField DataField="Short_Name" HeaderText="Leave_Type" ItemStyle-HorizontalAlign="Right">
                                                                            <ItemStyle Width="13%" />
                                                                        </asp:BoundField>--%>
                                                                        <asp:BoundField DataField="Basic_Amount" HeaderText="Basic_Amount" ItemStyle-HorizontalAlign="Right">
                                                                            <ItemStyle Width="20%" />
                                                                        </asp:BoundField>
                                                                         <asp:BoundField DataField="Loan_amount_Required" HeaderText="Loan_amount_Required" ItemStyle-HorizontalAlign="Right">
                                                                            <ItemStyle Width="20%" />
                                                                        </asp:BoundField>
                                                                        <%--<asp:BoundField DataField="Used_Leaves" HeaderText="Used_Leaves" ItemStyle-HorizontalAlign="Right">
                                                                            <ItemStyle Width="" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Balance_Leaves" HeaderText="Balance_Leaves" ItemStyle-HorizontalAlign="Right">
                                                                            <ItemStyle Width="" />
                                                                        </asp:BoundField>--%>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="14%" />
                                                            <ItemStyle Width="50%" />
                                                        </asp:TemplateField>

                                                       <%-- <asp:TemplateField HeaderText="No Of Leaves" SortExpression="No_Of_Leaves">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNoOfLeaves" runat="server" Text='<%# Eval("Noof_Leavs")%>' Style="text-align: center;"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Leave Name" SortExpression="Leave_Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLeaveName" runat="server" Text='<%# Eval("Leave_Name")%>' Style="text-align: center;"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Leave Type" SortExpression="Leave_Type">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLeaveType" runat="server" Text='<%# Eval("Short_Name")%>' Style="text-align: center;"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Used Leaves" SortExpression="Used_Leaves">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCLUSed" runat="server" Text='<%# Eval("Used_Leaves")%>' Style="text-align: center;"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Balance Leaves" SortExpression="Balance_Leaves">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCLBalance" runat="server" Text='<%# Eval("Balance_Leaves")%>' Style="text-align: center;"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                    </Columns>
                                                </asp:GridView>

                    </div>
        </div>
                            </div>
                        </div>
                    </div>
             </div>
         
    </div>
</asp:Content>
