﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BAL;
using DAL;
using System.Data.Linq.SqlClient;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using System.IO;

namespace HRMS.Reports
{
    public partial class Saleryreport : System.Web.UI.Page
    {
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();
        Leave_AllotmentBLL objLeaveAllotmentBLL = new Leave_AllotmentBLL();
        EmployeeOfficialInfoBAL objeoinfo = new EmployeeOfficialInfoBAL();
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();
        DefineLeavesBAL objSCM_LeaveBAL = new DefineLeavesBAL();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Dbconnection"].ConnectionString);
        ERP_DatabaseDataContext objdal = new ERP_DatabaseDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind_Grid();
                txtEmpCode.Focus();
                Emp_NameBinding();
                setinitialrow();
               // setinitialrow1();
            }
            
        }
        public void Bind_Grid1()
        {

            try
            {
                string Cmpy = (string)Session["ClientRegistrationNo"];
                string Brnach = (string)Session["BranchID"];
                string UserType = (string)Session["User_Type"];
                string UserName = (string)Session["User_Name"];
                string Cration_Company = (string)Session["CreationCompany"]; //"000001";

                if (Session["User_Type"].ToString() == "CCF")
                {

                    string EMP_Code = (from EM in ObjDAL.HRMS_Employee_Master_PersonalDetails where EM.User_Name == UserName select EM.Emp_Code).FirstOrDefault();
                    var objHeaderInfo = (from header in ObjDAL.HRMS_Employee_Official_Info_Head_Details
                                         where header.Creation_Company == Cmpy && header.Emp_Code == EMP_Code
                                         select header).ToList();

                    //var objleaveinfo = (from li in ObjDAL.HRMS_Leave_Allotments
                    //                    where li.Creation_Company == Cmpy && li.Emp_Code == EMP_Code
                    //                    select li).ToList();

                    //var objmoinfo = (from mo in ObjDAL.HRMS_Employee_Master_Official_Informations
                    //                 where mo.Creation_Company == Cmpy && mo.Emp_Code == EMP_Code
                    //                 select mo).ToList();


                    //// List<Administrator_UserRegistration> ObjuserInfo = objSCM_userRegistration.Get_userRegDetails(Session["ClientRegistrationNo"].ToString());
                    if (objHeaderInfo.Count > 0)
                    {

                        grdpayroll.DataSource = objHeaderInfo;

                        //grdpayroll.DataSource = objHeaderInfo;
                        grdpayroll.DataBind();
                        //grdpayroll.DataSource = objmoinfo;
                        //grdpayroll.DataBind();
                    }
                    else
                    {
                        // // // Grid view using setting initial rows displaying Empty Header Displying
                        setinitialrow();
                    }
                }
            }


            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }
        }
        protected void txtEmpCode_TextChanged(object sender, EventArgs e)
        {
            string CreationCompany = Session["ClientRegistrationNo"].ToString();
            string Emp_Code = (txtEmpCode.Text.ToLower().Trim() == "") ? "" : txtEmpCode.Text.ToLower().Trim();
            var objEmPCodeDUP = (from itmes in ObjDAL.HRMS_Employee_Master_PersonalDetails
                                 where itmes.Creation_Company == CreationCompany && itmes.Emp_Code.ToLower().Trim() == Emp_Code
                                 select new
                                 {
                                     Emp_name = itmes.Emp_First_Name + " " + itmes.Emp_Last_Name + " " + itmes.Emp_Middle_Name,
                                     Emp_Code = itmes.Emp_Code
                                 }).ToList();
            if (objEmPCodeDUP.Count > 0)
            {

                ddlEmpName.SelectedItem.Text = objEmPCodeDUP[0].Emp_name;

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Emp Code is Not Valid');", true);
                txtEmpCode.Text = "";
                txtEmpCode.Focus();
                ddlEmpName.SelectedValue = "0";


            }
        }
        protected void ddlEmpName_SelectedIndexChanged(object sender, EventArgs e)
        {
            string CreationCompany = (string)Session["ClientRegistrationNo"].ToString();
            txtEmpCode.Text = ddlEmpName.SelectedValue;

        }
        public void Emp_NameBinding()
        {
            string CreationCompany = Session["ClientRegistrationNo"].ToString();

            var objEmPCodeDUP = (from itmes in ObjDAL.HRMS_Employee_Master_PersonalDetails
                                 where itmes.Creation_Company == CreationCompany
                                 select new
                                 {
                                     Emp_name = itmes.Emp_Last_Name + " " + itmes.Emp_First_Name + " " + itmes.Emp_Middle_Name,
                                     Emp_Code = itmes.Emp_Code
                                 }).ToList();
            if (objEmPCodeDUP.Count > 0)
            {
                ddlEmpName.DataSource = objEmPCodeDUP;
                ddlEmpName.DataTextField = "Emp_name";
                ddlEmpName.DataValueField = "Emp_Code";
                ddlEmpName.DataBind();
                ddlEmpName.Items.Insert(0, new ListItem("--Select--", "0"));
            }

        }
        public void setinitialrow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("Sno", typeof(string)));
            dt.Columns.Add(new DataColumn("Emp_Code", typeof(string)));
            dt.Columns.Add(new DataColumn("Emp_Name", typeof(string)));
            dt.Columns.Add(new DataColumn("Disgnation", typeof(string)));
            dt.Columns.Add(new DataColumn("Month", typeof(string)));
            dt.Columns.Add(new DataColumn("Year", typeof(string)));
            dt.Columns.Add(new DataColumn("Present Days", typeof(string)));
            dt.Columns.Add(new DataColumn("CL", typeof(string)));
            dt.Columns.Add(new DataColumn("EL", typeof(string)));
            dt.Columns.Add(new DataColumn("ML", typeof(string)));
            dt.Columns.Add(new DataColumn("PTL", typeof(string)));
            dt.Columns.Add(new DataColumn("HPL", typeof(string)));
            dt.Columns.Add(new DataColumn("EOL", typeof(string)));
            dt.Columns.Add(new DataColumn("SL", typeof(string)));
            dt.Columns.Add(new DataColumn("MTL", typeof(string)));
            dt.Columns.Add(new DataColumn("PH", typeof(string)));
            dt.Columns.Add(new DataColumn("CCL", typeof(string)));
            dt.Columns.Add(new DataColumn("Basic_Amount", typeof(string)));
            dt.Columns.Add(new DataColumn("Gross_Amount", typeof(string)));
            dt.Columns.Add(new DataColumn("DA", typeof(string)));
            dt.Columns.Add(new DataColumn("HRA", typeof(string)));
            dt.Columns.Add(new DataColumn("CCA", typeof(string)));
            dt.Columns.Add(new DataColumn("OA", typeof(string)));
            dt.Columns.Add(new DataColumn("PF", typeof(string)));
            dt.Columns.Add(new DataColumn("EPF", typeof(string)));
            dt.Columns.Add(new DataColumn("ESI", typeof(string)));
            dt.Columns.Add(new DataColumn("EESI", typeof(string)));
            dt.Columns.Add(new DataColumn("PTAX", typeof(string)));
            dt.Columns.Add(new DataColumn("TDS", typeof(string)));
            dt.Columns.Add(new DataColumn("APGLI", typeof(string)));
            dt.Columns.Add(new DataColumn("GIS", typeof(string)));
            dt.Columns.Add(new DataColumn("HBA", typeof(string)));
            dt.Columns.Add(new DataColumn("MCA", typeof(string)));
            dt.Columns.Add(new DataColumn("BA", typeof(string)));

            dr = dt.NewRow();
            dr["Sno"] = string.Empty;
            dr["Emp_Code"] = string.Empty;
            dr["Emp_Name"] = string.Empty;
            dr["Disgnation"] = string.Empty;
            dr["Month"] = string.Empty;
            dr["Year"] = string.Empty;
            dr["Present Days"] = string.Empty;
            dr["CL"] = string.Empty;
            dr["EL"] = string.Empty;
            dr["ML"] = string.Empty;
            dr["PTL"] = string.Empty;
            dr["HPL"] = string.Empty;
            dr["EOL"] = string.Empty;
            dr["SL"] = string.Empty;
            dr["MTL"] = string.Empty;
            dr["PH"] = string.Empty;
            dr["CCL"] = string.Empty;
            dr["Basic_Amount"] = string.Empty;
            dr["Gross_Amount"] = string.Empty;
            dr["DA"] = string.Empty;
            dr["HRA"] = string.Empty;
            dr["CCA"] = string.Empty;
            dr["OA"] = string.Empty;
            dr["PF"] = string.Empty;
            dr["EPF"] = string.Empty;
            dr["ESI"] = string.Empty;
            dr["EESI"] = string.Empty;
            dr["PTAX"] = string.Empty;
            dr["TDS"] = string.Empty;
            dr["APGLI"] = string.Empty;
            dr["GIS"] = string.Empty;
            dr["HBA"] = string.Empty;
            dr["MCA"] = string.Empty;
            dr["BA"] = string.Empty;

            dt.Rows.Add(dr);
            ViewState["CurrentTable"] = dt;
            grdpayroll.DataSource = dt;
            grdpayroll.DataBind();




        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            Bind_Grid1();
        }
        public void Bind_Grid()
        {


            string Cmpy = (string)Session["ClientRegistrationNo"];
            //string Brnach = (string)Session["BranchID"];
            //string UserType = (string)Session["User_Type"];
            //string UserName = (string)Session["User_Name"];
            //string Cration_Company = (string)Session["CreationCompany"]; //"000001";
            ////if (UserType == "User")
            //{


            // string EMP_Code = (from EM in objDAL.HRMS_Employee_Master_PersonalDetails where EM.User_Name == UserName select EM.Emp_Code).FirstOrDefault();
            var objHeaderInfo = (from header in objDAL.HRMS_Employee_Official_Info_Head_Details
                                 where header.Creation_Company == Cmpy
                                 orderby header.Emp_Code ascending
                                 select header).ToList();
            //// List<Administrator_UserRegistration> ObjuserInfo = objSCM_userRegistration.Get_userRegDetails(Session["ClientRegistrationNo"].ToString());
            if (objHeaderInfo.Count > 0)
            {

                grdleaves.DataSource = objHeaderInfo;
                grdleaves.DataBind();
            }
            else
            {
                // // // Grid view using setting initial rows displaying Empty Header Displying
                setinitialrow();
            }
        }


        //public void setinitialrow1()
        //{
        //    DataTable dt = new DataTable();
        //    DataRow dr = null;
        //    dt.Columns.Add(new DataColumn("Sno", typeof(string)));
           
        //    dt.Columns.Add(new DataColumn("Emp_Code", typeof(string)));
          
        //    dt.Columns.Add(new DataColumn("Salery", typeof(string)));

        //    dt.Rows.Add(dr);
        //    ViewState["CurrentTable"] = dt;
        //    grdleaves.DataSource = dt;
        //    grdleaves.DataBind();

        //}
    }
}