﻿var payslippage = (function () {

    var bindevents = function () {
        $('#btnsearch').on('click', function () {
            var templates = loadTemplates('Kendotemp', ['PAYSLIP']);
            var department = $("#ContentPlaceHolder1_ddldepartnemt").find('option:selected').val();
            var month = $("#ContentPlaceHolder1_ddlmonth").find('option:selected').val();
            $(function () {
                $.ajax({
                    type: "POST",
                    url: "DepartmentWisePayslip.aspx/AnalysisReportData",
                    data: JSON.stringify({
                        depid: department,
                        month: month
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (obj) {
                        var lds = obj;
                        var Getdata = lds;
                        BindData(Getdata);
                    }
                });
                function BindData(Getdata) {
                    var template = kendo.template(templates.PAYSLIP); 
                    var data = Getdata;
                    var result = template(data)
                    $("#demo").html(result)
                };
            });


        });
    };
    var init = function () {
        context = this;
        bindevents();

    };
    return {
        init: init
    }

}());
$(document).ready(function () {
    payslippage.init();
});