﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Administrator
{
    public partial class Admin : System.Web.UI.MasterPage
    {
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            string Cmpy = (string)Session["ClientRegistrationNo"];
            string Brnach = (string)Session["BranchID"];
            string UserType = (string)Session["User_Type"];
            string UserName = (string)Session["User_Name"];
            string Cration_Company = (string)Session["CreationCompany"]; //"000001";
        }

        protected void btnlogout_Click(object sender, EventArgs e)
        {
            try
            {
                Session.Clear();
                Response.Redirect("../Default.aspx");
            }
            catch (Exception ex)
            {
            }
        }
    }
}