﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

#region Private Using
using DAL;
using BAL;
using System.Data.SqlClient;
using System.Configuration;
#endregion
namespace HRMS.Administrator
{
    public partial class AddCompanyInfomation : System.Web.UI.Page
    {
        string objConfig = ConfigurationManager.ConnectionStrings["Dbconnection"].ToString();

        // // // Object Creation For Database LinqView
        ERP_DatabaseDataContext objDb = new ERP_DatabaseDataContext();

        // // // Bank Details Master
        BankMasterBAL objBankBAL = new BankMasterBAL();
        // // /// Object Creation For Sales Team BAL Class
        Administrator_CompanyInfoBAL ObjAdmComp = new Administrator_CompanyInfoBAL();
        // // /// Object Creation For Sales All Methods Class
        HRMS_ALL_Get_Methods ObjAllMethods = new HRMS_ALL_Get_Methods();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_Type"].ToString() == "Employees")
            {
                Response.Redirect("~/Dashboard/Dashboard.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    if (Session["ClientRegistrationNo"] == null)
                    {
                        Response.Redirect("~/Default.aspx");
                    }

                    txtCompanyname.Focus();

                    //Set initial Bank Detils Grid Display
                    SetInitialRow();

                    btnUpdate.Visible = false;

                    // // // This Is Related To Getting The Data 
                    // string id = Request.QueryString["id"];

                    if ((string)Session["EditCompany"] != null)
                    {
                        //  Request.QueryString.Clear();
                        string strEdit1 = (string)Session["EditCompany"];
                        txtCompanyid.Text = (string)Session["EditCompany"];
                        Edit_SalesDetails(strEdit1, Session["ClientRegistrationNo"].ToString());
                        Session["EditCompany"] = null;
                    }
                }
            }
        }

        #region Methods
        // // // // Binding The Company Details Based On Company Id
        public void Edit_SalesDetails(string ClientRegNO, string CreationCompany)
        {
            //List<Administrator_CompanyInfo> objCompanyinfo = ObjAllMethods.Get_AdminiStrator_Companyinformation(ClientRegNO, CreationCompany);
            List<Administrator_CompanyInfo> objCompanyinfo =
                (from c in objDb.Administrator_CompanyInfos
                 where c.ClientRegistrationNo == ClientRegNO && c.Creation_Company == CreationCompany
                 select c).ToList<Administrator_CompanyInfo>();

            if (objCompanyinfo.Count > 0)
            {

                txtPFCode.Text = objCompanyinfo[0].PAN_No;
                ddlPFRegion.SelectedValue = objCompanyinfo[0].PF_Region;
                ddlPFOffice.SelectedValue = objCompanyinfo[0].PF_Office;
                txtESIRegCode.Text = objCompanyinfo[0].ESI_Reg_Code;
                txtESIBranch.Text = objCompanyinfo[0].ESI_Branch;
                txtPTRegNo.Text = objCompanyinfo[0].PT_Reg_No;
                txtPANNo.Text = objCompanyinfo[0].PAN_No;
                ddlWeekOFF.SelectedValue = objCompanyinfo[0].Weak_Off;
                ddlstatus.SelectedValue = objCompanyinfo[0].Status;
                txtCompanyid.Text = objCompanyinfo[0].ClientRegistrationNo.ToString();
                txtCompanyname.Text = objCompanyinfo[0].Company_Name.ToString();
                txtAddress1.Text = objCompanyinfo[0].Address1.ToString();
                txtAddress2.Text = objCompanyinfo[0].Address2.ToString();
                txtState.Text = objCompanyinfo[0].State.ToString();
                txtCity.Text = objCompanyinfo[0].City.ToString();
                txtPinCode.Text = objCompanyinfo[0].Pincode.ToString();
                txtPhone.Text = objCompanyinfo[0].Phone_No.ToString();
                txtFaxNO.Text = objCompanyinfo[0].Fax_No.ToString();
                txtEmail.Text = objCompanyinfo[0].E_Mail.ToString();
                txtWeburl.Text = objCompanyinfo[0].Web_URL.ToString();
                txtTINNO.Text = objCompanyinfo[0].TIN_No.ToString();
                txtECCNo.Text = objCompanyinfo[0].ECC_No.ToString();
                //txtcerange.Text = objCompanyinfo[0].CE_Range.ToString();
                //txtcedivision.Text = objCompanyinfo[0].CE_Division.ToString();
                //txtcommissionerate.Text = objCompanyinfo[0].CE_Commissionarate.ToString();
                //ddlstatus.SelectedValue = objCompanyinfo[0].Status.ToString();



                //if (objCompanyinfo[0].ClientRegistrationNo == "")
                //{
                //    txtCompanyid.Text = "";
                //}
                //else
                //{

                //}

                //if (objCompanyinfo[0].Company_Name == "")
                //{
                //    txtCompanyname.Text = "";
                //}
                //else
                //{

                //}

                ////// // Address One Null handling
                //if (objCompanyinfo[0].Address1 == "")
                //{
                //    txtAddress1.Text = "";
                //}
                //else
                //{

                //}


                ////// // Address Two Null handling
                //if (objCompanyinfo[0].Address2 == "")
                //{
                //    txtAddress2.Text = "";
                //}
                //else
                //{

                //}

                ////// // Address Two Null handling
                //if (objCompanyinfo[0].State == "")
                //{
                //    txtState.Text = "";
                //}
                //else
                //{

                //}

                ////// // City Null handling
                //if (objCompanyinfo[0].City == "")
                //{
                //    txtCity.Text = "";
                //}
                //else
                //{

                //}

                ////// // City Null handling
                //if (objCompanyinfo[0].Pincode == "")
                //{
                //    txtPinCode.Text = "";
                //}
                //else
                //{

                //}

                ////// // City Null handling
                //if (objCompanyinfo[0].Phone_No == "")
                //{
                //    txtPhone.Text = "";
                //}
                //else
                //{

                //}

                ////// // City Null handling
                //if (objCompanyinfo[0].Fax_No == "")
                //{
                //    txtFaxNO.Text = "";
                //}
                //else
                //{

                //}

                ////// // City Null handling
                //if (objCompanyinfo[0].E_Mail == "")
                //{
                //    txtEmail.Text = "";
                //}
                //else
                //{

                //}

                ////// // City Null handling
                //if (objCompanyinfo[0].Web_URL == "")
                //{
                //    txtWeburl.Text = "";
                //}
                //else
                //{

                //}

                ////// // City Null handling
                //if (objCompanyinfo[0].TIN_No == null)
                //{
                //    txtTINNO.Text = "";
                //}
                //else
                //{

                //}

                ////// // City Null handling
                //if (objCompanyinfo[0].ECC_No == "")
                //{
                //    txtECCNo.Text = "";
                //}
                //else
                //{
                //}

                ////// // City Null handling
                //if (objCompanyinfo[0].CE_Range == "")
                //{
                //    txtcerange.Text = "";
                //}
                //else
                //{

                //}

                ////// // City Null handling
                //if (objCompanyinfo[0].CE_Division == "")
                //{
                //    txtcedivision.Text = "";
                //}
                //else
                //{


                //}

                ////// // City Null handling
                //if (objCompanyinfo[0].CE_Commissionarate == "")
                //{
                //    txtcommissionerate.Text = "";
                //}
                //else
                //{

                //}

                ////// // City Null handling
                //if (objCompanyinfo[0].Status == "")
                //{
                //    ddlstatus.SelectedValue = "";
                //}
                //else
                //{


                ////// // City Null handling
                //if (objCompanyinfo[0].CST_No == "")
                //{
                //    txtcstno.Text = "";
                //}
                //else
                //{

                //    }
                //    txtcstno.Text = objCompanyinfo[0].CST_No.ToString();
                //}


                Sub_Child_Grid(ClientRegNO, CreationCompany);
                btnSave.Visible = false;
                btnUpdate.Visible = true;
            }
            else
            {
                ClearAllFields();
            }
        }

        //Purchase Order Item Details Grid Binding
        public void Sub_Child_Grid(string ClientRegNO, string CreationCompany)
        {
            //  var Purchase_PurchaseOrder = (from PO_MASTER in objDb.Sp_Select_Purchase_PurchaseOrder_Master_Child_Terms_Po_No((string)Session["ClientRegistrationNo"], PO_No, 2) select PO_MASTER).ToList();
            var Purchage_Order_Child = ObjAllMethods.Get_AdminiStrator_Company_Bankinformation(txtCompanyid.Text, (string)Session["ClientRegistrationNo"]);
            //var Purchage_Order_Child =
            //    (from c in objDb.Administrator_CompanyInfo_BankDetails
            //     where c.ClientRegistrationNo == ClientRegNO
            //     select c).ToList();
            List<Administrator_CompanyInfo_BankDetail> Purchage_OrderChild = (List<Administrator_CompanyInfo_BankDetail>)Purchage_Order_Child;

            if (Purchage_OrderChild.Count > 0)
            {
                grvBankDetail.DataSource = Purchage_OrderChild;
                grvBankDetail.DataBind();
            }
            else
            {
                SetInitialRow();
            }

            // // // Footer Value And Total Qty Calculation


        }

        /// <summary>
        /// 
        /// </summary>
        public void SetInitialRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            // dt.Columns.Add(new DataColumn("Id", typeof(int)));
            //  dt.Columns.Add(new DataColumn("Bank_ID", typeof(string)));
            dt.Columns.Add(new DataColumn("Bank_Name", typeof(string)));
            dt.Columns.Add(new DataColumn("Account_Number", typeof(string)));
            dt.Columns.Add(new DataColumn("Branch", typeof(string)));

            //  dt.Columns.Add(new DataColumn("PAN", typeof(string)));

            dr = dt.NewRow();
            //    dr["Bank_ID"] = string.Empty;
            dr["Bank_Name"] = string.Empty;
            dr["Account_Number"] = string.Empty;
            dr["Branch"] = string.Empty;

            // dr["PAN"] = string.Empty;


            dt.Rows.Add(dr);
            ViewState["CurrentTable"] = dt;
            grvBankDetail.DataSource = dt;
            grvBankDetail.DataBind();
        }
        #endregion

        #region Events
        /// <summary>
        /// The Following Event Is Used For Save The Data To Company Information Master
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        int Parameter;
        protected void btnSave_Click(object sender, EventArgs e)
        {
            Parameter = 1;
            Saving();
            #region Old Code
            // // // Assign The Data To Domain Objects
            //ObjAdmComp.ClientRegistrationNo = txtCompanyid.Text;
            //ObjAdmComp.Company_Name = (txtCompanyname.Text == "") ? "" : txtCompanyname.Text;
            //ObjAdmComp.Address1 = (txtAddress1.Text == "") ? "" : txtAddress1.Text;
            //ObjAdmComp.Address2 = (txtAddress2.Text == "") ? "" : txtAddress2.Text;
            //ObjAdmComp.State = (txtState.Text == "") ? "" : txtState.Text;
            //ObjAdmComp.City = (txtCity.Text == "") ? "" : txtCity.Text;
            //ObjAdmComp.Pincode = (txtPinCode.Text == "") ? "" : txtPinCode.Text;
            //ObjAdmComp.Phone_No = (txtPhone.Text == "") ? "" : txtPhone.Text;
            //ObjAdmComp.E_Mail = (txtEmail.Text == "") ? "" : txtEmail.Text;
            //ObjAdmComp.Web_URL = (txtWeburl.Text == "") ? "" : txtWeburl.Text;
            //ObjAdmComp.ECC_No = (txtECCNo.Text == "") ? "" : txtECCNo.Text;
            //ObjAdmComp.CE_Range = (txtcerange.Text == "") ? "" : txtcerange.Text;
            //ObjAdmComp.CE_Division = (txtcedivision.Text == "") ? "" : txtcedivision.Text;
            //ObjAdmComp.Fax_No = (txtFaxNO.Text == "") ? "" : txtFaxNO.Text;
            //ObjAdmComp.CE_Commissionarate = (txtcommissionerate.Text == "") ? "" : txtcommissionerate.Text;
            //ObjAdmComp.CST_No = (txtcstno.Text == "") ? "" : txtcstno.Text;
            //ObjAdmComp.PF_Code = (txtPFCode.Text == "") ? "" : txtPFCode.Text;
            //ObjAdmComp.PF_Office = (ddlPFOffice.SelectedValue == "") ? "" : ddlPFOffice.SelectedValue;
            //ObjAdmComp.PF_Region = (ddlPFRegion.SelectedValue == "") ? "" : ddlPFRegion.SelectedValue;
            //ObjAdmComp.ESI_Reg_Code = (txtESIRegCode.Text == "") ? "" : txtESIRegCode.Text;
            //ObjAdmComp.ESI_Branch = (txtESIBranch.Text == "") ? "" : txtESIBranch.Text;
            //ObjAdmComp.PT_Reg_No = (txtPTRegNo.Text == "") ? "" : txtPTRegNo.Text;
            //ObjAdmComp.PAN_No = (txtPANNo.Text == "") ? "" : txtPANNo.Text;
            //ObjAdmComp.Weak_Off = (ddlWeekOFF.SelectedItem.Text == "") ? "" : ddlWeekOFF.SelectedItem.Text;
            //ObjAdmComp.Status = ddlstatus.SelectedValue;
            //ObjAdmComp.Creation_Company = Session["ClientRegistrationNo"].ToString();
            //ObjAdmComp.Created_By = Session["User_Name"].ToString();
            //ObjAdmComp.Created_Date = System.DateTime.Now;
            //ObjAdmComp.Modified_By = Session["User_Name"].ToString();
            //ObjAdmComp.Modified_Date = System.DateTime.Now;
            //ObjAdmComp.Parameter = 1; // // // This Can Be Used For Operation Related 1=Insert

            //if (ObjAdmComp.Insert_Administrator_Company_Info() != 0)
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Saved Sucessfully');", true);
            //    //ClearAllFields();
            //}
            //else
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
            //}

            #endregion
        }

        /// <summary>
        /// Update Event For Company Infomation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Parameter = 2;
            Update();
        }

        /// <summary>
        /// Update Data
        /// </summary>
        public void Update()
        {
            SqlTransaction transaction = null;
            SqlConnection objSqlConnection = null;
            bool debitResult = false;
            bool creditResult = false;
            bool debitResult1 = false;
            int parameter;
            try
            {
                objSqlConnection = new SqlConnection(objConfig);
                objSqlConnection.Open();

                int a1 = (int)grvBankDetail.Rows.Count;
                //DropDownList box11 = (DropDownList)grvBankDetail.Rows[a1].FindControl("ddlBankName") as DropDownList;
                //TextBox box12 = (TextBox)grvBankDetail.Rows[a1].FindControl("txtAccountNumber") as TextBox;
                //TextBox box13 = (TextBox)grvBankDetail.Rows[a1].FindControl("txtBranch") as TextBox;
                if (grvBankDetail.Rows.Count > 0)
                {

                    // // // Assign The Data To Domain Objects
                    ObjAdmComp.ClientRegistrationNo = txtCompanyid.Text;
                    ObjAdmComp.Company_Name = (txtCompanyname.Text == "") ? "" : txtCompanyname.Text;
                    ObjAdmComp.Address1 = (txtAddress1.Text == "") ? "" : txtAddress1.Text;
                    ObjAdmComp.Address2 = (txtAddress2.Text == "") ? "" : txtAddress2.Text;
                    ObjAdmComp.State = (txtState.Text == "") ? "" : txtState.Text;
                    ObjAdmComp.City = (txtCity.Text == "") ? "" : txtCity.Text;
                    ObjAdmComp.Pincode = (txtPinCode.Text == "") ? "" : txtPinCode.Text;
                    ObjAdmComp.Phone_No = (txtPhone.Text == "") ? "" : txtPhone.Text;
                    ObjAdmComp.E_Mail = (txtEmail.Text == "") ? "" : txtEmail.Text;
                    ObjAdmComp.Web_URL = (txtWeburl.Text == "") ? "" : txtWeburl.Text;
                    ObjAdmComp.ECC_No = (txtECCNo.Text == "") ? "" : txtECCNo.Text;
                    ObjAdmComp.TIN_No = (txtTINNO.Text == "") ? "" : txtTINNO.Text;
                    ObjAdmComp.CE_Range = (txtcerange.Text == "") ? "" : txtcerange.Text;
                    ObjAdmComp.CE_Division = (txtcedivision.Text == "") ? "" : txtcedivision.Text;
                    ObjAdmComp.Fax_No = (txtFaxNO.Text == "") ? "" : txtFaxNO.Text;
                    ObjAdmComp.CE_Commissionarate = (txtcommissionerate.Text == "") ? "" : txtcommissionerate.Text;
                    ObjAdmComp.CST_No = (txtcstno.Text == "") ? "" : txtcstno.Text;
                    ObjAdmComp.PF_Code = (txtPFCode.Text == "") ? "" : txtPFCode.Text;
                    ObjAdmComp.PF_Office = (ddlPFOffice.SelectedValue == "") ? "" : ddlPFOffice.SelectedValue;
                    ObjAdmComp.PF_Region = (ddlPFRegion.SelectedValue == "") ? "" : ddlPFRegion.SelectedValue;
                    ObjAdmComp.ESI_Reg_Code = (txtESIRegCode.Text == "") ? "" : txtESIRegCode.Text;
                    ObjAdmComp.ESI_Branch = (txtESIBranch.Text == "") ? "" : txtESIBranch.Text;
                    ObjAdmComp.PT_Reg_No = (txtPTRegNo.Text == "") ? "" : txtPTRegNo.Text;
                    ObjAdmComp.PAN_No = (txtPANNo.Text == "") ? "" : txtPANNo.Text;
                    ObjAdmComp.Weak_Off = (ddlWeekOFF.SelectedItem.Text == "") ? "" : ddlWeekOFF.SelectedItem.Text;
                    ObjAdmComp.Status = ddlstatus.SelectedValue;
                    ObjAdmComp.Creation_Company = Session["ClientRegistrationNo"].ToString();
                    ObjAdmComp.Created_By = Session["User_Name"].ToString();
                    ObjAdmComp.Created_Date = System.DateTime.Now;
                    ObjAdmComp.Modified_By = Session["User_Name"].ToString();
                    ObjAdmComp.Modified_Date = System.DateTime.Now;

                    ObjAdmComp.Parameter = Parameter; // // // This Can Be Used For Operation Related 1=Insert   
                    transaction = objSqlConnection.BeginTransaction();

                    using (SqlCommand objSqlCommand = objSqlConnection.CreateCommand())
                    {
                        objSqlCommand.CommandType = CommandType.StoredProcedure;
                        objSqlCommand.CommandText = "Sp_Insert_Select_Update_Administrator_CompanyInfo";
                        objSqlCommand.Parameters.AddWithValue("@ClientRegistrationNo", ObjAdmComp.ClientRegistrationNo);
                        objSqlCommand.Parameters.AddWithValue("@Company_Name", ObjAdmComp.Company_Name);
                        objSqlCommand.Parameters.AddWithValue("@Address1", ObjAdmComp.Address1);
                        objSqlCommand.Parameters.AddWithValue("@Address2", ObjAdmComp.Address2);
                        objSqlCommand.Parameters.AddWithValue("@State", ObjAdmComp.State);
                        objSqlCommand.Parameters.AddWithValue("@City", ObjAdmComp.City);
                        objSqlCommand.Parameters.AddWithValue("@Pincode", ObjAdmComp.Pincode);
                        objSqlCommand.Parameters.AddWithValue("@Phone_No", ObjAdmComp.Phone_No);
                        objSqlCommand.Parameters.AddWithValue("@E_Mail", ObjAdmComp.E_Mail);
                        objSqlCommand.Parameters.AddWithValue("@Web_URL", ObjAdmComp.Web_URL);//10
                        objSqlCommand.Parameters.AddWithValue("@ECC_No", ObjAdmComp.ECC_No);
                        objSqlCommand.Parameters.AddWithValue("@TIN_No", ObjAdmComp.TIN_No);
                        objSqlCommand.Parameters.AddWithValue("@CE_Range", ObjAdmComp.CE_Range);
                        objSqlCommand.Parameters.AddWithValue("@CE_Division", ObjAdmComp.CE_Division);
                        objSqlCommand.Parameters.AddWithValue("@Fax_No", ObjAdmComp.Fax_No);
                        objSqlCommand.Parameters.AddWithValue("@CE_Commissionarate", ObjAdmComp.CE_Commissionarate);
                        objSqlCommand.Parameters.AddWithValue("@CST_No", ObjAdmComp.CST_No);
                        objSqlCommand.Parameters.AddWithValue("@PF_Code", ObjAdmComp.PF_Code);
                        objSqlCommand.Parameters.AddWithValue("@PF_Region", ObjAdmComp.PF_Region);
                        objSqlCommand.Parameters.AddWithValue("@PF_Office", ObjAdmComp.PF_Office);
                        objSqlCommand.Parameters.AddWithValue("@ESI_Reg_Code", ObjAdmComp.ESI_Reg_Code);//10
                        objSqlCommand.Parameters.AddWithValue("@ESI_Branch", ObjAdmComp.ESI_Branch);
                        objSqlCommand.Parameters.AddWithValue("@PT_Reg_No", ObjAdmComp.PT_Reg_No);
                        objSqlCommand.Parameters.AddWithValue("@PAN_No", ObjAdmComp.PAN_No);
                        objSqlCommand.Parameters.AddWithValue("@Weak_Off", ObjAdmComp.Weak_Off);
                        objSqlCommand.Parameters.AddWithValue("@Status", ObjAdmComp.Status);
                        objSqlCommand.Parameters.AddWithValue("@Creation_Company", ObjAdmComp.Creation_Company);
                        objSqlCommand.Parameters.AddWithValue("@Created_By", ObjAdmComp.Created_By);
                        objSqlCommand.Parameters.AddWithValue("@Created_Date", ObjAdmComp.Created_Date);
                        objSqlCommand.Parameters.AddWithValue("@Modified_By", ObjAdmComp.Modified_By);
                        objSqlCommand.Parameters.AddWithValue("@Modified_Date", ObjAdmComp.Modified_Date);//10
                        objSqlCommand.Parameters.AddWithValue("@Parameter", ObjAdmComp.Parameter);

                        objSqlCommand.Transaction = transaction;
                        if (objSqlCommand.ExecuteNonQuery() != 0)
                        { debitResult = true; }
                    }
                    string ClientRegistrationNo, Creation_Company;

                    ClientRegistrationNo = txtCompanyid.Text;
                    Creation_Company = Session["ClientRegistrationNo"].ToString();

                    using (SqlCommand objSqlCommand = objSqlConnection.CreateCommand())
                    {
                        objSqlCommand.CommandType = CommandType.Text;
                        objSqlCommand.CommandText = "DELETE FROM Administrator_CompanyInfo_BankDetails where ClientRegistrationNo='" + ClientRegistrationNo + "'and Creation_Company='" + Creation_Company + "'";

                        objSqlCommand.Transaction = transaction;
                        if (objSqlCommand.ExecuteNonQuery() != 0 || objSqlCommand.ExecuteNonQuery() == 0)
                        { debitResult1 = true; }
                    }

                    if (ObjAdmComp.Parameter == 2)
                    {
                        parameter = 4;
                    }
                    if (grvBankDetail.Rows.Count != 0)
                    {
                        foreach (GridViewRow objrow in grvBankDetail.Rows)
                        {
                            using (SqlCommand objSqlCommand = objSqlConnection.CreateCommand())
                            {
                                // Only look in data rows, ignore header and footer rows
                                if (objrow.RowType == DataControlRowType.DataRow)
                                {
                                    DropDownList ddl1 = objrow.FindControl("ddlBankName") as DropDownList;
                                    TextBox box1 = objrow.FindControl("txtAccountNumber") as TextBox;
                                    TextBox box2 = objrow.FindControl("txtBranch") as TextBox;

                                    if (box2.Text != "")
                                    {
                                        //   ObjAdmComp.Voucher_No = Voucher_ID;
                                        ObjAdmComp.Bank_Name = ddl1.SelectedItem.Value.ToString();
                                        ObjAdmComp.Account_Number = box1.Text;
                                        ObjAdmComp.Branch = box2.Text;
                                        // ObjAdmComp.Parameter = Parameter;
                                        ObjAdmComp.Creation_Company = (string)Session["ClientRegistrationNo"];
                                        ObjAdmComp.Created_By = (string)Session["User_Name"];
                                        ObjAdmComp.Created_Date = Convert.ToDateTime(DateTime.Now.ToString());
                                        ObjAdmComp.Modified_By = (string)Session["User_Name"];
                                        ObjAdmComp.Modified_Date = Convert.ToDateTime(DateTime.Now.ToString());

                                        objSqlCommand.CommandType = CommandType.StoredProcedure;
                                        objSqlCommand.CommandText = "Sp_Insert_Select_Update_Administrator_CompanyInfo_BankDetails";
                                        objSqlCommand.Parameters.AddWithValue("@ClientRegistrationNo", ObjAdmComp.ClientRegistrationNo);
                                        objSqlCommand.Parameters.AddWithValue("@Bank_Name", ObjAdmComp.Bank_Name);
                                        objSqlCommand.Parameters.AddWithValue("@Account_Number", ObjAdmComp.Account_Number);
                                        objSqlCommand.Parameters.AddWithValue("@Branch", ObjAdmComp.Branch);
                                        objSqlCommand.Parameters.AddWithValue("@Creation_Company", ObjAdmComp.Creation_Company);
                                        objSqlCommand.Parameters.AddWithValue("@Created_By", ObjAdmComp.Created_By);
                                        objSqlCommand.Parameters.AddWithValue("@Created_Date", ObjAdmComp.Created_Date);
                                        objSqlCommand.Parameters.AddWithValue("@Modified_By", ObjAdmComp.Modified_By);
                                        objSqlCommand.Parameters.AddWithValue("@Modified_Date", ObjAdmComp.Modified_Date);
                                        objSqlCommand.Parameters.AddWithValue("@Parameter", 4);
                                        objSqlCommand.Transaction = transaction;
                                        if (objSqlCommand.ExecuteNonQuery() != 0)
                                        { creditResult = true; }
                                    }
                                }
                            }
                        }
                        //}

                        if (debitResult && creditResult && debitResult1)
                        {
                            transaction.Commit();

                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Saved Successfully');", true);

                            ClearAllFields();
                        }
                        else
                        {
                            transaction.Rollback();
                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Enter Bank Details');", true);
                    DropDownList box11 = (DropDownList)grvBankDetail.Rows[a1 - 1].FindControl("ddlBankName") as DropDownList;
                    box11.Focus();
                }
            }
            catch (Exception ex)
            {
                Session["Error"] = ex.Message.ToString();
            }

            finally
            {
            }

        }
        /// <summary>
        /// Save The Data To 
        /// </summary>
        /// <param name="sender"></param>
        private void Saving()
        {
            SqlTransaction transaction = null;
            SqlConnection objSqlConnection = null;
            bool debitResult = false;
            bool creditResult = false;
            try
            {
                objSqlConnection = new SqlConnection(objConfig);
                objSqlConnection.Open();

                int a1 = (int)grvBankDetail.Rows.Count;
                //DropDownList box11 = (DropDownList)grvBankDetail.Rows[a1].FindControl("ddlBankName") as DropDownList;
                //TextBox box12 = (TextBox)grvBankDetail.Rows[a1].FindControl("txtAccountNumber") as TextBox;
                //TextBox box13 = (TextBox)grvBankDetail.Rows[a1].FindControl("txtBranch") as TextBox;
                if (grvBankDetail.Rows.Count > 0)
                {

                    // // // Assign The Data To Domain Objects
                    ObjAdmComp.ClientRegistrationNo = txtCompanyid.Text;
                    ObjAdmComp.Company_Name = (txtCompanyname.Text == "") ? "" : txtCompanyname.Text;
                    ObjAdmComp.Address1 = (txtAddress1.Text == "") ? "" : txtAddress1.Text;
                    ObjAdmComp.Address2 = (txtAddress2.Text == "") ? "" : txtAddress2.Text;
                    ObjAdmComp.State = (txtState.Text == "") ? "" : txtState.Text;
                    ObjAdmComp.City = (txtCity.Text == "") ? "" : txtCity.Text;
                    ObjAdmComp.Pincode = (txtPinCode.Text == "") ? "" : txtPinCode.Text;
                    ObjAdmComp.Phone_No = (txtPhone.Text == "") ? "" : txtPhone.Text;
                    ObjAdmComp.E_Mail = (txtEmail.Text == "") ? "" : txtEmail.Text;
                    ObjAdmComp.Web_URL = (txtWeburl.Text == "") ? "" : txtWeburl.Text;
                    ObjAdmComp.ECC_No = (txtECCNo.Text == "") ? "" : txtECCNo.Text;
                    ObjAdmComp.TIN_No = (txtTINNO.Text == "") ? "" : txtTINNO.Text;
                    ObjAdmComp.CE_Range = (txtcerange.Text == "") ? "" : txtcerange.Text;
                    ObjAdmComp.CE_Division = (txtcedivision.Text == "") ? "" : txtcedivision.Text;
                    ObjAdmComp.Fax_No = (txtFaxNO.Text == "") ? "" : txtFaxNO.Text;
                    ObjAdmComp.CE_Commissionarate = (txtcommissionerate.Text == "") ? "" : txtcommissionerate.Text;
                    ObjAdmComp.CST_No = (txtcstno.Text == "") ? "" : txtcstno.Text;
                    ObjAdmComp.PF_Code = (txtPFCode.Text == "") ? "" : txtPFCode.Text;
                    ObjAdmComp.PF_Office = (ddlPFOffice.SelectedValue == "") ? "" : ddlPFOffice.SelectedValue;
                    ObjAdmComp.PF_Region = (ddlPFRegion.SelectedValue == "") ? "" : ddlPFRegion.SelectedValue;
                    ObjAdmComp.ESI_Reg_Code = (txtESIRegCode.Text == "") ? "" : txtESIRegCode.Text;
                    ObjAdmComp.ESI_Branch = (txtESIBranch.Text == "") ? "" : txtESIBranch.Text;
                    ObjAdmComp.PT_Reg_No = (txtPTRegNo.Text == "") ? "" : txtPTRegNo.Text;
                    ObjAdmComp.PAN_No = (txtPANNo.Text == "") ? "" : txtPANNo.Text;
                    ObjAdmComp.Weak_Off = (ddlWeekOFF.SelectedItem.Text == "") ? "" : ddlWeekOFF.SelectedItem.Text;
                    ObjAdmComp.Status = ddlstatus.SelectedValue;
                    ObjAdmComp.Creation_Company = Session["ClientRegistrationNo"].ToString();
                    ObjAdmComp.Created_By = Session["User_Name"].ToString();
                    ObjAdmComp.Created_Date = System.DateTime.Now;
                    ObjAdmComp.Modified_By = Session["User_Name"].ToString();
                    ObjAdmComp.Modified_Date = System.DateTime.Now;
                    ObjAdmComp.Parameter = Parameter; // // // This Can Be Used For Operation Related 1=Insert               

                    transaction = objSqlConnection.BeginTransaction();
                    using (SqlCommand objSqlCommand = objSqlConnection.CreateCommand())
                    {
                        objSqlCommand.CommandType = CommandType.StoredProcedure;
                        objSqlCommand.CommandText = "Sp_Insert_Select_Update_Administrator_CompanyInfo";
                        objSqlCommand.Parameters.AddWithValue("@ClientRegistrationNo", ObjAdmComp.ClientRegistrationNo);
                        objSqlCommand.Parameters.AddWithValue("@Company_Name", ObjAdmComp.Company_Name);
                        objSqlCommand.Parameters.AddWithValue("@Address1", ObjAdmComp.Address1);
                        objSqlCommand.Parameters.AddWithValue("@Address2", ObjAdmComp.Address2);
                        objSqlCommand.Parameters.AddWithValue("@State", ObjAdmComp.State);
                        objSqlCommand.Parameters.AddWithValue("@City", ObjAdmComp.City);
                        objSqlCommand.Parameters.AddWithValue("@Pincode", ObjAdmComp.Pincode);
                        objSqlCommand.Parameters.AddWithValue("@Phone_No", ObjAdmComp.Phone_No);
                        objSqlCommand.Parameters.AddWithValue("@E_Mail", ObjAdmComp.E_Mail);
                        objSqlCommand.Parameters.AddWithValue("@Web_URL", ObjAdmComp.Web_URL);//10
                        objSqlCommand.Parameters.AddWithValue("@ECC_No", ObjAdmComp.ECC_No);
                        objSqlCommand.Parameters.AddWithValue("@TIN_No", ObjAdmComp.TIN_No);
                        objSqlCommand.Parameters.AddWithValue("@CE_Range", ObjAdmComp.CE_Range);
                        objSqlCommand.Parameters.AddWithValue("@CE_Division", ObjAdmComp.CE_Division);
                        objSqlCommand.Parameters.AddWithValue("@Fax_No", ObjAdmComp.Fax_No);
                        objSqlCommand.Parameters.AddWithValue("@CE_Commissionarate", ObjAdmComp.CE_Commissionarate);
                        objSqlCommand.Parameters.AddWithValue("@CST_No", ObjAdmComp.CST_No);
                        objSqlCommand.Parameters.AddWithValue("@PF_Code", ObjAdmComp.PF_Code);
                        objSqlCommand.Parameters.AddWithValue("@PF_Region", ObjAdmComp.PF_Region);
                        objSqlCommand.Parameters.AddWithValue("@PF_Office", ObjAdmComp.PF_Office);
                        objSqlCommand.Parameters.AddWithValue("@ESI_Reg_Code", ObjAdmComp.ESI_Reg_Code);//10
                        objSqlCommand.Parameters.AddWithValue("@ESI_Branch", ObjAdmComp.ESI_Branch);
                        objSqlCommand.Parameters.AddWithValue("@PT_Reg_No", ObjAdmComp.PT_Reg_No);
                        objSqlCommand.Parameters.AddWithValue("@PAN_No", ObjAdmComp.PAN_No);
                        objSqlCommand.Parameters.AddWithValue("@Weak_Off", ObjAdmComp.Weak_Off);
                        objSqlCommand.Parameters.AddWithValue("@Status", ObjAdmComp.Status);
                        objSqlCommand.Parameters.AddWithValue("@Creation_Company", ObjAdmComp.Creation_Company);
                        objSqlCommand.Parameters.AddWithValue("@Created_By", ObjAdmComp.Created_By);
                        objSqlCommand.Parameters.AddWithValue("@Created_Date", ObjAdmComp.Created_Date);
                        objSqlCommand.Parameters.AddWithValue("@Modified_By", ObjAdmComp.Modified_By);
                        objSqlCommand.Parameters.AddWithValue("@Modified_Date", ObjAdmComp.Modified_Date);//10
                        objSqlCommand.Parameters.AddWithValue("@Parameter", ObjAdmComp.Parameter);

                        objSqlCommand.Transaction = transaction;
                        if (objSqlCommand.ExecuteNonQuery() != 0)
                        { debitResult = true; }
                    }
                    if (grvBankDetail.Rows.Count != 0)
                    {
                        foreach (GridViewRow objrow in grvBankDetail.Rows)
                        {
                            using (SqlCommand objSqlCommand = objSqlConnection.CreateCommand())
                            {
                                // Only look in data rows, ignore header and footer rows
                                if (objrow.RowType == DataControlRowType.DataRow)
                                {
                                    DropDownList ddl1 = objrow.FindControl("ddlBankName") as DropDownList;
                                    TextBox box1 = objrow.FindControl("txtAccountNumber") as TextBox;
                                    TextBox box2 = objrow.FindControl("txtBranch") as TextBox;

                                    if (box2.Text != "")
                                    {
                                        //   ObjAdmComp.Voucher_No = Voucher_ID;
                                        ObjAdmComp.Bank_Name = ddl1.SelectedItem.Value.ToString();
                                        ObjAdmComp.Account_Number = box1.Text;
                                        ObjAdmComp.Branch = box2.Text;
                                        ObjAdmComp.Parameter = Parameter;
                                        ObjAdmComp.Creation_Company = (string)Session["ClientRegistrationNo"];
                                        ObjAdmComp.Created_By = (string)Session["User_Name"];
                                        ObjAdmComp.Created_Date = Convert.ToDateTime(DateTime.Now.ToString());
                                        ObjAdmComp.Modified_By = (string)Session["User_Name"];
                                        ObjAdmComp.Modified_Date = Convert.ToDateTime(DateTime.Now.ToString());

                                        objSqlCommand.CommandType = CommandType.StoredProcedure;
                                        objSqlCommand.CommandText = "Sp_Insert_Select_Update_Administrator_CompanyInfo_BankDetails";
                                        objSqlCommand.Parameters.AddWithValue("@ClientRegistrationNo", ObjAdmComp.ClientRegistrationNo);
                                        objSqlCommand.Parameters.AddWithValue("@Bank_Name", ObjAdmComp.Bank_Name);
                                        objSqlCommand.Parameters.AddWithValue("@Account_Number", ObjAdmComp.Account_Number);
                                        objSqlCommand.Parameters.AddWithValue("@Branch", ObjAdmComp.Branch);
                                        objSqlCommand.Parameters.AddWithValue("@Creation_Company", ObjAdmComp.Creation_Company);
                                        objSqlCommand.Parameters.AddWithValue("@Created_By", ObjAdmComp.Created_By);
                                        objSqlCommand.Parameters.AddWithValue("@Created_Date", ObjAdmComp.Created_Date);
                                        objSqlCommand.Parameters.AddWithValue("@Modified_By", ObjAdmComp.Modified_By);
                                        objSqlCommand.Parameters.AddWithValue("@Modified_Date", ObjAdmComp.Modified_Date);
                                        objSqlCommand.Parameters.AddWithValue("@Parameter", ObjAdmComp.Parameter);
                                        objSqlCommand.Transaction = transaction;
                                        if (objSqlCommand.ExecuteNonQuery() != 0)
                                        { creditResult = true; }
                                    }
                                }
                            }
                        }
                        //}

                        if (debitResult && creditResult)
                        {
                            transaction.Commit();

                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Saved Successfully');", true);

                            ClearAllFields();
                        }
                        else
                        {
                            transaction.Rollback();
                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Enter Bank Details');", true);
                    DropDownList box11 = (DropDownList)grvBankDetail.Rows[a1 - 1].FindControl("ddlBankName") as DropDownList;
                    box11.Focus();
                }
            }
            catch (Exception ex)
            {
                Session["Error"] = ex.Message.ToString();
            }

            finally
            {
            }
        }

        /// <param name="e"></param>
        protected void btnAddDetails_Click(object sender, EventArgs e)
        {
            Button btnPopup = (Button)sender;
            GridViewRow gvr = (GridViewRow)btnPopup.NamingContainer;
            int index = gvr.RowIndex;
            Session["grdIndex"] = index;

            this.mp1.Show();
            txtBankName.Focus();
        }

        //Clear all Files Event
        protected void btnCancel_click(object sender, EventArgs e)
        {
            ClearAllFields();
        }
        /// <summary>
        /// Adding New Bank Details by Using 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddNewRecord_Click(object sender, EventArgs e)
        {
            // Adding Empty Row Data into Customer Address Details

            int Count = (int)grvBankDetail.Rows.Count;
            DropDownList BankName = (DropDownList)grvBankDetail.Rows[Count - 1].FindControl("ddlBankName") as DropDownList;
            TextBox AccountNumber = (TextBox)grvBankDetail.Rows[Count - 1].FindControl("txtAccountNumber") as TextBox;
            TextBox Branch = (TextBox)grvBankDetail.Rows[Count - 1].FindControl("txtBranch") as TextBox;

            if (BankName.Text == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Select Bank Name');", true);
                BankName.Focus();
                this.mp1.Show();
                txtBankName.Focus();
            }
            else if (AccountNumber.Text == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Enter Account Number');", true);
                AccountNumber.Focus();
            }
            else if (Branch.Text == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Enter Branch');", true);
                Branch.Focus();
            }

            else
            {
                AddNewRowToGrid();
                int aa = (int)grvBankDetail.Rows.Count;
                TextBox box = (TextBox)grvBankDetail.Rows[aa - 1].FindControl("txtbranchId") as TextBox;
                BankName.Focus();
            }

        }

        /// <summary>
        /// Remove Bank Name in Grid view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbRemove_Click(object sender, EventArgs e)
        {
            AddRemoveNewRowToGrid();
            //  SetRemovePreviousData();
            LinkButton lb = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow.RowIndex;
            if (ViewState["CurrentTable"] != null)
            {

                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 1)
                {
                    //Remove the Selected Row data and reset row number
                    dt.Rows.Remove(dt.Rows[rowID]);
                }

                //Store the current data in ViewState for future reference
                ViewState["CurrentTable"] = dt;
                //   LinkButton AddressDetails = (LinkButton)grvCustomerAddressDetail.Rows[rowID].FindControl("lbRemove");
                // AddressDetails.Focus();

                grvBankDetail.DataSource = dt;
                grvBankDetail.DataBind();
            }
        }

        //Pop up Reset Button for Bank Details
        protected void btnAddReset_Click(object sender, EventArgs e)
        {
            txtBankName.Text = txtBankID.Text = "";
            this.mp1.Show();
        }

        /// <summary>
        /// Grid Row DataBound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grvBankDetail_DataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddlItem_Name = (DropDownList)e.Row.FindControl("ddlBankName");
                GridViewRow gr = grvBankDetail.SelectedRow;
                Bind_ProdName(ddlItem_Name);

                TextBox bankname = (TextBox)e.Row.FindControl("txtBank_Name");
                ddlItem_Name.SelectedValue = bankname.Text;
            }
        }


        /// <summary>
        /// Binding Bank Names
        /// </summary>
        /// <param name="ddlBankName"></param>
        public void Bind_ProdName(DropDownList ddlBankName)
        {
            try
            {
                String Creation_Company = Session["ClientRegistrationNo"].ToString();
                List<HRMS_Bank_Master> objGroup = objBankBAL.GetData_BankMaster(Creation_Company);

                if (objGroup.Count > 0)
                {
                    ddlBankName.DataSource = objGroup;
                    ddlBankName.DataTextField = "Bank_Name";
                    ddlBankName.DataValueField = "Bank_Name";
                    ddlBankName.DataBind();
                    ddlBankName.Items.Insert(0, new ListItem("Select Bank", "Select Bank")); //updated code
                }
                else
                {
                    ddlBankName.Items.Insert(0, new ListItem("Select Bank", "Select Bank")); //updated code
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            {

            }
        }


        /// Inserting bank name to Bank_Master Table
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNewAddBankDetails_Click(object sender, EventArgs e)
        {
            if (txtBankName.Text != "")
            {
                String Creation_Company = Session["ClientRegistrationNo"].ToString();
                string bankname = txtBankName.Text;
                List<HRMS_Bank_Master> ObjGroup = (from master in objDb.HRMS_Bank_Masters
                                                   where master.Creation_Company == Creation_Company &&
                                                         master.Bank_Name.ToLower().Trim() == bankname.ToLower().Trim()
                                                   select master).ToList();
                if (ObjGroup.Count > 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Enter Department Name Already Exist..');", true);
                    this.mp1.Show();
                    txtBankName.Text = "";
                    txtBankName.Focus();
                }
                else
                {

                    BankDetailsSave();

                    //DropDownList ddlbank = (DropDownList)sender;
                    //GridViewRow gvr = (GridViewRow)ddlbank.NamingContainer;
                    //int index = gvr.RowIndex;
                    //Session["grdIndex"] = index;
                    //DropDownList ddlbank = (DropDownList)grvBankDetail.FindControl("ddlBankName");

                    //if (ddlbank.Items.FindByValue(txtBankName.Text) == null)
                    //{
                    //    ddlbank.Items.Insert(1, new ListItem(txtBankName.Text, txtBankName.Text));
                    //    txtBankName.Text = "";
                    //    ddlbank.SelectedValue = "Select Bank";
                    //    this.mp1.Show();
                    //    txtBankName.Focus();
                    //}
                }


            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Plz Enter Valid Data..');", true);
                this.mp1.Show();
                txtBankName.Focus();
            }
        }

        /// <summary>
        /// Inserting Group Name To Group_Master Table
        /// </summary>

        public void BankDetailsSave()
        {
            objBankBAL.Bank_Id = "";
            objBankBAL.Bank_Name = txtBankName.Text;
            objBankBAL.Created_By = Session["User_Name"].ToString();
            objBankBAL.Created_Date = System.DateTime.Now;
            objBankBAL.Creation_Company = Session["ClientRegistrationNo"].ToString();
            objBankBAL.Modified_By = Session["User_Name"].ToString();
            objBankBAL.Modified_Date = System.DateTime.Now;
            objBankBAL.Parameter = 1;
            if (objBankBAL.Insert_Bank_Master() != 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);

                this.mp1.Show();
            }
        }

        #endregion

        #region Privte Methods

        // // // // The Following Method is Used For Clear all Fields 
        // // Created By Rambabu
        public void ClearAllFields()
        {
            txtCompanyname.Text = txtAddress1.Text = txtAddress2.Text = txtcedivision.Text = txtcerange.Text = txtCity.Text =
                txtcommissionerate.Text = txtCompanyid.Text = txtcstno.Text = txtECCNo.Text = txtEmail.Text = txtFaxNO.Text =
                txtPhone.Text = txtPinCode.Text = txtState.Text = txtTINNO.Text = txtWeburl.Text = txtcstno.Text =
                txtPFCode.Text = txtESIBranch.Text = txtESIRegCode.Text = txtPTRegNo.Text = txtPANNo.Text = "";
            ddlstatus.SelectedValue = ddlstatus.SelectedValue = ddlPFRegion.SelectedValue = ddlPFOffice.SelectedValue = "0";
            ddlWeekOFF.SelectedValue = "Select Week OFF";

            // Empty Rows Binding grid view
            SetInitialRow();

            txtCompanyname.Focus();
            btnSave.Visible = true;
            btnUpdate.Visible = false;
        }

        //Add New Record
        public void AddNewRowToGrid()
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        DropDownList BankName = (DropDownList)grvBankDetail.Rows[rowIndex].Cells[0].FindControl("ddlBankName");
                        TextBox AccountNumber = (TextBox)grvBankDetail.Rows[rowIndex].Cells[1].FindControl("txtAccountNumber");
                        TextBox Branch = (TextBox)grvBankDetail.Rows[rowIndex].Cells[2].FindControl("txtBranch");

                        // TextBox PAN = (TextBox)grvCustomerAddressDetail.Rows[rowIndex].Cells[5].FindControl("txtPAN");
                        drCurrentRow = dtCurrentTable.NewRow();
                        dtCurrentTable.Rows[i - 1]["Bank_Name"] = BankName.SelectedValue;
                        dtCurrentTable.Rows[i - 1]["Account_Number"] = AccountNumber.Text;
                        dtCurrentTable.Rows[i - 1]["Branch"] = Branch.Text;
                        //  dtCurrentTable.Rows[i - 1]["PAN"] = PAN.Text;

                        rowIndex += 1;
                    }

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["CurrentTable"] = dtCurrentTable;
                    grvBankDetail.DataSource = dtCurrentTable;
                    grvBankDetail.DataBind();

                }
            }
            else
            {
                Response.Write("ViewState is null");
            }

            SetPreviousData();

        }

        /// <summary>
        /// Removing added  Row Data Into Customer Address Details 
        /// Created By Divya
        /// </summary>
        /// <param name="sender"></param>

        // // add new row remove
        public void AddRemoveNewRowToGrid()
        {

            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = null;
                dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                int index = dtCurrentTable.Rows.Count;

                if (dtCurrentTable.Rows.Count > 0)
                {


                    DropDownList BankName = (DropDownList)grvBankDetail.Rows[index - 1].Cells[0].FindControl("ddlBankName");
                    TextBox AccountNumber = (TextBox)grvBankDetail.Rows[index - 1].Cells[1].FindControl("txtAccountNumber");
                    TextBox Branch = (TextBox)grvBankDetail.Rows[index - 1].Cells[2].FindControl("txtBranch");

                    // TextBox PAN = (TextBox)grvCustomerAddressDetail.Rows[rowIndex].Cells[5].FindControl("txtPAN");



                    drCurrentRow = dtCurrentTable.NewRow();
                    dtCurrentTable.Rows[index - 1]["Bank_Name"] = BankName.SelectedValue;
                    dtCurrentTable.Rows[index - 1]["Account_Number"] = AccountNumber.Text;
                    dtCurrentTable.Rows[index - 1]["Branch"] = Branch.Text;

                    //  dtCurrentTable.Rows[index - 1]["PAN"] = PAN.Text;


                    rowIndex += 1;
                    //  }

                    // dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["CurrentTable"] = dtCurrentTable;
                    grvBankDetail.DataSource = dtCurrentTable;
                    grvBankDetail.DataBind();

                }
            }
            else
            {
                Response.Write("ViewState is null");
            }

            SetPreviousData();

        }

        /// <summary>
        /// Set reomve Row Data Into Customer Address Details 
        /// Created By Divya
        /// </summary>
        /// <param name="sender"></param>

        // set remove row 
        private void SetRemovePreviousData()
        {

            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        //Set the Previous Selected Items on Each DropDownList on Postbacks

                        DropDownList BankName = (DropDownList)grvBankDetail.Rows[rowIndex].Cells[0].FindControl("ddlBankName");
                        TextBox AccountNumber = (TextBox)grvBankDetail.Rows[rowIndex].Cells[1].FindControl("txtAccountNumber");
                        TextBox Branch = (TextBox)grvBankDetail.Rows[rowIndex].Cells[2].FindControl("txtBranch");


                        // TextBox PAN = (TextBox)grvCustomerAddressDetail.Rows[rowIndex].Cells[5].FindControl("txtPAN");


                        if (i < dt.Rows.Count - 1)
                        {
                            BankName.SelectedValue = dt.Rows[i]["Bank_Name"].ToString();
                            AccountNumber.Text = dt.Rows[i]["Account_Number"].ToString();
                            Branch.Text = dt.Rows[i]["Branch"].ToString();

                            //   PAN.Text = dt.Rows[i]["PAN"].ToString();


                        }

                        rowIndex++;
                    }
                }
            }


        }

        /// <summary>
        /// Set Previous Row Data Into Customer Address Details 
        /// Created By Divya
        /// </summary>
        /// <param name="sender"></param>

        //Set Previous Record on Grid view
        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        //Set the Previous Selected Items on Each DropDownList on Postbacks
                        DropDownList BankName = (DropDownList)grvBankDetail.Rows[rowIndex].Cells[0].FindControl("ddlBankName");
                        TextBox AccountNumber = (TextBox)grvBankDetail.Rows[rowIndex].Cells[1].FindControl("txtAccountNumber");
                        TextBox Branch = (TextBox)grvBankDetail.Rows[rowIndex].Cells[2].FindControl("txtBranch");
                        // TextBox PAN = (TextBox)grvCustomerAddressDetail.Rows[rowIndex].Cells[5].FindControl("txtPAN");
                        if (i < dt.Rows.Count - 1)
                        {
                            BankName.SelectedValue = dt.Rows[i]["Bank_Name"].ToString();
                            AccountNumber.Text = dt.Rows[i]["Account_Number"].ToString();
                            Branch.Text = dt.Rows[i]["Branch"].ToString();
                            //  PAN.Text = dt.Rows[i]["PAN"].ToString();
                        }

                        rowIndex++;
                    }
                }
            }


        }

        #endregion

        protected void txtCompanyid_TextChanged(object sender, EventArgs e)
        {
            Edit_SalesDetails(txtCompanyid.Text, Session["ClientRegistrationNo"].ToString());
        }

        protected void txtCompanyname_TextChanged(object sender, EventArgs e)
        {
            try
            {
                var CompanyList = (from L in objDb.Administrator_CompanyInfos
                                   where L.Company_Name == txtCompanyname.Text
&& L.Creation_Company == Session["ClientRegistrationNo"].ToString()
                                   select L).ToList();
                if (CompanyList.Count > 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Company Name already Exists');", true);
                    txtCompanyname.Text = "";
                    txtCompanyname.Focus();
                }
                else
                {
                    txtAddress1.Focus();
                }


            }
            catch (Exception ex)
            {

            }
        }
    }
}
