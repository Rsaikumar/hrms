﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Company_Info_Webuser.ascx.cs"
    Inherits="HRMS.Administrator.Company_Info_Webuser" %>
<link href="../css/Administrator/Company_Info_Style.css" rel="stylesheet" type="text/css" />
<%--<link href="../css/icons_styles.css" rel="stylesheet" type="text/css" />--%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<script type="text/javascript" language="javascript">
    function Search() {
        if (document.getElementById('<%=txtSearch.ClientID%>').value == "") {
            alert("Please Enter Search Keys for Storage Location");
            document.getElementById('<%=txtSearch.ClientID%>').focus();
            return false;
        }
        return true;
    } 

</script>
<script type="text/javascript">
    function SelectedObjRptPopUp(url) {
        window.location = url;
    }
</script>
<div id="main">
    <div class="container-fluid">
        <div class="page-header">
            <div class="pull-left Search_Div" id="btnSearch_Div">
                <div class="Search_Icon">
                    <asp:TextBox ID="txtSearch" runat="server" placeholder="Search..." class="form-control txtSearch"></asp:TextBox>
                </div>
            </div>
            <div class="pull-left">
                <asp:Button ID="btnSearch" runat="server" class="search_Btn" />
            </div>
            <div class="pull-left" style="visibility: hidden">
                <asp:DropDownList ID="ddlAction" runat="server" class="form-control ddlAction" data-style="btn-primary"
                    OnSelectedIndexChanged="ddlAction_OnSelectedIndexChanged" AutoPostBack="true">
                    <asp:ListItem Text="Action" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Export-Excel" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Export-PDF" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Delete" Value="2"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="pull-right">
                <ul class="stats">
                    <li>
                        <asp:Button ID="btnAdd" runat="server" class="add_Btn" OnClick="btnAdd_Click" /></li>
                    <li>
                        <asp:Button ID="btnImport" runat="server" class="PDF_Btn" /></li>
                    <li>
                        <asp:Button ID="Button1" runat="server" class="Excel_Btn" /></li>
                </ul>
            </div>
        </div>
        <div class="breadcrumbs">
            <ul>
                <li><a href="../Dashboard/Dashboard.aspx">Administrator</a> <i class="fa fa-angle-right">
                </i></li>
                <li><a href="../Administrator/Company_Info.aspx">Company Info</a> <i class="fa fa-angle-right">
                </i></li>
                <li><a href="../Administrator/AddCompanyInfomation.aspx">Add Company Info</a> </li>
            </ul>
            <div class="close-bread">
                <a href="#"><i class="fa fa-times"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                    <div class="box-title">
                        <h3>
                            <i class="fa fa-th-list"></i>Company Info</h3>
                    </div>
                    <div class="box-content">
                        <form action="#" class='form-horizontal form-bordered'>
                        <div class="col-sm-12" style="margin-bottom: 0.3%;">
                            <div class="row">
                                <asp:GridView ID="grdCompanyInfo" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                    Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%;" runat="server"
                                    DataKeyNames="ClientRegistrationNo" EmptyDataText="No Records Found" EmptyDataRowStyle-Font-Size="Medium"
                                    EmptyDataRowStyle-ForeColor="Red" GridLines="Vertical" AutoGenerateColumns="False"
                                    OnPageIndexChanging="grdCompanyinfo_PageIndexChanging" AllowPaging="True" OnRowEditing="grdCompanyInfo_RowEditing"
                                    OnRowUpdating="grdCompanyInfo_RowUpdating" OnRowCommand="grdCompanyInfo_Registration_RowCommand"
                                    OnRowCancelingEdit="grdCompanyInfo_RowCancelingEdit">
                                    <AlternatingRowStyle BackColor="#EEEEEE" />
                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#000065" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Company Id">
                                            <ItemTemplate>
                                                <asp:Label ID="lblcompanyid" runat="server" placeholder="Company ID" Text='<%# Eval("ClientRegistrationNo")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Company Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblcompanyname" runat="server" placeholder="Company Name" Text='<%# Eval("Company_Name")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtcompanyname" runat="server" placeholder="Company Name" Text='<%# Eval("Company_Name")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="State">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstate" runat="server" placeholder="State" Text='<%# Eval("State")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtstate" runat="server" placeholder="State" Text='<%# Eval("State")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PIN Code">
                                            <ItemTemplate>
                                                <asp:Label ID="lblpincode" runat="server" placeholder="PIN Code" Text='<%# Eval("Pincode")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtpincode" runat="server" placeholder="PIN Code" Text='<%# Eval("Pincode")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="E-Mail">
                                            <ItemTemplate>
                                                <asp:Label ID="lblemail" runat="server" placeholder="E Mail" Text='<%# Eval("E_Mail")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtemail" runat="server" placeholder="E Mail" Text='<%# Eval("E_Mail")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Web URL">
                                            <ItemTemplate>
                                                <asp:Label ID="lblweburl" runat="server" placeholder="Web URL" Text='<%# Eval("Web_URL")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtweburl" runat="server" placeholder="Web URL" Text='<%# Eval("Web_URL")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TIN No">
                                            <ItemTemplate>
                                                <asp:Label ID="lbltinno" runat="server" placeholder="TIN NO" Text='<%# Eval("TIN_No")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txttinno" runat="server" placeholder="TIN NO" Text='<%# Eval("TIN_No")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnEdit" runat="server" CommandName="Edit" Style="color: gray;
                                                    text-decoration: none; float: left; background-image: url('../img/file_edit.png');
                                                    height: 30px; width: 35px; background-position: left; background-repeat: no-repeat;"
                                                    OnClientClick="return confirm('Are you sure you want to Edit this Record?')"
                                                    CausesValidation="false" CommandArgument='<%# Eval("ClientRegistrationNo")%>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkRemove" runat="server" CommandName="Delete" Style="color: gray;
                                                    text-decoration: none; float: left; background-image: url('../img/delete.png');
                                                    height: 30px; width: 35px; background-position: left; background-repeat: no-repeat;"
                                                    OnClientClick="return confirm('Are you sure you want to Remove this Record? /n Note : There Is No Undo')"
                                                    CausesValidation="false" CommandArgument='<%# Eval("ClientRegistrationNo")%>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%-- <asp:CommandField ShowEditButton="true" />--%>
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <%--  <cc1:ModalPopupExtender ID="mp1" runat="server" PopupControlID="Panel1" TargetControlID="btnImport"
                                CancelControlID="btnClose" BackgroundCssClass="modalBackground2">
                            </cc1:ModalPopupExtender>
                            <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup2" ScrollBars="None" align="center"
                                Style="display: none">
                                <div class="modal2-header" runat="server" id="Popupheader">
                                    <h3>
                                        Import Company Info <a class="close-modal2" href="#" id="btnClose">&times;</a></h3>
                                </div>
                                <div class="modal-body">
                                    <div class="col-sm-12" style="">
                                        <div class="form-group">
                                            <div class="col-sm-2" style="margin-left: 0px;">
                                                <asp:FileUpload ID="fullcompanyinfo" runat="server" class="btn btn-primary" />
                                            </div>
                                            <div class="col-sm-5" style="margin-left: 0px;">
                                                <asp:Button ID="Import" class="btn btn-primary" runat="server" Text="Import Data"
                                                    OnClick="btnImportData_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>--%>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
