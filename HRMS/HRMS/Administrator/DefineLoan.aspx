﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="DefineLoan.aspx.cs" Inherits="HRMS.Payroll.Define_Loan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <script type="text/javascript">
    function Validate() {
        var ddlFruits = document.getElementById("<%=ddlAllotment.ClientID %>");
        if (ddlFruits.value == "Select") {
            //If the "Please Select" option is selected display error.
            alert("Please select an option!");
            return false;
        }
        return true;
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <section id="main-content">
        <section class="wrapper">

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a></li>
                        <li><a href="#">Payroll</a><i class="fa fa-angle-right"></i> </li>
                        <li><a href="DefineLoan.aspx" class="current">Define Loan Types </a></li>
                    </ul>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <%--<i class="fa fa-th-list"> Define Loan Types </i>--%>
                        </header>
                        <div class="panel-body">
                            <form action="#" method="POST" class='form-horizontal form-bordered'>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="col-md-12">
                                            <div class="form-group col-md-6">
                                                <label for="textfield" class="control-label col-md-5">
                                                    Loan Name<span id="Span1" runat="server" style="color: Red; display: inline;">*</span></label>
                                                <div class="col-md-7 ">
                                                    <asp:TextBox ID="txtLoanName" runat="server" TabIndex="0" placeholder="Loan Name" class="form-control"
                                                        MaxLength="50" AutoPostBack="true" OnTextChanged="txtLoanName_TextChanged" required></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="textfield" class="control-label col-md-5">
                                                    Short Name<span style="color: #ff0000; font-size: 14px; display: inline;">*</span></label>
                                                <div class="col-md-7" id="Div1">
                                                    <asp:TextBox ID="txtShortName" runat="server" TabIndex="1" placeholder="Short Name" class="form-control"
                                                        MaxLength="50" AutoPostBack="true" OnTextChanged="txtShortName_TextChanged" required></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <div class="col-md-1" style="" id="Div2">
                                                    <asp:CheckBox ID="chbCertification" runat="server" Text="" AutoPostBack="true" />
                                                </div>
                                                <label for="textfield" class="control-label col-md-11" style="">
                                                    Document Evidence is required..?</label>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="textfield" class="control-label col-md-5">
                                                    Allotment<span style="color: #ff0000; font-size: 14px; display: inline;">*</span>
                                                </label>
                                                <div class="col-md-7">
                                                    <asp:DropDownList ID="ddlAllotment" runat="server" class="form-control DropdownCss" required>
                                                        <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                        <asp:ListItem Text="Auto" Value="Auto"></asp:ListItem>
                                                        <asp:ListItem Text="Manual" Value="Manual"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <div class="col-md-12 text-center">
                                    <asp:Button ID="btnSave" runat="server" Text="Submit" class="btn btn-primary" OnClientClick="return Validate();" OnClick="btnSave_Click" />
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-primary" OnClick="btnUpdate_Click" />
                                </div>
                            </form>
                        </div>
                    </section>
                </div>

            </div>

        </section>
    </section>
</asp:Content>
