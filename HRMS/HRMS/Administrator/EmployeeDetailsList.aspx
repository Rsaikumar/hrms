﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/Admin.Master" AutoEventWireup="true" CodeBehind="EmployeeDetailsList.aspx.cs" Inherits="HRMS.Administrator.EmployeeDetailsList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
        <section class="wrapper">

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i> Dashboard</a> </li>
                        <li><a href="../Employee/EmployeeDetailsList.aspx" class="current"> Employees List</a> </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <i class="fa fa-th-list">Employees List</i>
                        </header>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div style="max-height: 515px; overflow: scroll;" class="table-responsive">
                                    <asp:GridView ID="gdvemplist"  CssClass="mydatagrid" HeaderStyle-CssClass="headergrid" RowStyle-CssClass="rows"
                                        runat="server" Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%; overflow:scroll;"  AutoGenerateColumns="false" OnRowDataBound="gdvemplist_RowDataBound" OnSelectedIndexChanged="gdvemplist_SelectedIndexChanged" >
                                        <Columns>
                                            <asp:BoundField DataField="EmpId" HeaderText="Emp Id" ItemStyle-Width="80" />
                                            <asp:BoundField DataField="FirstName" HeaderText="First Name" ItemStyle-Width="200" />
                                            <asp:BoundField DataField="LastName" HeaderText="Last Name" ItemStyle-Width="200" />
                                            <asp:BoundField DataField="UserRole" HeaderText="Designation" ItemStyle-Width="200" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                             <div class="col-md-12 text-center">
                                <asp:Button ID="btncancel" runat="server" CssClass=" btn btn-primary" Text="Back" OnClick="btncancel_Click" />
                            </div>
                        </div>  
                    </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
