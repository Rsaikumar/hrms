﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;


namespace HRMS.Administrator
{
    public partial class SalaryDeductions : System.Web.UI.Page
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                HRMSSalaryDeductionPercentage Salary = new HRMSSalaryDeductionPercentage();
                Salary.PF = Convert.ToDecimal(txtpf.Text);
                Salary.DA = Convert.ToDecimal(txtda.Text);
                Salary.HRA = Convert.ToDecimal(txthra.Text);
                Salary.CCA = Convert.ToDecimal(txtcca.Text);
                Salary.ESI = Convert.ToDecimal(txtesi.Text);
                Salary.CreatedBy = "user";
                Salary.CreatedOn = DateTime.Now;
                objDAL.HRMSSalaryDeductionPercentages.InsertOnSubmit(Salary);
                objDAL.SubmitChanges();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);
                ClearFields();
            }
            catch (Exception ex)
            {
                ex.Message.ToString();

            }
        }

        public void ClearFields()
        {
            txtpf.Text = string.Empty;
            txtda.Text = string.Empty;
            txthra.Text = string.Empty;
            txtcca.Text = string.Empty;
            txteesi.Text = string.Empty;
            txtesi.Text = string.Empty;
        }

    }
}