﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Administrator
{
    public partial class EarningsAndDeductionsView : System.Web.UI.Page
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                if ((string)Session["EmployeeDetailsCode"] != null)
                {

                    string empcode1 = (string)Session["EmployeeDetailsCode"];

                    Edit_Salary_Details(empcode1);
                }
                else
                {

                }
            }
        }

        public void Edit_Salary_Details(string empcode)
        {
            try
            {
                string ID = Convert.ToString(empcode);
                List<HRMSEarningAndDeduction> objEmp = (from master in objDAL.HRMSEarningAndDeductions
                                                        where master.Id == Convert.ToInt32( ID)
                                                     select master).ToList();

                if (objEmp.Count > 0)
                {
                    txtname.Text = Convert.ToString(objEmp[0].Name);
                    txtvalue.Text = Convert.ToString(objEmp[0].Value);
                    ddltype.SelectedItem.Text = Convert.ToString(objEmp[0].Type);
                    ddlpayslip.SelectedItem.Text = Convert.ToString(objEmp[0].PayslipType);

                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {

                string empcode = (string)Session["EmployeeDetailsCode"];
                int empid = Convert.ToInt32(empcode);

                var role = (from s in objDAL.HRMSEarningAndDeductions.Where(x => x.Id == Convert.ToInt32(empcode)) select s).SingleOrDefault();
                {
                    role.Name = txtname.Text;
                    role.Type = ddltype.SelectedItem.Text;
                    role.Value = Convert.ToDecimal(txtvalue.Text);
                    role.PayslipType = ddlpayslip.SelectedItem.Text;

                    role.ModifiedOn = DateTime.Now;
                    role.ModifiedBy = Convert.ToString(Session["Emp_Code"]);
                    objDAL.SubmitChanges();
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Updated Sucessfully');window.location ='../Administrator/EarningAndDeduction.aspx';", true);

                }
            }
            catch
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Try Again');", true);

            }
        }

        protected void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                string empcode = (string)Session["EmployeeDetailsCode"];
                int empid = Convert.ToInt32(empcode);

                var address = (from s in objDAL.HRMSEarningAndDeductions.Where(x => x.Id == empid) select s).SingleOrDefault();
                objDAL.HRMSEarningAndDeductions.DeleteOnSubmit(address);
                objDAL.SubmitChanges();



                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Removed Sucessfully');window.location ='../Administrator/EarningAndDeduction.aspx';", true);

            }


            catch
            {
            }
        }

        protected void btncancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Administrator/EarningAndDeduction.aspx");

        }
    }
}