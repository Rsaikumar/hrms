﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Administrator
{
    public partial class EmployeeList : System.Web.UI.Page
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string empcode = Convert.ToString(Session["EmployeeDetailsCode"]);

                bindgirdemployeedetails();
            }
        }


        public void bindgirdemployeedetails()
        {
            //getting session from login page 
            int roleid = Convert.ToInt32(Session["role"].ToString());
            int empid = Convert.ToInt32(Session["Emp_Code"]);
            //here employee list is filtered based on employee role 

            var qry = objDAL.usp_GetEmployeeList();

            var distinctList = qry.GroupBy(x => x.EmpId)
                     .Select(g => g.First()).OrderByDescending(x => x.EmpId)
                     .Distinct().ToList();

            if (qry != null)
            {
                gdvemplist.DataSource = distinctList;
                gdvemplist.DataBind();
            }
            else
            {
            }
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Administrator/AdminDasboard.aspx");
        }
    }
}