﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BAL;
using DAL;
using System.Data.Linq.SqlClient;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using System.IO;

namespace HRMS.Administrator
{
    public partial class EmployeesEditDetails : System.Web.UI.Page
    {
        #region Declaration
        string objConfig = ConfigurationManager.ConnectionStrings["Dbconnection"].ToString();

        CityMasterBAL objCityBAL = new CityMasterBAL();
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();
        HRMS_City_Master objCityTable = new HRMS_City_Master();
        DepartmentMasterBAL objDeptBAL = new DepartmentMasterBAL();
        EmployeeGroupMasterBAL objEmpGroupBAL = new EmployeeGroupMasterBAL();
        RecruitMasterBAL objRecruitBAL = new RecruitMasterBAL();
        Employee_Location_BAL ObjEmpLocation = new Employee_Location_BAL();

        JobTypeBAL objJobTypeBAL = new JobTypeBAL();
        EmployeeBAL objEmpBAL = new EmployeeBAL();
        EmployeeOfficialInfoBAL objeoinfo = new EmployeeOfficialInfoBAL();
        EmployeeBAL Total1EmpDetailsBAL = new EmployeeBAL();

        // // // Bank Details Master
        BankMasterBAL objBankBAL = new BankMasterBAL();
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_Type"].ToString() == "CCF")
            {
                //// // Paste The Employee Binding Methods
                string Creation_Company = Session["ClientRegistrationNo"].ToString();
                string empcode = Session["Emp_Code"].ToString();

            }
            else
            {
                if (!IsPostBack)
                {
                    txtcasualleave.Visible = true;
                    txtearnedleave.Visible = true;
                    lblcasualid.Text = "CASUAL Leave";
                    lblearnedid.Text = "Earned Leave";
                    bindtheleaveid();
                    BindRole();
                    BindSubRole();
                    AuthorityBinding();
                    iddiv.Visible = false;
                    string empcode = Convert.ToString(Session["EmployeeDetailsCode"]);
                    Edit_Employee_Details(empcode);

                    if (Session["ClientRegistrationNo"] == null)
                    {
                        Response.Redirect("~/Default.aspx");
                    }

                }
            }
        }

        public void Edit_Employee_Details(string empcode)
        {
            try
            {
                int ID = Convert.ToInt32(empcode);
                List<HRMSEmployeeMasterRegistrationDetail> objEmpRegistration = (from master in ObjDAL.HRMSEmployeeMasterRegistrationDetails
                                                                                 where master.ID == ID
                                                                                 select master).ToList();

                if (objEmpRegistration.Count > 0)
                {
                    txtEmp_Code.Text = Convert.ToString(objEmpRegistration[0].ID);
                    ddluserrole.SelectedValue = Convert.ToString(objEmpRegistration[0].UserRole);
                    ddlUSersubRole.SelectedValue = Convert.ToString(objEmpRegistration[0].UserSubRole);

                    List<HRMSEmplyeeMasterPersonalDetail> objEmppersonaldetails = (from master in ObjDAL.HRMSEmplyeeMasterPersonalDetails
                                                                                   where master.EmpId == txtEmp_Code.Text
                                                                                   select master).ToList();
                    txtFirstName.Text = objEmppersonaldetails[0].FirstName;
                    txtMiddleName.Text = objEmppersonaldetails[0].MiddleName;
                    txtLastName.Text = objEmppersonaldetails[0].LastName;

                    txtDateOfBirth.Text = Convert.ToDateTime(objEmppersonaldetails[0].DateOfBirth).ToString("yyyy-MM-dd");
                    ddlMaritalStatus.SelectedValue = objEmppersonaldetails[0].MaritalStatus;
                    rblGender.SelectedValue = objEmppersonaldetails[0].Gender;
                    txtBloodGroup.Text = objEmppersonaldetails[0].BloodGroup;
                    ddlNationality.SelectedValue = objEmppersonaldetails[0].Nationality;

                    List<HRMSEmplyeeMasterPersonalIdentificationDetail> objEmpidentification = (from master in ObjDAL.HRMSEmplyeeMasterPersonalIdentificationDetails
                                                                                                where master.EmpId == txtEmp_Code.Text
                                                                                                select master).ToList();


                    List<HRMSEmployeeMasterAddressDetail> objEmpaddress = (from master in ObjDAL.HRMSEmployeeMasterAddressDetails
                                                                           where master.EmpId == txtEmp_Code.Text
                                                                           select master).ToList();
                    txtAddress1.Text = objEmpaddress[0].PermanentAddress1;
                    txtAddress2.Text = objEmpaddress[0].PermanentAddress2;
                    txtcity.Text = objEmpaddress[0].PermanentCity;
                    txtState.Text = objEmpaddress[0].PermanentState;
                    txtMobile.Text = objEmpaddress[0].MobileNo;
                    txtPostalCode.Text = Convert.ToInt32(objEmpaddress[0].PresentPostalCode).ToString();
                    txtCountry.Text = objEmpaddress[0].PermanentCountry;

                    List<HRMSEmployeeMasterSocialNetworkingDetail> objEmpnetworking = (from master in ObjDAL.HRMSEmployeeMasterSocialNetworkingDetails
                                                                                       where master.EmpId == txtEmp_Code.Text
                                                                                       select master).ToList();
                    txtEmail.Text = objEmpnetworking[0].EmailId;



                    List<HRMSEmployeeMasterOfficialInformationDetail> objEmpofficialinfo = (from master in ObjDAL.HRMSEmployeeMasterOfficialInformationDetails
                                                                                            where master.EmpId == txtEmp_Code.Text
                                                                                            select master).ToList();
                    txtDateofJoin.Text = Convert.ToDateTime(objEmpofficialinfo[0].DateOfJoining).ToString("yyyy-MM-dd");
                    //    ddlReportingAuthority.SelectedItem.Text = Convert.ToString(objEmpofficialinfo[0].first);

                    List<HRMSEmployeeLeave> objEmpleave = (from master in ObjDAL.HRMSEmployeeLeaves
                                                           where master.EmpId == Convert.ToInt32(txtEmp_Code.Text)
                                                           select master).ToList();

                    if (objEmpleave.Count > 0)
                    {
                        txtcasualleave.Text = Convert.ToString(objEmpleave[1].NoOfLeaves);
                        txtearnedleave.Text = Convert.ToString(objEmpleave[0].NoOfLeaves);
                    }
                    else
                    {
                        txtcasualleave.Text = "NA";
                        txtearnedleave.Text = "NA";
                    }




                    List<HRMSEmployeeMasterSalaryDetail> objEmpsalaryinfo = (from master in ObjDAL.HRMSEmployeeMasterSalaryDetails
                                                                             where master.EmpId == txtEmp_Code.Text
                                                                             select master).ToList();
                    txtbasicamount.Text = Convert.ToString(objEmpsalaryinfo[0].BasicAmount);
                    txtgrossamount.Text = Convert.ToString(objEmpsalaryinfo[0].GrossAmount);


                    List<HRMSEmployeeMasterDeductionsDetail> objEmpdeduction = (from master in ObjDAL.HRMSEmployeeMasterDeductionsDetails
                                                                                where master.EmpId == txtEmp_Code.Text
                                                                                select master).ToList();

                    txtnetamount.Text = Convert.ToString(objEmpdeduction[0].NetAmount);


                    int id = Convert.ToInt32(txtEmp_Code.Text);
                    var earnings = (from item in ObjDAL.EarningPercentageAmounts
                                    where item.EmpId == id
                                    select item).ToList();


                    if (earnings.Count > 0)
                    {
                        gvdearnings.DataSource = earnings;
                        gvdearnings.DataBind();
                    }
                    else
                    {
                        gvdearnings.DataSource = earnings;
                        gvdearnings.DataBind();


                    }
                    var deductions = (from item in ObjDAL.DeductionPercentageAmounts
                                      where item.EmpId == id
                                      select item).ToList();


                    if (deductions.Count > 0)
                    {
                        gvddeductions.DataSource = deductions;
                        gvddeductions.DataBind();
                    }
                    else
                    {
                        gvddeductions.DataSource = deductions;
                        gvddeductions.DataBind();


                    }

                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }

        public void bindtheleaveid()
        {
            try
            {

                List<HRMSEmployeeLeave> deduction = (from item in ObjDAL.HRMSEmployeeLeaves
                                                     select item).ToList();


                if (deduction.Count > 0)
                {
                    txtcasualleave.Text = Convert.ToString(deduction[0].NoOfLeaves);
                    txtearnedleave.Text = Convert.ToString(deduction[1].NoOfLeaves);

                }
                else
                {



                }
            }
            catch
            {
            }
        }

        public void AuthorityBinding()
        {
            try
            {
                int empcode = Convert.ToInt32(Session["Emp_Code"]);
                List<HRMSEmployeeMasterOfficialInformationDetail> objEmpofficialinfo = (from master in ObjDAL.HRMSEmployeeMasterOfficialInformationDetails
                                                                                        where master.EmpId == Convert.ToString(Session["Emp_Code"])
                                                                                        select master).ToList();

                var reporting = (from item in ObjDAL.USP_gettingemployeenameandid(empcode)
                                 select item).ToList();

                ddlReportingAuthority.DataSource = reporting;
                ddlReportingAuthority.DataTextField = "empname";
                ddlReportingAuthority.DataValueField = "empid";
                ddlReportingAuthority.DataBind();
            }
            catch (Exception ex)
            {
                throw;
            }
        }




        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Administrator/EmployeeDetailsList.aspx");

        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                int empid = Convert.ToInt32(txtEmp_Code.Text);
                var erd = (from s in ObjDAL.HRMSEmployeeMasterRegistrationDetails.Where(x => x.ID == empid) select s).SingleOrDefault();

                {
                    erd.UserRole = Convert.ToInt32(ddluserrole.SelectedValue);
                    erd.RoleType = ddluserrole.SelectedItem.Text;
                    erd.UserSubRole = Convert.ToInt32(ddlUSersubRole.SelectedValue);
                    erd.ModifiedOn = DateTime.Now;
                    erd.ModifiedBy = Convert.ToString(Session["Emp_Code"]);
                    ObjDAL.SubmitChanges();

                    int id = erd.ID;


                    var epd = (from s in ObjDAL.HRMSEmplyeeMasterPersonalDetails.Where(x => x.EmpId == Convert.ToString(id)) select s).SingleOrDefault();
                    {
                        epd.FirstName = txtFirstName.Text;
                        epd.MiddleName = txtMiddleName.Text;
                        epd.LastName = txtLastName.Text;
                        epd.Gender = rblGender.SelectedValue;
                        epd.DateOfBirth = Convert.ToDateTime(txtDateOfBirth.Text);
                        epd.MaritalStatus = ddlMaritalStatus.SelectedValue;
                        epd.BloodGroup = txtBloodGroup.Text;
                        epd.Nationality = ddlNationality.SelectedValue;
                        epd.Photo = "";
                        epd.ModifiedOn = DateTime.Now;
                        epd.ModifiedBy = Convert.ToString(Session["Emp_Code"]);
                    }

                    var pid = (from s in ObjDAL.HRMSEmplyeeMasterPersonalIdentificationDetails.Where(x => x.EmpId == Convert.ToString(id)) select s).SingleOrDefault();
                    {
                        pid.PassportFile = "";
                        pid.ModifiedOn = DateTime.Now;
                        pid.ModifiedBy = Convert.ToString(Session["Emp_Code"]);
                    }

                    var ead = (from s in ObjDAL.HRMSEmployeeMasterAddressDetails.Where(x => x.EmpId == Convert.ToString(id)) select s).SingleOrDefault();
                    {
                        ead.PermanentAddress1 = txtAddress1.Text;
                        ead.PermanentAddress2 = txtAddress2.Text;
                        ead.PermanentCity = txtcity.Text;
                        ead.PermanentState = txtState.Text;
                        ead.PermanentCountry = txtCountry.Text;
                        ead.PresentPostalCode = Convert.ToInt32(txtPostalCode.Text);
                        ead.MobileNo = txtMobile.Text;
                        ead.ModifiedOn = DateTime.Now;
                        ead.ModifiedBy = Convert.ToString(Session["Emp_Code"]);
                    }

                    var snd = (from s in ObjDAL.HRMSEmployeeMasterSocialNetworkingDetails.Where(x => x.EmpId == Convert.ToString(id)) select s).SingleOrDefault();
                    {
                        snd.EmailId = txtEmail.Text;
                        snd.ModifiedOn = DateTime.Now;
                        snd.ModifiedBy = Convert.ToString(Session["Emp_Code"]);
                    }

                    try
                    {

                        List<HRMSNewLeaveMaster> leaveid = (from item in ObjDAL.HRMSNewLeaveMasters
                                                            select item).ToList();

                        if (leaveid.Count > 0)
                        {
                            {
                                foreach (var a in leaveid)
                                {
                                    var leav = (from s in ObjDAL.HRMSEmployeeLeaves.Where(item => item.EmpId == Convert.ToInt32(id) && item.LeaveId == Convert.ToInt32(a.Id)) select s).SingleOrDefault();

                                    if (leav != null)
                                    {

                                        leav.EmpId = Convert.ToInt32(id);
                                        leav.LeaveId = a.Id;
                                        leav.NoOfLeaves = Convert.ToInt32(txtearnedleave.Text);
                                        leav.ModifiedOn = DateTime.Now;
                                        leav.ModifiedBy = Convert.ToString(Session["Emp_Code"]);

                                    }

                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }


                    var oid = (from s in ObjDAL.HRMSEmployeeMasterOfficialInformationDetails.Where(x => x.EmpId == Convert.ToString(id)) select s).SingleOrDefault();
                    {
                        oid.DateOfJoining = Convert.ToDateTime(txtDateofJoin.Text);
                        oid.ReportingAuthority = ddlReportingAuthority.SelectedValue;
                        oid.ModifiedOn = DateTime.Now;
                        oid.ModifiedBy = Convert.ToString(Session["Emp_Code"]);
                    }

                    var sd = (from s in ObjDAL.HRMSEmployeeMasterSalaryDetails.Where(x => x.EmpId == Convert.ToString(id)) select s).SingleOrDefault();
                    {
                        sd.BasicAmount = Convert.ToDecimal(txtbasicamount.Text);
                        sd.GrossAmount = Convert.ToDecimal(txtgrossamount.Text);
                        sd.ModifiedOn = DateTime.Now;
                        sd.ModifiedBy = Convert.ToString(Session["Emp_Code"]);
                        try
                        {
                            var earnsal = (from s in ObjDAL.EarningPercentageAmounts.Where(x => x.EmpId == Convert.ToInt32(id)) select s).ToList();
                            {
                                foreach (GridViewRow row in gvdearnings.Rows)
                                {
                                    if (row.RowType == DataControlRowType.DataRow)
                                    {
                                        CheckBox CheckRow = (row.Cells[0].FindControl("chbboxdesignation") as CheckBox);
                                        if (CheckRow.Checked)
                                        {
                                            TextBox valuepercentage = (row.Cells[2].FindControl("txtvalue") as TextBox);
                                            string salvalue = valuepercentage.Text;
                                            Decimal PercentageValue = Convert.ToDecimal(salvalue);

                                            string name = gvdearnings.Rows[row.RowIndex].Cells[1].Text;


                                            TextBox Amount1 = (row.Cells[3].FindControl("txtearningamount") as TextBox);

                                            string salamount = Amount1.Text;
                                            Decimal amount = Convert.ToDecimal(salamount);

                                            earnsal[row.RowIndex].EmpId = Convert.ToInt32(id);
                                            earnsal[row.RowIndex].Value = Convert.ToDecimal(PercentageValue);
                                            earnsal[row.RowIndex].Amount = Convert.ToDecimal(amount);
                                            earnsal[row.RowIndex].Name = Convert.ToString(name);
                                            earnsal[row.RowIndex].ModifiedOn = DateTime.Now;
                                            earnsal[row.RowIndex].ModifiedBy = Convert.ToString(Session["Emp_Code"]);

                                        }

                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    var dd = (from s in ObjDAL.HRMSEmployeeMasterDeductionsDetails.Where(x => x.EmpId == Convert.ToString(id)) select s).SingleOrDefault();
                    {
                        dd.NetAmount = Convert.ToDecimal(txtnetamount.Text);
                        dd.ModifiedOn = DateTime.Now;
                        dd.ModifiedBy = Convert.ToString(Session["Emp_Code"]);
                        try
                        {
                            var earn = (from s in ObjDAL.DeductionPercentageAmounts.Where(x => x.EmpId == Convert.ToInt32(id)) select s).ToList();
                            {
                                foreach (GridViewRow row in gvddeductions.Rows)
                                {
                                    if (row.RowType == DataControlRowType.DataRow)
                                    {
                                        CheckBox CheckRow = (row.Cells[0].FindControl("chboxdeductions") as CheckBox);
                                        if (CheckRow.Checked)
                                        {

                                            TextBox valuepercentage = (row.Cells[2].FindControl("txtdedvalue") as TextBox);
                                            string salvalue = valuepercentage.Text;
                                            Decimal PercentageValue = Convert.ToDecimal(salvalue);

                                            string name = gvddeductions.Rows[row.RowIndex].Cells[1].Text;

                                            string design = gvddeductions.Rows[row.RowIndex].Cells[1].Text;
                                            TextBox Amount1 = (row.Cells[3].FindControl("txtdeductions") as TextBox);

                                            string deductionamount = Amount1.Text;
                                            Decimal amount = Convert.ToDecimal(deductionamount);

                                            earn[row.RowIndex].EmpId = Convert.ToInt32(id);
                                            earn[row.RowIndex].Value = Convert.ToDecimal(PercentageValue);
                                            earn[row.RowIndex].Name = Convert.ToString(name);
                                            earn[row.RowIndex].Amount = Convert.ToDecimal(amount);
                                            earn[row.RowIndex].ModifiedOn = DateTime.Now;
                                            earn[row.RowIndex].ModifiedBy = Convert.ToString(Session["Emp_Code"]);

                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    ObjDAL.SubmitChanges();
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Updated Successfully');window.location ='../../Administrator/AdminDasboard.aspx';", true);
                }
            }
            catch (Exception ex)
            {

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Insert All Details');", true);
            }
        }

        public void BindRole()
        {
            try
            {
                var objHeaderInfo = (from header in ObjDAL.HRMSDepartmentMasters
                                     where header.DepartmentId >= Convert.ToInt32(Session["role"].ToString())
                                     select header).ToList();
                if (objHeaderInfo.Count > 0)
                {
                    ddlUSersubRole.DataSource = objHeaderInfo;
                    ddlUSersubRole.DataTextField = "DepartmentName";
                    ddlUSersubRole.DataValueField = "DepartmentId";
                    ddlUSersubRole.DataBind();
                    ddlUSersubRole.Items.Insert(0, new ListItem("---Select---", "0"));
                }
                else
                {
                    ddlUSersubRole.Items.Insert(0, new ListItem("---Select---", "0"));
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }
        }

        //Binding SubRoles 
        public void BindSubRole()
        {
            try
            {
                var objHeaderInfo = (from header in ObjDAL.HRMSRoleMasters
                                     select header).ToList();
                if (objHeaderInfo.Count > 0)
                {
                    ddluserrole.DataSource = objHeaderInfo;
                    ddluserrole.DataTextField = "RoleName";
                    ddluserrole.DataValueField = "RoleId";
                    ddluserrole.DataBind();
                    ddluserrole.Items.Insert(0, new ListItem("---Select---", "0"));
                }
                else
                {
                    ddluserrole.Items.Insert(0, new ListItem("---Select---", "0"));
                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }
        }





        protected void txtdedvalue_TextChanged(object sender, EventArgs e)
        {
            decimal amount = Convert.ToDecimal(txtgrossamount.Text);
            decimal amount1 = Convert.ToDecimal(txtgrossamount.Text);

            foreach (GridViewRow row in gvddeductions.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox CheckRow = (row.Cells[0].FindControl("chboxdeductions") as CheckBox);
                    if (CheckRow.Checked)
                    {
                        string design = gvddeductions.Rows[row.RowIndex].Cells[1].Text;


                        TextBox t = (TextBox)row.FindControl("txtdeductions");
                        TextBox tb = (TextBox)row.FindControl("txtdedvalue");

                        decimal value = Convert.ToDecimal(tb.Text);
                        t.Text = Convert.ToString(amount1 * value);

                        amount = amount - Convert.ToDecimal(t.Text);



                    }
                    else
                    {

                        string design = gvddeductions.Rows[row.RowIndex].Cells[0].Text;
                        TextBox t = (TextBox)row.FindControl("txtdeductions");

                    }

                }
            }
        }

        protected void chboxdeductions_CheckedChanged(object sender, EventArgs e)
        {
            decimal amount = Convert.ToDecimal(txtgrossamount.Text);
            decimal amount1 = Convert.ToDecimal(txtgrossamount.Text);

            foreach (GridViewRow row in gvddeductions.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox CheckRow = (row.Cells[0].FindControl("chboxdeductions") as CheckBox);
                    if (CheckRow.Checked)
                    {
                        string design = gvddeductions.Rows[row.RowIndex].Cells[1].Text;


                        TextBox t = (TextBox)row.FindControl("txtdeductions");
                        TextBox tb = (TextBox)row.FindControl("txtdedvalue");

                        decimal value = Convert.ToDecimal(tb.Text);
                        t.Text = Convert.ToString(amount1 * value);

                        amount = amount - Convert.ToDecimal(t.Text);
                        txtnetamount.Text = Convert.ToString(amount);



                    }
                    else
                    {

                        string design = gvddeductions.Rows[row.RowIndex].Cells[0].Text;
                        TextBox t = (TextBox)row.FindControl("txtdeductions");
                        t.Text = string.Empty;
                        txtnetamount.Text = amount + t.Text;

                    }

                }
            }
        }

        protected void txtvalue_TextChanged(object sender, EventArgs e)
        {
            decimal amount = Convert.ToDecimal(txtgrossamount.Text);
            decimal amount1 = Convert.ToDecimal(txtgrossamount.Text);

            foreach (GridViewRow row in gvdearnings.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox CheckRow = (row.Cells[0].FindControl("chbboxdesignation") as CheckBox);
                    if (CheckRow.Checked)
                    {
                        string design = gvdearnings.Rows[row.RowIndex].Cells[1].Text;


                        TextBox t = (TextBox)row.FindControl("txtearningamount");
                        TextBox tb = (TextBox)row.FindControl("txtvalue");

                        decimal value = Convert.ToDecimal(tb.Text);
                        t.Text = Convert.ToString(amount1 * value);

                        amount = amount - Convert.ToDecimal(t.Text);



                    }
                    else
                    {

                        string design = gvdearnings.Rows[row.RowIndex].Cells[0].Text;
                        TextBox t = (TextBox)row.FindControl("txtearningamount");

                    }
                }



            }
        }

        protected void chbboxdesignation_CheckedChanged(object sender, EventArgs e)
        {
            decimal amount = Convert.ToDecimal(txtgrossamount.Text);
            decimal amount1 = Convert.ToDecimal(txtgrossamount.Text);

            foreach (GridViewRow row in gvdearnings.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox CheckRow = (row.Cells[0].FindControl("chbboxdesignation") as CheckBox);
                    if (CheckRow.Checked)
                    {


                        TextBox t = (TextBox)row.FindControl("txtearningamount");
                        TextBox tb = (TextBox)row.FindControl("txtvalue");

                        decimal value = Convert.ToDecimal(tb.Text);
                        t.Text = Convert.ToString(amount1 * value);

                        amount = amount - Convert.ToDecimal(t.Text);
                        txtbasicamount.Text = Convert.ToString(amount);



                    }
                    else
                    {

                        string design = gvdearnings.Rows[row.RowIndex].Cells[0].Text;
                        TextBox t = (TextBox)row.FindControl("txtearningamount");
                        t.Text = string.Empty;

                        txtbasicamount.Text = amount + t.Text;



                    }

                }
            }
        }

        protected void txtgrossamount_TextChanged(object sender, EventArgs e)
        {

            decimal amount = Convert.ToDecimal(txtgrossamount.Text);
            txtbasicamount.Text = Convert.ToString(amount);
            txtnetamount.Text = Convert.ToString(amount);
        }

        protected void ddluserrole_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtcasualleave.Visible = false;
            txtearnedleave.Visible = false;

            if ((ddluserrole.SelectedItem.Value == "7") || (ddluserrole.SelectedItem.Value == "8") || (ddluserrole.SelectedItem.Value == "9") || (ddluserrole.SelectedItem.Value == "10"))
            {


                txtcasualleave.Visible = false;
                txtearnedleave.Visible = false;
                lblcasualid.Visible = false;
                lblearnedid.Visible = false;
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Leaves After One Year Applicable..');", true);
            }
            else
            {
                txtcasualleave.Visible = true;
                txtearnedleave.Visible = true;
                lblcasualid.Visible = true;
                lblearnedid.Visible = true;

                if (txtDateofJoin.Text != "")
                {
                    DateTime joindate = DateTime.ParseExact(txtDateofJoin.Text, "yyyy-MM-dd", CultureInfo.InvariantCulture);

                    String seeDate = DateTime.Now.ToString("yyyy-MM-dd");

                    DateTime sodate = DateTime.ParseExact(seeDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);

                    if (joindate < sodate)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Select Valid Date..');", true);
                        txtDateofJoin.Text = "";
                        txtDateofJoin.Focus();

                    }

                    try
                    {
                        String seDate = DateTime.Now.ToString("yyyy-MM-dd");
                        DateTime jointodate = DateTime.ParseExact(txtDateofJoin.Text, "yyyy-MM-dd", CultureInfo.InvariantCulture);

                        String dy = jointodate.Day.ToString();
                        String mn = jointodate.Month.ToString();
                        String yy = jointodate.Year.ToString();

                        DateTime datevalue = (Convert.ToDateTime(seDate.ToString()));

                        String dy1 = datevalue.Day.ToString();
                        String mn1 = datevalue.Month.ToString();
                        String yy1 = datevalue.Year.ToString();

                        if (yy == yy1)
                        {
                            if (15 >= Convert.ToInt32(dy))
                            {
                                int month = Convert.ToInt32(mn);
                                txtcasualleave.Text = Convert.ToString(11 - month);
                                txtearnedleave.Text = Convert.ToString((11 - month) / 3);
                                lblcasualid.Text = "CASUAL Leave";
                                lblearnedid.Text = "Earned Leave";

                            }

                            else
                            {
                                int month = Convert.ToInt32(mn);
                                txtcasualleave.Text = Convert.ToString(12 - month);
                                txtearnedleave.Text = Convert.ToString((12 - month) / 3);
                                lblcasualid.Text = "CASUAL Leave";
                                lblearnedid.Text = "Earned Leave";


                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Select the joining  Date..');", true);
                    txtDateofJoin.Focus();
                }

            }
        }

        protected void txtDateofJoin_TextChanged(object sender, EventArgs e)
        {
            String seeDate = DateTime.Now.ToString("yyyy-MM-dd");

            DateTime sodate = DateTime.ParseExact(seeDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);

            DateTime joindate = DateTime.ParseExact(txtDateofJoin.Text, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            if (joindate < sodate)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Select Valid Date..');", true);
                txtDateofJoin.Text = "";
                txtDateofJoin.Focus();

            }

            try
            {
                String seDate = DateTime.Now.ToString("yyyy-MM-dd");
                DateTime jointodate = DateTime.ParseExact(txtDateofJoin.Text, "yyyy-MM-dd", CultureInfo.InvariantCulture);

                String dy = jointodate.Day.ToString();
                String mn = jointodate.Month.ToString();
                String yy = jointodate.Year.ToString();

                DateTime datevalue = (Convert.ToDateTime(seDate.ToString()));

                String dy1 = datevalue.Day.ToString();
                String mn1 = datevalue.Month.ToString();
                String yy1 = datevalue.Year.ToString();

                if (yy == yy1)
                {
                    if (15 >= Convert.ToInt32(dy))
                    {
                        int month = Convert.ToInt32(mn);
                        txtcasualleave.Text = Convert.ToString(11 - month);
                        txtearnedleave.Text = Convert.ToString((11 - month) / 3);
                    }

                    else
                    {

                        int month = Convert.ToInt32(mn);
                        txtcasualleave.Text = Convert.ToString(12 - month);
                        txtearnedleave.Text = Convert.ToString((12 - month) / 3);


                    }
                }
            }
            catch (Exception ex)
            {

            }
        }



        public void clear()
        {
            txtgrossamount.Text = "";
            txtbasicamount.Text = "";

        }


    }
}