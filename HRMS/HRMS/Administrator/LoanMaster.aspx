﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/Admin.Master" AutoEventWireup="true" CodeBehind="LoanMaster.aspx.cs" Inherits="HRMS.Administrator.LoanMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.js"></script>
     <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.js"></script>
       <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.js"></script>
      <script type="text/javascript">
          $(document).ready(function () {
              $(".txtOnly").keypress(function (e) {
                  var key = e.keyCode;
                  if (key < 48 || (key > 57 && key < 65) || (key > 90 && key < 97) || key > 122 || (key >= 48 && key <= 57)) {
                      e.preventDefault();
                  }
              });
          });

          $(document).ready(function () {
              $(".txtno").keypress(function (e) {
                  var key = e.keyCode;
                  if (keyCode > 31 && (keyCode < 48 || keyCode > 57)) {
                      e.preventDefault();
                  }
              });
          });

    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <section id="main-content">
        <section class="wrapper">

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a></li>
                        <li><a href="LoanMaster.aspx" class="current">Define Loan Types </a></li>
                    </ul>
                </div>
            </div>


            <div class="row">
                <section class="panel">
                    <header class="panel-heading">
                        <%--<i class="fa fa-th-list"> Define Loan Types </i>--%>
                    </header>

                    <div class="panel-body">
                        <form action="#" method="POST" class='form-horizontal form-bordered'>
                            <div class="col-md-12">
                                <div class="row">
                                    <section class="panel">
                                        <header class="panel-heading tab-bg-dark-navy-blue ">
                                            <ul class="nav nav-tabs">
                                                <li class="active">
                                                    <a data-toggle="tab" href="#LoanType" aria-expanded="false"><i class="fa fa-user">Loan Type</i></a>
                                                </li>
                                                <li class="">
                                                    <a data-toggle="tab" href="#List" aria-expanded="true"><i class="fa fa-bars">List</i></a>
                                                </li>
                                            </ul>
                                        </header>
                                        <div class="panel-body">
                                            <div class="tab-content">
                                                <div id="LoanType" class="tab-pane active">

                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                        <ContentTemplate>
                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-md-5">
                                                                     Name<span id="Span1" runat="server" style="color: Red; display: inline;">*</span></label>
                                                                <div class="col-md-7 ">
                                                                    <asp:TextBox ID="txtLoanName" runat="server" TabIndex="0" placeholder="Loan Name" class="form-control txtOnly"
                                                                        MaxLength="50" AutoPostBack="true" required></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-md-5">
                                                                    Short Name<span style="color: #ff0000; font-size: 14px; display: inline;">*</span></label>
                                                                <div class="col-md-7" id="Div1">
                                                                    <asp:TextBox ID="txtShortName" runat="server" TabIndex="1" placeholder="Short Name" class="form-control txtOnly"
                                                                        MaxLength="50" AutoPostBack="true" required></asp:TextBox>
                                                                </div>
                                                            </div>

                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-md-5">
                                                            Tenure<span style="color: #ff0000; font-size: 14px; display: inline;">*</span>
                                                        </label>
                                                        <div class="col-md-7">
                                                            <asp:DropDownList ID="ddltenure" runat="server" class="form-control DropdownCss" required>
                                                                <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                                <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                                <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                                <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                                                <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                                                <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                                                <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                                                <asp:ListItem Text="24" Value="24"></asp:ListItem>

                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <span class="col-md-7" margin-bottom="15px" style="display: inline-block; width: 700%;">
                                                            <label for="ContentPlaceHolder1_chbdesignationlist"></label>
                                                        </span>
                                                        <asp:CheckBox ID="chbdesignationlist" CssClass="col-md-7" OnCheckedChanged="chbdesignationlist_CheckedChanged" runat="server"
                                                            Text=" Apply to Designation" AutoPostBack="true" />
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-md-5" id="laleid" runat="server">
                                                            Elgibility Amount<span style="color: #ff0000; font-size: 14px; display: inline;">*</span></label>
                                                        <div class="col-md-7" id="Div2">
                                                            <asp:TextBox ID="txtelgibityamunt" runat="server" TabIndex="1" placeholder="  Elgibility Amount" class="form-control txtno"
                                                                MaxLength="50" OnTextChanged="txtelgibityamunt_TextChanged" AutoPostBack="true" required></asp:TextBox>
                                                        </div>
                                                    </div>


                                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                        <ContentTemplate>
                                                            <div class="col-md-12">
                                                                <div class="box-body table-responsive text-center ">
                                                                    <div class=" form-group ">
                                                                        <asp:GridView ID="gdvdesignation" runat="server" AutoGenerateColumns="false" Visible="false"  CssClass="mydatagrid" HeaderStyle-CssClass="headergrid" RowStyle-CssClass="rows">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="">
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="chbdesignation" runat="server" AutoPostBack="true" CssClass="" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="RoleName" HeaderText="Designation Name" ItemStyle-Width="150" />
                                                                                <asp:TemplateField HeaderText="Elgibility Amount">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="txtelgibityamountlist" AutoPostBack="true" CssClass="form-control" runat="server">
                                                                                        </asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <div class="col-md-12 text-center">
                                                        <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary" OnClick="btnSave_Click" /><%--OnClientClick="return fileUploadValidation()"--%>
                                                    </div>
                                                </div>


                                                <div id="List" class="tab-pane">

                                                    <div style="max-height: 250px; overflow: auto;" class="table-responsive">
                                                        <asp:GridView ID="gvdlonmaster" CssClass="mydatagrid" HeaderStyle-CssClass="headergrid" RowStyle-CssClass="rows"
                                                            runat="server"  Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%; overflow: scroll;" EmptyDataText="No records Found" AutoGenerateColumns="false" OnSelectedIndexChanged="gvdlonmaster_SelectedIndexChanged" OnRowDataBound="gvdlonmaster_RowDataBound"  >
                                                            <Columns>
                                                                <asp:BoundField DataField="Id" HeaderText="Loan No" ItemStyle-Width="200" />
                                                                <asp:BoundField DataField="LoanName" HeaderText="Loan Name" ItemStyle-Width="200" />
                                                                <asp:BoundField DataField="ShortName" HeaderText="Short Name" ItemStyle-Width="200" />
                                                                <asp:BoundField DataField="Tenure" HeaderText="Tenure" ItemStyle-Width="200" />
                                                                <asp:BoundField DataField="EligibilityAmount" HeaderText="Eligibility Amount" ItemStyle-Width="200" />

                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                    </div>
            </div>
        </section>
    </section>



</asp:Content>
