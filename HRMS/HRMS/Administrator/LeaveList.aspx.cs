﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Administrator
{
    public partial class LeaveList : System.Web.UI.Page
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                approveleavedetails();
            }
           
        }

        public void approveleavedetails()
        {
            var approve = (from item in objDAL.Usp_GettingApprovedleaveDetailsinList()
                           select item).ToList();
            if (approve.Count > 0)
            {
                gdvleaveapproved.DataSource = approve;
                gdvleaveapproved.DataBind();
            }
            else
            {
                gdvleaveapproved.DataSource = approve;
                gdvleaveapproved.DataBind();
            }
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Administrator/AdminDasboard.aspx");
        }
    }
}