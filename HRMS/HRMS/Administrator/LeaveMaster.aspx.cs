﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Administrator
{
    public partial class LeaveMaster : System.Web.UI.Page
    {

        #region Declaration

        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();

     

        #endregion

        #region Methods

        /// <summary>
        /// clearing the details
        /// </summary>
        public void cleartext()
        {
            txtleavename.Text = string.Empty;
            txtShortName.Text = string.Empty;
            txtnoofleaves.Text = string.Empty;


        }

        /// <summary>
        /// getting the leave list details
        /// </summary>
        public void bindgdvleavemaster()
        {
            var leavemaster = (from item in ObjDAL.HRMSNewLeaveMasters
                               select item).ToList();


            if (leavemaster.Count > 0)
            {
                gdvleavemaster.DataSource = leavemaster;
                gdvleavemaster.DataBind();
            }
            else
            {
                gdvleavemaster.DataSource = leavemaster;
                gdvleavemaster.DataBind();
                //        gdvleavemaster.Rows[0].Cells[0].Text = "No Records Found";


            }
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindgdvleavemaster();

            }

        }
        protected void gdvleavemaster_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(gdvleavemaster, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = "View Leave Type Details";
                e.Row.Attributes["style"] = "cursor:pointer";
            }
        }

        /// <summary>
        /// clicking the row it shows the particular details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gdvleavemaster_SelectedIndexChanged(object sender, EventArgs e)
        {
            string id = gdvleavemaster.SelectedRow.Cells[0].Text;
            string path = "~/Administrator/LeaveView.aspx";
            Session["EmployeeDetailsCode"] = id;
            Response.Redirect(path);
        }

        /// <summary>
        /// saving the leave details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {

            try
            {
                HRMSNewLeaveMaster role = new HRMSNewLeaveMaster();
                role.Name = txtleavename.Text;
                role.ShortName = txtShortName.Text;
                role.NoOfLeaves = Convert.ToInt32(txtnoofleaves.Text);
                role.CreatedBy = "";
                role.CreatedOn = DateTime.Now;
                ObjDAL.HRMSNewLeaveMasters.InsertOnSubmit(role);
                ObjDAL.SubmitChanges();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Save Sucessfully');window.location ='../Administrator/AdminDasboard.aspx';", true);
                bindgdvleavemaster();
                cleartext();
            }
            catch
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Try Again');", true);

            }
        }

        #endregion

    }






}

