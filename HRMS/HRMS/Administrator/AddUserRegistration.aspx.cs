﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using BAL;
using DAL;
using System.Configuration;

using System.Globalization;
using System.Web.Services;


namespace HRMS.Administrator
{
    public partial class AddUserRegistration : System.Web.UI.Page
    {

        string objConfig = System.Configuration.ConfigurationManager.ConnectionStrings["dbconnection"].ToString();

        // // /// Object Creation For User Registration Domain
        Administrator_User_RegistrationBAL ObjSCM_UserRegistration = new Administrator_User_RegistrationBAL();

        // // /// Object Creation For Company Info Domain
        Administrator_CompanyInfoBAL ObjSCM_CompanyInfo = new Administrator_CompanyInfoBAL();

        public String StrModules, StrModules1, StrBranch1;


        ERP_DatabaseDataContext objdb = new ERP_DatabaseDataContext();

        // // // // Main Method
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_Type"].ToString() == "Employees")
            {
                Response.Redirect("~/Dashboard/Dashboard.aspx");
            }
            else
            {
                if (!IsPostBack)
                {

                    if (Session["ClientRegistrationNo"] == null)
                    {
                        Response.Redirect("~/Default.aspx");
                    }
                    txtCompanyID.Text = (string)Session["ClientRegistrationNo"];
                    btnUpdate.Visible = false;

                    // // // Bind Company List
                    CompanyList();

                    // // // Bind BranchID
                    //  Bind_BranchList();
                    txtLoginId.Focus();
                    if ((string)Session["EditUserRegistration"] != null)
                    {

                        //  Request.QueryString.Clear();
                        string ClientRegistrationNo = Session["ClientRegistrationNo"].ToString();
                        string LoginID = (string)Session["EditUserRegistration"];
                        //string LoginID = Request.QueryString["id"];
                        Edit_UserDetails(LoginID, ClientRegistrationNo);
                    }

                }
            }
        }

        #region Private Methods


        ///// <summary>
        ///// This Method used for insert user registration Details
        ///// Created By Divya
        ///// </summary>
        ///// <param name="sender"></param>



        public void Saving_Method()
        {


            try
            {


                ObjSCM_UserRegistration.Login_Id = (txtLoginId.Text == "") ? "" : txtLoginId.Text;
                ObjSCM_UserRegistration.Password = (txtPassword.Text == "") ? "" : txtPassword.Text;
                ObjSCM_UserRegistration.User_Name = (txtEmployeeName.Text == "") ? "" : txtEmployeeName.Text;
                ObjSCM_UserRegistration.E_Mail = (txtEmail.Text == "") ? "" : txtEmail.Text;
                ObjSCM_UserRegistration.User_Type = (ddlUserType.SelectedItem.Value == "") ? "" : ddlUserType.SelectedItem.Value;
                ObjSCM_UserRegistration.ClientRegistrationNo = (txtCompanyID.Text == "") ? "" : txtCompanyID.Text;

                ObjSCM_UserRegistration.Status = (ddlStatus.SelectedItem.Value == "") ? "" : ddlStatus.SelectedItem.Value;


                foreach (ListItem li in chlCompanyLists.Items)
                {
                    if (li.Selected == true)
                    {
                        StrModules += li.Text + ",";
                        StrModules1 += li.Value + ",";

                    }

                }
                StrModules1 = StrModules1.Trim(",".ToCharArray());
                StrModules = StrModules.Trim(",".ToCharArray());

                ObjSCM_UserRegistration.AllowCompanyListID = (StrModules1 == "") ? "" : StrModules1;

                ObjSCM_UserRegistration.AllowCompanyList = (StrModules == "") ? "" : StrModules;
                ObjSCM_UserRegistration.Administrator_Create = (txtAdministratorCreate.Text == "") ? "" : txtAdministratorCreate.Text;
                ObjSCM_UserRegistration.Administrator_Modify = (txtAdministratorModify.Text == "") ? "" : txtAdministratorModify.Text;
                ObjSCM_UserRegistration.Administrator_Approve = (txtAdministratorApprove.Text == "") ? "" : txtAdministratorApprove.Text;
                ObjSCM_UserRegistration.Administrator_View = (txtAdministratorView.Text == "") ? "" : txtAdministratorView.Text;
                ObjSCM_UserRegistration.Master_Create = (txtMasterCreate.Text == "") ? "" : txtMasterCreate.Text;
                ObjSCM_UserRegistration.Master_Modify = (txtMasterModify.Text == "") ? "" : txtMasterModify.Text;
                ObjSCM_UserRegistration.Master_Approve = (txtMasterApprove.Text == "") ? "" : txtMasterApprove.Text;
                ObjSCM_UserRegistration.Master_View = (txtMasterView.Text == "") ? "" : txtMasterView.Text;

                ObjSCM_UserRegistration.Transcation_Create = (txtTransactionsCreate.Text == "") ? "" : txtTransactionsCreate.Text;
                ObjSCM_UserRegistration.Transcation_Modify = (txtTransactionsModify.Text == "") ? "" : txtTransactionsModify.Text;
                ObjSCM_UserRegistration.Transcation_Approve = (txtTransactionsApprove.Text == "") ? "" : txtTransactionsApprove.Text;
                ObjSCM_UserRegistration.Transcation_View = (txtTransactionsView.Text == "") ? "" : txtTransactionsView.Text;

                ObjSCM_UserRegistration.Reports_Create = (txtReportsCreate.Text == "") ? "" : txtReportsCreate.Text;

                ObjSCM_UserRegistration.Reports_Modify = (txtReportsModify.Text == "") ? "" : txtReportsModify.Text;
                ObjSCM_UserRegistration.Reports_Approve = (txtReportsApprove.Text == "") ? "" : txtReportsApprove.Text;
                ObjSCM_UserRegistration.Reports_View = (txtReportsView.Text == "") ? "" : txtReportsView.Text;


                ObjSCM_UserRegistration.Creation_Company = Session["ClientRegistrationNo"].ToString();
                ObjSCM_UserRegistration.Created_By = Session["User_Name"].ToString();
                ObjSCM_UserRegistration.Created_Date = System.DateTime.Now;
                ObjSCM_UserRegistration.Modified_By = Session["User_Name"].ToString();
                ObjSCM_UserRegistration.Modified_Date = System.DateTime.Now;
                //if (ddlBranchID.SelectedValue == "ALL")
                //{
                //    string Creation_Company = Session["ClientRegistrationNo"].ToString();
                //    string Company_Name = Session["Creation_Company"].ToString();
                //    var objcompanyBranchID = (from company in objdb.Administrator_CompanyInfos
                //                              where company.Creation_Company == Creation_Company && company.ClientRegistrationNo == Creation_Company
                //                              select company).ToList();
                //      for (int i = 0;  objcompanyBranchID.Count>i; i++)
                //    {

                //        StrBranch1 += objcompanyBranchID[i].Branch_ID + ",";
                //        i++;

                //    }
                //      StrBranch1 = StrModules.Trim(",".ToCharArray());
                //}
                //else
                //{

                //   ObjSCM_UserRegistration.Branch_ID = (ddlBranchID.SelectedValue == "") ? "" : ddlBranchID.SelectedValue;
                //}
                ObjSCM_UserRegistration.Parameter = 1;
                string UserType = (string)Session["User_Type"];

                if (ddlUserType.SelectedValue == "Admin" || ddlUserType.SelectedValue == "User")
                {
                    if (ObjSCM_UserRegistration.Insert_Administrator_UserRegistration() != 0)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('User Registration Data Saved Sucessfully');", true);
                        ClearAllFields();
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('User Registration Data Not Saved');", true);
                    }
                }
                else if (ddlUserType.SelectedValue == "SuperAdmin" && UserType == "SuperAdmin")
                {
                    if (ObjSCM_UserRegistration.Insert_Administrator_UserRegistration() != 0)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('User Registration Data Saved Successfully');", true);
                        ClearAllFields();
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('User Registration Data Not Saved');", true);
                    }
                }
                //if (ddlUserType.SelectedValue == "SuperAdmin")
                else
                {

                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Sorry You Do not have Permissions to Create Super Admin  ..!');", true);

                }

            }
            catch (Exception ex)
            {

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved ');", true);
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            {

            }

        }

        /// <summary>
        /// this method used for Update User Registration Details
        /// Created By Divya
        /// </summary>
        /// <param name="sender"></param>
        /// 
        public void Update_Method()
        {


            try
            {


                ObjSCM_UserRegistration.Login_Id = (txtLoginId.Text == "") ? "" : txtLoginId.Text;
                ObjSCM_UserRegistration.Password = (txtPassword.Text == "") ? "" : txtPassword.Text;
                ObjSCM_UserRegistration.User_Name = (txtEmployeeName.Text == "") ? "" : txtEmployeeName.Text;
                ObjSCM_UserRegistration.E_Mail = (txtEmail.Text == "") ? "" : txtEmail.Text;
                ObjSCM_UserRegistration.User_Type = (ddlUserType.SelectedItem.Value == "") ? "" : ddlUserType.SelectedItem.Value;
                ObjSCM_UserRegistration.ClientRegistrationNo = (txtCompanyID.Text == "") ? "" : txtCompanyID.Text;

                ObjSCM_UserRegistration.Status = (ddlStatus.SelectedItem.Value == "") ? "" : ddlStatus.SelectedItem.Value;


                foreach (ListItem li in chlCompanyLists.Items)
                {
                    if (li.Selected == true)
                    {
                        StrModules += li.Text + ",";
                        StrModules1 += li.Value + ",";

                    }

                }
                StrModules1 = StrModules1.Trim(",".ToCharArray());
                StrModules = StrModules.Trim(",".ToCharArray());

                ObjSCM_UserRegistration.AllowCompanyListID = (StrModules1 == "") ? "" : StrModules1;

                ObjSCM_UserRegistration.AllowCompanyList = (StrModules == "") ? "" : StrModules;
                ObjSCM_UserRegistration.Administrator_Create = (txtAdministratorCreate.Text == "") ? "" : txtAdministratorCreate.Text;
                ObjSCM_UserRegistration.Administrator_Modify = (txtAdministratorModify.Text == "") ? "" : txtAdministratorModify.Text;
                ObjSCM_UserRegistration.Administrator_Approve = (txtAdministratorApprove.Text == "") ? "" : txtAdministratorApprove.Text;
                ObjSCM_UserRegistration.Administrator_View = (txtAdministratorView.Text == "") ? "" : txtAdministratorView.Text;
                ObjSCM_UserRegistration.Master_Create = (txtMasterCreate.Text == "") ? "" : txtMasterCreate.Text;
                ObjSCM_UserRegistration.Master_Modify = (txtMasterModify.Text == "") ? "" : txtMasterModify.Text;
                ObjSCM_UserRegistration.Master_Approve = (txtMasterApprove.Text == "") ? "" : txtMasterApprove.Text;
                ObjSCM_UserRegistration.Master_View = (txtMasterView.Text == "") ? "" : txtMasterView.Text;

                ObjSCM_UserRegistration.Transcation_Create = (txtTransactionsCreate.Text == "") ? "" : txtTransactionsCreate.Text;
                ObjSCM_UserRegistration.Transcation_Modify = (txtTransactionsModify.Text == "") ? "" : txtTransactionsModify.Text;
                ObjSCM_UserRegistration.Transcation_Approve = (txtTransactionsApprove.Text == "") ? "" : txtTransactionsApprove.Text;
                ObjSCM_UserRegistration.Transcation_View = (txtTransactionsView.Text == "") ? "" : txtTransactionsView.Text;

                ObjSCM_UserRegistration.Reports_Create = (txtReportsCreate.Text == "") ? "" : txtReportsCreate.Text;

                ObjSCM_UserRegistration.Reports_Modify = (txtReportsModify.Text == "") ? "" : txtReportsModify.Text;
                ObjSCM_UserRegistration.Reports_Approve = (txtReportsApprove.Text == "") ? "" : txtReportsApprove.Text;
                ObjSCM_UserRegistration.Reports_View = (txtReportsView.Text == "") ? "" : txtReportsView.Text;


                ObjSCM_UserRegistration.Creation_Company = Session["ClientRegistrationNo"].ToString();
                ObjSCM_UserRegistration.Created_By = Session["User_Name"].ToString();
                ObjSCM_UserRegistration.Created_Date = System.DateTime.Now;
                ObjSCM_UserRegistration.Modified_By = Session["User_Name"].ToString();
                ObjSCM_UserRegistration.Modified_Date = System.DateTime.Now;
                //if (ddlBranchID.SelectedValue == "ALL")
                //{
                //    string Creation_Company = Session["ClientRegistrationNo"].ToString();
                //    string Company_Name = Session["Creation_Company"].ToString();
                //    var objcompanyBranchID = (from company in objdb.SCM_Administrator_CompanyInfo_Chailds
                //                              where company.Creation_Company == Creation_Company && company.ClientRegistrationNo == Creation_Company
                //                              select company).ToList();
                //    for (int i = 0; objcompanyBranchID.Count > i; i++)
                //    {

                //        StrBranch1 += objcompanyBranchID[i].Branch_ID + ",";
                //        i++;

                //    }
                //    StrBranch1 = StrModules.Trim(",".ToCharArray());
                //}
                //else
                //{

                //    ObjSCM_UserRegistration.Branch_ID = (ddlBranchID.SelectedValue == "") ? "" : ddlBranchID.SelectedValue;
                //}
                // ObjSCM_UserRegistration.Branch_ID = (ddlBranchID.SelectedValue=="")? "" : ddlBranchID.SelectedValue; 
                ObjSCM_UserRegistration.Parameter = 2;

                string UserType = (string)Session["User_Type"];

                if (ddlUserType.SelectedValue == "Admin" || ddlUserType.SelectedValue == "User")
                {
                    if (ObjSCM_UserRegistration.Insert_Administrator_UserRegistration() != 0)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('User Registration Data Saved Successfully');", true);
                        ClearAllFields();
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('User Registration Data Not Saved');", true);
                    }
                }
                else if (ddlUserType.SelectedValue == "SuperAdmin" && UserType == "SuperAdmin")
                {
                    if (ObjSCM_UserRegistration.Insert_Administrator_UserRegistration() != 0)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('User Registration Data Saved Successfully');", true);
                        ClearAllFields();
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('User Registration Data Not Saved');", true);
                    }
                }

                ////if (ddlUserType.SelectedValue == "SuperAdmin")
                else
                {

                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Sorry You Do not have Permissions to Create Super Admin  ..!');", true);

                }


            }
            catch (Exception ex)
            {

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved ');", true);
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            {

            }

        }



        /// <summary>
        /// Get Data From Company Info Details
        /// Created By Divya
        /// </summary>
        /// <param name="sender"></param>
        /// 
        public void CompanyList()
        {
            try
            {
                string Creation_Company = Session["ClientRegistrationNo"].ToString();
                string Company_Name = Session["CreationCompany"].ToString();

                List<Administrator_CompanyInfo> objCompanyInfoDetails = (from Cmpny in objdb.Administrator_CompanyInfos
                                                                         where Cmpny.Creation_Company == Creation_Company || Cmpny.ClientRegistrationNo == Creation_Company
                                                                         select Cmpny).ToList();
                //List<SCM_Administrator_CompanyInfo> objCompanyInfoDetails = ObjSCM_CompanyInfo.GetData_CompanyInfo(Creation_Company);
                if (objCompanyInfoDetails.Count > 0)
                {

                    chlCompanyLists.DataSource = objCompanyInfoDetails;

                    chlCompanyLists.DataTextField = "Company_Name";
                    chlCompanyLists.DataValueField = "ClientRegistrationNo";
                    chlCompanyLists.DataBind();
                    //ddlProdGroup.Items.Insert(0, new ListItem("--Group--", "0")); //updated code
                }
                else
                {
                    chlCompanyLists.Items.Insert(0, new ListItem(Company_Name, Creation_Company)); //updated code
                }

            }
            catch (Exception ex)
            {
                string Error = ex.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + Error + "');", true);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Get Data From Company Info Details Child
        /// Created By Divya
        /// </summary>
        /// <param name="sender"></param>
        /// 
        //public void Bind_BranchList()
        //{
        //    try
        //    {
        //        string Creation_Company = Session["ClientRegistrationNo"].ToString();
        //        string Company_Name = Session["Creation_Company"].ToString();
        //        var objcompanyBranchID = (from company in objdb.SCM_Administrator_CompanyInfo_Chailds
        //                                  where company.Creation_Company == Creation_Company && company.ClientRegistrationNo == Creation_Company
        //                                  select company).ToList();
        //        List<SCM_Administrator_CompanyInfo_Chaild> objCompanyInfoDetails = ObjSCM_CompanyInfo.GetData_BranchID(Creation_Company);
        //        if (objcompanyBranchID.Count > 0)
        //        {

        //            ddlBranchID.DataSource = objcompanyBranchID;

        //            ddlBranchID.DataTextField = "Branch_ID";
        //            ddlBranchID.DataValueField = "Branch_ID";
        //            ddlBranchID.DataBind();
        //            ddlBranchID.Items.Insert(0, new ListItem("-- Select Branch ID -", "0"));
        //            ddlBranchID.Items.Insert(1, new ListItem("ALL", "ALL"));//updated code
        //            //ddlProdGroup.Items.Insert(0, new ListItem("--Group--", "0")); //updated code
        //        }
        //        else
        //        {
        //            ddlBranchID.Items.Insert(0, new ListItem("-- Select Branch ID -", "0")); //updated code
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        string Error = ex.ToString();
        //        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + Error + "');", true);
        //    }
        //    finally
        //    {
        //    }
        //}

        /// <summary>
        /// Clear fields data into  User RegistrationDetails
        /// Created By Divya
        /// </summary>
        /// <param name="sender"></param>
        public void ClearAllFields()
        {
            txtLoginId.Text = "";
            txtPassword.Text = "";

            txtPassword.Attributes.Add("value", txtPassword.Text);
            txtEmployeeName.Text = "";
            txtEmail.Text = "";

            ddlUserType.SelectedValue = "0";
            ddlStatus.SelectedValue = "0";

            ddlBranchID.ClearSelection();

            txtCompanyID.Text = "";
            txtAdministratorCreate.Text = txtAdministratorModify.Text = txtAdministratorApprove.Text = txtAdministratorView.Text = txtMasterCreate.Text = txtMasterModify.Text = txtMasterApprove.Text = txtMasterView.Text = "";
            txtTransactionsCreate.Text = txtTransactionsModify.Text = txtTransactionsApprove.Text = txtTransactionsView.Text = txtReportsCreate.Text = txtReportsModify.Text = txtReportsApprove.Text = txtReportsView.Text = "";
            for (int i = 0; i < chlCompanyLists.Items.Count; i++)
            {

                chlCompanyLists.Items[i].Selected = false;


            }
            btnSave.Visible = true;
            btnUpdate.Visible = false;
        }

        // // // // Binding The User Details Based On Company Id
        public void Edit_UserDetails(string LoginID, string CreationCompany)
        {
            try
            {
                var objUserRegistrationID = (from UserGet in objdb.Administrator_UserRegistrations
                                             where UserGet.Login_Id == LoginID && UserGet.Creation_Company == CreationCompany
                                             select UserGet).ToList();
                //var objUserRegistrationID = ObjSCM_UserRegistration.Get_Sale_Login(LoginID, CreationCompany);

                // List<Sales_SalesTeam> objSaleTeam_Person = objSaleAllGetMethod.Get_Sale_saleteam_Based_SaleCode(Sales_Team_ID, Creation_Company);
                if (objUserRegistrationID.Count > 0)
                {
                    string[] CmpName = objUserRegistrationID[0].AllowCompanyListID.Split(',');


                    for (int ii = 0; ii < CmpName.Length; ii++)
                    {
                        if (CmpName[ii] == CreationCompany)
                        {
                            txtLoginId.Text = objUserRegistrationID[0].Login_Id.ToString();
                            txtPassword.Text = objUserRegistrationID[0].Password.ToString();
                            txtPassword.Attributes.Add("value", txtPassword.Text);
                            txtEmployeeName.Text = objUserRegistrationID[0].User_Name.ToString();
                            txtEmail.Text = objUserRegistrationID[0].E_Mail;
                            ddlUserType.SelectedValue = objUserRegistrationID[0].User_Type;
                            ddlStatus.SelectedValue = objUserRegistrationID[0].Status;
                            //  ddlStatus.SelectedItem.Text = objUserRegistrationID[0].Status;
                            ddlBranchID.SelectedValue = objUserRegistrationID[0].Branch_ID;

                            StrModules = objUserRegistrationID[0].AllowCompanyList.ToString();

                            if (StrModules.Length > 1)
                            {
                                string[] items = StrModules.Split(',');

                                for (int i = 0; i < chlCompanyLists.Items.Count; i++)
                                {
                                    //    checkedListBox1.SetItemChecked(i, false);//First uncheck the old value!

                                    for (int x = 0; x < items.Length; x++)
                                    {

                                        if (chlCompanyLists.Items[i].Text == items[x])
                                        {
                                            chlCompanyLists.Items[i].Selected = true;
                                        }

                                    }
                                }
                            }

                            txtAdministratorCreate.Text = objUserRegistrationID[0].Administrator_Create;
                            txtAdministratorModify.Text = objUserRegistrationID[0].Administrator_Modify;
                            txtAdministratorApprove.Text = objUserRegistrationID[0].Administrator_Approve;
                            txtAdministratorView.Text = objUserRegistrationID[0].Administrator_View;

                            txtMasterCreate.Text = objUserRegistrationID[0].Master_Create;
                            txtMasterModify.Text = objUserRegistrationID[0].Master_Modify;
                            txtMasterApprove.Text = objUserRegistrationID[0].Master_Approve;
                            txtMasterView.Text = objUserRegistrationID[0].Master_View;

                            txtTransactionsCreate.Text = objUserRegistrationID[0].Transcation_Create;
                            txtTransactionsModify.Text = objUserRegistrationID[0].Transcation_Modify;
                            txtTransactionsApprove.Text = objUserRegistrationID[0].Transcation_Approve;
                            txtTransactionsView.Text = objUserRegistrationID[0].Transcation_View;

                            txtReportsView.Text = objUserRegistrationID[0].Reports_View;


                            btnSave.Visible = false;
                            btnUpdate.Visible = true;
                            Session["EditUserRegistration"] = null;

                        }

                    }


                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('No Data Found ');", true);
                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }

        #endregion

        #region events


        // // to avoid duplicates in loginid
        protected void txtLoginId_OnTextChanged(object sender, EventArgs e)
        {
            string strEdit1 = txtLoginId.Text;

            List<Administrator_UserRegistration> objuser = ObjSCM_UserRegistration.Get_Sale_Login(strEdit1, Session["ClientRegistrationNo"].ToString());
            if (objuser.Count > 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Log In Id Already Exist');", true);
                txtLoginId.Text = "";
                txtLoginId.Focus();

            }
            else
            {
                txtPassword.Focus();
            }


        }

        #endregion


        #region clickevents
        /// <summary>
        /// Created By Divya
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        // // To save the data entered
        protected void btnSave_Click(object sender, EventArgs e)
        {
            Saving_Method();
        }

        /// <summary>
        /// Created By Divya
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
         // // To save the data entered
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Update_Method();
        }

        #endregion

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearAllFields();
        }



        #region Auto Fill Administrator Form Details


        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetCompletionList(string prefixText, int count)
        {
            using (SqlConnection con = new SqlConnection())
            {
                con.ConnectionString = ConfigurationManager.ConnectionStrings["dbconnection"].ConnectionString;

                using (SqlCommand com = new SqlCommand())
                {
                    com.CommandText = "select Administrator from SCM_Administrator_UserPermission where " + "Administrator like @Search + '%'";

                    com.Parameters.AddWithValue("@Search", prefixText);
                    com.Connection = con;
                    con.Open();
                    List<string> countryNames = new List<string>();
                    using (SqlDataReader sdr = com.ExecuteReader())
                    {
                        //if (sdr.Read())
                        //{
                        while (sdr.Read())
                        {

                            countryNames.Add(sdr["Administrator"].ToString());

                        }
                        //}
                        //else
                        //{
                        //    countryNames.Add("No Record");
                        //}

                    }
                    con.Close();
                    return countryNames;


                }

            }

        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetMasterCompletionList(string prefixText, int count)
        {
            using (SqlConnection con = new SqlConnection())
            {
                con.ConnectionString = ConfigurationManager.ConnectionStrings["dbconnection"].ConnectionString;

                using (SqlCommand com = new SqlCommand())
                {
                    com.CommandText = "select Masters from SCM_Administrator_UserPermission where " + "Masters like @Search + '%'";

                    com.Parameters.AddWithValue("@Search", prefixText);
                    com.Connection = con;
                    con.Open();
                    List<string> countryNames = new List<string>();
                    using (SqlDataReader sdr = com.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            countryNames.Add(sdr["Masters"].ToString());
                        }
                    }
                    con.Close();
                    return countryNames;


                }

            }

        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetTransactionsCompletionList(string prefixText, int count)
        {
            using (SqlConnection con = new SqlConnection())
            {
                con.ConnectionString = ConfigurationManager.ConnectionStrings["dbconnection"].ConnectionString;

                using (SqlCommand com = new SqlCommand())
                {
                    com.CommandText = "select Transactions from SCM_Administrator_UserPermission where " + "Transactions like @Search + '%'";

                    com.Parameters.AddWithValue("@Search", prefixText);
                    com.Connection = con;
                    con.Open();
                    List<string> countryNames = new List<string>();
                    using (SqlDataReader sdr = com.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            countryNames.Add(sdr["Transactions"].ToString());
                        }
                    }
                    con.Close();
                    return countryNames;


                }

            }

        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetReportsCompletionList(string prefixText, int count)
        {
            using (SqlConnection con = new SqlConnection())
            {
                con.ConnectionString = ConfigurationManager.ConnectionStrings["dbconnection"].ConnectionString;

                using (SqlCommand com = new SqlCommand())
                {
                    com.CommandText = "select Reports from SCM_Administrator_UserPermission where " + "Reports like @Search + '%'";

                    com.Parameters.AddWithValue("@Search", prefixText);
                    com.Connection = con;
                    con.Open();
                    List<string> countryNames = new List<string>();
                    using (SqlDataReader sdr = com.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            countryNames.Add(sdr["Reports"].ToString());
                        }
                    }
                    con.Close();
                    return countryNames;


                }

            }

        }

        #endregion
    }
}