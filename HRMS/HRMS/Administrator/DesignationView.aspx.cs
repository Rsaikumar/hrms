﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Administrator
{
    public partial class DesignationView : System.Web.UI.Page
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                divhide.Visible = false;
                if ((string)Session["EmployeeDetailsCode"] != null)
                {

                    string empcode1 = (string)Session["EmployeeDetailsCode"];

                    Edit_Designation_Details(empcode1);
                }
                else
                {

                }
            }
        }
        public void Edit_Designation_Details(string empcode)
        {
            try
            {
                int ID = Convert.ToInt32(empcode);
                List<HRMSRoleMaster> objEmp = (from master in objDAL.HRMSRoleMasters
                                                                                 where master.Id == ID
                                                                                 select master).ToList();

                if (objEmp.Count > 0)
                {
                    txtdesignationid.Text = Convert.ToString(objEmp[0].Id);
                    txtDesignationname.Text = Convert.ToString(objEmp[0].RoleName);


                   
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }

       

        protected void btnUpdate_Click1(object sender, EventArgs e)
        {
            try
            {

                string empcode = (string)Session["EmployeeDetailsCode"];
                int empid = Convert.ToInt32(empcode);

                var role = (from s in objDAL.HRMSRoleMasters.Where(x => x.Id == Convert.ToInt32(empcode)) select s).SingleOrDefault();
                {
                    role.RoleName = txtDesignationname.Text;
                    role.Id = Convert.ToInt32( txtdesignationid.Text);
                    role.ModifiedOn = DateTime.Now;
                    role.ModifiedBy = Convert.ToString(Session["Emp_Code"]);
                    objDAL.SubmitChanges();
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Updated Sucessfully');window.location ='../Administrator/AdminDasboard.aspx';", true);

                }
            }
            catch
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Try Again');", true);

            }
        }

        protected void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                string empcode = (string)Session["EmployeeDetailsCode"];
                int empid = Convert.ToInt32(empcode);

                var address = (from s in objDAL.HRMSRoleMasters.Where(x => x.Id == empid) select s).SingleOrDefault();
                objDAL.HRMSRoleMasters.DeleteOnSubmit(address);
                objDAL.SubmitChanges();



                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Removed Sucessfully');window.location ='../Administrator/AdminDasboard.aspx';", true);

            }


            catch
            {
            }
        }

        protected void btncancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Administrator/DesignationMaster.aspx");

        }

    }

}
