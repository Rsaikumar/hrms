﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/Admin.Master" AutoEventWireup="true" CodeBehind="LeaveMaster.aspx.cs" Inherits="HRMS.Administrator.LeaveMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.js"></script>
       <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.js"></script>
      <script type="text/javascript">
          $(document).ready(function () {
              $(".txtOnly").keypress(function (e) {
                  var key = e.keyCode;
                  if (key < 48 || (key > 57 && key < 65) || (key > 90 && key < 97) || key > 122 || (key >= 48 && key <= 57)) {
                      e.preventDefault();
                  }
              });
          });

          $(document).ready(function () {
              $(".txtno").keypress(function (e) {
                  var key = e.keyCode;
                  if (keyCode > 31 && (keyCode < 48 || keyCode > 57)) {
                      e.preventDefault();
                  }
              });
          });

    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a></li>
                        <li><a href="LoanMaster.aspx" class="current">Define Leave Types </a></li>
                    </ul>
                </div>
            </div>


            <div class="row">
                <section class="panel">
                    <header class="panel-heading">
                        <%--<i class="fa fa-th-list"> Define Loan Types </i>--%>
                    </header>

                    <div class="panel-body">
                        <form action="#" method="POST" class='form-horizontal form-bordered'>
                            <div class="col-md-12">
                                <div class="row">
                                    <section class="panel">
                                        <header class="panel-heading tab-bg-dark-navy-blue ">
                                            <ul class="nav nav-tabs">
                                                <li class="active">
                                                    <a data-toggle="tab" href="#LeaveType" aria-expanded="false"><i class="fa fa-user">Leave Type</i></a>
                                                </li>
                                                <li class="">
                                                    <a data-toggle="tab" href="#List" aria-expanded="true"><i class="fa fa-bars">List</i></a>
                                                </li>
                                            </ul>
                                        </header>

                                        <div class="panel-body">
                                            <div class="tab-content">
                                                <div id="LeaveType" class="tab-pane active">
                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                        <ContentTemplate>
                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-md-5">
                                                                    Leave Name<span id="Span1" runat="server" style="color: Red; display: inline;">*</span></label>
                                                                <div class="col-md-7 ">
                                                                    <asp:TextBox ID="txtleavename" runat="server" TabIndex="0" placeholder="Leave Name" class="form-control txtOnly"
                                                                        MaxLength="50" AutoPostBack="true" required></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-md-5">
                                                                    Short Name<span style="color: #ff0000; font-size: 14px; display: inline;">*</span></label>
                                                                <div class="col-md-7" id="Div1">
                                                                    <asp:TextBox ID="txtShortName" runat="server" TabIndex="1" placeholder="Short Name" class="form-control txtOnly"
                                                                        MaxLength="50" AutoPostBack="true" required></asp:TextBox>
                                                                </div>
                                                            </div>

                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>

                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-md-5" id="laleid" runat="server">
                                                            No Of Leaves<span style="color: #ff0000; font-size: 14px; display: inline;">*</span></label>
                                                        <div class="col-md-7" id="Div2">
                                                            <asp:TextBox ID="txtnoofleaves" runat="server" TabIndex="1" placeholder="No Of Leave" class="form-control txtno"
                                                                MaxLength="50" AutoPostBack="true" required></asp:TextBox>
                                                        </div>
                                                    </div>
                                                     <div class="col-md-12 text-center">
                                                    <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary" OnClick="btnSave_Click" /><%--OnClientClick="return fileUploadValidation()"--%>
                                                <%--    <asp:Button ID="btndelete" runat="server" Text="Delete" class="btn btn-primary" OnClick="btndelete_Click" /><%--OnClientClick="return fileUploadValidation()"--%>

                                                </div>
                                                </div>

                                                <div id="List" class="tab-pane">

                                                    <div style="max-height: 515px; overflow: auto;" class="table-responsive">
                                                        <asp:GridView ID="gdvleavemaster" CssClass="mydatagrid" HeaderStyle-CssClass="headergrid" RowStyle-CssClass="rows"
                                                            runat="server" Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%; overflow: scroll;" EmptyDataText="No records Found" AutoGenerateColumns="false" OnSelectedIndexChanged="gdvleavemaster_SelectedIndexChanged" OnRowDataBound="gdvleavemaster_RowDataBound">
                                                            <Columns>
                                                                <asp:BoundField DataField="Id" HeaderText="Leave No" ItemStyle-Width="200" />
                                                                <asp:BoundField DataField="Name" HeaderText="Leave Name" ItemStyle-Width="200" />
                                                                <asp:BoundField DataField="ShortName" HeaderText="Short Type" ItemStyle-Width="200" />
                                                                <asp:BoundField DataField="NoOfLeaves" HeaderText="No Of Leaves" ItemStyle-Width="200" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                               
                                            </div>

                                        </div>
                                    </section>
                                </div>
                            </div>


                        </form>
                    </div>
                </section>
            </div>
        </section>
    </section>
</asp:Content>
