﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


#region Private Using
using DAL;
using BAL;
using System.Configuration;
using System.Data;
using System.Reflection;
using System.Data.Linq.SqlClient;
// // // Name Space For BAL & DAL ClassLibrary
#endregion

namespace SCM.Administrator
{
    public partial class UserRegistration : System.Web.UI.Page
    {
        // // // Object Creation For Database
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();



        // // /// Object Creation For Item Group Domain
        Administrator_User_RegistrationBAL objSCM_userRegistration = new Administrator_User_RegistrationBAL();

        // // // // Main Method

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_Type"].ToString() == "Employees")
            {
                Response.Redirect("~/Dashboard/Dashboard.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    if (Session["ClientRegistrationNo"] == null)
                    {
                        Response.Redirect("~/Default.aspx");
                    }
                    txtSearch.Focus();

                    //Bind User Registration
                    Bind_Grid();
                }
            }



        }

        #region Button Events

        // // // Navigation to User Registration Form For Created New User 
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            // if ((string)Session["User_Type"] == "SuperAdmin")
            // {
            Response.Redirect("AddUserRegistration.aspx");
            // }

            //  else
            //  {
            //     AdminPermissionAdd();
            // }
        }

        /// <summary>
        /// Search For UserRegistration Details
        /// Created By Divya
        /// </summar
        /// 

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {

                string StrSearch = txtSearch.Text;
                string CompanyName = Session["ClientRegistrationNo"].ToString();
                List<Administrator_UserRegistration> objProductSearch = objSCM_userRegistration.GetData_search_UserRegistration(StrSearch, CompanyName);

                if (objProductSearch.Count > 0)
                {
                    grdUserRegistration.DataSource = objProductSearch;
                    grdUserRegistration.DataBind();
                }
                else
                {
                    SetInitialRow();
                }


            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(\"" + Error + "\");", true);

            }
            finally
            {

            }
        }

        /// <summary>
        /// Navigation for User Registration create
        /// Created By Divya
        /// </summar
        /// 

        protected void lkbUserRegistration_Click(object sender, EventArgs e)
        {
            // if ((string)Session["User_Type"] == "SuperAdmin")
            //  {
            Response.Redirect("AddUserRegistration.aspx");
            // }

            // else
            // {
            // AdminPermissionAdd();
            // }
        }


        #endregion

        #region Methods

        /// <summary>
        /// Bind Grid Based on User Registration Details
        /// Created By Divya
        /// </summary>
        /// <param name="sender"></param>
        public void Bind_Grid()
        {
            try
            {
                string Cmpy = (string)Session["ClientRegistrationNo"];
                // string Brnach = (string)Session["BranchID"];
                string UserType = (string)Session["User_Type"];
                string UserName = (string)Session["User_Name"];
                string Cration_Company = (string)Session["CreationCompany"];
                //if (UserType == "User")
                //{
                //    //var objUSerInfo = (from UserGet in objDAL.Administrator_UserRegistrations
                //    //                   where UserGet.Branch_ID == Brnach && UserGet.Creation_Company == Cration_Company && UserGet.User_Type == UserType
                //    //                   && UserGet.Login_Id == UserName
                //    //                   //&& UserGet.AllowCompanyListID == Cmpy
                //    //                   select UserGet).ToList();
                //    var objUSerInfo = (from UserGet in objDAL.Administrator_UserRegistrations
                //                       where 
                //                       //UserGet.User_Type == UserType
                //                       //&& UserGet.Login_Id == UserName
                //                       //&&
                //                       UserGet.AllowCompanyListID == Cmpy
                //                       select UserGet).ToList();
                //    // List<Administrator_UserRegistration> ObjuserInfo = objSCM_userRegistration.Get_userRegDetails(Session["ClientRegistrationNo"].ToString());
                //    if (objUSerInfo.Count > 0)
                //    {
                //        string[] CmpName = objUSerInfo[0].AllowCompanyListID.Split(',');


                //        for (int i = 0; i < CmpName.Length; i++)
                //        {
                //            if (CmpName[i] == Cmpy)
                //            {
                //                grdUserRegistration.DataSource = objUSerInfo;
                //                grdUserRegistration.DataBind();
                //            }

                //        }

                //    }
                //    else
                //    {
                //        // // // // Grid view using setting initial rows displaying Empty Header Displying
                //        SetInitialRow();
                //    }
                //}
                //else if (UserType == "Admin")
                //{
                //    //var objUSerInfo = (from UserGet in objDAL.Administrator_UserRegistrations
                //    //                   where UserGet.Branch_ID == Brnach && UserGet.Creation_Company == Cration_Company && UserGet.User_Type == UserType || UserGet.User_Type == "User" || UserGet.AllowCompanyListID == Cmpy
                //    //                   select UserGet).ToList();
                //    var objUSerInfo = (from UserGet in objDAL.Administrator_UserRegistrations
                //                       where  UserGet.ClientRegistrationNo == Cmpy && UserGet.User_Type == UserType || UserGet.User_Type == "User" 
                //                       select UserGet).ToList();
                //    // List<Administrator_UserRegistration> ObjuserInfo = objSCM_userRegistration.Get_userRegDetails(Session["ClientRegistrationNo"].ToString());
                //    if (objUSerInfo.Count > 0)
                //    {
                //        string[] CmpName = objUSerInfo[0].AllowCompanyListID.Split(',');


                //        for (int i = 0; i < CmpName.Length; i++)
                //        {
                //            if (CmpName[i] == Cmpy)
                //            {
                //                grdUserRegistration.DataSource = objUSerInfo;
                //                grdUserRegistration.DataBind();
                //            }

                //        }
                //    }
                //    else
                //    {
                //        // // // // Grid view using setting initial rows displaying Empty Header Displying
                //        SetInitialRow();
                //    }
                //}
                //else if (UserType == "SuperAdmin")
                //{
                var objUSerInfo = (from UserGet in objDAL.Administrator_UserRegistrations
                                   where UserGet.ClientRegistrationNo == Cmpy && UserGet.User_Type == UserType || UserGet.User_Type == "User" || UserGet.User_Type == "Admin"
                                   select UserGet).ToList();
                // List<Administrator_UserRegistration> ObjuserInfo = objSCM_userRegistration.Get_userRegDetails(Session["ClientRegistrationNo"].ToString());
                if (objUSerInfo.Count > 0)
                {
                    string[] CmpName = objUSerInfo[0].AllowCompanyListID.Split(',');


                    for (int i = 0; i < CmpName.Length; i++)
                    {
                        if (CmpName[i] == Cmpy || objUSerInfo[i].Creation_Company == Cmpy)
                        {
                            grdUserRegistration.DataSource = objUSerInfo;
                            grdUserRegistration.DataBind();
                        }

                    }
                }
                else
                {
                    // // // // Grid view using setting initial rows displaying Empty Header Displying
                    SetInitialRow();
                }
                //}
                //else
                //{
                //    // // // // Grid view using setting initial rows displaying Empty Header Displying
                //    SetInitialRow();
                //}
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(\"" + Error + "\");", true);

            }
            finally
            {

            }
        }


        /// <summary>
        /// Setting Initial Empty Row Data Into User Registration Details 
        /// Created By Divya
        /// </summary>
        /// <param name="sender"></param>
        public void SetInitialRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            //dt.Columns.Add(new DataColumn("Set_Default_Addrs", typeof(bool)));
            dt.Columns.Add(new DataColumn("Login_Id", typeof(string)));
            dt.Columns.Add(new DataColumn("User_Name", typeof(string)));
            dt.Columns.Add(new DataColumn("User_Type", typeof(string)));
            //dt.Columns.Add(new DataColumn("City", typeof(string)));

            //dt.Columns.Add(new DataColumn("State", typeof(string)));
            dt.Columns.Add(new DataColumn("Status", typeof(string)));
            // dt.Columns.Add(new DataColumn("Max_Stock", typeof(string)));
            // dt.Columns.Add(new DataColumn("Min_Stock", typeof(string)));

            dr = dt.NewRow();
            //dr["Set_Default_Addrs"] = DBNull.Value;
            dr["Login_Id"] = string.Empty;
            dr["User_Name"] = string.Empty;
            dr["User_Type"] = string.Empty;

            // dr["City"] = string.Empty;
            dr["Status"] = string.Empty;

            // dr["Max_Stock"] = string.Empty;
            //  dr["Min_Stock"] = string.Empty;

            dt.Rows.Add(dr);

            grdUserRegistration.DataSource = dt;
            grdUserRegistration.DataBind();

            int columncount = 7;
            grdUserRegistration.Rows[0].Cells.Clear();
            grdUserRegistration.Rows[0].Cells.Add(new TableCell());
            grdUserRegistration.Rows[0].Cells[0].ColumnSpan = columncount;
            grdUserRegistration.Rows[0].Cells[0].Text = "No Records Found";
            //  grdStorageLocation.HeaderStyle.HorizontalAlign = "center";
            grdUserRegistration.Rows[0].Cells[0].ForeColor = System.Drawing.Color.Red;
        }

        /// <summary>
        ///  The Following Method is used for Delete Grid records to Data From User Registration  Table
        ///  Created By Divya
        /// </summary>
        public void Delete_UserRegistrationDetails(string LogID, string Creation_Company)
        {
            try
            {
                Administrator_UserRegistration a = objSCM_userRegistration.Delete_UserDetails(LogID, Session["ClientRegistrationNo"].ToString());

                if (a.Login_Id != null)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('User Details Deleted Successfully ');", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('User Details Already Existing In Products ');", true);
                }
                Bind_Grid();
                txtSearch.Focus();

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.ToString() + "');", true);
            }
            finally
            {
            }
        }

        #endregion

        #region User Registration Details Bind For Grid view


        /// <summary>
        /// Row Command Grid view  For UserRegistration Details
        /// Created By Divya
        /// </summar
        /// 

        protected void grdUserRegistration_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string LoginID = e.CommandArgument.ToString();

            string Cmpy = (string)Session["ClientRegistrationNo"];
            // string Brnach = (string)Session["BranchID"];
            string UserType = (string)Session["User_Type"];
            string UserName = (string)Session["User_Name"];
            string Cration_Company = (string)Session["CreationCompany"];

            if (e.CommandName == "Edit")
            {

                if (UserType == "SuperAdmin")
                {
                    string path = "AddUserRegistration.aspx";
                    Session["EditUserRegistration"] = LoginID;
                    Response.Redirect(path);
                }
                else
                {


                    //string path = "AddUserRegistration.aspx?id=" + LoginID;
                    //string path = "AddUserRegistration.aspx";
                    //Session["EditUserRegistration"] = LoginID;
                    //Response.Redirect(path);
                    AdminPermissionModify(LoginID);
                }

            }
            if (e.CommandName == "Delete")
            {
                if (UserType == "SuperAdmin")
                {
                    string Creation_Company = Session["ClientRegistrationNo"].ToString();

                    //Delete Method For User Registration 
                    Delete_UserRegistrationDetails(LoginID, Creation_Company);

                    //string path = "AddUserRegistration.aspx";
                    //Session["EditUserRegistration"] = LoginID;
                    //Response.Redirect(path);
                }
                else
                {
                    //Checking Permissions For User Registrtation Delete
                    //Delete Method For User Registration 
                    AdminPermissionDelete(LoginID);
                }

            }


        }


        /// <summary>
        /// Paging For User Registration Details
        /// Created By Divya
        /// </summar
        /// 
        protected void grdUserRegistration_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdUserRegistration.PageIndex = e.NewPageIndex;
            Bind_Grid();
        }


        /// <summary>
        /// Row Delte Grid view  For UserRegistration
        /// Created By Divya
        /// </summar
        /// 
        protected void grdUserRegistration_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        /// <summary>
        /// Row Editing Grid view  For UserRegistration
        /// Created By Divya
        /// </summar
        /// 

        protected void grdUserRegistration_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        /// <summary>
        /// Row Sorting Grid view  For UserRegistration Details
        /// Created By Divya
        /// </summar
        /// 

        protected void grdUserRegistration_Sorting(object sender, GridViewSortEventArgs e)
        {
            string Sortdir = GetSortDirection(e.SortExpression);
            string SortExp = e.SortExpression;
            String Creation_Company = Session["ClientRegistrationNo"].ToString();
            List<Administrator_UserRegistration> list = objSCM_userRegistration.Get_UserRegistration(Creation_Company);


            if (Sortdir == "ASC")
            {
                list = Sort<Administrator_UserRegistration>(list, SortExp, SortDirection.Ascending);
            }
            else
            {
                list = Sort<Administrator_UserRegistration>(list, SortExp, SortDirection.Descending);
            }
            this.grdUserRegistration.DataSource = list;
            this.grdUserRegistration.DataBind();
        }

        private List<Administrator_UserRegistration> Sort<TKey>(List<Administrator_UserRegistration> list, string sortBy, SortDirection direction)
        {
            PropertyInfo property = list.GetType().GetGenericArguments()[0].GetProperty(sortBy);
            if (direction == SortDirection.Ascending)
            {
                return list.OrderBy(e => property.GetValue(e, null)).ToList<Administrator_UserRegistration>();
            }
            else
            {
                return list.OrderByDescending(e => property.GetValue(e, null)).ToList<Administrator_UserRegistration>();
            }
        }

        //  GetSortDirection Method it using PRoduct Grid Sorting  
        private string GetSortDirection(string column)
        {
            string sortDirection = "ASC";
            string sortExpression = ViewState["SortExpression"] as string;
            if (sortExpression != null)
            {
                if (sortExpression == column)
                {
                    string lastDirection = ViewState["SortDirection"] as string;
                    if ((lastDirection != null) && (lastDirection == "ASC"))
                    {
                        sortDirection = "DESC";
                    }
                }
            }
            ViewState["SortDirection"] = sortDirection;
            ViewState["SortExpression"] = column;
            return sortDirection;
        }

        #endregion



        #region Permissions

        // // // Add Functioanlity Permissions
        public void AdminPermissionAdd()
        {
            try
            {
                string Cmpy = (string)Session["ClientRegistrationNo"];
                string Brnach = (string)Session["BranchID"];
                string UserType = (string)Session["User_Type"];
                string UserName = (string)Session["User_Name"];
                var objUserPermissions = (from UserPerm in objDAL.Administrator_UserRegistrations
                                          where UserPerm.Branch_ID == Brnach
                                          && UserPerm.Login_Id == UserName && UserPerm.User_Type == UserType
                                          select UserPerm).ToList();
                if (objUserPermissions.Count > 0)
                {
                    string[] CmpName = objUserPermissions[0].AllowCompanyListID.Split(',');
                    int count = 0;

                    for (int i = 0; i < CmpName.Length; i++)
                    {
                        if (CmpName[i] == Cmpy)
                        {
                            string[] AllowCreate = objUserPermissions[0].Administrator_Create.Split(',');

                            for (int j = 0; j < AllowCreate.Length; j++)
                            {



                                string abc = AllowCreate[j];
                                switch (abc)
                                {
                                    case "User Registration":
                                        {
                                            count = count + 1;
                                            break;
                                        }

                                    case "":
                                        {
                                            //   count = count + 1;
                                            break;
                                        }


                                }

                            }

                        }

                    }
                    if (count > 0)
                    {
                        Response.Redirect("AddUserRegistration.aspx");
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Sorry You Do not have Permissions for Creating new User Registration  ..!');", true);
                    }
                }


            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        // // // Modify Functionality Permissions
        public void AdminPermissionModify(string strEdit)
        {
            try
            {
                string Cmpy = (string)Session["ClientRegistrationNo"];
                string Brnach = (string)Session["BranchID"];
                string UserType = (string)Session["User_Type"];
                string UserName = (string)Session["User_Name"];
                var objUserPermissions = (from UserPerm in objDAL.Administrator_UserRegistrations
                                          where
                                          //UserPerm.Branch_ID == Brnach
                                          //&&
                                          UserPerm.Login_Id == UserName && UserPerm.User_Type == UserType
                                          select UserPerm).ToList();
                if (objUserPermissions.Count > 0)
                {
                    string[] CmpName = objUserPermissions[0].AllowCompanyListID.Split(',');
                    int count = 0;

                    for (int i = 0; i < CmpName.Length; i++)
                    {
                        if (CmpName[i] == Cmpy)
                        {
                            string[] AllowCreate = objUserPermissions[0].Administrator_Modify.Split(',');

                            for (int j = 0; j < AllowCreate.Length; j++)
                            {


                                string abc = AllowCreate[j];
                                switch (abc)
                                {
                                    case "User Registration":
                                        {
                                            count = count + 1;
                                            break;
                                        }

                                    case "":
                                        {
                                            // count = count + 1;
                                            break;
                                        }


                                }

                            }

                        }

                    }
                    if (count > 0)
                    {
                        string path = "AddUserRegistration.aspx";
                        Session["EditUserRegistration"] = strEdit;
                        //string path = "AddUserRegistration.aspx?id=" + strEdit;
                        Response.Redirect(path);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Sorry You Do not have Permissions for Editing User Registration Details..!');", true);
                    }
                }


            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(\"" + Error + "\");", true);

            }
            finally
            {

            }
        }

        // // // Delete Functionality Permissions
        public void AdminPermissionDelete(string strDelete)
        {
            try
            {
                string Cmpy = (string)Session["ClientRegistrationNo"];
                string Brnach = (string)Session["BranchID"];
                string UserType = (string)Session["User_Type"];
                string UserName = (string)Session["User_Name"];
                var objUserPermissions = (from UserPerm in objDAL.Administrator_UserRegistrations
                                          where
                                          //UserPerm.Branch_ID == Brnach
                                          //&& 
                                          UserPerm.Login_Id == UserName && UserPerm.User_Type == UserType
                                          select UserPerm).ToList();
                if (objUserPermissions.Count > 0)
                {
                    string[] CmpName = objUserPermissions[0].AllowCompanyListID.Split(',');
                    int count = 0;

                    for (int i = 0; i < CmpName.Length; i++)
                    {
                        if (CmpName[i] == Cmpy)
                        {
                            string[] AllowCreate = objUserPermissions[0].Administrator_Modify.Split(',');

                            for (int j = 0; j < AllowCreate.Length; j++)
                            {
                                //if (AllowCreate[j] == "Company Information")
                                //{
                                //    Response.Redirect("AddCompanyInformation.aspx");
                                //}



                                string abc = AllowCreate[j];
                                switch (abc)
                                {
                                    case "User Registration":
                                        {
                                            count = count + 1;
                                            break;
                                        }

                                    case "":
                                        {
                                            //count = count + 1;
                                            break;
                                        }


                                }

                            }

                        }

                    }
                    if (count > 0)
                    {
                        string Creation_Company = Session["ClientRegistrationNo"].ToString();

                        Delete_UserRegistrationDetails(strDelete, Creation_Company);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Sorry You Do not have Permissions For Deleteing User Registration Details..!');", true);
                    }
                }


            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(\"" + Error + "\");", true);

            }
            finally
            {

            }
        }
        #endregion




    }
}