﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using System.Web.UI.WebControls;


namespace HRMS.Administrator
{
    public partial class DepartmentWisePayslip : System.Web.UI.Page
    {
        static ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindRole();
            }
        }

        public void BindRole()
        {
            try
            {
                var objHeaderInfo = (from header in ObjDAL.HRMSDepartmentMasters
                                     where header.DepartmentId >= Convert.ToInt32(Session["role"].ToString())
                                     select header).ToList();
                if (objHeaderInfo.Count > 0)
                {
                    ddldepartnemt.DataSource = objHeaderInfo;
                    ddldepartnemt.DataTextField = "DepartmentName";
                    ddldepartnemt.DataValueField = "DepartmentId";
                    ddldepartnemt.DataBind();
                    ddldepartnemt.Items.Insert(0, new ListItem("---Select---", "0"));
                }
                else
                {
                    ddldepartnemt.Items.Insert(0, new ListItem("---Select---", "0"));
                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }

        }
        public class EmployeePaySlip
        {
            public List<EmployeeInfo> EmployeeInfolist { get; set; }
        }

        public class EmployeeInfo
        {
            public string Name { get; set; }
            public int MonthlyDays { get; set; }
            public int EmployeeId { get; set; }
            public int WorkingDays { get; set; }
            public int AbsentDays { get; set; }
            public int ApprovedLeaves { get; set; }
            public decimal BasicAmount { get; set; }
            public decimal NetAmount { get; set; }
            public decimal GrossAmount { get; set; }
            public decimal OT { get; set; }
            public string OThour { get; set; }
            public decimal TotalOTAmount { get; set; }
            public decimal LOP { get; set; }
            public decimal EarningTotal { get; set; }
            public decimal DeductionTotal { get; set; }
            public List<Earnings> Earnings { get; set; }
            public List<Deduction> Deductions { get; set; }
        }

        public class Earnings
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public decimal Amount { get; set; }
        }
        public class Deduction
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public decimal Amount { get; set; }
        }

        private static List<EmployeeInfo> GetEmployeesData(int depid, int month)
        {
            var employeepaySlip = new List<EmployeeInfo>();


            var employees = ObjDAL.HRMSEmployeeMasterRegistrationDetails.Where(x => x.UserSubRole == depid).ToList();
            if (employees != null && employees.Count > 0)
            {
                foreach (var employee in employees)
                {
                    var Earningsdata = new List<Earnings>();
                    var Deductiondata = new List<Deduction>();
                    var empDetails = new EmployeeInfo();
                    var employeename = (from itmes in ObjDAL.USP_gettingemployeenameandid(employee.ID)
                                        select itmes).FirstOrDefault();
                    empDetails.Name = employeename.empname;
                    empDetails.EmployeeId = employee.ID;
                    empDetails.MonthlyDays = DateTime.DaysInMonth(DateTime.Now.Year, month);

                    //Salary Details
                    var salaryDetails = ObjDAL.HRMSEmployeeMasterSalaryDetails.FirstOrDefault(x => x.EmpId == employee.ID.ToString());
                    empDetails.BasicAmount = salaryDetails != null ? Convert.ToDecimal(salaryDetails.BasicAmount) : 0;
                    empDetails.GrossAmount = salaryDetails != null ? Convert.ToDecimal(salaryDetails.GrossAmount) : 0;
                    var earningDetails = ObjDAL.HRMSEmployeeMasterDeductionsDetails.FirstOrDefault(x => x.EmpId == employee.ID.ToString());
                    empDetails.NetAmount = salaryDetails != null ? Convert.ToDecimal(earningDetails.NetAmount) : 0;

                    //Monthly Timesheet
                    decimal OTMinutes = 0;
                    var timeSheetData = ObjDAL.TimeSheets.Where(x => x.Date.Value.Year == DateTime.Now.Year &&
                                                                     x.Date.Value.Month == month && 
                                                                     x.EmpId == employee.ID).Distinct().ToList();

                    //Loop through time sheet and calculate the total OT hours of the employee
                    foreach (var timeSheet in timeSheetData)
                    {
                        var hours = timeSheet.OTHrs.Value.Hours;
                        var minutes = timeSheet.OTHrs.Value.Minutes;
                        OTMinutes += ((hours * 60) + minutes);
                    }
                    double Othrs = Convert.ToDouble(OTMinutes);
                    TimeSpan ts = TimeSpan.FromMinutes(Othrs);


                    decimal perdaySalary = (Convert.ToDecimal(earningDetails.NetAmount) / empDetails.MonthlyDays);
                    decimal perHourSalary = ((Convert.ToDecimal(earningDetails.NetAmount) / empDetails.MonthlyDays)/8);
                    decimal perMinuteSalary = ((Convert.ToDecimal(earningDetails.NetAmount) / empDetails.MonthlyDays) / 480);
                    decimal totalOTAmount = OTMinutes * perMinuteSalary;//Total cost for Ot hours worked.
                    empDetails.OT = OTMinutes / 60;//Minutes converting to hours
                    empDetails.TotalOTAmount = Convert.ToDecimal(totalOTAmount.ToString("#.00"));
                    empDetails.OThour= totalOTAmount > 0 ? (string.Format("{0} : {1}", ts.Hours, ts.Minutes) + " hrs") : "0.00";
                    
                    //Loss of pay for absent days
                    var absentDays = ObjDAL.AttendanceLists.Where(x => x.Date.Value.Year == DateTime.Now.Year &&
                                                                       x.Date.Value.Month == month &&
                                                                       x.EmpId == employee.ID.ToString() &&
                                                                       x.Status.ToLower() == "Absent")
                                                                       .ToList().Count;

                    var leaveApplied = ObjDAL.HRMSLeaveApplications.Where(x => x.AppliedDate.Value.Year == DateTime.Now.Year &&
                                                                         x.AppliedDate.Value.Month == month &&
                                                                         x.EmpCode == employee.ID.ToString() &&
                                                                         x.LeaveStatus.ToLower() == "approved")
                                                                         .ToList().Count;
                    empDetails.AbsentDays = absentDays;
                    empDetails.WorkingDays = empDetails.MonthlyDays - absentDays;

                    empDetails.ApprovedLeaves = leaveApplied;
                    empDetails.LOP = Convert.ToDecimal(((absentDays - leaveApplied) * perdaySalary));
                    empDetails.NetAmount = Convert.ToDecimal(((empDetails.NetAmount + empDetails.TotalOTAmount) - empDetails.LOP).ToString("#.##"));
                    //Earnings
                    Earningsdata.Clear();
                    var employeeEarnings = ObjDAL.EarningPercentageAmounts.Where(x => x.EmpId == employee.ID).Distinct().ToList();
                    decimal totalEarnings = 0;
                    employeeEarnings.ForEach(e => totalEarnings += Convert.ToDecimal(e.Amount));
                    empDetails.EarningTotal = Convert.ToDecimal((totalEarnings + totalOTAmount));

                    employeeEarnings.ForEach(e => Earningsdata.Add(new Earnings { Amount = Convert.ToDecimal(e.Amount), Name = e.Name }));
                    empDetails.Earnings = Earningsdata;



                    //Deductions
                    Deductiondata.Clear();
                    var employeeDeductions = ObjDAL.DeductionPercentageAmounts.Where(x => x.EmpId == employee.ID).Distinct().ToList();

                    decimal totalDeductions = 0;
                    employeeDeductions.ForEach(e => totalDeductions += Convert.ToDecimal(e.Amount));
                    empDetails.DeductionTotal = Convert.ToDecimal((empDetails.LOP + totalDeductions));

                    employeeDeductions.ForEach(e => Deductiondata.Add(new Deduction { Amount = Convert.ToDecimal(e.Amount), Name = e.Name }));
                    empDetails.Deductions = Deductiondata;


                    employeepaySlip.Add(empDetails);
                }
            }
            return employeepaySlip;
        }

        [WebMethod]
        public static List<EmployeeInfo> AnalysisReportData(int depid, int month)
        {
            List<EmployeeInfo> responce = new List<EmployeeInfo>();
            responce = GetEmployeesData(depid, month);
            return responce;
        }

        protected void btnBacktoDashboard_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Administrator/AdminDasboard.aspx");
        }
    }
}