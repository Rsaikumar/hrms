﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/Admin.Master" AutoEventWireup="true" CodeBehind="HolidayMaster.aspx.cs" Inherits="HRMS.Administrator.HolidayMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.js"></script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a></li>
                        <li><a href="LoanMaster.aspx" class="current">Holiday</a></li>
                    </ul>
                </div>
            </div>


            <div class="row">
                <section class="panel">
                    <div class="panel-body">
                        <form action="#" method="POST" class='form-horizontal form-bordered'>
                            <div class="col-md-12">
                                <div class="row">
                                    <section class="panel">
                                        <header class="panel-heading tab-bg-dark-navy-blue ">
                                            <ul class="nav nav-tabs">
                                                <li class="active">
                                                    <a data-toggle="tab" href="#Holiday" aria-expanded="false"><i class="fa fa-user">Holiday</i></a>
                                                </li>
                                                <li class="">
                                                    <a data-toggle="tab" href="#List" aria-expanded="true"><i class="fa fa-bars">List</i></a>
                                                </li>
                                            </ul>
                                        </header>

                                        <div class="panel-body">
                                            <div class="tab-content">
                                                <div id="Holiday" class="tab-pane active">
                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                        <ContentTemplate>
                                                            <div class="col-md-6 form-group">
                                                                <div class="col-md-4">
                                                                    <asp:Label ID="Label3" runat="server" Text="Label">Name</asp:Label>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <asp:DropDownList ID="ddlempname" runat="server" class="form-control"
                                                                        AutoPostBack="true" required>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-sm-4" style="">
                                                                    Select Date<span id="Span4" runat="server" style="color: Red; display: inline;">*</span></label>
                                                                <div class="col-sm-7" style="" id="Div1">
                                                                    <asp:TextBox ID="txtdate" runat="server" placeholder="Ex:dd-mm-yyyy" required class="form-control"
                                                                        MaxLength="20" AutoPostBack="true"></asp:TextBox>
                                                                    <cc1:CalendarExtender ID="actdate" PopupButtonID="imgPopup" runat="server" TargetControlID="txtdate"
                                                                        Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                                    </cc1:CalendarExtender>
                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <div class="col-md-12 text-center">
                                                        <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary" OnClick="btnSave_Click"/>
                                                    </div>
                                                </div>
                                                <div id="List" class="tab-pane">
                                                    <div style="max-height: 250px; overflow: auto;" class="table-responsive">
                                                        <asp:GridView ID="gdvleavemaster" HeaderStyle-ForeColor="Black" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                                            runat="server" Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%; overflow: scroll;" EmptyDataText="No records Found" AutoGenerateColumns="false">
                                                            <Columns>
                                                                <asp:BoundField DataField="Id" HeaderText="Leave No" ItemStyle-Width="200" />
                                                                <asp:BoundField DataField="Name" HeaderText="Leave Name" ItemStyle-Width="200" />
                                                                <asp:BoundField DataField="ShortName" HeaderText="Short Type" ItemStyle-Width="200" />
                                                                <asp:BoundField DataField="NoOfLeaves" HeaderText="No Of Leaves" ItemStyle-Width="200" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </section>
    </section>
</asp:Content>
