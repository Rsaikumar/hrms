﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/Admin.Master" AutoEventWireup="true" CodeBehind="EarningAndDeduction.aspx.cs" Inherits="HRMS.Employee.EarningAndDeduction" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.js"></script>
      <script type="text/javascript">
            $(document).ready(function () {
                $(".txtOnly").keypress(function (e) {
                    var key = e.keyCode;
                    if (key < 48 || (key > 57 && key < 65) || (key > 90 && key < 97) || key > 122 || (key >= 48 && key <= 57)) {
                        e.preventDefault();
                    }
                });
            });

            $(document).ready(function () {
                $(".txtno").keypress(function (e) {
                    var key = e.keyCode;
                    if (keyCode > 31 && (keyCode < 48 || keyCode > 57)) {
                        e.preventDefault();
                    }
                });
            });

    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="../Administrator/EarningAndDeduction.aspx" class="current">Earnings And Deductions</a> </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <i class="fa fa-th-list">Earnings & Deductions</i>
                            <div class="pull-right">
                                <asp:Button id="btnearningadd" class="btn btn-primary" data-target="#TypeEarningandDeduction" data-toggle="modal" >ADD</asp:Button>
                                
                            </div>
                        </header>
                        <div class="panel-body">
                              <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                <ContentTemplate>
                            <div class="col-md-12">
                                <div style="max-height: 515px; margin-bottom: 0.3%; height: 300px;">
                                    <asp:GridView ID="gdvearings" CssClass="mydatagrid" HeaderStyle-CssClass="headergrid" RowStyle-CssClass="rows"
                                        runat="server" Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%;" AutoGenerateColumns="false" OnRowDataBound="gdvearings_RowDataBound" OnSelectedIndexChanged="gdvearings_SelectedIndexChanged">
                                        <Columns>
                                            <asp:BoundField DataField="Id" HeaderText="Id" ItemStyle-Width="150" ControlStyle-CssClass="hidden"/>

                                            <asp:BoundField DataField="Name" HeaderText="Name" ItemStyle-Width="150" />
                                            <asp:BoundField DataField="Value" HeaderText="Value" ItemStyle-Width="200" />
                                            <asp:BoundField DataField="Type" HeaderText="Type" ItemStyle-Width="200" />
                                            <asp:BoundField DataField="PayslipType" HeaderText="PaySilpType" ItemStyle-Width="200" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                                                    </ContentTemplate>
                                  </asp:UpdatePanel>
                        </div>
                     
                    </section>
                </div>
                <div id="TypeEarningandDeduction" class="bootbox modal fade bootbox-prompt in" tabindex="-1" role="dialog" style="padding-right: 17px;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="bootbox-close-button close" data-dismiss="modal" id="btnpopmclose" aria-hidden="true">×</button>
                                <h4 class="modal-title">Earnings & Deductions</h4>
                            </div>
                            <div class="modal-body">
                                <div class="bootbox-body">
                                    <form class="form-horizontal bootbox-form" role="form" id="modalmaterial">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Name </label>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtername" runat="server" CssClass="form-control txtOnly"  data-required-msg="Please Select Name"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group" id="Valuediv">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Value</label>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txteaevalue" runat="server" CssClass="form-control txtno" onkeydown="return (!(event.keyCode>=65) && event.keyCode!=32);" data-required-msg="Please Enter Name"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group" id="typeid">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Type</label>
                                            <div class="col-sm-9">
                                                  <asp:DropDownList ID="ddleartype" runat="server" CssClass="form-control DropdownCss">
                                                    <asp:ListItem Text="---Select---" Value="---Select---"></asp:ListItem>
                                                    <asp:ListItem Text="Pecentage" Value="Pecentage"></asp:ListItem>
                                                    <asp:ListItem Text="Amount" Value="Amount"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group" id="Div1">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">PaySlip</label>
                                            <div class="col-sm-9">
                                                <asp:DropDownList ID="ddlearpayslip" runat="server" CssClass="form-control DropdownCss">
                                                    <asp:ListItem Text="---Select---" Value="---Select---"></asp:ListItem>
                                                    <asp:ListItem Text="Earnings" Value="Earnings"></asp:ListItem>
                                                    <asp:ListItem Text="Deductions" Value="Deductions"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="modal-footer">
                                <button data-bb-handler="cancel" data-dismiss="modal" type="button" class="btn btn-default" id="btnpopmcancel">Cancel</button>
                                <asp:Button ID="btnsave" runat="server" Text="Submit" CssClass="btn btn-primary" OnClick="btneardecsubmit_Click" />
                            </div>
                        </div>
                    </div>
                </div>
                <div id="Div2" class="bootbox modal fade bootbox-prompt in" tabindex="-1" role="dialog" style="padding-right: 17px;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="bootbox-close-button close" data-dismiss="modal" id="Button1" aria-hidden="true">×</button>
                                <h4 class="modal-title">Earnings & Deductions</h4>
                            </div>
                            <div class="modal-body">
                                <div class="bootbox-body">
                                    <form class="form-horizontal bootbox-form" role="form" id="Form1">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Name </label>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control txtOnly"  data-required-msg="Please Select Name"></asp:TextBox>
                                            </div>
                                        </div>
                                        
                                   
                                    </form>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="modal-footer">
                                <button data-bb-handler="cancel" data-dismiss="modal" type="button" class="btn btn-default" id="Button2">Cancel</button>
                                <asp:Button ID="Button3" runat="server" Text="Submit" CssClass="btn btn-primary" OnClick="btneardecsubmit_Click" />
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </section>

</asp:Content>
