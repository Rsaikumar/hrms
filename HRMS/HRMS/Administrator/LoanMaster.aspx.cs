﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Administrator
{
    public partial class LoanMaster : System.Web.UI.Page
    {

        #region Declaration

        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindgriddesignation();
                bindgrid();
            }
        }

        #endregion

        #region Methods

        public void cleartext()
        {
            txtLoanName.Text = string.Empty;
            txtShortName.Text = string.Empty;
            txtelgibityamunt.Text = string.Empty;
            ddltenure.SelectedIndex = 0;


        }

        public void bindgriddesignation()
        {

            var earnings = (from item in ObjDAL.HRMSRoleMasters
                            select item).ToList();


            if (earnings.Count > 0)
            {
                gdvdesignation.DataSource = earnings;
                gdvdesignation.DataBind();
            }
            else
            {
                gdvdesignation.DataSource = earnings;
                gdvdesignation.DataBind();
                gdvdesignation.Rows[0].Cells[0].Text = "No Records Found";


            }
        }

        public void bindgrid()
        {
            var loan = (from item in ObjDAL.HRMSNewLoanMasters
                        select item).ToList();


            if (loan.Count > 0)
            {
                gvdlonmaster.DataSource = loan;
                gvdlonmaster.DataBind();
            }
            else
            {
                gvdlonmaster.DataSource = loan;
                gvdlonmaster.DataBind();


            }
        }

        #endregion

        #region Events

        protected void chbdesignationlist_CheckedChanged(object sender, EventArgs e)
        {
            if (chbdesignationlist.Checked)
            {
                gdvdesignation.Visible = true;
                txtelgibityamunt.Visible = false;
                laleid.Visible = false;
            }
            else
            {
                gdvdesignation.Visible = false;
                txtelgibityamunt.Visible = true;
                laleid.Visible = true;
            }
        }

        /// <summary>
        /// elgibility amount should be more than or equal to 10000
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtelgibityamunt_TextChanged(object sender, EventArgs e)
        {
            decimal amount = Convert.ToDecimal(txtelgibityamunt.Text);

            if (amount == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Enter the Amount More Than 10000');", true);

            }
        }

        /// <summary>
        /// on slection the row it shows the particular details 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvdlonmaster_SelectedIndexChanged(object sender, EventArgs e)
        {

            string id = gvdlonmaster.SelectedRow.Cells[0].Text;
            string path = "~/Administrator/LoanMasterView.aspx";
            Session["LoanId"] = id;
            Response.Redirect(path);
        }

        /// <summary>
        /// on row bound showing the view details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvdlonmaster_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(gvdlonmaster, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = "View Loan Type Details";
                e.Row.Attributes["style"] = "cursor:pointer";
            }
        }

        /// <summary>
        /// saving the loan details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                HRMSNewLoanMaster role = new HRMSNewLoanMaster();
                role.LoanName = txtLoanName.Text;
                role.ShortName = txtShortName.Text;
                role.Tenure = Convert.ToInt32(ddltenure.SelectedItem.Text);
                role.EligibilityAmount = Convert.ToDecimal(txtelgibityamunt.Text.Trim() != "" ? txtelgibityamunt.Text.Trim() : "0");
                role.CreatedBy = "";
                role.CreatedOn = DateTime.Now;
                ObjDAL.HRMSNewLoanMasters.InsertOnSubmit(role);
                ObjDAL.SubmitChanges();
                bindgriddesignation();

                int id = role.Id;

                foreach (GridViewRow row in gdvdesignation.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox CheckRow = (row.Cells[0].FindControl("chbdesignation") as CheckBox);
                        if (CheckRow.Checked)
                        {
                            string designationname = gdvdesignation.Rows[row.RowIndex].Cells[1].Text;
                            TextBox Amount1 = (row.Cells[2].FindControl("txtelgibityamountlist") as TextBox);

                            string elgibility = Amount1.Text;
                            Decimal amount = Convert.ToDecimal(elgibility);


                            HRMSDesignationLoanMaster objpc = new HRMSDesignationLoanMaster();

                            objpc.LoanTypeId = Convert.ToInt32(id);
                            objpc.DesignationName = designationname;
                            objpc.ElgibilityAmount = Convert.ToDecimal(amount);
                            objpc.ModifiedOn = DateTime.Now;
                            objpc.ModifiedBy = objpc.ModifiedBy;
                            objpc.CreatedOn = DateTime.Now;
                            objpc.CreatedBy = objpc.CreatedBy;
                            ObjDAL.HRMSDesignationLoanMasters.InsertOnSubmit(objpc);
                         

                        }

                    }
                }
                ObjDAL.SubmitChanges();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Saved Sucessfully');", true);
                bindgriddesignation();
                cleartext();




            }

            catch (Exception ex)
            {
            }
        }

        #endregion
    }
}