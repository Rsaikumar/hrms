﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Administrator
{
    public partial class LeaveView : System.Web.UI.Page
    {
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                if ((string)Session["EmployeeDetailsCode"] != null)
                {

                    string empcode1 = (string)Session["EmployeeDetailsCode"];

                    Edit_Leave_Details(empcode1);
                }
                else
                {

                }
            }
        }

        public void Edit_Leave_Details(string empcode)
        {
            try
            {
                int ID = Convert.ToInt32(empcode);
                List<HRMSNewLeaveMaster> objEmp = (from master in ObjDAL.HRMSNewLeaveMasters
                                                               where master.Id == ID
                                                               select master).ToList();

                if (objEmp.Count > 0)
                {
                    txtleavename.Text = Convert.ToString(objEmp[0].Name);
                    txtShortName.Text = Convert.ToString(objEmp[0].ShortName);
                    txtnoofleaves.Text = Convert.ToString(objEmp[0].NoOfLeaves);

                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }

        protected void btndelete_Click1(object sender, EventArgs e)
        {
            try
            {
                string empcode = (string)Session["LoanId"];
                int empid = Convert.ToInt32(empcode);

                var address = (from s in ObjDAL.HRMSNewLeaveMasters.Where(x => x.Id == empid) select s).SingleOrDefault();
                ObjDAL.HRMSNewLeaveMasters.DeleteOnSubmit(address);
                ObjDAL.SubmitChanges();



                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Updated Sucessfully');window.location ='../Administrator/AdminDasboard.aspx';", true);

            }


            catch
            {
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {

                string empcode = (string)Session["EmployeeDetailsCode"];
                int empid = Convert.ToInt32(empcode);

                var role = (from s in ObjDAL.HRMSNewLeaveMasters.Where(x => x.Id == Convert.ToInt32(empcode)) select s).SingleOrDefault();
                {
                    role.Name = txtleavename.Text;
                    role.ShortName = txtShortName.Text;
                    role.NoOfLeaves = Convert.ToInt32(txtnoofleaves.Text);
                    role.ModifiedOn = DateTime.Now;
                    role.ModifiedBy = Convert.ToString(Session["Emp_Code"]);
                    ObjDAL.SubmitChanges();
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Updated Sucessfully');window.location ='../Administrator/AdminDasboard.aspx';", true);
                   
                }
            }
            catch
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Try Again');", true);

            }
        }

        protected void btncancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Administrator/LeaveMaster.aspx");
        }

    }
}