﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/Admin.Master" AutoEventWireup="true" CodeBehind="EarningsAndDeductionsView.aspx.cs" Inherits="HRMS.Administrator.EarningsAndDeductionsView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
        <section class="wrapper">

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="../Administrator/EarningsAndDeductionsView.aspx" class="current">EarningsAndDeductionsView</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">

                        <div class="panel-body">
                            <div class="tab-content">
                                <div class="col-md-12">
                                    <div class="row">
                                        <section class="panel">
                                            <header class="panel-heading">
                                                <i class="fa fa-th-list">Earnings And Deductions Details</i>
                                            </header>
                                            <div class="panel-body">
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-4" style="">
                                                        Name</label>
                                                    <div class="col-sm-8" style="" id="">
                                                        <asp:TextBox ID="txtname" runat="server" class="form-control"
                                                            AutoPostBack="true" MaxLength="50">
                                                        </asp:TextBox>

                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-4" style="">
                                                        Value</label>
                                                    <div class="col-sm-8" style="" id="Div1">
                                                        <asp:TextBox ID="txtvalue" runat="server" placeholder="Value" required class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-4" style="">
                                                        Type</label>
                                                    <div class="col-sm-8" style="" id="Div1">
                                                        <asp:DropDownList ID="ddltype" runat="server" placeholder="Type Name" required class="form-control">
                                                            <asp:ListItem Text="---Select---" Value="---Select---"></asp:ListItem>
                                                            <asp:ListItem Text="Pecentage" Value="Pecentage"></asp:ListItem>
                                                            <asp:ListItem Text="Amount" Value="Amount"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-4" style="">
                                                        PaySilpType</label>
                                                    <div class="col-sm-8" style="" id="Div1">
                                                        <asp:DropDownList ID="ddlpayslip" runat="server" placeholder="Designation Name" required class="form-control">
                                                            <asp:ListItem Text="---Select---" Value="---Select---"></asp:ListItem>
                                                            <asp:ListItem Text="Earnings" Value="Earnings"></asp:ListItem>
                                                            <asp:ListItem Text="Deductions" Value="Deductions"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 text-center">
                                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-primary" OnClick="btnUpdate_Click" /><%--OnClientClick="return fileUploadValidation()"--%>
                                                    <asp:Button ID="btndelete" runat="server" Text="Delete" class="btn btn-danger" OnClick="btndelete_Click" OnClientClick="javascript:return confirm('Are you sure you want to delete this class?');" /><%--OnClientClick="return fileUploadValidation()"--%>
                                                    <asp:Button ID="btncancle" runat="server" Text="Cancel" class="btn btn-primary" OnClick="btncancle_Click" /><%--OnClientClick="return fileUploadValidation()"--%>

                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

            </div>
        </section>

    </section>
</asp:Content>
