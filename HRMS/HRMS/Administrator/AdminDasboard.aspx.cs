﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Administrator
{
    public partial class AdminDasboard : System.Web.UI.Page
    {
        #region Declarations

        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            EmployeeList();
        }

        #endregion

        #region Methods

        public void EmployeeList()
        {
          
           var empdetails = (from lm in ObjDAL.HRMSEmployeeMasterRegistrationDetails
                            
                                select lm).ToList().Distinct();

          spnempCount.InnerText= Convert.ToString( empdetails.Count());

          var emploan = (from lm in ObjDAL.HRMSLoanApplications
                            select lm).ToList();

          spnloan.InnerText = Convert.ToString(emploan.Count());

          var empeave = (from lm in ObjDAL.HRMSLeaveApplications
                         select lm).ToList();

          spnleavcount.InnerText = Convert.ToString(empeave.Count());

        }

        #endregion
    }
}