﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BAL;
using DAL;
using System.Globalization;
using System.Data;

namespace HRMS.Administrator
{
    public partial class HolidayList : System.Web.UI.Page
    {
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();
        HolidaysListBAL objHolidayListBAL = new HolidaysListBAL();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            string CompanyName = Session["ClientRegistrationNo"].ToString().Trim().ToLower();
            if (!IsPostBack)
            {
                Bind_Grid();
                string date = System.DateTime.Now.ToShortDateString();
                string[] date1 = date.Split('/');

               // string currentYear = date1[2];
                //foreach (ListItem li in ddlYear.Items)
                //{
                //    if (li.Value == currentYear)
                //    {
                //        ddlYear.SelectedValue = li.ToString();
                //    }
                //}
               
                ddlYear.Focus();
                if ((string)Session["HolidayCode"] != null)
                {

                    //  Request.QueryString.Clear();
                    string ClientRegistrationNo = Session["ClientRegistrationNo"].ToString();
                    string hcode = (string)Session["HolidayCode"];
                    Session["UpdateHolidayCode"] = hcode;
                    //string LoginID = Request.QueryString["id"];
                    Edit_HolidayDetails(hcode, ClientRegistrationNo);
                }
                else
                {
                    btnUpdate.Visible = false;
                    //SetInitialRow();
                }
            }
        }

        //Setinitialrow
        public void SetInitialRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("Date", typeof(string)));
            dt.Columns.Add(new DataColumn("Event_Name", typeof(string)));
            dr = dt.NewRow();

            dr["Date"] = string.Empty;
            dr["Event_Name"] = string.Empty;

            dt.Rows.Add(dr);
            grdHolidayEvent.DataSource = dt;
            grdHolidayEvent.DataBind();
            int columncount = 2;
            grdHolidayEvent.Rows[0].Cells.Clear();
            grdHolidayEvent.Rows[0].Cells.Add(new TableCell());
            grdHolidayEvent.Rows[0].Cells[0].ColumnSpan = columncount;
            grdHolidayEvent.Rows[0].Cells[0].Text = "No Records Found";
            //  grdStorageLocation.HeaderStyle.HorizontalAlign = "center";
            grdHolidayEvent.Rows[0].Cells[0].ForeColor = System.Drawing.Color.Red;
        }


        /// <summary>
        /// PopUp Display 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void btnAddYear_Click(object sender, EventArgs e)
        //{
        //    this.mp1.Show();
        //    txtYear.Focus();
        //    //Panel1.Attributes.Add("style", "display:block;border:1px solid #000;");
        //}


        #region Button Events
        public void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Saving();
                Bind_Grid();
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }
            finally { }
        }

        public void btnCancel_Click(object sender, EventArgs e)
        {
            ClearAllFields();
        }

        public void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            Bind_Gridyear();
        }

        public void btnUpdate_Click(object sender, EventArgs e)
        {
            Update();

        }
        // updating holiday details
        private void Update()
        {
            try
            {
                //string holidaycode = e.CommandName.ToString();
                string d1 = txtDate.Text.Substring(0, 2);
                string m1 = txtDate.Text.Substring(3, 2);
                string y1 = txtDate.Text.Substring(6, 4);


                DateTime DATE = DateTime.ParseExact(txtDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);

                objHolidayListBAL.Holiday_Code = (string)Session["UpdateHolidayCode"];
                objHolidayListBAL.Year = ddlYear.SelectedValue;
                objHolidayListBAL.Date = DATE;
                objHolidayListBAL.Event_Name = txtEventName.Text;

                objHolidayListBAL.Created_By = Session["User_Name"].ToString();
                objHolidayListBAL.Created_Date = System.DateTime.Now;
                objHolidayListBAL.Creation_Company = Session["ClientRegistrationNo"].ToString();
                objHolidayListBAL.Modified_By = Session["User_Name"].ToString();
                objHolidayListBAL.Modified_Date = System.DateTime.Now;
                objHolidayListBAL.Parameter = 2;
                if (objHolidayListBAL.Insert_HRMS_Holiday_Master() != 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);
                    Bind_Grid();
                    ClearAllFields();

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('"+msg+"');", true);
            }
        }
        #endregion

        #region Methods

        String StrDate = "";
        // // Show the date format dd-mm-yy in sale Order Grids
        public string GetDate(object Dt)
        {

            StrDate = string.Format("{0:dd-MM-yyyy}", Dt);
            return StrDate;
        }

        /// <summary>
        /// Bind Grid Based on grdHolidayEvent
        /// </summary>
        /// <param name="sender"></param>
     public void Bind_Grid()
        {
            try
            {
                string Cmpy = (string)Session["ClientRegistrationNo"];
                // string Brnach = (string)Session["BranchID"];
                string UserType = (string)Session["User_Type"];
                string UserName = (string)Session["User_Name"];
                string Cration_Company = (string)Session["CreationCompany"]; //"000001";
                string year = ddlYear.SelectedItem.Text;

                var objOTInfo = (from header in ObjDAL.HRMS_Holiday_Masters
                                 where  header.Creation_Company == Cmpy
                                 orderby header.Date
                                 select header).ToList();
                var distinctList = objOTInfo.GroupBy(x => x.Date)
                         .Select(g => g.First())
                         .ToList();
                if (distinctList.Count > 0)
                {
                    grdHolidayEvent.DataSource = distinctList;
                    grdHolidayEvent.DataBind();
                }
                else
                {
                    // // // // Grid view using setting initial rows displaying Empty Header Displying
                    SetInitialRow();
                }

            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
            finally
            {
            }
        }
        public void Bind_Gridyear()
        {
            try
            {
                string Cmpy = (string)Session["ClientRegistrationNo"];
                // string Brnach = (string)Session["BranchID"];
                string UserType = (string)Session["User_Type"];
                string UserName = (string)Session["User_Name"];
                string Cration_Company = (string)Session["CreationCompany"]; //"000001";
                string year = ddlYear.SelectedItem.Text;

                var objOTInfo = (from header in ObjDAL.HRMS_Holiday_Masters
                                 //where header.Year == year && header.Creation_Company == Cmpy
                                 orderby header.Date
                                 select header).ToList();
                var distinctList = objOTInfo.GroupBy(x => x.Date)
                         .Select(g => g.First())
                         .ToList();
                if (distinctList.Count > 0)
                {
                    grdHolidayEvent.DataSource = distinctList;
                    grdHolidayEvent.DataBind();
                }
                else
                {
                    // // // // Grid view using setting initial rows displaying Empty Header Displying
                    SetInitialRow();
                }

            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
            finally
            {
            }
        }

        //clear Controls
        public void ClearAllFields()
        {
            ddlYear.SelectedValue = "Select Year";
            txtDate.Text = "";
            txtEventName.Text = "";
            //SetInitialRow();

        }

        //Saving
        public void Saving()
        {
            try
            {
                string d1 = txtDate.Text.Substring(0, 2);
                string m1 = txtDate.Text.Substring(3, 2);
                string y1 = txtDate.Text.Substring(6, 4);


                DateTime DATE = DateTime.ParseExact(txtDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);

                objHolidayListBAL.Holiday_Code = "";
                objHolidayListBAL.Year = ddlYear.SelectedValue;
                objHolidayListBAL.Date = DATE;
                objHolidayListBAL.Event_Name = txtEventName.Text;

                objHolidayListBAL.Created_By = Session["User_Name"].ToString();
                objHolidayListBAL.Created_Date = System.DateTime.Now;
                objHolidayListBAL.Creation_Company = Session["ClientRegistrationNo"].ToString();
                objHolidayListBAL.Modified_By = Session["User_Name"].ToString();
                objHolidayListBAL.Modified_Date = System.DateTime.Now;
                objHolidayListBAL.Parameter = 1;
                if (objHolidayListBAL.Insert_HRMS_Holiday_Master() != 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);
                    ClearAllFields();
                    Bind_Grid();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
                }

            }
            catch (Exception ex)
            {

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
        }






        #endregion

        #region grid event
        public void grdHolidayEvent_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string holidycode = e.CommandArgument.ToString();
            string Creation_Company = Session["ClientRegistrationNo"].ToString();
            if (e.CommandName== "Edit")
            {
                string path = "HolidayList.aspx";
                Session["HolidayCode"] = holidycode;
                Response.Redirect(path);
                //Edit_HolidayDetails(holidycode, Creation_Company);
            }
            if (e.CommandName == "Delete")
            {
                var objOTInfo = (from header in ObjDAL.HRMS_Holiday_Masters
                                 where  header.Creation_Company == Creation_Company
                                 orderby header.Date
                                 select header).First();
                
                ObjDAL.HRMS_Holiday_Masters.DeleteOnSubmit(objOTInfo);
                ObjDAL.SubmitChanges();
                Bind_Grid();
                //Delete_HolidayDetails(holidycode, Creation_Company);
                //string path = "HolidayList.aspx";
                //Session["HolidayCode"] = holidycode;
                //Response.Redirect(path);
            }
         }

        /// <summary>
        ///  The Following Method is used for Delete Grid records to Data From Holiday Master  Table
        /// </summary>
        public void Delete_HolidayDetails(string holidycode, string Creation_Company)
        {
            try
            {
                //HRMS_Holiday_Master a = objHolidayListBAL.Delete_Holiday_Master(holidycode, Session["ClientRegistrationNo"].ToString());

                //if (a != null)
                //{
                //    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Header Details Deleted Successfully ');", true);
                //}
                //else
                //{
                //    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Header Details Already Existing In Header Page ');", true);
                //}
                //Bind_Grid();
                

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.ToString() + "');", true);
            }
            finally
            {
            }
        }

        // Editing Group Details
        public void Edit_HolidayDetails(string holidycode, string Creation_Company)
        {
            try
            {

                var objHolidayDetails = objHolidayListBAL.Get_Holiday_Details(holidycode, Creation_Company);
                if (objHolidayDetails.Count > 0)
                {
                   // ddlYear.SelectedValue = objHolidayDetails[0].Year.ToString();
                    txtDate.Text = GetDate(objHolidayDetails[0].Date).ToString();
                    txtEventName.Text = objHolidayDetails[0].Event_Name.ToString();
                    btnUpdate.Visible = true;
                    btnSave.Visible = false;
                    Session["HolidayCode"] = null;


                }
                else
                {
                    Bind_Grid();
                   // ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('No Data Found ');", true);

                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }
        #endregion

       

        protected void grdHolidayEvent_RowDeleting1(object sender, GridViewDeleteEventArgs e)
        {

        }
    }
}