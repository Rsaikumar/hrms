﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Administrator
{
    public partial class AttendanceListView : System.Web.UI.Page
    {


        #region DECLARATIONS

        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();

        #endregion


        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if ((string)Session["EmployeeDetailsCode"] != null)
                {
                    string empcode = (string)Session["EmployeeDetailsCode"];
                    Session["EmployeeFamilyDetailsCode"] = empcode;
                    Session["UpdateEmployeeDetailsCode"] = empcode;
                    string ClientRegistrationNo = Session["ClientRegistrationNo"].ToString();
                    Edit_Attendance_Details(empcode);
                }
                else
                {

                }
            }
        }

        /// <summary>
        /// onclick it redirect to the dashboard.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Administrator/AdminDasboard.aspx");

        }

        /// <summary>
        /// on row click shows the particular details
        /// </summary>
        /// <param name="empcode"></param>
        public void Edit_Attendance_Details(string empcode)
        {
            try
            {
                int ID = Convert.ToInt32(empcode);

                var objbindattendancegrid = (from ST in ObjDAL.USP_GetEmployeeDetailsAttendace()
                                             where ST.EmpId == ID
                                             select ST).ToList();
                if (objbindattendancegrid.Count() >= 0)
                {
                    gvdAttendancelist.DataSource = objbindattendancegrid;
                    gvdAttendancelist.DataBind();
                }

                else
                {
                    gvdAttendancelist.DataSource = objbindattendancegrid;
                    gvdAttendancelist.DataBind();
                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }

        /// <summary>
        /// saving the attendance list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                string empcode = (string)Session["EmployeeDetailsCode"];

                int ID = Convert.ToInt32(empcode);

                var earnsal = (from s in ObjDAL.AttendanceLists.Where(x => x.EmpId == Convert.ToString(ID)) select s).FirstOrDefault();
                {

                    foreach (GridViewRow row in gvdAttendancelist.Rows)
                    {
                        DropDownList Status = (DropDownList)gvdAttendancelist.Rows[row.RowIndex].FindControl("ddlstatus");


                        earnsal.Status = Status.SelectedItem.Text.ToString();
                        earnsal.ModifiedBy = Session["Emp_Code"].ToString();
                        earnsal.ModifiedOn = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd"));

                    }
                    ObjDAL.SubmitChanges();
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Updated Successfully');window.location ='../Administrator/AdminDasboard.aspx';", true);
                }


            }
            catch (Exception ex)
            {

                ex.Message.ToString();
            }

        }

        #endregion
    }




    
}

