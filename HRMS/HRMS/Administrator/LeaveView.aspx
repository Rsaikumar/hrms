﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/Admin.Master" AutoEventWireup="true" CodeBehind="LeaveView.aspx.cs" Inherits="HRMS.Administrator.LeaveView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.js"></script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="LeaveView.aspx" class="current">Leave Information</a> </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading text-left">
                            <div class="text-left">
                                <i class="fa fa-user">Leave Information</i>
                            </div>
                            <%--<div class="text-right">
                                <asp:LinkButton ID="lkbedit" runat="server" ForeColor="Green"><i class="fa fa-edit"> <b>Edit</b></i></asp:LinkButton>
                                <asp:LinkButton ID="lkbdelete" runat="server" ForeColor="Red" OnClientClick="javascript:return confirm('Are you sure you want to delete this class?');"><i class="fa fa-edit"> <b>Delete</b></i></asp:LinkButton>

                            </div>--%>
                        </header>
                        <div class="panel-body">
                            <div class="tab-content">
                                <div id="leavetype" class="tab-pane active">
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group col-md-6">
                                                <label for="textfield" class="control-label col-md-5">
                                                    Leave Name<span id="Span1" runat="server" style="color: Red; display: inline;">*</span></label>
                                                <div class="col-md-7 ">
                                                    <asp:TextBox ID="txtleavename" runat="server" TabIndex="0" placeholder="Leave Name" class="form-control"
                                                        MaxLength="50" AutoPostBack="true" required></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="textfield" class="control-label col-md-5">
                                                    Short Name<span style="color: #ff0000; font-size: 14px; display: inline;">*</span></label>
                                                <div class="col-md-7" id="Div1">
                                                    <asp:TextBox ID="txtShortName" runat="server" TabIndex="1" placeholder="Short Name" class="form-control"
                                                        MaxLength="50" AutoPostBack="true" required></asp:TextBox>
                                                </div>
                                            </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                    <div class="form-group col-md-6">
                                        <label for="textfield" class="control-label col-md-5" id="laleid" runat="server">
                                            No Of Leaves<span style="color: #ff0000; font-size: 14px; display: inline;">*</span></label>
                                        <div class="col-md-7" id="Div2">
                                            <asp:TextBox ID="txtnoofleaves" runat="server" TabIndex="1" placeholder="No Of Leave" class="form-control"
                                                MaxLength="50" AutoPostBack="true" required></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-primary" OnClick="btnUpdate_Click" /><%--OnClientClick="return fileUploadValidation()"--%>
                                        <asp:Button ID="btndelete" runat="server" Text="Delete" class="btn btn-danger" OnClick="btndelete_Click1" OnClientClick="javascript:return confirm('Are you sure you want to delete this class?');" /><%--OnClientClick="return fileUploadValidation()"--%>
                                        <asp:Button ID="btncancle" runat="server" Text="Cancel" class="btn btn-primary" OnClick="btncancle_Click"  /><%--OnClientClick="return fileUploadValidation()"--%>

                                    </div>
                                </div>



                            </div>

                        </div>

                    </section>
                </div>
            </div>
        </section>
    </section>

</asp:Content>
