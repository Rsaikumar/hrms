﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

#region Private Using
using System.Data;
using DAL;
using BAL;
using System.IO;

using System.Data.Common;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Configuration;
#endregion
namespace HRMS.Administrator
{
    public partial class Company_Info_Webuser : System.Web.UI.UserControl
    {
        Administrator_CompanyInfoBAL objCompanyBAL = new Administrator_CompanyInfoBAL();

        ERP_DatabaseDataContext objDb = new ERP_DatabaseDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["ClientRegistrationNo"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                Bind_Grid();
                txtSearch.Focus();
            }
        }


        #region Events

        /// <summary>
        /// Create button click it directly navigated to Add_Company_Info
        /// Created By Anusha
        /// </summary>

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddCompanyInfomation.aspx");
        }

        // // Delete the Record in Company Info
        protected void lnkRemove_OnClick(object sender, EventArgs e)
        {
            LinkButton lnkRemove = (LinkButton)sender;
            string ClientRegistrationNo = lnkRemove.CommandArgument;

            objCompanyBAL.Delete_CompanyInfo(ClientRegistrationNo, Session["ClientRegistrationNo"].ToString());
            Bind_Grid();

        }

        /// <summary>
        /// Import The Data To Database
        /// Created By Anusha
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnImportData_Click(object sender, EventArgs e)
        {
            //if (fullcompanyinfo.HasFile)
            //{
            //    try
            //    {
            //        string path = string.Concat(Server.MapPath("~/UploadedFolder/" + fullcompanyinfo.FileName));
            //        fullcompanyinfo.SaveAs(path);

            //        // Connection String to Excel Workbook
            //        string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);
            //        OleDbConnection connection = new OleDbConnection();
            //        connection.ConnectionString = excelConnectionString;
            //        OleDbCommand command = new OleDbCommand("select * from [Sheet1$]", connection);
            //        connection.Open();
            //        // Create DbDataReader to Data Worksheet
            //        DbDataReader dr = command.ExecuteReader();

            //        // SQL Server Connection String
            //        //string sqlConnectionString = @"Data Source=LAKSANASERVER;Initial Catalog=Laksana_ERP;User ID=sa;Password=laksana@2015";
            //        string sqlConnectionString = ConfigurationManager.ConnectionStrings["Dbconnection"].ToString();
            //        // Bulk Copy to SQL Server 
            //        SqlBulkCopy bulkInsert = new SqlBulkCopy(sqlConnectionString);
            //        bulkInsert.DestinationTableName = "Administrator_CompanyInfo";
            //        bulkInsert.WriteToServer(dr);
            //        // Label1.Text = "Ho Gaya";
            //    }
            //    catch (Exception ex)
            //    {
            //        //Label1.Text = ex.Message;
            //    }
            //    finally
            //    {
            //        Bind_Grid();
            //    }
            //}
        }

        #endregion

        #region
        /// <summary>
        /// Grid View On Row Command Means Using For Edit Delete ButtonsUsing Command Name
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdCompanyInfo_Registration_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string Customer_Code = e.CommandArgument.ToString();
            if (e.CommandName == "Edit")
            {
                string path = "AddCompanyInfomation.aspx?id=" + Customer_Code;
                Response.Redirect(path);
            }
            if (e.CommandName == "Delete")
            {
                Bind_Grid();
            }
        }

        /// <summary>
        /// Paging For Company_Info
        /// Created By Anusha
        /// </summar
        /// 
        protected void grdCompanyinfo_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdCompanyInfo.PageIndex = e.NewPageIndex;
            Bind_Grid();
        }

        /// <summary>
        /// Row Editing Grid view  For Company_Info
        /// Created By Anusha
        /// </summar
        /// 
        protected void grdCompanyInfo_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grdCompanyInfo.EditIndex = e.NewEditIndex;
            Bind_Grid();
        }

        /// <summary>
        /// Row Data Bound Grid view  For Company_Info
        /// Created By Anusha
        /// </summar
        /// 
        protected void grdCompanyInfo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if ((e.Row.RowState == DataControlRowState.Normal) || (e.Row.RowState == DataControlRowState.Alternate))
                {
                    e.Row.Attributes.Add("onMouseOver", "this.style.cursor = 'hand';this.style.backgroundColor = 'white';");
                    e.Row.Attributes.Add("onMouseOut", "this.style.backgroundColor = 'white';");
                    //string Sale_Exec_Code = ((Label)grdSaleTeam.Rows[e.RowIndex].FindControl("lblSaleExeCode")).Text;
                    string ClientRegistrationNo = grdCompanyInfo.DataKeys[e.Row.RowIndex].Value.ToString();
                    // DataControlFieldCell cid = (DataControlFieldCell)(id from grid view element );                  
                    string path = "Add_Company_Info.aspx?id=" + ClientRegistrationNo;
                    e.Row.Attributes["onclick"] = " return SelectedObjRptPopUp('" + path + "')";
                }
            }
        }


        /// <summary>
        /// Row Updating Gridview For Company_Info
        /// Created By Anusha
        /// </summar
        /// 
        protected void grdCompanyInfo_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            string ClientRegistrationNo = ((Label)grdCompanyInfo.Rows[e.RowIndex].FindControl("lblcompanyid")).Text;
            string Company_Name = ((TextBox)grdCompanyInfo.Rows[e.RowIndex].FindControl("txtcompanyname")).Text;
            string State = ((TextBox)grdCompanyInfo.Rows[e.RowIndex].FindControl("txtstate")).Text;
            string PINCode = ((TextBox)grdCompanyInfo.Rows[e.RowIndex].FindControl("txtpincode")).Text;
            string E_Mail = ((TextBox)grdCompanyInfo.Rows[e.RowIndex].FindControl("txtemail")).Text;
            string WebUrl = ((TextBox)grdCompanyInfo.Rows[e.RowIndex].FindControl("txtweburl")).Text;
            string TINNo = ((TextBox)grdCompanyInfo.Rows[e.RowIndex].FindControl("txttinno")).Text;


            objCompanyBAL.ClientRegistrationNo = ClientRegistrationNo;
            objCompanyBAL.Company_Name = Company_Name;
            objCompanyBAL.State = State;
            objCompanyBAL.Pincode = PINCode;
            objCompanyBAL.E_Mail = E_Mail;
            objCompanyBAL.Web_URL = WebUrl;
            objCompanyBAL.TIN_No = TINNo;

            objCompanyBAL.Creation_Company = Session["ClientRegistrationNo"].ToString();
            objCompanyBAL.Created_By = Session["User_Name"].ToString();
            objCompanyBAL.Created_Date = System.DateTime.Now;
            objCompanyBAL.Modified_By = Session["User_Name"].ToString();
            objCompanyBAL.Modified_Date = System.DateTime.Now;
            objCompanyBAL.Parameter = 2;

            objCompanyBAL.Insert_Administrator_Company_Info();
            grdCompanyInfo.EditIndex = -1;
            Bind_Grid();

        }


        /// <summary>
        /// Row Cancel Grid view  For Company_Info
        /// Created By Anusha
        /// </summar
        /// 
        protected void grdCompanyInfo_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grdCompanyInfo.EditIndex = -1;
            Bind_Grid();

        }

        #endregion

        #region Private Method
        /// <summary>
        /// Binding The 
        /// </summary>
        public void Bind_Grid()
        {
            List<Administrator_CompanyInfo> ObjCompanyInfo = objCompanyBAL.GetData_CompanyInfo(Session["ClientRegistrationNo"].ToString());
            grdCompanyInfo.DataSource = ObjCompanyInfo;
            grdCompanyInfo.DataBind();
        }

        /// <summary>
        /// Clear All Fields 
        /// Created By Divya
        /// </summary>

        public void ClearAllFields()
        {
            txtSearch.Text = "";
        }

        /// <summary>
        /// Setting Initial Empty Row Data Into Company Info
        /// Created By Anusha
        /// </summary>
        /// <param name="sender"></param>

        public void SetInitialRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("ClientRegistrationNo", typeof(string)));
            dt.Columns.Add(new DataColumn("Company_Name", typeof(string)));
            dt.Columns.Add(new DataColumn("State", typeof(string)));

            dt.Columns.Add(new DataColumn("Pincode", typeof(string)));
            dt.Columns.Add(new DataColumn("E_Mail", typeof(string)));

            dt.Columns.Add(new DataColumn("Web_URL", typeof(string)));
            dt.Columns.Add(new DataColumn("TIN_No", typeof(string)));


            dr = dt.NewRow();


            dr["ClientRegistrationNo"] = string.Empty;
            dr["Company_Name"] = string.Empty;
            dr["State"] = string.Empty;
            dr["Pincode"] = string.Empty;
            dr["E_Mail"] = string.Empty;
            dr["Web_URL"] = string.Empty;
            dr["TIN_No"] = string.Empty;



            dt.Rows.Add(dr);


            grdCompanyInfo.DataSource = dt;
            grdCompanyInfo.DataBind();

            int columncount = 7;
            grdCompanyInfo.Rows[0].Cells.Clear();
            grdCompanyInfo.Rows[0].Cells.Add(new TableCell());
            grdCompanyInfo.Rows[0].Cells[0].ColumnSpan = columncount;
            grdCompanyInfo.Rows[0].Cells[0].Text = "No Records Found";
            //  grdStorageLocation.HeaderStyle.HorizontalAlign = "center";
            grdCompanyInfo.Rows[0].Cells[0].ForeColor = System.Drawing.Color.Red;
        }

        /// <summary>
        ///  The Following Method is used for Export Grid view Data From Excel Sheet
        ///  Created By Divya
        /// </summary>

        //export excel
        public void ExportCompanyInfoExcel()
        {
            try
            {
                if (grdCompanyInfo.Rows.Count > 0)
                {
                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=" + "Storage_Location" + DateTime.Now.ToString("dd_MM_yyyy_HHmmss") + ".xls");
                    Response.ContentType = "application/ms-excel";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    grdCompanyInfo.AllowPaging = false;
                    grdCompanyInfo.AllowSorting = false;
                    //DataTable ds = (DataTable)ViewState["currentdata"];
                    //grdStorageLocation.DataSource = ds;
                    //grdStorageLocation.DataBind();
                    Bind_Grid();
                    grdCompanyInfo.RenderControl(htw);

                    Response.Write(sw.ToString());
                    Response.End();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Data...');", true);
                }
            }
            catch (Exception ex)
            {
            }

        }

        //ddlAction_OnSelectedIndexChanged

        protected void ddlAction_OnSelectedIndexChanged(object sender, EventArgs e)
        {


            if (grdCompanyInfo.Rows.Count > 0)
            {
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=" + "Administrator_CompanyInfo" + DateTime.Now.ToString("dd_MM_yyyy_HHmmss") + ".xls");
                Response.ContentType = "application/ms-excel";
                System.IO.StringWriter sw = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
                grdCompanyInfo.AllowPaging = false;
                grdCompanyInfo.AllowSorting = false;
                Bind_Grid();


                System.Web.UI.HtmlControls.HtmlForm form = new System.Web.UI.HtmlControls.HtmlForm();
                Controls.Add(form);
                form.Controls.Add(grdCompanyInfo);
                form.RenderControl(htw);
                Response.Write(sw.ToString());
                Response.End();

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Data...');", true);
            }


        }

        public override void RenderControl(HtmlTextWriter writer)
        {
            base.RenderControl(writer);
        }

        protected override void RenderChildren(HtmlTextWriter writer)
        {
            base.RenderChildren(writer);
        }


        #endregion


    }
}