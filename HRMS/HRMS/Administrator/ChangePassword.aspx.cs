﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BAL;
using DAL;
using System.Data.Linq.SqlClient;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using System.IO;


namespace HRMS.Administrator
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        string objConfig = ConfigurationManager.ConnectionStrings["Dbconnection"].ToString();
        SqlConnection objSqlConnection;
        SqlCommand objSqlCommand;
        SqlDataAdapter da;
        DataSet ds;
        DataTable dt;
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }
        protected void txtOldPwd_TextChanged(object sender, EventArgs e)
        {
            string oldpwd = txtOldPwd.Text;
            string username = (string)Session["User_Name"];
            string creationcompany = (string)Session["ClientRegistrationNo"];

            int valid = (from user in ObjDAL.Administrator_UserRegistrations
                         where user.Login_Id == username && user.Password == oldpwd && user.Creation_Company == creationcompany
                         select user).Count();
            if (valid > 0)
            {
                txtNewPwd.Focus();
            }
            else
            {
                txtOldPwd.Text = "";
                txtOldPwd.Focus();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Enter Password Correctly..');", true);
            }
        }

        public void update()
        {

            string logidid = (string)Session["User_Name"];
            string creationcompany = (string)Session["ClientRegistrationNo"];
            string branchid = (string)Session["BranchID"];
            try
            {
                objSqlConnection = new SqlConnection(objConfig);
                objSqlCommand = new SqlCommand();
                objSqlCommand.Connection = objSqlConnection;
                objSqlConnection.Open();
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.CommandText = "Sp_SCM_Change_Password";
                objSqlCommand.Parameters.AddWithValue("@Password", txtCPwd.Text);
                objSqlCommand.Parameters.AddWithValue("@Modified_Date", System.DateTime.Now);
                objSqlCommand.Parameters.AddWithValue("@Login_Id", logidid);
                objSqlCommand.Parameters.AddWithValue("@Creation_Company", creationcompany);
                da = new SqlDataAdapter(objSqlCommand);
                dt = new DataTable();
                da.Fill(dt);
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Changed Sucessfully..');", true);
            }
            catch (Exception ex)
            {
                Console.Write("Message: " + ex.Message);
                ex.ToString();
                //  return dt;
            }
            finally
            {

            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            update();
        }
    }
}