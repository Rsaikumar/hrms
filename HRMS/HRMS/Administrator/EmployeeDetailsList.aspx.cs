﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Administrator
{
    public partial class EmployeeDetailsList : System.Web.UI.Page
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string empcode = Convert.ToString(Session["EmployeeDetailsCode"]);

                bindgirdemployeedetails();
            }
        }

        public void bindgirdemployeedetails()
        {
            ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

            //getting session from login page 
            int roleid = Convert.ToInt32(Session["role"].ToString());
            int empid = Convert.ToInt32(Session["Emp_Code"]);
            //here employee list is filtered based on employee role 
            var employeelist = (from item in objDAL.HRMSEmployeeMasterRegistrationDetails
                                join per in objDAL.HRMSEmplyeeMasterPersonalDetails on item.ID equals Convert.ToInt32(per.EmpId)
                                join official in objDAL.HRMSEmployeeMasterOfficialInformationDetails on item.ID equals Convert.ToInt32(official.EmpId)
                             
                                select new
                                {
                                    EmpId = item.ID,
                                    FirstName = per.FirstName,
                                    LastName = per.LastName,
                                    UserRole = item.RoleType,
                                    EmployeeStatus = item.EmployeeStatus,
                                    Gender = per.Gender,
                                    DateOfBirth = per.DateOfBirth

                                }).ToList();

            if (employeelist.Count > 0)
            {
                gdvemplist.DataSource = employeelist;
                gdvemplist.DataBind();
            }
            else
            {

            }
        }


        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Administrator/AdminDasboard.aspx");
        }

        protected void gdvemplist_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(gdvemplist, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = "View Employee Details";
                e.Row.Attributes["style"] = "cursor:pointer";
            }
        }

        protected void gdvemplist_SelectedIndexChanged(object sender, EventArgs e)
        {
            string Empid = gdvemplist.SelectedRow.Cells[0].Text;
            string path = "~/Administrator/EmployeeDetailsView.aspx";
            Session["EmployeeDetailsCode"] = Empid;
            Response.Redirect(path);
        }
    }
}