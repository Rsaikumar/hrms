﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Administrator
{
    public partial class LoanList : System.Web.UI.Page
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                 approveloandetails();
            }
           
        }


        public void approveloandetails()
        {
            var approve = (from item in objDAL.Usp_GettingEmployeeDetailsLoanList()
                           select item).ToList();
            if (approve.Count > 0)
            {
                gdvloanapprove.DataSource = approve;
                gdvloanapprove.DataBind();
            }
            else
            {
                gdvloanapprove.DataSource = approve;
                gdvloanapprove.DataBind();
            }
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Administrator/AdminDasboard.aspx");
        }

    }
}