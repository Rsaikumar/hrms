﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


#region Private Using
using DAL;
using BAL;
using System.Configuration;
using System.Data;
using System.Reflection;
using System.Data.Linq.SqlClient;
// // // Name Space For BAL & DAL ClassLibrary
#endregion

namespace HRMS.Administrator
{
    public partial class CompanyInformation : System.Web.UI.Page
    {
        // // // Object Creation For Database
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();



        // // /// Object Creation For Item Group Domain
        Administrator_CompanyInfoBAL objSCM_userRegistration = new Administrator_CompanyInfoBAL();

        // // // // Main Method

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["ClientRegistrationNo"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                //txtSearch.Focus();

                //Bind Company information
                Bind_Grid();

            }



        }

        #region Button Events

        // // // Navigation to Company Information Form For Created New Company 
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            // if ((string)Session["User_Type"] == "SuperAdmin")
            // {
            Response.Redirect("AddCompanyInfomation.aspx");
            // }

            //  else
            //  {
            //     AdminPermissionAdd();
            // }
        }

        /// <summary>
        /// Search For Company Information Details
        /// Created By Divya
        /// </summar
        /// 

        //protected void btnSearch_Click(object sender, EventArgs e)
        //{
        //    try
        //    {

        //       // string StrSearch = txtSearch.Text;
        //        string CompanyName = Session["ClientRegistrationNo"].ToString();
        //        List<Administrator_CompanyInfo> objProductSearch = objSCM_userRegistration.GetData_search_CompanyInfo(StrSearch, CompanyName);

        //        if (objProductSearch.Count > 0)
        //        {
        //            grdCompanyInformation.DataSource = objProductSearch;
        //            grdCompanyInformation.DataBind();
        //        }
        //        else
        //        {
        //            SetInitialRow();
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.ToString() + "');", true);
        //    }
        //}

        /// <summary>
        /// Navigation for Company Information create
        /// Created By Divya
        /// </summar
        /// 

        protected void lkbUserRegistration_Click(object sender, EventArgs e)
        {
            // if ((string)Session["User_Type"] == "SuperAdmin")
            //  {
            Response.Redirect("AddUserRegistration.aspx");
            // }

            // else
            // {
            // AdminPermissionAdd();
            // }
        }


        #endregion

        #region Methods

        /// <summary>
        /// Bind Grid Based on User Registration Details
        /// Created By Divya
        /// </summary>
        /// <param name="sender"></param>
        public void Bind_Grid()
        {
            try
            {
                List<Administrator_CompanyInfo> ObjCompanyInfo = objSCM_userRegistration.GetData_CompanyInfo(Session["ClientRegistrationNo"].ToString());
                if (ObjCompanyInfo.Count > 0)
                {
                    grdCompanyInformation.DataSource = ObjCompanyInfo;
                    grdCompanyInformation.DataBind();

                }
                else
                {
                    // // // // Grid view using setting initial rows displaying Empty Header Displying
                    SetInitialRow();
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }


        /// <summary>
        /// Setting Initial Empty Row Data Into Company Information Details 
        /// Created By Divya
        /// </summary>
        /// <param name="sender"></param>
        public void SetInitialRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("ClientRegistrationNo", typeof(string)));
            dt.Columns.Add(new DataColumn("Company_Name", typeof(string)));
            dt.Columns.Add(new DataColumn("State", typeof(string)));

            dt.Columns.Add(new DataColumn("Pincode", typeof(string)));
            dt.Columns.Add(new DataColumn("E_Mail", typeof(string)));

            dt.Columns.Add(new DataColumn("Web_URL", typeof(string)));
            dt.Columns.Add(new DataColumn("TIN_No", typeof(string)));


            dr = dt.NewRow();


            dr["ClientRegistrationNo"] = string.Empty;
            dr["Company_Name"] = string.Empty;
            dr["State"] = string.Empty;
            dr["Pincode"] = string.Empty;
            dr["E_Mail"] = string.Empty;
            dr["Web_URL"] = string.Empty;
            dr["TIN_No"] = string.Empty;



            dt.Rows.Add(dr);


            grdCompanyInformation.DataSource = dt;
            grdCompanyInformation.DataBind();

            int columncount = 7;
            grdCompanyInformation.Rows[0].Cells.Clear();
            grdCompanyInformation.Rows[0].Cells.Add(new TableCell());
            grdCompanyInformation.Rows[0].Cells[0].ColumnSpan = columncount;
            grdCompanyInformation.Rows[0].Cells[0].Text = "No Records Found";
            //  grdStorageLocation.HeaderStyle.HorizontalAlign = "center";
            grdCompanyInformation.Rows[0].Cells[0].ForeColor = System.Drawing.Color.Red;
        }

        /// <summary>
        ///  The Following Method is used for Delete Grid records to Data From User Registration  Table
        ///  Created By Divya
        /// </summary>
        public void Delete_UserRegistrationDetails(string LogID, string Creation_Company)
        {
            try
            {
                Administrator_CompanyInfo a = objSCM_userRegistration.Delete_CompanyDetails(LogID, Session["ClientRegistrationNo"].ToString());

                if (a.Creation_Company != null)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Company Details Deleted Successfully ');", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Company Details Already Existing In User Registration');", true);
                }
                Bind_Grid();
                //txtSearch.Focus();

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.ToString() + "');", true);
            }
            finally
            {
            }
        }

        #endregion

        #region Company Information Details Bind For Grid view


        /// <summary>
        /// Row Command Grid view  For Company Information Details
        /// Created By Divya
        /// </summar
        /// 

        protected void grdUserRegistration_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string LoginID = e.CommandArgument.ToString();
            if (e.CommandName == "Edit")
            {
                //string path = "AddUserRegistration.aspx?id=" + LoginID;
                string path = "AddCompanyInfomation.aspx";
                Session["EditCompany"] = LoginID;
                Response.Redirect(path);
                //   AdminPermissionModify(LoginID);

            }
            if (e.CommandName == "Delete")
            {
                // AdminPermissionDelete(LoginID);
                string Creation_Company = Session["ClientRegistrationNo"].ToString();

                Delete_UserRegistrationDetails(LoginID, Creation_Company);

            }


        }


        /// <summary>
        /// Paging For Company Information Details
        /// Created By Divya
        /// </summar
        /// 
        protected void grdUserRegistration_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdCompanyInformation.PageIndex = e.NewPageIndex;
            Bind_Grid();
        }


        /// <summary>
        /// Row Delte Grid view  For Company Information
        /// Created By Divya
        /// </summar
        /// 
        protected void grdUserRegistration_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        /// <summary>
        /// Row Editing Grid view  For Company Information
        /// Created By Divya
        /// </summar
        /// 

        protected void grdUserRegistration_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        /// <summary>
        /// Row Sorting Grid view  For Company Information Details
        /// Created By Divya
        /// </summar
        /// 

        protected void grdUserRegistration_Sorting(object sender, GridViewSortEventArgs e)
        {
            string Sortdir = GetSortDirection(e.SortExpression);
            string SortExp = e.SortExpression;
            String Creation_Company = Session["ClientRegistrationNo"].ToString();
            List<Administrator_CompanyInfo> list = objSCM_userRegistration.GetData_CompanyInfo(Creation_Company);


            if (Sortdir == "ASC")
            {
                list = Sort<Administrator_CompanyInfo>(list, SortExp, SortDirection.Ascending);
            }
            else
            {
                list = Sort<Administrator_CompanyInfo>(list, SortExp, SortDirection.Descending);
            }
            this.grdCompanyInformation.DataSource = list;
            this.grdCompanyInformation.DataBind();
        }

        private List<Administrator_CompanyInfo> Sort<TKey>(List<Administrator_CompanyInfo> list, string sortBy, SortDirection direction)
        {
            PropertyInfo property = list.GetType().GetGenericArguments()[0].GetProperty(sortBy);
            if (direction == SortDirection.Ascending)
            {
                return list.OrderBy(e => property.GetValue(e, null)).ToList<Administrator_CompanyInfo>();
            }
            else
            {
                return list.OrderByDescending(e => property.GetValue(e, null)).ToList<Administrator_CompanyInfo>();
            }
        }

        //  GetSortDirection Method it using PRoduct Grid Sorting  
        private string GetSortDirection(string column)
        {
            string sortDirection = "ASC";
            string sortExpression = ViewState["SortExpression"] as string;
            if (sortExpression != null)
            {
                if (sortExpression == column)
                {
                    string lastDirection = ViewState["SortDirection"] as string;
                    if ((lastDirection != null) && (lastDirection == "ASC"))
                    {
                        sortDirection = "DESC";
                    }
                }
            }
            ViewState["SortDirection"] = sortDirection;
            ViewState["SortExpression"] = column;
            return sortDirection;
        }

        #endregion



    

    }
}