﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="HRMS.Administrator.ChangePassword" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link href="../css/Administrator/ChangePassword.css" rel="stylesheet" />

    <script type="text/javascript">
        function CheckPasswordStrength(password) {
            var password_strength = document.getElementById("password_strength");

            //TextBox left blank.
            if (password.length == 0) {
                password_strength.innerHTML = "";
                return;
            }
            //Regular Expressions.
            var regex = new Array();
            regex.push("[A-Z]"); //Uppercase Alphabet.
            regex.push("[a-z]"); //Lowercase Alphabet.
            regex.push("[0-9]"); //Digit.
            regex.push("[$@$!%*#?&]"); //Special Character.

            var passed = 0;

            //Validate for each Regular Expression.
            for (var i = 0; i < regex.length; i++) {
                if (new RegExp(regex[i]).test(password)) {
                    passed++;
                }
            }
            //Validate for length of Password.
            if (passed > 2 && password.length > 8) {
                passed++;
            }

            //Display status.
            var color = "";
            var strength = "";
            switch (passed) {
                case 0:
                case 1:
                    strength = "Weak";
                    color = "red";
                    break;
                case 2:
                    strength = "Good";
                    color = "darkorange";
                    break;
                case 3:
                case 4:
                    strength = "Strong";
                    color = "green";
                    break;
                case 5:
                    strength = "Very Strong";
                    color = "darkgreen";
                    break;
            }
            password_strength.innerHTML = strength;
            password_strength.style.color = color;
        }
    </script>
    <script type="text/javascript">
        function checkPasswordLength(txtnewpsw) {
            var textbox = txtnewpsw;
            var regularExpression = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/;
            if (textbox.value.length < 8) {
                //alert("success");
                txtnewpsw.value = "";
                alert("make sure the Password is between atleast 8 characters & Any Special Characters long");
                txtnewpsw.focus();
                $("#txtNewPwd").focus();

                return false;
            }
            else {
                document.getElementById('<%=txtCPwd.ClientID%>').focus();
            }
            if (!regularExpression.test(txtnewpsw.value)) {
                alert("password should contain atleast one number and one special character");
                txtnewpsw.value = "";
                $("#txtNewPwd").focus();
                return false;
            }
            else {
                //document.getElementById('<%=txtCPwd.ClientID%>').focus();
                return true;
            }
        }

    </script>

    <script type="text/javascript">
        function Validate() {
            var password = document.getElementById('<%=txtNewPwd.ClientID%>');
            var confirmPassword = document.getElementById('<%=txtCPwd.ClientID%>');
            var valid = password.value == confirmPassword.value;
            if (password.value != confirmPassword.value)
            {
                alert("Passwords mis-match.");
                document.getElementById('<%=txtCPwd.ClientID%>').focus();
                document.getElementById('<%=txtCPwd.ClientID%>').value = "";
                $("#txtNewPwd").focus();
                return false;
            }
           
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx">SCM</a> <i class="fa fa-angle-right"></i>
                    </li>

                    <li><a href="ChangePassword.aspx">Change Password</a> </li>
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                                <div class="box-title">
                                    <h3>
                                        <i class="fa fa-th-list"></i>Change Password</h3>
                                </div>
                                <div class="box-content">
                                    <form action="#" class='form-horizontal form-bordered'>
                                        <div class="col-sm-12" style="">
                                            <div class="row">
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 loldpassword" style="">
                                                        Old Password<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                    <div class="col-sm-2 toldpassword" style="">
                                                        <asp:TextBox ID="txtOldPwd" runat="server" placeholder="Old Password" class="form-control"
                                                            MaxLength="20" AutoPostBack="true" OnTextChanged="txtOldPwd_TextChanged"></asp:TextBox>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-sm-12" style="">
                                            <div class="row">
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lNewPassword" style="">
                                                        New Password<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                    <div class="col-sm-2 tNewPassword" style="">
                                                        <asp:TextBox ID="txtNewPwd" runat="server" placeholder="New Password" class="form-control"
                                                            MaxLength="20" onChange="return checkPasswordLength(this);" onkeyup="CheckPasswordStrength(this.value)"></asp:TextBox>

                                                    </div>
                                                    <div>
                                                        <span id="password_strength"></span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-sm-12" style="">
                                            <div class="row">
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lconfromPassword" style="">
                                                        Confirm Password<span style="color: #ff0000; font-size: 14px;">*</span>
                                                    </label>
                                                    <div class="col-sm-2 tconfromPassword" style="">
                                                        <asp:TextBox ID="txtCPwd" runat="server" placeholder="Confirm Password" class="form-control"
                                                            MaxLength="20" onChange="return Validate();"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions col-sm-offset-2 col-sm-10 IGButtons">
                                            <asp:Button ID="btnUpdate" runat="server" Text="Change" class="btn btn-primary" OnClick="btnUpdate_Click" />
                                            <asp:Button ID="btnCancel" runat="server" Text="Reset" class="btn btn-primary" /><%--OnClick="btnCancel_Click"--%>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
