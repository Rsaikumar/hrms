﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/Admin.Master" AutoEventWireup="true" CodeBehind="DesignationView.aspx.cs" Inherits="HRMS.Administrator.DesignationView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
        <section class="wrapper">

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="../Administrator/DesignationView.aspx" class="current">Designation View </a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">

                        <div class="panel-body">
                            <div class="tab-content">

                                <div class="col-md-12">
                                    <div class="row">

                                        <section class="panel">
                                            <header class="panel-heading">
                                                <i class="fa fa-th-list">Designation Name</i>
                                            </header>
                                            <div class="panel-body">

                                                <div class="form-group col-md-6" id="divhide" runat="server">
                                                    <label for="textfield" class="control-label col-sm-4" style="">
                                                        DesignationMaster ID</label>
                                                    <div class="col-sm-8" style="" id="">
                                                        <asp:TextBox ID="txtdesignationid" runat="server" class="form-control"
                                                            AutoPostBack="true" MaxLength="50">
                                                        </asp:TextBox>

                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-4" style="">
                                                        Designation Name</label>
                                                    <div class="col-sm-8" style="" id="Div1">
                                                        <asp:TextBox ID="txtDesignationname" runat="server" placeholder="Designation Name" required class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 text-center">
                                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-primary" OnClick="btnUpdate_Click1" /><%--OnClientClick="return fileUploadValidation()"--%>
                                                    <asp:Button ID="btndelete" runat="server" Text="Delete" class="btn btn-danger" OnClick="btndelete_Click" OnClientClick="javascript:return confirm('Are you sure you want to delete this class?');" /><%--OnClientClick="return fileUploadValidation()"--%>
                                                    <asp:Button ID="btncancle" runat="server" Text="Cancel" class="btn btn-primary" OnClick="btncancle_Click" /><%--OnClientClick="return fileUploadValidation()"--%>

                                                </div>

                                            </div>


                                        </section>
                                    </div>

                                </div>
                            </div>

                        </div>
                        </section>
                    </div>
                
                </div>
        </section>
       
    </section>

</asp:Content>
