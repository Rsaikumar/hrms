﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Administrator
{
    public partial class Attendance : System.Web.UI.Page
    {
        #region Delaclartion

        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();

      

        #endregion
      
        #region Methods

        public void Attendancebind()
        {

            var objbindattendancegrid = (from ST in ObjDAL.USP_GetEmployeeDetailsAttendace()
                                         select ST).ToList();
            if (objbindattendancegrid.Count() >= 0)
            {
                gvdAttendancelist.DataSource = objbindattendancegrid;
                gvdAttendancelist.DataBind();
            }

            else
            {
                gvdAttendancelist.DataSource = objbindattendancegrid;
                gvdAttendancelist.DataBind();
            }
        }

        public void AttendanceList()
        {
            var objbindleavegrid = (from ST in ObjDAL.AttendanceLists
                                    select ST).ToList();
            if (objbindleavegrid.Count() >= 0)
            {
                gvdattendance.DataSource = objbindleavegrid;
                gvdattendance.DataBind();
            }

            else
            {
                gvdattendance.DataSource = objbindleavegrid;
                gvdattendance.DataBind();
            }
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Attendancebind();
                AttendanceList();

            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                foreach (GridViewRow row in gvdAttendancelist.Rows)
                {
                    Label EmpId = (Label)gvdAttendancelist.Rows[row.RowIndex].FindControl("lblempid");
                    Label EmpName = (Label)gvdAttendancelist.Rows[row.RowIndex].FindControl("lblname");
                    Label Designation = (Label)gvdAttendancelist.Rows[row.RowIndex].FindControl("lbldesignation");
                    DropDownList Status = (DropDownList)gvdAttendancelist.Rows[row.RowIndex].FindControl("ddlstatus");

                    AttendanceList objattendance = new AttendanceList();

                    objattendance.EmpId = EmpId.Text.ToString();
                    objattendance.Name = EmpName.Text.ToString();
                    objattendance.Designation = Designation.Text.ToString();
                    objattendance.Status = Status.SelectedItem.Text.ToString();
                    objattendance.Date = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd"));
                    objattendance.CreatedBy = Session["Emp_Code"].ToString();
                    objattendance.CreatedOn = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd"));
                    ObjDAL.AttendanceLists.InsertOnSubmit(objattendance);

                }
                ObjDAL.SubmitChanges();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Saved Successfully');window.location ='../Administrator/AdminDasboard.aspx';", true);
                AttendanceList();
            }

            catch (Exception ex)
            {

                ex.Message.ToString();
            }





        }

        protected void gvdattendance_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

            }
        }

        protected void gvdattendance_SelectedIndexChanged(object sender, EventArgs e)
        {
           string Empid = gvdattendance.SelectedRow.Cells[0].Text;
            string path = "~/Administrator/AttendanceListView.aspx";
           Session["EmployeeDetailsCode"] = Empid;
            Response.Redirect(path);
        }

        #endregion

    }
}