﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="PaySlipMaster.aspx.cs" Inherits="HRMS.Administrator.PaySlipMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.js"></script>
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="#">Administrator</a><i class="fa fa-angle-right"></i> </li>
                        <li><a href="../Administrator/EmployeeDetailsList.aspx" class="current">PaySlips</a> </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <i class="fa fa-th-list">PaySlip</i>
                        </header>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-6" style="">
                                        Emp ID
                                    </label>
                                    <div class="col-sm-6" style="">
                                        <label for="textfield" id="lblempid" runat="server" class="control-label" style=""></label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-6" style="">
                                        Name
                                    </label>
                                    <div class="col-sm-6" style="">
                                        <label for="textfield" id="lblname" runat="server" class="control-label col-sm-5" style=""></label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-6" style="">
                                        Department
                                    </label>
                                    <div class="col-sm-6" style="">
                                        <label for="textfield" id="lbldepartment" runat="server" class="control-label" style=""></label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-6" style="">
                                        Designation
                                    </label>
                                    <div class="col-sm-6" style="">
                                        <label for="textfield" id="lbldesignation" runat="server" class="control-label " style=""></label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-6" style="">
                                        Date Of Birth
                                    </label>
                                    <div class="col-sm-6" style="">
                                        <label for="textfield" id="lbldob" runat="server" class="control-label" style=""></label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-6" style="">
                                        Bank Account No
                                    </label>
                                    <div class="col-sm-6" style="">
                                        <label for="textfield" id="lblaccno" runat="server" class="control-label" style=""></label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-6" style="">
                                        Date Of Joining
                                    </label>
                                    <div class="col-sm-6" style="">
                                        <label for="textfield" id="lbldoj" runat="server" class="control-label" style=""></label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <h5 style="color: rgb(236, 7, 7);">Earnings</h5>
                                        <hr />
                                    </div>
                                    <div class="col-md-6">
                                        <h5 style="color: rgb(236, 7, 7);">Deductions</h5>
                                        <hr />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-6" style="">
                                        Basic Pay
                                    </label>
                                    <div class="col-sm-6" style="">
                                        <label for="textfield" id="lblbasicpay" runat="server" class="control-label" style=""></label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-6" style="">
                                        TDS 
                                    </label>
                                    <div class="col-sm-6" style="">
                                        <label for="textfield" id="lbltdsamt" runat="server" class="control-label" style=""></label>
                                    </div>
                                </div>
                                    <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-6" style="">
                                        CCA 
                                    </label>
                                    <div class="col-sm-6" style="">
                                        <label for="textfield" id="lblcca" runat="server" class="control-label" style=""></label>
                                    </div>
                                </div>
                                  <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-6" style="">
                                        Provident Fund 
                                    </label>
                                    <div class="col-sm-6" style="">
                                        <label for="textfield" id="lblpfamt" runat="server" class="control-label" style=""></label>
                                    </div>
                                </div>
                                 <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-6" style="">
                                        DA 
                                    </label>
                                    <div class="col-sm-6" style="">
                                        <label for="textfield" id="lbldaamt" runat="server" class="control-label" style=""></label>
                                    </div>
                                </div>
                                 <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-6" style="">
                                        ESI 
                                    </label>
                                    <div class="col-sm-6" style="">
                                        <label for="textfield" id="lblesiamt" runat="server" class="control-label" style=""></label>
                                    </div>
                                </div>
                                 <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-6" style="">
                                        HRA
                                    </label>
                                    <div class="col-sm-6" style="">
                                        <label for="textfield" id="lblhraamt" runat="server" class="control-label" style=""></label>
                                    </div>
                                </div>
                                 <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-6" style="">
                                        Professional Tax 
                                    </label>
                                    <div class="col-sm-6" style="">
                                        <label for="textfield" id="lblptamt" runat="server" class="control-label" style=""></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
