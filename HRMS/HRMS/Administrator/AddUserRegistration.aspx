﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="AddUserRegistration.aspx.cs" Inherits="HRMS.Administrator.AddUserRegistration" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/Administrator/UserRegistration.css" rel="stylesheet" type="text/css" />
    <script type="text/Javascript">
        function isvalid() {

            if (document.getElementById('<%=txtLoginId.ClientID%>').value == "") {
                alert("Please Provide LoginID");
                document.getElementById('<%=txtLoginId.ClientID%>').focus();
                return false;
            }
            if (document.getElementById('<%=txtPassword.ClientID%>').value == "") {
                alert("Please Enter Password");
                document.getElementById('<%=txtPassword.ClientID%>').focus();
                return false;
            }

            if (document.getElementById('<%=ddlUserType.ClientID%>').options[document.getElementById('<%=ddlUserType.ClientID%>').selectedIndex].value == "0") {
                alert("Please Select User Type");
                document.getElementById('<%=ddlUserType.ClientID%>').focus();
                return false;
            }

            if (document.getElementById('<%=ddlStatus.ClientID%>').options[document.getElementById('<%=ddlStatus.ClientID%>').selectedIndex].value == "0") {
                alert("Please Select Status");
                document.getElementById('<%=ddlStatus.ClientID%>').focus();
                return false;
            }


            var atLeast = 1
            var CHK = document.getElementById("<%=chlCompanyLists.ClientID%>");
            var checkbox = CHK.getElementsByTagName("input");
            var counter = 0;
            for (var i = 0; i < checkbox.length; i++) {
                if (checkbox[i].checked) {
                    counter++;
                }
            }
            if (atLeast > counter) {
                alert("Please Check atleast " + atLeast + " Company Name");
                return false;
            }

            return true;
        }

        function checkLength() {
            var textbox = document.getElementById('<%=txtLoginId.ClientID%>');
            if (textbox.value.length <= 10 && textbox.value.length >= 6) {
                //alert("success");
                document.getElementById('<%=txtLoginId.ClientID%>').focus();
                return true;
            }
            else {
                alert("make sure the login Id is between 6-10 characters long");
                document.getElementById('<%=txtLoginId.ClientID%>').value = "";
                document.getElementById('<%=txtLoginId.ClientID%>').focus();

                return false;
            }
        }

        function checkPasswordLength() {
            var textbox = document.getElementById('<%=txtPassword.ClientID%>');
            if (textbox.value.length <= 10 && textbox.value.length >= 6) {
                //alert("success");
                document.getElementById('<%=txtPassword.ClientID%>').focus();
                return true;
            }
            else {
                alert("make sure the Password is between 6-10 characters & Any Special Characters long");
                document.getElementById('<%=txtPassword.ClientID%>').value = "";
                document.getElementById('<%=txtPassword.ClientID%>').focus();

                return false;
            }
        }

        function checkEmail(emailID) {

            var email = emailID;

            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

            if (!filter.test(email.value)) {
                alert('Please provide a valid email address');
                email.focus();
                email.value = "";
                return false;
            }
            else {

                return true;
            }
            return true;
        }        

    </script>

       <script type="text/javascript">
    function CheckPasswordStrength(password) {
        var password_strength = document.getElementById("password_strength");
 
        //TextBox left blank.
        if (password.length == 0) {
            password_strength.innerHTML = "";
            return ;
        }
 
        //Regular Expressions.
        var regex = new Array();
        regex.push("[A-Z]"); //Uppercase Alphabet.
        regex.push("[a-z]"); //Lowercase Alphabet.
        regex.push("[0-9]"); //Digit.
        regex.push("[$@$!%*#?&]"); //Special Character.
 
        var passed = 0;
 
        //Validate for each Regular Expression.
        for (var i = 0; i < regex.length; i++) {
            if (new RegExp(regex[i]).test(password)) {
                passed++;
            }
        }
 
        //Validate for length of Password.
        if (passed > 2 && password.length > 8) {
            passed++;
        }
 
        //Display status.
        var color = "";
        var strength = "";
        switch (passed) {
            case 0:
            case 1:
                strength = "Weak";
                color = "red";
                break;
            case 2:
                strength = "Good";
                color = "darkorange";
                break;
            case 3:
            case 4:
                strength = "Strong";
                color = "green";
                break;
            case 5:
                strength = "Very Strong";
                color = "darkgreen";
                break;
        }
        password_strength.innerHTML = strength;
        password_strength.style.color = color;
      
    }
</script>
    
    <script type="text/javascript">
        function checkPasswordLength(txtnewpsw) {
            var textbox = txtnewpsw;
            var regularExpression = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/;
            if (textbox.value.length < 8) {
                //alert("success");
                txtnewpsw.value = "";
                alert("make sure the Password is between atleast 8 characters & Any Special Characters long");
                txtnewpsw.focus();
                document.getElementById('<%=txtPassword.ClientID%>').focus();

                return false;
            }
            else {
                document.getElementById('<%=txtPassword.ClientID%>').focus();
            }
            if (!regularExpression.test(txtPassword.value)) {
                alert("password should contain atleast one number and one special character");
                txtnewpsw.value = "";
                document.getElementById('<%=txtPassword.ClientID%>').focus();

                return false;
            }
            else {
                //document.getElementById('<%=txtPassword.ClientID%>').focus();
                return true;
            }
        }

    </script>

         
    <style>
        #ContentPlaceHolder1_Modify_completionListElem
        {
            visibility: visible;
        }
    </style>
    <style type="text/css">
        .completionList
        {
            border: solid 1px Gray;
            margin: 0px;
            padding: 3px;
            height: 120px;
            overflow: auto;
            background-color: #FFFFFF;
            z-index: 10000;
        }
        
        .listItem
        {
            color: #191919;
        }
        
        .itemHighlighted
        {
            background-color: #ADD6FF;
        }
       
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

        <div id="main">
        <div class="container-fluid">
           <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">Administrator</a><i class="fa fa-angle-right"></i>
                    </li>
                    <li><a href="UserRegistration.aspx">User Registration List</a> <i class="fa fa-angle-right"></i>
                    </li>
                    <li><a href="AddUserRegistration.aspx">User Registration</a> </li>
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                                <div class="box-title">
                                    <h3>
                                        <i class="fa fa-th-list"></i>User Registration</h3>
                                </div>
                                <div class="box-content">
                                    <form action="#" method="POST" class='form-horizontal form-bordered'>
                                      
                                       

                                                                 <div class="col-sm-12" style="">
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lblAULoginID" style="">
                                                Log in ID<span style="color: #ff0000; font-size: 14px;">*</span>
                                            </label>
                                            <div class="col-sm-2 tAULoginID" style="">
                                                  <asp:TextBox ID="txtLoginId" runat="server" placeholder="Log in ID" class="form-control"
                                                    MaxLength="10"  AutoPostBack="true" OnTextChanged="txtLoginId_OnTextChanged"
                                                    onchange="checkLength(this)"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lblAUPassword" style="">
                                                Password<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                            <div class="col-sm-2 tAUPassword" style="">
                                                <asp:TextBox ID="txtPassword" runat="server" placeholder="Password" class="form-control"
                                                    MaxLength="10"  TextMode="Password" onkeyup="CheckPasswordStrength(this.value)" onChange="return checkPasswordLength(this);"></asp:TextBox>
                                            </div>
                                             <div>
                                                        <span id="password_strength"></span>
                                                    </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="">
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lblAUEmployeeName" style="">
                                                Employee Name
                                            </label>
                                            <div class="col-sm-5 tAUEmployeeName" style="">
                                                <asp:TextBox ID="txtEmployeeName" runat="server" placeholder=" Employee  Name" class="form-control"
                                                    MaxLength="20" ></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="">
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lblAUEmail" style="">
                                                Email
                                            </label>
                                            <div class="col-sm-5 tAUEmail" style="">
                                              <asp:TextBox ID="txtEmail" runat="server" placeholder="Email" class="form-control"
                                                    MaxLength="25"  onchange="checkEmail(this)"></asp:TextBox>
                                            </div>
                                        </div>
                                       
                                    </div>
                                    <div class="col-sm-12" style="">
                                     <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lblAUUserType" style="">
                                                User Role<span style="color: #ff0000; font-size: 14px;">*</span>
                                            </label>
                                            <div class="col-sm-2 tAUUserType" style="" id="">
                                                <asp:DropDownList ID="ddlUserType" runat="server" class="form-control DropdownCss"
                                                    >
                                                    <asp:ListItem Text="-- User Role --" Value="0"></asp:ListItem>
                                                     <asp:ListItem Text="APCCF" Value="APCCF"></asp:ListItem>
                                                    <asp:ListItem Text="HOFF" Value="HOFF"></asp:ListItem>
                                                    <asp:ListItem Text="POA" Value="POA"></asp:ListItem>
                                                    <asp:ListItem Text="CCF" Value="CCF"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group" style="display:none;">
                                            <label for="textfield" class="control-label col-sm-1 lblAUCompanyID" style="">
                                                Company ID<span style="color: #ff0000; font-size: 14px;">*</span>
                                            </label>
                                            <div class="col-sm-2 tAUCompanyID" style="">
                                                <asp:TextBox ID="txtCompanyID" runat="server" placeholder="Company ID" class="form-control"
                                                    MaxLength="10"  ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lblAUStatus" style="">
                                                Status<span style="color: #ff0000; font-size: 14px;">*</span>
                                            </label>
                                            <div class="col-sm-2 tAUStatus" style="">
                                                <asp:DropDownList ID="ddlStatus" runat="server" class="form-control DropdownCss"
                                                    >
                                                    <asp:ListItem Text="-- Status --" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Active " Value="Active"></asp:ListItem>
                                                    <asp:ListItem Text="De-active " Value="De-active"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>

                                        <div class="col-sm-12" style="display:none;">
                                     <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lblAUUserType" style="">
                                                Branch ID<span style="color: #ff0000; font-size: 14px;">*</span>
                                            </label>
                                            <div class="col-sm-2 tAUUserType" style="" id="Div1">
                                                <asp:DropDownList ID="ddlBranchID" runat="server" class="form-control DropdownCss">                                                   
                                                    
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        
                                    </div>

                                  

                                      <div class="col-sm-12" style="">
                                      <div class="form-group ">
                                            <label for="textfield" class="control-label col-sm-1 lblAUCompanyID" style="">
                                             Allowed Company Lists<span style="color: #ff0000; font-size: 14px;">*</span>
                                            </label>
                                            <div class="col-sm-8 tAUCompanyID " style="">
                                                <asp:CheckBoxList ID="chlCompanyLists" runat="server" class="ListControl" RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="3"
                                                    Width="100%" >
                                                
                                                </asp:CheckBoxList>
                                            </div>

                                      </div>
                                    </div>
                                  
                                   <div class="col-sm-12" style="display:block;">
                                        <asp:Panel ID="Panel1" runat="server" GroupingText="Form Level Permissions" Style="border-bottom: 0px solid #8F8F8F;">
                                            <div class="col-sm-12" style="margin-bottom: 0.3%;">
                                                <div class="row">
                                                    <table class="table table-hover table-nomargin table-bordered" style="margin-bottom: 0.53%;
                                                        margin-top: 0.53%;">
                                                        <thead>
                                                            <tr>
                                                                <th class='hidden-480 rBRepNo' style="">
                                                                    Form/Report Name
                                                                </th>
                                                                <th class='hidden-480 rBRepDate' style="">
                                                                    Add/Create
                                                                </th>
                                                                <th class='hidden-480 rBVersionNo' style="">
                                                                    Modify
                                                                </th>
                                                                <th class='hidden-480 rBVersionDate' style="">
                                                                    Authorize/Approve
                                                                </th>
                                                                <th class='hidden-480 rBQuotNo' style="">
                                                                    View/Print
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <label for="textfield" class="control-label col-sm-1 lblAUStatus" style="">
                                                                        Administrator
                                                                    </label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtAdministratorCreate" runat="server" placeholder="Create" class="form-control"></asp:TextBox>
                                                                    <cc1:AutoCompleteExtender ServiceMethod="GetCompletionList" MinimumPrefixLength="1"
                                                                        CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtAdministratorCreate"
                                                                        CompletionListCssClass="completionList" CompletionListItemCssClass="listItem"
                                                                        CompletionListHighlightedItemCssClass="itemHighlighted" ID="AdministratorCreate"
                                                                        runat="server" FirstRowSelected="false" DelimiterCharacters=", " ShowOnlyCurrentWordInCompletionListItem="true">
                                                                    </cc1:AutoCompleteExtender>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtAdministratorModify" runat="server" placeholder="Modify" class="form-control"></asp:TextBox>
                                                                    <cc1:AutoCompleteExtender ServiceMethod="GetCompletionList" MinimumPrefixLength="1"
                                                                        CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtAdministratorModify"
                                                                        CompletionListCssClass="completionList" CompletionListItemCssClass="listItem"
                                                                        CompletionListHighlightedItemCssClass="itemHighlighted" ID="AutoCompleteExtender1"
                                                                        runat="server" FirstRowSelected="false" DelimiterCharacters=", " ShowOnlyCurrentWordInCompletionListItem="true">
                                                                    </cc1:AutoCompleteExtender>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtAdministratorApprove" runat="server" placeholder="Approve" class="form-control" style="display:none;"></asp:TextBox>
                                                                    <cc1:AutoCompleteExtender ServiceMethod="GetCompletionList" MinimumPrefixLength="1"
                                                                        CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtAdministratorApprove"
                                                                        CompletionListCssClass="completionList" CompletionListItemCssClass="listItem"
                                                                        CompletionListHighlightedItemCssClass="itemHighlighted" ID="AdministratorApprove"
                                                                        runat="server" FirstRowSelected="false" DelimiterCharacters=", " ShowOnlyCurrentWordInCompletionListItem="true">
                                                                    </cc1:AutoCompleteExtender>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtAdministratorView" runat="server" placeholder="View" class="form-control" style="display:none;"></asp:TextBox>
                                                                    <cc1:AutoCompleteExtender ServiceMethod="GetCompletionList" MinimumPrefixLength="1"
                                                                        CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtAdministratorView"
                                                                        CompletionListCssClass="completionList" CompletionListItemCssClass="listItem"
                                                                        CompletionListHighlightedItemCssClass="itemHighlighted" ID="AdministratorView"
                                                                        runat="server" FirstRowSelected="false" DelimiterCharacters=", " ShowOnlyCurrentWordInCompletionListItem="true">
                                                                    </cc1:AutoCompleteExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <label for="textfield" class="control-label col-sm-1 lblAUStatus" style="">
                                                                        Masters
                                                                    </label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtMasterCreate" runat="server" placeholder="Create" class="form-control"></asp:TextBox>
                                                                    <cc1:AutoCompleteExtender ServiceMethod="GetMasterCompletionList" MinimumPrefixLength="1"
                                                                        CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtMasterCreate"
                                                                        CompletionListCssClass="completionList" CompletionListItemCssClass="listItem"
                                                                        CompletionListHighlightedItemCssClass="itemHighlighted" ID="AutoCompleteExtender2"
                                                                        runat="server" FirstRowSelected="false" DelimiterCharacters=", " ShowOnlyCurrentWordInCompletionListItem="true">
                                                                    </cc1:AutoCompleteExtender>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtMasterModify" runat="server" placeholder="Modify" class="form-control"></asp:TextBox>
                                                                    <cc1:AutoCompleteExtender ServiceMethod="GetMasterCompletionList" MinimumPrefixLength="1"
                                                                        CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtMasterModify"
                                                                        CompletionListCssClass="completionList" CompletionListItemCssClass="listItem"
                                                                        CompletionListHighlightedItemCssClass="itemHighlighted" ID="AutoCompleteExtender3"
                                                                        runat="server" FirstRowSelected="false" DelimiterCharacters=", " ShowOnlyCurrentWordInCompletionListItem="true">
                                                                    </cc1:AutoCompleteExtender>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtMasterApprove" runat="server" placeholder="Approve" class="form-control" style="display:none;"></asp:TextBox>
                                                                    <cc1:AutoCompleteExtender ServiceMethod="GetMasterCompletionList" MinimumPrefixLength="1"
                                                                        CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtMasterApprove"
                                                                        CompletionListCssClass="completionList" CompletionListItemCssClass="listItem"
                                                                        CompletionListHighlightedItemCssClass="itemHighlighted" ID="AutoCompleteExtender4"
                                                                        runat="server" FirstRowSelected="false" DelimiterCharacters=", " ShowOnlyCurrentWordInCompletionListItem="true">
                                                                    </cc1:AutoCompleteExtender>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtMasterView" runat="server" placeholder="View" class="form-control" style="display:none;"></asp:TextBox>
                                                                    <cc1:AutoCompleteExtender ServiceMethod="GetMasterCompletionList" MinimumPrefixLength="1"
                                                                        CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtMasterView"
                                                                        CompletionListCssClass="completionList" CompletionListItemCssClass="listItem"
                                                                        CompletionListHighlightedItemCssClass="itemHighlighted" ID="AutoCompleteExtender5"
                                                                        runat="server" FirstRowSelected="false" DelimiterCharacters=", " ShowOnlyCurrentWordInCompletionListItem="true">
                                                                    </cc1:AutoCompleteExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <label for="textfield" class="control-label col-sm-1 lblAUStatus" style="">
                                                                        Transactions
                                                                    </label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtTransactionsCreate" runat="server" placeholder="Create" class="form-control"></asp:TextBox>
                                                                    <cc1:AutoCompleteExtender ServiceMethod="GetTransactionsCompletionList" MinimumPrefixLength="1"
                                                                        CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtTransactionsCreate"
                                                                        CompletionListCssClass="completionList" CompletionListItemCssClass="listItem"
                                                                        CompletionListHighlightedItemCssClass="itemHighlighted" ID="AutoCompleteExtender6"
                                                                        runat="server" FirstRowSelected="false" DelimiterCharacters=", " ShowOnlyCurrentWordInCompletionListItem="true">
                                                                    </cc1:AutoCompleteExtender>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtTransactionsModify" runat="server" placeholder="Modify" class="form-control"></asp:TextBox>
                                                                    <cc1:AutoCompleteExtender ServiceMethod="GetTransactionsCompletionList" MinimumPrefixLength="1"
                                                                        CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtTransactionsModify"
                                                                        CompletionListCssClass="completionList" CompletionListItemCssClass="listItem"
                                                                        CompletionListHighlightedItemCssClass="itemHighlighted" ID="AutoCompleteExtender7"
                                                                        runat="server" FirstRowSelected="false" DelimiterCharacters=", " ShowOnlyCurrentWordInCompletionListItem="true">
                                                                    </cc1:AutoCompleteExtender>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtTransactionsApprove" runat="server" placeholder="Approve" class="form-control"></asp:TextBox>
                                                                    <cc1:AutoCompleteExtender ServiceMethod="GetTransactionsCompletionList" MinimumPrefixLength="1"
                                                                        CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtTransactionsApprove"
                                                                        CompletionListCssClass="completionList" CompletionListItemCssClass="listItem"
                                                                        CompletionListHighlightedItemCssClass="itemHighlighted" ID="AutoCompleteExtender8"
                                                                        runat="server" FirstRowSelected="false" DelimiterCharacters=", " ShowOnlyCurrentWordInCompletionListItem="true">
                                                                    </cc1:AutoCompleteExtender>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtTransactionsView" runat="server" placeholder="View" class="form-control"></asp:TextBox>
                                                                    <cc1:AutoCompleteExtender ServiceMethod="GetTransactionsCompletionList" MinimumPrefixLength="1"
                                                                        CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtTransactionsView"
                                                                        CompletionListCssClass="completionList" CompletionListItemCssClass="listItem"
                                                                        CompletionListHighlightedItemCssClass="itemHighlighted" ID="AutoCompleteExtender9"
                                                                        runat="server" FirstRowSelected="false" DelimiterCharacters=", " ShowOnlyCurrentWordInCompletionListItem="true">
                                                                    </cc1:AutoCompleteExtender>
                                                                </td>
                                                            </tr>

                                                             <tr>
                                                                <td>
                                                                    <label for="textfield" class="control-label col-sm-1 lblAUStatus" style="">
                                                                       Reports
                                                                    </label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtReportsCreate" runat="server" placeholder="Create" class="form-control" style="display:none;"></asp:TextBox>
                                                                    <cc1:AutoCompleteExtender ServiceMethod="GetReportsCompletionList" MinimumPrefixLength="1"
                                                                        CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtReportsCreate"
                                                                        CompletionListCssClass="completionList" CompletionListItemCssClass="listItem"
                                                                        CompletionListHighlightedItemCssClass="itemHighlighted" ID="AutoCompleteExtender10"
                                                                        runat="server" FirstRowSelected="false" DelimiterCharacters=", " ShowOnlyCurrentWordInCompletionListItem="true">
                                                                    </cc1:AutoCompleteExtender>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtReportsModify" runat="server" placeholder="Modify" class="form-control" style="display:none;"></asp:TextBox>
                                                                    <cc1:AutoCompleteExtender ServiceMethod="GetReportsCompletionList" MinimumPrefixLength="1"
                                                                        CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtReportsModify"
                                                                        CompletionListCssClass="completionList" CompletionListItemCssClass="listItem"
                                                                        CompletionListHighlightedItemCssClass="itemHighlighted" ID="AutoCompleteExtender11"
                                                                        runat="server" FirstRowSelected="false" DelimiterCharacters=", " ShowOnlyCurrentWordInCompletionListItem="true">
                                                                    </cc1:AutoCompleteExtender>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtReportsApprove" runat="server" placeholder="Approve" class="form-control" style="display:none;"></asp:TextBox>
                                                                    <cc1:AutoCompleteExtender ServiceMethod="GetReportsCompletionList" MinimumPrefixLength="1"
                                                                        CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtReportsApprove"
                                                                        CompletionListCssClass="completionList" CompletionListItemCssClass="listItem"
                                                                        CompletionListHighlightedItemCssClass="itemHighlighted" ID="AutoCompleteExtender12"
                                                                        runat="server" FirstRowSelected="false" DelimiterCharacters=", " ShowOnlyCurrentWordInCompletionListItem="true">
                                                                    </cc1:AutoCompleteExtender>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtReportsView" runat="server" placeholder="View" class="form-control" ></asp:TextBox>
                                                                    <cc1:AutoCompleteExtender ServiceMethod="GetReportsCompletionList" MinimumPrefixLength="1"
                                                                        CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtReportsView"
                                                                        CompletionListCssClass="completionList" CompletionListItemCssClass="listItem"
                                                                        CompletionListHighlightedItemCssClass="itemHighlighted" ID="AutoCompleteExtender13"
                                                                        runat="server" FirstRowSelected="false" DelimiterCharacters=", " ShowOnlyCurrentWordInCompletionListItem="true">
                                                                    </cc1:AutoCompleteExtender>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                             
                              
                                <div class="form-actions col-sm-offset-2 col-sm-10 ACButtons">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary"  OnClientClick="return isvalid();"
                                            OnClick="btnSave_Click" />
                                       <asp:Button ID="btnUpdate" runat="server" Text="Save" class="btn btn-primary" 
                                            OnClick="btnUpdate_Click" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Reset" class="btn" OnClick="btnCancel_Click" />

                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    </div>

               
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

</asp:Content>
