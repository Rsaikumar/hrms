﻿using DAL;
using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace HRMS.Administrator
{
    public partial class EmployeMonthlyPayslipReport : System.Web.UI.Page
    {
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlbind();
            }
        }


        public void ddlbind()
        {
            try
            {
                int empcode = Convert.ToInt32(Session["Emp_Code"].ToString());

                var master = (from item in ObjDAL.HRMSEmployeeMasterRegistrationDetails
                              join per in ObjDAL.HRMSEmplyeeMasterPersonalDetails on item.ID equals Convert.ToInt32(per.EmpId)
                              select per.FirstName + " " + per.MiddleName + " " + per.LastName).ToList();

                if (master.Count > 0)
                {
                    ddlempname.DataSource = master;
                    ddlempname.DataBind();
                    ddlempname.Items.Insert(0, new ListItem("--Select--", "0"));
                }
            }
            catch (Exception e)
            {

            }
        }

        

        public void ClearFields()
        {
            lblempname.InnerText = "";
            lblmonthlydays.InnerText = "";
            lblempid.InnerText = "";
            lblnoofworkingdays.InnerText = "";
            lblbasicamountno.InnerText = "";
            lblabsentdays.InnerText = "";
            lblothrs.InnerText = "";
            lblotcal.InnerText = "";
            lblearningtotal.InnerText = "";
            lbldeductiontotal.InnerText = "";
            lbllossofpay.InnerText = "";
            lbldeductiontotal.InnerText = "";
            lblbasicamountno.InnerText = "";
            lblgrossamount.InnerText = "";
            lblnetamount.InnerText = "";
            lblothrs.InnerText = "";
            // gvddeductions.Columns.Clear();
            //gvdearnings.Columns.Clear();
            gvddeductions.DataSource = "";
            gvdearnings.DataSource = "";
        }

        protected void btnsearch_Click1(object sender, EventArgs e)
        {
            ClearFields();

            int Month = Convert.ToInt32(ddlmonth.SelectedItem.Value);
            string monthname = ddlmonth.SelectedItem.Text;
            int Year = Convert.ToInt32(ddlyear.SelectedItem.Text);
            var Empname = ddlempname.SelectedItem.Text;
            var monthsdays = DateTime.Now.Day;
            var month = DateTime.Now.Month;
            var year = DateTime.Now.Year;
            string[] n = new string[0];
            n = Empname.Split(' ');

            if (year >= Convert.ToInt32(Year))
            {
                if (month >= Convert.ToInt32(Month))
                {

                    var Employeeid = (from item in ObjDAL.HRMSEmplyeeMasterPersonalDetails
                                      where item.FirstName == n[0] && item.MiddleName == n[1] && item.LastName == n[2]
                                      select item).FirstOrDefault();
                    int empcode =Convert.ToInt32( Employeeid.EmpId);

                    lblmonthlydays.InnerText = (DateTime.DaysInMonth(Year, Month)).ToString();
                    int monthlydays = Convert.ToInt32(lblmonthlydays.InnerText);
                    lblpayslipid.InnerText = (monthname + " " + Year).ToString();
                    lblempname.InnerText = Empname;
                    // int empcode = 3072;
                 // int empcode = Convert.ToInt32(Session["Emp_Code"].ToString());
                    lblempid.InnerText = empcode.ToString();
                    //Salary Details
                    var salaryDetails = ObjDAL.HRMSEmployeeMasterSalaryDetails.FirstOrDefault(x => x.EmpId == empcode.ToString());
                    lblbasicamountno.InnerText = (salaryDetails != null ? Convert.ToDecimal(salaryDetails.BasicAmount) : 0).ToString();
                    lblgrossamount.InnerText = (salaryDetails != null ? Convert.ToDecimal(salaryDetails.GrossAmount) : 0).ToString();
                    var earningDetails = ObjDAL.HRMSEmployeeMasterDeductionsDetails.FirstOrDefault(x => x.EmpId == empcode.ToString());
                    lblnetamount.InnerText = (salaryDetails != null ? Convert.ToDecimal(earningDetails.NetAmount) : 0).ToString();

                    //Monthly Timesheet
                    decimal OTMinutes = 0;
                    var timeSheetData = ObjDAL.TimeSheets.Where(x => x.Date.Value.Year == Year &&
                                                                     x.Date.Value.Month == Month &&
                                                                     x.EmpId == empcode).Distinct().ToList();
                    //Loop through time sheet and calculate the total OT hours of the employee
                    foreach (var timeSheet in timeSheetData)
                    {
                        var hours = timeSheet.OTHrs.Value.Hours;
                        var minutes = timeSheet.OTHrs.Value.Minutes;
                        OTMinutes += ((hours * 60) + minutes);
                    }
                    double Othrs = Convert.ToDouble(OTMinutes);
                    TimeSpan ts = TimeSpan.FromMinutes(Othrs);
                    decimal perdaySalary = (Convert.ToDecimal(earningDetails.NetAmount) / monthlydays);
                    decimal perHourSalary = ((Convert.ToDecimal(earningDetails.NetAmount) / monthlydays) / 8);
                    decimal perMinuteSalary = ((Convert.ToDecimal(earningDetails.NetAmount) / monthlydays) / 480);
                    decimal totalOTAmount = OTMinutes * perMinuteSalary;//Total cost for Ot hours worked.
                                                                        // empDetails.OT = OTMinutes / 60;//Minutes converting to hours
                    lblotamount.InnerText = totalOTAmount.ToString("#.00");

                    if (totalOTAmount > 0)
                    {

                        //empDetails.OThr = "(" + string.Format("{0} : {1}", ts.Hours, ts.Minutes) + "*" + perHourSalary.ToString("#.00") + "hrs)" + "=" + totalOTAmount.ToString("#.00"); }
                        lblothrs.InnerText = string.Format("{0} : {1}", ts.Hours, ts.Minutes) + " hrs";
                    }
                    else
                    {
                        lblothrs.InnerText = "0hrs";
                    }
                    //Loss of pay for absent days
                    var absentDays = ObjDAL.AttendanceLists.Where(x => x.Date.Value.Year == Year &&
                                                                       x.Date.Value.Month == Month &&
                                                                       x.EmpId == empcode.ToString() &&
                                                                       x.Status.ToLower() == "Absent")
                                                                       .ToList().Count;
                    var leaveApplied = ObjDAL.HRMSLeaveApplications.Where(x => x.AppliedDate.Value.Year == Year &&
                                                                         x.AppliedDate.Value.Month == Month &&
                                                                         x.EmpCode == empcode.ToString() &&
                                                                         x.LeaveStatus.ToLower() == "approved")
                                                                         .ToList().Count;

                    lblabsentdays.InnerText = absentDays.ToString();
                    lblnoofworkingdays.InnerText = (monthlydays - absentDays).ToString();
                    //loss of pay
                    lbllossofpay.InnerText = ((absentDays - leaveApplied) * perdaySalary).ToString("#.00");
                    decimal lopamount = Convert.ToDecimal(lbllossofpay.InnerText);
                    lblnetamount.InnerText = (Convert.ToDecimal(earningDetails.NetAmount + totalOTAmount) - lopamount).ToString("#.00");



                    //Earnings 
                    var employeeEarnings = ObjDAL.EarningPercentageAmounts.Where(x => x.EmpId == empcode).Distinct().ToList();
                    decimal totalEarnings = 0;
                    employeeEarnings.ForEach(x => totalEarnings += Convert.ToDecimal(x.Amount));
                    lblearningtotal.InnerText = (totalEarnings + totalOTAmount).ToString("#.00");


                    if (employeeEarnings.Count > 0)
                    {
                        gvdearnings.DataSource = employeeEarnings;
                        gvdearnings.DataBind();
                    }
                    else
                    {
                        gvdearnings.DataSource = employeeEarnings;
                        gvdearnings.DataBind();
                        lblearningtotal.InnerText = totalOTAmount.ToString("#.00");

                    }

                    //Deductions
                    var employeeDeductions = ObjDAL.DeductionPercentageAmounts.Where(x => x.EmpId == empcode).Distinct().ToList();

                    decimal totalDeductions = 0;
                    employeeDeductions.ForEach(x => totalDeductions += Convert.ToDecimal(x.Amount));
                    lbldeductiontotal.InnerText = (lopamount + totalDeductions).ToString(".00");

                    if (employeeDeductions.Count > 0)
                    {
                        gvddeductions.DataSource = employeeDeductions;
                        gvddeductions.DataBind();
                    }
                    else
                    {
                        gvddeductions.DataSource = employeeDeductions;
                        gvddeductions.DataBind();
                        lbldeductiontotal.InnerText = lopamount.ToString(".00");

                    }
                }
                else
                {
                   ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Select valid Montrh');", true);
                }
            }
            else
            {
                //Write notification as all other projects
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Select valid Year');", true);
            }
        }

        protected void btnBacktoDashboard_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Administrator/AdminDasboard.aspx");
        }
    }
}
