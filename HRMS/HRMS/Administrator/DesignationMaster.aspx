﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/Admin.Master" AutoEventWireup="true" CodeBehind="DesignationMaster.aspx.cs" Inherits="HRMS.Employee.DesignationMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <script type="text/javascript">
         $(document).ready(function () {
             $(".txtOnly").keypress(function (e) {
                 var key = e.keyCode;
                 if (key < 48 || (key > 57 && key < 65) || (key > 90 && key < 97) || key > 122 || (key >= 48 && key <= 57)) {
                     e.preventDefault();
                 }
             });
         });

         $(document).ready(function () {
             $(".txtno").keypress(function (e) {
                 var key = e.keyCode;
                 if (keyCode > 31 && (keyCode < 48 || keyCode > 57)) {
                     e.preventDefault();
                 }
             });
         });

    </script>
    <section id="main-content">
        <section class="wrapper">

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="../Administrator/DesignationMaster.aspx" class="current">DesignationMaster</a> </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading tab-bg-dark-navy-blue ">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a data-toggle="tab" href="#DesignationName" aria-expanded="false"><i class="fa fa-user">Designation Name</i></a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#List" aria-expanded="true"><i class="fa fa-bars"> List</i></a>
                                </li>
                            </ul>
                        </header>
                        <div class="panel-body">
                            <div class="tab-content">
                                <div id="DesignationName" class="tab-pane active">

                                    <div class="col-md-12">
                                        <div class="row">

                                            <section class="panel">
                                                <div class="panel-body">

                                                    <div class="form-group col-md-6" runat="server" id="desghide">
                                                        <label for="textfield" class="control-label col-sm-4" style="">
                                                            DesignationMaster ID</label>
                                                        <div class="col-sm-8" style="" id="">
                                                            <asp:TextBox ID="txtdesignationid" runat="server" class="form-control"
                                                                AutoPostBack="true" MaxLength="50">
                                                            </asp:TextBox>

                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-4" style="">
                                                            Designation Name</label>
                                                        <div class="col-sm-8" style="" id="Div1">
                                                            <asp:TextBox ID="txtDesignationname" runat="server" placeholder="Designation Name" required class="form-control txtOnly"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 text-center">
                                                        <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary" OnClick="btnSave_Click" /><%--OnClientClick="return fileUploadValidation()"--%>
                                                    </div>

                                                </div>


                                            </section>
                                        </div>

                                    </div>
                                </div>
                                <div id="List" class="tab-pane">

                                    <div style="max-height: 515px; overflow: auto;" class="table-responsive">
                                        <asp:GridView ID="gdvdesignation"  CssClass="mydatagrid" HeaderStyle-CssClass="headergrid" RowStyle-CssClass="rows"
                                            runat="server" Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%; overflow: scroll;"  EmptyDataText="No records Found" AutoGenerateColumns="false" OnRowDataBound="gdvdesignation_RowDataBound" OnSelectedIndexChanged="gdvdesignation_SelectedIndexChanged">
                                            <Columns>
                                                <asp:BoundField DataField="Id" HeaderText="Designation No" ItemStyle-Width="200" />
                                                <asp:BoundField DataField="RoleName" HeaderText="Designation Name" ItemStyle-Width="200" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>


</asp:Content>
