﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Employee
{
    public partial class DepatmentMaster : System.Web.UI.Page
    {
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();

        #region Declaration

        /// <summary>
        /// to load the methods
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            incrementcode();
            bindgrid();
            depid.Visible = false;

        }

        #endregion

        #region Methods

        /// <summary>
        /// getting the list of the department
        /// </summary>
        public void bindgrid()
        {
            var designation = (from item in ObjDAL.HRMSDepartmentMasters
                               select item).ToList();


            if (designation.Count > 0)
            {
                gvddepartnemtname.DataSource = designation;
                gvddepartnemtname.DataBind();
            }
            else
            {
                gvddepartnemtname.DataSource = designation;
                gvddepartnemtname.DataBind();


            }
        }

        /// <summary>
        /// clear the text of the department name
        /// </summary>
        protected void cleartext()
        {
            txtDepartmentname.Text = string.Empty;
        }

        protected void incrementcode()
        {
            var objHeaderInfo = (from header in ObjDAL.HRMSDepartmentMasters
                                 select header).ToList();
            int subroleid = Convert.ToInt32(objHeaderInfo.Count) + 1;
            txtid.Text = Convert.ToString(subroleid);
        }

              #endregion

        #region Events

        protected void gvddepartnemtname_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(gvddepartnemtname, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = "View Department Details";
                e.Row.Attributes["style"] = "cursor:pointer";
            }
        }

        /// <summary>
        /// on rowclick to show the particular details 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvddepartnemtname_SelectedIndexChanged(object sender, EventArgs e)
        {
            string Empid = gvddepartnemtname.SelectedRow.Cells[0].Text;
            string path = "~/Administrator/DepartmentView.aspx";
            Session["EmployeeDetailsCode"] = Empid;
            Response.Redirect(path);
        }

        /// <summary>
        /// saving the department name
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                HRMSDepartmentMaster role = new HRMSDepartmentMaster();
                role.DepartmentId = Convert.ToInt32(txtid.Text);
                role.DepartmentName = txtDepartmentname.Text;
                role.CreatedBy = "";
                role.CreatedOn = DateTime.Now;
                ObjDAL.HRMSDepartmentMasters.InsertOnSubmit(role);
                ObjDAL.SubmitChanges();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Saved');", true);
                cleartext();
                incrementcode();
                bindgrid();
            }
            catch (Exception)
            {
            }
        }

        #endregion

    }
}