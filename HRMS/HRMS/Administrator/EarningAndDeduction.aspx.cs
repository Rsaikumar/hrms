﻿using BAL;
using DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

namespace HRMS.Employee
{
    public partial class EarningAndDeduction : System.Web.UI.Page
    {
        #region Declaration
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

   

        #endregion

        #region Methods

        /// <summary>
        /// getting the earnings and deduction list
        /// </summary>
        public void bindgridearning()
        {

            var earnings = (from item in objDAL.HRMSEarningAndDeductions
                            select item).ToList();


            if (earnings.Count > 0)
            {
                gdvearings.DataSource = earnings;
                gdvearings.DataBind();
            }
            else
            {
                gdvearings.DataSource = earnings;
                gdvearings.DataBind();
                gdvearings.Rows[0].Cells[0].Text = "No Records Found";


            }
        }

        /// <summary>
        /// clearing the details
        /// </summary>
        protected void cleartext()
        {
            txtername.Text = string.Empty;
            ddleartype.Text = string.Empty;
            txteaevalue.Text = string.Empty;
            ddlearpayslip.Text = string.Empty;

        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindgridearning();
            }


        }

        protected void gdvearings_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(gdvearings, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = "View Salary Details";
                e.Row.Attributes["style"] = "cursor:pointer";
            }
        }

        /// <summary>
        /// on row click it shows the particular details.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gdvearings_SelectedIndexChanged(object sender, EventArgs e)
        {
            string Empid = gdvearings.SelectedRow.Cells[0].Text;
            string path = "~/Administrator/EarningsAndDeductionsView.aspx";
            Session["EmployeeDetailsCode"] = Empid;
            Response.Redirect(path);
        }

        /// <summary>
        /// saving the earnings and deduction details.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btneardecsubmit_Click(object sender, EventArgs e)
        {
            try
            {

                HRMSEarningAndDeduction role = new HRMSEarningAndDeduction();

                role.Name = txtername.Text;
                role.Type = ddleartype.SelectedItem.Text;
                role.Value = Convert.ToDecimal(txteaevalue.Text);
                role.PayslipType = ddlearpayslip.SelectedItem.Text;
                role.CreatedBy = "";
                role.CreatedOn = DateTime.Now;
                objDAL.HRMSEarningAndDeductions.InsertOnSubmit(role);
                objDAL.SubmitChanges();

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Saved');", true);
                bindgridearning();
                cleartext();
            }
            catch (Exception)
            {
            }
        }


        #endregion








    }
}


       


       
    
