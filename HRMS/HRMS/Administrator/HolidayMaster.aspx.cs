﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Administrator
{
    public partial class HolidayMaster : System.Web.UI.Page
    {
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            ddlbind();
        }



        public void ddlbind()
        {
            try
            {

                var master = (from item in ObjDAL.HRMSEmployeeMasterRegistrationDetails
                              join per in ObjDAL.HRMSEmplyeeMasterPersonalDetails on item.ID equals Convert.ToInt32(per.EmpId)
                              select per.FirstName + " " + per.MiddleName + " " + per.LastName).ToList();


                if (master.Count > 0)
                {
                    ddlempname.DataSource = master;
                    ddlempname.DataBind();
                    ddlempname.Items.Insert(0, new ListItem("--Select--", "0"));
                }
            }
            catch (Exception)
            {

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int empcode = Convert.ToInt32(Session["Emp_Code"].ToString());
            var name = ddlempname.SelectedItem.Text;

            try
            {

                HolidayList role = new HolidayList();

                //role.Name= ddlempname.SelectedItem.Text;
                //role.ShortName = txtShortName.Text;
                //role.NoOfLeaves = Convert.ToInt32(txtnoofleaves.Text);
                //role.CreatedBy = "";
                //role.CreatedOn = DateTime.Now;
                //ObjDAL.HolidayLists.InsertOnSubmit(role);
                //ObjDAL.SubmitChanges();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Save Sucessfully');window.location ='../Administrator/AdminDasboard.aspx';", true);

            }
            catch
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Try Again');", true);

            }
        }

    }
}