﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Employee
{
    public partial class DesignationMaster : System.Web.UI.Page
    {
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();

        #region Declaration

        /// <summary>
        /// to load the methods
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            incrementcode();
            bindgdvdesignation();
            desghide.Visible = false;
        }

        #endregion

        #region  Methods
       
        /// <summary>
        ///it shows the list of the designation
        /// </summary>
        public void bindgdvdesignation()
        {
            var designation = (from item in ObjDAL.HRMSRoleMasters
                               select item).ToList();


            if (designation.Count > 0)
            {
                gdvdesignation.DataSource = designation;
                gdvdesignation.DataBind();
            }
            else
            {
                gdvdesignation.DataSource = designation;
                gdvdesignation.DataBind();


            }
        }

        /// <summary>
        /// clear the designation name
        /// </summary>
        protected void cleartext()
        {
            txtDesignationname.Text = string.Empty;
        }


        protected void incrementcode()
        {
            var objHeaderInfo = (from header in ObjDAL.HRMSRoleMasters
                                 select header).ToList();
            int subroleid = Convert.ToInt32(objHeaderInfo.Count) + 1;
            txtdesignationid.Text = Convert.ToString(subroleid);
        }


        #endregion

        #region Events

     
        protected void gdvdesignation_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(gdvdesignation, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = "View Designation Details";
                e.Row.Attributes["style"] = "cursor:pointer";
            }
        }

        /// <summary>
        /// By clicking the row it shows the particular details of that designation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gdvdesignation_SelectedIndexChanged(object sender, EventArgs e)
        {
            string Empid = gdvdesignation.SelectedRow.Cells[0].Text;
            string path = "~/Administrator/DesignationView.aspx";
            Session["EmployeeDetailsCode"] = Empid;
            Response.Redirect(path);
        }

        /// <summary>
        /// saving the particular designation name
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                HRMSRoleMaster role = new HRMSRoleMaster();
                role.RoleId = Convert.ToInt32(txtdesignationid.Text);
                role.RoleName = txtDesignationname.Text;
                role.CreatedBy = "";
                role.CreatedOn = DateTime.Now;
                ObjDAL.HRMSRoleMasters.InsertOnSubmit(role);
                ObjDAL.SubmitChanges();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Saved');", true);
                cleartext();
                incrementcode();
                bindgdvdesignation();
            }
            catch (Exception)
            {
            }
        }

        #endregion

    }
    }