﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BAL;
using DAL;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using System.Net.Mail;
using System.Web.Security;

namespace HRMS.Administrator
{
    public partial class EmployeeFullDetails : System.Web.UI.Page
    {


        #region declarations

        string objConfig = ConfigurationManager.ConnectionStrings["Dbconnection"].ToString();
        CityMasterBAL objCityBAL = new CityMasterBAL();
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();
        HRMS_City_Master objCityTable = new HRMS_City_Master();
        DepartmentMasterBAL objDeptBAL = new DepartmentMasterBAL();
        EmployeeGroupMasterBAL objEmpGroupBAL = new EmployeeGroupMasterBAL();
        RecruitMasterBAL objRecruitBAL = new RecruitMasterBAL();
        Employee_Location_BAL ObjEmpLocation = new Employee_Location_BAL();
        JobTypeBAL objJobTypeBAL = new JobTypeBAL();
        EmployeeBAL objEmpBAL = new EmployeeBAL();
        EmployeeOfficialInfoBAL objeoinfo = new EmployeeOfficialInfoBAL();
        EmployeeBAL Total1EmpDetailsBAL = new EmployeeBAL();
        BankMasterBAL objBankBAL = new BankMasterBAL();

        #endregion

        #region methods


        public string password;
        internal string Sendmailpwd( string EmailId)
        {
           password = Membership.GeneratePassword(12, 1);
            MailMessage mail = new MailMessage();
            mail.To.Add(EmailId);
            mail.From = new MailAddress("treebolaproject@gmail.com");
            mail.Subject = " Registration Succefully.";

            mail = new MailMessage("ttreebolaproject@gmail.com",
                 EmailId, "Registration Succefully",
                 "<center>Registration Succefully</center><br />" +
                 "<br />" +
                 "<br />" +
                 "Your login Password is:<br />" + password +
                 "<br />" +
                  "<br />" + "<br />" +
                 "This Password is confidential. For security reasons, DO NOT share the Password with anyone.<br />" +
                 "<br />" +
                 "<br />" +

                 "We are takes your account security very seriously. We  will never email you and ask you to disclose or verify your Trebola password, credit card, or banking account number. If you receive a suspicious email with a link to update your account information<br />" +
                   "<br />" +
                     "<br />" +
                 "We hope to see you again soon.<br />" +

                 "");


            mail.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
            // smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address
            smtp.UseDefaultCredentials = false;
            smtp.EnableSsl = true;
            smtp.Credentials = new System.Net.NetworkCredential("treebolaproject@gmail.com", "thinklogic");
            // smtp.Port = 587;
            //Or your Smtp Email ID and Password
            smtp.Send(mail);
            var response = "Sucess";
            return response;
        }



        public void CitiesBind()
        {
            String Creation_Company = Session["ClientRegistrationNo"].ToString();
            List<HRMS_City_Master> objGroup = objCityBAL.GetData_CityMaster(Creation_Company);

            if (objGroup.Count > 0)
            {

            }
            else
            {

            }
        }

        public void AuthorityBinding()
        {
            try
            {
                int empcode = Convert.ToInt32(Session["Emp_Code"]);
                List<HRMSEmployeeMasterOfficialInformationDetail> objEmpofficialinfo = (from master in ObjDAL.HRMSEmployeeMasterOfficialInformationDetails
                                                                                        where master.EmpId == Convert.ToString(Session["Emp_Code"])
                                                                                        select master).ToList();

                var reporting = (from item in ObjDAL.USP_gettingemployeenameandid(empcode)
                                 select item).ToList();

                ddlReportingAuthority.DataSource = reporting;
                ddlReportingAuthority.DataTextField = "empname";
                ddlReportingAuthority.DataValueField = "empid";
                ddlReportingAuthority.DataBind();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void GetPersonalData()
        {
            string ClientRegistrationNo = Session["ClientRegistrationNo"].ToString();
            var query = (from person in ObjDAL.HRMS_Employee_Master_PersonalDetails
                         where person.Creation_Company == ClientRegistrationNo && person.Emp_Code == txtEmp_Code.Text
                         select person).ToList();
            txtFirstName.Text = query[0].Emp_First_Name;
            txtMiddleName.Text = query[0].Emp_Middle_Name;
            txtLastName.Text = query[0].Emp_Last_Name;
            ddluserole.SelectedValue = query[0].Emp_Roll;
            userImg.ImageUrl = query[0].Emp_Image;
            //ddlEmpStatus.SelectedValue = query[0].Emp_Status;
        }


        public void incrementcode()
        {
            var incrementempID = (from item in ObjDAL.HRMSEmployeeMasterRegistrationDetails
                                  select item).ToList();
            int EmpID = incrementempID.Count() + 1;
            txtEmp_Code.Text = Convert.ToString(EmpID);
            txtEmp_Code.ReadOnly = true;
        }


        String StrDate = "";
        public string GetDate(object Dt)
        {
            StrDate = string.Format("{0:dd-MM-yyyy}", Dt);
            return StrDate;
        }

        //public void PercentageBind()
        //{
        //    var perbind = (from item in ObjDAL.HRMSSalaryDeductionPercentages
        //                   select item).ToList();
        //    if (perbind.Count > 0)
        //    {
        //        txtPFper.Text = perbind[0].PF.ToString();
        //        txtdaper.Text = perbind[0].DA.ToString();
        //        txthraper.Text = perbind[0].HRA.ToString();
        //        txtccaper.Text = perbind[0].CCA.ToString();
        //        txtESIper.Text = perbind[0].ESI.ToString();
        //    }
        //}

        private void SavingPersonal()
        {
            try
            {
                if (txtDateOfBirth.Text != "")
                {
                    string d1 = txtDateOfBirth.Text.Substring(0, 2);
                    string m1 = txtDateOfBirth.Text.Substring(3, 2);
                    string y1 = txtDateOfBirth.Text.Substring(6, 4);
                    DateTime DOB = DateTime.ParseExact(txtDateOfBirth.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);
                    objEmpBAL.Emp_DOB = DOB;
                }
                else
                {
                    objEmpBAL.Emp_DOB = DateTime.Parse("01-01-2001");
                }


                objEmpBAL.Emp_Code = txtEmp_Code.Text;
                if (fupUserImage.HasFile)
                {
                    string imgname = fupUserImage.FileName;
                    fupUserImage.PostedFile.SaveAs(Server.MapPath("~/EmployeesImages/" + imgname));
                    objEmpBAL.Emp_Image = "~/EmployeesImages/" + imgname;
                }
                else
                {
                    objEmpBAL.Emp_Image = "";
                }
                objEmpBAL.Emp_First_Name = txtFirstName.Text;
                objEmpBAL.Emp_Middle_Name = txtMiddleName.Text;
                objEmpBAL.Emp_Last_Name = txtLastName.Text;
                objEmpBAL.Emp_Roll = ddlUSersubRole.SelectedValue;
                //objEmpBAL.User_Name = txtUsername.Text;
                //objEmpBAL.Password = txtPassword.Text;
                //objEmpBAL.Emp_Status = ddlEmpStatus.SelectedValue;

                //objEmpBAL.Emp_Age = Convert.ToDecimal(txtAge.Text);

                objEmpBAL.Gender = rblGender.SelectedValue;
                objEmpBAL.Blood_Group = txtBloodGroup.Text;


                objEmpBAL.Marital_Status = ddlMaritalStatus.SelectedValue;
                objEmpBAL.Nationality = ddlNationality.SelectedValue;
                //objEmpBAL.Religion = ddlReligion.SelectedValue;
                //objEmpBAL.Languages_Known = txtLanguagesKnown.Text;

                //if (chbPermentAddress.Checked)
                //{
                //    objEmpBAL.PresentAsPrev = "true";
                //}
                //else { objEmpBAL.PresentAsPrev = "false"; }

                //objEmpBAL.Driving_Licenece_Number = txtDriving_License_Number.Text;
                //objEmpBAL.Licence_Type = ddldrivinglicenseType.SelectedValue;

                //if (FupLicense.HasFile == true)
                //{
                //    string filename = FupLicense.PostedFile.FileName;
                //    var FileExtension = Path.GetExtension(FupLicense.PostedFile.FileName).ToString();
                //    if (FileExtension.ToString().ToLower() == ".jpg" || FileExtension.ToString().ToLower() == ".pdf")
                //    {
                //        string path = Server.MapPath("~/Attachments/");
                //        FupLicense.SaveAs(path + filename);
                //        string LicenceAttachurl = "~/Attachments/" + filename;
                //        objEmpBAL.Licence_Attachment = LicenceAttachurl.ToString();
                //    }
                //    else { ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('plz upload image files or pdf files only..');", true); }

                //}
                //else
                //{
                //    objEmpBAL.Licence_Attachment = "";
                //}

                //objEmpBAL.Aadhar_Number = txtAadharNumber.Text;
                //if (FupAadhar.HasFile == true)
                //{
                //    string filename1 = FupAadhar.PostedFile.FileName;
                //    var FileExtension1 = Path.GetExtension(FupAadhar.PostedFile.FileName).ToString();
                //    if (FileExtension1.ToString().ToLower() == ".jpg" || FileExtension1.ToString().ToLower() == ".pdf")
                //    {
                //        string path1 = Server.MapPath("~/Attachments/");
                //        FupAadhar.SaveAs(path1 + filename1);
                //        string aadharAttachurl = "~/Attachments/" + filename1;
                //        objEmpBAL.Aadhar_Attachment = aadharAttachurl.ToString();
                //    }
                //    else
                //    {
                //        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('plz upload image files or pdf files only..');", true);
                //    }
                //}
                //else
                //{
                //    objEmpBAL.Aadhar_Attachment = "";
                //}
                //objEmpBAL.Pan_Number = txtpanno.Text;

                //if (FupPanNo.HasFile == true)
                //{
                //    string filename2 = FupPanNo.PostedFile.FileName;
                //    var FileExtension2 = Path.GetExtension(FupPanNo.PostedFile.FileName).ToString();
                //    if (FileExtension2.ToString().ToLower() == ".jpg" || FileExtension2.ToString().ToLower() == ".pdf")
                //    {
                //        string path2 = Server.MapPath("~/Attachments/");
                //        FupPanNo.SaveAs(path2 + filename2);
                //        string PanAttachurl = "~/Attachments/" + filename2;
                //        objEmpBAL.Pan_Attachment = PanAttachurl.ToString();
                //    }
                //    else
                //    {
                //        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('plz upload image files or pdf files only..');", true);
                //    }
                //}
                //else
                //{ objEmpBAL.Pan_Attachment = ""; }
                //objEmpBAL.Passport_Number = txtPassportNumber.Text;


                //if (FupPassport.HasFile == true)
                //{
                //    string filename3 = FupPassport.PostedFile.FileName;
                //    var FileExtension3 = Path.GetExtension(FupPassport.PostedFile.FileName).ToString();
                //    if (FileExtension3.ToString().ToLower() == ".jpg" || FileExtension3.ToString().ToLower() == ".pdf")
                //    {
                //        string path3 = Server.MapPath("~/Attachments/");
                //        FupPassport.SaveAs(path3 + filename3);
                //        string PassportAttachurl = "~/Attachments/" + filename3;
                //        objEmpBAL.Passport_Attachment = PassportAttachurl.ToString();
                //    }
                //    else
                //    {
                //        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('plz upload image files or pdf files only..');", true);
                //    }

                //}
                //else
                //{ objEmpBAL.Passport_Attachment = ""; }
                //objEmpBAL.Bank_Name = ddlPBankName.SelectedValue;
                //objEmpBAL.Account_Number = txtPAccountNumber.Text;
                //objEmpBAL.Health_Insurance_Poly_No = txtPHealth.Text;
                //objEmpBAL.Sum_Insured = txtSumInsured.Text;

                //objEmpBAL.Present_Address1 = txtAddress1.Text;
                //objEmpBAL.Present_Address2 = txtAddress2.Text;
                //objEmpBAL.Present_City = txtcity.Text;
                //objEmpBAL.Present_State = txtState.Text;
                //objEmpBAL.Present_Pincode = txtPostalCode.Text;
                //objEmpBAL.Present_Country = txtCountry.Text;
                //objEmpBAL.Permanent_Address1 = txtPAddress1.Text;
                //objEmpBAL.Permanent_Address2 = txtPaddress2.Text;
                //objEmpBAL.Permanent_City = txtpcity.Text;
                //objEmpBAL.Permanent_Country = txtpCountry.Text;
                //objEmpBAL.Permanent_Pincode = txtPPalost.Text;
                //objEmpBAL.Permanent_State = txtPSate.Text;
                //objEmpBAL.Permanent_Mobile = txtMobile.Text;
                //objEmpBAL.Permanent_Email = txtEmail.Text;

                //objEmpBAL.Created_By = Session["User_Name"].ToString();
                //objEmpBAL.Created_Date = System.DateTime.Now;
                //objEmpBAL.Creation_Company = Session["ClientRegistrationNo"].ToString();
                //objEmpBAL.Modified_By = Session["User_Name"].ToString();
                //objEmpBAL.Modified_Date = System.DateTime.Now;
                var topEmpCode = (from person in ObjDAL.HRMS_Employee_Master_PersonalDetails
                                  where person.Emp_Code == txtEmp_Code.Text &&
                                  person.Creation_Company == Session["ClientRegistrationNo"].ToString()
                                  select person).ToList();

                if (topEmpCode.Count > 0)
                {
                    if (txtEmp_Code.Text == topEmpCode[0].Emp_Code)
                    {
                        objEmpBAL.Parameter = 2;
                        objEmpBAL.Emp_Code = txtEmp_Code.Text;
                    }
                    else
                    {
                        objEmpBAL.Emp_Code = txtEmp_Code.Text;
                    }
                }
                else
                {

                    objEmpBAL.Emp_Code = txtEmp_Code.Text;
                }
                if (objEmpBAL.Insert_Employee_Details() != 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);
                    GetPersonalData();
                }
            }
            catch (Exception ex)
            {
                string abc = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + abc + "');", true);
            }
        }

        //public void UpdateEmpPersonal()
        //{
        //    try
        //    {
        //        if (txtDateOfBirth.Text != "")
        //        {
        //            string d1 = txtDateOfBirth.Text.Substring(0, 2);
        //            string m1 = txtDateOfBirth.Text.Substring(3, 2);
        //            string y1 = txtDateOfBirth.Text.Substring(6, 4);
        //            DateTime DOB = DateTime.ParseExact(txtDateOfBirth.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);
        //            //objEmpBAL.Emp_DOB = (txtDateOfBirth.Text == "") ? DateTime.Parse("01-01-2001") : DOB;
        //            objEmpBAL.Emp_DOB = DOB;
        //        }
        //        else
        //        {
        //            objEmpBAL.Emp_DOB = DateTime.Parse("01-01-2001");
        //        }

        //        if (txtLicenseExpiryDate.Text != "")
        //        {
        //            string d3 = txtLicenseExpiryDate.Text.Substring(0, 2);
        //            string m3 = txtLicenseExpiryDate.Text.Substring(3, 2);
        //            string y3 = txtLicenseExpiryDate.Text.Substring(6, 4);
        //            DateTime LicenceExpiryDate = DateTime.ParseExact(txtLicenseExpiryDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);
        //            objEmpBAL.Licence_Exp_Date = LicenceExpiryDate;
        //        }
        //        else
        //        {
        //            objEmpBAL.Licence_Exp_Date = DateTime.Parse("01-01-2001");
        //        }
        //        if (txtIssuedDate.Text != "")
        //        {
        //            string d4 = txtIssuedDate.Text.Substring(0, 2);
        //            string m4 = txtIssuedDate.Text.Substring(3, 2);
        //            string y4 = txtIssuedDate.Text.Substring(6, 4);
        //            DateTime IssuedDate = DateTime.ParseExact(txtIssuedDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);
        //            objEmpBAL.Issued_Date = IssuedDate;
        //        }
        //        else
        //        {
        //            objEmpBAL.Issued_Date = DateTime.Parse("01-01-2001");
        //        }
        //        if (txtExpiryDate.Text != "")
        //        {
        //            string d5 = txtExpiryDate.Text.Substring(0, 2);
        //            string m5 = txtExpiryDate.Text.Substring(3, 2);
        //            string y5 = txtExpiryDate.Text.Substring(6, 4);
        //            DateTime expiryDate = DateTime.ParseExact(txtExpiryDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);
        //            objEmpBAL.Exp_Date = expiryDate;
        //        }
        //        else
        //        {
        //            objEmpBAL.Exp_Date = DateTime.Parse("01-01-2001");
        //        }
        //        if (txtRenewalDate.Text != "")
        //        {
        //            string d6 = txtRenewalDate.Text.Substring(0, 2);
        //            string m6 = txtRenewalDate.Text.Substring(3, 2);
        //            string y6 = txtRenewalDate.Text.Substring(6, 4);
        //            DateTime renewalDate = DateTime.ParseExact(txtRenewalDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);
        //            objEmpBAL.Renewal_Date = renewalDate;
        //        }
        //        else
        //        {
        //            objEmpBAL.Renewal_Date = DateTime.Parse("01-01-2001");
        //        }

        //        objEmpBAL.Emp_Code = txtEmp_Code.Text;
        //        if (fupUserImage.HasFile)
        //        {
        //            string imagename = fupUserImage.PostedFile.FileName;
        //            fupUserImage.PostedFile.SaveAs(Server.MapPath("~/EmployeesImages/" + imagename));
        //            objEmpBAL.Emp_Image = "~/EmployeesImages/" + imagename;
        //        }
        //        else
        //        {
        //            if (Session["DbImagePath"] != null)
        //            { objEmpBAL.Emp_Image = (string)Session["DbImagePath"]; }
        //            else { objEmpBAL.Emp_Image = ""; }
        //        }

        //        objEmpBAL.Emp_First_Name = txtFirstName.Text;
        //        objEmpBAL.Emp_Middle_Name = txtMiddleName.Text;
        //        objEmpBAL.Emp_Last_Name = txtLastName.Text;
        //        objEmpBAL.Emp_Roll = ddlUSerRole.SelectedValue;
        //        objEmpBAL.User_Name = txtUsername.Text;
        //        objEmpBAL.Password = txtPassword.Text;
        //        objEmpBAL.Emp_Status = ddlEmpStatus.SelectedValue;
        //        objEmpBAL.Emp_Age = Convert.ToDecimal(txtAge.Text);
        //        objEmpBAL.Gender = rblGender.SelectedValue;
        //        objEmpBAL.Blood_Group = txtBloodGroup.Text;
        //        objEmpBAL.Marital_Status = ddlMaritalStatus.SelectedValue;
        //        objEmpBAL.Nationality = ddlNationality.SelectedValue;
        //        objEmpBAL.Religion = ddlReligion.SelectedValue;
        //        objEmpBAL.Languages_Known = txtLanguagesKnown.Text;
        //        objEmpBAL.Driving_Licenece_Number = txtDriving_License_Number.Text;
        //        objEmpBAL.Licence_Type = ddldrivinglicenseType.SelectedValue;

        //        //Personal Identification Attached Files
        //        if (FupLicense.HasFile == true)
        //        {
        //            string filename = FupLicense.PostedFile.FileName;
        //            var FileExtension = Path.GetExtension(FupLicense.PostedFile.FileName).ToString();
        //            if (FileExtension.ToString().ToLower() == ".jpg" || FileExtension.ToString().ToLower() == ".pdf")
        //            {
        //                string path = Server.MapPath("~/Attachments/");
        //                FupLicense.SaveAs(path + filename);
        //                string LicenceAttachurl = "~/Attachments/" + filename;
        //                objEmpBAL.Licence_Attachment = LicenceAttachurl.ToString();
        //                // text.InnerText = filename.ToString();
        //            }
        //            else { ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('plz upload image files or pdf files only..');", true); }

        //        }
        //        else
        //        {
        //            objEmpBAL.Licence_Attachment = "";
        //        }

        //        objEmpBAL.Aadhar_Number = txtAadharNumber.Text;
        //        if (FupAadhar.HasFile == true)
        //        {
        //            string filename1 = FupAadhar.PostedFile.FileName;
        //            var FileExtension1 = Path.GetExtension(FupAadhar.PostedFile.FileName).ToString();
        //            if (FileExtension1.ToString().ToLower() == ".jpg" || FileExtension1.ToString().ToLower() == ".pdf")
        //            {
        //                string path1 = Server.MapPath("~/Attachments/");
        //                FupAadhar.SaveAs(path1 + filename1);
        //                string aadharAttachurl = "~/Attachments/" + filename1;
        //                objEmpBAL.Aadhar_Attachment = aadharAttachurl.ToString();
        //            }
        //            else
        //            {
        //                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('plz upload image files or pdf files only..');", true);
        //            }
        //        }
        //        else
        //        {
        //            objEmpBAL.Aadhar_Attachment = "";
        //        }
        //        objEmpBAL.Pan_Number = txtpanno.Text;

        //        if (FupPanNo.HasFile == true)
        //        {
        //            string filename2 = FupPanNo.PostedFile.FileName;
        //            var FileExtension2 = Path.GetExtension(FupPanNo.PostedFile.FileName).ToString();
        //            if (FileExtension2.ToString().ToLower() == ".jpg" || FileExtension2.ToString().ToLower() == ".pdf")
        //            {
        //                string path2 = Server.MapPath("~/Attachments/");
        //                FupPanNo.SaveAs(path2 + filename2);
        //                string PanAttachurl = "~/Attachments/" + filename2;
        //                objEmpBAL.Pan_Attachment = PanAttachurl.ToString();
        //            }
        //            else
        //            {
        //                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('plz upload image files or pdf files only..');", true);
        //            }
        //        }
        //        else
        //        { objEmpBAL.Pan_Attachment = ""; }
        //        objEmpBAL.Passport_Number = txtPassportNumber.Text;


        //        if (FupPassport.HasFile == true)
        //        {
        //            string filename3 = FupPassport.PostedFile.FileName;
        //            var FileExtension3 = Path.GetExtension(FupPassport.PostedFile.FileName).ToString();
        //            if (FileExtension3.ToString().ToLower() == ".jpg" || FileExtension3.ToString().ToLower() == ".pdf")
        //            {
        //                string path3 = Server.MapPath("~/Attachments/");
        //                FupPassport.SaveAs(path3 + filename3);
        //                string PassportAttachurl = "~/Attachments/" + filename3;
        //                objEmpBAL.Passport_Attachment = PassportAttachurl.ToString();
        //            }
        //            else
        //            {
        //                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('plz upload image files or pdf files only..');", true);
        //            }

        //        }
        //        else
        //        { objEmpBAL.Passport_Attachment = ""; }
        //        objEmpBAL.Bank_Name = ddlPBankName.SelectedValue;
        //        objEmpBAL.Account_Number = txtPAccountNumber.Text;
        //        objEmpBAL.Health_Insurance_Poly_No = txtPHealth.Text;
        //        objEmpBAL.Sum_Insured = txtSumInsured.Text;

        //        objEmpBAL.Present_Address1 = txtAddress1.Text;
        //        objEmpBAL.Present_Address2 = txtAddress2.Text;
        //        objEmpBAL.Present_City = txtcity.Text;
        //        objEmpBAL.Present_State = txtState.Text;
        //        objEmpBAL.Present_Pincode = txtPostalCode.Text;
        //        objEmpBAL.Present_Country = txtCountry.Text;
        //        objEmpBAL.Permanent_Address1 = txtPAddress1.Text;
        //        objEmpBAL.Permanent_Address2 = txtPaddress2.Text;
        //        if (chbPermentAddress.Checked)
        //        {
        //            objEmpBAL.PresentAsPrev = "true";
        //        }
        //        else { objEmpBAL.PresentAsPrev = "false"; }
        //        objEmpBAL.Permanent_City = txtpcity.Text;
        //        objEmpBAL.Permanent_Country = txtpCountry.Text;
        //        objEmpBAL.Permanent_Pincode = txtPPalost.Text;
        //        objEmpBAL.Permanent_State = txtPSate.Text;
        //        objEmpBAL.Permanent_Mobile = txtMobile.Text;
        //        objEmpBAL.Permanent_Email = txtEmail.Text;
        //        objEmpBAL.Created_By = Session["User_Name"].ToString();
        //        objEmpBAL.Created_Date = System.DateTime.Now;
        //        objEmpBAL.Creation_Company = Session["ClientRegistrationNo"].ToString();
        //        objEmpBAL.Modified_By = Session["User_Name"].ToString();
        //        objEmpBAL.Modified_Date = System.DateTime.Now;
        //        objEmpBAL.Parameter = 2;

        //        if (objEmpBAL.Insert_Employee_Details() != 0)
        //        {
        //            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Employee personal Data Saved Successfully');", true);
        //            //ClearAllFields();
        //        }
        //        else
        //        {
        //            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Employee personal Data Data Not Saved');", true);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string msg = ex.Message.ToString();
        //    }
        //}

        public void headdatasave()
        {
            SqlTransaction transaction = null;
            SqlConnection objSqlConnection = null;
            string empcode = (string)Session["EmployeeFamilyDetailsCode"];
            string creationCompany = (string)Session["ClientRegistrationNo"];
            objSqlConnection = new SqlConnection(objConfig);
            objSqlConnection.Open();
        }

        public void OfficialDataSave()
        {
            SqlTransaction transaction = null;
            SqlConnection objSqlConnection = null;
            bool debitResult = false;
            bool creditResult = false;
            bool debitResult1 = false;
            try
            {
                objSqlConnection = new SqlConnection(objConfig);
                objSqlConnection.Open();
                transaction = objSqlConnection.BeginTransaction();
                // // // Assign The Data To Domain Objects
                Total1EmpDetailsBAL.OfficialEmpCode = txtEmp_Code.Text;
                if (txtDateofJoin.Text != "")
                {
                    string dj = txtDateofJoin.Text.Substring(0, 2);
                    string mj = txtDateofJoin.Text.Substring(3, 2);
                    string yj = txtDateofJoin.Text.Substring(6, 4);
                    DateTime DateofJoin = DateTime.ParseExact(txtDateofJoin.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);
                    Total1EmpDetailsBAL.Date_Of_Join = DateofJoin;
                }
                else
                {
                    Total1EmpDetailsBAL.Date_Of_Join = DateTime.Parse("01-01-2001");
                }
                Total1EmpDetailsBAL.Reporting_Authority = (ddlReportingAuthority.SelectedValue == "") ? "" : ddlReportingAuthority.SelectedValue;
                //Total1EmpDetailsBAL.PF_Code = (txtpfamount.Text == "") ? "" : txtpfamount.Text;
                //Total1EmpDetailsBAL.ESI_Code = (txtesiamount.Text == "") ? "" : txtesiamount.Text;
                Total1EmpDetailsBAL.Creation_Company = Session["ClientRegistrationNo"].ToString();
                Total1EmpDetailsBAL.Created_By = Session["User_Name"].ToString();
                Total1EmpDetailsBAL.Created_Date = System.DateTime.Now;
                Total1EmpDetailsBAL.Modified_By = Session["User_Name"].ToString();
                Total1EmpDetailsBAL.Modified_Date = System.DateTime.Now;
                Total1EmpDetailsBAL.OfficialParameter = OfficialParameter; // // // This Can Be Used For Operation Related 1=Insert                   
                //transaction = objSqlConnection.BeginTransaction();
                using (SqlCommand objSqlCommand = objSqlConnection.CreateCommand())
                {
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.CommandText = "Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_Official_Information";
                    objSqlCommand.Parameters.AddWithValue("@Emp_Code", Total1EmpDetailsBAL.OfficialEmpCode);
                    objSqlCommand.Parameters.AddWithValue("@Date_Of_Join", Total1EmpDetailsBAL.Date_Of_Join);
                    objSqlCommand.Parameters.AddWithValue("@Designation", Total1EmpDetailsBAL.Desgnation);
                    objSqlCommand.Parameters.AddWithValue("@Employee_Grade", Total1EmpDetailsBAL.Employee_Grade);
                    objSqlCommand.Parameters.AddWithValue("@Department", Total1EmpDetailsBAL.Department);

                    objSqlCommand.Parameters.AddWithValue("@Location", Total1EmpDetailsBAL.Location); // // Newly Added Field

                    objSqlCommand.Parameters.AddWithValue("@Reporting_Authority", Total1EmpDetailsBAL.Reporting_Authority);
                    objSqlCommand.Parameters.AddWithValue("@Working_Shifts", Total1EmpDetailsBAL.Working_Shifts);
                    objSqlCommand.Parameters.AddWithValue("@PF", Total1EmpDetailsBAL.PF);
                    objSqlCommand.Parameters.AddWithValue("@PF_Code", Total1EmpDetailsBAL.PF_Code);
                    objSqlCommand.Parameters.AddWithValue("@ESI", Total1EmpDetailsBAL.ESI);//10
                    objSqlCommand.Parameters.AddWithValue("@ESI_Code", Total1EmpDetailsBAL.ESI_Code);
                    objSqlCommand.Parameters.AddWithValue("@ESI_Dispensary", Total1EmpDetailsBAL.ESI_Dispensary);
                    objSqlCommand.Parameters.AddWithValue("@Profisional_Tax", Total1EmpDetailsBAL.Profisional_Tax);
                    objSqlCommand.Parameters.AddWithValue("@TDS", Total1EmpDetailsBAL.TDS);
                    objSqlCommand.Parameters.AddWithValue("@Quarter_Accommodation", Total1EmpDetailsBAL.Quarter_Accommodation);
                    objSqlCommand.Parameters.AddWithValue("@Four_Wheeler", Total1EmpDetailsBAL.Four_Wheeler);
                    objSqlCommand.Parameters.AddWithValue("@Two_Wheeler", Total1EmpDetailsBAL.Two_Wheeler);
                    objSqlCommand.Parameters.AddWithValue("@Laptop", Total1EmpDetailsBAL.Laptop);
                    objSqlCommand.Parameters.AddWithValue("@Mobile_Phone", Total1EmpDetailsBAL.Mobile_Phone);
                    objSqlCommand.Parameters.AddWithValue("@Other_Facilities", Total1EmpDetailsBAL.Other_Facilities);

                    objSqlCommand.Parameters.AddWithValue("@Creation_Company", Total1EmpDetailsBAL.Creation_Company);
                    objSqlCommand.Parameters.AddWithValue("@Created_By", Total1EmpDetailsBAL.Created_By);
                    objSqlCommand.Parameters.AddWithValue("@Created_Date", Total1EmpDetailsBAL.Created_Date);
                    objSqlCommand.Parameters.AddWithValue("@Modified_By", Total1EmpDetailsBAL.Modified_By);
                    objSqlCommand.Parameters.AddWithValue("@Modified_Date", Total1EmpDetailsBAL.Modified_Date);//10
                    objSqlCommand.Parameters.AddWithValue("@Parameter", Total1EmpDetailsBAL.OfficialParameter);
                    objSqlCommand.Parameters.AddWithValue("@Work_Type", Total1EmpDetailsBAL.WorkType);
                    objSqlCommand.Parameters.AddWithValue("@Recruit", Total1EmpDetailsBAL.Recruit);

                    objSqlCommand.Transaction = transaction;
                    if (objSqlCommand.ExecuteNonQuery() != 0)
                    {
                        debitResult = true;
                    }
                }

                SqlCommand objsqlcommand1;
                using (objsqlcommand1 = objSqlConnection.CreateCommand())
                {
                    objeoinfo.Emp_Code = txtEmp_Code.Text;
                    objeoinfo.Heads = "";
                    objeoinfo.Amount = 0;
                    objeoinfo.Basic_Amount = (txtbasicamount.Text == "") ? Convert.ToDecimal("0") : Convert.ToDecimal(txtbasicamount.Text);
                    objeoinfo.Gross_Amount = (txtgrossamount.Text == "") ? Convert.ToDecimal("0") : Convert.ToDecimal(txtgrossamount.Text);
                    //objeoinfo.DA = (txtda.Text == "") ? Convert.ToDecimal("0") : Convert.ToDecimal(txtda.Text);
                    //objeoinfo.HRA = (txthra.Text == "") ? Convert.ToDecimal("0") : Convert.ToDecimal(txthra.Text);
                    //objeoinfo.CCA = (txtcca.Text == "") ? Convert.ToDecimal("0") : Convert.ToDecimal(txtcca.Text);
                    objeoinfo.OA = Convert.ToDecimal("0");
                    //objeoinfo.PF = (txtpfamount.Text == "") ? Convert.ToDecimal("0") : Convert.ToDecimal(txtpfamount.Text);
                    //objeoinfo.ESI = (txtesiamount.Text == "") ? Convert.ToDecimal("0") : Convert.ToDecimal(txtesiamount.Text);
                    objeoinfo.TDS = Convert.ToDecimal("0");
                    objeoinfo.APGLI = Convert.ToDecimal("0");
                    objeoinfo.GIS = Convert.ToDecimal("0");
                    objeoinfo.HBA = Convert.ToDecimal("0");
                    objeoinfo.MCA = Convert.ToDecimal("0");
                    objeoinfo.BA = Convert.ToDecimal("0");
                    objeoinfo.LOAN = Convert.ToDecimal("0");
                    objeoinfo.OTHERS = Convert.ToDecimal("0");
                    objeoinfo.Criteria = "";
                    objeoinfo.Salary_Range = "";
                    objeoinfo.Parameter = Convert.ToInt32(1);
                    objeoinfo.Creation_Company = (string)Session["ClientRegistrationNo"];
                    objeoinfo.Created_By = (string)Session["User_Name"];
                    objeoinfo.Created_Date = Convert.ToDateTime(DateTime.Now.ToString());
                    objeoinfo.Modified_By = (string)Session["User_Name"];
                    objeoinfo.Modified_Date = Convert.ToDateTime(DateTime.Now.ToString());

                    objsqlcommand1.CommandType = CommandType.StoredProcedure;
                    objsqlcommand1.CommandText = "Sp_HRMS_Insert_Select_Update_HRMS_Employee_Official_Info_Head_Details";
                    objsqlcommand1.Parameters.AddWithValue("@Emp_Code", objeoinfo.Emp_Code);
                    objsqlcommand1.Parameters.AddWithValue("@Heads", objeoinfo.Heads);
                    objsqlcommand1.Parameters.AddWithValue("@Amount", objeoinfo.Amount);
                    objsqlcommand1.Parameters.AddWithValue("@Basic_Amount", objeoinfo.Basic_Amount);
                    objsqlcommand1.Parameters.AddWithValue("@Gross_Amount", objeoinfo.Gross_Amount);
                    objsqlcommand1.Parameters.AddWithValue("@DA", objeoinfo.DA);
                    objsqlcommand1.Parameters.AddWithValue("@HRA", objeoinfo.HRA);
                    objsqlcommand1.Parameters.AddWithValue("@CCA", objeoinfo.CCA);
                    objsqlcommand1.Parameters.AddWithValue("@OA", objeoinfo.OA);
                    objsqlcommand1.Parameters.AddWithValue("@PF", objeoinfo.PF);
                    objsqlcommand1.Parameters.AddWithValue("@EESI", objeoinfo.EESI);
                    objsqlcommand1.Parameters.AddWithValue("@ESI", objeoinfo.ESI);
                    objsqlcommand1.Parameters.AddWithValue("@PTAX", objeoinfo.PTAX);
                    objsqlcommand1.Parameters.AddWithValue("@TDS", objeoinfo.TDS);
                    objsqlcommand1.Parameters.AddWithValue("@APGLI", objeoinfo.APGLI);
                    objsqlcommand1.Parameters.AddWithValue("@GIS", objeoinfo.GIS);
                    objsqlcommand1.Parameters.AddWithValue("@HBA", objeoinfo.HBA);
                    objsqlcommand1.Parameters.AddWithValue("@MCA", objeoinfo.MCA);
                    objsqlcommand1.Parameters.AddWithValue("@BA", objeoinfo.BA);
                    objsqlcommand1.Parameters.AddWithValue("@LOAN", objeoinfo.LOAN);
                    objsqlcommand1.Parameters.AddWithValue("@OTHERS", objeoinfo.OTHERS);
                    objsqlcommand1.Parameters.AddWithValue("@NET", objeoinfo.NET);
                    objsqlcommand1.Parameters.AddWithValue("@Criteria", objeoinfo.Criteria);
                    objsqlcommand1.Parameters.AddWithValue("@Salary_Range", objeoinfo.Salary_Range);
                    objsqlcommand1.Parameters.AddWithValue("@Creation_Company", objeoinfo.Creation_Company);
                    objsqlcommand1.Parameters.AddWithValue("@Created_By", objeoinfo.Created_By);
                    objsqlcommand1.Parameters.AddWithValue("@Created_Date", objeoinfo.Created_Date);
                    objsqlcommand1.Parameters.AddWithValue("@Modified_By", objeoinfo.Modified_By);
                    objsqlcommand1.Parameters.AddWithValue("@Modified_Date", objeoinfo.Modified_Date);
                    objsqlcommand1.Parameters.AddWithValue("@Parameter", objeoinfo.Parameter);
                    objsqlcommand1.Transaction = transaction;
                    if (objsqlcommand1.ExecuteNonQuery() != 0)
                    {
                        creditResult = true;
                    }
                }

                if (debitResult || creditResult || debitResult1)
                {
                    transaction.Commit();
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Saved Successfully');", true);
                }
                else
                {
                    transaction.Rollback();
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
                }
            }
            catch (Exception ex)
            {
                Session["Error"] = ex.ToString();
            }

            finally
            {
            }
        }

        public void BindRole()
        {
            try
            {
                var objHeaderInfo = (from header in ObjDAL.HRMSDepartmentMasters
                                     where header.DepartmentId >= Convert.ToInt32(Session["role"].ToString())
                                     select header).ToList();
                if (objHeaderInfo.Count > 0)
                {
                    ddlUSersubRole.DataSource = objHeaderInfo;
                    ddlUSersubRole.DataTextField = "DepartmentName";
                    ddlUSersubRole.DataValueField = "DepartmentId";
                    ddlUSersubRole.DataBind();
                    ddlUSersubRole.Items.Insert(0, new ListItem("---Select---", "0"));
                }
                else
                {
                    ddlUSersubRole.Items.Insert(0, new ListItem("---Select---", "0"));
                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }

        }

        public void BindSubRole()
        {
            try
            {
                var objHeaderInfo = (from header in ObjDAL.HRMSRoleMasters
                                     select header).ToList();
                if (objHeaderInfo.Count > 0)
                {
                    ddluserole.DataSource = objHeaderInfo;
                    ddluserole.DataTextField = "RoleName";
                    ddluserole.DataValueField = "RoleId";
                    ddluserole.DataBind();
                    ddluserole.Items.Insert(0, new ListItem("---Select---", "0"));
                }
                else
                {
                    ddluserole.Items.Insert(0, new ListItem("---Select---", "0"));
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }
        }

        #endregion

        #region events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                incrementcode();
                BindRole();
                BindSubRole();
                iddiv.Visible = false;
                AuthorityBinding();
                binddeductionsgrid();
                bindearningsgrid();
                //loctionblock();
                lblcasualid.Visible = false;
                lblearnedid.Visible = false;
                txtcasualleave.Visible = false;
                txtearnedleave.Visible = false;

            }

            if (Session["User_Type"].ToString() == "CCF")
            {
                string Creation_Company = Session["ClientRegistrationNo"].ToString();
                string empcode = Session["Emp_Code"].ToString();
            }
            else
            {

                if (!IsPostBack)
                {
                    if (Session["ClientRegistrationNo"] == null)
                    {
                        Response.Redirect("~/Default.aspx");
                        CitiesBind();
                        if ((string)Session["EmployeeDetailsCode"] != null)
                        {
                            string empcode = (string)Session["EmployeeDetailsCode"];
                            Session["EmployeeFamilyDetailsCode"] = empcode;
                            Session["UpdateEmployeeDetailsCode"] = empcode;
                            string ClientRegistrationNo = Session["ClientRegistrationNo"].ToString();
                        }
                        else
                        {

                        }
                    }
                }
            }
        }


        protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            string city = txtcity.Text;
            string creationCompany = Session["ClientRegistrationNo"].ToString();
            if (city != "Select")
            {
                List<HRMS_City_Master> objrow = (from state in ObjDAL.HRMS_City_Masters
                                                 where state.Creation_Company == creationCompany && state.City_Name == city
                                                 select state).ToList();
                if (objrow.Count > 0)
                {
                    txtState.Text = objrow[0].State_Name;
                    txtCountry.Text = objrow[0].Country;
                    txtPostalCode.Focus();
                }
                else
                {
                    txtState.Text = "";
                    txtCountry.Text = "";
                }
            }
            else
            {
                txtState.Text = "";
                txtCountry.Text = "";
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Plz Select City..');", true);
            }
        }

        protected void ddlPCity_SelectedIndexChanged(object sender, EventArgs e)
        {
           // string city = txtpcity.Text;
            string creationCompany = Session["ClientRegistrationNo"].ToString();
            //if (city != "Select")
            //{
            //    List<HRMS_City_Master> objrow = (from state in ObjDAL.HRMS_City_Masters
            //                                     where state.Creation_Company == creationCompany && state.City_Name == city
            //                                     select state).ToList();
            //    if (objrow.Count > 0)
            //    {
            //        //txtPSate.Text = objrow[0].State_Name;
            //       // txtpCountry.Text = objrow[0].Country;
            //    }
            //    else
            //    {
            //       // txtPSate.Text = "";
            //       // txtpCountry.Text = "";
            //    }
            //}
            //else
            //{
            //   // txtPSate.Text = "";
            //   // txtpCountry.Text = "";
            //    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Plz Select City..');", true);
            //}
        }

        protected void btnSavePersonal_Click(object sender, EventArgs e)
        {
            //  SavingPersonal();
        }


        protected void btnUpdatePersonal_Click(object sender, EventArgs e)
        {
            // UpdateEmpPersonal();

        }

        public void btnSaveOfficial_Click(object sender, EventArgs e)
        {
            OfficialParameter = 1;
            OfficialDataSave();
            headdatasave();
        }

        public void btnUpdateOfficial_Click(object sender, EventArgs e)
        {
            string empcode = (string)Session["EmployeeFamilyDetailsCode"];
            string creationCompany = (string)Session["ClientRegistrationNo"];
            List<HRMS_Employee_Master_Official_Information> officaldata = (from offc in ObjDAL.HRMS_Employee_Master_Official_Informations
                                                                           where offc.Emp_Code == empcode && offc.Creation_Company == creationCompany
                                                                           select offc).ToList();
            ObjDAL.HRMS_Employee_Master_Official_Informations.DeleteAllOnSubmit(officaldata);
            ObjDAL.SubmitChanges();
            List<HRMS_Employee_Master_Employee_Deduction> Deductdata = (from Deduct in ObjDAL.HRMS_Employee_Master_Employee_Deductions
                                                                        where Deduct.Emp_Code == empcode && Deduct.Creation_Company == creationCompany
                                                                        select Deduct).ToList();
            ObjDAL.HRMS_Employee_Master_Employee_Deductions.DeleteAllOnSubmit(Deductdata);
            ObjDAL.SubmitChanges();
            List<HRMS_Employee_Official_Info_Head_Detail> Headdata = (from Head in ObjDAL.HRMS_Employee_Official_Info_Head_Details
                                                                      where Head.Emp_Code == empcode && Head.Creation_Company == creationCompany
                                                                      select Head).ToList();
            ObjDAL.HRMS_Employee_Official_Info_Head_Details.DeleteAllOnSubmit(Headdata);
            ObjDAL.SubmitChanges();
            OfficialParameter = 1;

            OfficialDataSave();
        }

        // // // // Save The Data To Employee Official Data
        int OfficialParameter;

        protected void grvLoan_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataTable dt = (DataTable)ViewState["LoanCurrentTable"];
                LinkButton lb = (LinkButton)e.Row.FindControl("lbRemoveRowLoane");
                if (lb != null)
                {
                    if (dt.Rows.Count > 1)
                    {
                        if (e.Row.RowIndex == dt.Rows.Count - 1)
                        {
                            lb.Visible = false;
                        }
                    }
                    else
                    {
                        lb.Visible = false;
                    }
                }
            }
        }

        protected void grvLeave_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chbEligible = (CheckBox)e.Row.FindControl("chbEligible");
                TextBox txtEligible = (TextBox)e.Row.FindControl("txtEligible");

                if (txtEligible.Text.ToLower() == "true")
                { chbEligible.Checked = true; }
                else
                { chbEligible.Checked = false; }
            }
        }

        protected void txtEmp_Code_TextChanged(object sender, EventArgs e)
        {
            Session["EmployeeFamilyDetailsCode"] = txtEmp_Code.Text;
            txtFirstName.Focus();
        }

        protected void ddlEmpStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (ddlEmpStatus.SelectedValue == "Resigned" || ddlEmpStatus.SelectedValue == "Retired")
                //{
                //    //lblResignedDate.Text = ddlEmpStatus.SelectedValue + " Date";
                //    //ResignedDate.Visible = true;
                //}
                //else
                //{
                //    // ResignedDate.Visible = false;
                //}
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }



        int id;
        //saving
        protected void btnsave_Click1(object sender, EventArgs e)
        {
            try
            {
                HRMSEmployeeMasterRegistrationDetail erd = new HRMSEmployeeMasterRegistrationDetail();

                erd.ID = Convert.ToInt32(txtEmp_Code.Text);
                erd.UserRole = Convert.ToInt32(ddluserole.SelectedValue);
                erd.RoleType = ddluserole.SelectedItem.Text;
                erd.UserName = txtEmail.Text;
                erd.UserSubRole = Convert.ToInt32(ddlUSersubRole.SelectedValue);
                erd.CompanyID = Convert.ToInt32(Session["ClientRegistrationNo"]);
                erd.CreatedOn = DateTime.Now;
                erd.CreatedBy = Convert.ToString(Session["Emp_Code"]);
                ObjDAL.HRMSEmployeeMasterRegistrationDetails.InsertOnSubmit(erd);
                ObjDAL.SubmitChanges();

                id = erd.ID;

                HRMSEmplyeeMasterPersonalDetail epd = new HRMSEmplyeeMasterPersonalDetail();

                epd.EmpId = Convert.ToString(id);
                epd.FirstName = txtFirstName.Text;
                epd.MiddleName = txtMiddleName.Text;
                epd.LastName = txtLastName.Text;
                epd.Gender = rblGender.SelectedValue;
                epd.DateOfBirth = Convert.ToDateTime(txtDateOfBirth.Text);
                epd.MaritalStatus = ddlMaritalStatus.SelectedValue;
                epd.BloodGroup = txtBloodGroup.Text;
                epd.Nationality = ddlNationality.SelectedValue;
                epd.Photo = "";
                epd.CompanyID = Convert.ToInt32(Session["ClientRegistrationNo"]);
                epd.CreatedOn = DateTime.Now;
                epd.CreatedBy = Convert.ToString(Session["Emp_Code"]);
                ObjDAL.HRMSEmplyeeMasterPersonalDetails.InsertOnSubmit(epd);

                HRMSEmplyeeMasterPersonalIdentificationDetail pid = new HRMSEmplyeeMasterPersonalIdentificationDetail();

                pid.EmpId = Convert.ToString(id);
                pid.DrivingLicenseFile = "";
                pid.AadharFile = "";
                pid.PanFile = "";
                pid.PassportFile = "";
                pid.CompanyID = Convert.ToInt32(Session["ClientRegistrationNo"]);
                pid.CreatedOn = DateTime.Now;
                pid.CreatedBy = Convert.ToString(Session["Emp_Code"]);
                ObjDAL.HRMSEmplyeeMasterPersonalIdentificationDetails.InsertOnSubmit(pid);

                HRMSEmployeeMasterAddressDetail ead = new HRMSEmployeeMasterAddressDetail();

                ead.EmpId = Convert.ToString(id);
                ead.PermanentAddress1 = txtAddress1.Text;
                ead.PermanentAddress2 = txtAddress2.Text;
                ead.PermanentCity = txtcity.Text;
                ead.PermanentState = txtState.Text;
                ead.PermanentCountry = txtCountry.Text;
                ead.PresentPostalCode = Convert.ToInt32(txtPostalCode.Text);
                ead.MobileNo = txtMobile.Text;
                ead.CompanyID = Convert.ToInt32(Session["ClientRegistrationNo"]);
                ead.CreatedOn = DateTime.Now;
                ead.CreatedBy = Convert.ToString(Session["Emp_Code"]);
                ObjDAL.HRMSEmployeeMasterAddressDetails.InsertOnSubmit(ead);

                HRMSEmployeeMasterSocialNetworkingDetail snd = new HRMSEmployeeMasterSocialNetworkingDetail();

                snd.EmpId = Convert.ToString(id);
                snd.EmailId = txtEmail.Text;
                snd.CompanyID = Convert.ToInt32(Session["ClientRegistrationNo"]);
                snd.CreatedOn = DateTime.Now;
                snd.CreatedBy = Convert.ToString(Session["Emp_Code"]);
                ObjDAL.HRMSEmployeeMasterSocialNetworkingDetails.InsertOnSubmit(snd);

                HRMSEmployeeMasterWorkLocationDetail wld = new HRMSEmployeeMasterWorkLocationDetail();

                wld.EmpId = Convert.ToString(id);
                wld.CompanyID = Convert.ToInt32(Session["ClientRegistrationNo"]);
                wld.CreatedOn = DateTime.Now;
                wld.CreatedBy = Convert.ToString(Session["Emp_Code"]);
                ObjDAL.HRMSEmployeeMasterWorkLocationDetails.InsertOnSubmit(wld);


                try
                {

                    List<HRMSNewLeaveMaster> leaveid = (from item in ObjDAL.HRMSNewLeaveMasters
                                                        select item).ToList();

                    if (leaveid.Count > 0)
                    {
                        foreach (var a in leaveid)
                        {

                            if (a.Id == 2)
                            {
                                HRMSEmployeeLeave leav = new HRMSEmployeeLeave();

                                leav.EmpId = Convert.ToInt32(id);
                                leav.LeaveId = a.Id;
                                leav.NoOfLeaves = Convert.ToInt32(txtearnedleave.Text);
                                leav.CreatedOn = DateTime.Now;
                                leav.CreatedBy = Convert.ToString(Session["Emp_Code"]);
                                ObjDAL.HRMSEmployeeLeaves.InsertOnSubmit(leav);
                                ObjDAL.SubmitChanges();
                            }


                            else
                            {
                                HRMSEmployeeLeave leav = new HRMSEmployeeLeave();

                                leav.EmpId = Convert.ToInt32(id);
                                leav.LeaveId = a.Id;
                                leav.NoOfLeaves = Convert.ToInt32(txtcasualleave.Text);
                                leav.CreatedOn = DateTime.Now;
                                leav.CreatedBy = Convert.ToString(Session["Emp_Code"]);
                                ObjDAL.HRMSEmployeeLeaves.InsertOnSubmit(leav);
                                ObjDAL.SubmitChanges();
                            }
                        }
                    }
                }
                catch
                {
                }


                if (FileUpload1.HasFile)
                {
                    string imgname = FileUpload1.FileName;
                    FileUpload1.PostedFile.SaveAs(Server.MapPath("~/EmployeesImages/" + imgname));
                }
                else
                {
                }

                HRMSEmployeeMasterOfficialInformationDetail oid = new HRMSEmployeeMasterOfficialInformationDetail();

                oid.EmpId = Convert.ToString(id);
                oid.DateOfJoining = DateTime.ParseExact(txtDateofJoin.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                oid.ReportingAuthority = ddlReportingAuthority.SelectedValue;
                oid.CompanyID = Convert.ToInt32(Session["ClientRegistrationNo"]);
                oid.CreatedOn = DateTime.Now;
                oid.CreatedBy = Convert.ToString(Session["Emp_Code"]);
                ObjDAL.HRMSEmployeeMasterOfficialInformationDetails.InsertOnSubmit(oid);

                HRMSEmployeeMasterSalaryDetail sd = new HRMSEmployeeMasterSalaryDetail();

                sd.EmpId = Convert.ToString(id);
                sd.BasicAmount = Convert.ToDecimal(txtbasicamount.Text);
                sd.GrossAmount = Convert.ToDecimal(txtgrossamount.Text);
                sd.CompanyID = Convert.ToInt32(Session["ClientRegistrationNo"]);
                sd.CreatedOn = DateTime.Now;
                sd.CreatedBy = Convert.ToString(Session["Emp_Code"]);
                ObjDAL.HRMSEmployeeMasterSalaryDetails.InsertOnSubmit(sd);

                try
                {

                    foreach (GridViewRow row in gvdearnings.Rows)
                    {
                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            CheckBox CheckRow = (row.Cells[0].FindControl("chbboxdesignation") as CheckBox);
                            if (CheckRow.Checked)
                            {
                                TextBox valuepercentage = (row.Cells[2].FindControl("txtvalue") as TextBox);
                                string salvalue = valuepercentage.Text;
                                Decimal PercentageValue = Convert.ToDecimal(salvalue);

                                string name = gvdearnings.Rows[row.RowIndex].Cells[1].Text;

                                
                                TextBox Amount1 = (row.Cells[3].FindControl("txtearningamount") as TextBox);

                                string salamount = Amount1.Text;
                                Decimal amount = Convert.ToDecimal(salamount);


                                EarningPercentageAmount objpc = new EarningPercentageAmount();

                                objpc.EmpId = Convert.ToInt32(id);
                                objpc.Value = Convert.ToDecimal(PercentageValue);
                                objpc.Amount = Convert.ToDecimal(amount);
                                objpc.Name = Convert.ToString(name);
                                objpc.ModifiedOn = DateTime.Now;
                                objpc.ModifiedBy = objpc.ModifiedBy;

                                objpc.CreatedOn = DateTime.Now;
                                objpc.CreatedBy = Convert.ToString(Session["Emp_Code"]);

                                ObjDAL.EarningPercentageAmounts.InsertOnSubmit(objpc);
                                ObjDAL.SubmitChanges();
                            }

                        }
                    }
                  
                }
                catch
                {
                }

                HRMSEmployeeMasterDeductionsDetail dd = new HRMSEmployeeMasterDeductionsDetail();

                dd.EmpId = Convert.ToString(id);
                dd.NetAmount = Convert.ToDecimal(txtnetamount.Text);
                dd.CompanyID = Convert.ToInt32(Session["ClientRegistrationNo"]);
                dd.CreatedOn = DateTime.Now;
                dd.CreatedBy = Convert.ToString(Session["Emp_Code"]);
                ObjDAL.HRMSEmployeeMasterDeductionsDetails.InsertOnSubmit(dd);


                try
                {

                    foreach (GridViewRow row in gvddeductions.Rows)
                    {
                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            CheckBox CheckRow = (row.Cells[0].FindControl("chboxdeductions") as CheckBox);
                            if (CheckRow.Checked)
                            {

                                TextBox valuepercentage = (row.Cells[2].FindControl("txtdedvalue") as TextBox);
                                string salvalue = valuepercentage.Text;
                                Decimal PercentageValue = Convert.ToDecimal(salvalue);

                                string name = gvddeductions.Rows[row.RowIndex].Cells[1].Text;

                                string design = gvddeductions.Rows[row.RowIndex].Cells[1].Text;
                                TextBox Amount1 = (row.Cells[3].FindControl("txtdeductions") as TextBox);

                                string deductionamount = Amount1.Text;
                                Decimal amount = Convert.ToDecimal(deductionamount);


                                DeductionPercentageAmount objpc = new DeductionPercentageAmount();

                                objpc.EmpId = Convert.ToInt32(id);
                                objpc.Value = Convert.ToDecimal(PercentageValue);
                                objpc.Name = Convert.ToString(name);
                                objpc.Amount = Convert.ToDecimal(amount);
                                objpc.CreatedOn = DateTime.Now;
                                objpc.CreatedBy = Convert.ToString(Session["Emp_Code"]);

                                ObjDAL.DeductionPercentageAmounts.InsertOnSubmit(objpc);
                                ObjDAL.SubmitChanges();

                            }

                        }
                    }
                }
                catch
                {
                }
                HRMSEmployeeMasterFacilitiesDetail fd = new HRMSEmployeeMasterFacilitiesDetail();

                fd.EmpId = Convert.ToString(id);
                fd.CompanyID = Convert.ToInt32(Session["ClientRegistrationNo"]);
                fd.CreatedOn = DateTime.Now;
                fd.CreatedBy = Convert.ToString(Session["Emp_Code"]);
                ObjDAL.HRMSEmployeeMasterFacilitiesDetails.InsertOnSubmit(fd);
                ObjDAL.SubmitChanges();
                Sendmailpwd(txtEmail.Text);
                Updatepassword();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Registered Successfully');window.location ='../Administrator/AdminDashboard.aspx';", true);

            }
            catch (Exception ex)
            {
                var error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Insert All Details');", true);
            }
        }

        private void Updatepassword()
        {
            try
            {

                int Id = Convert.ToInt32(id);
                var erd = (from s in ObjDAL.HRMSEmployeeMasterRegistrationDetails.Where(x => x.ID == Id) select s).SingleOrDefault();

                {
                    erd.Password = password;
                   
                }
                ObjDAL.SubmitChanges();
            }
            catch (Exception ex)
            {
                
                
            }
        }

        //dateOfJoin
        protected void txtDateofJoin_TextChanged(object sender, EventArgs e)
        {
            String seeDate = DateTime.Now.ToString("dd-MM-yyyy");

            DateTime sodate = DateTime.ParseExact(seeDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

            DateTime joindate = Convert.ToDateTime(txtDateofJoin.Text);
            if (joindate < sodate)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Select Valid Date..');", true);
                txtDateofJoin.Text = "";
                txtDateofJoin.Focus();

            }

            try
            {
                String seDate = DateTime.Now.ToString("dd-MM-yyyy");
                DateTime jointodate = Convert.ToDateTime(txtDateofJoin.Text);

                String dy = jointodate.Day.ToString();
                String mn = jointodate.Month.ToString();
                String yy = jointodate.Year.ToString();

                DateTime datevalue = (Convert.ToDateTime(seDate.ToString()));

                String dy1 = datevalue.Day.ToString();
                String mn1 = datevalue.Month.ToString();
                String yy1 = datevalue.Year.ToString();

                if (15 >= Convert.ToInt32(dy))
                {
                    int month = Convert.ToInt32(mn);
                    txtcasualleave.Text = Convert.ToString(11 - month);
                    txtearnedleave.Text = Convert.ToString((11 - month) / 3);

                }

                else
                {
                    int month = Convert.ToInt32(mn);
                    txtcasualleave.Text = Convert.ToString(12 - month);

                    txtearnedleave.Text = Convert.ToString((12 - month) / 3);


                }
            }

            catch (Exception ex)
            {

            }
        }

        protected void ddlusersubrole_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtcasualleave.Visible = false;
            txtearnedleave.Visible = false;
            lblcasualid.Visible = false;
            lblearnedid.Visible = false;



            if ((ddluserole.SelectedItem.Value == "7") || (ddluserole.SelectedItem.Value == "8") || (ddluserole.SelectedItem.Value == "9") || (ddluserole.SelectedItem.Value == "10"))
            {


                txtcasualleave.Visible = false;

                txtearnedleave.Visible = false;
                lblcasualid.Visible = false;
                lblearnedid.Visible = false;
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Leaves After One Year Applicable..');", true);
            }

            else
            {
                txtcasualleave.Visible = true;

                txtearnedleave.Visible = true;
                lblcasualid.Visible = true;
                lblearnedid.Visible = true;

                if (txtDateofJoin.Text != "")
                {
                    DateTime joindate = Convert.ToDateTime(txtDateofJoin.Text);

                    String seeDate = DateTime.Now.ToString("dd-MM-yyyy");

                    DateTime sodate = DateTime.ParseExact(seeDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                    if (joindate < sodate)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Select Valid Date..');", true);
                        txtDateofJoin.Text = "";
                        txtDateofJoin.Focus();

                    }

                    try
                    {
                        String seDate = DateTime.Now.ToString("dd-MM-yyyy");
                        DateTime jointodate = Convert.ToDateTime(txtDateofJoin.Text);

                        String dy = jointodate.Day.ToString();
                        String mn = jointodate.Month.ToString();
                        String yy = jointodate.Year.ToString();

                        DateTime datevalue = DateTime.ParseExact(seDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                        String dy1 = datevalue.Day.ToString();
                        String mn1 = datevalue.Month.ToString();
                        String yy1 = datevalue.Year.ToString();

                            if (15 >= Convert.ToInt32(dy))
                            {
                                int month = Convert.ToInt32(mn);
                                txtcasualleave.Text = Convert.ToString(11 - month);
                                txtearnedleave.Text = Convert.ToString((11 - month) / 3);
                                lblcasualid.Text = "Casual Leave";
                                lblearnedid.Text = "Earned Leave";

                            }

                            else
                            {
                                int month = Convert.ToInt32(mn);
                                txtcasualleave.Text = Convert.ToString(12 - month);
                                txtearnedleave.Text = Convert.ToString((12 - month) / 3);
                                lblcasualid.Text = "Casual Leave";
                                lblearnedid.Text = "Earned Leave";


                            }
                        
                    }
                    catch (Exception ex)
                    {

                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Select the joining  Date..');", true);
                    txtDateofJoin.Focus();
                    ddluserole.SelectedIndex = 0;
                    txtcasualleave.Visible = false;
                    txtearnedleave.Visible = false;
                }

            }
        }

        protected void txttdsper_TextChanged(object sender, EventArgs e)
        {
            txtgrossamount.ReadOnly = false;
            txtgrossamount.Focus();
        }

        public void bindearningsgrid()
        {
            var earnings = (from item in ObjDAL.HRMSEarningAndDeductions
                            where item.PayslipType == "Earnings"
                            select item).ToList();


            if (earnings.Count > 0)
            {
                gvdearnings.DataSource = earnings;
                gvdearnings.DataBind();
            }
            else
            {
                gvdearnings.DataSource = earnings;
                gvdearnings.DataBind();


            }
        }


        public void binddeductionsgrid()
        {
            var deductions = (from item in ObjDAL.HRMSEarningAndDeductions
                              where item.PayslipType == "Deductions"
                              select item).ToList();


            if (deductions.Count > 0)
            {
                gvddeductions.DataSource = deductions;
                gvddeductions.DataBind();
            }
            else
            {
                gvddeductions.DataSource = deductions;
                gvddeductions.DataBind();


            }
        }

        protected void chbboxdesignation_CheckedChanged(object sender, System.EventArgs e)
        {
            decimal amount = Convert.ToDecimal(txtgrossamount.Text);
            decimal amount1 = Convert.ToDecimal(txtgrossamount.Text);

            foreach (GridViewRow row in gvdearnings.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox CheckRow = (row.Cells[0].FindControl("chbboxdesignation") as CheckBox);
                    if (CheckRow.Checked)
                    {
                        string design = gvdearnings.Rows[row.RowIndex].Cells[1].Text;


                        TextBox t = (TextBox)row.FindControl("txtearningamount");
                        TextBox tb = (TextBox)row.FindControl("txtvalue");

                        decimal value = Convert.ToDecimal(tb.Text);
                        t.Text = Convert.ToString(amount1 * value);

                        amount = amount - Convert.ToDecimal(t.Text);
                        txtbasicamount.Text = Convert.ToString(amount);



                    }
                    else
                    {

                        string design = gvdearnings.Rows[row.RowIndex].Cells[0].Text;
                        TextBox t = (TextBox)row.FindControl("txtearningamount");
                        t.Text = string.Empty;

                        txtbasicamount.Text = amount + t.Text;



                    }

                }
            }
        }

        protected void chboxdeductions_CheckedChanged(object sender, System.EventArgs e)
        {
            decimal amount = Convert.ToDecimal(txtgrossamount.Text);
            decimal amount1 = Convert.ToDecimal(txtgrossamount.Text);

            foreach (GridViewRow row in gvddeductions.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox CheckRow = (row.Cells[0].FindControl("chboxdeductions") as CheckBox);
                    if (CheckRow.Checked)
                    {
                        string design = gvddeductions.Rows[row.RowIndex].Cells[1].Text;


                        TextBox t = (TextBox)row.FindControl("txtdeductions");
                        TextBox tb = (TextBox)row.FindControl("txtdedvalue");

                        decimal value = Convert.ToDecimal(tb.Text);
                        t.Text = Convert.ToString(amount1 * value);

                        amount = amount - Convert.ToDecimal(t.Text);
                        txtnetamount.Text = Convert.ToString(amount);



                    }
                    else
                    {

                        string design = gvddeductions.Rows[row.RowIndex].Cells[0].Text;
                        TextBox t = (TextBox)row.FindControl("txtdeductions");
                        t.Text = string.Empty;
                        txtnetamount.Text = amount + t.Text;

                    }

                }
            }
        }

        protected void txtgrossamount_TextChanged(object sender, System.EventArgs e)
        {
            decimal amount = Convert.ToDecimal(txtgrossamount.Text);
            txtbasicamount.Text = Convert.ToString(amount);
            txtnetamount.Text = Convert.ToString(amount);
        }



        #endregion

        protected void txtvalue_TextChanged(object sender, System.EventArgs e)
        {
            decimal amount = Convert.ToDecimal(txtgrossamount.Text);
            decimal amount1 = Convert.ToDecimal(txtgrossamount.Text);

            foreach (GridViewRow row in gvdearnings.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox CheckRow = (row.Cells[0].FindControl("chbboxdesignation") as CheckBox);
                    if (CheckRow.Checked)
                    {
                        string design = gvdearnings.Rows[row.RowIndex].Cells[1].Text;


                        TextBox t = (TextBox)row.FindControl("txtearningamount");
                        TextBox tb = (TextBox)row.FindControl("txtvalue");

                        decimal value = Convert.ToDecimal(tb.Text);
                        t.Text = Convert.ToString(amount1 * value);

                        amount = amount - Convert.ToDecimal(t.Text);



                    }
                    else
                    {

                        string design = gvdearnings.Rows[row.RowIndex].Cells[0].Text;
                        TextBox t = (TextBox)row.FindControl("txtearningamount");

                    }
                    }



                }
            }

        protected void txtdedvalue_TextChanged(object sender, System.EventArgs e)
        {
            decimal amount = Convert.ToDecimal(txtgrossamount.Text);
            decimal amount1 = Convert.ToDecimal(txtgrossamount.Text);

            foreach (GridViewRow row in gvddeductions.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox CheckRow = (row.Cells[0].FindControl("chboxdeductions") as CheckBox);
                    if (CheckRow.Checked)
                    {
                        string design = gvddeductions.Rows[row.RowIndex].Cells[1].Text;


                        TextBox t = (TextBox)row.FindControl("txtdeductions");
                        TextBox tb = (TextBox)row.FindControl("txtdedvalue");

                        decimal value = Convert.ToDecimal(tb.Text);
                        t.Text = Convert.ToString(amount1 * value);

                        amount = amount - Convert.ToDecimal(t.Text);



                    }
                    else
                    {

                        string design = gvddeductions.Rows[row.RowIndex].Cells[0].Text;
                        TextBox t = (TextBox)row.FindControl("txtdeductions");

                    }

                }
            }
        }
        }
    }

