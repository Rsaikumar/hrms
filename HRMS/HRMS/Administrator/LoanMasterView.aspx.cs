﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Administrator
{
    public partial class LoanMasterView : System.Web.UI.Page
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if ((string)Session["LoanId"] != null)
                {

                    string empcode1 = (string)Session["LoanId"];

                    Edit_Loan_Details(empcode1);
                    gdvdesignationlist.Visible = true;

                }
                else
                {

                }
            }
        }


        //public void bindgriddesignation()
        //{

        //    var designation = (from item in objDAL.HRMSRoleMasters
        //                    select item).ToList();


        //    if (designation.Count > 0)
        //    {
        //        gdvdesignationlist.DataSource = designation;
        //        gdvdesignationlist.DataBind();
        //    }
        //    else
        //    {
        //        gdvdesignationlist.DataSource = designation;
        //        gdvdesignationlist.DataBind();
        //        gdvdesignationlist.Rows[0].Cells[0].Text = "No Records Found";


        //    }
        //}


        #region Events

        public void Edit_Loan_Details(string empcode1)
        {
            try
            {
                int ID = Convert.ToInt32(empcode1);
                List<HRMSNewLoanMaster> objEmp = (from master in objDAL.HRMSNewLoanMasters
                                                  where master.Id == Convert.ToInt32(ID)
                                                  select master).ToList();

                if (objEmp.Count > 0)
                {
                    txtLoanName.Text = Convert.ToString(objEmp[0].LoanName);
                    txtShortName.Text = Convert.ToString(objEmp[0].ShortName);
                    txtelgibityamunt.Text = Convert.ToString(objEmp[0].EligibilityAmount);
                    ddltenure.SelectedItem.Text = Convert.ToString(objEmp[0].Tenure);

                    var designatin = (from item in objDAL.HRMSDesignationLoanMasters
                                      where item.LoanTypeId == Convert.ToInt32(ID)
                                      select item).ToList();

                    if (designatin.Count > 0)
                    {
                        gdvdesignationlist.DataSource = designatin;
                        gdvdesignationlist.DataBind();
                    }
                    else
                    {
                        gdvdesignationlist.DataSource = designatin;
                        gdvdesignationlist.DataBind();
                    }

                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int LoanId = Convert.ToInt32(Session["LoanId"].ToString());

                var role = (from s in objDAL.HRMSNewLoanMasters.Where(x => x.Id == Convert.ToInt32(LoanId)) select s).SingleOrDefault();
                {
                    role.LoanName = txtLoanName.Text;
                    role.ShortName = txtShortName.Text;
                    role.Tenure = Convert.ToInt32(ddltenure.SelectedItem.Text);
                    role.EligibilityAmount = Convert.ToDecimal(txtelgibityamunt.Text);
                    role.ModifiedBy = Convert.ToString(Session["Emp_Code"]);
                    role.ModifiedOn = DateTime.Now;
                    int id = role.Id;
                    var objpc = (from s in objDAL.HRMSDesignationLoanMasters.Where(x => x.LoanTypeId == Convert.ToInt32(id)) select s).SingleOrDefault();
                    {
                        foreach (GridViewRow row in gdvdesignationlist.Rows)
                        {
                            if (row.RowType == DataControlRowType.DataRow)
                            {
                                CheckBox CheckRow = (row.Cells[0].FindControl("chbboxdesignation") as CheckBox);
                                if (CheckRow.Checked)
                                {
                                    string design = gdvdesignationlist.Rows[row.RowIndex].Cells[1].Text;
                                    TextBox Amount1 = (row.Cells[2].FindControl("txtelgibityamountlist") as TextBox);

                                    string elgibility = Amount1.Text;
                                    Decimal date = Convert.ToDecimal(elgibility);

                                    objpc.LoanTypeId = Convert.ToInt32(id);
                                    objpc.DesignationName = design;
                                    objpc.ElgibilityAmount = Convert.ToDecimal(date);
                                    objpc.ModifiedOn = DateTime.Now;
                                    objpc.ModifiedBy = objpc.ModifiedBy;
                                    objpc.CreatedOn = DateTime.Now;
                                    objpc.CreatedBy = objpc.CreatedBy;
                                    objDAL.SubmitChanges();


                                }

                            }
                        }
                    }



                }
                objDAL.SubmitChanges();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Updated Sucessfully');window.location ='../Administrator/LoanMaster.aspx';", true);

            }

            catch (Exception)
            {
            }
        }

        protected void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                string loancode = (string)Session["LoanId"];
                int empid = Convert.ToInt32(loancode);

                var address = (from s in objDAL.HRMSNewLoanMasters.Where(x => x.Id == empid) select s).SingleOrDefault();
                objDAL.HRMSNewLoanMasters.DeleteOnSubmit(address);
                objDAL.SubmitChanges();



                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Removed Sucessfully');window.location ='../Administrator/LoanMaster.aspx';", true);

            }


            catch
            {
            }
        }

        protected void txtelgibityamunt_TextChanged(object sender, EventArgs e)
        {
            decimal amount = Convert.ToDecimal(txtelgibityamunt.Text);

            if (amount == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Enter the Amount More Than 10000');", true);

            }
        }

        #endregion

        protected void btncancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Administrator/LoanMaster.aspx");

        }

        protected void chkdesignation_CheckedChanged(object sender, EventArgs e)
        {
            if (chkdesignation.Checked)
            {
                gdvdesignationlist.Visible = true;
                txtelgibityamunt.Visible = false;
                laleid.Visible = false;
                // bindgriddesignation();
            }
            else
            {
                gdvdesignationlist.Visible = false;
                txtelgibityamunt.Visible = true;
                laleid.Visible = true;
            }
        }


    }
}