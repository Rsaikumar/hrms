﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="PromotionOrder.aspx.cs" Inherits="HRMS.Promotions.PromotionOrder" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function ValidationForDropDowns() {

            if (document.getElementById('<%=ddlnewdesignation.ClientID%>').options[document.getElementById('<%=ddlnewdesignation.ClientID%>').selectedIndex].value == "0") {
                alert("Please Select New Designation..");
                document.getElementById('<%=ddlnewdesignation.ClientID%>').focus();
                return false;
            }
               if (document.getElementById('<%=ddlplace.ClientID%>').options[document.getElementById('<%=ddlplace.ClientID%>').selectedIndex].value == "0") {
                alert("Please Select Reporting Place..");
                document.getElementById('<%=ddlplace.ClientID%>').focus();
                return false;
               }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.js"></script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>

                        <li><a href="../Employee/EmployeeDetailsList.aspx" class="current">Promotion Order</a> </li>
                    </ul>
                </div>
            </div>
        </section>
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <div class="panel-heading form-group col-md-12">
                            <header class="col-md-6 text-left">
                                <i class="fa fa-user"> Promotion Order</i>
                            </header>
                            <header class="col-md-6 form-group text-right">
                                <asp:LinkButton ID="lnkbackbtn" OnClick="lnkbackbtn_Click" runat="server"><i class="fa fa-arrow-left"> <b>Back</b></i></asp:LinkButton>
                            </header>
                        </div>
                    <div class="panel-body">

                        <div class="form-group col-md-6">
                            <label for="textfield" class="control-label col-sm-4" style="">
                                Employee Name</label>
                            <div class="col-sm-8" style="" id="Div13">
                                <asp:TextBox ID="txtempname" runat="server" placeholder="EmployeeName" class="form-control"
                                    MaxLength="25" TextMode="SingleLine" ReadOnly="true"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="textfield" class="control-label col-sm-4" style="">
                                Promotion Order Date<span style="color: #ff0000; font-size: 14px;">*</span></label>
                            <div class="col-sm-8" style="" id="Div66">
                                <asp:TextBox ID="txtpromotionorderdate" runat="server" placeholder="Ex:MM-dd-yyyy" class="form-control"
                                    MaxLength="25" TextMode="SingleLine" required></asp:TextBox>
                                <ajaxToolKit:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="imgPopup"
                                    TargetControlID="txtpromotionorderdate" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                </ajaxToolKit:CalendarExtender>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="form-group col-md-6">
                            <label for="textfield" class="control-label col-sm-4" style="">
                                Current Designation<span style="color: #ff0000; font-size: 14px;">*</span></label>
                            <div class="col-sm-8" style="" id="Div2">
                                <asp:TextBox ID="txtcurrentdesignation" runat="server" ReadOnly="true" placeholder="Current Designation" class="form-control"
                                    MaxLength="25" TextMode="SingleLine"></asp:TextBox>

                            </div>
                        </div>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-4" style="">
                                        New Designation<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                    <div class="col-sm-8" style="" id="Div3">
                                        <asp:DropDownList ID="ddlnewdesignation" runat="server" class="form-control DropdownCss">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="form-group col-md-6">
                            <label for="textfield" class="control-label col-sm-4" style="">
                                Old Basic Pay<span style="color: #ff0000; font-size: 14px;">*</span></label>
                            <div class="col-sm-8" style="" id="Div6">
                                <asp:TextBox ID="txtoldbasicpay" runat="server"  placeholder="Basic Pay" class="form-control"
                                    MaxLength="25" TextMode="SingleLine" ReadOnly="true" required></asp:TextBox>

                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="textfield" class="control-label col-sm-4" style="">
                                New Basic Pay<span style="color: #ff0000; font-size: 14px;">*</span></label>
                            <div class="col-sm-8" style="" id="Div7">
                                <asp:TextBox ID="txtnewbasicpay" runat="server" placeholder="New Basic Pay" class="form-control"
                                    MaxLength="25" TextMode="SingleLine" required></asp:TextBox>

                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="textfield" class="control-label col-sm-4" style="">
                                Old Reporting Officer<span style="color: #ff0000; font-size: 14px;">*</span></label>
                            <div class="col-sm-8" style="" id="Div8">
                                <asp:TextBox ID="txtoldreportingofficer" ReadOnly="true" runat="server" placeholder=" Old Reporting Officer" class="form-control"
                                    MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="textfield" class="control-label col-sm-4" style="">
                                New Reporting Officer<span style="color: #ff0000; font-size: 14px;">*</span></label>
                            <div class="col-sm-8" style="" id="Div9">
                                <asp:DropDownList ID="ddlnewreportingofficer" runat="server" class="form-control DropdownCss">
                                    <asp:ListItem Value="0">--Select Name--</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="textfield" class="control-label col-sm-4" style="">
                                Date Of Relieving<span style="color: #ff0000; font-size: 14px;">*</span></label>
                            <div class="col-sm-8" style="" id="Div10">
                                <asp:TextBox ID="txtdateofrelieving" runat="server" placeholder=" Date Of Relieving" class="form-control"
                                    MaxLength="25" TextMode="SingleLine" required></asp:TextBox>
                                <ajaxToolKit:CalendarExtender ID="CalendarExtender2" runat="server" PopupButtonID="imgPopup"
                                    TargetControlID="txtdateofrelieving" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                </ajaxToolKit:CalendarExtender>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="textfield" class="control-label col-sm-4" style="">
                                Date Of Joining<span style="color: #ff0000; font-size: 14px;">*</span></label>
                            <div class="col-sm-8" style="" id="Div11">
                                <asp:TextBox ID="txtdateofjoining" runat="server" placeholder="  Date Of Joining" class="form-control"
                                    MaxLength="25" TextMode="SingleLine" required></asp:TextBox>
                                <ajaxToolKit:CalendarExtender ID="CalendarExtender3" runat="server" PopupButtonID="imgPopup"
                                    TargetControlID="txtdateofjoining" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                </ajaxToolKit:CalendarExtender>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="textfield" class="control-label col-sm-4" style="">
                                Reporting Place<span style="color: #ff0000; font-size: 14px;">*</span></label>
                            <div class="col-sm-8" style="" id="Div12">
                                <asp:DropDownList ID="ddlplace" runat="server" class="form-control DropdownCss">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 text-center">
                            <asp:Button ID="btnsave" runat="server" Text="Save" CssClass="btn btn-primary" OnClientClick="return ValidationForDropDowns()" OnClick="btnsave_Click" />
                        </div>

                    </div>
                </section>
            </div>
        </div>


    </section>

</asp:Content>
