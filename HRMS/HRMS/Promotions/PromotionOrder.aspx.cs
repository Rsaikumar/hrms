﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;


namespace HRMS.Promotions
{
    public partial class PromotionOrder : System.Web.UI.Page
    {
        #region declarations

        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

        #endregion

        #region events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bindnewdesignation();
                ddlnewauthorityofficerbind();
                BindingReportingPlace();
                BindingDetails();

                if (Session["EmpID"] != null)
                {
                    int empid = Convert.ToInt32(Session["EmpID"]);

                    var bindname = (from item in objDAL.USP_gettingemployeenameandid(empid)
                                    select item).ToList();

                    if (bindname.Count() > 0)
                    {
                        txtempname.Text = bindname[0].empname;
                    }
                }
            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                int empid = Convert.ToInt32(Session["EmpID"]);
                HRMSEmployeeMasterRegistrationDetail emrd = objDAL.HRMSEmployeeMasterRegistrationDetails.Single(item => item.ID == empid);

                emrd.UserSubRole = Convert.ToInt32(ddlnewdesignation.SelectedValue);
                objDAL.SubmitChanges();

                var SalaryPercentages = (from item in objDAL.HRMSSalaryDeductionPercentages
                                         select item).ToList();

                double ccaper = Convert.ToDouble(SalaryPercentages[0].CCA);
                double esiper = Convert.ToDouble(SalaryPercentages[0].ESI);
                double hraper = Convert.ToDouble(SalaryPercentages[0].HRA);
                double daper = Convert.ToDouble(SalaryPercentages[0].DA);
                double pfper = Convert.ToDouble(SalaryPercentages[0].PF);

                double daamt = (Math.Round(Convert.ToDouble(txtnewbasicpay.Text) * Convert.ToDouble(daper)));
                double hraamt = (Math.Round(Convert.ToDouble(txtnewbasicpay.Text) * Convert.ToDouble(hraper)));
                double ccaamt = (Math.Round(Convert.ToDouble(txtnewbasicpay.Text) * Convert.ToDouble(ccaper)));
                double pfamt = (Math.Round(Convert.ToDouble(txtnewbasicpay.Text) * Convert.ToDouble(pfper)));
                double ga = Math.Round(Convert.ToDouble(txtnewbasicpay.Text) + Convert.ToDouble(daamt) + Convert.ToDouble(hraamt) + Convert.ToDouble(ccaamt));
                double esiamt = (Math.Round(Convert.ToDouble(ga) * Convert.ToDouble(esiper)));

                double total1;
                double TD;
                double x = 0.4;
                double y = 1600;

                total1 = (Math.Round(Convert.ToDouble(txtnewbasicpay.Text) * Convert.ToDouble(x)) + Convert.ToDouble(y));
                TD = (ga - total1) * 12;

                string tdsamount = 0.ToString();
                if (TD >= 0 && TD <= 250000)
                {
                    tdsamount = "0.00";
                }
                else if (TD >= 250001 && TD <= 500000)
                {
                    double p = 0.1;
                    double tds1 = (Math.Round(Convert.ToDouble(TD) * Convert.ToDouble(p)));
                    tdsamount = tds1.ToString();
                }
                else if (TD >= 500001 && TD <= 1000000)
                {
                    double q = 0.2;
                    double tds2 = (Math.Round(Convert.ToDouble(TD) * Convert.ToDouble(q)));
                    tdsamount = tds2.ToString();

                }
                else if (TD >= 1000001)
                {
                    double r = 0.3;
                    double tds3 = (Math.Round(Convert.ToDouble(TD) * Convert.ToDouble(r)));
                    tdsamount = tds3.ToString();

                }

                double less = 150;
                double more = 200;
                string professional = 0.ToString();
                if (ga >= 0 && ga <= 15000)
                {
                    professional = "0.00";
                }
                else if (ga >= 15001 && ga <= 20000)
                {
                    professional = less.ToString();
                }
                else if (ga >= 20001)
                {
                    professional = more.ToString();
                }

                HRMSEmployeeMasterSalaryDetail emsd = objDAL.HRMSEmployeeMasterSalaryDetails.Single(item => item.ID == empid);

                emsd.BasicAmount = Convert.ToDecimal(txtnewbasicpay.Text);
                emsd.GrossAmount = Convert.ToDecimal(ga);
                emsd.DA = Convert.ToDecimal(daamt);
                emsd.HRA = Convert.ToDecimal(hraamt);
                emsd.CCA = Convert.ToDecimal(ccaamt);
                objDAL.SubmitChanges();


                HRMSEmployeeMasterDeductionsDetail emd = objDAL.HRMSEmployeeMasterDeductionsDetails.Single(item => item.ID == empid);

                emd.TDSAmount = Convert.ToDecimal(tdsamount);
                emd.PFAmount = Convert.ToDecimal(pfamt);
                emd.ESIAmount = Convert.ToDecimal(esiamt);
                emd.NetAmount = Convert.ToDecimal(professional);
                objDAL.SubmitChanges();


                HRMSEmployeePromotionHistory eph = new HRMSEmployeePromotionHistory();
                eph.EmpId = empid;
                eph.Designation = txtcurrentdesignation.Text;
                eph.Basicpay = Convert.ToDecimal(txtoldbasicpay.Text);
                eph.ReportingOfficer = txtoldreportingofficer.Text;
                eph.OrderDate = DateTime.ParseExact(txtpromotionorderdate.Text, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                objDAL.HRMSEmployeePromotionHistories.InsertOnSubmit(eph);
                objDAL.SubmitChanges();

                HRMSPromotionOrder promotionorder = new HRMSPromotionOrder();

                promotionorder.EmployeeID = empid;
                promotionorder.PromotionOrderDate = DateTime.ParseExact(txtpromotionorderdate.Text, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                promotionorder.CurrentDesignation = txtcurrentdesignation.Text;
                promotionorder.NewDesignation = ddlnewdesignation.SelectedItem.Text;
                promotionorder.OldBasicPay = Convert.ToDecimal(txtoldbasicpay.Text);
                promotionorder.NewBasicPay = Convert.ToDecimal(txtnewbasicpay.Text);
                promotionorder.OldReportingOfficer = txtoldreportingofficer.Text;
                promotionorder.NewReportingOfficer = ddlnewreportingofficer.SelectedValue;
                promotionorder.DateOfRelieving = DateTime.ParseExact(txtdateofrelieving.Text, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                promotionorder.DateOfJoining = DateTime.ParseExact(txtdateofjoining.Text, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                promotionorder.ReportingPlace = ddlplace.SelectedItem.Text;
                promotionorder.CreatedBy = Convert.ToString(Session["Emp_Code"]);
                promotionorder.CreatedOn = DateTime.Now;
                promotionorder.ModifiedBy = "";
                objDAL.HRMSPromotionOrders.InsertOnSubmit(promotionorder);
                objDAL.SubmitChanges();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('sucessfully Saved');window.location ='../Employee/EmployeeDetailsView.aspx';", true);
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected void lnkbackbtn_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Employee/EmployeeDetailsView.aspx");
        }


        #endregion

        #region methods

        public void BindingDetails()
        {
            try
            {
                int empid = Convert.ToInt32(Session["EmpID"]);
                int ID = Convert.ToInt32(Session["Emp_Code"]);
                List<HRMSEmployeeMasterRegistrationDetail> objdesignation = (from master in objDAL.HRMSEmployeeMasterRegistrationDetails
                                                                             where master.ID == empid
                                                                             select master).ToList();
                if (objdesignation.Count > 0)
                {
                    List<HRMSRoleMaster> objsubrole = (from or in objDAL.HRMSRoleMasters
                                                              where or.RoleId == objdesignation[0].UserSubRole
                                                              select or).ToList();

                    txtcurrentdesignation.Text = Convert.ToString(objsubrole[0].RoleName);

                    List<HRMSEmployeeMasterOfficialInformationDetail> objEmpofficialinfo = (from master in objDAL.HRMSEmployeeMasterOfficialInformationDetails
                                                                                            where master.ID == Convert.ToInt32( empid.ToString())
                                                                                            select master).ToList();

                    var reporting = (from item in objDAL.USP_gettingemployeenameandid(Convert.ToInt32(objEmpofficialinfo[0].ReportingAuthority))
                                     select item).ToList();

                    txtoldreportingofficer.Text = reporting[0].empname;

                    List<HRMSEmployeeMasterSalaryDetail> objEmpsalaryinfo = (from master in objDAL.HRMSEmployeeMasterSalaryDetails
                                                                             where master.ID == Convert.ToInt32(empid.ToString())
                                                                             select master).ToList();
                    txtoldbasicpay.Text = Convert.ToString(objEmpsalaryinfo[0].BasicAmount);
                }
            }
            catch (Exception ex)
            {
               
            }
        }

        public void Bindnewdesignation()
        {
            try
            {
                var objHeaderInfo = (from header in objDAL.HRMSRoleMasters
                                     select header).ToList();
                if (objHeaderInfo.Count > 0)
                {
                    ddlnewdesignation.DataSource = objHeaderInfo;
                    ddlnewdesignation.DataTextField = "RoleName";
                    ddlnewdesignation.DataValueField = "RoleId";
                    ddlnewdesignation.DataBind();
                    ddlnewdesignation.Items.Insert(0, new ListItem("---Select---", "0"));
                }
                else
                {
                    ddlnewdesignation.Items.Insert(0, new ListItem("---Select---", "0"));
                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }
        }

        public void ddlnewauthorityofficerbind()
        {
            try
            {
                int empid = Convert.ToInt32(Session["Emp_Code"]);

                var Emps = (from Emp in objDAL.USP_GettingEmployeeNameandIdInPromotion()
                            where Emp.empid != Convert.ToInt32(Session["EmpID"])
                            select Emp).ToList();


                ddlnewreportingofficer.DataSource = Emps;
                ddlnewreportingofficer.DataTextField = "empname";
                ddlnewreportingofficer.DataValueField = "empid";
                ddlnewreportingofficer.DataBind();
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }
        }

        public void BindingReportingPlace()
        {
            try
            {
                var objPlace = (from header in objDAL.Circles
                                     orderby header.Circlename ascending
                                     select new { circlename = header.Circlename, circlecd = header.Circlecd }).Distinct().ToList();
                if (objPlace.Count > 0)
                {
                    ddlplace.DataSource = objPlace;
                    ddlplace.DataTextField = "circlename";
                    ddlplace.DataValueField = "circlecd";
                    ddlplace.DataBind();
                    ddlplace.Items.Insert(0, new ListItem("---Select---", "0"));
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }
        }

        #endregion
    }
}