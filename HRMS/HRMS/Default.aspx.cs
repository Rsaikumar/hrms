﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using BAL;
using DAL;

namespace HRMS
{
    public partial class Default : System.Web.UI.Page
    {
        Administrator_User_RegistrationBAL ObjAdmUser = new Administrator_User_RegistrationBAL();
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();
        AllMethodsBAL objAllGetMethods = new AllMethodsBAL();


        // // /// Object Creation For Item Group Domain
        Administrator_CompanyInfoBAL objSCM_userRegistration = new Administrator_CompanyInfoBAL();

        //Empaloyee BAL


        public string Status;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                /* Textbox text clear Empty start */
                txtUserName.Text = string.Empty;
                txtPassword.Text = string.Empty;

                /* Textbox text clear Empty end */
                Session.Abandon();
                txtUserName.Focus();
            }
        }


        #region Events
        //Login Button
        protected void btnLogin_Click(object sender, EventArgs e)
        {

            try
            {
                var objEmpAdlogin = (from log in objDAL.HRMSEmployeeMasterRegistrationDetails
                                   select log).ToList();
                if (txtUserName.Text == "Admin" && txtPassword.Text == "1234")
                {
                    Session["CreationCompany"] = "TSFD";
                    Session["ClientRegistrationNo"] = "000004";
                    Session["role"] = objEmpAdlogin[0].UserRole;
                    Session["User_Type"] = objEmpAdlogin[0].RoleType;
                    Session["User_Name"] = objEmpAdlogin[0].UserName;
                    Session["Emp_Code"] = objEmpAdlogin[0].ID;
                    Response.Redirect("Administrator/AdminDasboard.aspx");
                   

                }
                else
                {
                    ObjAdmUser.Login_Id = txtUserName.Text;
                    string login = txtUserName.Text;
                    ObjAdmUser.Password = txtPassword.Text;
                    string password = txtPassword.Text;
                    ObjAdmUser.ClientRegistrationNo = "000004";
                    ObjAdmUser.Creation_Company = "000004";
                    Session["ClientRegistrationNo"] = "000004";

                    //if (chkUserType.Checked)
                    //{

                    //  List<employee_detail_information> objEmplogin1 = objAllGetMethods.Get_Emp_LoginUser(txtUserName, txtPassword, (string)Session["ClientRegistrationNo"]);
                    var objEmplogin = (from log in objDAL.HRMSEmployeeMasterRegistrationDetails
                                       where log.UserName == txtUserName.Text && log.Password == txtPassword.Text
                                       select log).ToList();

                    if (objEmplogin.Count > 0)
                    {
                        List<Administrator_CompanyInfo> ObjCompanyInfo = objSCM_userRegistration.GetData_CompanyInfo(Session["ClientRegistrationNo"].ToString());
                        if (ObjCompanyInfo.Count > 0)
                        {
                            Session["CreationCompany"] = "TSFD";
                            Session["ClientRegistrationNo"] = "000004";
                            Session["role"] = objEmplogin[0].UserRole;
                            Session["User_Type"] = objEmplogin[0].RoleType;
                            Session["User_Name"] = objEmplogin[0].UserName;
                            Session["Emp_Code"] = objEmplogin[0].ID;


                            var GettingReportingAuthority = (from item in objDAL.HRMSEmployeeMasterOfficialInformationDetails
                                                             where item.EmpId == Convert.ToString(objEmplogin[0].ID)
                                                             select item).ToList();
                            string ra = GettingReportingAuthority[0].ReportingAuthority.ToString();

                            Session["ReportingAuthority"] = GettingReportingAuthority[0].ReportingAuthority.ToString();

                            //Session["Module"] = objEmplogin[0].Modules;
                            Response.Redirect("Dashboard/Dashboard.aspx");
                        }
                    }
                    else
                    {
                        txtUserName.Text = "";
                        txtUserName.Focus();
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Error: You Have Entered Incorrect Username Or Password.');", true);
                    }
                }
            }
            //else
            //{
            //    var objUserLogin = (from UserLogin in objDAL.Administrator_UserRegistrations
            //                        where UserLogin.Login_Id == login && UserLogin.Password == password
            //                        select UserLogin).ToList();
            //    if (objUserLogin.Count > 0)
            //    {
            //        string[] CmpName = objUserLogin[0].AllowCompanyListID.Split(',');
            //        string UserType = objUserLogin[0].User_Type;

                //        for (int i = 0; i < CmpName.Length; i++)
            //        {
            //            if (CmpName[i] == "000004")
            //            {
            //                Session["ClientRegistrationNo"] = "000004";
            //                Session["CreationCompany"] = "TSFD";
            //                Session["User_Type"] = UserType;
            //                Session["User_Name"] = txtUserName.Text;
            //                //  Session["Password"] = txtPassword.Text;
            //                Response.Redirect("Dashboard/Dashboard.aspx");
            //            }

                //        }
            //        if (ddlCompanyName.SelectedValue == "0")
            //        {
            //            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Select Company Name ... ?');", true);
            //            ddlCompanyName.Focus();
            //        }
            //    }
            //    //List<Administrator_UserRegistration> objuserlogin = objAllGetMethods.Get_Sale_LoginUser(login, password, (string)Session["CreationCompany1"]);
            //    //if (objuserlogin.Count > 0)
            //    //{
            //    //    Session["ClientRegistrationNo"] = objuserlogin[0].ClientRegistrationNo;
            //    //    Session["Creation_Company"] = objuserlogin[0].Creation_Company;
            //    //    Session["User_Type"] = objuserlogin[0].User_Type;
            //    //    Session["User_Name"] = objuserlogin[0].User_Name;
            //    //  //  Session["Module"] = objuserlogin[0].Modules;
            //    //    Response.Redirect("Dashboard/Dashboard.aspx");
            //    //}
            //    else
            //    {
            //        txtUserName.Text = "";
            //        txtUserName.Focus();
            //        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Error: You Have Entered Incorrect Username Or Password.');", true);
            //    }
            //}
            // }
            //}
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }
            finally
            {

            }
        }

        //protected void txtUserName_TextChanged(object sender, EventArgs e)
        //{
        //   // CompaniesBind();

        //}
        #endregion
        //protected void btnLogin_Click(object sender, EventArgs e)
        //{
        //    Response.Redirect("Dashboard/Dashboard.aspx");
        //}

        #region Methods

        //DropDown Bind Code for Select Company List

        //public void CompaniesBind()
        //{
        //    try
        //    {

        //        ObjAdmUser.Login_Id = txtUserName.Text.ToString();

        //        string login = txtUserName.Text.ToString();
        //        ddlCompanyName.Items.Clear();
        //        List<Administrator_UserRegistration> objUserRegistration = ObjAdmUser.Get_CompanyDetails(login);


        //        if (objUserRegistration.Count > 0)
        //        {
        //            ddlCompanyName.DataSource = objUserRegistration;
        //            string[] items1 = objUserRegistration[0].AllowCompanyList.Split(',');

        //            string[] CmpName = objUserRegistration[0].AllowCompanyListID.Split(',');
        //            //Session["BranchID"] = objUserRegistration[0].Branch_ID;
        //            Session["Status"] = objUserRegistration[0].Status;
        //            //  ddlCompanyName.DataBind();
        //            //foreach (var li in items1)
        //            //{
        //            //    ddlCompanyName.DataTextField = li;
        //            //    ddlCompanyName.DataValueField = "Creation_Company";
        //            //}
        //            for (int i = 0; i < items1.Length; i++)
        //            {
        //                ddlCompanyName.Items.Insert(i, new ListItem(items1[i], CmpName[i]));
        //                //ddlCompanyName.Items.Insert(i, new ListItem(items1[i], items1[i]));
        //            }

        //            ddlCompanyName.Items.Insert(0, new ListItem("-- Select Company Name --", "0")); //updated code
        //            txtPassword.Focus();
        //        }
        //        else
        //        {
        //            var objuser = (from ep in objDAL.HRMS_Employee_Master_PersonalDetails
        //                           join CM in objDAL.Administrator_CompanyInfos on ep.Creation_Company equals CM.ClientRegistrationNo
        //                           where ep.User_Name == txtUserName.Text
        //                           select new
        //                           {
        //                               CM.Company_Name,
        //                               ep.Creation_Company,
        //                               ep.Emp_Code,
        //                               ep.Emp_Status

        //                           }).Distinct().ToList();
        //            if (objuser.Count > 0)
        //            {
        //                Session["Emp_Code"] = objuser[0].Emp_Code.ToString();
        //                Session["CreationCompany"] = "TSFD";
        //                Session["ClientRegistrationNo"] = "000004";
        //                ddlCompanyName.DataSource = objuser;
        //                ddlCompanyName.DataTextField = "Company_Name";
        //                ddlCompanyName.DataValueField = "Creation_Company";
        //                ddlCompanyName.DataBind();
        //                ddlCompanyName.Items.Insert(0, new ListItem("--Select Company Name--", "Select Company Name"));
        //                Session["Status"] = objuser[0].Emp_Status;
        //                txtPassword.Focus();
        //            }
        //            else
        //            {
        //                ddlCompanyName.DataSource = null;
        //                ddlCompanyName.DataBind();
        //                ddlCompanyName.Items.Clear();
        //                ddlCompanyName.Items.Insert(0, new ListItem("Select Company Name", "Select Company Name"));
        //                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Error: You Have Entered Incorrect Username.');", true);
        //                txtUserName.Text = "";
        //                txtUserName.Focus();

        //            }
        //            //ddlCompanyName.DataSource = null;
        //            //ddlCompanyName.DataBind();
        //            //ddlCompanyName.Items.Clear();
        //            //ddlCompanyName.Items.Insert(0, new ListItem("--Select Company Name--", "0"));
        //            //ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Error: You Have Entered Incorrect Username.');", true);
        //            //txtUserName.Focus();
        //            //txtUserName.Text = "";
        //            // ddlCompanyName.Items.Insert(0, new ListItem("-- Select Company Name --", "0")); //updated code
        //            // ddlCompanyName.Items.Insert(1, new ListItem("Laksana IT pvt ltd", "L000001")); //updated code
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Session["Error"] = ex.ToString();

        //        // Server.Transfer("~/Modules/Error_Page.aspx");

        //    }

        //}
        #endregion
    }
}