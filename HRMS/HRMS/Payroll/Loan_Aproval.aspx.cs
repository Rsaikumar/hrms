﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BAL;
using DAL;
using System.Data.Linq.SqlClient;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using System.IO;

namespace HRMS.Payroll
{
    public partial class Loan_Aproval : System.Web.UI.Page
    {
        LoanapplicationBAL objlapp = new LoanapplicationBAL();
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();
        HRMSLoanMaster objLoanapp = new HRMSLoanMaster();

        private int SNO = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               // drop();
                Bind_Loan();
                Emp_NameBinding();

                bindbasicamount();
                if ((string)Session["AdditionCode"] != null)
                {

                    //  Request.QueryString.Clear();

                    string ClientRegistrationNo = Session["ClientRegistrationNo"].ToString();
                    string AdditionCode = (string)Session["AdditionCode"];
                    Session.Remove("Loan_Code");

                    //string LoginID = Request.QueryString["id"];
                    Edit_HeaderDetails(AdditionCode, ClientRegistrationNo);
                    btnSave.Visible = false;

                }
                else
                {
                    btnUpdate.Visible = false;
                }
            }
        }
        public void bindbasicamount()
        {

            try
            {
                string CreationCompany = Session["ClientRegistrationNo"].ToString();

                string Emp_Code = (from em in ObjDAL.HRMS_Employee_Master_PersonalDetails where em.User_Name == Session["User_Name"].ToString() select em.Emp_Code).FirstOrDefault();
                var basicamount = (from amount in ObjDAL.HRMS_Employee_Official_Info_Head_Details
                                   where amount.Creation_Company == CreationCompany && amount.Emp_Code == Emp_Code
                                   select amount.Basic_Amount).FirstOrDefault();



                txtbasicamount.Text = basicamount.ToString();

                //ddlbasicamount.DataValueField = "Emp_Code";
                //ddlbasicamount.DataBind();



            }
            catch (Exception)
            {

                throw;
            }

        }

        public void Emp_NameBinding()
        {
            try
            {
                string CreationCompany = Session["ClientRegistrationNo"].ToString();

                var objEmPCodeDUP = (from itmes in ObjDAL.HRMS_Employee_Master_PersonalDetails
                                     where itmes.Creation_Company == CreationCompany
                                     select new
                                     {
                                         Emp_name = itmes.Emp_Last_Name + " " + itmes.Emp_First_Name + " " + itmes.Emp_Middle_Name,
                                         Emp_Code = itmes.Emp_Code
                                     }).ToList();
                if (objEmPCodeDUP.Count > 0)
                {
                    ddlEmpName.DataSource = objEmPCodeDUP;
                    ddlEmpName.DataTextField = "Emp_name";
                    ddlEmpName.DataValueField = "Emp_Code";
                    ddlEmpName.DataBind();
                    ddlEmpName.Items.Insert(0, new ListItem("--Select--", "0"));
                }

            }
            catch (Exception)
            {

                throw;
            }

        }
        public void Bind_Loan()
        {
            try
            {
                string Cmpy = (string)Session["ClientRegistrationNo"];
                string Brnach = (string)Session["BranchID"];
                string UserType = (string)Session["User_Type"];
                string UserName = (string)Session["User_Name"];
                string Cration_Company = (string)Session["CreationCompany"]; //"000001";
                //if (UserType == "User")
                //{
                var objHeaderInfo = (from header in ObjDAL.HRMS_Loan_Masters
                                     where header.Creation_Company == Cmpy
                                     orderby header.Sno descending
                                     select header).ToList();
                // List<Administrator_UserRegistration> ObjuserInfo = objSCM_userRegistration.Get_userRegDetails(Session["ClientRegistrationNo"].ToString());
                if (objHeaderInfo.Count > 0)
                {
                    ddlLeaveApply.DataTextField = "Short_Name";
                    ddlLeaveApply.DataValueField = "Loan_Code";
                    ddlLeaveApply.DataSource = objHeaderInfo;
                    ddlLeaveApply.DataBind();


                }
                else
                {

                    ddlLeaveApply.Items.Insert(0, new ListItem("--Select--", "Select"));
                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }

        }

        //public void drop()
        //{
        //    var listo = (from leave in ObjDAL.HRMS_Loan_Masters
        //                 select leave.Short_Name).ToList();
        //    ddlLeaveApply.DataSource = listo;

        //    ddlLeaveApply.DataBind();
        //}
        //clearfields
        public void ClearFields()
        {
            txtEmpCode.Text = "";

            txtDepartment.Text = "";
            txtDesignation.Text = "";

            ddlLeaveApply.SelectedValue = "Select";

            ddlEmpName.SelectedIndex = 0;



        }
        //Saving
        private void Saving()
        {

            try
            {
                objlapp.Emp_Code = txtEmpCode.Text;

                objlapp.Emp_Name = txtEmpName.Text;// ddlEmpName.SelectedValue;
                objlapp.Department = txtDepartment.Text;
                objlapp.Designation = txtDesignation.Text;
                objlapp.Reporting_Authority = txtReportingAuthority.Text;
                objlapp.Type_Of_Loan_Apply = ddlLeaveApply.SelectedItem.Text;
                objlapp.Basic_Amount = (txtbasicamount.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtbasicamount.Text);
                objlapp.Loan_amount_Required = (txtlar.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtlar.Text);
                objlapp.Eligibility_Amount = (txtelgamount.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtelgamount.Text);
                // objlapp.Rate_Of_Interest=(txtroi.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtroi.Text);
                objlapp.Status = ddlstatus.SelectedItem.Text;

                objlapp.Created_By = Session["User_Name"].ToString();
                objlapp.Created_Date = System.DateTime.Now;
                objlapp.Creation_Company = Session["ClientRegistrationNo"].ToString();
                objlapp.Modified_By = Session["User_Name"].ToString();
                objlapp.Modified_Date = System.DateTime.Now;
                objlapp.Parameter = 1;




                if (objlapp.Insert_HRMS_Loan_Application() != 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);
                    ClearFields();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
                }

            }
            catch (Exception ex)
            {

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            { }
        }
        public void Edit_HeaderDetails(string additioncode, string CreationCompany)
        {
            if (Session["User_Type"].ToString() == "CCF")
            {
                ddlstatus.Enabled = false;
                txttenure.ReadOnly = true;
                txtroi.ReadOnly = true;
                txtlar.ReadOnly = true;
                ddlLeaveApply.Enabled = false;
                txtelgamount.ReadOnly = true;
            }

            SNO = Convert.ToInt32(Request.QueryString["Id"].ToString());
            try
            {

                var listo = (from leave in ObjDAL.HRMS_Loan_Applications
                             where leave.ID == SNO
                             select leave).ToList();
                if (listo.Count > 0)
                {
                    //drop();

                    txtEmpCode.Text = listo[0].EmpCode;
                   // txtEmpName.Text = listo[0].Emp_Name;
                    //txtDepartment.Text = listo[0].Department;
                    //txtDesignation.Text = listo[0].Designation;
                    txtReportingAuthority.Text = listo[0].ReportingAuthority;
                    ddlLeaveApply.Text = listo[0].TypeOfLoanApply;
                    ddlstatus.Text = listo[0].Status;
                    txttenure.Text = Convert.ToInt32(listo[0].Tenure).ToString();
                    txtroi.Text = Convert.ToDecimal(listo[0].RateOfInterest).ToString();
                    txtbasicamount.Text = listo[0].BasicAmount.ToString();
                    txtelgamount.Text = listo[0].EligibilityAmount.ToString();
                    txtlar.Text = listo[0].LoanAmountRequired.ToString();


                    var listLeaveD = (from loan in ObjDAL.HRMS_Loan_Applications
                                      select loan).ToList();




                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }


        }



        public void Update_Method()
        {
            try
            {


                SNO = Convert.ToInt32(Request.QueryString["Id"].ToString());
                objlapp.Emp_Code = txtEmpCode.Text;
                objlapp.Emp_Name = txtEmpName.Text;// ddlEmpName.SelectedValue;
                objlapp.Department = txtDepartment.Text;
                objlapp.Designation = txtDesignation.Text;
                objlapp.Reporting_Authority = txtReportingAuthority.Text;
                objlapp.Type_Of_Loan_Apply = ddlLeaveApply.SelectedItem.Text;
                objlapp.Status = ddlstatus.SelectedValue;
                objlapp.Loan_Code = ddlLeaveApply.SelectedValue;
                objlapp.Basic_Amount = (txtbasicamount.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtbasicamount.Text);
                objlapp.Loan_amount_Required = (txtlar.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtlar.Text);
                objlapp.Eligibility_Amount = (txtelgamount.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtelgamount.Text);
                objlapp.Rate_Of_Interest = (txtroi.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtroi.Text);
                objlapp.tenure = Convert.ToInt32(txttenure.Text);
                objlapp.Created_By = Session["User_Name"].ToString();
                objlapp.Created_Date = System.DateTime.Now;
                objlapp.Creation_Company = Session["ClientRegistrationNo"].ToString();
                objlapp.Modified_By = Session["User_Name"].ToString();
                objlapp.Modified_Date = System.DateTime.Now;
                objlapp.Parameter = 2;
                objlapp.Sno = SNO;
                if (objlapp.Insert_HRMS_Loan_Application() != 0)
                {

                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);
                    ClearFields();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            { }
        }
        protected void txtEmpCode_OnTextChanged(object sender, EventArgs e)
        {
            string CreationCompany = (string)Session["ClientRegistrationNo"].ToString();
            string Emp_Code = (txtEmpCode.Text.ToLower().Trim() == "") ? "" : txtEmpCode.Text.ToLower().Trim();
            var objEmPCodeDUP = (from itmes in ObjDAL.HRMS_Employee_Master_PersonalDetails
                                 where itmes.Creation_Company == CreationCompany && itmes.Emp_Code.ToLower().Trim() == Emp_Code
                                 select new
                                 {
                                     Emp_name = itmes.Emp_Last_Name + " " + itmes.Emp_First_Name + " " + itmes.Emp_Middle_Name,
                                     Emp_Code = itmes.Emp_Code
                                 }).ToList();
            if (objEmPCodeDUP.Count > 0)
            {
                ddlEmpName.SelectedItem.Text = Emp_Code;
                var s = (from x in ObjDAL.HRMS_Employee_Master_Official_Informations
                         where x.Emp_Code == txtEmpCode.Text && x.Creation_Company == CreationCompany
                         select x).ToList();
                if (s.Count > 0)
                {
                    txtDepartment.Text = s[0].Department;
                    txtDesignation.Text = s[0].Designation;
                    txtReportingAuthority.Text = s[0].Reporting_Authority;
                    ddlLeaveApply.Focus();
                }
                else
                {
                    txtDepartment.Text = "";
                    txtDesignation.Text = "";
                    txtReportingAuthority.Text = "";
                }
            }
            else
            {
                txtEmpCode.Text = "";
                txtEmpCode.Focus();
                ddlEmpName.SelectedValue = "0";
                txtDepartment.Text = "";
                txtDesignation.Text = "";
                txtReportingAuthority.Text = "";
            }
        }
        protected void ddlLeaveApply_SelectedIndexChanged(object sender, EventArgs e)
        {



            var objHeaderInfo = (from header in ObjDAL.HRMS_Loan_Masters
                                 where header.Short_Name == ddlLeaveApply.SelectedItem.Text
                                 orderby header.Sno descending
                                 select header).ToList();

        }




        protected void ddlEmpName_SelectedIndexChanged(object sender, EventArgs e)
        {
            string CreationCompany = (string)Session["ClientRegistrationNo"].ToString();
            string Emp_Code = ddlEmpName.SelectedItem.Text;
            var objEmPCodeDUP = (from itmes in ObjDAL.HRMS_Employee_Master_PersonalDetails
                                 where itmes.Creation_Company == CreationCompany && itmes.Emp_Code.ToLower().Trim() == Emp_Code
                                 select new
                                 {
                                     Emp_name = itmes.Emp_Last_Name + " " + itmes.Emp_First_Name + " " + itmes.Emp_Middle_Name,
                                     Emp_Code = itmes.Emp_Code
                                 }).ToList();
            if (objEmPCodeDUP.Count > 0)
            {
                ddlEmpName.SelectedItem.Text = Emp_Code;
                txtEmpCode.Text = ddlEmpName.SelectedItem.Text;

                var s = (from x in ObjDAL.HRMS_Employee_Master_Official_Informations
                         where x.Emp_Code == Emp_Code && x.Creation_Company == CreationCompany
                         select x).ToList();
                if (s.Count > 0)
                {
                    txtDepartment.Text = s[0].Department;
                    txtDesignation.Text = s[0].Designation;
                    txtReportingAuthority.Text = s[0].Reporting_Authority;
                    ddlLeaveApply.Focus();
                }
                else
                {
                    txtEmpCode.Text = "";
                    txtDepartment.Text = "";
                    txtDesignation.Text = "";
                    txtReportingAuthority.Text = "";
                    ddlEmpName.Focus();
                    ddlEmpName.SelectedIndex = 0;
                }
            }
            else
            {
                txtEmpCode.Text = "";
                txtEmpCode.Focus();
                ddlEmpName.SelectedValue = "0";
                txtDepartment.Text = "";
                txtDesignation.Text = "";
                txtReportingAuthority.Text = "";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Saving();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Update_Method();
        }


    }
}