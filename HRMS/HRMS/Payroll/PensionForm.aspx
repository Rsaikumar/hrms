﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="PensionForm.aspx.cs" Inherits="HRMS.Payroll.Pension_Form" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/formcss.css" rel="stylesheet" />
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="#">Employee</a><i class="fa fa-angle-right"></i> </li>
                        <li><a href="PensionForm.aspx" class="current">Pension Form</a> </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <i class="fa fa-th-list"> Pension Form</i>
                        </header>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group col-md-6" id="empid" runat="server" visible="false">
                                            <label for="textfield" class="control-label col-sm-5" style="">
                                                Emp Code
                                            </label>
                                            <div class="col-sm-7" style="">
                                                <asp:TextBox ID="txtEmpCode" runat="server" placeholder="Emp Code" MaxLength="50"
                                                    class="form-control" OnTextChanged="txtEmpCode_TextChanged"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="textfield" class="control-label col-sm-5" style="">
                                                Circle</label>
                                            <div class="col-sm-7" style="" id="Div39">
                                                <asp:TextBox ID="txtcicle" runat="server" MaxLength="50"
                                                    class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="textfield" class="control-label col-sm-5" style="">
                                                Divisions</label>
                                            <div class="col-sm-7" style="" id="Div41">
                                                <asp:DropDownList ID="ddlforestdivisions" runat="server" class="form-control DropdownCss" Visible="false">
                                                    <asp:ListItem Text="---Select---" Value="---Select---"></asp:ListItem>
                                                </asp:DropDownList>

                                                <asp:TextBox ID="txtdivisions" runat="server" MaxLength="50"
                                                    class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6" id="Div1" runat="server">
                                            <label for="textfield" class="control-label col-sm-5" style="">
                                                Department<span id="Span2" runat="server" style="color: Red; display: inline;"></span></label>
                                            <div class="col-sm-7" style="" id="Div2">
                                                <asp:DropDownList ID="ddldepartment" runat="server" Visible="false" class="form-control DropdownCss">
                                                    <asp:ListItem>---Select---</asp:ListItem>
                                                    <asp:ListItem>PCCF</asp:ListItem>
                                                </asp:DropDownList>

                                                <asp:TextBox ID="txtdeprtment" runat="server" MaxLength="50"
                                                    class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <section class="panel">
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="form-group col-md-6" id="Div3" runat="server">
                            <label for="textfield" class="control-label col-sm-5" style="">
                                Name Of Employee Retired & Retiring within Before (18) Months<span id="Span3" runat="server" style="color: Red; display: inline;"></span></label>
                            <div class="col-sm-7" style="" id="Div4">
                                <asp:TextBox ID="txtnameretiremonths" runat="server" MaxLength="50"
                                    class="form-control" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-md-6" id="Div5" runat="server">
                            <label for="textfield" class="control-label col-sm-5" style="">
                                Designation<span id="Span4" runat="server" style="color: Red; display: inline;"></span></label>
                            <div class="col-sm-7" style="" id="Div6">
                                <asp:TextBox ID="txtdesignation" runat="server" MaxLength="50"
                                    class="form-control" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group col-md-6" id="Div42" runat="server">
                            <label for="textfield" class="control-label col-sm-5" style="">
                                Date Of Birth<span id="Span21" runat="server" style="color: Red; display: inline;"></span></label>
                            <div class="col-sm-7" style="" id="Div43">
                                <asp:TextBox ID="txtdob" runat="server" MaxLength="50"
                                    class="form-control" required></asp:TextBox>
                                <ajaxToolKit:CalendarExtender ID="CalendarExtender2" PopupButtonID="imgPopup" runat="server"
                                    TargetControlID="txtdob" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                </ajaxToolKit:CalendarExtender>
                            </div>
                        </div>
                        <div class="form-group col-md-6" id="Div44" runat="server">
                            <label for="textfield" class="control-label col-sm-5" style="">
                                Date Of Entry Into Service<span id="Span22" runat="server" style="color: Red; display: inline;"></span></label>
                            <div class="col-sm-7" style="" id="Div45">

                                <asp:TextBox ID="txtenterservice" runat="server" MaxLength="50"
                                    class="form-control" required></asp:TextBox>
                                <ajaxToolKit:CalendarExtender ID="CalendarExtender1" PopupButtonID="imgPopup" runat="server"
                                    TargetControlID="txtenterservice" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                </ajaxToolKit:CalendarExtender>
                            </div>
                        </div>
                        <div class="form-group col-md-6" id="Div46" runat="server">
                            <label for="textfield" class="control-label col-sm-5" style="">
                                Place Of Last station/Presently Working<span id="Span23" runat="server" style="color: Red; display: inline;"></span></label>
                            <div class="col-sm-7" style="" id="Div47">
                                <asp:TextBox ID="txtplaceofstation" runat="server" MaxLength="50"
                                    class="form-control" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-md-6" id="Div7" runat="server">
                            <label for="textfield" class="control-label col-sm-5" style="">
                                Date Of Retirement/Death<span id="Span5" runat="server" style="color: Red; display: inline;"></span></label>
                            <div class="col-sm-7" style="" id="Div8">
                                <asp:TextBox ID="txtdateofretirement" runat="server" MaxLength="50"
                                    class="form-control" required> </asp:TextBox>
                                <ajaxToolKit:CalendarExtender ID="CalendarExtender3" PopupButtonID="imgPopup" runat="server"
                                    TargetControlID="txtdateofretirement" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                </ajaxToolKit:CalendarExtender>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group col-md-6" id="Div9" runat="server">
                            <label for="textfield" class="control-label col-sm-5" style="">
                                Date Of Receipt Of Pension Papers<span id="Span6" runat="server" style="color: Red; display: inline;"></span></label>
                            <div class="col-sm-7" style="" id="Div10">
                                <asp:TextBox ID="txtdateofreceiptofpensionpapers" runat="server" MaxLength="50"
                                    class="form-control" required></asp:TextBox>
                                <ajaxToolKit:CalendarExtender ID="CalendarExtender8" PopupButtonID="imgPopup" runat="server"
                                    TargetControlID="txtdateofreceiptofpensionpapers" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                </ajaxToolKit:CalendarExtender>
                            </div>
                        </div>
                        <div class="form-group col-md-6" id="Div15" runat="server">
                            <label for="textfield" class="control-label col-sm-5" style="">
                                Status On Processing of Pension Papers<span id="Span9" runat="server" style="color: Red; display: inline;"></span></label>
                            <div class="col-sm-7" style="" id="Div16">
                                <asp:DropDownList ID="ddlstatuspensionpapers" runat="server" CssClass="form-control">
                                    <asp:ListItem>---Select---</asp:ListItem>
                                    <asp:ListItem>Under Scruntity</asp:ListItem>
                                    <asp:ListItem>Forwarded To Pension Sanctioning Authority</asp:ListItem>
                                    <asp:ListItem>Forwared To AG/Audit Officer</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group col-md-6" id="Div17" runat="server">
                            <label for="textfield" class="control-label col-sm-5" style="">
                                Date Of Forwarding Pension Papers To The AG/AD<span id="Span10" runat="server" style="color: Red; display: inline;"></span></label>
                            <div class="col-sm-7" style="" id="Div18">
                                <asp:TextBox ID="txtdateofforwarding" runat="server" MaxLength="50"
                                    class="form-control" required></asp:TextBox>
                                <ajaxToolKit:CalendarExtender ID="CalendarExtender4" PopupButtonID="imgPopup" runat="server"
                                    TargetControlID="txtdateofforwarding" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                </ajaxToolKit:CalendarExtender>
                            </div>
                        </div>
                        <div class="form-group col-md-6" id="Div19" runat="server">
                            <label for="textfield" class="control-label col-sm-5" style="">
                                Date Of Receipt Of PVR from AG/AD in PSA<span id="Span11" runat="server" style="color: Red; display: inline;"></span></label>
                            <div class="col-sm-7" style="" id="Div20">
                                <asp:TextBox ID="txtdatepvr" runat="server" MaxLength="50"
                                    class="form-control" required></asp:TextBox>
                                <ajaxToolKit:CalendarExtender ID="CalendarExtender5" PopupButtonID="imgPopup" runat="server"
                                    TargetControlID="txtdatepvr" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                </ajaxToolKit:CalendarExtender>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group col-md-6" id="Div11" runat="server">
                            <label for="textfield" class="control-label col-sm-5" style="">
                                Proposals Forwarded to AG/AD for Authorisation Of<span id="Span7" runat="server" style="color: Red; display: inline;"></span></label>
                            <div class="col-sm-7" style="" id="Div12">
                                <asp:DropDownList ID="ddlforwardedproposals" runat="server" CssClass="form-control">
                                    <asp:ListItem>---Select---</asp:ListItem>
                                    <asp:ListItem>Service Pension</asp:ListItem>
                                    <asp:ListItem>Provisional Pension</asp:ListItem>
                                    <asp:ListItem>Family Pension</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group col-md-6" id="Div21" runat="server">
                            <label for="textfield" class="control-label col-sm-5" style="">
                                Date Of Sending for Authorisation to AG/AD<span id="Span12" runat="server" style="color: Red; display: inline;"></span></label>
                            <div class="col-sm-7" style="" id="Div22">
                                <asp:TextBox ID="txtdateofsendingauthorisation" runat="server" MaxLength="50"
                                    class="form-control"></asp:TextBox>
                                <ajaxToolKit:CalendarExtender ID="CalendarExtender9" PopupButtonID="imgPopup" runat="server"
                                    TargetControlID="txtdateofsendingauthorisation" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                </ajaxToolKit:CalendarExtender>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group col-md-6" id="Div13" runat="server">
                            <label for="textfield" class="control-label col-sm-5" style="">
                                Date Of receipt for Authorisation from AG/AD<span id="Span8" runat="server" style="color: Red; display: inline;"></span></label>
                            <div class="col-sm-7" style="" id="Div14">
                                <asp:TextBox ID="txtdateofreceiptgauthorisation" runat="server" MaxLength="50"
                                    class="form-control" required></asp:TextBox>
                                <ajaxToolKit:CalendarExtender ID="CalendarExtender10" PopupButtonID="imgPopup" runat="server"
                                    TargetControlID="txtdateofreceiptgauthorisation" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                </ajaxToolKit:CalendarExtender>
                            </div>
                        </div>
                        <div class="form-group col-md-6" id="Div54" runat="server">
                            <label for="textfield" class="control-label col-sm-5" style="">
                                Reasons For Not Sending for Full Pension & Gratuity authorisation to AG/AD<span id="Span27" runat="server" style="color: Red; display: inline;"></span></label>
                            <div class="col-sm-7" style="" id="Div55">
                                <asp:DropDownList ID="ddlgratuity" runat="server" CssClass="form-control">
                                    <asp:ListItem>---Select---</asp:ListItem>
                                    <asp:ListItem>Disc Cases</asp:ListItem>
                                    <asp:ListItem>Vig Cases</asp:ListItem>
                                    <asp:ListItem>ACB Cases</asp:ListItem>
                                    <asp:ListItem>Court Cases</asp:ListItem>
                                    <asp:ListItem>Criminal Cases</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group col-md-6" id="Div50" runat="server">
                            <label for="textfield" class="control-label col-sm-5" style="">
                                Date Of Receipt Of Requisition for Sanction Of Anticipatory Pension<span id="Span25" runat="server" style="color: Red; display: inline;"></span></label>
                            <div class="col-sm-7" style="" id="Div51">

                                <asp:TextBox ID="txtDateOfReceiptOfRequisitionforSanctionOfAnticipatoryPension" runat="server" MaxLength="50"
                                    class="form-control" required></asp:TextBox>
                                <ajaxToolKit:CalendarExtender ID="CalendarExtender6" PopupButtonID="imgPopup" runat="server"
                                    TargetControlID="txtDateOfReceiptOfRequisitionforSanctionOfAnticipatoryPension" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                </ajaxToolKit:CalendarExtender>
                            </div>
                        </div>
                        <div class="form-group col-md-6" id="Div48" runat="server">
                            <label for="textfield" class="control-label col-sm-5" style="">
                                Date of Sanction Of Anticipatory pension by the Competent authority<span id="Span24" runat="server" style="color: Red; display: inline;"></span></label>
                            <div class="col-sm-7" style="" id="Div49">
                                <asp:TextBox ID="txtdatereceiptofautherity" runat="server" MaxLength="50"
                                    class="form-control" required></asp:TextBox>
                                <ajaxToolKit:CalendarExtender ID="CalendarExtender7" PopupButtonID="imgPopup" runat="server"
                                    TargetControlID="txtdatereceiptofautherity" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                </ajaxToolKit:CalendarExtender>
                            </div>
                        </div>
                        <div class="form-group col-md-6" id="Div23" runat="server">
                            <label for="textfield" class="control-label col-sm-5" style="">
                                Status On Issues Mentioned in col.no.16<span id="Span13" runat="server" style="color: Red; display: inline;"></span></label>
                            <div class="col-sm-7" style="" id="Div24">
                                <asp:DropDownList ID="ddlissuesmentioned" runat="server" CssClass="form-control">
                                    <asp:ListItem>---Select---</asp:ListItem>
                                    <asp:ListItem>Under Process</asp:ListItem>
                                    <asp:ListItem>Pending At FDO</asp:ListItem>
                                    <asp:ListItem>Pending At DFO</asp:ListItem>
                                    <asp:ListItem>Pending At CF</asp:ListItem>
                                    <asp:ListItem>Pending At PCCF</asp:ListItem>
                                    <asp:ListItem>Pending At Commissioner Of Enquiries</asp:ListItem>
                                    <asp:ListItem>Pending At Govt</asp:ListItem>
                                    <asp:ListItem>Para Wise Remarks Approved</asp:ListItem>
                                    <asp:ListItem>Counter Filed</asp:ListItem>
                                    <asp:ListItem>Pending At Court</asp:ListItem>
                                    <asp:ListItem>Conclused</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group col-md-6" id="Div56" runat="server">
                            <label for="textfield" class="control-label col-sm-5" style="">
                                Action Taken On Conclusion Of Issues Mentioned In Column No.16<span id="Span28" runat="server" style="color: Red; display: inline;"></span></label>
                            <div class="col-sm-7" style="" id="Div57">
                                <asp:DropDownList ID="ddlactiontaken" runat="server" CssClass="form-control">
                                    <asp:ListItem>---Select---</asp:ListItem>
                                    <asp:ListItem>Calling Of NDC</asp:ListItem>
                                    <asp:ListItem>Proposals Forwarding To PSA for Sanction Orders</asp:ListItem>
                                    <asp:ListItem>Forwarding Sanction Orders To AG/AD by PSA</asp:ListItem>
                                    <asp:ListItem>Receipt Of Authorisation From AG/AD</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group col-md-6" id="Div58" runat="server">
                            <label for="textfield" class="control-label col-sm-5" style="">
                                Action Taken On the issues related to column No.18 For Full Pension And gratuity<span id="Span29" runat="server" style="color: Red; display: inline;"></span></label>
                            <div class="col-sm-7" style="" id="Div59">
                                <asp:DropDownList ID="ddlfullpension" runat="server" CssClass="form-control">
                                    <asp:ListItem>---Select---</asp:ListItem>
                                    <asp:ListItem>Calling NDC</asp:ListItem>
                                    <asp:ListItem>Calling LPC</asp:ListItem>
                                    <asp:ListItem>proposals forwarded to AG/AD for Full Pension Authorisation </asp:ListItem>
                                    <asp:ListItem>Date Of receipt of authorisation </asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group col-md-6" id="Div37" runat="server">
                            <label for="textfield" class="control-label col-sm-5" style="">
                                Remarks<span id="Span20" runat="server" style="color: Red; display: inline;"></span></label>
                            <div class="col-sm-7" style="" id="Div38">
                                <asp:TextBox ID="txtremarks" runat="server" placeholder="Remarks" MaxLength="50"
                                    class="form-control" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-12 text-center">
                            <asp:Button ID="btnSave" runat="server" Text="Submit" class="btn btn-primary" OnClick="btnSave_Click" />
                           <%-- <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-primary" />
                            <asp:Button ID="btnCancel" runat="server" Text="Reset" class="btn" />--%>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </section>

</asp:Content>
