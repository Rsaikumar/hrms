﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="Leave_Application_List.aspx.cs" Inherits="HRMS.Payroll.Leave_Application_List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <script type="text/javascript" lang="javascript">
    function Search() {
        if (document.getElementById('<%=txtSearch.ClientID%>').value == "") {
            alert("Search Box Can't be Empty ... ");
            document.getElementById('<%=txtSearch.ClientID%>').focus();
            return false;
        }
        return true;
    }
   
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    
      <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
 
<div id="main">
    <div class="container-fluid">
        <div class="page-header">
            <div class="pull-left Search_Div" id="btnSearch_Div">
                <div class="Search_Icon">
                    <asp:TextBox ID="txtSearch" runat="server" placeholder="Search For Define Leave Types List.."
                        class="form-control txtSearch"></asp:TextBox>
                </div>
            </div>
            <div class="pull-left">
               <asp:Button ID="btnSearch"   runat="server" class="search_Btn" OnClientClick="return Search()" OnClick="btnSearch_Click" />
            </div>
          
            <div class="pull-right">
                <ul class="stats">
                    <li>
                        <asp:Button ID="btnAdd" runat="server" class="add_Btn" OnClick="btnAdd_Click" ToolTip="Create Define Leave Types Details" /></li>
                    <li>
                        <asp:Button ID="btnPdf" runat="server" class="PDF_Btn" ToolTip="Export Define Leave Types Details To Pdf" /></li>
                    <li>
                        <asp:Button ID="btnExcel" runat="server" class="Excel_Btn" ToolTip="Export Define Leave Types Details To Excel" /></li>
                </ul>
            </div>
        </div>
        <div class="breadcrumbs">
            <ul>
                <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i>
                </li>
                <li>
                        <a href="">Payroll</a><i class="fa fa-angle-right"></i>
                    </li>
                    <li><a href="Leave_Application_List.aspx">Leave Application List</a> <i class="fa fa-angle-right"></i></li>
                 
                <li>
                <asp:LinkButton Text="Leave Application" id="lkbAddHeader" runat="server" OnClick="lkbAddHeader_Click"   ></asp:LinkButton>
                <%--<a href="AddUserRegistration.aspx">User Registration</a>--%> </li>
            </ul>
            <div class="close-bread">
                <a href="#"><i class="fa fa-times"></i></a>
            </div>+
        </div>
         <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                    <div class="box-title">
                        <h3>
                            <i class="fa fa-th-list"></i>Leave Application List</h3>
                    </div>
                    <div class="box-content">
                        <form action="#" class='form-horizontal form-bordered'>
                        <div class="col-sm-12" style="margin-bottom: 0.3%;">
                            <div class="row">
                                <asp:GridView ID="grdLeaveApplicationList" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                    Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%;" runat="server"
                                    AutoGenerateColumns="false" DataKeyNames="Leave_Code" EmptyDataText="No Records Found"
                                    ShowFooter="false" AllowPaging="true" 
                                     PageSize="15" AllowSorting="true" 
                                    onrowcommand="grdLeaveApplicationList_RowCommand" 
                                    onsorting="grdLeaveApplicationList_Sorting" 
                                    onpageindexchanging="grdLeaveApplicationList_PageIndexChanging" 
                                    onrowdeleting="grdLeaveApplicationList_RowDeleting" 
                                    onrowediting="grdLeaveApplicationList_RowEditing" >
                                    <AlternatingRowStyle BackColor="#EEEEEE" />
                                    <PagerStyle BackColor="#EEEEEE" ForeColor="Black" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#000065" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="S.No" SortExpression="">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Emp Code" >
                                            <ItemTemplate>
                                                <asp:Label ID="lblEmpCode" runat="server" Text='<%#Eval("Emp_Code") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Emp Name" >
                                            <ItemTemplate>
                                                <asp:Label ID="lblEmpName" runat="server" Text='<%#Eval("Emp_Name") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Department" >
                                            <ItemTemplate>
                                                <asp:Label ID="lblDepartment" runat="server" Text='<%#Eval("Department") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                       
                                            <asp:TemplateField HeaderText="Designation" >
                                            <ItemTemplate>
                                                <asp:Label ID="lblDesignation" runat="server" Text='<%#Eval("Designation") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Reporting Authority" >
                                            <ItemTemplate>
                                                <asp:Label ID="lblReportingAuthority" runat="server" Text='<%#Eval("Reporting_Authority") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TypeOf Leave" >
                                            <ItemTemplate>
                                                <asp:Label ID="lblTypeOfLeaveApple" runat="server" Text='<%#Eval("TypeOfLeaveApple") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Leave Status" >
                                            <ItemTemplate>
                                                <asp:Label ID="lblLeaveStatus" runat="server" Text='<%#Eval("Leave_Status") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbkEdit" runat="server" CommandName="Edit" Style="color: gray;
                                                    text-decoration: none; float: left; background-image: url('../img/file_edit.png');
                                                    height: 30px; width: 35px; background-position: left; background-repeat: no-repeat;"
                                                    OnClientClick="return confirm('Are you sure you want to Edit this Record ...?')" CausesValidation="false"
                                                    CommandArgument='<%#Eval("Leave_Code") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete" Style="color: gray;
                                                    text-decoration: none; float: left; background-image: url('../img/delete.png');
                                                    height: 30px; width: 35px; background-position: left; background-repeat: no-repeat;"
                                                    OnClientClick="return confirm('Are you sure you want to Remove this Record ...?')" CausesValidation="false"
                                                    CommandArgument='<%#Eval("Leave_Code") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                   
                                    </Columns>
                                </asp:GridView>
                            </div>
                    
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
     </ContentTemplate>
            </asp:UpdatePanel>

</asp:Content>
