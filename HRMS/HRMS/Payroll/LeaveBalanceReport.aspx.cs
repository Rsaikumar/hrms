﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Data;
using DAL;
using System.Data.SqlClient;
using System.Configuration;
namespace HRMS.Payroll
{
    public partial class Leave_Balance_Report : System.Web.UI.Page
    {
        #region declarations

        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Dbconnection"].ConnectionString);
        ERP_DatabaseDataContext objdal = new ERP_DatabaseDataContext();
        SqlConnection objSqlConnection;
        SqlCommand objSqlCommand;
        SqlDataAdapter da;

        #endregion

        #region events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["ClientRegistrationNo"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
            }
        }
       
        protected void lnkbtnpast_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Payroll/PastLeaveBalanceReport.aspx");
        }

        #endregion
    }
}