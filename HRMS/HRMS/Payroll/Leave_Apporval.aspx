﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="Leave_Apporval.aspx.cs" Inherits="HRMS.Payroll.Leave_Apporval" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

     <link href="../css/Payroll/Leave_Aplication.css" rel="stylesheet" />
    <style type="text/css">
        #ContentPlaceHolder1_actdate_container, #ContentPlaceHolder1_CalendarExtender2_popupDiv, #ContentPlaceHolder1_CalendarExtender3_popupDiv, #ContentPlaceHolder1_CalendarExtender1_container, #ContentPlaceHolder1_CalendarExtender2_container, #ContentPlaceHolder1_actdate_popupDiv, #ContentPlaceHolder1_CalendarExtender1_popupDiv {
            z-index: 10000;
        }

        .DateHide {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <div id="main">
        <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i></li>
                     <li>
                        <a href="">Payroll</a><i class="fa fa-angle-right"></i>
                    </li>
                    <li><a href="Leave_List_Approvel.aspx">Leave Application Details</a> <i class="fa fa-angle-right"></i></li>
                    <li><a href="Leave_Apporval.aspx">Leave Application</a> </li>
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-sm-12">

                            <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                                <div class="box-title">
                                    <h3>
                                        <i class="fa fa-th-list"></i>Leave Application</h3>
                                </div>
                                <div class="box-content">
                                    <form action="#" method="POST" class='form-horizontal form-bordered'>
                                        <div class="col-sm-12" style="">

                                            <div class="col-sm-12" style="">
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lAEmpCode" style="">
                                                       Emp Code
                                                    </label>
                                                    <div class="col-sm-2 tAEmpCode" style="">
                                                        <asp:TextBox ID="txtEmpCode" runat="server" placeholder="Emp Code" MaxLength="50"
                                                            class="form-control" ReadOnly="true" AutoPostBack="true" OnTextChanged="txtEmpCode_OnTextChanged" ></asp:TextBox>

                                                         <asp:TextBox ID="txtReportNo" runat="server" placeholder="Emp Code" MaxLength="50"
                                                            class="form-control" Visible="false" ReadOnly="true" AutoPostBack="true" OnTextChanged="txtEmpCode_OnTextChanged" ></asp:TextBox>

                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lALeaveName" style="">
                                                        Employee Name<span id="Span2" runat="server" style="color: Red; display: inline;">*</span></label>
                                                    <div class="col-sm-5 tAEmpName" style="" id="">
                                                        <asp:DropDownList ID="ddlEmpName" runat="server"  class="form-control DropdownCss"  
                                                            AutoPostBack="true" Visible="false" OnSelectedIndexChanged="ddlEmpName_SelectedIndexChanged"
                                                            
                                                            >                                                          
                                                          
                                                        </asp:DropDownList>
                                                         <asp:TextBox ID="txtEmpName" runat="server" placeholder="Emp Name" MaxLength="50"
                                                            class="form-control" ReadOnly="true"   ></asp:TextBox>
                                                        
                                                    </div>
                                                </div>
                                                </div>
                                            <div class="col-sm-12">
                                                  <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lADepartment" style="">
                                                        Department<span id="Span3" runat="server" style="color: Red; display: inline;">*</span></label>
                                                    <div class="col-sm-2 tADepartment" style="" id="">
                                                        <asp:DropDownList ID="ddlDepartment" runat="server" style="display:none;" class="form-control DropdownCss"  
                                                            >
                                                          
                                                          
                                                        </asp:DropDownList>
                                                    
                                                        
                                                         <asp:TextBox ID="txtDepartment" runat="server" placeholder="Department" class="form-control" onkeydown="return jsDecimals(event);"
                                                            MaxLength="50" ReadOnly="true"></asp:TextBox>
                                                    </div>
                                                </div>
                                                  <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lADesignation" style="">
                                                        Designation<span id="Span4" runat="server" style="color: Red; display: inline;">*</span></label>
                                                    <div class="col-sm-2 tADesignation" style="" id="">
                                                        <asp:DropDownList ID="ddlDesignation" runat="server"  style="display:none;" class="form-control DropdownCss"  
                                                            >
                                                          
                                                          
                                                        </asp:DropDownList>
                                                        
                                                         <asp:TextBox ID="txtDesignation" runat="server" placeholder="Designation" class="form-control" onkeydown="return jsDecimals(event);"
                                                            MaxLength="50" ReadOnly="true"></asp:TextBox>
                                                    </div>
                                                </div>
                                                  <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lAReportingAuthority" style="">
                                                        Reporting Authority<span id="Span5" runat="server" style="color: Red; display: inline;">*</span></label>
                                                    <div class="col-sm-2 tAReportingAuthority" style="" id="">
                                                        <asp:DropDownList ID="ddlReportingAuthority" runat="server" style="display:none;"  class="form-control DropdownCss"  
                                                            >
                                                          
                                                          
                                                        </asp:DropDownList>

                                                         <asp:TextBox ID="txtReportingAuthority" runat="server" placeholder="Reporting Authority" class="form-control" onkeydown="return jsDecimals(event);"
                                                            MaxLength="50" ReadOnly="true"></asp:TextBox>
                                                        
                                                    </div>
                                                </div>
                                              
                                            </div>

                                            <div class="col-sm-12" style="">

                                                     <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lALeaveApply" style="">
                                                        Type Of Leave Apply<span style="color: Red; display: inline;">*</span></label>
                                                    <div class="col-sm-2 tALeaveApply" style="" id="">
                                                        <asp:DropDownList ID="ddlLeaveApply" runat="server"  class="form-control DropdownCss" AutoPostBack="True" OnSelectedIndexChanged="ddlLeaveApply_SelectedIndexChanged" ReadOnly="true">
                                                          
                                                          
                                                        </asp:DropDownList>
                                                        
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                                           
                                                                           
                                                   <label for="textfield" class="control-label col-sm-1 lABalancesAvailable">
                                                                               Balances Available</label>
                                                                       
                                                 <div class="col-sm-2 tBalancesAvailable" style="" id="">
                                                                               <asp:TextBox ID="txtBalancesAvailable" runat="server" placeholder="Balances Available" class="form-control" ReadOnly="true"
                                                            MaxLength="20" AutoPostBack="True" OnTextChanged="txtBalancesAvailable_TextChanged"></asp:TextBox>
                                                                            </div>
                                                </div>
                                                </div>
                                            <div class="col-sm-12">

                                            
                                             <div class="form-group">
                                                                           
                                                                            
                                                   <label for="textfield" class="control-label col-sm-1 lALApply" style="">
                                                                                Leave Apply</label>
                                                                        </div>
                                                  <div class="col-sm-2 tLApplyFrom" style="" id="">
                                                                               <asp:TextBox ID="txtFromDate" runat="server" placeholder="Ex:dd-mm-yyyy" class="form-control" ReadOnly="true"
                                                                                            MaxLength="20" AutoPostBack="true" OnTextChanged="txtFromDate_OnTextChanged"></asp:TextBox>
                                                       <cc1:CalendarExtender ID="actdate" PopupButtonID="imgPopup" runat="server" TargetControlID="txtFromDate"
                                                        Format="dd-MM-yyyy" PopupPosition="BottomLeft" >
                                                    </cc1:CalendarExtender>
                                                                            </div>
                                                  <div class="col-sm-2 tLApplyTo" style="" id="">
                                                                               <asp:TextBox ID="txtToDate" runat="server" placeholder="Ex:dd-mm-yyyy" class="form-control" 
                                                           ReadOnly="true" MaxLength="20" AutoPostBack="true" OnTextChanged="txtToDate_OnTextChanged"></asp:TextBox>
                                                       <cc1:CalendarExtender ID="CalendarExtender1" PopupButtonID="imgPopup" runat="server" TargetControlID="txtToDate"
                                                        Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                    </cc1:CalendarExtender>
                                                                            </div>
                                                
                                                 <div class="form-group">
                                                                           
                                                                        
                                                   <label for="textfield" class="control-label col-sm-1 lANoOfDays" style="">
                                                                              No Of Days</label>
                                                                       
                                                 <div class="col-sm-2 tNoofDays" style="" id="">
                                                                               <asp:TextBox ID="txtNoofDays" runat="server" placeholder="No of Days" class="form-control" 
                                                        ReadOnly="true"    MaxLength="20" AutoPostBack="True" OnTextChanged="txtNoofDays_TextChanged"></asp:TextBox>
                                                                            </div>
                                                </div>
                                                </div>
                                            
                                              <div class="col-sm-12" style="">
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lAStatus" style="">
                                                        Status<span style="color: #ff0000; font-size: 14px;display:inline;">*</span>
                                                    </label>
                                                    <div class="col-sm-2 tAStatus" style="">
                                                    
                                                        <asp:DropDownList ID="ddlStatus" runat="server"  class="form-control DropdownCss"  
                                                            >
                                                            <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                            <asp:ListItem Text="Wating For Approval" Value="Wating For Approval"></asp:ListItem>
                                                            <asp:ListItem Text="Recommended" Value="Recommended"></asp:ListItem>
                                                            <asp:ListItem Text="Authorize" Value="Authorize"></asp:ListItem>
                                                           <asp:ListItem Text="Approval" Value="Approval"></asp:ListItem>
                                                             <asp:ListItem Text="Reject" Value="Reject"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                  
                                               
                                             
                                            </div>
                                        </div>  
                                        <div class="col-sm-5 ACButtons">
                                            <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary" OnClick="btnSave_Click"   />
                                            <asp:Button ID="btnUpdate" runat="server" Text="Save" class="btn btn-primary" OnClick="btnUpdate_Click" />
                                            <asp:Button ID="btnCancel" runat="server" Text="Reset" class="btn" OnClick="btnCancel_Click"   />
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
