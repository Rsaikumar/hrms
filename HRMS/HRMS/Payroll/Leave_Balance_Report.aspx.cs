﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BAL;
using DAL;
using System.Data.Linq.SqlClient;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using System.IO;
namespace HRMS.Payroll
{
    public partial class Leave_Balance_Report : System.Web.UI.Page
    {
         ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();
        DefineLeavesBAL objSCM_LeaveBAL = new DefineLeavesBAL();
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Dbconnection"].ConnectionString);
        ERP_DatabaseDataContext objdal = new ERP_DatabaseDataContext();
        SqlConnection objSqlConnection;
        SqlCommand objSqlCommand;
        SqlDataAdapter da;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               
                if (Session["ClientRegistrationNo"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
               

                //txtSearch.Focus();

                //Departments Binding
                BindingDepartments();

                //Binding EmpCodes
                BindingEmpCodes();


               
                // DateCount();
                setinitialrow();
                Bind_Grid();
                

            }
        }

      
        #region Binding dropdownlists
        public void BindingDepartments()
        {
            string creationcompany = Session["ClientRegistrationNo"].ToString();
            List<HRMS_Department_Master> depts = (from dept in ObjDAL.HRMS_Department_Masters
                                                  where  dept.Creation_Company== creationcompany
                                                  select dept).ToList();
            var distinctDepts = depts.GroupBy(x => x.Department_Code)
                         .Select(g => g.First()).OrderByDescending(x => x.Department_Code)
                         .ToList();

            ddlDepartment.DataSource = distinctDepts;
            ddlDepartment.DataTextField = "Department_Name";
            ddlDepartment.DataValueField = "Department_Name";
            ddlDepartment.DataBind();
            ddlDepartment.Items.Insert(0, new ListItem("Select", "Select"));
        }

        public void BindingEmpCodes()
        {
            try
            {
                string creationcompany = Session["ClientRegistrationNo"].ToString();
                var Emps = (from Emp in ObjDAL.USP_gettingemployeenameandid()
                            select Emp).ToList();
                var distinctEmps = Emps.GroupBy(x => x.empid)
                             .Select(g => g.First()).OrderByDescending(x => x.empid)
                             .ToList();

                ddlEmpCode.DataSource = distinctEmps;
                ddlEmpCode.DataTextField = "empname";
                ddlEmpCode.DataValueField = "empid";
                ddlEmpCode.DataBind();
                ddlEmpCode.Items.Insert(0, new ListItem("Select", "Select"));
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }
        }
        #endregion


        #region Button Events related Methods
        
        protected void btnShow_Click(object sender, EventArgs e)
        {
            bindgrid();
           
        }
        int parameter;
      
        

        // // to bind the corresponding data to the front end grid
        private void bindgrid()
        {
            try
            {

       


                //#region Display Code
                if ( ddlYear.SelectedValue != "Select" )
                {
                    parameter = 1;
                }
                if (ddlYear.SelectedValue != "Select" && ddlEmpCode.SelectedValue != "Select")
                {
                    parameter = 2;
                }

                if (ddlYear.SelectedValue != "Select" && ddlDepartment.SelectedValue != "Select")
                {

                    parameter = 3;
                }
                


                string objConfig = ConfigurationManager.ConnectionStrings["Dbconnection"].ToString();
                
                objSqlConnection = new SqlConnection(objConfig);
                objSqlCommand = new SqlCommand();
                objSqlCommand.Connection = objSqlConnection;

                objSqlConnection.Open();
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.CommandText = "Sp_HRMS_Emppoyee_Leave_Details";

                objSqlCommand.Parameters.AddWithValue("@Year", ddlYear.SelectedValue);
                objSqlCommand.Parameters.AddWithValue("@Emp_Code", ddlEmpCode.SelectedValue);
                objSqlCommand.Parameters.AddWithValue("@Department", ddlDepartment.SelectedValue);
                objSqlCommand.Parameters.AddWithValue("@Creation_Company", (string)Session["ClientRegistrationNo"]);
                objSqlCommand.Parameters.AddWithValue("@Parameter", parameter);

                da = new SqlDataAdapter(objSqlCommand);
                DataTable ds = new DataTable();
                da.Fill(ds);
                var distinctrows = ds.AsEnumerable()
                        .Select(s => new
                        {
                            Emp_Code = s.Field<string>("Emp_Code"),
                            Emp_Name = s.Field<string>("Emp_Name"),
                            Department = s.Field<string>("Department"),
                            
                        }).Distinct().ToList();

                grd_LEave_Balance.Visible = true;
                Panel1.Visible = true;
                btnExcel.Visible = true;

                if (ds.Rows.Count > 0)
                {
                    grd_LEave_Balance.DataSource = distinctrows;
                    grd_LEave_Balance.DataBind();
                    ViewState["CurrentTable"] = ds;
                    Session["DS"] = ds;
                    Iscroll.Attributes.Add("style", "overflow-x:scroll;width:99%;");
                }
                else
                {
                    #region header with no record
                    ds.Rows.Add(ds.NewRow());
                    grd_LEave_Balance.DataSource = ds;
                    grd_LEave_Balance.DataBind();
                    int columncount = grd_LEave_Balance.Rows[0].Cells.Count;
                    grd_LEave_Balance.Rows[0].Cells.Clear();
                    grd_LEave_Balance.Rows[0].Cells.Add(new TableCell());
                    grd_LEave_Balance.Rows[0].Cells[0].ColumnSpan = columncount;
                    grd_LEave_Balance.Rows[0].Cells[0].Text = "No Records Found";
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Session["Error"] = ex.ToString();
                Server.Transfer("~/Modules/Error_Page.aspx");
            }
            finally
            {

            }
        }

        protected void grd_LEave_Balance_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grd_LEave_Balance.PageIndex = e.NewPageIndex;
            bindgrid();
            Panel1.Visible = true;

        }

        //Child Grid bind
        public void grd2Bind(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowIndex != -1)
                {
                    string EmpNo = grd_LEave_Balance.DataKeys[e.Row.RowIndex].Value.ToString();
                    GridView grdstc = (GridView)e.Row.FindControl("grd2");
                    string objConfig = ConfigurationManager.ConnectionStrings["Dbconnection"].ToString();

                    objSqlConnection = new SqlConnection(objConfig);
                    objSqlCommand = new SqlCommand();
                    objSqlCommand.Connection = objSqlConnection;

                    objSqlConnection.Open();
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.CommandText = "Sp_HRMS_Emppoyee_Leave_Details";

                    objSqlCommand.Parameters.AddWithValue("@Year", ddlYear.SelectedValue);
                    objSqlCommand.Parameters.AddWithValue("@Emp_Code", EmpNo);
                    objSqlCommand.Parameters.AddWithValue("@Department", "");
                    objSqlCommand.Parameters.AddWithValue("@Creation_Company", (string)Session["ClientRegistrationNo"]);
                    objSqlCommand.Parameters.AddWithValue("@Parameter", 2);

                    da = new SqlDataAdapter(objSqlCommand);
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                    //DataTable dt = (DataTable)ViewState["CurrentTable"];
                    grdstc.DataSource = dt;
                    grdstc.DataBind();


                    

                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
        }

        #endregion

        #region Excel Report
        //public override void VerifyRenderingInServerForm(Control control)
        //{
        //    /* Verifies that the control is rendered */
        //}


        /// <summary>
        ///  to export the grid data to excel 
        /// </summary>
        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                if (grd_LEave_Balance.Rows.Count > 0)
                {
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=" + "Leave_Report" + DateTime.Now.ToString("dd_MM_yyyy_HHmmss") + ".xls");
                    Response.ContentType = "application/ms-excel";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    grd_LEave_Balance.AllowPaging = false;
                    DataTable ds = (DataTable)ViewState["CurrentTable"];
                    var distinctrows = ds.AsEnumerable()
                            .Select(s => new
                            {
                                Emp_Code = s.Field<string>("Emp_Code"),
                                Emp_Name = s.Field<string>("Emp_Name"),
                                Department = s.Field<string>("Department"),
                            }).Distinct().ToList();
                    grd_LEave_Balance.DataSource = distinctrows;
                    grd_LEave_Balance.DataBind();
                    grd_LEave_Balance.RenderControl(htw);
                    Response.Write(sw.ToString());
                    Response.End();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Data...');", true);
                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }
        }

        #endregion

        #region leave details grid bind
        public void Bind_Grid()
        {
            try
            {
                string Cmpy = (string)Session["ClientRegistrationNo"];
                string Brnach = (string)Session["BranchID"];
                string UserType = (string)Session["User_Type"];
                string UserName = (string)Session["User_Name"];
                string Cration_Company = (string)Session["CreationCompany"]; //"000001";
                //if (UserType == "User")
                //{
                if (Session["User_Type"].ToString() == "CCF")
                {

                    string EMP_Code = (from EM in objDAL.HRMS_Employee_Master_PersonalDetails where EM.User_Name == UserName select EM.Emp_Code).FirstOrDefault();
                    var objHeaderInfo = (from header in objDAL.HRMS_Leave_Applications
                                         where header.Creation_Company == Cmpy && header.Emp_Code == EMP_Code
                                         orderby header.Leave_Code descending
                                         select header).ToList();
                    //// List<Administrator_UserRegistration> ObjuserInfo = objSCM_userRegistration.Get_userRegDetails(Session["ClientRegistrationNo"].ToString());
                    if (objHeaderInfo.Count > 0)
                    {

                        grdleaves.DataSource = objHeaderInfo;
                        grdleaves.DataBind();
                    }
                    else
                    {
                        // // // Grid view using setting initial rows displaying Empty Header Displying
                        setinitialrow();
                    }
                }
                else if (Session["User_Type"].ToString() == "APCCF")
                {
                    var objHeaderInfo = (from LP in objDAL.HRMS_Leave_Applications
                                         where LP.Creation_Company == Cmpy && LP.Leave_Status == "Wating For Approval"
                                         orderby LP.Leave_Code descending
                                         select LP).ToList();
                    if (objHeaderInfo.Count > 0)
                    {

                        grdleaves.DataSource = objHeaderInfo;
                        grdleaves.DataBind();
                    }
                    else
                    {
                        setinitialrow();
                    }

                }
                else if (Session["User_Type"].ToString() == "POA")
                {
                    var objHeaderInfo = (from LP in objDAL.HRMS_Leave_Applications
                                         where LP.Creation_Company == Cmpy && LP.Leave_Status == "Recommended" || LP.Leave_Status == "Wating For Approval" || LP.Leave_Status == "Approval" || LP.Leave_Status == "Reject" || LP.Leave_Status == "Authorize"
                                         orderby LP.Leave_Code descending
                                         select LP).ToList();
                    if (objHeaderInfo.Count > 0)
                    {

                        grdleaves.DataSource = objHeaderInfo;
                        grdleaves.DataBind();
                    }
                    else
                    {
                        setinitialrow();
                    }

                }
                else if (Session["User_Type"].ToString() == "HOFF")
                {
                    var objHeaderInfo = (from LP in objDAL.HRMS_Leave_Applications
                                         where LP.Creation_Company == Cmpy && LP.Leave_Status == "Recommended"
                                         orderby LP.Leave_Code descending
                                         select LP).ToList();
                    if (objHeaderInfo.Count > 0)
                    {

                        grdleaves.DataSource = objHeaderInfo;
                        grdleaves.DataBind();
                    }
                    else
                    {
                        setinitialrow();
                    }

                }

                else
                {
                    var objHeaderInfo = (from LP in objDAL.HRMS_Leave_Applications
                                         where LP.Creation_Company == Cmpy
                                         orderby LP.Leave_Code descending
                                         select LP).ToList();
                    //// List<Administrator_UserRegistration> ObjuserInfo = objSCM_userRegistration.Get_userRegDetails(Session["ClientRegistrationNo"].ToString());
                    if (objHeaderInfo.Count > 0)
                    {
                        grdleaves.DataSource = objHeaderInfo;
                        grdleaves.DataBind();
                    }
                    else
                    {
                        // // // Grid view using setting initial rows displaying Empty Header Displying
                        setinitialrow();
                    }
                }


            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }

        }

        public void setinitialrow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("Sno", typeof(string)));
            dt.Columns.Add(new DataColumn("Leave_Code", typeof(string)));
            dt.Columns.Add(new DataColumn("Emp_Code", typeof(string)));
            dt.Columns.Add(new DataColumn("Emp_Name", typeof(string)));
            dt.Columns.Add(new DataColumn("TypeOfLeaveApple", typeof(string)));
            dt.Columns.Add(new DataColumn("Noof_Days", typeof(string)));
            dt.Columns.Add(new DataColumn("Leave_Status", typeof(string)));

            dr = dt.NewRow();
            dr["Sno"] = string.Empty;
            dr["Leave_Code"] = string.Empty;
            dr["Emp_Code"] = string.Empty;
            dr["Emp_Name"] = string.Empty;
            dr["TypeOfLeaveApple"] = string.Empty;
            // dr["Prod_Spec"] = string.Empty;
            dr["Noof_Days"] = string.Empty;

            dt.Rows.Add(dr);
            ViewState["CurrentTable"] = dt;
            grdleaves.DataSource = dt;
            grdleaves.DataBind();

        }

        public void getleave()
        {
            var objHeaderInfo = (from header in objdal.HRMS_Leave_Applications
                                 join leave_master in objdal.HRMS_Leave_Masters on header.Leave_Code equals leave_master.Leave_Code
                                 where leave_master.Creation_Company == (string)Session["ClientRegistrationNo"]
                                 where header.Creation_Company == (string)Session["ClientRegistrationNo"]
                                 && header.Leave_Status == "Wating For Approval" //|| header.Leave_Status == "Wating For Approval"
                                 //&& leave_master.UserName1 == (string)Session["User_Name"]
                                 orderby header.Leave_Code descending
                                 select header).ToList();
            //// List<Administrator_UserRegistration> ObjuserInfo = objSCM_userRegistration.Get_userRegDetails(Session["ClientRegistrationNo"].ToString());
            if (objHeaderInfo.Count > 0)
            {
                grdleaves.DataSource = objHeaderInfo;
                grdleaves.DataBind();
            }
            else
            {
                setinitialrow();
            }
        }


        public void getleave2()
        {
            // // // Grid view using setting initial rows displaying Empty Header Displying
            var LEave2 = (from header in objdal.HRMS_Leave_Applications

                          join leave_master in objdal.HRMS_Leave_Masters on header.Leave_Code equals leave_master.Leave_Code
                          where leave_master.Creation_Company == (string)Session["ClientRegistrationNo"]
                          where header.Creation_Company == (string)Session["ClientRegistrationNo"]
                          && header.Leave_Status == "Recommended"
                        //  && leave_master.UserName2 == (string)Session["User_Name"]
                          orderby header.Leave_Code descending
                          select header).ToList();
            if (LEave2.Count > 0)
            {
                grdleaves.DataSource = LEave2;
                grdleaves.DataBind();
            }
            else
            {
                setinitialrow();
            }
        }

        //protected void ddlSelect_Month_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (ddlSelect_Month.SelectedItem.Text == "For Approval")
        //    {
        //        getleave();
        //    }
        //    else if (ddlSelect_Month.SelectedItem.Text == "Recommend")
        //    {
        //        getleave2();
        //    }
        //}

        protected void lbRemove_OnClick(object sender, EventArgs e)
        {
            LinkButton txtval = (LinkButton)sender;
            GridViewRow gvr = (GridViewRow)txtval.NamingContainer;
            int index = gvr.RowIndex;
            Label txtLeave_Code = (Label)grdleaves.Rows[index].FindControl("txtLeave_Code");
            string lblSNO = grdleaves.Rows[index].Cells[0].Text;
            string path = "~/Payroll/Leave_Apporval.aspx?Id=" + lblSNO + "";
            Session["AdditionCode"] = txtLeave_Code.Text;
            Response.Redirect(path);
        }

        //protected void lbview_Click(object sender, EventArgs e)
        //{
        //    LinkButton txtva = (LinkButton)sender;
        //    GridViewRow gv = (GridViewRow)txtva.NamingContainer;
        //    int index = gv.RowIndex;
        //    TextBox txtloancode = (TextBox)grdloan.Rows[index].FindControl("txtloancode");
        //    string lblSNO = grdloan.Rows[index].Cells[0].Text;
        //    string path = "~/Payroll/Loan_Aproval.aspx?Id=" + lblSNO + "";
        //    Session["AdditionCode"] = txtloancode.Text;
        //    Response.Redirect(path);
        //}
        //public void Bind_GridLoan()
        //{

        //    string Cmpy = (string)Session["ClientRegistrationNo"];
        //    string Brnach = (string)Session["BranchID"];
        //    string UserType = (string)Session["User_Type"];
        //    string UserName = (string)Session["User_Name"];
        //    string Cration_Company = (string)Session["CreationCompany"]; //"000001";
        //    //if (UserType == "User")
        //    //{
        //    if (Session["User_Type"].ToString() == "CCF")
        //    {

        //        string EMP_Code = (from EM in objDAL.HRMS_Employee_Master_PersonalDetails where EM.User_Name == UserName select EM.Emp_Code).FirstOrDefault();
        //        var objHeaderInfo = (from header in objDAL.HRMS_Loan_Applications
        //                             where header.Creation_Company == Cmpy && header.Emp_Code == EMP_Code
        //                             orderby header.Emp_Code descending
        //                             select header).ToList();
        //        //// List<Administrator_UserRegistration> ObjuserInfo = objSCM_userRegistration.Get_userRegDetails(Session["ClientRegistrationNo"].ToString());
        //        if (objHeaderInfo.Count > 0)
        //        {

        //            grdloan.DataSource = objHeaderInfo;
        //            grdloan.DataBind();
        //        }
        //    }
        //    else if (Session["User_Type"].ToString() == "POA")
        //    {
        //        var objHeaderInfo = (from header in objDAL.HRMS_Loan_Applications
        //                             where header.Creation_Company == Cmpy && header.Status == "Wating For Approval"

        //                             select header).ToList();
        //        //// List<Administrator_UserRegistration> ObjuserInfo = objSCM_userRegistration.Get_userRegDetails(Session["ClientRegistrationNo"].ToString());
        //        if (objHeaderInfo.Count > 0)
        //        {

        //            grdloan.DataSource = objHeaderInfo;
        //            grdloan.DataBind();
        //        }

        //    }
        //    else
        //    {
        //        // // // Grid view using setting initial rows displaying Empty Header Displying
        //        setinitialrow();
        //    }
        //}
        #endregion
    }
}