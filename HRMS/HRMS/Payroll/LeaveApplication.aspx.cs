﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BAL;
using DAL;
using System.Data.Linq.SqlClient;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Collections;

namespace HRMS.Payroll
{
    public partial class LeaveApplication : System.Web.UI.Page
    {
        #region Declarations

        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();
        LeaveApplicationBAL objLeaveApp = new LeaveApplicationBAL();

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                employeenamebind();
                GetLeaveDetails();
                lblempcode.Visible = false;
                txtEmpCode.Visible = false;
               // rabind();
               // bindemp();
                approveleavedetails();
                divstatus.Visible = false;
                divoofleaves.Visible = false;
              
                if (Session["ClientRegistrationNo"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                txtstatus.Text = "Wating For Approval";
                txtEmpCode.Focus();
               // rabind();
                leaveAva();
                //BalanceAva();
                Bind_Leave();

                dtholidays = GetPublicHolidays();
                if ((string)Session["Leave_code"] != null)
                {
                    string ClientRegistrationNo = Session["ClientRegistrationNo"].ToString();
                }
                else
                {
                }
            }
        }



        public void employeenamebind()
        { 
            try
            {

                List<string> master = new List<string>();
                List<string> mastername = new List<string>();
                int empcode =Convert.ToInt32( (Session["Emp_Code"].ToString()));

               master = (from item in ObjDAL.HRMSEmployeeMasterRegistrationDetails
                              join per in ObjDAL.HRMSEmplyeeMasterPersonalDetails on item.ID equals Convert.ToInt32(per.EmpId)
                              where item.CreatedBy== empcode.ToString()
                              select per.FirstName + " " + per.MiddleName + " " + per.LastName).ToList();
               mastername = (from item in ObjDAL.HRMSEmployeeMasterRegistrationDetails
                          join per in ObjDAL.HRMSEmplyeeMasterPersonalDetails on item.ID equals Convert.ToInt32(per.EmpId)
                          where item.ID == empcode
                          select per.FirstName + " " + per.MiddleName + " " + per.LastName).ToList();

                master.AddRange(mastername);

                if (master!= null)
                {
                    ddlempname.DataSource = master;
                    ddlempname.DataBind();
                    ddlempname.Items.Insert(0, new ListItem("--Select--", "Select"));
                }
                else
                {
                    ddlempname.Items.Insert(0, new ListItem("--Select--", "Select"));

                }
            }
            catch (Exception e)
            {

            }
        }



        /// <summary>
        /// Emp Code On TextChanged Event For Checking Emp Code 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtEmpCode_OnTextChanged(object sender, EventArgs e)
        {
            string CreationCompany = (string)Session["ClientRegistrationNo"].ToString();
            string Emp_Code = (txtEmpCode.Text.ToLower().Trim() == "") ? "" : txtEmpCode.Text.ToLower().Trim();
            var objEmPCodeDUP = (from itmes in ObjDAL.HRMS_Employee_Master_PersonalDetails
                                 where itmes.Creation_Company == CreationCompany && itmes.Emp_Code.ToLower().Trim() == Emp_Code
                                 select new
                                 {
                                     Emp_name = itmes.Emp_Last_Name + " " + itmes.Emp_First_Name + " " + itmes.Emp_Middle_Name,
                                     Emp_Code = itmes.Emp_Code
                                 }).ToList();
            if (objEmPCodeDUP.Count > 0)
            {
                var s = (from x in ObjDAL.HRMS_Employee_Master_Official_Informations
                         where x.Emp_Code == txtEmpCode.Text && x.Creation_Company == CreationCompany
                         select x).ToList();
                if (s.Count > 0)
                {
                    txtDepartment.Text = s[0].Department;
                    txtDesignation.Text = s[0].Designation;
                    ddlLeaveApply.Focus();
                }
                else
                {
                    txtDepartment.Text = "";
                    txtDesignation.Text = "";
                }
            }
            else
            {
                txtEmpCode.Text = "";
                txtEmpCode.Focus();
                txtDepartment.Text = "";
                txtDesignation.Text = "";
            }
        }

        /// <summary>
        /// Text Change Event For From Date Caluculation  Days
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtFromDate_OnTextChanged(object sender, EventArgs e)
        {
            DateTime fromdate = DateTime.ParseExact(txtFromDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            // DateTime fromdate = Convert.ToDateTime(txtFromDate.Text);

            if (fromdate <= DateTime.Now)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Select Valid Date..');", true);
                txtFromDate.Text = "";
                txtFromDate.Focus();

            }

        }

        protected void txtToDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                noofdays();
                decimal balance = Convert.ToDecimal(txtToDate.Text);
                decimal days = Convert.ToDecimal(txtNoofDays.Text);
                if (balance < days)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Applied Leaves Are Greater Than Available Leaves..');", true);
                    txtNoofDays.Text = "";
                    txtToDate.Text = "";
                    txtToDate.Focus();
                }

            }
            catch (Exception ex)
            {

            }

        }

        public void GetLeaveDetails()
        {
            int empcode = Convert.ToInt32(Session["Emp_Code"]);
            int currentyear = Convert.ToInt32(DateTime.Now.Year.ToString());
            var leave = (from item in objDAL.HRMSNewLeaveMasters
                         select item).ToList();
            if (leave.Count > 0)
            {
                GdvBalanceLeaves.DataSource = leave;
                GdvBalanceLeaves.DataBind();
            }
        }

        protected void lnkbtnpast_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Payroll/PastLeaveBalanceReport.aspx");
        }

        /// <summary>
        /// Text Change Event for To date Calculation for Days
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        List<DateTime> dtholidays = null;
        protected void actdate_DayRender(object sender, DayRenderEventArgs e)
        {
            if (dtholidays.Contains(e.Day.Date))
            {
                e.Day.IsSelectable = false;
            }
        }

        private List<DateTime> GetPublicHolidays()
        {
            List<DateTime> list = new List<DateTime>();
            //populate from database or other sources
            list.Add(new DateTime(2016, 01, 14));
            list.Add(new DateTime(2016, 01, 15));
            list.Add(new DateTime(2016, 01, 26));
            list.Add(new DateTime(2016, 03, 07));
            list.Add(new DateTime(2016, 03, 23));
            list.Add(new DateTime(2016, 03, 25));
            list.Add(new DateTime(2016, 04, 05));
            list.Add(new DateTime(2016, 04, 08));
            list.Add(new DateTime(2016, 04, 14));
            list.Add(new DateTime(2016, 04, 15));
            list.Add(new DateTime(2016, 07, 06));
            list.Add(new DateTime(2016, 07, 07));
            list.Add(new DateTime(2016, 08, 01));
            list.Add(new DateTime(2016, 08, 15));
            list.Add(new DateTime(2016, 08, 25));
            list.Add(new DateTime(2016, 09, 05));
            list.Add(new DateTime(2016, 09, 12));
            list.Add(new DateTime(2016, 09, 30));
            list.Add(new DateTime(2016, 10, 11));
            list.Add(new DateTime(2016, 10, 12));
            list.Add(new DateTime(2016, 11, 14));
            list.Add(new DateTime(2016, 12, 12));
            list.Add(new DateTime(2016, 12, 26));
            return list;
        }

        /// <summary>
        /// EmpCode TextChange Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>      

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (uploadCertification.Visible == true)
            {
                if (ViewState["File"] != null)
                {
                    Saving();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('file uploades success and Leave Applied..');", true);
                    fupCertification.Focus();
                }

            }

            if (ddlLeaveApply.SelectedItem.Text == "CL")
            {
                var leavedetails = leavedata(ddlLeaveApply.SelectedItem.Text);
                if (Convert.ToInt32(txtNoofDays.Text) <= Convert.ToInt32(txtnoofleaves.Text))
                {

                    Saving();
                }
                else
                {
                    txtNoofDays.Text = "";
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Casual leave apllicabale for 12 Days In a Year');", true);
                }
            }



            else if (ddlLeaveApply.SelectedItem.Text == "EL")
            {
                var leavedetails = leavedata(ddlLeaveApply.SelectedItem.Text);
                if (Convert.ToInt32(txtNoofDays.Text) <= Convert.ToInt32(txtnoofleaves.Text))
                {

                    Saving();
                }
                else
                {
                    txtNoofDays.Text = "";
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Eared leave apllicabale for 4 Days In a Year');", true);
                }
            }


        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Update_Method();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearFields();

        }

        //dropdowntypeleave
        protected void ddlLeaveApply_SelectedIndexChanged(object sender, EventArgs e)
        {
            var leave = (from leav in objDAL.HRMSNewLeaveMasters
                         select leav).ToList();

            if (ddlLeaveApply.SelectedItem.Value == "2")
            {
                txtnoofleaves.Text = Convert.ToString(leave[0].NoOfLeaves);


            }
            if (ddlLeaveApply.SelectedItem.Value == "3")
            {
                txtnoofleaves.Text = Convert.ToString(leave[1].NoOfLeaves);
            }
            else
            {

                try
                {

                    int Emp_Code = (from em in ObjDAL.HRMSEmployeeMasterRegistrationDetails where em.UserName == Session["User_Name"].ToString() select em.ID).FirstOrDefault();
                    var Emp_List = (from liste in ObjDAL.USP_getbalanceleavedetails(Emp_Code, Convert.ToString(ddlLeaveApply.SelectedItem.Text)) select liste).ToList();
                    if (Emp_List.Count > 0)
                    {
                        //txtBalancesAvailable.Text = Convert.ToString(Emp_List[0].leavebalance);
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }

        protected void txtNoofDays_TextChanged(object sender, EventArgs e)
        {
            //BalanceAva();
        }

        protected void txtBalancesAvailable_TextChanged(object sender, EventArgs e)
        {
            //BalanceAva();
        }

        protected void gdvleaveapproved_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(gdvleaveapproved, "Select$" + e.Row.RowIndex);
                    e.Row.ToolTip = "View Leave Details";
                    e.Row.Attributes["style"] = "cursor:pointer";
                }
            }
        }

        protected void gdvleaveapproved_SelectedIndexChanged(object sender, EventArgs e)
        {
            string EmpCode = gdvleaveapproved.SelectedRow.Cells[0].Text;
            string path = "~/Payroll/LeaveApproval.aspx";
            Session["LeaveapplicationNo"] = EmpCode;
            Response.Redirect(path);
        }

        #endregion Buttons

        #region Methods
        public void noofdays()
        {
            DateTime start = DateTime.ParseExact(txtFromDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            DateTime finish = DateTime.ParseExact(txtToDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            if (start <= finish)
            {
                TimeSpan difference = (finish.Subtract(start));
                var NumberOfDays = difference.TotalDays.ToString();
                var nod = Convert.ToInt32(NumberOfDays) + 1;
                txtNoofDays.Text = nod.ToString();
            }
            else
            {
                txtNoofDays.Text = "";
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Plz select from date greaterthan  todate');", true);
            }
        }
        private HRMSNewLeaveMaster leavedata(string LeaveType)
        {
            var leavedetails = (from lm in ObjDAL.HRMSNewLeaveMasters
                                where lm.ShortName == LeaveType
                                select lm).FirstOrDefault();
            return leavedetails;
        }

        public void Bind_Leave()
        {
            try
            {
                string Cmpy = (string)Session["ClientRegistrationNo"];
                string Brnach = (string)Session["BranchID"];
                string UserType = (string)Session["User_Type"];
                string UserName = (string)Session["User_Name"];
                string Cration_Company = (string)Session["CreationCompany"]; //"000001";
                var objHeaderInfo = (from header in ObjDAL.HRMSNewLeaveMasters
                                     //where header.CreationCompany == Cmpy
                                     //orderby header.ID descending
                                     select header).ToList();
                if (objHeaderInfo.Count > 0)
                {
                    ddlLeaveApply.DataTextField = "ShortName";
                    ddlLeaveApply.DataValueField = "ID";
                    ddlLeaveApply.DataSource = objHeaderInfo;
                    ddlLeaveApply.DataBind();
                    ddlLeaveApply.Items.Insert(0, new ListItem("--Select--", "Select"));
                }
                else
                {
                    ddlLeaveApply.Items.Insert(0, new ListItem("--Select--", "Select"));
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }
        }
        public void rabind()
        {
            try
            {
                var reporting = (from item in ObjDAL.USP_gettingemployeenameandid(Convert.ToInt32(Session["ReportingAuthority"]))
                                 select item).ToList();

                txtreportingauthority.Text = reporting[0].empname;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //clearfields
        public void ClearFields()
        {
            txtEmpCode.Text = "";
            txtDepartment.Text = "";
            txtDesignation.Text = "";
            txtFromDate.Text = "";
            txtToDate.Text = "";
            txtstatus.Text = "";
            ddlLeaveApply.SelectedValue = "Select";
        }
        private void Saving()
        {
            try
            {
                string d1 = txtFromDate.Text.Substring(0, 2);
                string m1 = txtFromDate.Text.Substring(3, 2);
                string y1 = txtFromDate.Text.Substring(6, 4);
                DateTime FromDATE = DateTime.ParseExact(txtFromDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                string d2 = txtToDate.Text.Substring(0, 2);
                string m2 = txtToDate.Text.Substring(3, 2);
                string y2 = txtToDate.Text.Substring(6, 4);
                DateTime ToDATE = DateTime.ParseExact(txtToDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);
                objLeaveApp.EmpCode = txtEmpCode.Text;
                objLeaveApp.ReportingAuthority = Session["ReportingAuthority"].ToString();
                objLeaveApp.TypeOfLeaveApple = ddlLeaveApply.SelectedItem.Text;
                objLeaveApp.LeaveFrom = FromDATE;
                objLeaveApp.LeaveTo = ToDATE;
                objLeaveApp.NoofDays = (txtNoofDays.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtNoofDays.Text);
                objLeaveApp.LeaveStatus = txtstatus.Text;
                objLeaveApp.CreatedBy = Convert.ToString(Session["Emp_Code"]);
                objLeaveApp.CreatedDate = System.DateTime.Now;
                objLeaveApp.AppliedDate = System.DateTime.Now;
                objLeaveApp.CreationCompany = Session["ClientRegistrationNo"].ToString();
                objLeaveApp.ModifiedBy = Session["Emp_Code"].ToString();
                objLeaveApp.ModifiedDate = System.DateTime.Now;
                objLeaveApp.Parameter = 1;

                if (fupCertification.HasFile)
                {
                    string file = fupCertification.FileName;
                    fupCertification.PostedFile.SaveAs(Server.MapPath("~/HRMS/Certifications/" + file));
                    objLeaveApp.CerificationPath = "~/Certifications/" + file;
                }
                else
                {
                    objLeaveApp.CerificationPath = "";
                }

                if (objLeaveApp.Insert_HRMS_Leave_Application() != 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Leave Applied Successfully');window.location ='../Payroll/LeaveApplication';", true);
                    ClearFields();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
                }
            }
            catch (Exception ex)
            {

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            { }
        }
        // Editing Header Details
        String StrDate = "";
        // // Show the date format dd-mm-yy in sale Order Grids
        public string GetDate(object Dt)
        {
            StrDate = string.Format("{0:dd-MM-yyyy}", Dt);
            return StrDate;
        }

        //Updating Method
        public void bindemp()
        {
            int Emp_Code = (from em in ObjDAL.HRMSEmployeeMasterRegistrationDetails where em.UserName == Session["User_Name"].ToString() select em.ID).FirstOrDefault();
            var Emp_List = (from liste in ObjDAL.USP_gettingemployeedetailsinleave(Emp_Code) select liste).ToList();

            if (Emp_List.Count > 0)
            {
                txtEmpCode.Text = Convert.ToString(Emp_List[0].empid);
                ddlempname.SelectedItem.Text = Emp_List[0].empname;
                txtDepartment.Text = Emp_List[0].userrole;
                txtDesignation.Text = Emp_List[0].usersubrole;
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Emp Code is Not Valid');", true);
                txtEmpCode.Text = "";
                txtEmpCode.Focus();
                txtDepartment.Text = "";
                txtDesignation.Text = "";
            }
        }
        public void Update_Method()
        {
            try
            {
                string d1 = txtFromDate.Text.Substring(0, 2);
                string m1 = txtFromDate.Text.Substring(3, 2);
                string y1 = txtFromDate.Text.Substring(6, 4);

                DateTime FromDATE = DateTime.ParseExact(txtFromDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);

                string d2 = txtToDate.Text.Substring(0, 2);
                string m2 = txtToDate.Text.Substring(3, 2);
                string y2 = txtToDate.Text.Substring(6, 4);

                DateTime ToDATE = DateTime.ParseExact(txtToDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);

                objLeaveApp.EmpCode = txtEmpCode.Text;
                objLeaveApp.ReportingAuthority = Session["ReportingAuthority"].ToString();
                objLeaveApp.TypeOfLeaveApple = ddlLeaveApply.SelectedItem.Text;
                objLeaveApp.LeaveFrom = FromDATE;
                objLeaveApp.LeaveTo = ToDATE;
                objLeaveApp.NoofDays = (txtNoofDays.Text == "") ? Convert.ToInt32(0) : Convert.ToInt32(txtNoofDays.Text);

                objLeaveApp.CreatedBy = Session["Emp_Code"].ToString();
                objLeaveApp.CreatedDate = System.DateTime.Now;
                objLeaveApp.CreationCompany = Session["ClientRegistrationNo"].ToString();
                objLeaveApp.ModifiedBy = Session["Emp_Code"].ToString();
                objLeaveApp.ModifiedDate = System.DateTime.Now;
                objLeaveApp.Parameter = 2;
                if (objLeaveApp.Insert_HRMS_Leave_Application() != 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);
                    ClearFields();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            { }
        }

        /// <summary>
        /// No Of Days Calculation
        /// </summary>
        public void NoOfDaysCal()
        {
            try
            {
                if (txtFromDate.Text != "" && txtToDate.Text != "")
                {
                    string d1 = txtFromDate.Text.Substring(0, 2);
                    string m1 = txtFromDate.Text.Substring(3, 2);
                    string y1 = txtFromDate.Text.Substring(6, 4);
                    DateTime FromYear = Convert.ToDateTime(m1 + '-' + d1 + '-' + y1);

                    string d2 = txtToDate.Text.Substring(0, 2);
                    string m2 = txtToDate.Text.Substring(3, 2);
                    string y2 = txtToDate.Text.Substring(6, 4);
                    DateTime ToYear = Convert.ToDateTime(m2 + '-' + d2 + '-' + y2);

                    //Creating object of TimeSpan Class  
                    TimeSpan objTimeSpan = FromYear - ToYear;
                    //years
                    int Years = FromYear.Year - ToYear.Year;

                    //months
                    int month = FromYear.Month - ToYear.Month;

                    //Total Months
                    int TotalMonths = (Years * 12) + month;

                    //TotalDays  
                    double Days = Math.Abs(Convert.ToDouble(objTimeSpan.TotalDays));
                    txtNoofDays.Text = Convert.ToString(Days);
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }
            finally
            {
            }
        }

        public void approveleavedetails()
        {
            int empid = Convert.ToInt32(Session["Emp_Code"].ToString());


            var approve = (from item in ObjDAL.Usp_GettingApprovedleaveDetailsinDashboard(empid)
                           select item).ToList();
            if (approve.Count > 0)
            {
                gdvleaveapproved.DataSource = approve;
                gdvleaveapproved.DataBind();
            }
            else
            {
                gdvleaveapproved.DataSource = approve;
                gdvleaveapproved.DataBind();
            }
        }

        public void leaveAva()
        {
            if (txtToDate.Text != "" && txtFromDate.Text != "")
            {
                if (Convert.ToDateTime(txtToDate.Text) > Convert.ToDateTime(txtFromDate.Text))
                {
                    txtNoofDays.Text = (Convert.ToDateTime(txtToDate.Text) - Convert.ToDateTime(txtFromDate.Text)).ToString();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Your Have Only " + txtNoofDays.Text.ToString() + " Leaves ');", true);
                    txtFromDate.Text = "";
                    txtToDate.Text = "";
                    //  txtNoofDays.Text = string.Empty;
                }
            }
        }

        public void binddnoofleavesvalue()
        {
            try
            {

                List<HRMSNewLeaveMaster> noofleaves = (from item in ObjDAL.HRMSNewLeaveMasters
                                                       select item).ToList();


                if (ddlLeaveApply.SelectedItem.Value == "2")
                {
                    txtnoofleaves.Text = Convert.ToString(noofleaves[0].NoOfLeaves);


                }
                else
                {



                }
            }
            catch
            {
            }
        }



        #endregion

        protected void ddlempname_SelectedIndexChanged1(object sender, EventArgs e)
        {
            string name = ddlempname.SelectedItem.Text;
            var empcode = (from per in objDAL.HRMSEmplyeeMasterPersonalDetails
                           join rep in objDAL.HRMSEmployeeMasterOfficialInformationDetails on per.EmpId equals rep.EmpId
                           where per.FirstName + " " + per.MiddleName + " " + per.LastName == name
                           select per.EmpId).FirstOrDefault();

            var Emp_List = (from liste in ObjDAL.USP_gettingemployeedetailsinleave( Convert.ToInt32(empcode)) select liste).FirstOrDefault();

           var Emp_ReportingAuthorityName = (from rep in ObjDAL.HRMSEmplyeeMasterPersonalDetails
                                            where rep.EmpId== Emp_List.ReportingAuthority
                                             select rep.FirstName + " " + rep.MiddleName + " " + rep.LastName).FirstOrDefault();

            if (Emp_List!=null) { 
                txtEmpCode.Text = Convert.ToString(Emp_List.empid);
                ddlempname.SelectedItem.Text = Emp_List.empname;
                txtDepartment.Text = Emp_List.userrole;
                txtDesignation.Text = Emp_List.usersubrole;
                txtreportingauthority.Text = Emp_ReportingAuthorityName;
            }

            else
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Emp Code is Not Valid');", true);

                txtEmpCode.Text = "";
                txtEmpCode.Focus();
                txtDepartment.Text = "";
                txtDesignation.Text = "";
                txtreportingauthority.Text = "";
            }
        }
    }
}
