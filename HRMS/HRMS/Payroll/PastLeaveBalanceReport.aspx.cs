﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;

namespace HRMS.Payroll
{
    public partial class PastLeaveBalanceReport : System.Web.UI.Page
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void GetLeaveDetails()
        {
            int empcode = Convert.ToInt32(Session["Emp_Code"]);
            var year = Convert.ToString(ddlYear.SelectedItem);
            var leavebalance = (from item in objDAL.Usp_GettingTotalBalanceLeavesInLeaveBalancePage(year, empcode)
                                select item).ToList();
            if (leavebalance.Count > 0)
            {
                GdvBalanceLeaves.DataSource = leavebalance;
                GdvBalanceLeaves.DataBind();
            }
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            GetLeaveDetails();
        }
    }
}