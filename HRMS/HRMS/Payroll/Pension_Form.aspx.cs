﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BAL;
using DAL;

namespace HRMS.Payroll
{
    public partial class Pension_Form : System.Web.UI.Page
    {
        ERP_DatabaseDataContext objdb = new ERP_DatabaseDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               
                Emp_NameBinding();

                //Bind_Grid1();

            }
        }
        protected void txtEmpCode_TextChanged(object sender, EventArgs e)
        {
            string CreationCompany = Session["ClientRegistrationNo"].ToString();
            string Emp_Code = (txtEmpCode.Text.ToLower().Trim() == "") ? "" : txtEmpCode.Text.ToLower().Trim();
            var objEmPCodeDUP = (from itmes in objdb.employee_detail_informations

                                 where itmes.Emp_Code.ToLower().Trim() == Emp_Code
                                 select new
                                 {
                                     User_Name = itmes.First_Name + " " + itmes.Last_Name + " " + itmes.Middle_Name,
                                     Emp_Code = itmes.Emp_Code
                                 }).ToList();
            if (objEmPCodeDUP.Count > 0)
            {

                ddlemployename.SelectedItem.Text = objEmPCodeDUP[0].User_Name;

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Emp Code is Not Valid');", true);
                txtEmpCode.Text = "";
                txtEmpCode.Focus();
                ddlemployename.SelectedValue = "0";


            }
        }
        protected void ddlemployename_SelectedIndexChanged(object sender, EventArgs e)
        {
            string CreationCompany = (string)Session["ClientRegistrationNo"].ToString();
            txtEmpCode.Text = ddlemployename.SelectedValue;
            var objEmPCodeDUP = (from itmes in objdb.employee_detail_informations

                                 where itmes.Emp_Code == txtEmpCode.Text
                                 select itmes).ToList();
            if (objEmPCodeDUP.Count > 0)
            {
                txtcicle.Text = objEmPCodeDUP[0].circle;
                txtdfo.Text = objEmPCodeDUP[0].district;
                txtdivisions.Text = objEmPCodeDUP[0].Divisions;
                txtdeprtment.Text = Convert.ToInt32(objEmPCodeDUP[0].User_Role).ToString();

            }

        }
        public void Emp_NameBinding()
        {
            string CreationCompany = Session["ClientRegistrationNo"].ToString();

            var objEmPCodeDUP = (from itmes in objdb.employee_detail_informations

                                 select new
                                 {
                                     User_Name = itmes.Last_Name + " " + itmes.First_Name + " " + itmes.Middle_Name,
                                     Emp_Code = itmes.Emp_Code
                                 }).ToList();
            if (objEmPCodeDUP.Count > 0)
            {
                ddlemployename.DataSource = objEmPCodeDUP;
                ddlemployename.DataTextField = "User_Name";
                ddlemployename.DataValueField = "Emp_Code";
                ddlemployename.DataBind();
                ddlemployename.Items.Insert(0, new ListItem("--Select--", "0"));
            }

        }
        protected void ddlcircle_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlcircle.SelectedItem.Text == "Adilabad")
            {
                ddldistrictforestofficers.Items.Clear();
                ddldistrictforestofficers.Items.Add("---Select---");
                ddldistrictforestofficers.Items.Add("Adilabad");
                ddldistrictforestofficers.Items.Add("Komarambheem");


            }
            else if (ddlcircle.SelectedItem.Text == "Kawal Tiger Reserve")
            {
                ddldistrictforestofficers.Items.Clear();
                ddldistrictforestofficers.Items.Add("---Select---");
                ddldistrictforestofficers.Items.Add("Mancherial");
                ddldistrictforestofficers.Items.Add("Nirmal(New)");

            }
            else if (ddlcircle.SelectedItem.Text == "Kothagudem")
            {
                ddldistrictforestofficers.Items.Clear();
                ddldistrictforestofficers.Items.Add("---Select---");
                ddldistrictforestofficers.Items.Add("Kothagudem");
                ddldistrictforestofficers.Items.Add("Mahabubabad");

            }
            else if (ddlcircle.SelectedItem.Text == "Khammam")
            {
                ddldistrictforestofficers.Items.Clear();
                ddldistrictforestofficers.Items.Add("---Select---");
                ddldistrictforestofficers.Items.Add("Khammam");
                ddldistrictforestofficers.Items.Add("Suryapet");

            }
            else if (ddlcircle.SelectedItem.Text == "Nizamabad")
            {
                ddldistrictforestofficers.Items.Clear();
                ddldistrictforestofficers.Items.Add("---Select---");
                ddldistrictforestofficers.Items.Add("Nizamabad");
                ddldistrictforestofficers.Items.Add("Kamareddy");

            }
            else if (ddlcircle.SelectedItem.Text == "Amrabada Tiger Reserve")
            {
                ddldistrictforestofficers.Items.Clear();
                ddldistrictforestofficers.Items.Add("---Select---");
                ddldistrictforestofficers.Items.Add("Nagarkarnool");
                ddldistrictforestofficers.Items.Add("Nalgonda");

            }
            else if (ddlcircle.SelectedItem.Text == "Hyderabad")
            {
                ddldistrictforestofficers.Items.Clear();
                ddldistrictforestofficers.Items.Add("---Select---");
                ddldistrictforestofficers.Items.Add("Hyderabad");
                ddldistrictforestofficers.Items.Add("Medchal");
                ddldistrictforestofficers.Items.Add("Yadadri");

            }
            else if (ddlcircle.SelectedItem.Text == "Karimnagar")
            {
                ddldistrictforestofficers.Items.Clear();
                ddldistrictforestofficers.Items.Add("---Select---");
                ddldistrictforestofficers.Items.Add("Jagital(New)");
                ddldistrictforestofficers.Items.Add("Karimnagar + Sircilla");
                ddldistrictforestofficers.Items.Add("Peddapally");

            }
            else if (ddlcircle.SelectedItem.Text == "Warangal")
            {

                ddldistrictforestofficers.Items.Add("Bhupalpally");
                ddldistrictforestofficers.Items.Add("Warngal(U) + Jangoan");
                ddldistrictforestofficers.Items.Add("Warngal(R)");

            }
            else if (ddlcircle.SelectedItem.Text == "Medak")
            {
                ddldistrictforestofficers.Items.Clear();
                ddldistrictforestofficers.Items.Add("---Select---");
                ddldistrictforestofficers.Items.Add("Siddipet");
                ddldistrictforestofficers.Items.Add("Medak");
                ddldistrictforestofficers.Items.Add("Sangareddy");

            }
            else if (ddlcircle.SelectedItem.Text == "Mahabubnagar")
            {
                ddldistrictforestofficers.Items.Clear();
                ddldistrictforestofficers.Items.Add("---Select---");
                ddldistrictforestofficers.Items.Add("Mahabubnagar");
                ddldistrictforestofficers.Items.Add("Gadwal + Wanaparthy");

            }
            else if (ddlcircle.SelectedItem.Text == "Ranga Reddy")
            {
                ddldistrictforestofficers.Items.Clear();
                ddldistrictforestofficers.Items.Add("---Select---");

                ddldistrictforestofficers.Items.Add("Ranga Reddy");
                ddldistrictforestofficers.Items.Add("Vikarabad");

            }
            else
            {

            }

        }

        protected void ddldistrictforestofficers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddldistrictforestofficers.SelectedItem.Text == "Adilabad")
            {
                ddlforestdivisions.Items.Clear();
                ddlforestdivisions.Items.Add("---Select---");
                ddlforestdivisions.Items.Add("Adilabad");
                ddlforestdivisions.Items.Add("Utnoor FDPT");
                ddlforestdivisions.Items.Add("Echoda");
            }
            else if (ddldistrictforestofficers.SelectedItem.Text == "Komarambheem")
            {
                ddlforestdivisions.Items.Clear();
                ddlforestdivisions.Items.Add("---Select---");
                ddlforestdivisions.Items.Add("Asifabad");
                ddlforestdivisions.Items.Add("Khagaznagar");

            }
            else if (ddldistrictforestofficers.SelectedItem.Text == "Mancherial")
            {
                ddlforestdivisions.Items.Clear();
                ddlforestdivisions.Items.Add("---Select---");
                ddlforestdivisions.Items.Add("Jannaram FDPT");
                ddlforestdivisions.Items.Add("Chennur");
                ddlforestdivisions.Items.Add("Bellampally");
                ddlforestdivisions.Items.Add("Mancherial");
            }
            else if (ddldistrictforestofficers.SelectedItem.Text == "Nirmal(New)")
            {
                ddlforestdivisions.Items.Clear();
                ddlforestdivisions.Items.Add("---Select---");
                ddlforestdivisions.Items.Add("Nirmal");

                ddlforestdivisions.Items.Add("Khanapur");
            }
            else if (ddldistrictforestofficers.SelectedItem.Text == "Kothagudem")
            {
                ddlforestdivisions.Items.Clear();
                ddlforestdivisions.Items.Add("---Select---");
                ddlforestdivisions.Items.Add("Kothagudem");
                ddlforestdivisions.Items.Add("Yellanadu");
                ddlforestdivisions.Items.Add("Paloncha");
                ddlforestdivisions.Items.Add("Mangur");
                ddlforestdivisions.Items.Add("WLM Kinnerasani");
                ddlforestdivisions.Items.Add("Bhadrachalam");
            }
            else if (ddldistrictforestofficers.SelectedItem.Text == "Mahabubabad")
            {
                ddlforestdivisions.Items.Clear();
                ddlforestdivisions.Items.Add("---Select---");
                ddlforestdivisions.Items.Add("Mahabubabad");
                ddlforestdivisions.Items.Add("WL Gudur");

            }
            else if (ddldistrictforestofficers.SelectedItem.Text == "Khammam")
            {
                ddlforestdivisions.Items.Clear();
                ddlforestdivisions.Items.Add("---Select---");
                ddlforestdivisions.Items.Add("Khammam");

                ddlforestdivisions.Items.Add("Sathupally");
            }
            else if (ddldistrictforestofficers.SelectedItem.Text == "Nizamabad")
            {
                ddlforestdivisions.Items.Clear();
                ddlforestdivisions.Items.Add("---Select---");
                ddlforestdivisions.Items.Add("Nizamabad");

                ddlforestdivisions.Items.Add("Armoor");
            }
            else if (ddldistrictforestofficers.SelectedItem.Text == "Kamareddy")
            {
                ddlforestdivisions.Items.Clear();
                ddlforestdivisions.Items.Add("---Select---");
                ddlforestdivisions.Items.Add("Kamareddy");

                ddlforestdivisions.Items.Add("Banswada");
            }
            else if (ddldistrictforestofficers.SelectedItem.Text == "Nagarkarnool")
            {
                ddlforestdivisions.Items.Clear();
                ddlforestdivisions.Items.Add("---Select---");
                ddlforestdivisions.Items.Add("Achampet");

                ddlforestdivisions.Items.Add("Amarbad");
            }
            else if (ddldistrictforestofficers.SelectedItem.Text == "Nalgonda")
            {
                ddlforestdivisions.Items.Clear();
                ddlforestdivisions.Items.Add("---Select---");
                ddlforestdivisions.Items.Add("Nalgonda");

                ddlforestdivisions.Items.Add("Nagarjunsagar WL");
            }
            else if (ddldistrictforestofficers.SelectedItem.Text == "Bhupalpally")
            {
                ddlforestdivisions.Items.Clear();
                ddlforestdivisions.Items.Add("---Select---");
                ddlforestdivisions.Items.Add("Bhupalpally");
                ddlforestdivisions.Items.Add("Mahadevpur");
                ddlforestdivisions.Items.Add("Mulugu");
                ddlforestdivisions.Items.Add("Tadval");
                ddlforestdivisions.Items.Add("WLM Eturungaram");
                ddlforestdivisions.Items.Add("Venkatapuram");
            }
            else if (ddldistrictforestofficers.SelectedItem.Text == "Ranga Reddy")
            {
                ddlforestdivisions.Items.Clear();
                ddlforestdivisions.Items.Add("---Select---");
                ddlforestdivisions.Items.Add("Shamshabad");

                ddlforestdivisions.Items.Add("Amangal");
            }

            else
            {

            }

        }

        //protected void btnSave_Click(object sender, EventArgs e)
        //{
        //    try
        //    {

           
        //    pension_form pf = new pension_form();

        //    pf.Employee_ID = txtEmpCode.Text;
        //    pf.Employee_Name = ddlemployename.SelectedItem.Text;
        //    pf.Name_Of_Circle = ddldepartment.SelectedItem.Text;
        //    pf.Designation = ddldesignation.SelectedItem.Text;
        //    pf.Date_retirement_death = (txtdateofretirement.Text == "") ? DateTime.Now : Convert.ToDateTime(txtdateofretirement.Text);

        //    pf.no_papers_received = (txtrecived.Text == "") ? 0 : Convert.ToInt32(txtrecived.Text);
                
        //    pf.no_papers_balance = (txtbalance.Text == "") ?0 : Convert.ToInt32(txtbalance.Text);
               
        //    pf.forwarding_pension_papers = txtwithinsixmonths.Text;
        //    pf.circle = ddlcircle.SelectedItem.Text;
        //    pf.distric = ddldistrictforestofficers.SelectedItem.Text;
        //    pf.sub_distric = ddlforestdivisions.SelectedItem.Text;
        //    pf.DOb = (txtdob.Text == "") ? DateTime.Now : Convert.ToDateTime(txtdob.Text);
               
        //    pf.date_of_entry_service = (txtenterservice.Text == "") ? DateTime.Now : Convert.ToDateTime(txtenterservice.Text);
                
        //    pf.date_sending_auterisation = (txtsendingauterisation.Text == "") ? DateTime.Now : Convert.ToDateTime(txtsendingauterisation.Text);
               
        //    pf.date_recipt_auterisation = (txtreceiptofautherity.Text == "") ? DateTime.Now : Convert.ToDateTime(txtreceiptofautherity.Text);
           
        //    pf.reason_not_sending_autherisation = txtnotsending.Text;
        //    string interests = string.Empty;
        //    foreach (ListItem item in this.chkbl.Items)
        //    {
        //        if (item.Selected)
        //        {
        //            interests += item + ",";
        //        }
        //    }
        //    pf.receipt_pps = interests.TrimEnd(',');
        //    pf.date_of_forwarding_pension_to_ag = (txtdateofforwarding.Text == "") ? DateTime.Now : Convert.ToDateTime(txtdateofforwarding.Text);

        //    pf.date_pvr = Convert.ToDateTime(txtdatepvr.Text);
        //    pf.sanction_order_psa = TextBox3.Text;
        //    pf.autherization_issued = TextBox4.Text;
        //    string pendingpps = string.Empty;
        //    foreach (ListItem item in this.CheckBoxList1.Items)
        //    {
        //        if (item.Selected)
        //        {
        //            pendingpps += item + ",";
        //        }
        //    }
        //    pf.resaons_for_pending_non_pps = pendingpps.TrimEnd(',');
        //    pf.pending_whome = TextBox5.Text;
        //     string sanction = string.Empty;
        //    foreach (ListItem item in this.CheckBoxList2.Items)
        //    {
        //        if (item.Selected)
        //        {
        //            sanction += item + ",";
        //        }
        //    }
        //    pf.due_to_disc_cases = sanction.TrimEnd(',');
        //    string anticipotory = string.Empty;
        //    foreach (ListItem item in this.CheckBoxList3.Items)
        //    {
        //        if (item.Selected)
        //        {
        //            anticipotory += item + ",";
        //        }
        //    }
        //    pf.non_finalisation_ndc = anticipotory.TrimEnd(',');
        //    string family = string.Empty;
        //    foreach (ListItem item in this.CheckBoxList4.Items)
        //    {
        //        if (item.Selected)
        //        {
        //            family += item + ",";
        //        }
        //    }
        //    pf.death_employee = family.TrimEnd(',');
        //     string issue = string.Empty;
        //    foreach (ListItem item in this.CheckBoxList5.Items)
        //    {
        //        if (item.Selected)
        //        {
        //            issue += item + ",";
        //        }
        //    }
        //    pf.forward_ag_autherization = issue.TrimEnd(',');
           
        //    pf.autherisation_recd = TextBox6.Text;


        //    objdb.pension_forms.InsertOnSubmit(pf);
        //    objdb.SubmitChanges();
        //    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('data saved');", true);
        //    }
        //    catch (Exception ex)
        //    {

        //        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('please Check All The Details');", true);
        //    }
        //}
    }
}