﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="LoanApproval.aspx.cs" Inherits="HRMS.Payroll.LoanApproval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .fontweight-pm {
            font-weight: 200;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx" class="current"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="#">Loan Approval</a><i class="fa fa-angle-right"></i> </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <i class="fa fa-user">Loan Approval</i>
                        </header>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-5">
                                        Employee Name</label>
                                    <div class="col-sm-7" style="" id="Div9">
                                        <label class="control-label fontweight-pm" runat="server" id="lblempname"></label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-5" style="">
                                        Department</label>
                                    <div class="col-sm-7" style="" id="Div10">
                                        <label class="control-label fontweight-pm" runat="server" id="lbldept"></label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-5 " style="">
                                        Designation</label>
                                    <div class="col-sm-7" style="" id="Div11">
                                        <label class="control-label fontweight-pm" runat="server" id="lbldesignation"></label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-5 " style="">
                                        Reporting
                                    </label>
                                    <div class="col-sm-7" style="" id="Div1">
                                        <label class="control-label fontweight-pm" runat="server" id="lblreportingauthority"></label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-5" style="">
                                        Loan Type</label>
                                    <div class="col-sm-7" style="" id="Div12">
                                        <label class="control-label fontweight-pm" runat="server" id="lblloantype"></label>
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-5" style="">
                                        Status
                                    </label>
                                    <div class="col-sm-7">
                                        <label class="control-label fontweight-pm" runat="server" id="lblstatus"></label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-5" style="">
                                        Tenure
                                    </label>
                                    <div class="col-sm-7">
                                        <label class="control-label fontweight-pm" runat="server" id="lbltenure"></label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-5" style="">
                                        Basic Amount
                                    </label>
                                    <div class="col-sm-7">
                                        <label class="control-label fontweight-pm" runat="server" id="lblbasicamount"></label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-5" style="">
                                        Loan Amount Required
                                    </label>
                                    <div class="col-sm-7">
                                        <label class="control-label fontweight-pm" runat="server" id="lblamountrequired"></label>
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-5" style="">
                                        Rate Of Interst
                                    </label>
                                    <div class="col-sm-7">
                                        <label class="control-label fontweight-pm" runat="server" id="lblrateofinterst"></label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-5" style="">
                                        Applied Date
                                    </label>
                                    <div class="col-sm-7">
                                        <label class="control-label fontweight-pm" runat="server" id="lbldate"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 text-center">
                                <asp:Button ID="btnapprove" runat="server" CssClass=" btn btn-primary" Text="Approve" OnClick="btnapprove_Click" />
                                <asp:Button ID="btncancel" runat="server" CssClass=" btn btn-primary" Text="Back" OnClick="btncancel_Click" />

                                <asp:Button ID="btnreject" runat="server" CssClass=" btn btn-danger" Text="Reject" OnClick="btnreject_Click" />
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
