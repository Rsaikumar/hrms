﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BAL;
using DAL;

namespace HRMS.Payroll
{
    public partial class Pension_Form : System.Web.UI.Page
    {
        ERP_DatabaseDataContext objdb = new ERP_DatabaseDataContext();
        #region pageload
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                empnamedetailsbind();
                txtEmpCode.Visible = false;
                Employeenamebinding();
            }
        }
        #endregion

        #region Methods
        public void empnamedetailsbind()
        {
            string emp = Convert.ToString(Session["Emp_Code"]);
            var location = (from item in objdb.HRMSEmployeeMasterWorkLocationDetails
                            where item.ID == Convert.ToInt32( emp)
                            select item).SingleOrDefault();


            List<HRMSDepartmentMaster> objrole = (from or in objdb.HRMSDepartmentMasters
                                               where or.DepartmentId == Convert.ToInt32(emp)
                                               select or).ToList();

            txtdeprtment.Text = Convert.ToString(objrole[0].DepartmentName);

            List<Division> divisiondetails = (from master in objdb.Divisions
                                              where master.Divisioncd == location.Division.Trim()
                                              select master).ToList();

            txtdivisions.Text = divisiondetails[0].Divisionname;

            List<Circle> circledetails = (from master in objdb.Circles
                                          where master.Circlecd == location.Circle.Trim()
                                          select master).ToList();

            txtcicle.Text = circledetails[0].Circlename;
        }
        protected void bind()
        {
            int emp = Convert.ToInt32(Session["Emp_Code"]);
            txtEmpCode.Text = txtnameretiremonths.Text;
            var objEmPCodeDUP = (from itmes in objdb.employee_detail_informations

                                 where itmes.Emp_Code == txtEmpCode.Text
                                 select itmes).ToList();
            if (objEmPCodeDUP.Count > 0)
            {
                txtcicle.Text = objEmPCodeDUP[0].circle;
                txtdivisions.Text = objEmPCodeDUP[0].Divisions;
                txtdeprtment.Text = Convert.ToInt32(objEmPCodeDUP[0].User_Role).ToString();

            }

        }

        
        //namebinding     
        public void Employeenamebinding()
        {
            try
            {
                List<HRMSEmployeeMasterOfficialInformationDetail> objEmpofficialinfo = (from master in objdb.HRMSEmployeeMasterOfficialInformationDetails
                                                                                        where master.ID == Convert.ToInt32(Session["Emp_Code"])
                                                                                        select master).ToList();

                var reporting = (from item in objdb.USP_gettingemployeenameandid(Convert.ToInt32(Session["Emp_Code"]))
                                 select item).ToList();

                txtnameretiremonths.Text = reporting[0].empname;
               
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region Events
        //saving
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                HRMSEmployeePensionForm erd = new HRMSEmployeePensionForm();
                //erd.NameOfTheEmployee = txtnameretiremonths.Text;
                erd.Designation = txtdesignation.Text;
                erd.DateOfBirth = DateTime.ParseExact(txtdob.Text, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                erd.DateOfEntry = DateTime.ParseExact(txtenterservice.Text, "dd-MM-yyyy",System.Globalization.CultureInfo.InvariantCulture);
                erd.PlaceOfStation = txtplaceofstation.Text;
                erd.DateOfRetirement = DateTime.ParseExact(txtdateofretirement.Text, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                erd.DateOfReceiptPension = DateTime.ParseExact(txtdateofreceiptofpensionpapers.Text, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                erd.StatusPensionPapers = ddlstatuspensionpapers.SelectedItem.Text;
                erd.DateOfForwarding = DateTime.ParseExact(txtdateofforwarding.Text, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                erd.DateOfReceiptPVR = DateTime.ParseExact(txtdatepvr.Text, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                erd.ForwardProposals = ddlforwardedproposals.SelectedItem.Text;
                erd.DateOfSendingAuthorisation = DateTime.ParseExact(txtdateofsendingauthorisation.Text, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                erd.DateOfReceiptAuthorisation = DateTime.ParseExact(txtdateofreceiptgauthorisation.Text, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                erd.GratuityAuthorisation = ddlgratuity.SelectedItem.Text;
                erd.DateOfReceiptRequestion = DateTime.ParseExact(txtDateOfReceiptOfRequisitionforSanctionOfAnticipatoryPension.Text, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                erd.DateOfReceiptAuthority = DateTime.ParseExact(txtdatereceiptofautherity.Text, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                erd.IssuesMentioned = ddlissuesmentioned.SelectedItem.Text;
                erd.ActionTaken = ddlactiontaken.SelectedItem.Text;
                erd.FullPension = ddlfullpension.SelectedItem.Text;
                erd.Remarks = txtremarks.Text;
                erd.CreatedBy = Convert.ToString(Session["Emp_Code"]);
                erd.CreatedOn = DateTime.Now;
                erd.ModifiedBy = "";
                objdb.HRMSEmployeePensionForms.InsertOnSubmit(erd);
                 objdb.SubmitChanges();
                 ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Pension Applied sucessfully');window.location ='../Dashboard/Dashboard.aspx';", true);

            }

            catch (Exception ex)
            {
                var error = ex.Message.ToString();

            }
        }
        protected void txtEmpCode_TextChanged(object sender, EventArgs e)
        {
            string CreationCompany = Session["ClientRegistrationNo"].ToString();
            string Emp_Code = (txtEmpCode.Text.ToLower().Trim() == "") ? "" : txtEmpCode.Text.ToLower().Trim();
            var objEmPCodeDUP = (from itmes in objdb.employee_detail_informations

                                 where itmes.Emp_Code.ToLower().Trim() == Emp_Code
                                 select new
                                 {
                                     User_Name = itmes.First_Name + " " + itmes.Last_Name + " " + itmes.Middle_Name,
                                     Emp_Code = itmes.Emp_Code
                                 }).ToList();
            if (objEmPCodeDUP.Count > 0)
            {

                txtnameretiremonths.Text = objEmPCodeDUP[0].User_Name;

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Emp Code is Not Valid');", true);
                txtEmpCode.Text = "";
                txtEmpCode.Focus();
                txtnameretiremonths.Text = "0";


            }
        }

      
    }
}
        #endregion