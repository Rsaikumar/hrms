﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BAL;
using DAL;

namespace HRMS.Payroll
{
    public partial class Leave_Allotment : System.Web.UI.Page
    {
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();
        Leave_AllotmentBLL objLeaveAllotmentBLL = new Leave_AllotmentBLL();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["User_Type"].ToString() == "User")
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('You Don't have Permissions To Create..');", true);
                    Response.Redirect("~/Dashboard/Dashboard.aspx");
                }
                {
                    if (Session["ClientRegistrationNo"] == null)
                    {
                        Response.Redirect("~/Default.aspx");
                    }
                    btnUpdate.Visible = false;

                    txtEmpCode.Focus();
                    Emp_NameBinding();
                    Panel2.Visible = false;
                }
            }
        }
        //protected void txtEmpCode_OnTextChanged(object sender, EventArgs e)
        //{
        //    string CreationCompany = Session["ClientRegistrationNo"].ToString();
        //    string Emp_Code = (txtEmpCode.Text.ToLower().Trim() == "") ? "" : txtEmpCode.Text.ToLower().Trim();
        //    var objEmPCodeDUP = (from itmes in ObjDAL.HRMSEmployeeMasterRegistrationDetails
        //                                       where itmes.CompanyID == Convert.ToInt32(CreationCompany) && itmes.ID == Convert.ToInt32(Emp_Code)
        //                         select new
        //                         {
        //                           Emp_name= itmes.UserName,
        //                           Emp_Code = itmes.ID
        //                         }).ToList();
        //    if (objEmPCodeDUP.Count > 0)
        //    {
        //        ddlEmpName.SelectedItem.Text = objEmPCodeDUP[0].Emp_name;
        //    }
        //    else
        //    {
        //        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Emp Code is Not Valid');", true);
        //        txtEmpCode.Text = "";
        //        txtEmpCode.Focus();
        //        ddlEmpName.SelectedValue = "0";
        //    }
        //}

        public void Emp_NameBinding()
        {

            //int empcode = Convert.ToInt32(txtEmpCode.Text);
            string CreationCompany = Session["ClientRegistrationNo"].ToString();
          
            var objEmPCodeDUP = (from itmes in ObjDAL.USP_GetEmployeeFullNameInLeaveBalance()
                                 select new
                                 {
                                     Emp_name = itmes.empname,
                                     Emp_Code = itmes.empid
                                 }).ToList();
            if (objEmPCodeDUP.Count > 0)
            {
                ddlEmpName.DataSource = objEmPCodeDUP;
                ddlEmpName.DataTextField = "Emp_name";
                ddlEmpName.DataValueField = "Emp_Code";
                ddlEmpName.DataBind();
                ddlEmpName.Items.Insert(0, new ListItem("--Select--", "0"));
            }
           
        }
        protected void RBLNewAllotment_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if(RBLNewAllotment.SelectedValue=="1")
            {
                Panel2.Visible = true;
                string CreationCompany = Session["ClientRegistrationNo"].ToString();
                
                var objEmPCodeDUP = (from itmes in ObjDAL.HRMSLeaveMasters
                                     where itmes.CreationCompany == CreationCompany 
                                     select new
                                     {
                                         Leave_Name = itmes.ShortName,
                                         Leave_Year = itmes.NoOflLeavesPerYear
                                     }).ToList();
                grdLeaveInfo.DataSource = objEmPCodeDUP;
                grdLeaveInfo.DataBind();
                btnSave.Focus();
            }
            else
            {
                Panel2.Visible = true;
                string CreationCompany = Session["ClientRegistrationNo"].ToString();

                var objEmPCodeDUP = (from itmes in ObjDAL.HRMSLeaveMasters
                                     where itmes.CreationCompany == CreationCompany
                                     select new
                                     {
                                         Leave_Name = itmes.ShortName,
                                         Leave_Year = ""
                                     }).ToList();
                grdLeaveInfo.DataSource = objEmPCodeDUP;
                grdLeaveInfo.DataBind();
                TextBox txtEventName = (TextBox)grdLeaveInfo.Rows[0].FindControl("txtEventName");
                txtEventName.Focus();
            }
        }


        //Save Event
        protected void btnSave_Click(object sender, EventArgs e)
        {
            Saving();
        }
        //Save method
        private void Saving()
        {
            try
            {
                int count = 0;
                foreach (GridViewRow objrow in grdLeaveInfo.Rows)
                {
                    objLeaveAllotmentBLL.EmpCode = txtEmpCode.Text;
                    objLeaveAllotmentBLL.SelectYear = ddlYear.SelectedItem.Text;
                    if (RBLNewAllotment.SelectedValue == "1")
                    {
                        objLeaveAllotmentBLL.NewOpeningBalance = "1";
                    }
                    else
                    {
                        objLeaveAllotmentBLL.NewOpeningBalance = "2";
                    }
                    TextBox txtLeaveName = (TextBox)objrow.FindControl("txtDate");
                    TextBox txtEventName = (TextBox)objrow.FindControl("txtEventName");
                    objLeaveAllotmentBLL.LeaveName = (txtLeaveName.Text == "") ? "" : txtLeaveName.Text;
                    objLeaveAllotmentBLL.NoofLeavs = (txtEventName.Text == "") ? Convert.ToDecimal(00) : Convert.ToDecimal(txtEventName.Text);
                    objLeaveAllotmentBLL.CreatedBy = Session["User_Name"].ToString();
                    objLeaveAllotmentBLL.CreatedDate = System.DateTime.Now;
                    objLeaveAllotmentBLL.CreationCompany = Session["ClientRegistrationNo"].ToString();
                    objLeaveAllotmentBLL.ModifiedDate = System.DateTime.Now;
                    objLeaveAllotmentBLL.Parameter = 1;

                    objLeaveAllotmentBLL.Insert_HRMS_Leave_Allotment();                   
                    count = count + 1;
                }
                if(count>0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);
                    ClearFields();
                }
               
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
                }



            }
            catch (Exception ex)
            {

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            { }
        }

        //clearfields
        public void ClearFields()
        {
            txtEmpCode.Text= "";
            ddlEmpName.SelectedIndex = 0;
            ddlYear.SelectedIndex = 0;
           RBLNewAllotment.SelectedItem.Selected = false;
            Panel2.Visible = false;
            btnSave.Visible = true;
            btnUpdate.Visible = false;

        }
        //Update Method
        public void Update_Method()
        {

            var PC = (from POC in ObjDAL.HRMSLeaveAllotments
                      where
                          POC.CreationCompany == Session["ClientRegistrationNo"].ToString() && POC.EmpCode == txtEmpCode.Text 
                          
                      select POC);
            ObjDAL.HRMSLeaveAllotments.DeleteAllOnSubmit(PC);
            ObjDAL.SubmitChanges();          

            int count = 0;

            foreach (GridViewRow objrow in grdLeaveInfo.Rows)
            {
                objLeaveAllotmentBLL.EmpCode = txtEmpCode.Text;
                objLeaveAllotmentBLL.SelectYear = ddlYear.SelectedValue;
                if (RBLNewAllotment.SelectedValue == "1")
                {
                    objLeaveAllotmentBLL.NewOpeningBalance = "1";
                }
                else
                {
                    objLeaveAllotmentBLL.NewOpeningBalance = "2";
                }
                TextBox txtLeaveName = (TextBox)objrow.FindControl("txtDate");
                TextBox txtEventName = (TextBox)objrow.FindControl("txtEventName");
                objLeaveAllotmentBLL.LeaveName = (txtLeaveName.Text == "") ? "" : txtLeaveName.Text;
                objLeaveAllotmentBLL.NoofLeavs = (txtEventName.Text == "") ? Convert.ToDecimal(00) : Convert.ToDecimal(txtEventName.Text);
                objLeaveAllotmentBLL.CreatedBy = Session["User_Name"].ToString();
                objLeaveAllotmentBLL.CreatedDate = System.DateTime.Now;
                objLeaveAllotmentBLL.CreationCompany = Session["ClientRegistrationNo"].ToString();
                objLeaveAllotmentBLL.ModifiedDate = System.DateTime.Now;
                objLeaveAllotmentBLL.Parameter = 1;
                objLeaveAllotmentBLL.Insert_HRMS_Leave_Allotment();

                // objLeaveAllotmentBLL.Insert_HRMS_Leave_Allotment();
                count = count + 1;
            }
            if (count > 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);
                ClearFields();
            }

            else
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
            }


        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearFields();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Update_Method();
           
        }

        protected void btnSave_Click1(object sender, EventArgs e)
        {
            Saving();
        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
            {
            string CreationCompany = Session["ClientRegistrationNo"].ToString();
            if (txtEmpCode.Text!="" && ddlYear.Text!="")
            {
                string Emp_Code = (txtEmpCode.Text.ToLower().Trim() == "") ? "" : txtEmpCode.Text.ToLower().Trim();
                string year = (ddlYear.Text.ToLower().Trim() == "") ? "" : ddlYear.Text.ToLower().Trim();

                var objEmPdataBind = (from itmes in ObjDAL.HRMSLeaveAllotments
                                      where itmes.CreationCompany == CreationCompany 
                                      && itmes.EmpCode ==txtEmpCode.Text && itmes.SelectYear ==ddlYear.Text 
                                     select new
                                     {
                                         Leave_Name = itmes.LeaveName,
                                         Leave_Year = itmes.NoofLeavs,
                                        leave_check=itmes.NewOpeningBalance
                                       }).ToList();

                if (objEmPdataBind.Count > 0)
                {
                    if(objEmPdataBind[0].leave_check == "1")
                    RBLNewAllotment.SelectedValue = "1";  
                    else
                        RBLNewAllotment.SelectedValue = "2";

                    Panel2.Visible = true;
                    btnSave.Visible = false;
                    btnUpdate.Visible = true;
                    grdLeaveInfo.DataSource = objEmPdataBind;
                    grdLeaveInfo.DataBind();
                    btnUpdate.Focus();
                }
            }
        }

        protected void ddlEmpName_SelectedIndexChanged(object sender, EventArgs e)
        {
            string CreationCompany = (string)Session["ClientRegistrationNo"].ToString();
            txtEmpCode.Text = ddlEmpName.SelectedValue;
          
        }
    }
}