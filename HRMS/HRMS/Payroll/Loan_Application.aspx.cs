﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BAL;
using DAL;
using System.Data.Linq.SqlClient;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using System.IO;

namespace HRMS.Payroll
{
    public partial class Loan_Application : System.Web.UI.Page
    {

        LoanapplicationBAL objlapp = new LoanapplicationBAL();
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();
       
        HRMSLoanMaster objLoanapp = new HRMSLoanMaster();
        LeaveApplicationBAL objLeaveApp = new LeaveApplicationBAL();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind_Loan();
            }
            txtleavecode.Visible = false;
            txtroi.ReadOnly = true;
            bindemp();
            //loanbind();
            bindbasicamount();
            if ((string)Session["Leave_code"] != null)
            {
                //  Request.QueryString.Clear();
                string ClientRegistrationNo = Session["ClientRegistrationNo"].ToString();
                string AdditionCode = (string)Session["Leave_code"];
                Session.Remove("Leave_code");
                //string LoginID = Request.QueryString["id"];
                //Edit_HeaderDetails(AdditionCode, ClientRegistrationNo);
            }
            else
            {
                btnUpdate.Visible = false;
            }
        }
        public void bindbasicamount()
        {
            try
            {
                string CreationCompany = Session["ClientRegistrationNo"].ToString();
                string Emp_Code = (from em in ObjDAL.HRMS_Employee_Master_PersonalDetails where em.User_Name == Session["User_Name"].ToString() select em.Emp_Code).FirstOrDefault();

                var basicamount = (from amount in ObjDAL.HRMS_Employee_Official_Info_Head_Details
                                   where amount.Creation_Company == CreationCompany && amount.Emp_Code == Emp_Code
                                   select amount.Basic_Amount).FirstOrDefault();
                txtbasicamount.Text = basicamount.ToString();
                    //ddlbasicamount.DataValueField = "Emp_Code";
                    //ddlbasicamount.DataBind();
            }
            catch (Exception)
            {

                throw;
            }

        }
        
        public void Bind_Loan()
        {
            try
            {
             
                var objHeaderInfo = (from header in ObjDAL.HRMS_Loan_Masters
                                     orderby header.Sno descending
                                     select header).ToList();
                if (objHeaderInfo.Count > 0)
                {
                    ddlLoanApply.DataTextField = "Short_Name";
                    ddlLoanApply.DataValueField = "Loan_Code";
                    ddlLoanApply.DataSource = objHeaderInfo;
                    ddlLoanApply.DataBind();
                }
                else
                {
                    ddlLoanApply.Items.Insert(0, new ListItem("--Select--", "Select"));
                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }

        }

        //clearfields
        public void ClearFields()
        {
            txtEmpCode.Text = "";
           
            txtDepartment.Text = "";
            txtDesignation.Text = "";

            ddlLoanApply.SelectedValue = "Select";
           
            ddlEmpName.SelectedIndex = 0;



        }
        //Saving
        private void Saving()
        {
            try
            {
                objlapp.Emp_Code = txtEmpCode.Text;
                objlapp.Emp_Name = txtEmpName.Text;// ddlEmpName.SelectedValue;
                objlapp.Department = txtDepartment.Text;
                objlapp.Designation = txtDesignation.Text;
                objlapp.Reporting_Authority = txtReportingAuthority.Text;
                objlapp.Type_Of_Loan_Apply =ddlLoanApply.SelectedItem.Text;
                objlapp.Loan_Code = ddlLoanApply.SelectedValue;
                objlapp.Basic_Amount=(txtbasicamount.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtbasicamount.Text);
                objlapp.Loan_amount_Required=(txtlar.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtlar.Text);
                objlapp.Eligibility_Amount=(txtelgamount.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtelgamount.Text);
               // objlapp.Rate_Of_Interest=(txtroi.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtroi.Text);
                objlapp.Status=ddlstatus.SelectedItem.Text;
                objlapp.Created_By = Session["User_Name"].ToString();
                objlapp.Created_Date = System.DateTime.Now;
                objlapp.Creation_Company = Session["ClientRegistrationNo"].ToString();
                objlapp.Modified_By = Session["User_Name"].ToString();
                objlapp.Modified_Date = System.DateTime.Now;
                objlapp.Parameter = 1;
                if (objlapp.Insert_HRMS_Loan_Application() != 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);
                    ClearFields();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
                }

            }
            catch (Exception ex)
            {

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            { }
        }
       //// public void Edit_HeaderDetails(string additioncode, string CreationCompany)
       // {


       //     try
       //     {
       //         var listo = (from leave in ObjDAL.HRMS_Leave_Applications
       //                      where leave.Id == additioncode && leave.Creation_Company == CreationCompany
       //                      select leave).ToList();
       //         if (listo.Count > 0)
       //         {
                    
       //             txtEmpCode.Text = listo[0].Emp_Code;
       //             txtEmpName.Text = listo[0].Emp_Name;
       //             txtDepartment.Text = listo[0].Department;
       //             txtDesignation.Text = listo[0].Designation;
       //             txtReportingAuthority.Text = listo[0].Reporting_Authority;
       //             ddlLoanApply.SelectedValue = listo[0].Leave_Code;
                  

       //         }
       //     }
       //     catch (Exception ex)
       //     {
       //         string msg = ex.Message.ToString();
       //     }


       // }

        //Updating Method

        public void bindemp()
        {
            var Emp_List = (from liste in ObjDAL.USP_gettingemployeedetailsinleave(Convert.ToInt32(Session["Emp_Code"])) select liste).ToList();
                
            if (Emp_List.Count > 0)
            {
                txtEmpCode.Text = Convert.ToString(Emp_List[0].empid);
                txtEmpName.Text = Emp_List[0].empname;
                txtDepartment.Text = Emp_List[0].userrole;
                txtDesignation.Text = Emp_List[0].usersubrole;
               // txtReportingAuthority.Text = Emp_List[0].Reporting_Authority;

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Emp Code is Not Valid');", true);
                txtEmpCode.Text = "";
                txtEmpCode.Focus();
                ddlEmpName.SelectedValue = "0";
                txtDepartment.Text = "";
                txtDesignation.Text = "";
                txtReportingAuthority.Text = "";

            }

        }
        public void Update_Method()
        {
            try
            {
                objLeaveApp.EmpCode = txtEmpCode.Text;
                objLeaveApp.ReportingAuthority = txtReportingAuthority.Text;
                objLeaveApp.TypeOfLeaveApple = ddlLoanApply.SelectedItem.Text;
                objLeaveApp.CreatedBy = Session["User_Name"].ToString();
                objLeaveApp.CreatedDate = System.DateTime.Now;
                objLeaveApp.CreationCompany = Session["ClientRegistrationNo"].ToString();
                objLeaveApp.ModifiedBy = Session["User_Name"].ToString();
                objLeaveApp.ModifiedDate = System.DateTime.Now;
                objLeaveApp.Parameter = 2;
                if (objLeaveApp.Insert_HRMS_Leave_Application() != 0)
                {

                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);
                    ClearFields();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            { }
        }

        public void loanbind()
        {
            var objHeaderInfo = (from header in ObjDAL.HRMS_Loan_Masters
                                 orderby header.Sno descending
                                 select header).ToList();

            if (objHeaderInfo.Count > 0)
            {
                ddlLoanApply.DataTextField = "Short_Name";
                ddlLoanApply.DataValueField = "Loan_Code";
                ddlLoanApply.DataSource = objHeaderInfo;
                ddlLoanApply.DataBind();
                ddlLoanApply.Items.Insert(0, new ListItem("--Select--", "Select"));

            }
            else
            {
                ddlLoanApply.Items.Insert(0, new ListItem("--Select--", "Select"));
            }
        }
  
        protected void ddlEmpName_SelectedIndexChanged(object sender, EventArgs e)
        {
            string CreationCompany = (string)Session["ClientRegistrationNo"].ToString();
            string Emp_Code = ddlEmpName.SelectedItem.Text;
            var objEmPCodeDUP = (from itmes in ObjDAL.HRMS_Employee_Master_PersonalDetails
                                 where itmes.Creation_Company == CreationCompany && itmes.Emp_Code.ToLower().Trim() == Emp_Code
                                 select new
                                 {
                                     Emp_name = itmes.Emp_Last_Name + " " + itmes.Emp_First_Name + " " + itmes.Emp_Middle_Name,
                                     Emp_Code = itmes.Emp_Code
                                 }).ToList();
            if (objEmPCodeDUP.Count > 0)
            {
                ddlEmpName.SelectedItem.Text = Emp_Code;
                txtEmpCode.Text = ddlEmpName.SelectedItem.Text;

                var s = (from x in ObjDAL.HRMS_Employee_Master_Official_Informations
                         where x.Emp_Code == Emp_Code && x.Creation_Company == CreationCompany
                         select x).ToList();
                if (s.Count > 0)
                {
                    txtDepartment.Text = s[0].Department;
                    txtDesignation.Text = s[0].Designation;
                    txtReportingAuthority.Text = s[0].Reporting_Authority;
                    ddlLoanApply.Focus();
                }
                else
                {
                    txtEmpCode.Text = "";
                    txtDepartment.Text = "";
                    txtDesignation.Text = "";
                    txtReportingAuthority.Text = "";
                    ddlEmpName.Focus();
                    ddlEmpName.SelectedIndex = 0;
                }
            }
            else
            {
                txtEmpCode.Text = "";
                txtEmpCode.Focus();
                ddlEmpName.SelectedValue = "0";
                txtDepartment.Text = "";
                txtDesignation.Text = "";
                txtReportingAuthority.Text = "";
            }
        }

        public void eligibilityamount()
        {
             string CreationCompany = Session["ClientRegistrationNo"].ToString();
               

                var basicamt = (from amount in ObjDAL.HRMSEmployeeMasterSalaryDetails
                                   where amount.EmpID == txtEmpCode.Text
                                   select amount.BasicAmount).FirstOrDefault();

                txtbasicamount.Text = basicamt.ToString();
                decimal basicamount =Convert.ToDecimal(basicamt);

                if (ddlLoanApply.SelectedItem.Text == "HBA")
            {

                if (basicamount <= 26600)
                {
                    double i = 72;
                    double hba = (Math.Round(Convert.ToDouble(txtbasicamount.Text) * Convert.ToDouble(i)));
                    if (hba <= 1000000)
                    {
                        txtelgamount.Text = hba.ToString();
                        
                    }
                    else
                    {
                        txtelgamount.Text = "1000000.00";
                    }

                }
                else if (basicamount >= 26600 && basicamount <= 42490)
                {
                    double i = 72;
                    double hba = (Math.Round(Convert.ToDouble(txtbasicamount.Text) * Convert.ToDouble(i)));
                    if (hba <= 1230000)
                    {

                        txtelgamount.Text = hba.ToString();
                    }
                    else
                    {

                        txtelgamount.Text = "1230000.00";
                    }

                }
                else if (basicamount >= 42490 && basicamount <= 61450)
                {
                    double i = 72;
                    double hba = (Math.Round(Convert.ToDouble(txtbasicamount.Text) * Convert.ToDouble(i)));
                    if (hba <= 1500000)
                    {

                        txtelgamount.Text = hba.ToString();
                    }
                    else
                    {
                        txtelgamount.Text = "1500000.00";
                    }

                }
                else if (basicamount >= 61450)
                {
                    double i = 72;
                    double hba = (Math.Round(Convert.ToDouble(txtbasicamount.Text) * Convert.ToDouble(i)));
                    if (hba <= 2000000)
                    {
                        txtelgamount.Text = hba.ToString();
                    }
                    else
                    {
                        txtelgamount.Text = "2000000.00";
                    }

                }
            }
                else if (ddlLoanApply.SelectedItem.Text == "MCA")
            {
                if (basicamount >= 37100)
                {
                    double i = 15;
                    double mca = (Math.Round(Convert.ToDouble(txtbasicamount.Text) * Convert.ToDouble(i)));
                    if (mca <= 600000)
                    {
                        txtelgamount.Text = mca.ToString();
                    }
                    else
                    {
                        txtelgamount.Text = "600000.00";
                    }
                }
            }
            else if (ddlLoanApply.SelectedItem.Text == "MA")
            {

                double i = 15;
                double ma = (Math.Round(Convert.ToDouble(txtbasicamount.Text) * Convert.ToDouble(i)));
                    if (ma <= 600000)
                    {
                        txtelgamount.Text = ma.ToString();
                    }
                    else
                    {
                        txtelgamount.Text = "75000.00";
                    }
            }
            else if (ddlLoanApply.SelectedItem.Text == "MC")
            {
                if (basicamount >= 22460)
                {

                    double i = 7;
                    double mc = (Math.Round(Convert.ToDouble(txtbasicamount.Text) * Convert.ToDouble(i)));
                    if (mc <= 600000)
                    {
                        txtelgamount.Text = mc.ToString();
                    }
                    else
                    {
                        txtelgamount.Text = "80000.00";
                    }
                }
            }
            else if (ddlLoanApply.SelectedItem.Text == "MPA")
            {
                if (basicamount >= 16400)
                {

                    double i = 7;
                    double mc = (Math.Round(Convert.ToDouble(txtbasicamount.Text) * Convert.ToDouble(i)));
                    if (mc <= 600000)
                    {
                        txtelgamount.Text = mc.ToString();
                    }
                    else
                    {
                        txtelgamount.Text = "35000.00";
                    }

                }



            }
            else if (ddlLoanApply.SelectedItem.Text == "BA")
            {
                txtelgamount.Text = "10000.00";
                //if (basicamount >= 37100)
                //{
                //    double i = 15;
                //    double mca = (Math.Round(Convert.ToDouble(txtbasicamount.Text) * Convert.ToDouble(i)));
                //    if (mca <= 600000)
                //    {
                //        txtelgamount.Text = mca.ToString();
                //    }
                //    else
                //    {
                //        txtelgamount.Text = "600000.00";
                //    }


                //}
            }
            else if (ddlLoanApply.SelectedItem.Text == "EA")
            {
                txtelgamount.Text = "7500.00";
                //if (basicamount >= 37100)
                //{
                //    double i = 15;
                //    double mca = (Math.Round(Convert.ToDouble(txtbasicamount.Text) * Convert.ToDouble(i)));
                //    if (mca <= 600000)
                //    {
                //        txtelgamount.Text = mca.ToString();
                //    }
                //    else
                //    {
                //        txtelgamount.Text = "600000.00";
                //    }


                //}


            }
            else if (ddlLoanApply.SelectedItem.Text == "PCA")
            {

                if (basicamount >= 16400)
                {

                    txtelgamount.Text = "50000.00";

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Your Not eligible for this loan');", true);
                }


            }
            else if (ddlLoanApply.SelectedItem.Text == "FA")
            {
                if (basicamount <= 26600 && basicamount >= 77030)
                {
                    txtelgamount.Text = "7500.00";
                    //if (basicamount >= 37100)
                    //{
                    //    double i = 15;
                    //    double mca = (Math.Round(Convert.ToDouble(txtbasicamount.Text) * Convert.ToDouble(i)));
                    //    if (mca <= 600000)
                    //    {
                    //        txtelgamount.Text = mca.ToString();
                    //    }
                    //    else
                    //    {
                    //        txtelgamount.Text = "600000.00";
                    //    }

                }
                else
                {
                    txtelgamount.Text = "5000.00";
                }
                //}


            }
            else if (ddlLoanApply.SelectedItem.Text == "SFA")
            {
                txtelgamount.Text = "7500.00";
                //if (basicamount >= 37100)
                //{
                //    double i = 15;
                //    double mca = (Math.Round(Convert.ToDouble(txtbasicamount.Text) * Convert.ToDouble(i)));
                //    if (mca <= 600000)
                //    {
                //        txtelgamount.Text = mca.ToString();
                //    }
                //    else
                //    {
                //        txtelgamount.Text = "600000.00";
                //    }


                //}


            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Plz select corect options');", true);
            }
            //txtelgamount.Text = "1000000.00";
            //double i=72;
            //double hra = (Math.Round(Convert.ToDouble(txtbasicamount.Text) * Convert.ToDouble(i)));
            
            
            
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            Saving();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Update_Method();
        }

        protected void ddlLeaveApply_SelectedIndexChanged(object sender, EventArgs e)
        {
            eligibilityamount();
        }


    }
}