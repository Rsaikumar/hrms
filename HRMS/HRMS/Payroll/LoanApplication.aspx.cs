﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BAL;
using DAL;
using System.Data.Linq.SqlClient;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using System.IO;

namespace HRMS.Payroll
{
    public partial class LoanApplication : System.Web.UI.Page
    {
        #region Declarations

        LoanapplicationBAL objlapp = new LoanapplicationBAL();
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();
        HRMSLoanMaster objLoanapp = new HRMSLoanMaster();

        #endregion

        #region Methods

        public void loantypebind()
        {
            var objbindleavegrid = (from ST in ObjDAL.HRMSNewLoanMasters
                                    select ST).ToList();
            if (objbindleavegrid.Count() >= 0)
            {
                gdvdifeloantype.DataSource = objbindleavegrid;
                gdvdifeloantype.DataBind();
            }

            else
            {
                gdvdifeloantype.DataSource = objbindleavegrid;
                gdvdifeloantype.DataBind();
            }
        }





        public void approveloandetails()
        {
            int empcode = Convert.ToInt32(Session["Emp_Code"].ToString());

            var approve = (from item in ObjDAL.Usp_GettingEmployeeDetailsLoanDashboard(empcode)
                           select item).ToList();
            if (approve.Count > 0)
            {
                gdvloanapprove.DataSource = approve;
                gdvloanapprove.DataBind();
            }
            else
            {
                gdvloanapprove.DataSource = approve;
                gdvloanapprove.DataBind();
            }
        }

        public void eligibilityamount()
        {
            string CreationCompany = Session["ClientRegistrationNo"].ToString();



            decimal basicamount = Convert.ToDecimal(txtbasicamount.Text);

            if (ddlLoanApply.SelectedItem.Text == "HBA")
            {

                if (basicamount <= 26600)
                {
                    double i = 72;
                    double hba = (Math.Round(Convert.ToDouble(txtbasicamount.Text) * Convert.ToDouble(i)));
                    if (hba <= 1000000)
                    {
                        txtelgamount.Text = hba.ToString();

                    }
                    else
                    {
                        txtelgamount.Text = "1000000.00";
                    }

                }
                else if (basicamount >= 26600 && basicamount <= 42490)
                {
                    double i = 72;
                    double hba = (Math.Round(Convert.ToDouble(txtbasicamount.Text) * Convert.ToDouble(i)));
                    if (hba <= 1230000)
                    {

                        txtelgamount.Text = hba.ToString();
                    }
                    else
                    {

                        txtelgamount.Text = "1230000.00";
                    }

                }
                else if (basicamount >= 42490 && basicamount <= 61450)
                {
                    double i = 72;
                    double hba = (Math.Round(Convert.ToDouble(txtbasicamount.Text) * Convert.ToDouble(i)));
                    if (hba <= 1500000)
                    {

                        txtelgamount.Text = hba.ToString();
                    }
                    else
                    {
                        txtelgamount.Text = "1500000.00";
                    }

                }
                else if (basicamount >= 61450)
                {
                    double i = 72;
                    double hba = (Math.Round(Convert.ToDouble(txtbasicamount.Text) * Convert.ToDouble(i)));
                    if (hba <= 2000000)
                    {
                        txtelgamount.Text = hba.ToString();
                    }
                    else
                    {
                        txtelgamount.Text = "2000000.00";
                    }

                }
            }
            else if (ddlLoanApply.SelectedItem.Text == "MCA")
            {
                if (basicamount >= 37100)
                {
                    double i = 15;
                    double mca = (Math.Round(Convert.ToDouble(txtbasicamount.Text) * Convert.ToDouble(i)));
                    if (mca <= 600000)
                    {
                        txtelgamount.Text = mca.ToString();
                    }
                    else
                    {
                        txtelgamount.Text = "600000.00";
                    }
                }
            }
            else if (ddlLoanApply.SelectedItem.Text == "MA")
            {

                double i = 15;
                double ma = (Math.Round(Convert.ToDouble(txtbasicamount.Text) * Convert.ToDouble(i)));
                if (ma <= 600000)
                {
                    txtelgamount.Text = ma.ToString();
                }
                else
                {
                    txtelgamount.Text = "75000.00";
                }
            }
            else if (ddlLoanApply.SelectedItem.Text == "MC")
            {
                if (basicamount >= 22460)
                {

                    double i = 7;
                    double mc = (Math.Round(Convert.ToDouble(txtbasicamount.Text) * Convert.ToDouble(i)));
                    if (mc <= 600000)
                    {
                        txtelgamount.Text = mc.ToString();
                    }
                    else
                    {
                        txtelgamount.Text = "80000.00";
                    }
                }
            }
            else if (ddlLoanApply.SelectedItem.Text == "MPA")
            {
                if (basicamount >= 16400)
                {
                    double i = 7;
                    double mc = (Math.Round(Convert.ToDouble(txtbasicamount.Text) * Convert.ToDouble(i)));
                    if (mc <= 600000)
                    {
                        txtelgamount.Text = mc.ToString();
                    }
                    else
                    {
                        txtelgamount.Text = "35000.00";
                    }
                }
            }
            else if (ddlLoanApply.SelectedItem.Text == "BA")
            {
                txtelgamount.Text = "10000.00";
            }
            else if (ddlLoanApply.SelectedItem.Text == "EA")
            {
                txtelgamount.Text = "7500.00";
            }
            else if (ddlLoanApply.SelectedItem.Text == "PCA")
            {
                if (basicamount >= 16400)
                {
                    txtelgamount.Text = "50000.00";
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Your Not eligible for this loan');", true);
                }
            }
            else if (ddlLoanApply.SelectedItem.Text == "FA")
            {
                if (basicamount <= 26600 && basicamount >= 77030)
                {
                    txtelgamount.Text = "7500.00";

                }
                else
                {
                    txtelgamount.Text = "5000.00";
                }
            }
            else if (ddlLoanApply.SelectedItem.Text == "SFA")
            {
                txtelgamount.Text = "7500.00";
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Plz select corect options');", true);
            }
        }

        public void authoritybind()
        {
            try
            {
                var reporting = (from item in ObjDAL.USP_gettingemployeenameandid(Convert.ToInt32(Session["ReportingAuthority"]))
                                 select item).ToList();

                txtreportingauthority.Text = reporting[0].empname;

            }
            catch (Exception)
            {

            }
        }

        public void bindemp()
        {
            var Emp_List = (from liste in ObjDAL.USP_gettingemployeedetailsinleave(Convert.ToInt32(Session["Emp_Code"])) select liste).ToList();

            if (Emp_List.Count > 0)
            {
                txtEmpCode.Text = Convert.ToString(Emp_List[0].empid);
                ddlempname.SelectedValue = Emp_List[0].empname;
                txtDepartment.Text = Emp_List[0].userrole;
                txtDesignation.Text = Emp_List[0].usersubrole;

                var basicamt = (from amount in ObjDAL.HRMSEmployeeMasterSalaryDetails
                                where amount.ID == Convert.ToInt32(Emp_List[0].empid)
                                select amount.BasicAmount).FirstOrDefault();

                txtbasicamount.Text = basicamt.ToString();
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Emp Code is Not Valid');", true);
                txtEmpCode.Text = "";
                txtEmpCode.Focus();
                ddlempname.SelectedValue = "0";
                txtDepartment.Text = "";
                txtDesignation.Text = "";
            }
        }

        private void Saving()
        {
            try
            {
                objlapp.EmpCode = txtEmpCode.Text;
                objlapp.ReportingAuthority = Convert.ToString(Session["ReportingAuthority"]);
                objlapp.TypeOfLoanApply = ddlLoanApply.SelectedItem.Text;
                objlapp.BasicAmount = (txtbasicamount.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtbasicamount.Text);
                objlapp.LoanAmountRequired = (txtlar.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtlar.Text);
                objlapp.EligibilityAmount = (txtelgamount.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtelgamount.Text);
                objlapp.RateOfInterest = (txtroi.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtroi.Text);
                objlapp.AppliedDate = System.DateTime.Now;
                objlapp.Status = "Waiting For Approval";
                objlapp.CreatedBy = Session["Emp_Code"].ToString();
                objlapp.CreatedDate = System.DateTime.Now;
                objlapp.CreationCompany = Session["ClientRegistrationNo"].ToString();
                objlapp.ModifiedBy = Session["Emp_Code"].ToString();
                objlapp.ModifiedDate = System.DateTime.Now;

                objlapp.Tenure = Convert.ToInt32(ddltenure.SelectedItem.Text);
                objlapp.Parameter = 1;
                if (objlapp.InsertHRMSLoanApplication() != 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Loan Applied sucessfully');window.location ='../../Payroll/LoanApplication.aspx';", true);
                    ClearFields();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Loan Not Applied ');", true);
                }
            }
            catch (Exception ex)
            {

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            {
            }
        }

        public void ddlloanbind()
        {
            try
            {
                var master = (from item in ObjDAL.HRMSNewLoanMasters
                              select item).ToList();

                if (master.Count > 0)
                {
                    ddlLoanApply.DataSource = master;
                    ddlLoanApply.DataTextField = "ShortName";
                    ddlLoanApply.DataValueField = "ID";
                    ddlLoanApply.DataBind();
                    ddlLoanApply.Items.Insert(0, new ListItem("--Select--", "0"));
                }
            }
            catch (Exception)
            {

            }
        }

        public void ClearFields()
        {
            txtEmpCode.Text = "";

            txtDepartment.Text = "";
            txtDesignation.Text = "";
            ddlempname.SelectedIndex = 0;
            txtlar.Text = string.Empty;
            ddltenure.Text = string.Empty;
            txtroi.Text = string.Empty;
        }

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlloanbind();
                //authoritybind();
               // bindemp();
                loantypebind();
                employeenamebind();
                approveloandetails();
                divelgibityhide.Visible = false;
            }
            txtEmpCode.Visible = false;
            lblempcode.Visible = false;
            txtroi.ReadOnly = true;

            if ((string)Session["Leave_code"] != null)
            {
                string ClientRegistrationNo = Session["ClientRegistrationNo"].ToString();
                string AdditionCode = (string)Session["Leave_code"];
                Session.Remove("Leave_code");
                Edit_HeaderDetails(AdditionCode, ClientRegistrationNo);
            }
            else
            {

            }
        }

        private HRMSNewLoanMaster leavedata(string LoanType)
        {
            var loandetails = (from lm in ObjDAL.HRMSNewLoanMasters
                               where lm.ShortName == LoanType
                               select lm).FirstOrDefault();
            return loandetails;
        }

        //protected void ddlstatus_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    var status = ddlstatus.SelectedItem.Text;

        //    int empcode = Convert.ToInt32(Session["Emp_Code"].ToString());
        //    var approve = (from item in ObjDAL.Usp_GettingEmployeeDetailsLoanDashboard(empcode)
        //                   where item.Loanstatus == status
        //                   select item).ToList();
        //    if (approve.Count > 0)
        //    {
        //        gdvloanapprove.DataSource = approve;
        //        gdvloanapprove.DataBind();
        //    }

        //    else
        //    {
        //        gdvloanapprove.DataSource = approve;
        //        gdvloanapprove.DataBind();
        //    }

        //}


        protected void btnSave_Click(object sender, EventArgs e)
        {

            if (ddlLoanApply.SelectedItem.Text == "HBA")
            {
                if (Convert.ToInt32(txtlar.Text) <= Convert.ToDecimal(txtelgamount.Text))
                {

                    Saving();
                }
                else
                {
                    txtlar.Text = "";
                    txtroi.Text = "";
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('HBA loan Not apllicabale for More Than Elgibility Amount');", true);
                }
            }
            else if (ddlLoanApply.SelectedItem.Text == "MCA")
            {
                if (Convert.ToInt32(txtlar.Text) <= Convert.ToDecimal(txtelgamount.Text))
                {

                    Saving();
                }
                else
                {
                    txtlar.Text = "";
                    txtroi.Text = "";
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('MCA loan Not apllicabale for More Than Elgibility Amount');", true);
                }
            }
            else if (ddlLoanApply.SelectedItem.Text == "MA")
            {
                if (Convert.ToInt32(txtlar.Text) <= Convert.ToDecimal(txtelgamount.Text))
                {

                    Saving();
                }
                else
                {
                    txtlar.Text = "";
                    txtroi.Text = "";
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('MA loan Not apllicabale for More Than Elgibility Amount');", true);
                }
            }
            else if (ddlLoanApply.SelectedItem.Text == "EA")
            {
                if (Convert.ToInt32(txtlar.Text) <= Convert.ToDecimal(txtelgamount.Text))
                {

                    Saving();
                }
                else
                {
                    txtlar.Text = "";
                    txtroi.Text = "";
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('EA loan Not apllicabale for More Than Elgibility Amount');", true);
                }
            }
            else if (ddlLoanApply.SelectedItem.Text == "FA")
            {
                if (Convert.ToInt32(txtlar.Text) <= Convert.ToDecimal(txtelgamount.Text))
                {

                    Saving();
                }
                else
                {
                    txtlar.Text = "";
                    txtroi.Text = "";
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('FA loan Not apllicabale for More Than Elgibility Amount');", true);
                }
            }
            else if (ddlLoanApply.SelectedItem.Text == "MC")
            {
                if (Convert.ToInt32(txtlar.Text) <= Convert.ToDecimal(txtelgamount.Text))
                {

                    Saving();
                }
                else
                {
                    txtlar.Text = "";
                    txtroi.Text = "";
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('MC loan Not apllicabale for More Than Elgibility Amount');", true);
                }
            }
            else if (ddlLoanApply.SelectedItem.Text == "MPA")
            {
                if (Convert.ToInt32(txtlar.Text) <= Convert.ToDecimal(txtelgamount.Text))
                {

                    Saving();
                }
                else
                {
                    txtlar.Text = "";
                    txtroi.Text = "";
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('MPA loan Not apllicabale for More Than Elgibility Amount');", true);
                }
            }
            else if (ddlLoanApply.SelectedItem.Text == "PCA")
            {
                if (Convert.ToInt32(txtlar.Text) <= Convert.ToDecimal(txtelgamount.Text))
                {

                    Saving();
                }
                else
                {
                    txtlar.Text = "";
                    txtroi.Text = "";
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('PCA loan Not apllicabale for More Than Elgibility Amount');", true);
                }
            }
            else if (ddlLoanApply.SelectedItem.Text == "SFA")
            {
                if (Convert.ToInt32(txtlar.Text) <= Convert.ToDecimal(txtelgamount.Text))
                {

                    Saving();
                }
                else
                {
                    txtlar.Text = "";
                    txtroi.Text = "";
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('SFA loan Not apllicabale for More Than Elgibility Amount');", true);
                }
            }
        }

        public void Edit_HeaderDetails(string additioncode, string CreationCompany)
        {
            try
            {
                var listo = (from leave in ObjDAL.HRMSLeaveApplications
                             where leave.CreationCompany == CreationCompany
                             select leave).ToList();
                if (listo.Count > 0)
                {

                    txtEmpCode.Text = listo[0].EmpCode;
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }
        }


        public void employeenamebind()
        {
            try
            {

                List<string> master = new List<string>();
                List<string> mastername = new List<string>();
                int empcode = Convert.ToInt32((Session["Emp_Code"].ToString()));

                master = (from item in ObjDAL.HRMSEmployeeMasterRegistrationDetails
                          join per in ObjDAL.HRMSEmplyeeMasterPersonalDetails on item.ID equals Convert.ToInt32(per.EmpId)
                          where item.CreatedBy == empcode.ToString()
                          select per.FirstName + " " + per.MiddleName + " " + per.LastName).ToList();
                mastername = (from item in ObjDAL.HRMSEmployeeMasterRegistrationDetails
                              join per in ObjDAL.HRMSEmplyeeMasterPersonalDetails on item.ID equals Convert.ToInt32(per.EmpId)
                              where item.ID == empcode
                              select per.FirstName + " " + per.MiddleName + " " + per.LastName).ToList();

                master.AddRange(mastername);

                if (master != null)
                {
                    ddlempname.DataSource = master;
                    ddlempname.DataBind();
                    ddlempname.Items.Insert(0, new ListItem("--Select--", "Select"));
                }
                else
                {
                    ddlempname.Items.Insert(0, new ListItem("--Select--", "Select"));

                }
            }
            catch (Exception e)
            {

            }
        }

        protected void ddlLoanApply_SelectedIndexChanged(object sender, EventArgs e)
        {
            string CreationCompany = Session["ClientRegistrationNo"].ToString();
            //eligibilityamount();
            var loan = (from lo in ObjDAL.HRMSNewLoanMasters
                        select lo).ToList();

            if (ddlLoanApply.SelectedItem.Value == "1")
            {
                txtelgamount.Text = Convert.ToString(loan[0].EligibilityAmount);
            }
            if (ddlLoanApply.SelectedItem.Value == "2")
            {
                txtelgamount.Text = Convert.ToString(loan[1].EligibilityAmount);
            }
            if (ddlLoanApply.SelectedItem.Value == "4")
            {
                txtelgamount.Text = Convert.ToString(loan[2].EligibilityAmount);
            }
            if (ddlLoanApply.SelectedItem.Value == "6")
            {
                txtelgamount.Text = Convert.ToString(loan[3].EligibilityAmount);
            }
            if (ddlLoanApply.SelectedItem.Value == "7")
            {
                txtelgamount.Text = Convert.ToString(loan[4].EligibilityAmount);
            }
            if (ddlLoanApply.SelectedItem.Value == "8")
            {
                txtelgamount.Text = Convert.ToString(loan[5].EligibilityAmount);
            }
            if (ddlLoanApply.SelectedItem.Value == "9")
            {
                txtelgamount.Text = Convert.ToString(loan[6].EligibilityAmount);
            }
            if (ddlLoanApply.SelectedItem.Value == "10")
            {
                txtelgamount.Text = Convert.ToString(loan[7].EligibilityAmount);
            }
            if (ddlLoanApply.SelectedItem.Value == "11")
            {
                txtelgamount.Text = Convert.ToString(loan[8].EligibilityAmount);
            }
            else
            {

            }
        }

        protected void ddltenure_SelectedIndexChanged(object sender, EventArgs e)
        {
            decimal interest = Convert.ToDecimal(txtinterest.Text);
            decimal tenure = Convert.ToDecimal(ddltenure.SelectedItem.Text);
            decimal lar = Convert.ToDecimal(txtlar.Text);
            decimal roi = (interest / tenure) * lar;

            txtroi.Text = roi.ToString(".00");
        }

        protected void gdvloanapprove_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(gdvloanapprove, "Select$" + e.Row.RowIndex);
                    e.Row.ToolTip = "View Loan Details";
                    e.Row.Attributes["style"] = "cursor:pointer";
                }
            }
        }

        protected void gdvloanapprove_SelectedIndexChanged(object sender, EventArgs e)
        {
            {
                string Loanid = gdvloanapprove.SelectedRow.Cells[0].Text;
                string path = "~/Payroll/LoanApproval.aspx";
                Session["LoanApplicationNo"] = Loanid;
                Response.Redirect(path);
            }
        }

        #endregion


        protected void ddlempname_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name = ddlempname.SelectedItem.Text;
            var empcode = (from per in ObjDAL.HRMSEmplyeeMasterPersonalDetails
                           join rep in ObjDAL.HRMSEmployeeMasterOfficialInformationDetails on per.EmpId equals rep.EmpId
                           where per.FirstName + " " + per.MiddleName + " " + per.LastName == name
                           select per.EmpId).FirstOrDefault();

            var Emp_List = (from liste in ObjDAL.USP_gettingemployeedetailsinleave(Convert.ToInt32(empcode)) select liste).FirstOrDefault();

            var Emp_ReportingAuthorityName = (from rep in ObjDAL.HRMSEmplyeeMasterPersonalDetails
                                              where rep.EmpId == Emp_List.ReportingAuthority
                                              select rep.FirstName + " " + rep.MiddleName + " " + rep.LastName).FirstOrDefault();

            var basicamt = (from amount in ObjDAL.HRMSEmployeeMasterSalaryDetails
                            join per in ObjDAL.HRMSEmplyeeMasterPersonalDetails
                          on amount.EmpId equals per.EmpId
                            select amount.BasicAmount).FirstOrDefault();

         
            if (Emp_List != null)
            {
                txtEmpCode.Text = Convert.ToString(Emp_List.empid);
                ddlempname.SelectedItem.Text = Emp_List.empname;
                txtDepartment.Text = Emp_List.userrole;
                txtDesignation.Text = Emp_List.usersubrole;
                txtreportingauthority.Text = Emp_ReportingAuthorityName;
                txtbasicamount.Text = basicamt.ToString();

            }

            else
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Emp Code is Not Valid');", true);

                txtEmpCode.Text = "";
                txtEmpCode.Focus();
                txtDepartment.Text = "";
                txtDesignation.Text = "";
                txtreportingauthority.Text = "";
            }
        }
    }
}