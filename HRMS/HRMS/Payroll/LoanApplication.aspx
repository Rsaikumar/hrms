﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="LoanApplication.aspx.cs" Inherits="HRMS.Payroll.LoanApplication" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function Validation() {

            if (document.getElementById('<%=ddlLoanApply.ClientID%>').options[document.getElementById('<%=ddlLoanApply.ClientID%>').selectedIndex].value == "0") {
                alert("Please Select loan Type..");
                document.getElementById('<%=ddlLoanApply.ClientID%>').focus();
                return false;

            }
            return true;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i> Dashboard</a> </li>
                        <li><a href="../Payroll/LoanApplication.aspx" class="current"> Loan Application</a> </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading tab-bg-dark-navy-blue ">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a data-toggle="tab" href="#loanapplication" aria-expanded="false"><i class="fa fa-user"> Loan Application</i></a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#Myrequests" aria-expanded="true"><i class="fa fa-user"> My Requests</i></a>
                                </li>
                            </ul>
                        </header>
                        <div class="panel-body">
                            <div class="tab-content">
                                <div id="loanapplication" class="tab-pane active">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <section class="panel">
                                                    <header class="panel-heading">
                                                        <i class="fa fa-th-list"> Loan Application</i>
                                                    </header>
                                                    <div class="panel-body">
                                                        <div class="form-group col-md-6" visible="false">
                                                            <label for="textfield" class="control-label col-sm-5" style="" id="lblempcode" runat="server">
                                                                Emp Code
                                                            </label>
                                                            <div class="col-sm-7" style="">
                                                                <asp:TextBox ID="txtEmpCode" runat="server" placeholder="Emp Code" MaxLength="50"
                                                                    class="form-control" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-4" style="">
                                                              Name</label>
                                                            <div class="col-sm-8" style="" id="">
                                                                <asp:DropDownList ID="ddlempname" runat="server"  class="form-control"
                                                                   OnSelectedIndexChanged="ddlempname_SelectedIndexChanged" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-4" style="">
                                                                Department</label>
                                                            <div class="col-sm-8" style="" id="Div1">
                                                                <asp:TextBox ID="txtDepartment" runat="server" placeholder="Department" class="form-control"
                                                                    onkeydown="return jsDecimals(event);" MaxLength="50" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-4" style="">
                                                                Designation</label>
                                                            <div class="col-sm-8" style="" id="Div2">
                                                                <asp:TextBox ID="txtDesignation" runat="server" placeholder="Designation" class="form-control"
                                                                    onkeydown="return jsDecimals(event);" MaxLength="50" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-4" style="">
                                                                Reporting<span id="Span5" runat="server" style="color: Red; display: inline;">*</span></label>
                                                            <div class="col-sm-8" style="" id="Div3">
                                                                <asp:TextBox ID="txtreportingauthority" runat="server" placeholder="Reporting Authority Name" class="form-control"
                                                                    onkeydown="return jsDecimals(event);" MaxLength="50" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                <section class="panel">
                                                    <div class="panel-body">
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-4" style="">
                                                               Type<span style="color: Red; display: inline;">*</span></label>
                                                            <div class="col-sm-8" style="" id="Div4">
                                                                <asp:DropDownList ID="ddlLoanApply" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="ddlLoanApply_SelectedIndexChanged" runat="server"></asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-4" style="">
                                                                Basic Amount<span style="color: #ff0000; font-size: 14px; display: inline;">*</span>
                                                            </label>
                                                            <div class="col-sm-8" style="">
                                                                <asp:TextBox ID="txtbasicamount" runat="server" placeholder="Basic Amount" class="form-control"
                                                                    MaxLength="50" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-4" style="">
                                                               Amount Required
                                                            </label>
                                                            <div class="col-sm-8" style="">
                                                                <asp:TextBox ID="txtlar" runat="server" placeholder="Amount Required" MaxLength="50"
                                                                    class="form-control" ReadOnly="false"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-4" style="" id="Label1">
                                                                Interest%<span runat="server" style="color: Red; display: inline;">*</span></label>
                                                            <div class="col-sm-8" style="" id="Div10">
                                                                <asp:TextBox ID="txtinterest" runat="server" CssClass="form-control" Text="6.5" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-4" style="" id="lbltenure">
                                                                Tenure<span id="Span6" runat="server" style="color: Red; display: inline;">*</span></label>
                                                            <div class="col-sm-8" style="" id="Div5">
                                                                <asp:DropDownList ID="ddltenure" runat="server" class="form-control"
                                                                    AutoPostBack="true" OnSelectedIndexChanged="ddltenure_SelectedIndexChanged">
                                                                    <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                                    <asp:ListItem Value="1">3</asp:ListItem>
                                                                    <asp:ListItem Value="2">6</asp:ListItem>
                                                                    <asp:ListItem Value="3">9</asp:ListItem>
                                                                    <asp:ListItem Value="4">12</asp:ListItem>
                                                                    <asp:ListItem Value="5">15</asp:ListItem>
                                                                    <asp:ListItem Value="6">18</asp:ListItem>
                                                                    <asp:ListItem Value="7">21</asp:ListItem>
                                                                    <asp:ListItem Value="8">24</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-4" style="">
                                                                Rate Of Intrest<span style="color: #ff0000; font-size: 14px; display: inline;">*</span></label>
                                                            <div class="col-sm-8" style="" id="Div7">
                                                                <asp:TextBox ID="txtroi" runat="server" TabIndex="1" placeholder="Rate Of Intrest" class="form-control"
                                                                    MaxLength="50"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6" id="divelgibityhide" runat="server">
                                                            <label for="textfield" class="control-label col-sm-4" style="">
                                                                Eligibility Amount<span id="Span1" runat="server" style="color: Red; display: inline;">*</span></label>
                                                            <div class="col-sm-8" style="" id="Div6">
                                                                <asp:TextBox ID="txtelgamount" runat="server" TabIndex="0" placeholder="Eligibility Amount" class="form-control"
                                                                    MaxLength="50" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 text-center">
                                                            <asp:Button ID="btnSave" runat="server" Text="Submit" class="btn btn-primary" OnClick="btnSave_Click" OnClientClick="return  Validation() " /><%--OnClientClick="return fileUploadValidation()"--%>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="col-md-12">
                                                    <div class="panel">
                                                        <header class="panel-heading">
                                                            <i class="fa fa-user padd-r-1"> Loan Types</i>
                                                        </header>
                                                        <div class="panel-body">
                                                            <div style="max-height: 515px; overflow: auto;" class="table-responsive">
                                                                <asp:GridView ID="gdvdifeloantype" CssClass="mydatagrid" HeaderStyle-CssClass="headergrid" RowStyle-CssClass="rows"
                                                                    runat="server" Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%;" EmptyDataText="No records Found" AutoGenerateColumns="false">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="LoanName" HeaderText="Loan Name" ItemStyle-Width="150">
                                                                            <ItemStyle Width="150px"></ItemStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="ShortName" HeaderText="Short Name" ItemStyle-Width="150">
                                                                            <ItemStyle Width="150px"></ItemStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="EligibilityAmount" HeaderText="Eligibility Amount" ItemStyle-Width="150">
                                                                            <ItemStyle Width="150px"></ItemStyle>
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="Myrequests" class="tab-pane">

                                    <div style="max-height: 515px; overflow: auto" class="table-responsive">
                                        <asp:GridView ID="gdvloanapprove" CssClass="mydatagrid" HeaderStyle-CssClass="headergrid" RowStyle-CssClass="rows"
                                            runat="server" Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%; overflow:scroll;" EmptyDataText="No records Found" AutoGenerateColumns="false" OnRowDataBound="gdvloanapprove_RowDataBound" OnSelectedIndexChanged="gdvloanapprove_SelectedIndexChanged">
                                            <Columns>
                                                <asp:BoundField DataField="ID" HeaderText="Loan No" ItemStyle-Width="200" />
                                                <asp:BoundField DataField="empname" HeaderText="Employee Name" ItemStyle-Width="200" />
                                                <asp:BoundField DataField="loanapply" HeaderText="Loan Type" ItemStyle-Width="200" />
                                                <asp:BoundField DataField="EligibilityAmount" HeaderText="Eligibility Amount" ItemStyle-Width="200" />
                                                <asp:BoundField DataField="requiredammount" HeaderText="TypeOfLoanApply" ItemStyle-Width="200" />
                                                <asp:BoundField DataField="Tenure" HeaderText="Tenure" ItemStyle-Width="200" />
                                                <asp:BoundField DataField="Loanstatus" HeaderText="Status" ItemStyle-Width="200" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
