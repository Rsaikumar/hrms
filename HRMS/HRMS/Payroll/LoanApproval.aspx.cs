﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BAL;
using DAL;

namespace HRMS.Payroll
{
    public partial class LoanApproval : System.Web.UI.Page
    {
        #region declarations

        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();
        LoanapplicationBAL objLoanApp = new LoanapplicationBAL();

        #endregion

        #region events

        protected void Page_Load(object sender, EventArgs e)
        {
            loanapplicationdetails();
            buttonhide();
            authoritybind();
        }

        protected void btnapprove_Click(object sender, EventArgs e)
        {
            Update_Method();
        }

        protected void btnreject_Click(object sender, EventArgs e)
        {
            RejectUpdate_Method();
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Payroll/LoanApplication.aspx");
        }

        #endregion

        #region methods

        public void buttonhide()
        {
            try
            {
                int id = Convert.ToInt32(Session["LoanApplicationNo"]);
                var RA = (from item in objDAL.HRMSLoanApplications
                          where item.ID == id
                          select item).ToList();
                if (RA.Count > 0)
                {
                    string reporting = RA[0].ReportingAuthority;
                    string sess = Session["Emp_Code"].ToString();
                    if (reporting == sess)
                    {
                        btnapprove.Visible = true;
                        btnreject.Visible = true;
                    }
                    else
                    {
                        btnapprove.Visible = false;
                        btnreject.Visible = false;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void loanapplicationdetails()
        {
            var loanapplication = (from master in objDAL.Usp_GettingEmployeeDetailsInLoan(Convert.ToInt32(Session["LoanApplicationNo"].ToString()))
                                   select master).ToList();

            if (loanapplication.Count > 0)

            lblempname.InnerText = loanapplication[0].empname;
            lblloantype.InnerText = loanapplication[0].TypeOfLoanApply;
            lblrateofinterst.InnerText = Convert.ToString(loanapplication[0].RateOfInterest);
            lbltenure.InnerText = Convert.ToString(loanapplication[0].Tenure);
            lblbasicamount.InnerText = Convert.ToString(loanapplication[0].BasicAmount);
            lblstatus.InnerText = loanapplication[0].Status;
            lblamountrequired.InnerText = Convert.ToString(loanapplication[0].requiredammount);
            lbldesignation.InnerText = loanapplication[0].Designation;
            lbldept.InnerText = loanapplication[0].Department;
            lbldate.InnerText = Convert.ToDateTime(loanapplication[0].AppliedDate).ToString("dd-MM-yyyy");
        }

        //Updating Method
        public void Update_Method()
        {
            try
            {
                int id = Convert.ToInt32(Session["LoanApplicationNo"]);
                objLoanApp.ID = id;
                objLoanApp.Status = "Approved";
                objLoanApp.CreatedDate = System.DateTime.Now;
                objLoanApp.ModifiedBy = Session["Emp_Code"].ToString();
                objLoanApp.ModifiedDate = System.DateTime.Now;
                objLoanApp.Parameter = 2;
                if (objLoanApp.InsertHRMSLoanApplication() != 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Loan Approved sucessfully');window.location ='../Payroll/LoanApplication.aspx';", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Loan Not Approved');window.location ='../Payroll/LoanApproval.aspx';", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            { }
        }

        public void authoritybind()
        {
            try
            {
                int id = Convert.ToInt32(Session["LoanApplicationNo"]);
                var RA = (from item in objDAL.HRMSLoanApplications
                          where item.ID == id
                          select item).ToList();
                if (RA.Count > 0)
                {
                    string Empcode = RA[0].EmpCode;

                    var ObjRep = (from item in objDAL.HRMSEmployeeMasterOfficialInformationDetails
                                  where item.ID == Convert.ToInt32(Empcode)
                                  select item).ToList();
                    if (ObjRep.Count > 0)
                    {
                        string repauth = ObjRep[0].ReportingAuthority;

                        var repaut = (from item in objDAL.USP_gettingemployeenameandid(Convert.ToInt32(repauth))
                                      select item).ToList();

                        lblreportingauthority.InnerText = repaut[0].empname;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void RejectUpdate_Method()
        {
            try
            {
                int id = Convert.ToInt32(Session["LoanApplicationNo"]);
                objLoanApp.ID = id;
                objLoanApp.Status = "Reject";
                objLoanApp.CreatedDate = System.DateTime.Now;
                objLoanApp.ModifiedBy = Session["Emp_Code"].ToString();
                objLoanApp.ModifiedDate = System.DateTime.Now;
                objLoanApp.Parameter = 2;
                if (objLoanApp.InsertHRMSLoanApplication() != 0)
                {

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Loan Rejected sucessfully');window.location ='../Dashboard/Dashboard.aspx';", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Loan Not Rejected');window.location ='../Payroll/LoanApproval.aspx';", true);

                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            { }
        }

        #endregion
    }
}