﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BAL;
using DAL;

namespace HRMS.Payroll
{
    public partial class Define_Loan_List : System.Web.UI.Page
    {
        // // // Object Creation For Database
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();
        DefineLeavesBAL objSCM_LeaveBAL = new DefineLeavesBAL();
        DefineLoansBAL objloan = new DefineLoansBAL();
        // // /// Object Creation For Item Group Domain

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ClientRegistrationNo"] == null)
            {
                Response.Redirect("~/Default.aspx");
            }


            Bind_Grid();
        }



        #region Button Events

        // // // Navigation to User Registration Form For Created New User
        protected void btnAdd_Click(object sender, EventArgs e)
        {

            Response.Redirect("~/Payroll/Define_Loan.aspx");

        }

        /// <summary>
        /// Search For UserRegistration Details
        /// </summar
        ///


        /// <summary>
        /// Navigation for User Define Leave create
        /// </summar
        ///
        protected void lkbAddHeader_Click(object sender, EventArgs e)
        {

            Response.Redirect("~/Payroll/Define_Loan.aspx");
        }




        #endregion

        #region Methods

        /// <summary>
        /// Bind Grid Based on User Registration Details
        /// Created By Divya
        /// </summary>
        /// <param name="sender"></param>
        public void Bind_Grid()
        {
            var loan = (from l in objDAL.HRMS_Loan_Masters

                        select l).ToList();

            //var objHeaderInfo = (from header in objDAL.HRMS_Loan_Masters


            // select header).ToList();

            if (loan.Count > 0)
            {
                grdDefineLeaveTypesList.DataSource = loan;
                grdDefineLeaveTypesList.DataBind();
            }
            else
            {


                SetInitialRow();
            }



        }



        /// <summary>
        /// Setting Initial Empty Row Data Into Header Details
        /// </summary>
        /// <param name="sender"></param>
        public void SetInitialRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("Loan_Code", typeof(string)));
            dt.Columns.Add(new DataColumn("Loan_Name", typeof(string)));
            dt.Columns.Add(new DataColumn("Short_Name", typeof(string)));

            dr = dt.NewRow();

            dr["Loan_Code"] = string.Empty;
            dr["Loan_Name"] = string.Empty;
            dr["Short_Name"] = string.Empty;

            dt.Rows.Add(dr);

            grdDefineLeaveTypesList.DataSource = dt;
            grdDefineLeaveTypesList.DataBind();

            int columncount = 7;
            grdDefineLeaveTypesList.Rows[0].Cells.Clear();
            grdDefineLeaveTypesList.Rows[0].Cells.Add(new TableCell());
            grdDefineLeaveTypesList.Rows[0].Cells[0].ColumnSpan = columncount;
            grdDefineLeaveTypesList.Rows[0].Cells[0].Text = "No Records Found";
            // grdStorageLocation.HeaderStyle.HorizontalAlign = "center";
            grdDefineLeaveTypesList.Rows[0].Cells[0].ForeColor = System.Drawing.Color.Red;
        }

        /// <summary>
        /// The Following Method is used for Delete Grid records to Data From User Registration Table
        /// Created By Divya
        /// </summary>
        public void Delete_HeaderDetails(string AdditionCode, string Creation_Company)
        {
            try
            {


            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.ToString() + "');", true);
            }
            finally
            {
            }
        }

        #endregion

        #region User Registration Details Bind For Grid view


        /// <summary>
        /// Row Command Grid view For UserRegistration Details
        /// Created By Divya
        /// </summar
        ///

        protected void grdDefineLeaveTypesList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string Loan_Code = e.CommandArgument.ToString();
            string Cmpy = (string)Session["ClientRegistrationNo"];
            string Brnach = (string)Session["BranchID"];
            string UserType = (string)Session["User_Type"];
            string UserName = (string)Session["User_Name"];
            string Cration_Company = (string)Session["CreationCompany"];
            if (e.CommandName == "Edit")
            {
                string path = "~/Payroll/Define_Loan.aspx";
                Session["Loan_Code"] = Loan_Code;
                Response.Redirect(path);

            }
            if (e.CommandName == "Delete")
            {

                string Creation_Company = Session["ClientRegistrationNo"].ToString();
                // string path = "~/Payroll/Define_Leaves.aspx";
                Session["Loan_Code"] = Loan_Code;
                var objHeaderInfo = (from header in objDAL.HRMS_Loan_Masters
                                     where header.Sno == Convert.ToInt32(Loan_Code)
                                     select header).First();
                objDAL.HRMS_Loan_Masters.DeleteOnSubmit(objHeaderInfo);
                objDAL.SubmitChanges();
                //Delete_HeaderDetails(LeaveCode, Creation_Company);
                Bind_Grid();
                //Response.Redirect(path);
            }


        }


        /// <summary>
        /// Paging For User Registration Details
        /// </summar
        ///
        protected void grdDefineLeaveTypesList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdDefineLeaveTypesList.PageIndex = e.NewPageIndex;
            Bind_Grid();
        }


        /// <summary>
        /// Row Delte Grid view For UserRegistration
        /// </summary>
        ///
        protected void grdDefineLeaveTypesList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        /// <summary>
        /// Row Editing Grid view For UserRegistration
        /// </summary>
        ///

        protected void grdDefineLeaveTypesList_RowEditing(object sender, GridViewEditEventArgs e)
        {


        }

        /// <summary>
        /// Row Sorting Grid view For UserRegistration Details
        /// </summar
        ///

        protected void grdDefineLeaveTypesList_Sorting(object sender, GridViewSortEventArgs e)
        {

        }



        #endregion




    }
}