﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BAL;
using DAL;
using System.Data.Linq.SqlClient;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using System.IO;

namespace HRMS.Payroll
{
    public partial class Leave_Application : System.Web.UI.Page
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();
        LeaveApplicationBAL objLeaveApp = new LeaveApplicationBAL();

        public void noofdays()
        {
            DateTime start = DateTime.ParseExact(txtFromDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            // DateTime start = DateTime.Parse(txtFromDate.Text);
            DateTime finish = DateTime.ParseExact(txtToDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            // DateTime finish = DateTime.Parse(txtToDate.Text);
            if (start <= finish)
            {
                TimeSpan difference = (finish.Subtract(start));
                var NumberOfDays = difference.TotalDays.ToString();
                var nod = Convert.ToInt32(NumberOfDays) + 1;
                txtNoofDays.Text = nod.ToString();
            }
            else
            {
                txtNoofDays.Text = "";
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Plz select from date greaterthan  todate');", true);
            }

        }


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                bindemp();
                //if (Session["User_Type"] == "SuperAdmin")                {
                //    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('You Don't have Permissions To Create..');", true);
                //    Response.Redirect("~/Dashboard/Dashboard.aspx");
                //}
                if (Session["ClientRegistrationNo"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                txtstatus.Text = "Wating For Approval";
                //  btnUpdate.Visible = false;
                txtEmpCode.Focus();

                ddlauthoritybind();
                //Reporting Authority Binding

                BalanceAva();
                //Employee Name Binding
                //  Emp_NameBinding();

                //Leave Types Bind
                //Bind_Leave();
                ddlleavebind();
                // Edit_HeaderDetails(Session["User_Name"].ToString(), Session["ClientRegistrationNo"].ToString());
                //  txtToDate.Text = DateTime.Now.ToString();
                dtholidays = GetPublicHolidays();
                if ((string)Session["Leave_code"] != null)
                {

                    //  Request.QueryString.Clear();
                    string ClientRegistrationNo = Session["ClientRegistrationNo"].ToString();
                    string AdditionCode = (string)Session["Leave_code"];
                    Session.Remove("Leave_code");
                    //string LoginID = Request.QueryString["id"];
                    Edit_HeaderDetails(AdditionCode, ClientRegistrationNo);
                }
                else
                {
                    btnUpdate.Visible = false;
                }


            }
            //if (IsPostBack && fupCertification.PostedFile != null)
            //{
            //    if (fupCertification.PostedFile.FileName.Length > 0)
            //    {
            //        fupCertification.SaveAs(Server.MapPath("~/Certifications/") + fupCertification.PostedFile.FileName);
            //        ViewState["File"]= "~/ Certifications / " + fupCertification.PostedFile.FileName;
            //        //imguser.ImageUrl = "~/Images/" + fupCertification.PostedFile.FileName;
            //    }
            //}

        }


        #region Events

        /// <summary>
        /// Text Change Event For From Date Caluculation  Days
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtFromDate_OnTextChanged(object sender, EventArgs e)
        {
            //    if ( actdate.SelectedDate <=System.DateTime.Now)
            //    {


            //    }
            //    else
            //    {
            //        txtFromDate.Text = "";
            //        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('selected date should be greter than present date');", true);
            //    }

            //NoOfDaysCal();
            txtToDate.Focus();

        }

        /// <summary>
        /// Text Change Event for To date Calculation for Days
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtToDate_OnTextChanged(object sender, EventArgs e)
        {

            noofdays();



            // NoOfDaysCal();


        }
        List<DateTime> dtholidays = null;
        protected void actdate_DayRender(object sender, DayRenderEventArgs e)
        {
            if (dtholidays.Contains(e.Day.Date))
            {
                e.Day.IsSelectable = false;
            }
        }

        private List<DateTime> GetPublicHolidays()
        {
            List<DateTime> list = new List<DateTime>();
            //populate from database or other sources
            list.Add(new DateTime(2016, 01, 14));
            list.Add(new DateTime(2016, 01, 15));
            list.Add(new DateTime(2016, 01, 26));
            list.Add(new DateTime(2016, 03, 07));
            list.Add(new DateTime(2016, 03, 23));
            list.Add(new DateTime(2016, 03, 25));
            list.Add(new DateTime(2016, 04, 05));
            list.Add(new DateTime(2016, 04, 08));
            list.Add(new DateTime(2016, 04, 14));
            list.Add(new DateTime(2016, 04, 15));
            list.Add(new DateTime(2016, 07, 06));
            list.Add(new DateTime(2016, 07, 07));
            list.Add(new DateTime(2016, 08, 01));
            list.Add(new DateTime(2016, 08, 15));
            list.Add(new DateTime(2016, 08, 25));
            list.Add(new DateTime(2016, 09, 05));
            list.Add(new DateTime(2016, 09, 12));
            list.Add(new DateTime(2016, 09, 30));
            list.Add(new DateTime(2016, 10, 11));
            list.Add(new DateTime(2016, 10, 12));
            list.Add(new DateTime(2016, 11, 14));
            list.Add(new DateTime(2016, 12, 12));
            list.Add(new DateTime(2016, 12, 26));
            return list;
        }

        /// <summary>
        /// EmpCode TextChange Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>      

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (uploadCertification.Visible == true)
            {
                if (ViewState["File"] != null)
                {
                    Saving();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('file uploades success and Leave Applied..');", true);
                    fupCertification.Focus();
                }

            }

            if (ddlLeaveApply.SelectedItem.Text == "CL")
            {
                var leavedetails = leavedata(ddlLeaveApply.SelectedItem.Text);
                if (Convert.ToInt32(txtNoofDays.Text) <= leavedetails.No_Of_Days)
                {
                    Saving();
                    txtNoofDays.Text = "";
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Casual Leaves upto 3 Days only');", true);
                }
            }
            else if (ddlLeaveApply.SelectedItem.Text == "CCL")
            {
                Saving();
                txtNoofDays.Text = "";
            }
            else if (ddlLeaveApply.SelectedItem.Text == "MTL")
            {
                var leavedetails = leavedata(ddlLeaveApply.SelectedItem.Text);
                var gender = (from gen in ObjDAL.HRMS_Employee_Master_PersonalDetails
                              where gen.Emp_Code == txtEmpCode.Text
                              select gen).FirstOrDefault();
                if (gender.Gender.ToString() != "Male" && Convert.ToInt32(txtNoofDays.Text) <= leavedetails.No_Of_Days)
                {
                    Saving();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Maternity Leave Cannot be applied.Please contact your HR');", true);
                }
            }
            else if (ddlLeaveApply.SelectedItem.Text == "ML")
            {
                if (IsPostBack && fupCertification.PostedFile != null)
                {
                    if (fupCertification.PostedFile.ContentLength < 102400)
                    {
                        string filename = Path.GetFileName(fupCertification.FileName);
                        fupCertification.SaveAs(Server.MapPath("~/Certifications/") + filename);
                        Saving();
                    }
                    else
                    {
                        Response.Write("Upload file");
                    }

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Upload File');", true);
                }


            }
            else if (ddlLeaveApply.SelectedItem.Text == "HPL")
            {
                var leavedetails = leavedata(ddlLeaveApply.SelectedItem.Text);
                if (Convert.ToInt32(txtNoofDays.Text) <= leavedetails.No_Of_Days)
                {

                    Saving();
                }
                else
                {
                    txtNoofDays.Text = "";
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Half Pay Leaves upto 20 Days only');", true);
                }
            }
            else if (ddlLeaveApply.SelectedItem.Text == "SL")
            {
                var leavedetails = leavedata(ddlLeaveApply.SelectedItem.Text);
                if (Convert.ToInt32(txtNoofDays.Text) <= leavedetails.No_Of_Days)
                {

                    Saving();
                }
                else
                {
                    txtNoofDays.Text = "";
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Study leaves only apllicabale for 1 year only');", true);
                }
            }
            else if (ddlLeaveApply.SelectedItem.Text == "EL")
            {
                var leavedetails = leavedata(ddlLeaveApply.SelectedItem.Text);
                var ELdetails = (from LA in ObjDAL.HRMS_Leave_Applications
                                 where LA.Emp_Code == txtEmpCode.Text && LA.TypeOfLeaveApple == ddlLeaveApply.SelectedItem.Text
                                 select LA).ToList();
                string mm = Convert.ToDateTime(txtFromDate.Text).ToString("MM");
                int dt = Convert.ToInt32(mm);
                int noofdayslist = Convert.ToInt32(ELdetails.Sum(a => a.Noof_Days).Value);
                if (dt >= 7 && dt <= 12)
                {
                    if ((noofdayslist + Convert.ToInt32(txtNoofDays.Text)) >= leavedetails.No_Of_Days * 2)
                    {
                        Saving();
                        txtNoofDays.Text = "";
                    }
                    else
                    {
                        txtNoofDays.Text = "";
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Earned  Leaves upto 30 Days only if Open Balancer is 300');", true);
                    }
                }
                else if (Convert.ToInt32(txtNoofDays.Text) <= leavedetails.No_Of_Days)
                {
                    Saving();
                    txtNoofDays.Text = "";
                }
                else
                {
                    txtNoofDays.Text = "";
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Earned  Leaves upto 15 Days only');", true);
                }

            }
            else if (ddlLeaveApply.SelectedItem.Text == "PTL")
            {
                var leavedetails = leavedata(ddlLeaveApply.SelectedItem.Text);
                var gender = (from gen in ObjDAL.HRMS_Employee_Master_PersonalDetails
                              where gen.Emp_Code == txtEmpCode.Text
                              select gen).FirstOrDefault();
                if (gender.Gender.ToString() != "Female" && Convert.ToInt32(txtNoofDays.Text) <= leavedetails.No_Of_Days)
                {
                    Saving();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Maternity Leave Cannotr be applied.Please contact your HR');", true);
                }
            }
            else if (ddlLeaveApply.SelectedItem.Text == "EOL")
            {
                var leavedetails = leavedata(ddlLeaveApply.SelectedItem.Text);
                if (Convert.ToInt32(txtNoofDays.Text) <= leavedetails.No_Of_Days)
                {

                    Saving();
                }
                else
                {
                    txtNoofDays.Text = "";
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Extra Ordinary leaves max only 120days');", true);
                }
            }
            else
            {
                txtNoofDays.Text = "";
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Plz Select Any Leave Type');", true);
            }


        }


        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Update_Method();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearFields();

        }
        #endregion Buttons


        #region Methods
        /// <summary>
        /// Employee Drop Down Binding 
        /// </summary>
        //public void Emp_NameBinding()
        //{
        //    try
        //    {
        //        string CreationCompany = Session["ClientRegistrationNo"].ToString();

        //        var objEmPCodeDUP = (from itmes in ObjDAL.HRMS_Employee_Master_PersonalDetails
        //                             where itmes.Creation_Company == CreationCompany
        //                             select new
        //                             {
        //                                 Emp_name = itmes.Emp_Last_Name + " " + itmes.Emp_First_Name + " " + itmes.Emp_Middle_Name,
        //                                 Emp_Code = itmes.Emp_Code
        //                             }).ToList();
        //        if (objEmPCodeDUP.Count > 0)
        //        {
        //            ddlEmpName.DataSource = objEmPCodeDUP;
        //            ddlEmpName.DataTextField = "Emp_name";
        //            ddlEmpName.DataValueField = "Emp_Code";
        //            ddlEmpName.DataBind();
        //            ddlEmpName.Items.Insert(0, new ListItem("--Select--", "0"));
        //        }

        //    }
        //    catch (Exception)
        //    {

        //        // throw;
        //    }

        //}


        private HRMS_Leave_Master leavedata(string LeaveType)
        {
            var leavedetails = (from lm in ObjDAL.HRMS_Leave_Masters
                                where lm.Short_Name == LeaveType
                                select lm).FirstOrDefault();
            return leavedetails;


        }

        /// <summary>
        /// Bind Grid Based on User Registration Details      
        /// </summary>
        /// <param name="sender"></param>
        public void Bind_Leave()
        {
            try
            {
                string Cmpy = (string)Session["ClientRegistrationNo"];
                string Brnach = (string)Session["BranchID"];
                string UserType = (string)Session["User_Type"];
                string UserName = (string)Session["User_Name"];
                string Cration_Company = (string)Session["CreationCompany"]; //"000001";
                //if (UserType == "User")
                //{
                var objHeaderInfo = (from header in ObjDAL.HRMS_Leave_Masters
                                     select header).ToList();
                // List<Administrator_UserRegistration> ObjuserInfo = objSCM_userRegistration.Get_userRegDetails(Session["ClientRegistrationNo"].ToString());
                if (objHeaderInfo.Count > 0)
                {
                    ddlLeaveApply.DataTextField = "Short_Name";
                    ddlLeaveApply.DataValueField = "ID";
                    ddlLeaveApply.DataSource = objHeaderInfo;
                    ddlLeaveApply.DataBind();
                    ddlLeaveApply.Items.Insert(0, new ListItem("--Select--", "Select"));

                }
                else
                {

                    ddlLeaveApply.Items.Insert(0, new ListItem("--Select--", "Select"));
                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }

        }


        public void ddlleavebind()
        {
            try
            {
                var leavebind = (from item in objDAL.HRMS_Leave_Masters
                                     select item).ToList();


                if (leavebind.Count > 0)
                {
                    ddlLeaveApply.DataSource = leavebind;
                    ddlLeaveApply.DataTextField = "Short_Name";
                    ddlLeaveApply.DataValueField = "ID";
                    ddlLeaveApply.DataBind();
                    ddlLeaveApply.Items.Insert(0, new ListItem("--Select--", "0"));
                }

            }
            catch (Exception)
            {

            }
        }


        //clearfields
        public void ClearFields()
        {
            txtEmpCode.Text = "";
            txtBalancesAvailable.Text = "";
            txtDepartment.Text = "";
            txtDesignation.Text = "";
            txtFromDate.Text = "";
            txtToDate.Text = "";
            txtstatus.Text = "";
            ddlLeaveApply.SelectedValue = "Select";
            //ddlStatus.SelectedValue = "Select";
            //  ddlEmpName.SelectedIndex = 0;



        }
        //Saving
        private void Saving()
        {

            try
            {
                string d1 = txtFromDate.Text.Substring(0, 2);
                string m1 = txtFromDate.Text.Substring(3, 2);
                string y1 = txtFromDate.Text.Substring(6, 4);


                DateTime FromDATE = DateTime.ParseExact(txtFromDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);

                string d2 = txtToDate.Text.Substring(0, 2);
                string m2 = txtToDate.Text.Substring(3, 2);
                string y2 = txtToDate.Text.Substring(6, 4);


                DateTime ToDATE = DateTime.ParseExact(txtToDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);
                //var leavecode = (from lveMstr in ObjDAL.HRMS_Leave_Masters
                //                 where lveMstr.Short_Name == ddlLeaveApply.Text && lveMstr.Creation_Company == Session["ClientRegistrationNo"].ToString()
                //                 select lveMstr).ToList();
               
                // objLeaveApp.Leave_Code = "";
                objLeaveApp.Emp_Code = txtEmpCode.Text;

                objLeaveApp.Emp_Name = txtEmpName.Text;// ddlEmpName.SelectedValue;
                objLeaveApp.Department = txtDepartment.Text;
                objLeaveApp.Designation = txtDesignation.Text;
                objLeaveApp.Reporting_Authority = ddlreportingauthority.SelectedValue;
                objLeaveApp.TypeOfLeaveApple = ddlLeaveApply.SelectedItem.Text;
                objLeaveApp.Balances_Available = (txtBalancesAvailable.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtBalancesAvailable.Text);
                objLeaveApp.LeaveFrom = FromDATE;
                objLeaveApp.LeaveTo = ToDATE;
                objLeaveApp.Noof_Days = (txtNoofDays.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtNoofDays.Text);
                objLeaveApp.Leave_Status = txtstatus.Text;
                objLeaveApp.Created_By = Session["User_Name"].ToString();
                objLeaveApp.Created_Date = System.DateTime.Now;
                objLeaveApp.Creation_Company = Session["ClientRegistrationNo"].ToString();
                objLeaveApp.Modified_By = Session["User_Name"].ToString();
                objLeaveApp.Modified_Date = System.DateTime.Now;
                objLeaveApp.Parameter = 1;

                if (fupCertification.HasFile)
                {
                    string file = fupCertification.FileName;
                    fupCertification.PostedFile.SaveAs(Server.MapPath("~/HRMS/Certifications/" + file));
                    objLeaveApp.Cerification_Path = "~/Certifications/" + file;
                }
                else
                {
                    objLeaveApp.Cerification_Path = "";
                }



                if (objLeaveApp.Insert_HRMS_Leave_Application() != 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);
                    ClearFields();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
                }

            }
            catch (Exception ex)
            {

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            { }
        }
        // Editing Header Details

        String StrDate = "";
        // // Show the date format dd-mm-yy in sale Order Grids
        public string GetDate(object Dt)
        {

            StrDate = string.Format("{0:dd-MM-yyyy}", Dt);
            return StrDate;
        }
        public void Edit_HeaderDetails(string additioncode, string CreationCompany)
        {


            try
            {
                var listo = (from leave in ObjDAL.HRMS_Leave_Applications
                             where leave.Leave_Code == additioncode && leave.Creation_Company == CreationCompany
                             select leave).ToList();
                if (listo.Count > 0)
                {
                    txtEmpCode.ReadOnly = txtEmpName.ReadOnly = txtFromDate.ReadOnly = txtToDate.ReadOnly = txtNoofDays.ReadOnly = true;
                    txtEmpCode.Text = listo[0].Emp_Code;
                    txtEmpName.Text = listo[0].Emp_Name;
                    txtDepartment.Text = listo[0].Department;
                    txtDesignation.Text = listo[0].Designation;
                    ddlreportingauthority.SelectedValue = listo[0].Reporting_Authority;
                    ddlLeaveApply.SelectedValue = listo[0].Leave_Code;
                    txtBalancesAvailable.Text = listo[0].Balances_Available.ToString();
                    txtFromDate.Text = GetDate(listo[0].LeaveFrom);
                    txtToDate.Text = GetDate(listo[0].LeaveTo);
                    txtNoofDays.Text = listo[0].Noof_Days.ToString();
                    txtstatus.Text = listo[0].Leave_Status;

                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }


        }


        public void ddlauthoritybind()
        {
            try
            {
                var productmaster = (from item in objDAL.HRMSEmployeeMasterRegistrationDetails
                                     select item).ToList();


                if (productmaster.Count > 0)
                {
                    ddlreportingauthority.DataSource = productmaster;
                    ddlreportingauthority.DataTextField = "UserName";
                    ddlreportingauthority.DataValueField = "ID";
                    ddlreportingauthority.DataBind();
                    ddlreportingauthority.Items.Insert(0, new ListItem("--Select Product--", "0"));
                }

            }
            catch (Exception)
            {

            }
        }

        //Updating Method

        public void bindemp()
        {
            int Emp_Code = (from em in ObjDAL.HRMSEmployeeMasterRegistrationDetails where em.UserName == Session["User_Name"].ToString() select em.ID).FirstOrDefault();
            var Emp_List = (from liste in ObjDAL.USP_gettingemployeedetailsinleave(Emp_Code) select liste).ToList();

            if (Emp_List.Count > 0)
            {
                txtEmpCode.Text = Convert.ToString(Emp_List[0].empid);
                txtEmpName.Text = Emp_List[0].empname;
                txtDepartment.Text = Emp_List[0].userrole;
                txtDesignation.Text = Emp_List[0].usersubrole;
                ddlreportingauthority.SelectedValue = Emp_List[0].ReportingAuthority;
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Emp Code is Not Valid');", true);
                txtEmpCode.Text = "";
                txtEmpCode.Focus();
                txtDepartment.Text = "";
                txtDesignation.Text = "";
                ddlreportingauthority.SelectedValue = "";
            }



        }
        public void Update_Method()
        {
            try
            {
                string d1 = txtFromDate.Text.Substring(0, 2);
                string m1 = txtFromDate.Text.Substring(3, 2);
                string y1 = txtFromDate.Text.Substring(6, 4);

                DateTime FromDATE = DateTime.ParseExact(txtFromDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);

                string d2 = txtToDate.Text.Substring(0, 2);
                string m2 = txtToDate.Text.Substring(3, 2);
                string y2 = txtToDate.Text.Substring(6, 4);

                DateTime ToDATE = DateTime.ParseExact(txtToDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);

                objLeaveApp.Emp_Code = txtEmpCode.Text;
                objLeaveApp.Emp_Name = txtEmpName.Text;
                objLeaveApp.Department = txtDepartment.Text;
                objLeaveApp.Designation = txtDesignation.Text;
                objLeaveApp.Reporting_Authority = ddlreportingauthority.SelectedValue;
                objLeaveApp.TypeOfLeaveApple = ddlLeaveApply.SelectedItem.Text;
                objLeaveApp.Balances_Available = (txtBalancesAvailable.Text == "") ? Convert.ToInt32(0) : Convert.ToInt32(txtBalancesAvailable.Text);
                objLeaveApp.LeaveFrom = FromDATE;
                objLeaveApp.LeaveTo = ToDATE;
                objLeaveApp.Noof_Days = (txtNoofDays.Text == "") ? Convert.ToInt32(0) : Convert.ToInt32(txtNoofDays.Text);
                //  objLeaveApp.Leave_Status = ddlStatus.Text;

                objLeaveApp.Created_By = Session["User_Name"].ToString();
                objLeaveApp.Created_Date = System.DateTime.Now;
                objLeaveApp.Creation_Company = Session["ClientRegistrationNo"].ToString();
                objLeaveApp.Modified_By = Session["User_Name"].ToString();
                objLeaveApp.Modified_Date = System.DateTime.Now;
                objLeaveApp.Parameter = 2;
                if (objLeaveApp.Insert_HRMS_Leave_Application() != 0)
                {

                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);
                    ClearFields();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            { }
        }

        /// <summary>
        /// No Of Days Calculation
        /// </summary>
        public void NoOfDaysCal()
        {
            try
            {
                if (txtFromDate.Text != "" && txtToDate.Text != "")
                {
                    string d1 = txtFromDate.Text.Substring(0, 2);
                    string m1 = txtFromDate.Text.Substring(3, 2);
                    string y1 = txtFromDate.Text.Substring(6, 4);
                    DateTime FromYear = Convert.ToDateTime(m1 + '-' + d1 + '-' + y1);
                    //objEmpBAL.Period_From = PeriodFromDate;

                    string d2 = txtToDate.Text.Substring(0, 2);
                    string m2 = txtToDate.Text.Substring(3, 2);
                    string y2 = txtToDate.Text.Substring(6, 4);
                    DateTime ToYear = Convert.ToDateTime(m2 + '-' + d2 + '-' + y2);

                    //Creating object of TimeSpan Class  
                    TimeSpan objTimeSpan = FromYear - ToYear;
                    //years
                    int Years = FromYear.Year - ToYear.Year;

                    //months
                    int month = FromYear.Month - ToYear.Month;

                    //Total Months
                    int TotalMonths = (Years * 12) + month;

                    //TotalDays  
                    double Days = Math.Abs(Convert.ToDouble(objTimeSpan.TotalDays));
                    txtNoofDays.Text = Convert.ToString(Days);
                    //ddlStatus.Focus();
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }
            finally
            {
            }
        }

        #endregion

        /// <summary>
        /// Emp Code On TextChanged Event For Checking Emp Code 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtEmpCode_OnTextChanged(object sender, EventArgs e)
        {
            string CreationCompany = (string)Session["ClientRegistrationNo"].ToString();
            string Emp_Code = (txtEmpCode.Text.ToLower().Trim() == "") ? "" : txtEmpCode.Text.ToLower().Trim();
            var objEmPCodeDUP = (from itmes in ObjDAL.HRMS_Employee_Master_PersonalDetails
                                 where itmes.Creation_Company == CreationCompany && itmes.Emp_Code.ToLower().Trim() == Emp_Code
                                 select new
                                 {
                                     Emp_name = itmes.Emp_Last_Name + " " + itmes.Emp_First_Name + " " + itmes.Emp_Middle_Name,
                                     Emp_Code = itmes.Emp_Code
                                 }).ToList();
            if (objEmPCodeDUP.Count > 0)
            {
                // ddlEmpName.SelectedValue = Emp_Code;
                var s = (from x in ObjDAL.HRMS_Employee_Master_Official_Informations
                         where x.Emp_Code == txtEmpCode.Text && x.Creation_Company == CreationCompany
                         select x).ToList();
                if (s.Count > 0)
                {
                    txtDepartment.Text = s[0].Department;
                    txtDesignation.Text = s[0].Designation;
                    ddlreportingauthority.Text = s[0].Reporting_Authority;
                    ddlLeaveApply.Focus();
                }
                else
                {
                    txtDepartment.Text = "";
                    txtDesignation.Text = "";
                    ddlreportingauthority.Text = "";
                }
            }
            else
            {
                txtEmpCode.Text = "";
                txtEmpCode.Focus();
                // ddlEmpName.SelectedValue = "0";
                txtDepartment.Text = "";
                txtDesignation.Text = "";
                ddlreportingauthority.Text = "";
            }
        }

        //Balance Available bind method

        public void BalanceAva()
        {
            if (txtBalancesAvailable.Text != "" && txtNoofDays.Text != "")
            {
                if (Convert.ToDecimal(txtBalancesAvailable.Text) > Convert.ToDecimal(txtNoofDays.Text))
                {
                    txtBalancesAvailable.Text = (Convert.ToDecimal(txtBalancesAvailable.Text) - Convert.ToDecimal(txtNoofDays.Text)).ToString();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Your Have Only " + txtBalancesAvailable.Text.ToString() + " Leaves ');", true);
                    txtFromDate.Text = "";
                    txtToDate.Text = "";
                    txtNoofDays.Text = "";
                }
            }
        }

        protected void ddlLeaveApply_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int Emp_Code = (from em in ObjDAL.HRMSEmployeeMasterRegistrationDetails where em.UserName == Session["User_Name"].ToString() select em.ID).FirstOrDefault();
                var Emp_List = (from liste in ObjDAL.USP_getbalanceleavedetails(Emp_Code, Convert.ToString(ddlLeaveApply.SelectedItem.Text)) select liste).ToList();
                if (Emp_List.Count > 0)
                {
                    txtBalancesAvailable.Text = Convert.ToString(Emp_List[0].leavebalance);
                }

            }
            catch (Exception ex)
            {
            }



            //    if (ViewState["Leaves"] != null)
            //{
            //    var balance = (from EM in ObjDAL.HRMS_Leave_Allotments
            //                   where EM.Emp_Code == txtEmpCode.Text && EM.Leave_Name == ddlLeaveApply.SelectedItem.Text
            //                   select EM).FirstOrDefault();
            //    if (balance != null)
            //    {
            //        txtBalancesAvailable.Text = balance.Noof_Leavs.ToString();
            //    }
            //    else
            //    {
            //        txtBalancesAvailable.Text = "0";
            //    }
            //    //DataTable dt = (DataTable)ViewState["Leaves"];
            //    //for (int i = 0; i < dt.Rows.Count; i++)
            //    //{
            //    //    if (ddlLeaveApply.SelectedItem.Text == dt.Rows[i]["Leave_Name"].ToString())
            //    //    {

            //    //        txtBalancesAvailable.Text = dt.Rows[i]["Balance_Leaves"].ToString();
            //    //        break;
            //    //    }
            //    //}
            //}

            //var objHeaderInfo = (from header in ObjDAL.HRMS_Leave_Masters
            //                     where header.Short_Name == ddlLeaveApply.SelectedItem.Text
            //                     orderby header.Leave_Code descending
            //                     select header).ToList();
            //if (objHeaderInfo[0].Need_Certification.ToLower().Trim() == "yes")
            //{
            //    //if (fupCertification.HasFile)
            //    //{
            //    uploadCertification.Visible = true;
            //    //  ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Plz Upload related Certificates');", true);
            //    uploadCertification.Focus();



            //}
            //else
            //{
            //    uploadCertification.Visible = false;
            //}
        }

        protected void txtNoofDays_TextChanged(object sender, EventArgs e)
        {
            //BalanceAva();
        }

        protected void txtBalancesAvailable_TextChanged(object sender, EventArgs e)
        {
            //BalanceAva();
        }



        //protected void ddlEmpName_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    string CreationCompany = (string)Session["ClientRegistrationNo"].ToString();
        //    string Emp_Code = ddlEmpName.SelectedValue;
        //    var objEmPCodeDUP = (from itmes in ObjDAL.HRMS_Employee_Master_PersonalDetails
        //                         where itmes.Creation_Company == CreationCompany && itmes.Emp_Code.ToLower().Trim() == Emp_Code
        //                         select new
        //                         {
        //                             Emp_name = itmes.Emp_Last_Name + " " + itmes.Emp_First_Name + " " + itmes.Emp_Middle_Name,
        //                             Emp_Code = itmes.Emp_Code
        //                         }).ToList();
        //    if (objEmPCodeDUP.Count > 0)
        //    {
        //        ddlEmpName.SelectedValue = Emp_Code;
        //        txtEmpCode.Text = ddlEmpName.SelectedValue;

        //        var s = (from x in ObjDAL.HRMS_Employee_Master_Official_Informations
        //                 where x.Emp_Code == Emp_Code && x.Creation_Company == CreationCompany
        //                 select x).ToList();
        //        if (s.Count > 0)
        //        {
        //            txtDepartment.Text = s[0].Department;
        //            txtDesignation.Text = s[0].Designation;
        //            txtReportingAuthority.Text = s[0].Reporting_Authority;
        //            ddlLeaveApply.Focus();
        //        }
        //        else
        //        {
        //            txtEmpCode.Text = "";
        //            txtDepartment.Text = "";
        //            txtDesignation.Text = "";
        //            txtReportingAuthority.Text = "";
        //            ddlEmpName.Focus();
        //            ddlEmpName.SelectedIndex = 0;
        //        }
        //    }
        //    else
        //    {
        //        txtEmpCode.Text = "";
        //        txtEmpCode.Focus();
        //        ddlEmpName.SelectedValue = "0";
        //        txtDepartment.Text = "";
        //        txtDesignation.Text = "";
        //        txtReportingAuthority.Text = "";
        //    }
        //}
    }
}