﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BAL;
using DAL;
using System.Globalization;

namespace HRMS.Payroll
{
    public partial class Leave_Apporval : System.Web.UI.Page
    {
   

            ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();

            LeaveApplicationBAL objLeaveApp = new LeaveApplicationBAL();
            private int SNO = 0;
            protected void Page_Load(object sender, EventArgs e)
            {


                if (!IsPostBack)
                {
                    if (Session["User_Type"].ToString() == "CCF" || Session["User_Type"].ToString() == "POA")
                    {
                       // ClientScript.RegisterStartupScript(typeof(Page), "alertMessage", "<script type='text/javascript'>alert('You Don't have Permissions To Create..');window.location.replace('../Dashboard/Dashboard.aspx');</script>");

                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('You Don't have Permissions To Create..');", true);
                        ddlStatus.Enabled = false;
                        
                        //  Response.Redirect("~/Dashboard/Dashboard.aspx");
                    }
                    //else if (Session["User_Type"].ToString() == "APCCF")
                    //{
                    //    if (ddlLeaveApply.SelectedItem.Text == "CL")
                    //    {
                    //        ddlStatus.Items.Clear();
                    //        ddlStatus.Items.Insert(0, new ListItem("--Select--", "0"));
                    //        // ddlStatus.Items.Insert(0, new ListItem("Wating For Approval", "Wating For Approval"));
                    //        ddlStatus.Items.Insert(0, new ListItem("Approval", "Approval"));
                    //        ddlStatus.Items.Insert(0, new ListItem("Rejected", "Rejected"));
                    //    }
                    //    else
                    //    {
                    //        ddlStatus.Items.Clear();
                    //        ddlStatus.Items.Insert(0, new ListItem("--Select--", "0"));
                    //        // ddlStatus.Items.Insert(0, new ListItem("Wating For Approval", "Wating For Approval"));
                    //        ddlStatus.Items.Insert(0, new ListItem("Recommended", "Recommended"));
                    //        // ddlStatus.Items.Insert(0, new ListItem("Rejected", "Rejected"));
                    //    }
                    //}
                    if (Session["ClientRegistrationNo"] == null)
                    {
                        Response.Redirect("~/Default.aspx");
                    }

                    //  btnUpdate.Visible = false;
                    txtEmpCode.Focus();

                    //Employee Name Binding
                    Emp_NameBinding();

                    //Leave Types Bind
                    Bind_Leave();

                    //  txtToDate.Text = DateTime.Now.ToString();

                    if ((string)Session["AdditionCode"] != null)
                    {

                        //  Request.QueryString.Clear();
                        string ClientRegistrationNo = Session["ClientRegistrationNo"].ToString();
                        string AdditionCode = (string)Session["AdditionCode"];
                       
                        Session.Remove("AdditionCode");
                        //string LoginID = Request.QueryString["id"];
                        Edit_HeaderDetails(AdditionCode, ClientRegistrationNo);

                    }
                    else
                    {
                        btnUpdate.Visible = false;
                    }

                }
            }
    



        #region Events

        /// <summary>
        /// Text Change Event For From Date Caluculation  Days
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtFromDate_OnTextChanged(object sender, EventArgs e)
        {
            NoOfDaysCal();
            txtToDate.Focus();
            BalanceAva();
        }

        /// <summary>
        /// Text Change Event for To date Calculation for Days
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtToDate_OnTextChanged(object sender, EventArgs e)
        {
            NoOfDaysCal();
            BalanceAva();

        }
        /// <summary>
        /// EmpCode TextChange Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>      

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Saving();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Update_Method();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearFields();

        }
        #endregion Buttons


        #region Methods
        /// <summary>
        /// Employee Drop Down Binding 
        /// </summary>
        public void Emp_NameBinding()
        {
            try
            {
                string CreationCompany = Session["ClientRegistrationNo"].ToString();

                var objEmPCodeDUP = (from itmes in ObjDAL.HRMS_Employee_Master_PersonalDetails
                                     where itmes.Creation_Company == CreationCompany
                                     select new
                                     {
                                         Emp_name = itmes.Emp_Last_Name + " " + itmes.Emp_First_Name + " " + itmes.Emp_Middle_Name,
                                         Emp_Code = itmes.Emp_Code
                                     }).ToList();
                if (objEmPCodeDUP.Count > 0)
                {
                    ddlEmpName.DataSource = objEmPCodeDUP;
                    ddlEmpName.DataTextField = "Emp_name";
                    ddlEmpName.DataValueField = "Emp_Code";
                    ddlEmpName.DataBind();
                    ddlEmpName.Items.Insert(0, new ListItem("--Select--", "0"));
                }

            }
            catch (Exception)
            {

                throw;
            }

        }


        /// <summary>
        /// Bind Grid Based on User Registration Details      
        /// </summary>
        /// <param name="sender"></param>
        public void Bind_Leave()
        {
            try
            {
                string Cmpy = (string)Session["ClientRegistrationNo"];
                string Brnach = (string)Session["BranchID"];
                string UserType = (string)Session["User_Type"];
                string UserName = (string)Session["User_Name"];
                string Cration_Company = (string)Session["CreationCompany"]; //"000001";
                //if (UserType == "User")
                //{
                var objHeaderInfo = (from header in ObjDAL.HRMSLeaveMasters
                                     where header.CreationCompany == Cmpy
                                     orderby header.ID descending
                                     select header).ToList();
                // List<Administrator_UserRegistration> ObjuserInfo = objSCM_userRegistration.Get_userRegDetails(Session["ClientRegistrationNo"].ToString());
                if (objHeaderInfo.Count > 0)
                {
                    ddlLeaveApply.DataTextField = "Short_Name";
                    ddlLeaveApply.DataValueField = "Leave_Code";
                    ddlLeaveApply.DataSource = objHeaderInfo;
                    ddlLeaveApply.DataBind();
                    ddlLeaveApply.Items.Insert(0, new ListItem("--Select--", "Select"));
                }
                else
                {

                    ddlLeaveApply.Items.Insert(0, new ListItem("--Select--", "Select"));
                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }

        }

        //clearfields
        public void ClearFields()
        {
            txtEmpCode.Text = "";
            txtBalancesAvailable.Text = "";
            txtDepartment.Text = "";
            txtDesignation.Text = "";
            txtFromDate.Text = "";
            txtToDate.Text = "";
            ddlLeaveApply.SelectedValue = "Select";
            ddlStatus.SelectedValue = "Select";
            ddlEmpName.SelectedIndex = 0;



        }
        //Saving
        private void Saving()
        {
            try
            {
                string d1 = txtFromDate.Text.Substring(0, 2);
                string m1 = txtFromDate.Text.Substring(3, 2);
                string y1 = txtFromDate.Text.Substring(6, 4);


                DateTime FromDATE = DateTime.ParseExact(txtFromDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);

                string d2 = txtToDate.Text.Substring(0, 2);
                string m2 = txtToDate.Text.Substring(3, 2);
                string y2 = txtToDate.Text.Substring(6, 4);


                DateTime ToDATE = DateTime.ParseExact(txtToDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);

                objLeaveApp.EmpCode = txtEmpCode.Text;

               
                objLeaveApp.ReportingAuthority = txtReportingAuthority.Text;
                objLeaveApp.TypeOfLeaveApple = ddlLeaveApply.SelectedItem.Text;
                objLeaveApp.LeaveFrom = FromDATE;
                objLeaveApp.LeaveTo = ToDATE;
                objLeaveApp.NoofDays = (txtNoofDays.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtNoofDays.Text);
                objLeaveApp.LeaveStatus = ddlStatus.Text;


                objLeaveApp.CreatedBy = Session["User_Name"].ToString();
                objLeaveApp.CreatedDate = System.DateTime.Now;
                objLeaveApp.CreationCompany = Session["ClientRegistrationNo"].ToString();
                objLeaveApp.ModifiedBy = Session["User_Name"].ToString();
                objLeaveApp.ModifiedDate = System.DateTime.Now;
                objLeaveApp.Parameter = 1;
                if (objLeaveApp.Insert_HRMS_Leave_Application() != 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);
                    ClearFields();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
                }

            }
            catch (Exception ex)
            {

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            { }
        }

        String StrDate = "";
        // // Show the date format dd-mm-yy in sale Order Grids
        public string GetDate(object Dt)
        {

            StrDate = string.Format("{0:dd-MM-yyyy}", Dt);
            return StrDate;
        }
        // Editing Header Details
        public void Edit_HeaderDetails(string additioncode, string CreationCompany)
        {

            SNO=Convert.ToInt32(Request.QueryString["Id"].ToString());
            try
            {
                var listo = (from leave in ObjDAL.HRMSLeaveApplications
                             where leave.ID == SNO
                             select leave).ToList();
                if (listo.Count > 0)
                {

                    txtReportNo.Text = additioncode;
                    btnSave.Visible = false;
                    btnUpdate.Visible = true;
                    txtEmpCode.Text =listo[0].EmpCode;
                   // txtEmpName.Text = listo[0].Emp_Name;
                  //  txtDepartment.Text = listo[0].Department;
                   // txtDesignation.Text = listo[0].Designation;
                    txtReportingAuthority.Text = listo[0].ReportingAuthority;
                    ddlLeaveApply.SelectedItem.Text = listo[0].TypeOfLeaveApple;
                   // txtBalancesAvailable.Text = listo[0].Balances_Available.ToString();
                    txtFromDate.Text = GetDate(listo[0].LeaveFrom);
                    txtToDate.Text = GetDate(listo[0].LeaveTo);
                    txtNoofDays.Text = listo[0].NoOfDays.ToString();
                    ddlStatus.SelectedValue = listo[0].LeaveStatus;

                    var listLeaveD = (from leave in ObjDAL.HRMSLeaveMasters
                                      where leave.ID == Convert.ToInt32(listo[0].ID)
                                      select leave).ToList();
                    if (listLeaveD.Count() > 0)
                    {
                        if (Session["User_Type"].ToString() == "APCCF")
                        {
                            if (ddlLeaveApply.SelectedItem.Text == "CL")
                            {
                                ddlStatus.Items.Clear();
                               // ddlStatus.Items.Insert(0, new ListItem("--Select--", "0"));
                                // ddlStatus.Items.Insert(0, new ListItem("Wating For Approval", "Wating For Approval"));
                                ddlStatus.Items.Insert(0, new ListItem("Approval", "Approval"));
                                ddlStatus.Items.Insert(0, new ListItem("Rejected", "Rejected"));
                            }
                            else
                            {
                                ddlStatus.Items.Clear();
                               // ddlStatus.Items.Insert(0, new ListItem("--Select--", "0"));
                                // ddlStatus.Items.Insert(0, new ListItem("Wating For Approval", "Wating For Approval"));
                                ddlStatus.Items.Insert(0, new ListItem("Recommended", "Recommended"));
                                 ddlStatus.Items.Insert(0, new ListItem("Rejected", "Rejected"));
                            }
                        }
                        else if (Session["User_Type"].ToString() == "HOFF")
                        {
                           
                            
                            
                                ddlStatus.Items.Clear();
                               // ddlStatus.Items.Insert(0, new ListItem("--Select--", "0"));
                                //
                          //ddlStatus.Items.Insert(0, new ListItem("Wating For Approval", "Wating For Approval"));
                               // ddlStatus.Items.Insert(0, new ListItem("Recommended", "Recommended"));
                                ddlStatus.Items.Insert(0, new ListItem("Rejected", "Rejected"));
                                ddlStatus.Items.Insert(0, new ListItem("Approval", "Approval"));
                            
                        }
                       
                       
                        //if (listLeaveD[0].TwoStage == true.ToString())
                        //{

                        //    if (listLeaveD[0].UserName1 == Session["User_Name"].ToString())
                        //    {
                        //        ddlStatus.Items.Clear();
                        //        ddlStatus.Items.Insert(0, new ListItem("--Select--", "0"));
                        //        ddlStatus.Items.Insert(0, new ListItem("Wating For Approval", "Wating For Approval"));
                        //        ddlStatus.Items.Insert(0, new ListItem("Recommended", "Recommended"));
                        //        ddlStatus.Items.Insert(0, new ListItem("Rejected", "Rejected"));

                        //    }
                        //    else
                        //        if (listLeaveD[0].UserName2 == Session["User_Name"].ToString())
                        //        {
                        //            ddlStatus.Items.Clear();
                        //            ddlStatus.Items.Insert(0, new ListItem("--Select--", "0"));
                        //            ddlStatus.Items.Insert(0, new ListItem("Wating For Approval", "Recommended"));
                        //            // ddlStatus.Items.Insert(0, new ListItem("Recommended", "Recommended"));
                        //            ddlStatus.Items.Insert(0, new ListItem("Approval", "Approval"));
                        //        }
                        //}
                    }
                }
            }





            catch (Exception ex)
            {
                
            }

            //try
            //{

            //    var objHeaderID = objAddictionBAL.Get_Addition_Header(additioncode, CreationCompany);
            //    if (objHeaderID.Count > 0)
            //    {
            //        txtID.Text = objHeaderID[0].Addition_Code.ToString();
            //        txtHeadName.Text = objHeaderID[0].Head_Name.ToString();
            //        //txtPassword.Attributes.Add("value", txtPassword.Text);
            //        txtShortName.Text = objHeaderID[0].Short_Name.ToString();
            //        //  txtDeducton.Text = "";
            //        ddlCaluculation.SelectedValue = objHeaderID[0].Caluculation.ToString();
            //        txtPerOfBasic.Text = objHeaderID[0].Per_Of_Basic.ToString();
            //        //  txtworking.Text= objHeaderID[0].Ot_Day_Working_Hrs.ToString();
            //        //  txtNightWorking.Text = objHeaderID[0].Ot_Night_Working_Hrs.ToString();
            //        // txtWeeklyOff.Text = objHeaderID[0].Ot_On_Weakly_Off.ToString();
            //        // txtHolidays.Text = objHeaderID[0].Ot_On_Holiday.ToString();



            //        btnSave.Visible = false;
            //        btnUpdate.Visible = true;
            //        Session["AdditionCode"] = null;


            //    }
            //    else
            //    {
            //        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('No Data Found ');", true);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    string Error = ex.Message.ToString();
            //    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            //}
            //finally
            //{

            //}
        }

        //Updating Method
        public void Update_Method()
        {

            try
            {
                SNO = Convert.ToInt32(Request.QueryString["Id"].ToString());
                string d1 = txtFromDate.Text.Substring(0, 2);
                string m1 = txtFromDate.Text.Substring(3, 2);
                string y1 = txtFromDate.Text.Substring(6, 4);


                DateTime FromDATE = DateTime.ParseExact(txtFromDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);

                string d2 = txtToDate.Text.Substring(0, 2);
                string m2 = txtToDate.Text.Substring(3, 2);
                string y2 = txtToDate.Text.Substring(6, 4);


                DateTime ToDATE = DateTime.ParseExact(txtToDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);

                objLeaveApp.EmpCode = txtEmpCode.Text;
                objLeaveApp.ReportingAuthority = txtReportingAuthority.Text;
                objLeaveApp.TypeOfLeaveApple = ddlLeaveApply.SelectedItem.Text;
                objLeaveApp.LeaveFrom = FromDATE;
                objLeaveApp.LeaveTo = ToDATE;
                objLeaveApp.NoofDays = (txtNoofDays.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtNoofDays.Text);
                objLeaveApp.LeaveStatus = ddlStatus.SelectedItem.Text;

                objLeaveApp.CreatedBy = Session["User_Name"].ToString();
                objLeaveApp.CreatedDate = System.DateTime.Now;
               objLeaveApp.CreationCompany = Session["ClientRegistrationNo"].ToString();
                objLeaveApp.ModifiedBy = Session["User_Name"].ToString();
                objLeaveApp.ModifiedDate = System.DateTime.Now;
                objLeaveApp.Parameter = 2;
                objLeaveApp.ID = SNO;
                if (objLeaveApp.Insert_HRMS_Leave_Application() != 0)
                {
                    //TODO:Update the Open Blaance

                    HRMSLeaveAllotment objallotment = ObjDAL.HRMSLeaveAllotments.Single(a => a.EmpCode == txtEmpCode.Text && a.LeaveName == ddlLeaveApply.SelectedItem.Text);
                    objallotment.NoofLeavs = Convert.ToDecimal(objallotment.NoofLeavs) - Convert.ToDecimal(txtNoofDays.Text);
                    ObjDAL.SubmitChanges();
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);
                    ClearFields();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
                }

            }
            catch (Exception ex)
            {

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            { }
        }

        /// <summary>
        /// No Of Days Calculation
        /// </summary>
        public void NoOfDaysCal()
        {
            try
            {

                if (txtFromDate.Text != "" && txtToDate.Text != "")
                {

                    string d1 = txtFromDate.Text.Substring(0, 2);
                    string m1 = txtFromDate.Text.Substring(3, 2);
                    string y1 = txtFromDate.Text.Substring(6, 4);
                    DateTime FromYear = Convert.ToDateTime(m1 + '-' + d1 + '-' + y1);
                    //objEmpBAL.Period_From = PeriodFromDate;

                    string d2 = txtToDate.Text.Substring(0, 2);
                    string m2 = txtToDate.Text.Substring(3, 2);
                    string y2 = txtToDate.Text.Substring(6, 4);
                    DateTime ToYear = Convert.ToDateTime(m2 + '-' + d2 + '-' + y2);


                    //Creating object of TimeSpan Class  
                    TimeSpan objTimeSpan = FromYear - ToYear;
                    //years
                    int Years = FromYear.Year - ToYear.Year;

                    //months
                    int month = FromYear.Month - ToYear.Month;

                    //Total Months
                    int TotalMonths = (Years * 12) + month;

                    //TotalDays  
                    double Days = Math.Abs(Convert.ToDouble(objTimeSpan.TotalDays));
                    txtNoofDays.Text = Convert.ToString(Days);
                    ddlStatus.Focus();
                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }
            finally
            {

            }

        }

        #endregion

        /// <summary>
        /// Emp Code On TextChanged Event For Checking Emp Code 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtEmpCode_OnTextChanged(object sender, EventArgs e)
        {
            string CreationCompany = (string)Session["ClientRegistrationNo"].ToString();
            string Emp_Code = (txtEmpCode.Text.ToLower().Trim() == "") ? "" : txtEmpCode.Text.ToLower().Trim();
            var objEmPCodeDUP = (from itmes in ObjDAL.HRMS_Employee_Master_PersonalDetails
                                 where itmes.Creation_Company == CreationCompany && itmes.Emp_Code.ToLower().Trim() == Emp_Code
                                 select new
                                 {
                                     Emp_name = itmes.Emp_Last_Name + " " + itmes.Emp_First_Name + " " + itmes.Emp_Middle_Name,
                                     Emp_Code = itmes.Emp_Code
                                 }).ToList();
            if (objEmPCodeDUP.Count > 0)
            {

                //ddlEmpName.DataSource = objEmPCodeDUP;
                //ddlEmpName.DataTextField = "Emp_name";
                //ddlEmpName.DataValueField = "Emp_Code";
                //ddlEmpName.DataBind();
                //ddlEmpName.Items.Insert(0, new ListItem("--Select--", "0"));
                ddlEmpName.SelectedValue = Emp_Code;
                var s = (from x in ObjDAL.HRMS_Employee_Master_Official_Informations
                         where x.Emp_Code == txtEmpCode.Text && x.Creation_Company == CreationCompany
                         select x).ToList();
                if (s.Count > 0)
                {


                    txtDepartment.Text = s[0].Department;
                    txtDesignation.Text = s[0].Designation;
                    txtReportingAuthority.Text = s[0].Reporting_Authority;
                    ddlLeaveApply.Focus();
                }
                else
                {
                    txtDepartment.Text = "";
                    txtDesignation.Text = "";
                    txtReportingAuthority.Text = "";
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Emp Code is Not Valid');", true);
                txtEmpCode.Text = "";
                txtEmpCode.Focus();
                ddlEmpName.SelectedValue = "0";

                txtDepartment.Text = "";
                txtDesignation.Text = "";
                txtReportingAuthority.Text = "";

            }

        }

        //Balance Available bind method

        public void BalanceAva()
        {
            if (txtBalancesAvailable.Text != "" && txtNoofDays.Text != "")
            {
                if (Convert.ToDecimal(txtBalancesAvailable.Text) > Convert.ToDecimal(txtNoofDays.Text))
                {

                    txtBalancesAvailable.Text = (Convert.ToDecimal(txtBalancesAvailable.Text) - Convert.ToDecimal(txtNoofDays.Text)).ToString("0");
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Your leave is Not Approved');", true);
                    txtFromDate.Text = "";
                    txtToDate.Text = "";
                    txtNoofDays.Text = "";


                }

            }
        }

        protected void ddlLeaveApply_SelectedIndexChanged(object sender, EventArgs e)
        {
            string year = DateTime.Now.Year.ToString();

            if (ddlLeaveApply.SelectedValue != "")
            {
                var objBal = (from s in ObjDAL.HRMSLeaveAllotments
                              where s.LeaveName == ddlLeaveApply.SelectedItem.Text && s.EmpCode == txtEmpCode.Text
                              && s.CreationCompany == Session["ClientRegistrationNo"].ToString()
                              && s.SelectYear == year
                              select s).ToList();
                if (objBal.Count > 0)
                {
                    (txtBalancesAvailable.Text) = (objBal[0].NoofLeavs).ToString();

                }
            }
            BalanceAva();

        }

        protected void txtNoofDays_TextChanged(object sender, EventArgs e)
        {
            BalanceAva();
        }

        protected void txtBalancesAvailable_TextChanged(object sender, EventArgs e)
        {
            BalanceAva();
        }

        protected void ddlEmpName_SelectedIndexChanged(object sender, EventArgs e)
        {
            string CreationCompany = (string)Session["ClientRegistrationNo"].ToString();
            string Emp_Code = ddlEmpName.SelectedValue;
            var objEmPCodeDUP = (from itmes in ObjDAL.HRMS_Employee_Master_PersonalDetails
                                 where itmes.Creation_Company == CreationCompany && itmes.Emp_Code.ToLower().Trim() == Emp_Code
                                 select new
                                 {
                                     Emp_name = itmes.Emp_Last_Name + " " + itmes.Emp_First_Name + " " + itmes.Emp_Middle_Name,
                                     Emp_Code = itmes.Emp_Code
                                 }).ToList();
            if (objEmPCodeDUP.Count > 0)
            {

                //ddlEmpName.DataSource = objEmPCodeDUP;
                //ddlEmpName.DataTextField = "Emp_name";
                //ddlEmpName.DataValueField = "Emp_Code";
                //ddlEmpName.DataBind();
                //ddlEmpName.Items.Insert(0, new ListItem("--Select--", "0"));
                ddlEmpName.SelectedValue = Emp_Code;
                txtEmpCode.Text = ddlEmpName.SelectedValue;

                var s = (from x in ObjDAL.HRMS_Employee_Master_Official_Informations
                         where x.Emp_Code == Emp_Code && x.Creation_Company == CreationCompany
                         select x).ToList();
                if (s.Count > 0)
                {



                    txtDepartment.Text = s[0].Department;
                    txtDesignation.Text = s[0].Designation;
                    txtReportingAuthority.Text = s[0].Reporting_Authority;
                    ddlLeaveApply.Focus();
                }
                else
                {
                    txtEmpCode.Text = "";
                    txtDepartment.Text = "";
                    txtDesignation.Text = "";
                    txtReportingAuthority.Text = "";
                    ddlEmpName.Focus();
                    ddlEmpName.SelectedIndex = 0;
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Emp Code is Not Valid');", true);
                txtEmpCode.Text = "";
                txtEmpCode.Focus();
                ddlEmpName.SelectedValue = "0";

                txtDepartment.Text = "";
                txtDesignation.Text = "";
                txtReportingAuthority.Text = "";

            }

        }
    
    }
}