﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="PastLeaveBalanceReport.aspx.cs" Inherits="HRMS.Payroll.PastLeaveBalanceReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="#">Reports</a><i class="fa fa-angle-right"></i> </li>
                        <li><a href="../Payroll/PastLeaveBalanceReport.aspx" class="current">Past Leave Report</a> </li>
                    </ul>
                </div>
            </div>

            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-md-12">
                            <section class="panel">
                                <header class="panel-heading">
                                    <i class="fa fa-th-list"> Past Leave Balance Report</i>
                                </header>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="form-group col-md-6">
                                            <label for="textfield" class="col-sm-5 text-center">
                                                Year<span style="color: #ff0000; font-size: 14px;">*</span>
                                            </label>
                                            <div class="col-sm-7" style="">
                                                <asp:DropDownList ID="ddlYear" runat="server" class="form-control ">
                                                    <asp:ListItem Value="0">Select</asp:ListItem>
                                                    <asp:ListItem Value="2016">2016</asp:ListItem>
                                                    <asp:ListItem Value="2017">2017</asp:ListItem>
                                                    <asp:ListItem Value="2018">2018</asp:ListItem>
                                                    
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                      
                                        <div class="form-group col-md-6" runat="server" visible="false">
                                            <label for="textfield" class="control-label col-sm-5" style="">
                                                Department
                                            </label>
                                            <div class="col-sm-7" style="">
                                                <asp:DropDownList ID="ddlDepartment" runat="server" class="form-control  ">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-12 text-center">
                                        <asp:Button ID="btnShow" runat="server" Text="Submit" class="btn btn-primary" OnClick="btnShow_Click" OnClientClick="return Validation()" />
                                    </div>
                            </section>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel">
                                <div class="panel-heading">
                                    <i class="fa fa-th-user">Leave Balances Available</i>
                                </div>
                                <div class="panel-body">
                                    <asp:GridView ID="GdvBalanceLeaves" runat="server" EmptyDataText="No records Found" class="table table-nomargin table-bordered CustomerAddress" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundField DataField="leavename" HeaderText="Leave Name" ItemStyle-Width="150">
                                                <ItemStyle Width="150px"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="leavebalance" HeaderText="Balances Available" ItemStyle-Width="150">
                                                <ItemStyle Width="150px"></ItemStyle>
                                            </asp:BoundField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>

            </asp:UpdatePanel>


        </section>
    </section>
</asp:Content>
