﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BAL;
using DAL;

namespace HRMS.Payroll
{
    public partial class Define_Leaves : System.Web.UI.Page
    {
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();

        // DefineLeavesBAL objLeaveBAL = new DefineLeavesBAL();
        DefineLeavesBAL objLeaveBAL = new DefineLeavesBAL();
        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["User_Type"].ToString() == "User")
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('You Don't have Permissions To Create..');", true);
                    Response.Redirect("~/Dashboard/Dashboard.aspx");
                }
                {
                    if (Session["ClientRegistrationNo"] == null)
                    {
                        Response.Redirect("~/Dashboard/Dashboard.aspx");
                    }

                    txtEncashingLimit.ReadOnly = true;

                    txtMonthly.ReadOnly = true;
                    getemployee();
                    txtLeaveName.Focus();

                    if ((string)Session["Leave_code"] != null)
                    {

                        //  Request.QueryString.Clear();
                        string ClientRegistrationNo = Session["ClientRegistrationNo"].ToString();
                        string LeaveCode = (string)Session["Leave_code"];

                        Edit_HeaderDetails(LeaveCode, ClientRegistrationNo);
                    }
                    else
                    {
                        btnUpdate.Visible = false;
                    }

                }
            }
            if (txtLeaveName.Text == "")
            {
                txtID.ReadOnly = false;
            }
            else
            {
                txtID.ReadOnly = true;
            }


        }



        #region Methos

        public void ClearFields()
        {
            txtID.Text = "";
            txtLeaveName.Text = "";
            txtShortName.Text = "";
            ddlAllotment.SelectedValue = "Select";
            txtMonthly.Text = "";
            //chbMonthly.Checked = false;
            chbEncashment.Checked = false;
            chbBalances.Checked = false;
            txtEncashingLimit.Enabled = false;
            txtEncashingLimit.Text = "0";
            txtPerOfBasic.Text = "0";
            txtPerOfBasic.Text = "";

        }
        //Save Details
        private void Saving()
        {
            try
            {
                objLeaveBAL.Leave_Code = "";
                objLeaveBAL.Leave_Name = txtLeaveName.Text;
                objLeaveBAL.Short_Name = txtShortName.Text;
                objLeaveBAL.Monthly = "";
                objLeaveBAL.Monthly_Limit = (txtMonthly.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtMonthly.Text);
                objLeaveBAL.Encashment = chbEncashment.Text;
                objLeaveBAL.Balnces_Transfer = chbBalances.Text;
                objLeaveBAL.Encashment_Balance = (txtEncashingLimit.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtEncashingLimit.Text);
                objLeaveBAL.Allotment = ddlAllotment.Text;
                objLeaveBAL.Noof_leaves_Per_Year = (txtPerOfBasic.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtPerOfBasic.Text);
                objLeaveBAL.twoStage = (chktwostages.Checked == true) ? "Yes" : "No";
                objLeaveBAL.Need_Certification = (chbCertification.Checked == true) ? "Yes" : "No";
                objLeaveBAL.Authority1 = ddlauthority1.Text;
                objLeaveBAL.Username1 = txtusername.Text;
                objLeaveBAL.Authority2 = ddlauthority2.Text;
                objLeaveBAL.Username2 = txtusername2.Text;
                //objLeaveBAL. = ddlauthority2.Text;
                //objLeaveBAL.Username2 = txtusername2.Text;
                objLeaveBAL.Authority3 = ddlauthority3.Text;
                objLeaveBAL.UserName3 = txtusername3.Text;
                objLeaveBAL.No_Of_Days = (txtdays.Text=="") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtdays.Text); 
                objLeaveBAL.No_Of_Times = (txtnooftimes.Text=="") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtnooftimes.Text);
                objLeaveBAL.Period = ddlperiod.Text;
                objLeaveBAL.Created_By = Session["User_Name"].ToString();
                objLeaveBAL.Created_Date = System.DateTime.Now;
                objLeaveBAL.Creation_Company = Session["ClientRegistrationNo"].ToString();
                objLeaveBAL.Modified_By = Session["User_Name"].ToString();
                objLeaveBAL.Modified_Date = System.DateTime.Now;
                objLeaveBAL.Parameter = 1;
                if (objLeaveBAL.Insert_HRMS_Leaves_Master() != 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);
                    ClearFields();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
                }

            }
            catch (Exception ex)
            {

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            { }
        }
        // Editing Header Details
        public void Edit_HeaderDetails(string Leavecode, string CreationCompany)
        {
            try
            {

                var objLeaveID = objLeaveBAL.Get_Leave(Leavecode, CreationCompany);
                if (objLeaveID.Count > 0)
                {
                    txtID.Text = objLeaveID[0].Leave_Code.ToString();
                    txtLeaveName.Text = objLeaveID[0].Leave_Name.ToString();
                    txtShortName.Text = objLeaveID[0].Short_Name.ToString();
                    //if (objLeaveID[0].Monthly.ToString() == "true")
                    //    //chbMonthly.Checked = true;
                    //else
                    //    chbMonthly.Checked = false;
                    txtMonthly.Text = objLeaveID[0].Monthly_Limit.ToString();
                    txtEncashingLimit.Text = objLeaveID[0].Encashment_Balance.ToString();
                    if (objLeaveID[0].Balnces_Transfer.ToString() == "true")
                        chbBalances.Checked = true;
                    else
                        chbBalances.Checked = false;
                    ddlAllotment.Text = objLeaveID[0].Allotment.ToString();
                    txtPerOfBasic.Text = objLeaveID[0].Noof_leaves_Per_Year.ToString();
                   
                    txtdays.Text = objLeaveID[0].No_Of_Days.ToString();

                    btnSave.Visible = false;
                    btnUpdate.Visible = true;
                    Session["Leave_code"] = null;


                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('No Data Found ');", true);
                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }

        //Updating Method
        public void Update_Method()
        {
            try
            {

                objLeaveBAL.Leave_Code = txtID.Text;
                objLeaveBAL.Leave_Name = txtLeaveName.Text;
                objLeaveBAL.Short_Name = txtShortName.Text;
                //objLeaveBAL.Monthly = (chbMonthly.Checked == true) ? "true" : "false";
                objLeaveBAL.Monthly_Limit = (txtMonthly.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtMonthly.Text);
                objLeaveBAL.Encashment = (chbEncashment.Checked == true) ? "true" : "false";
                objLeaveBAL.Balnces_Transfer = (chbBalances.Checked == true) ? "true" : "false";
                objLeaveBAL.Encashment_Balance = (txtEncashingLimit.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtEncashingLimit.Text);
                objLeaveBAL.Allotment = ddlAllotment.Text;
                objLeaveBAL.Noof_leaves_Per_Year = (txtPerOfBasic.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtPerOfBasic.Text);
                objLeaveBAL.twoStage = (chktwostages.Checked == true) ? "Yes" : "No";
                objLeaveBAL.Authority1 = ddlauthority1.Text;
                objLeaveBAL.Username1 = txtusername.Text;
                objLeaveBAL.Authority2 = ddlauthority2.Text;
                objLeaveBAL.Username2 = txtusername2.Text;
                objLeaveBAL.Period = ddlperiod.Text;
                objLeaveBAL.No_Of_Times = (txtnooftimes.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtnooftimes.Text);
                objLeaveBAL.No_Of_Days = (txtdays.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtdays.Text);
                objLeaveBAL.Authority3 = ddlauthority3.Text;
                objLeaveBAL.UserName3 = txtusername3.Text;
                objLeaveBAL.Created_By = Session["User_Name"].ToString();
                objLeaveBAL.Created_Date = System.DateTime.Now;
                objLeaveBAL.Creation_Company = Session["ClientRegistrationNo"].ToString();
                objLeaveBAL.Modified_By = Session["User_Name"].ToString();
                objLeaveBAL.Modified_Date = System.DateTime.Now;
                objLeaveBAL.Creation_Company = Session["ClientRegistrationNo"].ToString();
                objLeaveBAL.Created_By = Session["User_Name"].ToString();
                objLeaveBAL.Created_Date = System.DateTime.Now;
                objLeaveBAL.Modified_By = Session["User_Name"].ToString();
                objLeaveBAL.Modified_Date = System.DateTime.Now;
                objLeaveBAL.Parameter = 2;

                if (objLeaveBAL.Insert_HRMS_Leaves_Master() != 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Updated Successfully');", true);
                    ClearFields();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
                }


            }
            catch (Exception ex)
            {

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved ');", true);
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            {

            }

        }

        #endregion methods    

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Saving();

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearFields();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Update_Method();
        }

        protected void chkRecommamd_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRecommamd.Checked)
            {
                recommadDiv.Visible = false;
                string ClientRegistrationNo = Session["ClientRegistrationNo"].ToString();
                string usename = Session["User_Name"].ToString();
                var emp = (from empl in ObjDAL.HRMS_Employee_Master_Official_Informations
                           where empl.Emp_Code == usename && empl.Creation_Company == ClientRegistrationNo
                           select empl).ToList();
                //ddlauthority1.SelectedValue = emp[0].Reporting_Authority;
                //txtusername.Text = emp[0];
                if (emp.Count > 0)
                {
                    var emp2 = (from em in ObjDAL.HRMS_Employee_Master_PersonalDetails
                                where (em.Emp_First_Name + " " + em.Emp_Middle_Name + " " + em.Emp_Last_Name) == emp[0].Reporting_Authority
                                && em.Creation_Company == ClientRegistrationNo
                                select em).ToList();
                    if (emp2 != null)
                    {
                        ddlauthority1.SelectedValue = emp2[0].Emp_Code;
                        txtusername.Text = emp2[0].User_Name;
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('You dont have authority person plz select ur authority');", true);
                    recommadDiv.Visible = true;
                    chkRecommamd.Checked = false;
                    ddlauthority1.Focus();
                }
            }
            else
            {
                recommadDiv.Visible = true;
                ddlauthority1.SelectedIndex =0;
                txtusername.Text = "";
            }
        }

        //protected void chbMonthly_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (chbMonthly.Checked == true)
        //    {
        //        txtMonthly.ReadOnly = false;
        //    }
        //    else
        //    {
        //        txtMonthly.ReadOnly = true;
        //    }
        //}

        protected void chbEncashment_CheckedChanged(object sender, EventArgs e)
        {
            if (chbEncashment.Checked == true || chbBalances.Checked == true)
            {
                txtEncashingLimit.ReadOnly = false;
            }
            else
            {
                txtEncashingLimit.ReadOnly = true;
            }
        }

        protected void txtLeaveName_TextChanged(object sender, EventArgs e)
        {
            string CreationCompany = Session["ClientRegistrationNo"].ToString();
            // string gname = txtGroupName.Text.ToLower().Trim();
            string GroupName = (txtLeaveName.Text.ToLower().Trim() == "") ? "" : txtLeaveName.Text.ToLower().Trim();
            List<HRMS_Leave_Master> objitemgroupDUP = (from itmes in ObjDAL.GetTable<HRMS_Leave_Master>()
                                                       where itmes.Creation_Company == CreationCompany &&
                                                                          itmes.Leave_Name.ToLower().Trim() == GroupName
                                                       select itmes).ToList<HRMS_Leave_Master>();
            if (objitemgroupDUP.Count > 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Leave Name Already Existed');", true);
                txtLeaveName.Text = "";
                txtLeaveName.Focus();
            }
            else
            {
                txtShortName.Focus();
            }
        }

        protected void txtShortName_TextChanged(object sender, EventArgs e)
        {
            string CreationCompany = Session["ClientRegistrationNo"].ToString();
            // string gname = txtGroupName.Text.ToLower().Trim();
            string GroupName = (txtShortName.Text.ToLower().Trim() == "") ? "" : txtShortName.Text.ToLower().Trim();
            List<HRMS_Leave_Master> objitemgroupDUP = (from itmes in ObjDAL.GetTable<HRMS_Leave_Master>()
                                                       where itmes.Creation_Company == CreationCompany &&
                                                                          itmes.Short_Name.ToLower().Trim() == GroupName
                                                       select itmes).ToList<HRMS_Leave_Master>();
            if (objitemgroupDUP.Count > 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Short Name Already Existed');", true);
                txtShortName.Text = "";
                txtShortName.Focus();
            }

        }

        protected void ddlauthority_SelectedIndexChanged(object sender, EventArgs e)
        {
            string CreationCompany = Session["ClientRegistrationNo"].ToString();
            string empcode = ddlauthority1.SelectedValue;

            var employee = (from emp in ObjDAL.HRMS_Employee_Master_PersonalDetails
                            where emp.Creation_Company == CreationCompany && emp.Emp_Code== empcode
                            select emp

                            ).ToList();

            if(employee.Count>0)
            {
                txtusername.Text = employee[0].User_Name;
            }
            else
            {

            }
        }

        protected void ddlauthority2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string CreationCompany = Session["ClientRegistrationNo"].ToString();
            string empcode = ddlauthority2.SelectedValue;

            var employee = (from emp in ObjDAL.HRMS_Employee_Master_PersonalDetails
                            where emp.Creation_Company == CreationCompany && emp.Emp_Code == empcode
                            select emp

                            ).ToList();

            if (employee.Count > 0)
            {
                txtusername2.Text = employee[0].User_Name;
            }
            else
            {

            }
        }
        protected void ddlauthority3_SelectedIndexChanged(object sender, EventArgs e)
        {
            string CreationCompany = Session["ClientRegistrationNo"].ToString();
            string empcode = ddlauthority3.SelectedValue;

            var employee = (from emp in ObjDAL.HRMS_Employee_Master_PersonalDetails
                            where emp.Creation_Company == CreationCompany && emp.Emp_Code == empcode
                            select emp

                            ).ToList();

            if (employee.Count > 0)
            {
                txtusername3.Text = employee[0].User_Name;
            }
            else
            {

            }
        }
        public void getemployee()
        {
            string CreationCompany = Session["ClientRegistrationNo"].ToString();

            var employee = (from emp in ObjDAL.HRMS_Employee_Master_PersonalDetails
                            where emp.Creation_Company == CreationCompany
                            select                           
                                emp.Emp_Code
                               
                            ).ToList();
            if(employee.Count>0)
            {
                ddlauthority1.DataSource = employee;
                ddlauthority1.DataBind();
                ddlauthority1.Items.Insert(0, new ListItem("Select Emp Code", "0"));
                //emp code 
                ddlauthority2.DataSource = employee;
                ddlauthority2.DataBind();
                ddlauthority2.Items.Insert(0, new ListItem("Select Emp Code", "0"));

                ddlauthority3.DataSource = employee;
                ddlauthority3.DataBind();
                ddlauthority3.Items.Insert(0, new ListItem("Select Emp Code", "0"));
            }
            else
            {
            }


        }
    }
}