﻿using BAL;
using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Payroll
{
    public partial class Leave_Application_List : System.Web.UI.Page
    {
        // // // Object Creation For Database
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();
        DefineLeavesBAL objSCM_LeaveBAL = new DefineLeavesBAL();

        // // /// Object Creation For Item Group Domain

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["User_Type"].ToString() == "User")
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('You Don't have Permissions To Create..');", true);
                    Response.Redirect("~/Dashboard/Dashboard.aspx");
                }
                {
                    if (!IsPostBack)
                    {
                        if (Session["ClientRegistrationNo"] == null)
                        {
                            Response.Redirect("~/Default.aspx");
                        }
                        txtSearch.Focus();

                        //Bind User Registration
                        Bind_Grid();
                    }
                }
            }
        }


        #region Button Events

        // // // Navigation to User Registration Form For Created New User 
        protected void btnAdd_Click(object sender, EventArgs e)
        {

            Response.Redirect("~/Payroll/Define_Leaves.aspx");

        }

        /// <summary>
        /// Search For UserRegistration Details
        /// </summar
        /// 

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                string StrSearch = txtSearch.Text.Trim().ToLower();
                string CompanyName = Session["ClientRegistrationNo"].ToString().Trim().ToLower();
                string query = "%" + StrSearch + "%";
                List<HRMSLeaveApplication> ObjHeadInfo = (from HeaderInfo in objDAL.HRMSLeaveApplications
                                                            where HeaderInfo.CreationCompany.ToLower().Trim() == CompanyName.ToLower().Trim() &&
                                                SqlMethods.Like(HeaderInfo.EmpCode.ToLower().Trim(), query.ToLower().Trim()) ||
                                               
                                                    SqlMethods.Like(HeaderInfo.ReportingAuthority.ToString().ToLower().Trim(), query.ToLower().Trim())||
                                                    SqlMethods.Like(HeaderInfo.TypeOfLeaveApple.ToString().ToLower().Trim(), query.ToLower().Trim())||
                                                    SqlMethods.Like(HeaderInfo.LeaveStatus.ToString().ToLower().Trim(), query.ToLower().Trim())
                                                    select HeaderInfo).ToList();
              
                if (ObjHeadInfo.Count > 0)
                {
                    grdLeaveApplicationList.DataSource = ObjHeadInfo;
                    grdLeaveApplicationList.DataBind();
                }
                else
                {
                    SetInitialRow();
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.ToString() + "');", true);
            }
        }

        /// <summary>
        /// Navigation for User Define Leave create
        /// </summar
        /// 
        protected void lkbAddHeader_Click(object sender, EventArgs e)
        {

            Response.Redirect("~/Payroll/Leave_Application.aspx");
        }




        #endregion

        #region Methods

        /// <summary>
        /// Bind Grid Based on User Registration Details
        /// Created By Divya
        /// </summary>
        /// <param name="sender"></param>
        public void Bind_Grid()
        {
            try
            {
                string Cmpy = (string)Session["ClientRegistrationNo"];
                string Brnach = (string)Session["BranchID"];
                string UserType = (string)Session["User_Type"];
                string UserName = (string)Session["User_Name"];
                string Cration_Company = (string)Session["CreationCompany"]; //"000001";
                                                                             //if (UserType == "User")
                                                                             //{
                if (Session["User_Type"].ToString() == "Employees")
                {
                    var objHeaderInfo = (from header in objDAL.HRMSLeaveApplications
                                         where header.CreationCompany == Cmpy &&  header.EmpCode== Session["Emp_Code"].ToString()
                                         orderby header.ID descending
                                         select header).ToList();
                    //// List<Administrator_UserRegistration> ObjuserInfo = objSCM_userRegistration.Get_userRegDetails(Session["ClientRegistrationNo"].ToString());
                    if (objHeaderInfo.Count > 0)
                    {
                        grdLeaveApplicationList.DataSource = objHeaderInfo;
                        grdLeaveApplicationList.DataBind();
                    }
                    else
                    {
                        // // // Grid view using setting initial rows displaying Empty Header Displying
                        SetInitialRow();
                    }
                }
                else {
                    var objHeaderInfo = (from header in objDAL.HRMSLeaveApplications
                                         where header.CreationCompany == Cmpy
                                         orderby header.ID descending
                                         select header).ToList();
                    //// List<Administrator_UserRegistration> ObjuserInfo = objSCM_userRegistration.Get_userRegDetails(Session["ClientRegistrationNo"].ToString());
                    if (objHeaderInfo.Count > 0)
                    {
                        grdLeaveApplicationList.DataSource = objHeaderInfo;
                        grdLeaveApplicationList.DataBind();
                    }
                    else
                    {
                        // // // Grid view using setting initial rows displaying Empty Header Displying
                        SetInitialRow();
                    }
                }
           

            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }

        }



        /// <summary>
        /// Setting Initial Empty Row Data Into Header Details 
        /// </summary>
        /// <param name="sender"></param>
        public void SetInitialRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("Emp_Code", typeof(string)));
            dt.Columns.Add(new DataColumn("Emp_Name", typeof(string)));
            dt.Columns.Add(new DataColumn("Department", typeof(string)));
            dt.Columns.Add(new DataColumn("Designation", typeof(string)));
            dt.Columns.Add(new DataColumn("Reporting_Authority", typeof(string)));
            dt.Columns.Add(new DataColumn("TypeOfLeaveApple", typeof(string)));
            dt.Columns.Add(new DataColumn("Leave_Status", typeof(string)));
            dr = dt.NewRow();

            dr["Emp_Code"] = string.Empty;
            dr["Emp_Name"] = string.Empty;
            dr["Department"] = string.Empty;
            dr["Designation"] = string.Empty;
            dr["Reporting_Authority"] = string.Empty;
            dr["TypeOfLeaveApple"] = string.Empty;
            dr["Leave_Status"] = string.Empty;
            dt.Rows.Add(dr);

            grdLeaveApplicationList.DataSource = dt;
            grdLeaveApplicationList.DataBind();

            int columncount = 7;
            grdLeaveApplicationList.Rows[0].Cells.Clear();
            grdLeaveApplicationList.Rows[0].Cells.Add(new TableCell());
            grdLeaveApplicationList.Rows[0].Cells[0].ColumnSpan = columncount;
            grdLeaveApplicationList.Rows[0].Cells[0].Text = "No Records Found";
            //  grdStorageLocation.HeaderStyle.HorizontalAlign = "center";
            grdLeaveApplicationList.Rows[0].Cells[0].ForeColor = System.Drawing.Color.Red;
        }

        /// <summary>
        ///  The Following Method is used for Delete Grid records to Data From User Registration  Table
        ///  Created By Divya
        /// </summary>
        public void Delete_HeaderDetails(string AdditionCode, string Creation_Company)
        {
            try
            {


            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.ToString() + "');", true);
            }
            finally
            {
            }
        }

        #endregion

        #region User Registration Details Bind For Grid view


        /// <summary>
        /// Row Command Grid view  For UserRegistration Details
        /// Created By Divya
        /// </summar
        /// 

        protected void grdLeaveApplicationList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string LeaveCode = e.CommandArgument.ToString();
            if (e.CommandName == "Edit")
            {
                string path = "~/Payroll/Leave_Application.aspx";
                Session["Leave_code"] = LeaveCode;
                Response.Redirect(path);

            }
            if (e.CommandName == "Delete")
            {

                string Creation_Company = Session["ClientRegistrationNo"].ToString();

                Delete_HeaderDetails(LeaveCode, Creation_Company);

            }


        }


        /// <summary>
        /// Paging For User Registration Details
        /// </summar
        /// 
        protected void grdLeaveApplicationList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdLeaveApplicationList.PageIndex = e.NewPageIndex;
            Bind_Grid();
        }


        /// <summary>
        /// Row Delte Grid view  For UserRegistration
        /// </summary>
        /// 
        protected void grdLeaveApplicationList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        /// <summary>
        /// Row Editing Grid view  For UserRegistration
        /// </summary>
        /// 

        protected void grdLeaveApplicationList_RowEditing(object sender, GridViewEditEventArgs e)
        {


        }

        /// <summary>
        /// Row Sorting Grid view  For UserRegistration Details
        /// </summar
        /// 

        protected void grdLeaveApplicationList_Sorting(object sender, GridViewSortEventArgs e)
        {
            //string Sortdir = GetSortDirection(e.SortExpression);
            //string SortExp = e.SortExpression;
            //String Creation_Company = Session["ClientRegistrationNo"].ToString();
            //List<Administrator_UserRegistration> list = objSCM_userRegistration.Get_UserRegistration(Creation_Company);


            //if (Sortdir == "ASC")
            //{
            //    list = Sort<Administrator_UserRegistration>(list, SortExp, SortDirection.Ascending);
            //}
            //else
            //{
            //    list = Sort<Administrator_UserRegistration>(list, SortExp, SortDirection.Descending);
            //}
            //this.grdUserRegistration.DataSource = list;
            //this.grdUserRegistration.DataBind();
        }

        //private List<Administrator_UserRegistration> Sort<TKey>(List<Administrator_UserRegistration> list, string sortBy, SortDirection direction)
        //{
        //    PropertyInfo property = list.GetType().GetGenericArguments()[0].GetProperty(sortBy);
        //    if (direction == SortDirection.Ascending)
        //    {
        //        return list.OrderBy(e => property.GetValue(e, null)).ToList<Administrator_UserRegistration>();
        //    }
        //    else
        //    {
        //        return list.OrderByDescending(e => property.GetValue(e, null)).ToList<Administrator_UserRegistration>();
        //    }
        //}

        //  GetSortDirection Method it using PRoduct Grid Sorting  
        //private string GetSortDirection(string column)
        //{
        //    string sortDirection = "ASC";
        //    string sortExpression = ViewState["SortExpression"] as string;
        //    if (sortExpression != null)
        //    {
        //        if (sortExpression == column)
        //        {
        //            string lastDirection = ViewState["SortDirection"] as string;
        //            if ((lastDirection != null) && (lastDirection == "ASC"))
        //            {
        //                sortDirection = "DESC";
        //            }
        //        }
        //    }
        //    ViewState["SortDirection"] = sortDirection;
        //    ViewState["SortExpression"] = column;
        //    return sortDirection;
        //}

        #endregion



    }
}