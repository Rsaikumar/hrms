﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="Define_Leave_List.aspx.cs" Inherits="HRMS.Payroll.Define_Leave_List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<script type="text/javascript" lang="javascript">
        function Search() {
            if (document.getElementById('<%=txtSearch.ClientID%>').value == "") {
                alert("Search Box Can't be Empty ... ");
                document.getElementById('<%=txtSearch.ClientID%>').focus();
                return false;
            }
            return true;
        }

    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <section id="main-content">
                <section class="wrapper">
                    <%--<div class="page-header">
                        <div class="pull-left Search_Div" id="btnSearch_Div">
                            <div class="Search_Icon">
                                <asp:TextBox ID="txtSearch" runat="server" placeholder="Search For Define Leave Types List.."
                                    class="form-control txtSearch"></asp:TextBox>
                            </div>
                        </div>
                        <div class="pull-left">
                            <asp:Button ID="btnSearch" runat="server" class="search_Btn" OnClientClick="return Search()" OnClick="btnSearch_Click" />
                        </div>

                        <div class="pull-right">
                            <ul class="stats">
                                <li>
                                    <asp:Button ID="btnAdd" runat="server" class="add_Btn" OnClick="btnAdd_Click" ToolTip="Create Define Leave Types Details" /></li>
                                <li>
                                    <asp:Button ID="btnPdf" runat="server" class="PDF_Btn" ToolTip="Export Define Leave Types Details To Pdf" /></li>
                                <li>
                                    <asp:Button ID="btnExcel" runat="server" class="Excel_Btn" ToolTip="Export Define Leave Types Details To Excel" /></li>
                            </ul>
                        </div>
                    </div>--%>
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="breadcrumbs-alt">
                                <li><a href="Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                                <li><a href="#">Payroll</a><i class="fa fa-angle-right"></i> </li>
                                <li><a href="Define_Leave_List.aspx" class="current">Define Leaves Types List</a> </li>
                            </ul>
                        </div>
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-md-12">
                                    <section class="panel">
                                        <header class="panel-heading">
                                            <i class="fa fa-th-list"> Define Leaves Types List</i>
                                        </header>
                                        <div class="panel-body">
                                            <div class="col-sm-12" style="margin-bottom: 0.3%;">
                                                <div class="row">
                                                    <asp:GridView ID="grdDefineLeaveTypesList" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                                        Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%;" runat="server"
                                                        AutoGenerateColumns="false" DataKeyNames="Leave_Code" EmptyDataText="No Records Found"
                                                        ShowFooter="false" AllowPaging="true"
                                                        PageSize="15" AllowSorting="true"
                                                        OnRowCommand="grdDefineLeaveTypesList_RowCommand"
                                                        OnSorting="grdDefineLeaveTypesList_Sorting"
                                                        OnPageIndexChanging="grdDefineLeaveTypesList_PageIndexChanging"
                                                        OnRowDeleting="grdDefineLeaveTypesList_RowDeleting"
                                                        OnRowEditing="grdDefineLeaveTypesList_RowEditing">
                                                        <AlternatingRowStyle BackColor="#EEEEEE" />
                                                        <PagerStyle BackColor="#EEEEEE" ForeColor="Black" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                        <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                        <SortedDescendingHeaderStyle BackColor="#000065" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="S.No" SortExpression="">
                                                                <ItemTemplate>
                                                                    <%#Container.DataItemIndex+1 %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Leave Code">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblLeaveCode" runat="server" Text='<%#Eval("Leave_Code") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Leave Name">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblLeaveName" runat="server" Text='<%#Eval("Leave_Name") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Short Name">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblShortName" runat="server" Text='<%#Eval("Short_Name") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lbkEdit" runat="server" CommandName="Edit" Style="color: gray; text-decoration: none; float: left; background-image: url('../img/file_edit.png'); height: 30px; width: 35px; background-position: left; background-repeat: no-repeat;"
                                                                        OnClientClick="return confirm('Are you sure you want to Edit this Record ...?')" CausesValidation="false"
                                                                        CommandArgument='<%#Eval("Leave_Code") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete" Style="color: gray; text-decoration: none; float: left; background-image: url('../img/delete.png'); height: 30px; width: 35px; background-position: left; background-repeat: no-repeat;"
                                                                        OnClientClick="return confirm('Are you sure you want to Remove this Record ...?')" CausesValidation="false"
                                                                        CommandArgument='<%#Eval("Leave_Code") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                                <div class="container">
                                                    <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Create new leave type Click Here</asp:LinkButton>
                                                </div>

                                            </div>

                                        </div>
                                    </section>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </section>
            </section>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
