﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BAL;
using System.Globalization;

namespace HRMS.Payroll
{
    public partial class LeaveApproval : System.Web.UI.Page
    {
        #region declarations

        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();
        LeaveApplicationBAL objLeaveApp = new LeaveApplicationBAL();

        #endregion

        #region events
        protected void Page_Load(object sender, EventArgs e)
        {
            leaveapplicationdetails();
            ButtonHide();
           
            noofLeavesid.Visible = false;
        }

        protected void btnapprove_Click(object sender, EventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(Session["LeaveapplicationNo"]);
                objLeaveApp.ID = id;
                objLeaveApp.LeaveStatus = "Approved";
                objLeaveApp.ModifiedBy = Session["Emp_Code"].ToString();
                objLeaveApp.ModifiedDate = System.DateTime.Now;
                objLeaveApp.Parameter = 2;
                if (objLeaveApp.Insert_HRMS_Leave_Application() != 0)
                {
                    var leaveapplication = (from master in ObjDAL.USP_getdetailsinleaveapproval(Convert.ToInt32(Session["LeaveapplicationNo"]))
                                            select master).ToList();

                    if (leaveapplication.Count > 0)
                    {
                        string empcode = leaveapplication[0].EmpCode;
                        var balanceleave = (from item in ObjDAL.HRMSLeaveAllotments
                                            where item.EmpCode == empcode
                                            select item).ToList();
                        if (balanceleave.Count > 0)
                        {
                            decimal bl = Convert.ToDecimal(balanceleave[0].NoofLeavs);
                            decimal totaldays = Convert.ToDecimal(lblnoofdays.InnerText);
                            decimal totalbalance = bl - totaldays;
                            HRMSLeaveAllotment objallotment = ObjDAL.HRMSLeaveAllotments.Single(a => a.EmpCode == empcode && a.LeaveName == lblleavetype.InnerText);
                            objallotment.NoofLeavs = totalbalance;
                            ObjDAL.SubmitChanges();
                        }
                    }

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert(' Application Has Been Approved ');window.location ='../Dashboard/Dashboard.aspx';", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            { }
        }

        protected void btnreject_Click(object sender, EventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(Session["LeaveapplicationNo"]);
                objLeaveApp.ID = id;
                objLeaveApp.LeaveStatus = "Rejected";
                objLeaveApp.LeaveFrom =Convert.ToDateTime(lblfromdate.InnerText);
                objLeaveApp.LeaveTo = Convert.ToDateTime(lbltodate.InnerText); 
                objLeaveApp.CreatedDate = System.DateTime.Now;
                objLeaveApp.ModifiedBy = Session["Emp_Code"].ToString();
                objLeaveApp.ModifiedDate = System.DateTime.Now;
                objLeaveApp.Parameter = 2;
                if (objLeaveApp.Insert_HRMS_Leave_Application() != 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert(' Application Has Been Rejected ');window.location ='../Dashboard/Dashboard.aspx';", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            { }
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Payroll/LeaveApplication.aspx");
        }

        #endregion

        #region methods

        public void leaveapplicationdetails()
        {
            var s = (Session["LeaveapplicationNo"]);
            var leaveapplication = (from master in ObjDAL.USP_getdetailsinleaveapproval(Convert.ToInt32(Session["LeaveapplicationNo"]))

                                    select master).ToList();
     
            if (leaveapplication.Count > 0)
            {
                lblempname.InnerText = leaveapplication[0].empname;
                lbldept.InnerText = Convert.ToString(leaveapplication[0].department);
                lbldesignation.InnerText = Convert.ToString(leaveapplication[0].designation);
                lblleavetype.InnerText = leaveapplication[0].leavetype;
                lblfromdate.InnerText = Convert.ToDateTime(leaveapplication[0].fromdate).ToString("dd-MM-yyyy");
                lbltodate.InnerText = Convert.ToDateTime(leaveapplication[0].todate).ToString("dd-MM-yyyy");
                lblnoofleavesavailable.InnerText = Convert.ToString(leaveapplication[0].noOfleave);
                lblnoofdays.InnerText = Convert.ToString(leaveapplication[0].totaldays);
                lblstatus.InnerText = leaveapplication[0].leavestatus;
                lbldate.InnerText = Convert.ToDateTime(leaveapplication[0].AppliedDate).ToString("dd-MM-yyyy");
                authoritybind(Convert.ToInt32( leaveapplication[0].EmpCode));
                
            }
        }

        public void authoritybind(int id)
        {
            try
            {
                var RA = (from item in ObjDAL.HRMSLeaveApplications
                          where item.ID == id
                          select item).ToList();
                if (RA.Count > 0)
                {
                    string Empcode = RA[0].EmpCode;

                    var ObjRep = (from item in ObjDAL.HRMSEmployeeMasterOfficialInformationDetails
                                  where item.EmpId == Convert.ToString(Empcode)
                                  select item).ToList();
                    if(ObjRep.Count > 0)
                    {
                        string repauth = ObjRep[0].ReportingAuthority;

                        var repaut = (from item in ObjDAL.USP_gettingemployeenameandid(Convert.ToInt32(repauth))
                                      select item).ToList();

                        lblreportingauthority.InnerText = repaut[0].empname;
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        public void ButtonHide()
        {
            int id = Convert.ToInt32(Session["LeaveapplicationNo"]);
            var RA = (from item in ObjDAL.HRMSLeaveApplications
                      where item.ID == id
                      select item).ToList();
            if (RA.Count > 0)
            {
                string reporting = RA[0].ReportingAuthority;
                string sess = Session["Emp_Code"].ToString();
                if (reporting == sess)
                {
                    btnapprove.Visible = true;
                    btnreject.Visible = true;
                }
                else
                {
                    btnapprove.Visible = false;
                    btnreject.Visible = false;
                }
            }
        }

        #endregion
    }
}