﻿using BAL;
using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Payroll
{
    public partial class Define_Leave_List : System.Web.UI.Page
    {
        // // // Object Creation For Database

        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();
        DefineLeavesBAL objSCM_LeaveBAL = new DefineLeavesBAL();

        // // /// Object Creation For Item Group Domain

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ClientRegistrationNo"] == null)
            {
                Response.Redirect("~/Default.aspx");
            }
          //  txtSearch.Focus();

            //Bind User Registration
            Bind_Grid();
        }
        


        #region Button Events

        // // // Navigation to User Registration Form For Created New User 
        protected void btnAdd_Click(object sender, EventArgs e)
        {

            Response.Redirect("~/Payroll/Define_Leaves.aspx");

        }

        /// <summary>
        /// Search For UserRegistration Details
        /// </summar
        /// 

        //protected void btnSearch_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //       // string StrSearch = txtSearch.Text.Trim().ToLower();
        //        string CompanyName = Session["ClientRegistrationNo"].ToString().Trim().ToLower();
        //        List<HRMS_Leave_Master> objLeaveSearch = objSCM_LeaveBAL.GetData_search_LeaveInfo(StrSearch, CompanyName);
        //        // objSCM_LeaveBAL.GetData_search_HeaderInfo(StrSearch, CompanyName);

        //        if (objLeaveSearch.Count > 0)
        //        {
        //            grdDefineLeaveTypesList.DataSource = objLeaveSearch;
        //            grdDefineLeaveTypesList.DataBind();
        //        }
        //        else
        //        {
        //            SetInitialRow();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.ToString() + "');", true);
        //    }
        //}

        /// <summary>
        /// Navigation for User Define Leave create
        /// </summar
        /// 
        protected void lkbAddHeader_Click(object sender, EventArgs e)
        {

            Response.Redirect("~/Payroll/Define_Leaves.aspx");
        }




        #endregion

        #region Methods

        /// <summary>
        /// Bind Grid Based on User Registration Details
        /// Created By Divya
        /// </summary>
        /// <param name="sender"></param>
        public void Bind_Grid()
        {
            try
            {
                string Cmpy = (string)Session["ClientRegistrationNo"];
                string Brnach = (string)Session["BranchID"];
                string UserType = (string)Session["User_Type"];
                string UserName = (string)Session["User_Name"];
                string Cration_Company = (string)Session["CreationCompany"]; //"000001";
                //if (UserType == "User")
                //{
                var objHeaderInfo = (from header in objDAL.HRMSLeaveMasters
                                     where header.CreationCompany == Cmpy
                                     orderby header.ID descending
                                     select header).ToList();
                // List<Administrator_UserRegistration> ObjuserInfo = objSCM_userRegistration.Get_userRegDetails(Session["ClientRegistrationNo"].ToString());
                if (objHeaderInfo.Count > 0)
                {
                    grdDefineLeaveTypesList.DataSource = objHeaderInfo;
                    grdDefineLeaveTypesList.DataBind();
                }
                else
                {
                    // // // // Grid view using setting initial rows displaying Empty Header Displying
                    SetInitialRow();
                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }

        }



        /// <summary>
        /// Setting Initial Empty Row Data Into Header Details 
        /// </summary>
        /// <param name="sender"></param>
        public void SetInitialRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("Leave_Code", typeof(string)));
            dt.Columns.Add(new DataColumn("Leave_Name", typeof(string)));
            dt.Columns.Add(new DataColumn("Short_Name", typeof(string)));

            dr = dt.NewRow();

            dr["Leave_Code"] = string.Empty;
            dr["Leave_Name"] = string.Empty;
            dr["Short_Name"] = string.Empty;

            dt.Rows.Add(dr);

            grdDefineLeaveTypesList.DataSource = dt;
            grdDefineLeaveTypesList.DataBind();

            int columncount = 7;
            grdDefineLeaveTypesList.Rows[0].Cells.Clear();
            grdDefineLeaveTypesList.Rows[0].Cells.Add(new TableCell());
            grdDefineLeaveTypesList.Rows[0].Cells[0].ColumnSpan = columncount;
            grdDefineLeaveTypesList.Rows[0].Cells[0].Text = "No Records Found";
            //  grdStorageLocation.HeaderStyle.HorizontalAlign = "center";
            grdDefineLeaveTypesList.Rows[0].Cells[0].ForeColor = System.Drawing.Color.Red;
        }

        /// <summary>
        ///  The Following Method is used for Delete Grid records to Data From User Registration  Table
        ///  Created By Divya
        /// </summary>
        public void Delete_HeaderDetails(string AdditionCode, string Creation_Company)
        {
            try
            {
                //string Cmpy = (string)Session["ClientRegistrationNo"];
                //string Brnach = (string)Session["BranchID"];
                //string UserType = (string)Session["User_Type"];
                //string UserName = (string)Session["User_Name"];
                //string Cration_Company = (string)Session["CreationCompany"]; 
                ////{
                //var objHeaderInfo = (from header in objDAL.HRMS_Leave_Masters
                //                     where header.Creation_Company == Cmpy && header.Leave_Code==
                                   
                //                     select header).ToList();

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.ToString() + "');", true);
            }
            finally
            {
            }
        }

        #endregion

        #region User Registration Details Bind For Grid view


        /// <summary>
        /// Row Command Grid view  For UserRegistration Details
        /// Created By Divya
        /// </summar
        /// 

        protected void grdDefineLeaveTypesList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string LeaveCode = e.CommandArgument.ToString();
             string Cmpy = (string)Session["ClientRegistrationNo"];
                string Brnach = (string)Session["BranchID"];
                string UserType = (string)Session["User_Type"];
                string UserName = (string)Session["User_Name"];
                string Cration_Company = (string)Session["CreationCompany"]; 
            if (e.CommandName == "Edit")
            {
                string path = "~/Payroll/Define_Leaves.aspx";
                Session["Leave_code"] = LeaveCode;
                Response.Redirect(path);

            }
            if (e.CommandName == "Delete")
            {

                string Creation_Company = Session["ClientRegistrationNo"].ToString();
               // string path = "~/Payroll/Define_Leaves.aspx";
                Session["Leave_code"] = LeaveCode;
                var objHeaderInfo = (from header in objDAL.HRMSLeaveMasters
                                     where header.ID == Convert.ToInt32( LeaveCode)
                                     select header).First();
                objDAL.HRMSLeaveMasters.DeleteOnSubmit(objHeaderInfo);
                objDAL.SubmitChanges();
                   //Delete_HeaderDetails(LeaveCode, Creation_Company);
                   Bind_Grid();
                //Response.Redirect(path);
            }


        }


        /// <summary>
        /// Paging For User Registration Details
        /// </summar
        /// 
        protected void grdDefineLeaveTypesList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdDefineLeaveTypesList.PageIndex = e.NewPageIndex;
            Bind_Grid();
        }


        /// <summary>
        /// Row Delte Grid view  For UserRegistration
        /// </summary>
        /// 
        //public void Delete_HeaderDetails(string LeaveCode, string Creation_Company)
        //{
        //    //HRMS_Leave_Master a = objDAL.HRMS_Leave_Masters.DeleteOnSubmit(LeaveCode);
        //}
        protected void grdDefineLeaveTypesList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        /// <summary>
        /// Row Editing Grid view  For UserRegistrationre
        /// </summary>
        /// 

        protected void grdDefineLeaveTypesList_RowEditing(object sender, GridViewEditEventArgs e)
        {


        }

        /// <summary>
        /// Row Sorting Grid view  For UserRegistration Details
        /// </summar
        /// 

        protected void grdDefineLeaveTypesList_Sorting(object sender, GridViewSortEventArgs e)
        {
            //string Sortdir = GetSortDirection(e.SortExpression);
            //string SortExp = e.SortExpression;
            //String Creation_Company = Session["ClientRegistrationNo"].ToString();
            //List<Administrator_UserRegistration> list = objSCM_userRegistration.Get_UserRegistration(Creation_Company);


            //if (Sortdir == "ASC")
            //{
            //    list = Sort<Administrator_UserRegistration>(list, SortExp, SortDirection.Ascending);
            //}
            //else
            //{
            //    list = Sort<Administrator_UserRegistration>(list, SortExp, SortDirection.Descending);
            //}
            //this.grdUserRegistration.DataSource = list;
            //this.grdUserRegistration.DataBind();
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Define_Leaves.aspx");
        }

        //private List<Administrator_UserRegistration> Sort<TKey>(List<Administrator_UserRegistration> list, string sortBy, SortDirection direction)
        //{
        //    PropertyInfo property = list.GetType().GetGenericArguments()[0].GetProperty(sortBy);
        //    if (direction == SortDirection.Ascending)
        //    {
        //        return list.OrderBy(e => property.GetValue(e, null)).ToList<Administrator_UserRegistration>();
        //    }
        //    else
        //    {
        //        return list.OrderByDescending(e => property.GetValue(e, null)).ToList<Administrator_UserRegistration>();
        //    }
        //}

        //  GetSortDirection Method it using PRoduct Grid Sorting  
        //private string GetSortDirection(string column)
        //{
        //    string sortDirection = "ASC";
        //    string sortExpression = ViewState["SortExpression"] as string;
        //    if (sortExpression != null)
        //    {
        //        if (sortExpression == column)
        //        {
        //            string lastDirection = ViewState["SortDirection"] as string;
        //            if ((lastDirection != null) && (lastDirection == "ASC"))
        //            {
        //                sortDirection = "DESC";
        //            }
        //        }
        //    }
        //    ViewState["SortDirection"] = sortDirection;
        //    ViewState["SortExpression"] = column;
        //    return sortDirection;
        //}

        #endregion




    }
}