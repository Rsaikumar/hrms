﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="Define_Leaves.aspx.cs" Inherits="HRMS.Payroll.Define_Leaves" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/Payroll/Define_leaves.css" rel="stylesheet" />   

    <script type="text/javascript" lang="javascript">
        function ControlValid() {
            if (document.getElementById('<%=txtLeaveName.ClientID%>').value == "") {
                alert("Please Enter Leave Name");

                document.getElementById('<%=txtLeaveName.ClientID%>').focus();

                return false;
            }
            if (document.getElementById('<%=txtShortName.ClientID%>').value == "") {
                alert("Please Enter Short Name");

                document.getElementById('<%=txtShortName.ClientID%>').focus();

                return false;
            }
            if (document.getElementById('<%=ddlAllotment.ClientID%>').options[document.getElementById('<%=ddlAllotment.ClientID%>').selectedIndex].value == 0) {
                alert("Please Select Allotment..");
                document.getElementById('<%=ddlAllotment.ClientID%>').focus();
                return false;
            }
            return true;
            if (document.getElementById('<%=ddlauthority1.ClientID%>').options[document.getElementById('<%=ddlauthority1.ClientID%>').selectedIndex].value == 0) {
                alert("Please Select Authority1..");
                document.getElementById('<%=ddlauthority1.ClientID%>').focus();
                return false;
            }
            return true;
            if (document.getElementById('<%=chktwostages.ClientID%>').checked) {
                if (document.getElementById('<%=ddlauthority2.ClientID%>').options[document.getElementById('<%=ddlauthority2.ClientID%>').selectedIndex].value == 0) {
                    alert("Please Select Authority2..");
                    document.getElementById('<%=ddlauthority1.ClientID%>').focus();
                    return false;
                }
            }
            return true;
        }
        function jsDecimals(e) {

            var evt = (e) ? e : window.event;
            var key = (evt.keyCode) ? evt.keyCode : evt.which;
            if (key != null) {
                key = parseInt(key, 10);
                if ((key < 48 || key > 57) && (key < 96 || key > 105)) {
                    if (!jsIsUserFriendlyChar(key, "Decimals")) {
                        return false;
                    }
                }
                else {
                    if (evt.shiftKey) {
                        return false;
                    }
                }
            }
            return true;
        }

        // Function to check for user friendly keys  
        //------------------------------------------
        function jsIsUserFriendlyChar(val, step) {
            // Backspace, Tab, Enter, Insert, and Delete  
            if (val == 8 || val == 9 || val == 13 || val == 45 || val == 46) {
                return true;
            }
            // Ctrl, Alt, CapsLock, Home, End, and Arrows  
            if ((val > 16 && val < 21) || (val > 34 && val < 41)) {
                return true;
            }
            if (step == "Decimals") {
                if (val == 190 || val == 110) {  //Check dot key code should be allowed
                    return true;
                }
            }
            // The rest  
            return false;
        }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div id="main">
        <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i></li>
                    <li>
                        <a href="">Payroll</a><i class="fa fa-angle-right"></i>
                    </li>
                    <li><a href="Define_Leave_List.aspx">Define Leave Types List</a> <i class="fa fa-angle-right"></i></li>
                    <li><a href="Define_Leaves.aspx">Define Leave Types</a> </li>
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-sm-12">

                            <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                                <div class="box-title">
                                    <h3>
                                        <i class="fa fa-th-list"></i>Define Leave Types</h3>
                                </div>
                                <div class="box-content">
                                    <form action="#" method="POST" class='form-horizontal form-bordered'>
                                        <div class="col-sm-12" style="">

                                            <div class="col-sm-12" style="">
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lAID" style="">
                                                        ID
                                                    </label>
                                                    <div class="col-sm-2 tAID" style="">
                                                        <asp:TextBox ID="txtID" runat="server" placeholder="ID" MaxLength="50"
                                                            class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lALeaveName" style="">
                                                        Leave Name<span id="Span1" runat="server" style="color: Red; display: inline;">*</span></label>
                                                    <div class="col-sm-2 tALeaveName" style="" id="">
                                                        <asp:TextBox ID="txtLeaveName" runat="server" TabIndex="0" placeholder="Leave Name" class="form-control"
                                                            MaxLength="50" AutoPostBack="true" OnTextChanged="txtLeaveName_TextChanged"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lAShortName" style="">
                                                        Short Name<span style="color: #ff0000; font-size: 14px; display: inline;">*</span></label>
                                                    <div class="col-sm-2 tAShortName" style="" id="">
                                                        <asp:TextBox ID="txtShortName" runat="server" TabIndex="1" placeholder="Short Name" class="form-control"
                                                            MaxLength="50" AutoPostBack="true" OnTextChanged="txtShortName_TextChanged"></asp:TextBox>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="col-sm-12" style="">                                                

                                                <div class="form-group">

                                                    <div class="col-sm-2 tBalances" style="" id="">
                                                        <asp:CheckBox ID="chbCertification" runat="server" Text="" AutoPostBack="true" OnCheckedChanged="chbEncashment_CheckedChanged" />
                                                    </div>
                                                   
                                                   <label for="textfield" class="control-label col-sm-1 lABalances" style="">
                                                       Document Evidence is required..?</label>
                                                </div>
                                                <div class="form-group">

                                                    <div class="col-sm-2 tEncashment" style="" id="">
                                                        <asp:CheckBox ID="chbEncashment" runat="server" Text="" AutoPostBack="true" OnCheckedChanged="chbEncashment_CheckedChanged" />
                                                    </div>
                                                    <label for="textfield" class="control-label col-sm-1 lAEncashment" style="">
                                                        Encashment Allowed..?</label>
                                                </div>
                                                <div class="form-group">

                                                    <div class="col-sm-2 tBalancestransfer" style="" id="">
                                                        <asp:CheckBox ID="chbBalances" runat="server" Text="" AutoPostBack="true" OnCheckedChanged="chbEncashment_CheckedChanged" />
                                                    </div>
                                                    <label for="textfield" class="control-label col-sm-1 lATransfer" style="">
                                                        Balance Transfer..?</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12" style="">
                                                
                                                <div class="form-group">

                                                    <label for="textfield" class="control-label col-sm-1 lABalanceTransfer" style="">
                                                        Balance Transfer/Encashing Limit</label>

                                                    <div class="col-sm-2 tBalanceTransfer" style="" id="">
                                                        <asp:TextBox ID="txtEncashingLimit" runat="server" placeholder="NA" class="form-control" onkeydown="return jsDecimals(event);"
                                                            MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group" style="visibility: hidden">

                                                    <div class="col-sm-2 tMonthly" style="" id="">
                                                        <%--<asp:CheckBox ID="chbMonthly" runat="server" Text="" AutoPostBack="true" OnCheckedChanged="chbMonthly_CheckedChanged" />--%>
                                                    </div>

                                                    <label for="textfield" class="control-label col-sm-1 lAMonthly">
                                                        Monthly Usage is Limited..?</label>

                                                    <div class="col-sm-2 tMonthly1" style="" id="">
                                                        <asp:TextBox ID="txtMonthly" runat="server" placeholder="NA" class="form-control" onkeydown="return jsDecimals(event);"
                                                            MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-sm-12" style="">
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lAAllotment" style="">
                                                        Allotment<span style="color: #ff0000; font-size: 14px; display: inline;">*</span>
                                                    </label>
                                                    <div class="col-sm-2 tAAllotment" style="">

                                                        <asp:DropDownList ID="ddlAllotment" runat="server" class="form-control DropdownCss">
                                                            <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                            <asp:ListItem Text="Auto" Value="Auto"></asp:ListItem>
                                                            <asp:ListItem Text="Manual" Value="Manual"></asp:ListItem>

                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lAPerBasic" style="">
                                                        No.of Leaves Per year<span id="tinValue" runat="server" style="color: Red;">*</span></label>
                                                    <div class="col-sm-2 tAPer" style="" id="">
                                                        <asp:TextBox ID="txtPerOfBasic" runat="server" placeholder="No.of Leaves Per year" class="form-control" onkeydown="return jsDecimals(event);"
                                                            MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-sm-12" style="">
                                            <asp:Panel runat="server" GroupingText="Leave Limitations">

                                                <div class="col-sm-12" style="">
                                                    <div class="form-group">
                                                        <label for="textfield" class="control-label col-sm-1 lblperiod" style="">
                                                            Period
                                                        </label>
                                                        <div class="col-sm-2 tperiod" style="">
                                                            <asp:DropDownList ID="ddlperiod" runat="server" class="form-control DropdownCss">
                                                                <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                <asp:ListItem Text="Monthly" Value="Monthly"></asp:ListItem>
                                                                <asp:ListItem Text="Quarterly" Value="Quarterly"></asp:ListItem>
                                                                <asp:ListItem Text="Halfly" Value="Halfly"></asp:ListItem>
                                                                <asp:ListItem Text="Annually" Value="Annually"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="textfield" class="control-label col-sm-1 lbltimes" style="">
                                                                No.Of Times
                                                            </label>
                                                            <div class="col-sm-2 ttimes" style="" id="">
                                                                <asp:TextBox ID="txtnooftimes" runat="server" placeholder="No.Of Times" class="form-control"
                                                                    MaxLength="50"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="textfield" class="control-label col-sm-1 lbldays" style="">
                                                                No.Of Days
                                                            </label>
                                                            <div class="col-sm-2 tdays" style="" id="">
                                                                <asp:TextBox ID="txtdays" runat="server" placeholder="No.Of Days" class="form-control"
                                                                    MaxLength="50"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </asp:Panel>
                                        </div>
                                        <div class="col-sm-12" style="">
                                            <asp:Panel runat="server" GroupingText="Leave Approving Cycle">
                                                <div class="col-sm-12" style="">
                                                    <div class="form-group">
                                                        <asp:CheckBox ID="chktwostages" runat="server" />
                                                        Multi Level Approval
                                                    </div>
                                                    <div class="form-group">
                                                        <asp:CheckBox ID="chkRecommamd" runat="server" AutoPostBack="true" OnCheckedChanged="chkRecommamd_CheckedChanged" />
                                                        Reporting Authority Of Respective Employee 
                                                    </div>

                                                </div>
                                                <div class="col-sm-12" id="recommadDiv" runat="server" style="">
                                                    <div class="form-group">
                                                        <label for="textfield" class="control-label col-sm-1 lblauthority" style="">
                                                            ReCommendation<span style="color: #ff0000; font-size: 14px; display: inline;">*</span>
                                                        </label>
                                                        <div class="col-sm-2 tauthority" style="">
                                                            <asp:DropDownList ID="ddlauthority1" runat="server" class="form-control DropdownCss" AutoPostBack="true"
                                                                OnSelectedIndexChanged="ddlauthority_SelectedIndexChanged">

                                                                <asp:ListItem Text="Select" Value="Select"></asp:ListItem>

                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-sm-2 tuname" style="" id="">
                                                            <asp:TextBox ID="txtusername" runat="server" placeholder="User Name" class="form-control"
                                                                MaxLength="50" ReadOnly="true"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12" style="">
                                                    <div class="form-group">
                                                        <label for="textfield" class="control-label col-sm-1 lblauthority" style="">
                                                            Authorization<span style="color: #ff0000; font-size: 14px; display: inline;">*</span>
                                                        </label>
                                                        <div class="col-sm-2 tauthority" style="">
                                                            <asp:DropDownList ID="ddlauthority2" runat="server" class="form-control DropdownCss" AutoPostBack="true"
                                                                OnSelectedIndexChanged="ddlauthority2_SelectedIndexChanged">

                                                                <asp:ListItem Text="Select" Value="Select"></asp:ListItem>

                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-sm-2 tuname" style="" id="">
                                                            <asp:TextBox ID="txtusername2" runat="server" placeholder="User Name" class="form-control"
                                                                MaxLength="50" ReadOnly="true"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="col-sm-12" style="">
                                                    <div class="form-group">
                                                        <label for="textfield" class="control-label col-sm-1 lblauthority" style="">
                                                            Approval<span style="color: #ff0000; font-size: 14px; display: inline;">*</span>
                                                        </label>
                                                        <div class="col-sm-2 tauthority" style="">
                                                            <asp:DropDownList ID="ddlauthority3" runat="server" class="form-control DropdownCss"
                                                                OnSelectedIndexChanged="ddlauthority3_SelectedIndexChanged">

                                                                <asp:ListItem Text="Select" Value="Select"></asp:ListItem>

                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-sm-2 tuname" style="" id="">
                                                            <asp:TextBox ID="txtusername3" runat="server" placeholder="User Name" class="form-control"
                                                                MaxLength="50" ReadOnly="true"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>

                                            </asp:Panel>
                                        </div>
                                        <div class="col-sm-5 ACButtons">
                                            <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary" OnClientClick="return ControlValid()" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-primary" OnClick="btnUpdate_Click" />
                                            <asp:Button ID="btnCancel" runat="server" Text="Reset" class="btn" OnClick="btnCancel_Click" />
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>



                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

</asp:Content>
