function loadTemplates(templateName, names) {
    var templates = {}
    var date = new Date();
    var timeStamp = date.getTime();
    $.ajax({
        url: '/kendoTemplates/' + templateName + '.htm?rev=' + timeStamp + "'",
        success: function (data) {
            var loadTemplate = function (index) {
                var name = names[index];
                var startInd = data.indexOf('<' + name + '>');
                var templateNameLen = name.length;
                startInd += templateNameLen + 2;
                var endInd = data.indexOf('</' + name + '>');
                templates[name] = data.substring(startInd, endInd)
                index++;
                if (index < names.length) {
                    loadTemplate(index);
                }
            }
            loadTemplate(0);
        },
        async: false
    });
    return templates;
}