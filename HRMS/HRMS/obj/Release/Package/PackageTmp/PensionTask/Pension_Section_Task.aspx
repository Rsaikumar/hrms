﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="Pension_Section_Task.aspx.cs" Inherits="HRMS.Pension_Section_Task" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" lang="javascript">
        function Search() {
            if (document.getElementById('<%=txtSearch.ClientID%>').value == "") {
            alert("Search Box Can't be Empty ... ");
            document.getElementById('<%=txtSearch.ClientID%>').focus();
            return false;
        }
        return true;
    }

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
 
<div id="main">
    <div class="container-fluid">
        <div class="page-header">
            <div class="pull-left Search_Div" id="btnSearch_Div">
                <div class="Search_Icon">
                    <asp:TextBox ID="txtSearch" runat="server" placeholder="Search For Define Leave Types List.."
                        class="form-control txtSearch"></asp:TextBox>
                </div>
            </div>
            <div class="pull-left">
               <asp:Button ID="btnSearch"   runat="server" class="search_Btn" OnClientClick="return Search()"  />
            </div>
          
            <div class="pull-right">
                <ul class="stats">
                    <li>
                        <asp:Button ID="btnAdd" runat="server" class="add_Btn"  ToolTip="Create pension Details" /></li>
                    <li>
                        <asp:Button ID="btnPdf" runat="server" class="PDF_Btn" ToolTip="Export Pension Details To Pdf" /></li>
                    <li>
                        <asp:Button ID="btnExcel" runat="server" class="Excel_Btn" ToolTip="Export pension form To Excel" /></li>
                </ul>
            </div>
        </div>
        <div class="breadcrumbs">
            <ul>
                <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i>
                </li>
                <li>
                        <a href="#">Pension Section</a><i class="fa fa-angle-right"></i>
                    </li>
                    <li><a href="#">Pension Task</a> <i class="fa fa-angle-right"></i></li>
                 <%--   <li><a href="Define_Leaves.aspx"></a> </li>--%>
                <li>
                <%--<asp:LinkButton Text="Define Leave Types" id="lkbAddHeader" runat="server" OnClick="lkbAddHeader_Click"   ></asp:LinkButton>--%>
                <%--<a href="AddUserRegistration.aspx">User Registration</a>--%> </li>
            </ul>
            <div class="close-bread">
                <a href="#"><i class="fa fa-times"></i></a>
            </div>
        </div>
         <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                    <div class="box-title">
                        <h3>
                            <i class="fa fa-th-list"></i>Pension Section</h3>
                    </div>
                    <div class="box-content">
                        <form action="#" class='form-horizontal form-bordered'>
                        <div class="col-sm-12" style="margin-bottom: 0.3%;">
                            <div class="row">
                                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False">
                                    <Columns>
                                        <asp:TemplateField HeaderText="SL.no"></asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pension ID"></asp:TemplateField>
                                        <asp:TemplateField HeaderText="Full Name"></asp:TemplateField>
                                        <asp:TemplateField HeaderText="Employee ID"></asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date of Retirement"></asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status"></asp:TemplateField>
                                        <asp:TemplateField></asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                            
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
     </ContentTemplate>
            </asp:UpdatePanel>
</asp:Content>
