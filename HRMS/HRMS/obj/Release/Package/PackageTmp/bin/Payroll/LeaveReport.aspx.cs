﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BAL;
using DAL;
using System.Data.Linq.SqlClient;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using System.IO;

namespace HRMS.Reports
{
    public partial class LeaveReport : System.Web.UI.Page
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();
        DefineLeavesBAL objSCM_LeaveBAL = new DefineLeavesBAL();
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();

        SqlConnection objSqlConnection;
        SqlCommand objSqlCommand;
        SqlDataAdapter da;
        protected void Page_Load(object sender, EventArgs e)
        {
            bindgrid();
        }
        private void bindgrid()
        {
            try
            {


                string objConfig = ConfigurationManager.ConnectionStrings["Dbconnection"].ToString();

                objSqlConnection = new SqlConnection(objConfig);
                objSqlCommand = new SqlCommand("select Emp_Code,Emp_Name from HRMS_Leave_Allotment", objSqlConnection);
                objSqlCommand.Connection = objSqlConnection;

                objSqlConnection.Open();
                // objSqlCommand.CommandType = CommandType.StoredProcedure;
                //objSqlCommand.CommandText = "Sp_HRMS_Emppoyee_Leave_Details";

                //objSqlCommand.Parameters.AddWithValue("@Year", ddlYear.SelectedValue);
                //objSqlCommand.Parameters.AddWithValue("@Emp_Code", ddlEmpCode.SelectedValue);
                //objSqlCommand.Parameters.AddWithValue("@Department", ddlDepartment.SelectedValue);
                //objSqlCommand.Parameters.AddWithValue("@Creation_Company", (string)Session["ClientRegistrationNo"]);
                //objSqlCommand.Parameters.AddWithValue("@Parameter", parameter);

                da = new SqlDataAdapter(objSqlCommand);
                DataTable ds = new DataTable();
                da.Fill(ds);
                var distinctrows = ds.AsEnumerable()
                        .Select(s => new
                        {
                            Emp_Code = s.Field<string>("Emp_Code"),
                            Emp_Name = s.Field<string>("Emp_Name"),
                           

                        }).Distinct().ToList();

                grd_LEave_Balance.Visible = true;



                if (ds.Rows.Count > 0)
                {
                    grd_LEave_Balance.DataSource = distinctrows;
                    grd_LEave_Balance.DataBind();
                    ViewState["CurrentTable"] = ds;
                    Session["DS"] = ds;
                    //Iscroll.Attributes.Add("style", "overflow-x:scroll;width:99%;");
                }
                else
                {
                    #region header with no record
                    ds.Rows.Add(ds.NewRow());
                    grd_LEave_Balance.DataSource = ds;
                    grd_LEave_Balance.DataBind();
                    int columncount = grd_LEave_Balance.Rows[0].Cells.Count;
                    grd_LEave_Balance.Rows[0].Cells.Clear();
                    grd_LEave_Balance.Rows[0].Cells.Add(new TableCell());
                    grd_LEave_Balance.Rows[0].Cells[0].ColumnSpan = columncount;
                    grd_LEave_Balance.Rows[0].Cells[0].Text = "No Records Found";
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Session["Error"] = ex.ToString();
                //Server.Transfer("~/Modules/Error_Page.aspx");
            }
            finally
            {

            }
        }
        public void grd2Bind(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowIndex != -1)
                {
                    string EmpNo = grd_LEave_Balance.DataKeys[e.Row.RowIndex].Value.ToString();
                    GridView grdstc = (GridView)e.Row.FindControl("grd2");
                    string objConfig = ConfigurationManager.ConnectionStrings["Dbconnection"].ToString();

                    objSqlConnection = new SqlConnection(objConfig);
                    objSqlCommand = new SqlCommand("select Emp_Code,Emp_Name,Leave_Name,Noof_Leavs from HRMS_Leave_Allotment", objSqlConnection);
                    objSqlCommand.Connection = objSqlConnection;

                    objSqlConnection.Open();
                   // objSqlCommand.CommandType = CommandType.StoredProcedure;
                   // objSqlCommand.CommandText = "Sp_HRMS_Emppoyee_Leave_Details";

                    //objSqlCommand.Parameters.AddWithValue("@Year", ddlYear.SelectedValue);
                    //objSqlCommand.Parameters.AddWithValue("@Emp_Code", EmpNo);
                    //objSqlCommand.Parameters.AddWithValue("@Department", "");
                    //objSqlCommand.Parameters.AddWithValue("@Creation_Company", (string)Session["ClientRegistrationNo"]);
                    //objSqlCommand.Parameters.AddWithValue("@Parameter", 2);

                    da = new SqlDataAdapter(objSqlCommand);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                     var distinctrows = dt.AsEnumerable()
                        .Select(s => new
                        {
                            Leave_Name = s.Field<string>("Leave_Name"),
                            Noof_Leavs = s.Field<decimal>("Noof_Leavs"),

                        }).Distinct().ToList();

                    //DataTable dt = (DataTable)ViewState["CurrentTable"];
                    grdstc.DataSource = dt;
                    grdstc.DataBind();
                    if (dt.Rows.Count > 0)
                    {
                        grdstc.DataSource = distinctrows;
                        grdstc.DataBind();
                        ViewState["CurrentTable"] = dt;
                        Session["DS"] = dt;
                        //Iscroll.Attributes.Add("style", "overflow-x:scroll;width:99%;");
                    }
                    else
                    {
                        #region header with no record
                        dt.Rows.Add(dt.NewRow());
                        grdstc.DataSource = dt;
                        grdstc.DataBind();
                        int columncount = grdstc.Rows[0].Cells.Count;
                        grd_LEave_Balance.Rows[0].Cells.Clear();
                        grd_LEave_Balance.Rows[0].Cells.Add(new TableCell());
                        grd_LEave_Balance.Rows[0].Cells[0].ColumnSpan = columncount;
                        grd_LEave_Balance.Rows[0].Cells[0].Text = "No Records Found";
                        #endregion
                    }



                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
        }
    }
}