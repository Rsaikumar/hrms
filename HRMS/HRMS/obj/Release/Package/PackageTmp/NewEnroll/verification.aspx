﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSapplication.Master" AutoEventWireup="true" CodeBehind="verification.aspx.cs" Inherits="HRMS.NewEnroll.verification" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   

    <section class="container">
        <div class="row">
            <div class="col-md-12" style="float: none; margin: 25px 0px;">

                <div class="panel panel-primary" align="center">
                    <div class="panel-heading" style="color: white">New Registration</div>
                    <div class="panel-body">

                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>FirstName :</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtfname" runat="server" class="form-control"></asp:TextBox>

                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtfname" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field" ValidationGroup="vg"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Middle Name :</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtmname" runat="server" CssClass="form-control"></asp:TextBox>

                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtmname" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field" ValidationGroup="vg"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Last Name :</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtlname" runat="server" CssClass="form-control"></asp:TextBox>

                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtlname" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field" ValidationGroup="vg"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Email Id :</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtemail" runat="server" class="form-control"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtemail" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field" ValidationGroup="vg"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtemail" ErrorMessage="*" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="vg"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>
                                Date Of Birth :</label>
                            <div class="col-sm-7">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtdob" runat="server" class="form-control"></asp:TextBox>
                                    <ajax:CalendarExtender ID="CalendarExtender2" PopupButtonID="imgPopup" runat="server" TargetControlID="txtdob"
                                        Format="MM-dd-yyyy" PopupPosition="BottomLeft">
                                    </ajax:CalendarExtender>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Mobile no. :</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtmno" runat="server" class="form-control"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtmno" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field" ValidationGroup="vg"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div align="center">
                            <asp:Button ID="btnverify" runat="server" Text="verify" OnClick="btnverify_Click" ValidationGroup="vg" class="btn btn-primary" />
                        </div>
                    </div>
                </div>
            </div>
    </section>

</asp:Content>
