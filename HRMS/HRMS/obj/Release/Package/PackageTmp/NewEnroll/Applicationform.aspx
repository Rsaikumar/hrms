﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSapplication.Master" AutoEventWireup="true" CodeBehind="Applicationform.aspx.cs" Inherits="HRMS.NewEnroll.Applicationform" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-10" style="float: none; margin: 80px;">

                    <div class="panel panel-primary" align="center">
                        <div class="panel-heading" style="color: palegreen">Employee Details </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group col-md-6 text-left" runat="server" visible="false">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Employee Code:</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtempcode" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator34" runat="server" ControlToValidate="txtempcode" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>First Name:</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtfname" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtfname" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Middle Name:</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtmname" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator35" runat="server" ControlToValidate="txtmname" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Last Name:</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtlname" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtlname" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>

                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Date Of Birth:</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtdob" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator36" runat="server" ControlToValidate="txtdob" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Age:</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtage" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtage" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Gender:</label>
                            <div class="col-sm-7">
                                <asp:RadioButtonList ID="rbtngen" runat="server" RepeatDirection="Horizontal" CssClass="form-control">
                                    <asp:ListItem>Male</asp:ListItem>
                                    <asp:ListItem>Female</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Marital Status:</label>
                            <div class="col-sm-7">
                                <asp:DropDownList ID="ddlmaritalsts" runat="server" CssClass="form-control">
                                    <asp:ListItem>-select-</asp:ListItem>
                                    <asp:ListItem>Single</asp:ListItem>
                                    <asp:ListItem>Married</asp:ListItem>
                                    <asp:ListItem>Divorced</asp:ListItem>
                                    <asp:ListItem>Separated</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" InitialValue="-select-" runat="server" ControlToValidate="ddlmaritalsts" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Religion:</label>
                            <div class="col-sm-7">
                                <asp:DropDownList ID="ddlreligion" runat="server" CssClass="form-control">
                                    <asp:ListItem>-select-</asp:ListItem>
                                    <asp:ListItem>Hindu</asp:ListItem>
                                    <asp:ListItem>Muslim</asp:ListItem>
                                    <asp:ListItem>Christian</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" InitialValue="-select-" runat="server" ControlToValidate="ddlreligion" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Nationality:</label>
                            <div class="col-sm-7">
                                <asp:DropDownList ID="ddlnat" runat="server" CssClass="form-control">
                                    <asp:ListItem>-select-</asp:ListItem>
                                    <asp:ListItem>Indian</asp:ListItem>
                                    <asp:ListItem>Overseas</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" InitialValue="-select-" ControlToValidate="ddlnat" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        </div>

                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Languages known:</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtklang" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtklang" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Address 1:</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtadd1" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtadd1" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Address 2:</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtadd2" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtadd2" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>City:</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtcity" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtcity" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>State:</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtstate" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator37" runat="server" ControlToValidate="txtstate" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>PinCode:</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtpincode" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtpincode" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Country:</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtcountry" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtcountry" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Permanent Address 1:</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtpadd1" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtpadd1" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Permanent Address :</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtpadd2" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtpadd2" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Permanent City:</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtpcity" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="txtpcity" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Permanent State:</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtpstate" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ControlToValidate="txtpstate" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Permanent PinCode:</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtppincodecode" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ControlToValidate="txtppincodecode" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Permanent Country:</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtpcountry" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ControlToValidate="txtpcountry" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Telephone:</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txttelephone" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ControlToValidate="txttelephone" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Mobile:</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtmobile" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ControlToValidate="txtmobile" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Email:</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtemail" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ControlToValidate="txtemail" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Aadhar Number:</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtaadharcd" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ControlToValidate="txtaadharcd" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>PAN No:</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtpanno" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ControlToValidate="txtpanno" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Name Of The Employer:</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtnoemployer" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" ControlToValidate="txtnoemployer" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Postion Held:</label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtpostionheld" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" ControlToValidate="txtpostionheld" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">Period From</label>
                            <div class="col-sm-7">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                   
                                    <asp:TextBox ID="txtperiodfrom" class="form-control" runat="server" placeholder="yyyy-MM-dd"></asp:TextBox>
                                    <ajax:CalendarExtender ID="CalendarExtender" runat="server" Format="yyyy-MM-dd" PopupPosition="BottomLeft" TargetControlID="txtperiodfrom" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-6 text-left">
                            <label class="col-sm-5 control-label">
                                <label style="color: red">*</label>Period To:</label>
                            <div class="col-sm-7">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtperiodto" runat="server" CssClass="form-control"></asp:TextBox>
                                    <ajax:CalendarExtender ID="CalendarExtender1" PopupButtonID="imgPopup" runat="server" TargetControlID="txtperiodto"
                                        Format="yyyy-MM-dd" PopupPosition="BottomLeft">
                                    </ajax:CalendarExtender>
                                </div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator30" runat="server" ControlToValidate="txtperiodto" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-6 text-left">
                        <label class="col-sm-5 control-label">
                            <label style="color: red">*</label>Total Duration:</label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txttduration" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator31" runat="server" ControlToValidate="txttduration" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                    </div>

                    <%--<td class="auto-style23">Date Of Joining:</td>
            <td class="auto-style29"> <asp:TextBox ID="txtdoj" runat="server"  placeholder="dd-mm-yyyy"  Width="172px" Height="23px"></asp:TextBox>
                 <i>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator34" runat="server" ControlToValidate="txtdoj" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                </i>
            </td>--%>

                    <%--<tr>
            <td class="auto-style22">Designation:</td>
            <td class="auto-style26"> <asp:DropDownList ID="ddldesignation" runat="server"  Width="172px" Height="23px">
                    <asp:ListItem>-select-</asp:ListItem>
                    <asp:ListItem>Manager HR</asp:ListItem>
                    <asp:ListItem>Worker</asp:ListItem>
                    <asp:ListItem>Superwiser</asp:ListItem>
                  <asp:ListItem>Accountant</asp:ListItem>
                  <asp:ListItem>Jr.Accountant</asp:ListItem>
                </asp:DropDownList>
                 <i>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator35" InitialValue="-select-" runat="server" ControlToValidate="ddldesignation" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                </i>
            </td>
            <td class="auto-style22">Employee Grade:</td>
           <td class="auto-style5"> <asp:DropDownList ID="ddlempgrade" runat="server"  Width="172px" Height="23px">
                    <asp:ListItem>-select-</asp:ListItem>
                    <asp:ListItem>DCF</asp:ListItem>
                    <asp:ListItem>CCF</asp:ListItem>
                    <asp:ListItem>APCCF</asp:ListItem>
                  <asp:ListItem>HOFF</asp:ListItem>
                </asp:DropDownList>
                <i>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator36" InitialValue="-select-" runat="server" ControlToValidate="ddlempgrade" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                </i>
           </td>
               <td class="auto-style22">Department:</td>
         <td class="auto-style28"> <asp:DropDownList ID="ddldepartment" runat="server"   Width="172px" Height="23px">
                    <asp:ListItem>-select-</asp:ListItem>
                    <asp:ListItem>HRD</asp:ListItem>
                </asp:DropDownList>
              <i>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator37" runat="server" InitialValue="-select-"  ControlToValidate="ddldepartment" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                </i>
         </td>
        </tr>
        <tr>
            <td class="auto-style22">Location:</td>
            <td class="auto-style26"><asp:TextBox ID="txtlocation" runat="server"  Width="172px" Height="23px"></asp:TextBox>
                 <i>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator38" runat="server" ControlToValidate="txtlocation" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                </i>
            </td>
            <td class="auto-style22">Work Type:</td>
             <td class="auto-style5"> <asp:DropDownList ID="ddlworktype"  runat="server"  Width="172px" Height="23px">
                    <asp:ListItem>-select-</asp:ListItem>
                    <asp:ListItem>Direct</asp:ListItem>
                    <asp:ListItem>In-Direct</asp:ListItem>
                </asp:DropDownList>
                  <i>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator39" InitialValue="-select-" runat="server" ControlToValidate="ddlworktype" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                </i>
             </td>
             <td class="auto-style17">Working in Shifts</td>
            <td class="auto-style16"> <asp:TextBox ID="txtworkshift" runat="server"  Width="172px" Height="23px"></asp:TextBox>
                 <i>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator40" runat="server" ControlToValidate="txtworkshift" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                </i>
            </td>
        </tr>--%>
                    <div class="form-group col-md-6 text-left">
                        <label class="col-sm-5 control-label">
                            <span style="color: red">*</span>Education Qualification:</label>
                        <div class="col-sm-7">
                            <asp:DropDownList ID="ddleduqlf" runat="server" CssClass="form-control">
                                <asp:ListItem>-select-</asp:ListItem>
                                <asp:ListItem>SSC</asp:ListItem>
                                <asp:ListItem>Intermediate</asp:ListItem>
                                <asp:ListItem>Elemantary School</asp:ListItem>
                                <asp:ListItem>MAster Degree</asp:ListItem>
                                <asp:ListItem>Technical School</asp:ListItem>
                                <asp:ListItem>Middle School</asp:ListItem>
                                <asp:ListItem>Bachalor Degree</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator32" runat="server" InitialValue="-select-" ControlToValidate="ddleduqlf" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                    </div>

                    <div class="form-group col-md-6 text-left">
                        <label class="col-sm-5 control-label">
                            <label style="color: red">*</label>Blood Group:</label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtbg" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator27" runat="server" ControlToValidate="txtbg" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group col-md-6 text-left">
                        <label class="col-sm-5 control-label">
                            <label style="color: red">*</label>Password:</label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtpwd" runat="server" placeholder="password" CssClass="form-control"></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator28" runat="server" ControlToValidate="txtpwd" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group col-md-6 text-left">
                        <label class="col-sm-5 control-label">
                            <label style="color: red">*</label>Confirm Password:</label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtcpwd" runat="server" placeholder="password" CssClass="form-control"></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator33" runat="server" ControlToValidate="txtcpwd" ErrorMessage="*" ForeColor="Red" ToolTip="mandatory field"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator2" runat="server" ToolTip="password did't match" ControlToValidate="txtcpwd" ErrorMessage="*" ControlToCompare="txtpwd"></asp:CompareValidator>

                    </div>

                    <div class="clearfix"></div>
                    <div align="center">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-primary" OnClick="btnSubmit_Click1" />
                    </div>

                </div>
            </div>

        </section>

    </div>
</asp:Content>
