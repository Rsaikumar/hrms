﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="Pension_Applications.aspx.cs" Inherits="HRMS.Employee.Pension_Applications" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="main">
         <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx">
                        <%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i></li>
                    <li><a href="">Employee</a><i class="fa fa-angle-right"></i> </li>
                    <%--<li><a href="Leave_Application_List.aspx">Leave Application Details</a> <i class="fa fa-angle-right"></i></li>--%>
                    <li><a href="">Pension</a> </li>
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>
            </div>
          <div class="row">
                <div class="col-sm-12">
                    <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                        <div class="box-title">
                            <h3>
                                <i class="fa fa-th-list"></i>Pension Application</h3>
                        </div>
                        <div class="box-content">
                            <form action="#" method="POST" class='form-horizontal form-bordered'>
                                <div class="col-sm-12" style="">
                                <div class="container" style="margin-top:40px">
            Select Pension Application Type :&nbsp;&nbsp;&nbsp;&nbsp; <asp:DropDownList ID="ddlpensionlist" runat="server">
                <asp:ListItem>--Select--</asp:ListItem>
                <asp:ListItem Value="0">STATE PENSION APPLICATION</asp:ListItem>
                <asp:ListItem Value="1">CENTRAL PENSION APPLICATION</asp:ListItem>
            </asp:DropDownList>
            <br />
            <asp:Button ID="btnsumit" runat="server" Text="Submit" OnClick="btnsumit_Click" />
        </div>                   
                        </div>  
                </div>
            </div>
        </div>
    </div>
       
    </div>
</asp:Content>
