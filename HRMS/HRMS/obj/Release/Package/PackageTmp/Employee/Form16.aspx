﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="Form16.aspx.cs" Inherits="HRMS.Employee.Form16" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            font-size: x-large;
        }
        .auto-style2 {
            font-size: x-large;
            width: 338px;
        }
        .auto-style3 {
            width: 338px;
        }
        .auto-style8 {
            width: 42px;
        }
        .auto-style9 {
            width: 210px;
        }
        .auto-style10 {
            width: 137px;
        }
        .auto-style11 {
            width: 85px;
        }
        .auto-style12 {
            width: 124px;
        }
        .auto-style13 {
            width: 97px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <script type = "text/javascript">
         function PrintPanel() {
             var panel = document.getElementById("<%=pnlContents.ClientID %>");
            var printWindow = window.open('', '', 'height=200,width=2000');
            printWindow.document.write('<html><head><title>DIV Contents</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
            return false;
        }
    </script>
         
    <div id="main">
        <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx">
                        <%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i></li>
                    <li><a href="">Employee</a><i class="fa fa-angle-right"></i> </li>
                    <%--<li><a href="Leave_Application_List.aspx">Leave Application Details</a> <i class="fa fa-angle-right"></i></li>--%>
                    <li><a href="#">Form 16</a> </li>
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>
            </div>
         </div>
    <div>
            <div class="row">
    <div class="col-sm-12">
                    <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                        <div class="box-title">
                            <h3>
                                <i class="fa fa-th-list"></i>Form 16</h3>
                        </div>
                        <div class="box-content">
    <div > 
      <asp:Panel id="pnlContents" runat = "server"> 
            <div>
                <div>
                <table border="1">
                    <tr>
                        <td colspan="5" align="center" class="auto-style1">
                            FORM 16
                        </td>
                    </tr>
                    <tr >
                       <td colspan="5" align="center" class="auto-style1">
                           HRMS Income Tax Department
                       </td>
                    </tr>
                    <tr>
                        <td class="auto-style2">
                            Certificate No. :</td>
                        <td colspan="2">
                            <asp:Label ID="lblcertificateno" runat="server" Text="" style="font-size: x-large"></asp:Label>
                        </td>
                        
                        <td class="auto-style1">
                            Last Update On:
                        </td>
                        <td >
                             <asp:Label ID="lbllastupdate" runat="server" Text="" style="font-size: x-large"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style1" colspan="3">
                           Name and Address Of the Employer
                        </td>
                        
                        <td class="auto-style1" colspan="2">
                           Name and Address Of the Employee
                        </td>
                        
                    </tr>
                    <tr>
                          <td class="auto-style2">
                          Pan Of the Deductor
                        </td>
                        <td class="auto-style1" colspan="2">
                            TAN of The Deductor
                        </td>
                       
                        <td>
                            <span class="auto-style1">PAN of The Employee </span>
                        </td>
                        <td class="auto-style1">
                             Employee Ref No. Provider By the Employer(If available)
                        </span>
                        </td>
                    </tr>
                    <tr class="auto-style1">
                          <td class="auto-style3">
                              <asp:Label ID="lblpanofdeductor" runat="server" Text=""></asp:Label>
                        </td>
                        <td colspan="2" align="center">
                            <asp:Label ID="lbltandeductor" runat="server" Text=""></asp:Label>
                        </td>
                       
                        <td>
                           <asp:Label ID="lblpanofemployee" runat="server" Text=""></asp:Label>
                        </td>
                        <td>
                              <asp:Label ID="lblemployerefno" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr class="auto-style1">
                        <td colspan="2">
                            TDS(CIT)
                        </td>
                        
                        <td>
                            Assesment Year
                        </td>
                        <td colspan="2" align="center">
                            Period With The Employer
                        </td>
                    </tr>
                       <tr class="auto-style1">
                        <td colspan="2">
                            <asp:Label ID="lbltdsaddress" runat="server" Text=""></asp:Label>
                        </td>
                        <td>
                              <asp:Label ID="lblassesmentyear" runat="server" Text=""></asp:Label>
                        </td>
                        <td>
                               <asp:Label ID="lblfromdate" runat="server" Text=""></asp:Label>
                        </td>
                           <td>
                                  <asp:Label ID="lbltodate" runat="server" Text=""></asp:Label>
                           </td>
                    </tr>
                    <tr>
                        <td colspan="5" class="auto-style1">
                            Summery of amount Paid/Credited and tax Deductedat source There On in Respect Of the Employee
                        </td>
                    </tr>
                    <tr class="auto-style1">
                        <td class="auto-style3">
                            Quarter(s)
                        </td>
                        <td>
                            Receipt Numbers of Original quaterly statements of TDS under Sub-Section(3) of section 200
                        </td>
                        <td>
                            Amount Paid/Credited
                        </td>
                        <td>
                            Amount Of Tax Deducted(Rs.)
                        </td>
                        <td>
                            Amount Of Tax deposited/remitted(Rs.)
                        </td>
                    </tr>
                    <tr class="auto-style1">
                        <td class="auto-style3">
                             <asp:Label ID="Label19" runat="server" Text="Q1"></asp:Label>
                        </td>
                        <td>
                             <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                        </td>
                        <td>
                             <asp:Label ID="Label18" runat="server" Text=""></asp:Label>
                        </td>
                        <td>
                             <asp:Label ID="Label17" runat="server" Text=""></asp:Label>
                        </td>
                        <td>
                             <asp:Label ID="Label16" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                      <tr class="auto-style1">
                        <td class="auto-style3">
                             <asp:Label ID="Label15" runat="server" Text="Q2"></asp:Label>
                        </td>
                        <td>
                             <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                        </td>
                        <td>
                             <asp:Label ID="Label14" runat="server" Text=""></asp:Label>
                        </td>
                        <td>
                             <asp:Label ID="Label13" runat="server" Text=""></asp:Label>
                        </td>
                        <td>
                             <asp:Label ID="Label12" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                      <tr class="auto-style1">
                        <td class="auto-style3">
                             <asp:Label ID="Label11" runat="server" Text="Q3"></asp:Label>
                        </td>
                        <td>
                             <asp:Label ID="Label4" runat="server" Text=""></asp:Label>
                        </td>
                        <td>
                             <asp:Label ID="Label10" runat="server" Text=""></asp:Label>
                        </td>
                        <td>
                             <asp:Label ID="Label9" runat="server" Text=""></asp:Label>
                        </td>
                        <td>
                             <asp:Label ID="Label8" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                      <tr class="auto-style1">
                        <td class="auto-style3">
                            Total (Rs.)
                        </td>
                        <td>

                        </td>
                        <td>
                             <asp:Label ID="Label5" runat="server" Text=""></asp:Label>
                        </td>
                        <td>
                             <asp:Label ID="Label6" runat="server" Text=""></asp:Label>
                        </td>
                        <td>
                             <asp:Label ID="Label7" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
              </table>
                    <table>
                    <tr>
                  
                  <td colspan="6">
                       DETAILS OF TAX DEDUCTED AND DEPOSITED IN THE CENTRAL GOVERNMENTACCOUNT THROUGHCHALLAN(The Deductor to provide payment wise details of tax deducted and deposited withrespect to the deducte)
                  </td>
                 
              </tr>
                    <tr  >
                        <td rowspan="2" >
                            Sl.No
                        </td>
                        <td rowspan="2">
                            Tax Deposited In Respect Of The Deductee(Rs.)
                        </td>
                        <td colspan="4">
                            challan Identification Number(CIN)
                        </td>
                    </tr>
                    <tr>
                        
                        <td>
                             BSR Code OF the Bank Branch
                        </td>
                        <td>
                            Date of Which Tax Deposited(dd/mm/yy)
                        </td>
                        <td>
                            Challan Serial Number
                        </td>
                        <td>
                            Status Of Matching with OLTAS
                        </td>
                    </tr>
                    <tr>
                        <td>
                            1.
                        </td>
                        <td></td>
                         <td></td>
                          <td></td>
                          <td></td> 
                         <td></td>
                    </tr>
                    <tr>
                        <td>
                            2.
                        </td>
                        <td></td>
                         <td></td>
                          <td></td>
                          <td></td> 
                         <td></td>
                    </tr>
                    <tr>
                       <td>
                           3.
                       </td>
                        <td></td>
                         <td></td>
                          <td></td>
                          <td></td> 
                         <td></td>
                    </tr>
                   <tr>
                        <td>
                            4.
                        </td>
                       <td></td>
                         <td></td>
                          <td></td>
                          <td></td> 
                         <td></td>
                    </tr>
                    <tr>
                        <td>
                            5.
                        </td>
                        <td></td>
                         <td></td>
                          <td></td>
                          <td></td> 
                         <td></td>
                    </tr>
                    <tr>
                        <td >
                            6.
                        </td>
                        <td></td>
                         <td></td>
                          <td></td>
                          <td></td> 
                         <td></td>
                    </tr>
                    <tr>
                        <td>
                            7.
                        </td>
                        <td></td>
                         <td></td>
                          <td></td>
                          <td></td> 
                         <td></td>
                    </tr>
                    <tr>
                        <td>
                            8.
                        </td>
                        <td></td>
                         <td></td>
                          <td></td>
                          <td></td> 
                         <td></td>
                    </tr>
                    <tr>
                        <td >
                            9.
                        </td>
                        <td></td>
                         <td></td>
                          <td></td>
                          <td></td> 
                         <td></td>
                    </tr>
                    <tr>
                        <td>
                            10.
                        </td>
                        <td></td>
                         <td></td>
                          <td></td>
                          <td></td> 
                         <td></td>
                    </tr>
                    <tr>
                        <td >
                            11.
                        </td>
                        <td></td>
                         <td></td>
                          <td></td>
                          <td></td> 
                         <td></td>
                    </tr>
                    <tr>
                        <td>
                            12.
                        </td>
                        <td></td>
                         <td></td>
                          <td></td>
                          <td></td> 
                         <td></td>
                    </tr>
                    <tr>
                        <td >
                            Total(Rs.)
                        </td>

                        <td>

                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                    </tr>
                        </table>
                    <table>
                    <tr>
                        <td colspan="5">
                            Verification
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            I,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ,son/daughter of&nbsp;
                            <br />
                            working in the capacity of&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (designation)do here by certify that a sum of Rs.<br /> &nbsp;#NAME?&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; has been deducted and deposited to the credit of the Central Govenment.I further certify that the information given above is true,complete and corret and is bsed on the books of Account,Documents,TDS Statements and TDS deposited and other Availbale records.
                        </td>
                    </tr>
                   
                    <tr>
                        <td>
                            Place:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Date:
                        </td>
                        <td>
                            (signature of person Responsible for tax)
                        </td>
                    </tr>
                     <tr>
                         <td class="auto-style8">
                             Designation:
                         </td>
                         <td>
                             Full Name:
                         </td>
                    </tr>
                        </table>
                    <table>
                    <tr>
                        <td colspan="5">
                            Note:
                       <br /><br />
                            1.Governament deductors to fill information in item  1.if tax is paid without production of an income-tax challan and in item 2.if tax is 
                            paid accompanied by an income-tax challan.
                              <br /> <br />                  
                            2.Non-Government Deductors to fill information in item 2.
                       <br /><br />
                          3.The deductor shall furnish the address of the Commissioner of Income-tax(TDS)having jurisdicton as regards TDS statements of the assessee
                       <br /><br />

                            4.If an assessee is employed under one employer only during the year,certificate in Form No.16 issued for the quarter ending on 31st March of the financial year shall contain the details of tax deducted and deposited for all the quarters of the financial year.
                        <br /><br />
                            5.If an assessee is employed under more than one employer during the year,each of the employers shall issue Part A of the certificate in Form No.16 pertaining to the period for which such assessee was employed with each of the employers.PartB(Annexure) of the certificate in Form No.16 may be issued by each of the employers or the last employer at the option of the assessee.
                        <br /><br />
                            6.In items I and II,in column for tax deposited in respect of deductee,furnish total amount of TDS and education cess. 
                        </td>
                    </tr>
                        </table>
                    <table>

                    <tr>
                        <td colspan="4" align="center">
                            PART B( Refer Note 1)
                        </td>
                       
                    </tr>
                    <tr>
                        <td>
                            Details of salery paid and any other income and tax deducted
                        </td>
                         <td>
                             Colom1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;            
                        </td>
                        <td> colom 2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                        <td> colom 3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                    </tr>
                    <tr>
                        <td >
                            1.Gross Salary <br />
                            </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        </tr>
                        <tr>
                        <td >
                            a).salary as per previous Conatained in sec 17(1)<br />
                            </td>
                        <td> Rs.<asp:TextBox ID="txtgsalery" runat="server" AutoPostBack="True" OnTextChanged="txtgsalery_TextChanged1"></asp:TextBox>
                            </td>
                        <td>
                            <table>
                                <tr>
                                    <td class="auto-style13">Basic Amount</td><td>
                                        Rs.<asp:Label ID="lblgbasicamount" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td class="auto-style13">HRA</td><td>
                                        Rs.<asp:Label ID="lblghra" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td class="auto-style13">CA</td><td>
                                       Rs. <asp:Label ID="lblgca" runat="server" Text=""></asp:Label></td>
                                </tr>
                               
                            </table>
                        </td>
                        <td></td>
                        </tr>
                        <tr>
                        <td >
                             b).Value of perquisities u/s 17(2)(as per form no 12BA,as per aplicable)<br />
                            </td>
                        <td>Rs.0 </td>
                        <td></td>
                        <td></td>
                        </tr>
                        <tr>
                        <td >
                           c).profits in lieu if salary under section 17(3)<br />
                                (as per frorm 12BB,wherever aplicable)<br />
                            </td>
                        <td>Rs.0</td>
                        <td></td>
                        <td></td>
                        </tr>
                        <tr>
                        <td >
                             d)Total<br /><br />
                            </td>
                        <td></td>
                        <td> Rs. <asp:Label ID="lblgsaltot" runat="server" Text=""></asp:Label></td>
                        <td></td>
                        </tr>
                            
                           
                            <tr>
                        <td >
                               2. Less:Allowance to the extent exempt u/s 10<br /><br />
                            <table>
                                <tr>
                                    <td class="auto-style9">
                                        Allowance
                                    </td>
                                    <td class="auto-style10">
                                        Rupes
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style9">
                                        hra&nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td class="auto-style10">

                                        <asp:Label ID="lblahra" runat="server" Text=""></asp:Label>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style9">
                                        cva
                                    </td>
                                    <td class="auto-style10">

                                       
                                        <asp:Label ID="lblacva" runat="server" Text="19200.00"></asp:Label>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style9">
                                        Medical
                                    </td>
                                    <td class="auto-style10">

                                        <asp:Label ID="lblamedical" runat="server" Text=""></asp:Label>

                                    </td>
                                </tr>
                            </table>
                            </td>
                        <td></td>
                        <td> Rs.<asp:Label ID="lbltotalalowancce" runat="server" Text=""></asp:Label></td>
                        <td></td>
                        </tr>
                        <tr>
                        <td >
                             3. Balance (1-2)<br /><br />
                            </td>
                        <td></td>
                        <td>RS.<asp:Label ID="lblbalanceg" runat="server" Text=""></asp:Label></td>
                        <td></td>
                        </tr>
                        <tr>
                        <td >
                              4. Deduction :<br />
                              (a) Entertainment allowance(pf)<br />
                              
                            </td>
                        <td>Rs.<asp:Label ID="lblealownace" runat="server" Text=""></asp:Label></td>
                        <td></td>
                        <td></td>
                        </tr>
                        <tr>
                        <td >
                             (b) Tax on employment(Esi)<br /><br />
                            </td>
                        <td>Rs.<asp:Label ID="lbltoemploment" runat="server" Text=""></asp:Label></td>
                        <td></td>
                        <td></td>
                        </tr>
                        <tr>
                        <td >
                             5. Aggregate of 4(a) and (b)<br /><br />
                            </td>
                        <td></td>
                        <td> Rs.<asp:Label ID="lblagragate" runat="server" Text=""></asp:Label></td>
                        <td></td>
                        </tr>
                        <tr>
                        <td >
                             6. Income chaneable under the head 'salaries'(3-5)<br /><br />
                            </td>
                        <td></td>
                        <td></td>
                        <td> Rs.<asp:Label ID="lblheadsaleries" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                        <td >
                             7.Add:Any other income report by the employee<br /><br />
                             <table>
                                <tr>
                                    <td class="auto-style12">
                                        Income&nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td class="auto-style11">
                                        Rupes.
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style12">

                                        &nbsp;&nbsp;</td>
                                    <td class="auto-style11">&nbsp;&nbsp;</td>
                                </tr>
                                 <tr>
                                    <td class="auto-style12">

                                    </td>
                                    <td class="auto-style11"></td>
                                </tr>
                            </table>
                            </td>
                        <td></td>
                        <td>Rs.<asp:Label ID="lbltotalincome" runat="server" Text=""></asp:Label></td>
                        <td></td>
                        </tr>
                        <tr>
                        <td >
                             8. Gross total income(6+7) <br /><br />
                            </td>
                        <td></td>
                        <td></td>
                        <td>Rs.<asp:Label ID="lblgrosstotalincome" runat="server" Text=""></asp:Label></td>
                        </tr>
                       
                       
                  

                    <tr>
                        <td >
                            9. Deductions under Chapter VIA<br />
                            (A) Section 80C,80CCC and 80CCD<br />
                            (a) Section 80C<br />
                            </td>
                        <td></td>
                        <td> Gross Amount</td>
                        <td>Deductible Amount</td>
                        </tr>
                        <tr>
                            <td>
                                 (i)<asp:TextBox ID="txtd1" runat="server"></asp:TextBox> <br />
                            </td>
                            <td>Rs.<asp:TextBox ID="txt1" runat="server" AutoPostBack="True" OnTextChanged="txt1_TextChanged">0</asp:TextBox></td>
                        <td></td>
                        <td></td>
                            </tr>
                         <tr>
                            <td>
                                 (ii)<asp:TextBox ID="txtd2" runat="server"></asp:TextBox><br />
                            </td>
                            <td>Rs.<asp:TextBox ID="txt2" runat="server" AutoPostBack="True" OnTextChanged="txt2_TextChanged">0</asp:TextBox></td>
                        <td></td>
                        <td></td>
                            </tr>
                           <tr>
                            <td>
                                 (iii)<asp:TextBox ID="txtd3" runat="server"></asp:TextBox><br />
                           
                            </td>
                            <td>Rs.<asp:TextBox ID="txt3" runat="server" AutoPostBack="True" OnTextChanged="txt3_TextChanged">0</asp:TextBox></td>
                        <td></td>
                        <td></td>
                            </tr>
                        <tr>
                            <td>
                                  (iv)<asp:TextBox ID="txtd4" runat="server"></asp:TextBox><br />
                           
                            </td>
                            <td>Rs.<asp:TextBox ID="txt4" runat="server" AutoPostBack="True" OnTextChanged="txt4_TextChanged">0</asp:TextBox></td>
                        <td></td>
                        <td></td>
                            </tr>
                        <tr>
                            <td>
                                 (v)<asp:TextBox ID="txtd5" runat="server"></asp:TextBox><br />
                           
                            </td>
                            <td>Rs.<asp:TextBox ID="txt5" runat="server" AutoPostBack="True" OnTextChanged="txt5_TextChanged">0</asp:TextBox></td>
                        <td></td>
                        <td></td>
                            </tr>
                        <tr>
                            <td>
                                 (vi)<asp:TextBox ID="txtd6" runat="server"></asp:TextBox><br />
                           
                            </td>
                            <td>Rs.<asp:TextBox ID="txt6" runat="server" AutoPostBack="True" OnTextChanged="txt6_TextChanged">0</asp:TextBox></td>
                        <td></td>
                        <td></td>
                            </tr>
                        <tr>
                            <td>
                                 (vii)<asp:TextBox ID="txtd7" runat="server"></asp:TextBox><br />
                            </td>
                            <td>Rs.<asp:TextBox ID="txt7" runat="server" AutoPostBack="True" OnTextChanged="txt7_TextChanged">0</asp:TextBox></td>
                        <td> Rs.<asp:Label ID="lbltotal1" runat="server" Text=""></asp:Label></td>
                        <td>Rs.<asp:Label ID="lbltot2" runat="server" Text=""></asp:Label></td>
                            </tr>
                        <tr>
                            <td>
                                (b) section 80CCC<br />
                            </td>
                            <td></td>
                        <td>Rs.<asp:TextBox ID="txt80cc" runat="server" AutoPostBack="True" OnTextChanged="txt80cc_TextChanged">0</asp:TextBox></td>
                        <td>Rs.<asp:Label ID="lbl80cc" runat="server" Text=""></asp:Label></td>
                            </tr>
                        <tr>
                            <td>
                                (c) Section 80CCD<br />
                            </td>
                            <td></td>
                        <td>Rs.<asp:TextBox ID="txt80ccd" runat="server" AutoPostBack="True" OnTextChanged="txt80ccd_TextChanged1">0</asp:TextBox></td>
                        <td>
                            Rs.<asp:Label ID="lbl80ccd" runat="server" Text=""></asp:Label></td>
                            </tr>
                        <tr>
                            <td>
                                 Aggregate amount deductible under the three Section, i.e.., 80C,80CCC and 80CCD<br /><br />
                            </td>
                            <td></td>
                        <td>
                           Rs. <asp:Label ID="lblagregateaount1" runat="server" Text=""></asp:Label></td>
                        <td>
                           Rs. <asp:Label ID="lblagregateamount2" runat="server" Text=""></asp:Label></td>
                            </tr>
                        <%--<tr>
                            <td>
                                    note:<br />1 Aggregate amount deductible under section 80C shall not exceed 1.5 lakh rupees.<br /><br />
                                 2 Aggregate amount deductible under the three sections, i.e., 80C,80CCC and 80CCD shall not exceed 1.5 lakh rupees<br />
                            </td>
                            <td></td>
                        <td></td>
                        <td></td>
                            </tr>
                        <tr>
                            <td>
                                  (B) Other sections (e.g. 80E,80G,80TTA,etc.)under Chapter VI-A<br />
                            </td>
                            <td>Gross Amount</td>
                        <td> Qulifying Amount</td>
                        <td> Deductible Amount</td>
                            </tr>
                        <tr>
                            <td>
                                 (i)<asp:TextBox ID="txttype1" runat="server"></asp:TextBox><br />
                            
                            </td>
                            <td>
                               Rs. <asp:TextBox ID="txtg1" runat="server"></asp:TextBox></td>
                        <td>
                           Rs. <asp:TextBox ID="txtq1" runat="server"></asp:TextBox></td>
                        <td>
                           Rs. <asp:TextBox ID="txtda1" runat="server"></asp:TextBox></td>
                            </tr>
                           
                            <tr>
                            <td>
                                 (ii)<asp:TextBox ID="txttype2" runat="server"></asp:TextBox><br />
                            </td>
                            <td>
                               Rs. <asp:TextBox ID="txtg2" runat="server"></asp:TextBox></td>
                        <td>
                           Rs. <asp:TextBox ID="txtq2" runat="server"></asp:TextBox></td>
                        <td>
                            Rs.<asp:TextBox ID="txtda2" runat="server"></asp:TextBox></td>
                            </tr>
                         <tr>
                            <td>
                               
                            (iii)<asp:TextBox ID="txttype3" runat="server"></asp:TextBox><br />
                           
                            </td>
                            <td>
                               Rs. <asp:TextBox ID="txtg3" runat="server"></asp:TextBox></td>
                        <td>
                           Rs. <asp:TextBox ID="txtq3" runat="server"></asp:TextBox></td>
                        <td>
                           Rs. <asp:TextBox ID="txtda3" runat="server"></asp:TextBox></td>
                            </tr>
                         <tr>
                            <td>
                                  (iv)<asp:TextBox ID="txttype4" runat="server"></asp:TextBox><br />
                           
                            </td>
                            <td>
                               Rs. <asp:TextBox ID="txtg4" runat="server"></asp:TextBox></td>
                        <td>
                            Rs.<asp:TextBox ID="txtq4" runat="server"></asp:TextBox></td>
                        <td>
                            Rs.<asp:TextBox ID="txtda4" runat="server"></asp:TextBox></td>
                            </tr>
                         <tr>
                            <td>
                                 (v)<asp:TextBox ID="txttype5" runat="server"></asp:TextBox><br /><br />
                            </td>
                            <td>
                                Rs.<asp:TextBox ID="txtg5" runat="server"></asp:TextBox></td>
                        <td>
                           Rs. <asp:TextBox ID="txtq5" runat="server"></asp:TextBox></td>
                        <td>
                           Rs. <asp:TextBox ID="txtda5" runat="server"></asp:TextBox></td>
                            </tr>--%>
                         <tr>
                            <td>
                                  10. Aggregate of deductible amount under Chapter VIA<br /><br />
                            </td>
                            <td></td>
                        <td></td>
                        <td>Rs.<asp:Label ID="lbl10" runat="server" Text=""></asp:Label></td>
                            </tr>
                         <tr>
                            <td>
                                 11. Total Income (8-10)<br /><br />
                           
                            </td>
                            <td></td>
                        <td></td>
                        <td>Rs.<asp:Label ID="lbl11" runat="server" Text=""></asp:Label></td>
                            </tr>

                             <tr>
                            <td>
                                 12. Tax on total income<br /><br />
                           
                            </td>
                            <td></td>
                        <td></td>
                        <td>Rs.<asp:Label ID="lbl12" runat="server" Text=""></asp:Label></td>
                            </tr>
                         <tr>
                            <td>
                                 13. Education cess @ 3% (on tax computed at S.no.12)<br /><br />
                          
                            </td>
                            <td></td>
                        <td></td>
                        <td>Rs.<asp:Label ID="lbl13" runat="server" Text=""></asp:Label></td>
                            </tr>
                         <tr>
                            <td>
                                  14. Tax payable (12+13)<br /><br />
                           
                            </td>
                            <td></td>
                        <td></td>
                        <td>Rs.<asp:Label ID="lbl14" runat="server" Text=""></asp:Label></td>
                            </tr>
                        
                         <tr>
                            <td>
                                 
                                15. Balance<br />
                            </td>
                            <td></td>
                        <td></td>
                        <td>Rs.<asp:Label ID="lbl18" runat="server" Text=""></asp:Label></td>
                            </tr>

                        </table>
                    <table>
                    <tr>
                        <td align="center" colspan="2">Verification</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            I,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ,son/daughter of
                            <br />
                            working in the capacity of&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (designation)do here by certify that the information given above is true,complete and correct and is based on the books of Account,Documents,TDS Statements and other Availbale records.
                        </td>
                    </tr>
                   
                    <tr>
                        <td>
                            Place:
                        </td>
                    </tr>
                    <tr>
                        <td >
                            Date:
                        </td>
                        <td>
                            (signature of person Responsible for tax)
                        </td>
                    </tr>
                     <tr>
                         <td>
                             Designation:
                         </td>
                         <td>
                             Full Name:
                         </td>
                    </tr>
                          </table>
                    <hr />
                </div>
            </div>
          
          </asp:Panel>
        </div>
                            </div>
            </div>            </div>
                    </div>
         <asp:Button align="center" ID="btnPrint" runat="server" Text="Print" OnClientClick = "return PrintPanel();" />
</asp:Content>
