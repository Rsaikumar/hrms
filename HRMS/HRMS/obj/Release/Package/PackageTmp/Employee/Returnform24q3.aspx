﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="Returnform24q3.aspx.cs" Inherits="HRMS.Employee.Returnform24q3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
    <div >
        <h4>
            Quarterly Reports
        </h4>
        <table>
            <tr>
                <td class="auto-style1">
                    Choose Report:
                </td>
                <td class="auto-style2">
                    <asp:DropDownList ID="DropDownList1" runat="server" Height="16px" Width="281px"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">Choose Quarter:</td>
                <td class="auto-style2">
                    <asp:DropDownList ID="DropDownList2" runat="server" Height="16px" Width="281px"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    Return type:
                </td>
                <td class="auto-style2">
                    <asp:DropDownList ID="DropDownList3" runat="server" Height="16px" Width="281px"></asp:DropDownList></td>
            </tr>
        </table>
        <table>
            <tr>
                <td>
                    <asp:Button ID="Button1" runat="server" Text="Electronic Return" Height="50px" /></td>
                <td>
                    <asp:Button ID="Button2" runat="server" Text="Paper Return" Height="50px"/></td>
                <td>
                    <asp:Button ID="Button3" runat="server" Text="Validation Utility-for manual Validation" Height="50px" Width="230px" OnClick="Button3_Click" />
                    </td>
                <td>
                    <asp:Button ID="Button4" runat="server" Text="Close" Height="50px" Width="69px"/></td>
            </tr>
        </table>
        <asp:Panel ID="Panel1" runat="server">
        <div>
            <h4>Validation Utility-for manual Validation</h4>
            <table>
                <tr>
                    <td>
                        TDS/TCS Input file name with path
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="Button5" runat="server" Text="Browse" />
                    </td>
                </tr>
                 <tr>
                    <td>
                        Challan Input file name with path
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="Button6" runat="server" Text="Browse" />
                    </td>
                </tr>
                 <tr>
                    <td>
                        Error/Upload & Statistics Report file path
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="Button7" runat="server" Text="Browse" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="Button8" runat="server" Text="Validate" />
                    </td>
                    <td>
                        <asp:Button ID="Button9" runat="server" Text="Exit" />
                    </td>
                    <td>
                        <asp:Button ID="Button10" runat="server" Text="Read me" /></td>
                </tr>
            </table>
        </div>
            </asp:Panel>
    </div>
    </div>
</asp:Content>
