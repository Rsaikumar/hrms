﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="SubRoleMaster.aspx.cs" Inherits="HRMS.Employee.SubRoleMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i>
                    </li>
                    <li><a href="SubRoleMaster.aspx">Sub-Role Master</a><i class="fa fa-angle-right"></i> </li>
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                        <div class="box-title">
                            <h3>
                                <i class="fa fa-user"></i>Sub-Role Master</h3>
                        </div>
                        <div class="box-content">
                            <div class="form-group col-md-6">
                                <label for="textfield" class="control-label col-sm-3" style="">
                                    Sub-Role Id<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                <div class="col-sm-9" style="" id="Div2">
                                    <asp:TextBox ID="txtsubroleid" runat="server" ReadOnly="true" placeholder="Role Id" class="form-control"
                                        MaxLength="50"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="textfield" class="control-label col-sm-3" style="">
                                    Sub-Role Short Name<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                <div class="col-sm-9" style="" id="Div1">
                                    <asp:TextBox ID="txtsubroleshortname" runat="server" placeholder="Short Name" class="form-control"
                                        MaxLength="30"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <label for="textfield" class="control-label col-sm-3" style="">
                                    Sub-Role Description</label>
                                <div class="col-sm-9" style="" id="Div3">
                                    <asp:TextBox ID="txtsubroledescription" runat="server" TextMode="MultiLine" placeholder="Description" class="form-control"
                                        MaxLength="100"></asp:TextBox>
                                </div>
                            </div>

                               <div class="col-md-12 text-center">
                                <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary" OnClick="btnSave_Click" /><%--OnClientClick="return fileUploadValidation()"--%>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</asp:Content>
