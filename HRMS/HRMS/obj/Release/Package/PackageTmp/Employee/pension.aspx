﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="pension.aspx.cs" Inherits="HRMS.Employee.pention" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #ContentPlaceHolder1_actdate_container, #ContentPlaceHolder1_CalendarExtender2_popupDiv, #ContentPlaceHolder1_CalendarExtender3_popupDiv, #ContentPlaceHolder1_CalendarExtender1_container, #ContentPlaceHolder1_CalendarExtender2_container, #ContentPlaceHolder1_actdate_popupDiv, #ContentPlaceHolder1_CalendarExtender1_popupDiv
        {
            z-index: 10000;
        }

        .DateHide
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="#">Employee</a><i class="fa fa-angle-right"></i> </li>
                        <li><a href="pension.aspx" class="current">Pension</a> </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <i class="fa fa-th-list"> Pension</i>
                        </header>
                        <div class="panel-body">
                            <div class="col-md-12" style="">
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-4" style="">
                                        Employee Name</label>
                                    <div class="col-sm-8" style="" id="">
                                        <asp:TextBox ID="txtempname" runat="server" MaxLength="50" ReadOnly="true"
                                            class="form-control" AutoPostBack="true"></asp:TextBox>
                                    </div>
                                </div>
                                   <div class="form-group col-md-6 text-left">
                                    <label for="textfield" id="lblempcode" runat="server" class="control-label col-sm-5 text-left  ">
                                        Emp Code
                                    </label>
                                    <div class="col-sm-7 " style="">
                                        <asp:TextBox ID="txtEmpCode" runat="server" placeholder="Emp Code" MaxLength="50" ReadOnly="true"
                                            class="form-control" AutoPostBack="true"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <i class="fa fa-th"> Pension Details</i>
                        </header>
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="col-sm-6 form-group">
                                    <label for="textfield" class="control-label col-sm-4 text-left" style="">
                                        Date Of Birth
                                    </label>
                                    <div class="col-sm-8" style="" id="Div1">
                                        <asp:TextBox ID="txtdateofbirth" runat="server" placeholder="Date Of Birth" class="form-control"
                                            onkeydown="return jsDecimals(event);" MaxLength="20" AutoPostBack="True"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="textfield" class="control-label col-sm-4" style="">
                                        Date Of Joining</label>
                                    <div class="col-sm-8 " style="" id="Div2">
                                        <asp:TextBox ID="txtdateofjoining" runat="server" placeholder="Date of Joining" class="form-control"
                                            onkeydown="return jsDecimals(event);" MaxLength="20" AutoPostBack="True"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-4 " style="">
                                        Date of Retirement</label>
                                    <div class="col-sm-8" style="" id="Div3">
                                        <asp:TextBox ID="txtretirement" runat="server" placeholder="Date of Retirement" class="form-control"
                                            onkeydown="return jsDecimals(event);" MaxLength="20" AutoPostBack="True"></asp:TextBox>
                                        <%--                                                       <cc1:calendarextender ID="retirementdate" PopupButtonID="imgPopup" runat="server" TargetControlID="txtretirementdate"
                                                        Format="dd-MM-yyyy" PopupPosition="BottomLeft" SelectedDate="<%# DateTime.Now %>">
                                                    </cc1:calendarextender>--%>
                                    </div>
                                </div>



                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-4" style="">
                                        Year</label>
                                    <div class="col-sm-8 " style="" id="Div5">
                                        <asp:TextBox ID="txtnoofyears" runat="server" placeholder="Year" class="form-control"
                                            onkeydown="return jsDecimals(event);" MaxLength="20" AutoPostBack="True"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-4" style="">
                                        Retirement Rule</label>
                                    <div class="col-sm-8" style="" id="Div7">
                                        <asp:TextBox ID="txtrule" runat="server" placeholder="Retirement rule" class="form-control"
                                            onkeydown="return jsDecimals(event);" MaxLength="20" AutoPostBack="True"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
