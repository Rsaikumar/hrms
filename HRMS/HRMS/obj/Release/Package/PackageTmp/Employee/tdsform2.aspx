﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="tdsform2.aspx.cs" Inherits="HRMS.Employee.tdsform2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx">
                        <%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i></li>
                    <li><a href="">Employee</a><i class="fa fa-angle-right"></i> </li>
                    <%--<li><a href="Leave_Application_List.aspx">Leave Application Details</a> <i class="fa fa-angle-right"></i></li>--%>
                    <li><a href="">TDS</a> </li>
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>
            </div>
         </div>
     <div class="row">
                <div class="col-sm-12">
                    <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                        <div class="box-title">
                            <h3>
                                <i class="fa fa-th-list"></i>TDS</h3>
                        </div>
                         <div class="box-content">
                            <form action="#" method="POST" class='form-horizontal form-bordered'>
                                <asp:Panel ID="Panel1" runat="server">
                             
                                    <div class="col-sm-12" style="">
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lAEmpCode" style="">
                                                Party Code
                                            </label>
                                            <div class="col-sm-2 tAEmpCode" style="">
                                                <asp:TextBox ID="txtEmpCode" runat="server" placeholder="Party Code" MaxLength="50"
                                                    class="form-control"  AutoPostBack="true" ></asp:TextBox>
                                            </div>
                                        </div>
                                         <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lALeaveName" style="">
                                                        Type of Compeny:<span id="Span2" runat="server" style="color: Red; display: inline;">*</span></label>
                                                    <div class="col-sm-5 tAEmpName" style="" id="Div1">
                                                        <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                                                            <asp:ListItem>Contactor</asp:ListItem>
                                                            <asp:ListItem>Sub-Contactor</asp:ListItem>
                                                            <asp:ListItem>Advertising Compeny</asp:ListItem>
                                                        </asp:RadioButtonList>  
                                                    </div>

                                                </div>
                                       
                                   </div>
                                    <div class="col-sm-12" style="">
                                        <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lALeaveName" style="">
                                                        Deduct type:<span id="Span1" runat="server" style="color: Red; display: inline;">*</span></label>
                                                    <div class="col-sm-2 tAEmpName" style="" id="">
                                                        <asp:DropDownList ID="ddlEmpName" runat="server"  class="form-control DropdownCss"  TabIndex="1" AutoPostBack="True"
                                                            >
                                                          
                                                        </asp:DropDownList>
                                                        
                                                    </div>

                                                </div>
                                       
                                    </div>
                                 <div class="col-sm-12" style="">
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lAEmpCode" style="">
                                               Deductee Name:
                                            </label>
                                            <div class="col-sm-2 tAEmpCode" style="">
                                                <asp:TextBox ID="txtdeductename" runat="server" placeholder="Deductee Name" MaxLength="50"
                                                    class="form-control"  AutoPostBack="true" ></asp:TextBox>
                                            </div>
                                        </div>
                                     </div>
                                     <div class="col-sm-12" style="">
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lAEmpCode" style="">
                                              Pan:
                                            </label>
                                            <div class="col-sm-2 tAEmpCode" style="">
                                                <asp:TextBox ID="txtpan" runat="server" placeholder="Deductee Name" MaxLength="50"
                                                    class="form-control"  AutoPostBack="true" ></asp:TextBox>
                                            </div>
                                             <div class="form-group">
                                                  <div class="col-sm-2 tAEmpCode" style="">
                                                      <asp:CheckBox ID="ckbaplied" runat="server" />
                                           
                                            <label for="textfield" class="control-label col-sm-3 lAEmpCode" style="">
                                               APPLIED
                                            </label>
                                                      </div>
                                                 
                                                 <div class="col-sm-3 tAEmpCode" style="">
                                                     <asp:Button ID="Button1" runat="server" Text="VerifyPin" />
                                            </div>
                                           
                                        </div>

                                     </div>
                                         </div>

                                         <div class="col-sm-12" style="">
                                        <div class="form-group">
                                            
                                                  <div class="col-sm-2 tAEmpCode" style="">
                                                      <asp:CheckBox ID="ckblowerrate" runat="server" /> Is Lower Rate
                                            </div>
                                            
                                            </div>
                      
                                             </div>
                                         
                                    </asp:Panel>
                                <asp:Panel ID="Panel2" runat="server">
                                     <div class="col-sm-12" style="">
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lAEmpCode" style="">
                                                Contact Person :
                                            </label>
                                            <div class="col-sm-2 tAEmpCode" style="">
                                                <asp:TextBox ID="TextBox1" runat="server" placeholder="Party Code" MaxLength="50"
                                                    class="form-control"  AutoPostBack="true" ></asp:TextBox>
                                            </div>
                                        </div>
                                         </div>
                                          <div class="col-sm-12" style="">
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lAEmpCode" style="">
                                               Mobile No. :
                                            </label>
                                            <div class="col-sm-2 tAEmpCode" style="">
                                                <asp:TextBox ID="TextBox2" runat="server" placeholder="Party Code" MaxLength="50"
                                                    class="form-control"  AutoPostBack="true" ></asp:TextBox>
                                            </div>
                                        </div>
                                              </div>
                                               <div class="col-sm-12" style="">
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lAEmpCode" style="">
                                              Email Address :
                                            </label>
                                            <div class="col-sm-2 tAEmpCode" style="">
                                                <asp:TextBox ID="TextBox3" runat="server" placeholder="Party Code" MaxLength="50"
                                                    class="form-control"  AutoPostBack="true" ></asp:TextBox>
                                            </div>
                                        </div>
                                                   </div>
                                                   <hr />
                                                    <div class="col-md-6">
        <h4>  CIT (TDS)</h4>
            <table>
              
                <tr>
                    <td>Address 1 :</td> <td> <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox></td>
                </tr>
            <tr>
                <td>Address 2 : </td> <td><asp:TextBox ID="TextBox8" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Address 3 :</td> <td> <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>City : </td> <td><asp:TextBox ID="TextBox10" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    Pincode : </td> <td><asp:TextBox ID="TextBox11" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Flat/Door/Block.no : </td> <td><asp:TextBox ID="TextBox4" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    Premise/Bldg./Village :</td> <td> <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Road/Street/Lane/P.O :</td> <td> <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Town/City/District :</td> <td> <asp:TextBox ID="TextBox16" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    State/U.T. :</td> <td> <asp:TextBox ID="TextBox17" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Country :</td> <td> <asp:TextBox ID="TextBox18" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Pincode :</td> <td> <asp:TextBox ID="TextBox19" runat="server"></asp:TextBox></td>
            </tr>
            </table>
 <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lAEmpCode" style="">
                                           Notes  :
                                            </label>
                                            <div class="col-sm-2 tAEmpCode" style="">
                                                <asp:TextBox ID="TextBox12" runat="server" placeholder="Party Code" MaxLength="50"
                                                    class="form-control"  AutoPostBack="true" ></asp:TextBox>
                                            </div>
     </div>
      <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lAEmpCode" style="">
                                          Phone :
                                            </label>
                                            <div class="col-sm-2 tAEmpCode" style="">
                                                <asp:TextBox ID="TextBox13" runat="server" placeholder="Party Code" MaxLength="50"
                                                    class="form-control"  AutoPostBack="true" ></asp:TextBox>
                                            </div>
          </div>
<div class="col-md-12">
           <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lAEmpCode" style="">
                                            Email Address :
                                            </label>
                                            <div class="col-sm-2 tAEmpCode" style="">
                                                <asp:TextBox ID="TextBox14" runat="server" placeholder="Party Code" MaxLength="50"
                                                    class="form-control"  AutoPostBack="true" ></asp:TextBox>
                                            </div>
               </div></div>
                <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lAEmpCode" style="">
                                            Web Site :
                                            </label>
                                            <div class="col-sm-2 tAEmpCode" style="">
                                                <asp:TextBox ID="TextBox15" runat="server" placeholder="Party Code" MaxLength="50"
                                                    class="form-control"  AutoPostBack="true" ></asp:TextBox>
                                            </div>

                    </div>
                                                                             <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lAEmpCode" style="">
                                           Keep This Deductee in this Group:
                                            </label>
                                           
                                                 <asp:DropDownList ID="DropDownList2" runat="server"></asp:DropDownList>
                                            </div>
 <div class="col-sm-2 tAEmpCode" style="">
     <asp:Button ID="Button2" runat="server" Text="Accept" />
                                            </div>
                                                                                  <div class="col-sm-2 tAEmpCode" style="">
                                                                                      <asp:Button ID="Button3" runat="server" Text="Cancel" />
                                            </div>

                    </div>
                                </asp:Panel>
                                </form>
                                </div>
                        </div>
                    </div>
         </div>
    
</asp:Content>
