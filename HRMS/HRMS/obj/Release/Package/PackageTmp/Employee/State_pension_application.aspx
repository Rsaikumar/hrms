﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="State_Pension_Application.aspx.cs" Inherits="HRMS.Employee.State_Pension_Application" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script type="text/javascript">
         function PrintPanel() {
             var panel = document.getElementById("<%=pnlContents.ClientID %>");
            var printWindow = window.open('', '', 'height=200,width=2000');
            printWindow.document.write('<html><head><title>DIV Contents</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
            return false;
        }
    </script>
    <style type="text/css">
        .auto-style1
        {
            width: 667px;
        }

        .auto-style2
        {
            width: 200px;
        }

        .auto-style3
        {
            width: 296px;
            height: 35px;
        }

        .auto-style5
        {
            width: 296px;
            height: 39px;
        }

        .auto-style6
        {
            width: 1104px;
            height: 39px;
        }

        .auto-style7
        {
            width: 1104px;
            height: 35px;
        }

        .auto-style8
        {
            width: 305px;
        }

        .auto-style9
        {
            width: 1105px;
        }

        .auto-style11
        {
            width: 45px;
        }

        .auto-style12
        {
            width: 266px;
        }

        .auto-style13
        {
            width: 394px;
        }

        .auto-style14
        {
            width: 45px;
            height: 63px;
        }

        .auto-style15
        {
            width: 266px;
            height: 63px;
        }

        .auto-style16
        {
            width: 394px;
            height: 63px;
        }

        .auto-style17
        {
            height: 63px;
        }

        .auto-style18
        {
            width: 301px;
        }

        .auto-style19
        {
            width: 399px;
        }

        .auto-style20
        {
            width: 452px;
        }

        .auto-style21
        {
            width: 660px;
        }

        .auto-style22
        {
            height: 19px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="#">Pension</a><i class="fa fa-angle-right"></i> </li>
                        <li><a href="State_pension_application.aspx" class="current">State Application</a> </li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <i class="fa fa-th-list"> State Pension Application Form</i>
                        </header>
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <asp:Panel ID="pnlContents" runat="server">
                                    <div>
                                        <br />
                                        <br />
                                        <p>
                                            1.	Application for Pensioner to be filled and give in pension dept. (Mr. Samuel)<br />
                                            <br />
                                            2.	From Pension dept. it goes to The Chief Secretary to Govt. (here they verify and the send to
                                    <br />
                                            <br />
                                            3.	Pension Department from there it goes to GAD ( General Administrative Department) here again they verify all the payables (basic salary +da)<br />
                                            <br />
                                            4.	PAO (Pay and Accounts Officer) here they will get all the details of Employees Pay Details , Service details and it will be forwarded to
                                    <br />
                                            <br />
                                            5.	AG (Accountant General) office here they will verify and give authorization for payment and sent to
                                    <br />
                                            <br />
                                            6.	JD / PPO (Joint Director or Pension Payment Officer, Nampally) here Payment will be done<br />
                                            <br />
                                            <br />
                                            At the time of filling application total details and calculation will be done.<br />
                                            X X X X X X X X X X X X X X X<br />
                                            Service Pension forwarding letter form collected.<br />
                                        </p>
                                        <br />
                                        <br />
                                        <br />

                                        <h4 align="center">SERVICE PENSION FORWARDING LETTER</h4>
                                        <h5 align="center">(G.O.Ms. No. 263 Date 23-11-1981)</h5>
                                        <div>
                                            <table>
                                                <tr>
                                                    <td class="auto-style1">Lr.No:</td>
                                                    <td class="auto-style2">Date :</td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style1">From</td>
                                                    <td class="auto-style2">To</td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style1">
                                                        <asp:TextBox ID="txtcircle" runat="server" required></asp:TextBox></td>
                                                    <td class="auto-style2">The Accountant General (A&E)</td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style1">
                                                        <asp:TextBox ID="txtdistrict" runat="server" required></asp:TextBox></td>
                                                    <td class="auto-style2">Audit Officer, LFA, District</td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style1">
                                                        <asp:TextBox ID="txtsubdistrict" runat="server" required></asp:TextBox></td>
                                                    <td class="auto-style2">Andhra Pradesh</td>
                                                </tr>

                                            </table>

                                        </div>


                                    </div>
                                    <br />
                                    <br />
                                    <br />
                                    <br />
                                    <p>Sir,</p>
                                    <p>Sub: Pension/Service Pension/Family pension and other Retiring Benefits in respect of  Sri/Smt. <asp:TextBox ID="txtemployeename" runat="server" required></asp:TextBox> proposals Submitted.</p>
                                    <p>Ref: G.O. Ms. No. 262 Fin & Plg. (FW-PSC) Dept. Dt. 23-11-98</p>
                                    <p>G.O. Ms. No. 263 Fin & Plg. (FW _ PSC) Dept. Dt. 23-11-98</p>
                                    <p>I am to forward here with the Service/Family Pension Papers of Sri/Smt <asp:TextBox ID="txtforwardservcename" runat="server" required>

                                                                                                              </asp:TextBox> who is due to retire/retired/expired while in service on <asp:TextBox ID="txtexpiredservice" runat="server" required></asp:TextBox>  <ajaxToolKit:CalendarExtender ID="CalendarRenewalDate" PopupButtonID="imgPopup" runat="server"
                                                  TargetControlID="txtexpiredservice" Format="dd-MM-yyyy" PopupPosition="BottomLeft">  </ajaxToolKit:CalendarExtender>
 the following documents are forwarded herewith for authorising pensioner Benefits.</p>
                                    <br />
                                    <br />
                                    <p>1.   Part – 1	:	Information furnished by the Govt. Servant/Family Pension   Beneficiary.</p>
                                    <br />
                                    <br />
                                    <p>2.   Part - IIA 	:	Information filled in by the Pension Sanctioning Authority.</p>
                                    <br />
                                    <br />
                                    <p>3.   Part – II B	: 	Sanction Order of Pensionary Benefits.</p>
                                    <br />
                                    <br />
                                    <p>4.   Annexure – I	: 	Descriptive Roll</p>
                                    <br />
                                    <br />
                                    <p>5.   Annexure – II	:	Nomination</p>
                                    <br />
                                    <br />
                                    <p>6.   Copy of Annexure – III</p>
                                    <br />
                                    <br />
                                    <p>7.   Service Register of Govt. Servant.</p>
                                    <br />
                                    <br />
                                    <p>8.   L.P.C</p>
                                    <br />
                                    <br />
                                    <p>9.   Death Certificate (in case of Death)</p>
                                    <br />
                                    <br />
                                    <p>10	Legal heir certificate ( - do - )</p>
                                    <br />
                                    <br />
                                    <p>11. <asp:TextBox ID="txtreceipt" runat="server" required></asp:TextBox></p>
                                    <br />
                                    <br />
                                    <p>The receipt o;f above documents may please be acknowledged and arrangements may be made to issue the PPO/Gratuity Payment Order and commutation Authorisation early.</p>
                                    <br />
                                    <br />

                                    <div>
                                        <table>
                                            <tr>
                                                <td class="auto-style6">No. of enclosures  (                      )</td>
                                                <td class="auto-style5"></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style7"></td>
                                                <td class="auto-style3">Yours faithfully</td>
                                            </tr>
                                        </table>
                                        <p>1.	Copy to Sri/Smt <asp:TextBox ID="txtcopyname" runat="server"></asp:TextBox></p>
                                        <p>2.	Copy to the <asp:TextBox ID="txtcopyaddress" runat="server"></asp:TextBox></p>
                                        <div>
                                            <h4 align="center">SERVICE PENSION PAPERS</h4>
                                            <h5 align="center">FORM OF APPLICATION FOR SERVICE PENSION / FAMILY PENSION / RETIREMENT GRATUITY / SERVICE GRATUITY / COMMUTATION</h5>
                                            <h5 align="center">(To be furnished in duplicate)</h5>
                                            <h5 align="center">G.O.Ms.No.263, Dated.23.11.1998</h5>
                                            <br />
                                            <br />
                                            <p>Part – I information to be furnished by the Government Servant / Applicant</p>
                                            <br />
                                            <br />
                                            <p>(The Pension Sanctioning Authority shall forward the application duly processed to the AG (A&E) / LF Authority within a period of 30 days</p>

                                            <div>
                                                <table border="1">
                                                    <tr>
                                                        <td>1</td>
                                                        <td colspan="3">a). Name of the Government servant </td>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="txtgvtserventname" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td colspan="3">b). Post held</td>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="txtpostheld" runat="server" required></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender1" PopupButtonID="imgPopup" runat="server"
                                                           TargetControlID="txtpostheld" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                       </ajaxToolKit:CalendarExtender>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td colspan="3">Name of the Applicant 
(in case of death of government servant)
                                                        </td>
                                                        <td colspan="2"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td colspan="3">Permanent Address</td>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="txtparmanetaddress" runat="server" required></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>4</td>
                                                        <td colspan="3">Address after retirement</td>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="txtafterretirementaddress" runat="server" required></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>5</td>
                                                        <td colspan="3">Commutation of Pension
                                                    <br />
                                                            a). Whether willing to commute 40% of monthly pension, subject to AP Civil pensions (commutation) Rules, 1944.
                                                        </td>
                                                        <td colspan="2"></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td colspan="3">b). If the answer is ‘NO’ specify the fraction less that 40%</td>
                                                        <td colspan="2"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>6</td>
                                                        <td colspan="3">a) Name of the Pension Disbursing Authority </td>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="txtpansionauthority" runat="server" required></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td colspan="3" rowspan="2">b) Name of the paying bank from where pension payment is desired by the pensioner / family pensioner / graduating </td>
                                                        <td>Name of the Bank and Branch </td>
                                                        <td>S.B . Account No.</td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                            <asp:TextBox ID="txtbankname" runat="server" required></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtbankacountno" runat="server" required></asp:TextBox>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td colspan="4">7. List of Family Members</td>

                                                    </tr>
                                                    <tr>
                                                        <td>(a)</td>
                                                        <td>(b)</td>
                                                        <td>(c)</td>
                                                        <td>(d)</td>
                                                        <td colspan="2">Marital / Employment status of the children of the applicant / deceased Government servant</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Sl.No</td>
                                                        <td>Name of the Family Member</td>
                                                        <td>Date of Birth</td>
                                                        <td>Relationship with government Servant </td>
                                                        <td>Married or unmarried Date of Marriage if married </td>
                                                        <td>Whether employed or not. Give details of employment </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <br />
                                            <br />
                                            <p>Instructions:</p>
                                            <br />
                                            <br />
                                            <p>
                                                1.	The Government servant is instructed to fill up the proforma very carefully as the date furnished is vital for sanction of family pension.  He / She may note that alterations of the data furnished at a later date is not permissible.<br />
                                                2.	The “family” for the purpose mean “wife” or “husband” as the case may be, “sons” and “unmarried daughters” as laid down in Rule 50(12) for (for Family Pension) and Rule 46(5) (for gratuity) of AP Revised Pension rules, 1980.<br />
                                                3.	In case of death while service of Government servant, the answer Married’ in case of daughters will be understood that the daughter is already married as on the date of death of the Government servant.<br />
                                            </p>
                                            <br />
                                            <br />
                                            <h5 align="center">DECLARATION</h5>
                                            <br />
                                            <br />
                                            <p>
                                                1.	I undertake to refund the amount of Pension, Gratuity and Commutation, if it is found subsequently to be in excess of the amount to which I was entitled under the Rules.<br />
                                                2.	I solemnly affirm that the particulars given by me in Part-I at item 7 are correct and true to the best of my knowledge.  If found false in future, I am liable for suitable action as may be taken by the Government.
                                        <br />
                                                3.	The particulars given above are correct and true to the best of my knowledge.  If round false in future I may be liable for any action that may be taken by the Government.<br />
                                            </p>
                                            <div>
                                                <table>
                                                    <tr>
                                                        <td class="auto-style9">Place:</td>
                                                        <td class="auto-style8">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="auto-style9"></td>
                                                        <td class="auto-style8">Signature of the </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="auto-style9"></td>
                                                        <td class="auto-style8">Government Servant / Applicant</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="auto-style9">Date:</td>
                                                        <td class="auto-style8"></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <br />
                                            <br />
                                            <h4 align="center">TO BE FILLED IN BY THE HEAD OF THE OFFICE</h4>
                                            <br />
                                            <br />
                                            <p>
                                                1. Application for pension / gratuity etc. in Part I is received on <asp:TextBox ID="txtappfpensionrecived" runat="server"></asp:TextBox><br /><ajaxToolKit:CalendarExtender ID="CalendarExtender2" PopupButtonID="imgPopup" runat="server"
                                                           TargetControlID="txtappfpensionrecived" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                       </ajaxToolKit:CalendarExtender>
                                                (Date to be recorded)<br />
                                                <br />

                                                2. Certified that the person / persons mentioned by the Government servant / Applicant in item 8 of Part I are legally entitled to receive the pension / share in gratuity.<br />
                                                <br />
                                                3. Guardianship certificate : (to be filled in wherever necessary)
                                        <br />
                                                <br />

                                                This is to certify that the following minors of the deceased Government Servant
                                        <br />
                                                <br />
                                                <br />
                                                Late Sri / Smt <asp:TextBox ID="txtlatename" runat="server"></asp:TextBox> is / are under the guardianship
                                        <br />
                                                <br />

                                                of Sri / Smt <asp:TextBox ID="txtlateguardname" runat="server"></asp:TextBox><br />
                                                <br />
                                            </p>
                                            <div>
                                                <table>
                                                    <tr>
                                                        <td class="auto-style11"></td>
                                                        <td class="auto-style12">Name</td>
                                                        <td class="auto-style13">Date Of Birth</td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="auto-style11">1</td>
                                                        <td class="auto-style12"></td>
                                                        <td class="auto-style13"></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="auto-style11">2</td>
                                                        <td class="auto-style12"></td>
                                                        <td class="auto-style13"></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="auto-style11">3</td>
                                                        <td class="auto-style12"></td>
                                                        <td class="auto-style13"></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="auto-style11">Place:</td>
                                                        <td class="auto-style12"></td>
                                                        <td class="auto-style13"></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="auto-style11">Date:</td>
                                                        <td class="auto-style12"></td>
                                                        <td class="auto-style13"></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="auto-style11"></td>
                                                        <td class="auto-style12"></td>
                                                        <td class="auto-style13"></td>
                                                        <td>Signature of the Head of the Office.</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="auto-style14"></td>
                                                        <td class="auto-style15"></td>
                                                        <td class="auto-style16"></td>
                                                        <td class="auto-style17">Office Seal:</td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div>
                                                <table border="1">
                                                    <tr>
                                                        <td colspan="4">Part – II (A) Information to be filled up by the Pension Sanctioning Authority</td>

                                                    </tr>
                                                    <tr>
                                                        <td>1. Name of the Government Servant and Post Held</td>
                                                        <td colspan="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2.Father’s Name / Husband’s Name</td>
                                                        <td colspan="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>3.Name of the Applicant(in case of death of government servant)</td>
                                                        <td colspan="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>4.Date of Birth of Government Servant</td>
                                                        <td colspan="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>5.Date of entering in to service  </td>
                                                        <td colspan="3">
                                                            <asp:TextBox ID="txtentering" runat="server" required></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender3" PopupButtonID="imgPopup" runat="server"
                                                           TargetControlID="txtentering" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                       </ajaxToolKit:CalendarExtender>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>6.Date of retirement / death</td>
                                                        <td colspan="3">
                                                            <asp:TextBox ID="txtdateofretirement" runat="server" required></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender4" PopupButtonID="imgPopup" runat="server"
                                                           TargetControlID="txtdateofretirement" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                       </ajaxToolKit:CalendarExtender>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>7.Designation and office from which the Government servant retires / retired / died</td>
                                                        <td colspan="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td rowspan="10">8.The rule applicable 
                            <br />
                                                            a). The relevant Rule under the AP Revised Pension Rules.  1980 applicable (tick the rule number(s) applicable and strike out the rest)</td>
                                                        <td colspan="3">Rule 33 Superannuation Pension (Rule42)</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">Rule 34 Retiring Pension (Rule 43 / 44)</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">Rule 35 Pension on absorption under a corporation </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">Rule 37 Invalid Pension </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">Rule 38 Compensation pension </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">Rule 39 Compulsory Retirement Pension </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">Rule 40 Compassionate Allowance</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">Rule 43 Retirement on Completion of 20 years of qualifying service </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">Rule 44 Retiring on completion of 33 years of qualifying service </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">Rule 46irement Gratuity </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">Rule 50 Family pension</td>
                                                    </tr>
                                                    <tr>
                                                        <td>b)Whether ANTICIPATORY  PENSION is being sanctioned in terms of Rule-51.</td>
                                                        <td colspan="3">
                                                            <asp:DropDownList ID="ddyes" runat="server" AutoPostBack="True">
                                                                <asp:ListItem>---select---</asp:ListItem>
                                                                <asp:ListItem>Yes</asp:ListItem>
                                                                <asp:ListItem>No</asp:ListItem>
                                                            </asp:DropDownList></td>
                                                    </tr>
                                                    <tr>
                                                        <td>c). Whether PROVISIONAL PENSION is being sanctioned in terms of Rule 9(4) read with Rule 32</td>
                                                        <td colspan="3">
                                                            <asp:DropDownList ID="ddno" runat="server" AutoPostBack="True">
                                                                <asp:ListItem>---select---</asp:ListItem>
                                                                <asp:ListItem>Yes</asp:ListItem>
                                                                <asp:ListItem>No</asp:ListItem>
                                                            </asp:DropDownList></td>
                                                    </tr>
                                                    <tr>
                                                        <td>d).Any other rule applicable </td>
                                                        <td colspan="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>9.Total Service (6-5)</td>
                                                        <td colspan="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>10.Periods of non-qualifying service</td>
                                                        <td colspan="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>a).E.O.I</td>
                                                        <td colspan="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>b).Suspension period </td>
                                                        <td colspan="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>c)Dies-non</td>
                                                        <td colspan="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>d).Boy Service </td>
                                                        <td colspan="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>e) Any other service not qualifying for pension </td>
                                                        <td colspan="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Total non-qualifying service (a to e)</td>
                                                        <td colspan="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>11.Net qualifying service (9-10)</td>
                                                        <td colspan="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>12.  Weightage, if any </td>
                                                        <td colspan="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>13.Total qualifying service for calculation of pension (11+12)</td>
                                                        <td colspan="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>14.Las pay drawn (Rule.31, 46(4), 50(12) (c) of APRPRs, 1980) Para 4 of G.O. Ms. No.87 Fin. & Plg. (FW-Pen.I) Dept. dt.25.05.98</td>
                                                        <td colspan="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>15.Calculation of service pension / Service Gratuity (Rule 45 of APRPRs, 1980)</td>
                                                        <td colspan="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>16.Calculation of Retirement gratuity (Rule 46 APRPRs, 1980</td>
                                                        <td colspan="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>17.a) Calculation of family pension </td>
                                                        <td colspan="3">
                                                            <asp:TextBox ID="txtfamilypension" runat="server" Text="0" ></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>b). Normal family pension </td>
                                                        <td colspan="3">
                                                            <asp:TextBox ID="txtnormalfamilypension" runat="server" Text="0"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>18.Period of payment of pension </td>
                                                        <td colspan="3">As applicable to the case under consideration </td>
                                                    </tr>
                                                    <tr>
                                                        <td>a).Service Pension </td>
                                                        <td colspan="3">From:         till death</td>
                                                    </tr>
                                                    <tr>
                                                        <td>b).Enhanced family pension </td>
                                                        <td colspan="3">From:         To:          </td>
                                                    </tr>
                                                    <tr>
                                                        <td>c).Normal Family Pension </td>
                                                        <td colspan="3">From:         To:         </td>
                                                    </tr>
                                                    <tr>
                                                        <td>19.Government dues to be recovered in respect </td>
                                                        <td>Principal</td>
                                                        <td>Interest </td>
                                                        <td>Total</td>
                                                    </tr>
                                                    <tr>
                                                        <td>a). House building Advance </td>
                                                        <td>
                                                            <asp:TextBox ID="txthbaprinciple" runat="server" Height="21px" Text="0"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txthbaintrest" runat="server" Text="0"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txthbatotal" runat="server" Text="0"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>b). Motor Car / Cycle advance </td>
                                                        <td>
                                                            <asp:TextBox ID="txtmotarprinciple" runat="server" Text="0"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtmotarintrest" runat="server" Text="0"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtmotartotal" runat="server" Text="0"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>c) Marriage Advance </td>
                                                        <td>
                                                            <asp:TextBox ID="txtmariageprinciple" runat="server" Text="0"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtmariageintrest" runat="server" Text="0"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtmariagetotal" runat="server" Text="0"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>d) Advance leave salary </td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>e) Advance salary on transfer </td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>f) Dues on Account of Government quarters</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>g)Telephone / trunk call charges</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>h) Festival advance </td>
                                                        <td>
                                                            <asp:TextBox ID="txtfestivalprinciple" runat="server" Text="0"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtfestivalintrest" runat="server" Text="0"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtfestivaltotal" runat="server" Text="0"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="auto-style22">i) Education advance </td>
                                                        <td class="auto-style22">
                                                            <asp:TextBox ID="educationprinciple" runat="server" Text="0"></asp:TextBox>
                                                        </td>
                                                        <td class="auto-style22">
                                                            <asp:TextBox ID="educationintrest" runat="server" Text="0"></asp:TextBox>
                                                        </td>
                                                        <td class="auto-style22">
                                                            <asp:TextBox ID="educationtotal" runat="server" Text="0"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>j) Computer Advance </td>
                                                        <td>
                                                            <asp:TextBox ID="txtcomputerprinciple" runat="server" Text="0"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtcomputerintrest" runat="server" Text="0"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtcomputertotal" runat="server" Text="0"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>k) Other Government dues </td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>

                                                    </tr>
                                                    <tr>
                                                        <td>Total</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4">Note: Information with conditions will not be accepted by Pension issuing Authority .  Amounts for recovery should be specified and should be in whole rupees.  Absence of information will be understood as no dues for recovery.</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4">20. LPC is enclosed / LPC will be sent after retirement </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div>
                                            <h4 align="center">PART – II (B)</h4>

                                            <h5 align="center">SANCTION OF PENSION</h5>
                                            <p>
                                                a). Certificate of competency to accord sanction (applicable in case of sanction of pension to non-gazetted officers including Class-IV employees)<br />
                                                <br />

                                                i). I am declared by the Head of the department to be the Head of an office to accord sanction in this case under the powers delegated vide G.O.Ms.No.262, Finance & Planning (FW-PSC) Department dated.23.11.1998.<br />
                                                <br />

                                                <h5 align="center">OR</h5>
                                                <br />
                                                <br />

                                                ii). I am the next Gazetted Authority in the hierarchy to the Head of the office in this case who is a non-gazetted officer and hence, I am competent to accord sanction under the powers delegated vide G.O.Ms.No.262, finance & Planning (FW-PSC) Department dated.23.11.1998.<br />
                                                (Strike off whichever is not applicable)<br />
                                                <br />

                                                b). Sanction order:<br />
                                                <br />
                                                Pensionary benefits including commutation found admissible under the rules may be authorized.  It is verified from the records in my custody and certify that no disciplinary or judicial proceedings are pending / contemplated against retiring / retired government servant to who I am the authority for sanction of pension.<br />
                                                <br />
                                                <p>
                                                </p>
                                                <div>
                                                    <table>
                                                        <tr>
                                                            <td class="auto-style18">i). Service pension </td>
                                                            <td>:</td>
                                                            <td class="auto-style19"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="auto-style18">ii).Retiring gratuity </td>
                                                            <td>:</td>
                                                            <td class="auto-style19"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="auto-style18">iii) Commutation </td>
                                                            <td>:</td>
                                                            <td class="auto-style19"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="auto-style18">iv) Family pension </td>
                                                            <td>:</td>
                                                            <td class="auto-style19">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="auto-style18">a) Enhanced family pension </td>
                                                            <td>:</td>
                                                            <td class="auto-style19"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="auto-style18">b) Normal Family pension </td>
                                                            <td>:</td>
                                                            <td class="auto-style19"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="auto-style18">Official Seal </td>
                                                            <td class="auto-style19"></td>
                                                            <td>Signature and Designation of Pension Sanctioning Authority </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="auto-style18"></td>
                                                            <td class="auto-style19"></td>
                                                            <td>Date</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <br />
                                                <p>
                                                    Note 1: This is to be prepared in duplicate by the Pension sanctioning Authority, one for the record of Pension Sanctioning Authority and the other one to be sent to Accountant General / Local Fund Audit Officer.
                    <br />
                                                    <br />
                                                    Note 2: The Pension sanctioning Authority should satisfy about the correctness of the particulars of family furnished by the Government servant / Applicant in Part-I
                    <br />
                                                    <br />
                                                    Note 3: If the pensionery benefits are not be released Part-II-B(b) shall be struck off.
                    <br />
                                                    <br />
                                                    Note 4: If there is any likelihood of delay, Anticipatory Pension / Anticipatory Gratuity as per Rule 51 of A.P.Revised Pension Rules 1980 shall be drawn and paid by the Head of office to the beneficiary without any delay.
                    <br />
                                                    <br />
                                                    Note 5: Heads of Departments are those listed in Appendix-I mentioned in Article 6 of A.P. Financial Code Volume-I / subsidiary Rule 32 (ii) of FR 9<br />
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                            </p>
                                        </div>
                                        <div>
                                            <h4 align="center">ANNEXURE-I</h4>
                                            <h5 align="center">DESCRIPTIVE ROOLS</h5>

                                            <p>A.SPACE FOR PHOTOGRAPHS:</p>
                                            <br />
                                            <table>
                                                <tr>
                                                    <td>single photo</td>
                                                    <td>joint photo</td>
                                                </tr>
                                                <tr>
                                                    <td>Service Pensioner / Family Pensioner / Gratuity / Guardian of Minor or Handicapped Child</td>
                                                    <td>Joint Photo of Service Pensioner with Family Pension beneficiary /Guardian with minor or Handicapped</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">(Attestation has to be done across the Photos by a Gazetted Office of AP Government in Service )</td>
                                                </tr>
                                            </table>
                                            <p>B.SPECIMEN SIGNATURE OF :</p>
                                            <br />
                                            <p>(i) Service Pensioner:</p>
                                            <br />
                                            <p>Specimen signature of Sri./ Smt / Kum</p>
                                            <br />
                                            <p>Son/ Wife / Daughter of </p>
                                            <br />
                                            <p>1.</p>
                                            <br />
                                            <p>2.</p>
                                            <br />
                                            <p>3.</p>
                                            <br />
                                            <br />
                                            <p>D.LEFT HADND THUMB AND FINGER IMPRESSIONS OF SERVICE PENSIONER / FAMILY PENSIONER / GRATUITANT / GUARDIN OF MINOR OR HANDICAPPED CHILD: (TO BE GIVEN BY THE ILLETERATE OR THOSE UNABLE TO SIGN AND FOR OTHERS IT IS OPTIONAL)</p>
                                            <br />
                                            <table border="1">
                                                <tr>
                                                    <td>Details</td>
                                                    <td>Thumb finger</td>
                                                    <td>Fore finger</td>
                                                    <td>Middle finger</td>
                                                    <td>Ring finger</td>
                                                    <td>Little finger</td>
                                                </tr>
                                                <tr>
                                                    <td>Service Pensioner </td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>Family Pensioner/ </td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>Gratuitant /</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>Guardian of Minor Handicapped Child </td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                            <div>
                                                <br />
                                                <br />
                                                <table>
                                                    <tr>
                                                        <td class="auto-style20">Place :</td>
                                                        <td>Attested by Signature</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="auto-style20">Date:</td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="auto-style20"></td>
                                                        <td>Name:</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="auto-style20"></td>
                                                        <td>Designation</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">Official Seal:</td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <br />
                                            <br />
                                            <p>
                                                (Attestation has to be done by a Gazzetted Officer of AP State Government in Service)<br />
                                                Note:3 copies will be forwarded to Accountant General / Local Fund Audit Officer by Pension Sanctioning Authority and one will be retained by the Pension Sanctioning Authority.
                                        <br />
                                            </p>
                                            <br />

                                        </div>
                                        <div>
                                            <h4 align="center">ANNEXURE – II
                                            </h4>
                                            <h5 align="center">NOMINATION
                                            </h5>
                                            <br />
                                            <p>
                                                (The Government servant may use separate forms, if he wishes to make different nominations for each type of payment mentioned below)
                                        <br />
                                                I here by nominate the person / persons mentioned below and confer on him / her / them the right to receive Life Time Arrears of Pension, Retirement Gratuity that may be sanctioned by Government, in the event of my death while in service and right to receive on my death Life Time Arrears of Pension, Retirement Gratuity, commuted value of pension, Death Relief which having become admissible to me on retirement – which may remain unpaid at my death.
                                            </p>
                                            <br />
                                            <br />
                                            <div>
                                                <table border="1">
                                                    <tr>
                                                        <td>Name and address of Nominee’s </td>
                                                        <td>Relation ship  with Government Servant </td>
                                                        <td>Age</td>
                                                        <td>Amount of share payable to each in Col. 1</td>
                                                        <td>Contingence on the happening of which the nomination shall become invalid(Death need not be mentioned </td>
                                                        <td>Name and address, relation ship and age of the alternative nominees(s) to whom the right conferred on the nominees (s) nomination to him / her / them becoming ineffective </td>
                                                        <td>Amount or share payable to each in Col .6</td>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>2</td>
                                                        <td>3</td>
                                                        <td>4</td>
                                                        <td>5</td>
                                                        <td>6</td>
                                                        <td>7</td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <br />
                                            <br />
                                            <p>
                                                This nomination supersedes the nomination made by me earlier on <asp:TextBox ID="txtnominationon" runat="server" required></asp:TextBox><br /><ajaxToolKit:CalendarExtender ID="CalendarExtender5" PopupButtonID="imgPopup" runat="server"
                                                           TargetControlID="txtnominationon" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                       </ajaxToolKit:CalendarExtender>
                                                NB. The Government Servant shall draw lines across the blank space below the last entry to prevent the insertions of any name after he / she has signed.<br />
                                                Dated this <asp:TextBox ID="txtdatedthis" runat="server" required></asp:TextBox> <ajaxToolKit:CalendarExtender ID="CalendarExtender6" PopupButtonID="imgPopup" runat="server"
                                                           TargetControlID="txtdatedthis" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                       </ajaxToolKit:CalendarExtender>day of <asp:TextBox ID="txtdayof" runat="server"></asp:TextBox> <ajaxToolKit:CalendarExtender ID="CalendarExtender7" PopupButtonID="imgPopup" runat="server"
                                                           TargetControlID="txtdayof" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                       </ajaxToolKit:CalendarExtender>20 <asp:TextBox ID="txtyear" runat="server"></asp:TextBox> at<asp:TextBox ID="txtplace" runat="server"></asp:TextBox><br />
                                            </p>
                                            <br />
                                            <br />
                                            <p>Witness:-</p>
                                            <br />
                                            <table>
                                                <tr>
                                                    <td>1.	Signature, Name and Address:
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>2.	Signature, Name and Address:
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>Signature of the Government Servant</td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>Name:</td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>Designation:</td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>Office:</td>
                                                </tr>
                                            </table>
                                            <p>
                                                Countersigned
                                            </p>
                                            <p>
                                                Signature of Head of Office/Department:<br />
                                                <br />
                                                Date:<br />
                                                <br />

                                                Name and Designation
                                        <br />
                                                <br />

                                                Office Seal:<br />
                                                <br />
                                            </p>

                                            <p>
                                                Note (1):	The Government servant who has a family may nominate one member or more than one member of the family as defined in Rule 46(5) of A.P. Revised Pension Rules, 1980.
                                        <br />
                                                <br />
                                                Note (2):	The Government servant who has no family mat nominate a person or persons, or a body of individuals, whether incorporated or not.
                                        <br />
                                                <br />
                                                Note (3):	The Government servant may not that the nomination with signature of two witnesses shall only have the legal validity or a WILL.  
                                        <br />
                                                <br />
                                                Note (4):	This nomination form is to be submitted by the employees in triplicate, one for use of the pension sanctioning authority and two copies to be forwarded to the forwarded to the Account General Local Fund Audit Offices.
                                        <br />
                                                <br />
                                                Note (5):	For the purpose of Rules 46,47,48 and 49 of Revised Pension Rules, 1980, family in to a Government servant means:-
	<br />
                                                <br />
                                                i)	Wife or wifes in the case of a male Government servant 
                                        <br />
                                                <br />
                                                ii)	Husband, in the case of a female Government servant.
                                        <br />
                                                <br />
                                                iii)	Sons including step sons, posthumous son, and adopted sons (whose personal law permits such adaptation).
	<br />
                                                <br />
                                                iv)	Unmarried daughters including step daughters, posthumous daughters and adopted daughters (Whose personal law permits such adoption).    
                                        <br />
                                                <br />
                                                v)	Widowed daughters including step daughters and adopted daughters.
                                        <br />
                                                <br />
                                                vi)	Father   | including adoptive patents in the case of 	         |	 individuals where whose personal law  
                                        <br />
                                                <br />
                                                vii)	Mother   |   Permits adoption.
	<br />
                                                <br />
                                                viii)	Brothers below the age of 18 years including step brothers.
                                        <br />
                                                <br />
                                                ix)	Unmarried sisters and widowed sisters including step sisters.
	<br />
                                                <br />
                                                x)	Married daughters, and  
                                        <br />
                                                <br />
                                                xi)	Children of a pre-deceased son.<br />
                                            </p>
                                        </div>
                                        <div>
                                            <h4 align="center">ANNEXURE-III</h4>
                                            <h5 align="center">BY REGISTERED POST</h5>
                                            <br />
                                            <p>
                                                From<br />
                                                <br />
                                                <asp:TextBox ID="txtfromname" runat="server" required></asp:TextBox>

                                        <br />
                                                <asp:TextBox ID="txtaddress" runat="server"></asp:TextBox><br />
                                                <br />
                                                (Pension sanctioning authority)<br />

                                                <br />
                                                To<br />

                                                <br />
                                                The <asp:TextBox ID="txtnameofauthority" runat="server" required></asp:TextBox><br />

                                                <br />
                                                <asp:TextBox ID="txtauthoityaddress" runat="server"></asp:TextBox><br />

                                                <br />
                                                (Disciplinary Authority/
Appointing Authority/<br />
                                                <br />
                                                Head of the Department)<br />
                                            </p>
                                            <p>Sub:Pension – Sanction of Pension and Other Retiring Benefits in respect of Sri/Smt <asp:TextBox ID="txtretiringbenefits" runat="server"></asp:TextBox>   Designation <asp:TextBox ID="txtpendeignation" runat="server"></asp:TextBox> Regarding. </p>
                                            <br />
                                            <p>I am to inform you that the Pension/Family Pension application form of Sri/Smt <asp:TextBox ID="txtpenappname" runat="server"></asp:TextBox> retired/retiring on <asp:TextBox ID="txtretiredon" runat="server"></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender8" PopupButtonID="imgPopup" runat="server"
                                                           TargetControlID="txtretiredon" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                       </ajaxToolKit:CalendarExtender> as <asp:TextBox ID="txtasdesignation" runat="server" required></asp:TextBox> was forwarded to AGAP/LFA on……………. As per the records held by me no disciplinary/judicial proceedings are pending/contemplated against the above retiring/retired government employees.  I request you to verify whether any such case is pending against the above employee which entails  withholding or withdrawing pension  or part oaf pension  permanently or for specified period as laid down under Rule 9 of RPRs 1980.  If so the AG(A & E) Dy. Accountant General (Pension), O/o the AG (A&E) Andhra Pradesh., Hyderabad/ Local Fund Officer may be intimated accordingly by name either by Registered post or through a special messenger within one month from the date of issue of this letter for withholding pensionary benefits as contemplated in Govt. Memo.No.33764-A/55/PSC/93, Finance & Planning (FW-PSC) Department, date: 15-10-1993 and reiterated in government Memo.No. 37254/361/A2/Pen-I/98, Finance & Planning (FW-Pen,1) Department, date: 4-7-1998.  Copies of such orders shall also be sent to the concerned Treasury Officers/PPO for withholding the pensionary benefits.  In this connection the instructions issued in Para 2,Part-II B of G.O.Ms.No.263, Finance & Planning (FW-PSC) Department, date: 23-11-1998 may be scrupulously followed</p>
                                            <table>
                                                <tr>
                                                    <td class="auto-style21"></td>
                                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Yourfaithfully,</td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style21">Station:
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style21">Date:
                                                    </td>

                                                    <td>(PENSION SANCTIONING AUTHORITY)</td>
                                                </tr>
                                            </table>

                                        </div>


                                    </div>
                                </asp:Panel>
                            </div>
                          

                        </div>
                          
                        <div class="col-md-12 text-center" >
                            <asp:Button ID="btnPrint" runat="server" Text="Print" OnClientClick="return PrintPanel();" />
                            <asp:Button ID="btnsubmit" runat="server" Text="Save" OnClick="btnsubmit_Click"  />
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
