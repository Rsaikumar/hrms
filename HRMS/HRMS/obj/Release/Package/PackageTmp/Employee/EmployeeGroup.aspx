﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="EmployeeGroup.aspx.cs" Inherits="HRMS.Employee.EmployeeGroup" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
        function Search() {
            if (document.getElementById('<%=txtSearch.ClientID%>').value == "") {
            alert("Search Box Can't be Empty ... ");
            document.getElementById('<%=txtSearch.ClientID%>').focus();
            return false;
        }
        return true;
    }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>

            <div id="main">
                <div class="container-fluid">
                    <div class="page-header">
                        <div class="pull-left Search_Div" id="btnSearch_Div">
                            <div class="Search_Icon">
                                <asp:TextBox ID="txtSearch" runat="server" placeholder="Search For Employee Group.."
                                    class="form-control txtSearch"></asp:TextBox>
                            </div>
                        </div>
                        <div class="pull-left">
                            <asp:Button ID="btnSearch" runat="server" class="search_Btn" OnClientClick="return Search()" OnClick="btnSearch_Click"  />
                        </div>

                        <div class="pull-right">
                            <ul class="stats">
                                <li>
                                    <asp:Button ID="btnAdd" runat="server" class="add_Btn" OnClick="btnAdd_Click"  ToolTip="Create Employee Group Details" /></li>
                                <li>
                                    <asp:Button ID="btnPdf" runat="server" class="PDF_Btn" ToolTip="Export Employee Group To Pdf" /></li>
                                <li>
                                    <asp:Button ID="btnExcel" runat="server" class="Excel_Btn" ToolTip="Export Employee Group To Excel" /></li>
                            </ul>
                        </div>
                    </div>
                    <div class="breadcrumbs">
                        <ul>
                            <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i>
                            </li>
                             <li>
                        <a href="">Payroll</a><i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="EmployeeGroup.aspx">Employee Group List</a><i class="fa fa-angle-right"></i>
                    </li>
                 
                            <li>
                                <asp:LinkButton Text="Employee Group" ID="lkbAddHeader" runat="server" OnClick="lkbAddGroup_Click" ></asp:LinkButton>
                                <%--<a href="AddUserRegistration.aspx">User Registration</a>--%> </li>
                        </ul>
                        <div class="close-bread">
                            <a href="#"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                                        <div class="box-title">
                                            <h3>
                                                <i class="fa fa-th-list"></i>Employee Group List</h3>
                                        </div>
                                        <div class="box-content">
                                            <form action="#" class='form-horizontal form-bordered'>
                                                <div class="col-sm-12" style="margin-bottom: 0.3%;">
                                                    <div class="row">
                                                       <asp:GridView ID="grdGroupList" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                                            Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%;" runat="server"
                                                            AutoGenerateColumns="false"  EmptyDataText="No Records Found"
                                                            ShowFooter="false" AllowPaging="true" DataKeyNames="Group_Code"
                                                            PageSize="10" AllowSorting="true"
                                                             OnRowCommand="grdGroupList_RowCommand"
                                                            OnSorting="grdGroupList_Sorting"
                                                            OnPageIndexChanging="grdGroupList_PageIndexChanging"
                                                            OnRowDeleting="grdGroupList_RowDeleting"
                                                            OnRowEditing="grdGroupList_RowEditing">
                                                            <AlternatingRowStyle BackColor="#EEEEEE" />
                                                            <PagerStyle BackColor="#EEEEEE" ForeColor="Black" HorizontalAlign="Center" />
                                                            <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                            <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                            <SortedDescendingHeaderStyle BackColor="#000065" />
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="S.No" SortExpression="">
                                                                    <ItemTemplate>
                                                                        <%#Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Group Code">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblGroupCode" runat="server" Text='<%#Eval("Group_Code") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Group Name">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblGroupName" runat="server" Text='<%#Eval("Group_Name") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Short Name">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblShortName" runat="server" Text='<%#Eval("Short_Name") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Applicable Heads">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblApplicableHeads" runat="server" Text='<%#Eval("Applicable_Heads") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lbkEdit" runat="server" CommandName="Edit" Style="color: gray; text-decoration: none; float: left; background-image: url('../img/file_edit.png'); height: 30px; width: 35px; background-position: left; background-repeat: no-repeat;"
                                                                            OnClientClick="return confirm('Are you sure you want to Edit this Record ...?')" CausesValidation="false"
                                                                            CommandArgument='<%#Eval("Group_Code") %>'></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete" Style="color: gray; text-decoration: none; float: left; background-image: url('../img/delete.png'); height: 30px; width: 35px; background-position: left; background-repeat: no-repeat;"
                                                                            CausesValidation="false"  CommandArgument='<%#Eval("Group_Code") %>'  OnClientClick="return confirm('Are you sure you want to Remove this Record ...?')"
                                                                           ></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>

                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
