﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true"
    CodeBehind="EmployeeList.aspx.cs" Inherits="HRMS.Employee.EmployeeList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" lang="javascript">
        function Search() {
            if (document.getElementById('<%=txtSearch.ClientID%>').value == "") {
                alert("Search Box Can't be Empty ... ");
                document.getElementById('<%=txtSearch.ClientID%>').focus();
                return false;
            }
            return true;
        }
   
    </script>
   
    <style>
        .lAEmp_Code
        {
            width: 20%;
            margin-left: -6%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <div id="main">
                <div class="container-fluid">
                    <div class="page-header">
                        <div class="pull-left Search_Div" id="btnSearch_Div">
                            <div class="Search_Icon">
                                <asp:TextBox ID="txtSearch" runat="server" placeholder="Search For Employee Details..."
                                    class="form-control txtSearch"></asp:TextBox>
                            </div>
                        </div>
                        <div class="pull-left">
                            <asp:Button ID="btnSearch" OnClientClick="return Search()" runat="server" class="search_Btn"
                                OnClick="btnSearch_Click" />
                        </div>
                        <div class="pull-right">
                            <ul class="stats">
                                <li>
                                    <asp:Button ID="btnAdd" runat="server" class="add_Btn" OnClick="btnAdd_Click" ToolTip="Create Employee Details" /></li>
                                <li>
                                    <asp:Button ID="btnPdf" runat="server" class="PDF_Btn" ToolTip="Export Employee Details To Pdf" /></li>
                                <li>
                                    <asp:Button ID="btnExcel" runat="server" class="Excel_Btn" ToolTip="Export Employee Details To Excel" /></li>
                            </ul>
                        </div>
                    </div>
                    <div class="breadcrumbs">
                        <ul>
                            <li><a href="../Dashboard/Dashboard.aspx">
                                <%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i></li>
                            <li><a href="">Payroll</a><i class="fa fa-angle-right"></i></li>
                            <li><a href="EmployeeList.aspx">Employee List</a> <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <asp:LinkButton Text="Add Employee" ID="lkbAddEmployee" runat="server" OnClick="lkbAddEmployee_Click"></asp:LinkButton>
                                <%--<a href="AddUserRegistration.aspx">User Registration</a>--%>
                            </li>
                        </ul>
                        <div class="close-bread">
                            <a href="#"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                                        <div class="box-title">
                                            <h3>
                                                <i class="fa fa-th-list"></i>Employee List</h3>
                                        </div>
                                        <div class="box-content">
                                            <form action="#" class='form-horizontal form-bordered'>
                                            <div class="col-sm-12" style="margin-bottom: 0.3%;">
                                                <div class="row">
                                                    <div style="margin-bottom: 0.3%; height: 300px; overflow-y: scroll;">
                                                        <asp:GridView ID="grdEmpList" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                                            Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%;" runat="server"
                                                            AutoGenerateColumns="false" DataKeyNames="Emp_Code" EmptyDataText="No Records Found"
                                                            ShowFooter="false" AllowPaging="true" PageSize="10" AllowSorting="true" OnRowCommand="grdHeaderList_RowCommand"
                                                            OnSorting="grdHeaderList_Sorting" OnPageIndexChanging="grdHeaderList_PageIndexChanging"
                                                            OnRowDeleting="grdHeaderList_RowDeleting" OnRowEditing="grdHeaderList_RowEditing">
                                                            <AlternatingRowStyle BackColor="#EEEEEE" />
                                                            <PagerStyle BackColor="#EEEEEE" ForeColor="Black" HorizontalAlign="Center" />
                                                            <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                            <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                            <SortedDescendingHeaderStyle BackColor="#000065" />
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="S.No" SortExpression="">
                                                                    <ItemTemplate>
                                                                        <%#Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Emp Code">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblEmpCode" runat="server" Text='<%#Eval("Emp_Code") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Emp Name">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblEmpName" runat="server" Text='<%#Eval("Name") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Date Of Join">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDoj" runat="server" Text='<%#GetDate(Eval("Date_Of_Join")) %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Department">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDepartment" runat="server" Text='<%#Eval("Department") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Emp Group">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblEmpGroup" runat="server" Text='<%#Eval("Employee_Grade") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Job Title">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblJobTitle" runat="server" Text='<%#Eval("Designation") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Gender">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblGender" runat="server" Text='<%#Eval("Gender") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Emp Status">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Emp_Status") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lbkEdit" runat="server" CommandName="Edit" Style="color: gray;
                                                                            text-decoration: none; float: left; background-image: url('../img/file_edit.png');
                                                                            height: 30px; width: 35px; background-position: left; background-repeat: no-repeat;"
                                                                            OnClientClick="return confirm('Are you sure you want to Edit this Record ...?')"
                                                                            CausesValidation="false" CommandArgument='<%#Eval("Emp_Code") %>'></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete" Style="color: gray;
                                                                            text-decoration: none; float: left; background-image: url('../img/delete.png');
                                                                            height: 30px; width: 35px; background-position: left; background-repeat: no-repeat;"
                                                                            OnClientClick="return confirm('Are you sure you want to Remove this Record ...?')"
                                                                            CausesValidation="false" CommandArgument='<%#Eval("Emp_Code") %>'></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12" style="display:none;">
                                                <div class="col-sm-12" style="">
                                                    <div class="form-group">
                                                        <label for="textfield" class="control-label col-sm-1 lAEmp_Code" style="">
                                                            Total Employees Count :</label>
                                                        <div class="col-sm-2 tAEmp_Code" style="">
                                                           <%-- <asp:Label ID="lblEmpCount" runat="server" class="control-label"></asp:Label>--%>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="textfield" class="control-label col-sm-1 lAEmp_Code" style="">
                                                            Total Male Employees Count :</label>
                                                        <div class="col-sm-2 tAEmp_Code" style="">
                                                            <%--<asp:Label ID="lblMaleCount" runat="server" class="control-label"></asp:Label>--%>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="textfield" class="control-label col-sm-1 lAEmp_Code" style="">
                                                            Total Female Employees Count :</label>
                                                        <div class="col-sm-2 tAEmp_Code" style="">
                                                            <%--<asp:Label ID="lblFemaleCount" runat="server" class="control-label"></asp:Label>--%>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12" style=" display:none;">
                                                    <div class="form-group">
                                                        <label for="textfield" class="control-label col-sm-1 lAEmp_Code" style="">
                                                            Ratio of Male & Female :
                                                        </label>
                                                        <div class="col-sm-2 tAEmp_Code" style="">
                                                            <asp:Label ID="lblRationMF" runat="server" class="control-label"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="textfield" class="control-label col-sm-1 lAEmp_Code" style="">
                                                            Total new Employees Join :
                                                        </label>
                                                        <div class="col-sm-2 tAEmp_Code" style="">
                                                           <%-- <asp:Label ID="lblJoin" runat="server" class="control-label"></asp:Label>--%>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="textfield" class="control-label col-sm-1 lAEmp_Code" style="">
                                                            Total Regine Employees :
                                                        </label>
                                                        <div class="col-sm-2 tAEmp_Code" style="">
                                                            <%--<asp:Label ID="lblResigned" runat="server" class="control-label"></asp:Label>--%>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12" style="">
                                                    <div class="form-group">
                                                        <label for="textfield" class="control-label col-sm-1 lAEmp_Code" style="">
                                                            Rate of attrition :
                                                        </label>
                                                        <div class="col-sm-2 tAEmp_Code" style="">
                                                            <%--<asp:Label ID="lblRate" runat="server" class="control-label"></asp:Label>--%>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                    <div class="col-sm-12">
                        <div class="col-lg-6" style="margin-top: -1.4%;">
                            <div class="box box-color box-bordered">
                                <div class="box-title">
                                    <h3>
                                        <i class="fa fa-bar-chart-o"></i>Employee Ratio
                                    </h3>
                                    <div class="actions">
                                        <a href="#" class="btn btn-mini content-refresh"><i class="fa fa-refresh"></i></a>
                                        <a href="#" class="btn btn-mini content-remove"><i class="fa fa-times"></i></a><a
                                            href="#" class="btn btn-mini content-slideUp"><i class="fa fa-angle-down"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="box-content">
                                    <div class="statistic-big">
                                        <div class="top">
                                            <div class="left">
                                            </div>
                                        </div>
                                        <div class="bottom">
                                            <%-- <div id="visualization" style="width: 600px; height: 350px;">
                                            </div>--%>
                                            <%-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js" type="text/javascript"></script>--%>
    <script type="text/javascript" src="//www.google.com/jsapi"></script>
                                            <div>
                                                <asp:Literal ID="ltScripts" runat="server"></asp:Literal>
                                            </div>
                                             <div id="chart_div">
                                            </div>
                                            <%--<div id="piechart_3d" style="">
                                            </div>--%>
                                            <asp:Label Text="" ID="txtTotalSale" runat="server" Style="float: right; color: #000; font-size: 14px; font-weight: bold;"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-bell fa-fw"></i>Notifications Panel For Employee Details
                                </div>
                                <div class="panel-body">
                                    <div class="list-group">
                                       
                                        <a href="#" class="list-group-item">
                                            <i class="fa fa-database fa-fw"></i> Total Employees Count<span class="pull-right text-muted small"><em>
                                                      <asp:Label ID="lblEmpCount" runat="server" class="control-label"></asp:Label>
                                                </em></span></a>
                                         <a href="#" class="list-group-item">
                                            <i class="fa fa-male" aria-hidden="true"></i> Total Male Employees Count <span class="pull-right text-muted small"><em>
                                                   <asp:Label ID="lblMaleCount" runat="server" class="control-label"></asp:Label>
                                                </em></span></a>
                                         <a href="#" class="list-group-item">
                                            <i class="fa fa-female" aria-hidden="true"></i> Total Female Employees Count <span class="pull-right text-muted small"><em>
                                                   <asp:Label ID="lblFemaleCount" runat="server" class="control-label"></asp:Label>
                                                </em></span></a>
                                         <a href="#" class="list-group-item">
                                            <i class="fa fa-clone" aria-hidden="true"></i> Total new Employees Join <span class="pull-right text-muted small"><em>
                                                    <asp:Label ID="lblJoin" runat="server" class="control-label"></asp:Label>
                                                </em></span></a>

                                        <a href="#" class="list-group-item">
                                            <i class="fa fa-pencil-square-o fa-fw"></i> Total Regine Employees  <span class="pull-right text-muted small"><em>
                                                   <asp:Label ID="lblResigned" runat="server" class="control-label"></asp:Label>
                                                </em></span></a>

                                        <a href="#" class="list-group-item">
                                            <i class="fa fa-sticky-note-o" aria-hidden="true"></i> Rate of attrition <span class="pull-right text-muted small"><em>
                                                   <asp:Label ID="lblRate" runat="server" class="control-label"></asp:Label>
                                                </em></span></a>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
