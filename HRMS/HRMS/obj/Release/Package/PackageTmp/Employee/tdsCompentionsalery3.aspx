﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="tdsCompentionsalery3.aspx.cs" Inherits="HRMS.Employee.tdsCompentionsalery3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
     <asp:Panel ID="Panel1" runat="server">
    <div>
        <table>
            <tr>
                <td><strong>Net Income</strong></td>
                <td>
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Tax<asp:Button ID="Button3" runat="server" Text="..." /></td>
                <td>
                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Add : Surcharge</td>
                <td>
                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Total : Tax with Surcharge and Edeu. cess</td>
                <td>
                    <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Less : Relief u/s 89(1)</td>
                <td>
                    <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <strong>Net Tax Liability</strong>
                </td>
                <td>
                    <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    Tax deducted by others
                </td>
                <td>
                    <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox><asp:Button ID="Button2" runat="server" Text="..." /></td>
            </tr>
            <tr>
                <td>Tax to be deducted by us</td>
                <td>
                    <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td><strong>Monthly tax deducted by us</strong></td>
                <td>
                    <asp:TextBox ID="TextBox10" runat="server"></asp:TextBox>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>April</td>
                <td>
                    <asp:TextBox ID="TextBox11" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>May</td>
                <td>
                    <asp:TextBox ID="TextBox12" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>June</td>
                <td>
                    <asp:TextBox ID="TextBox13" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>July</td>
                <td>
                    <asp:TextBox ID="TextBox14" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>August</td>
                <td>
                    <asp:TextBox ID="TextBox15" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>September</td>
                <td>
                    <asp:TextBox ID="TextBox16" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>October</td>
                <td>
                    <asp:TextBox ID="TextBox17" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>November</td>
                <td>
                    <asp:TextBox ID="TextBox18" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>December</td>
                <td>
                    <asp:TextBox ID="TextBox19" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>January</td>
                <td>
                    <asp:TextBox ID="TextBox20" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>February</td>
                <td>
                    <asp:TextBox ID="TextBox21" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>March</td>
                <td>
                    <asp:TextBox ID="TextBox22" runat="server"></asp:TextBox></td>
            </tr>

        </table>

        <table>
            <tr>
                <td><strong>Balance Tax to be deducted</strong><asp:TextBox ID="TextBox23" runat="server"></asp:TextBox><asp:Button ID="Button1" runat="server" Text="..." OnClick="Button1_Click" /></td>
            </tr>
        </table>
    </div>
         </asp:Panel>
     <asp:Panel ID="Panel2" runat="server">
         <h4>
             Balance Tax -Yet to be Deducted By Us
         </h4>
          <table>
                <tr>
                    <td>
                        April
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        May
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox24" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        June
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox25" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        July
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox26" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        August
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox27" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        september
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox28" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        October
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox29" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        November
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox30" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        December
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox31" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        January
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox32" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        February
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox33" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        March
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox34" runat="server"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td>
                      Total :
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox35" runat="server"></asp:TextBox>
                    </td>
                </tr>
              <tr>
                  <td colspan="2"align="center">
                      <asp:Button ID="Button4" runat="server" Text="Close" OnClick="Button4_Click" />
                  </td>
              </tr>
            </table>
     </asp:Panel>
    </div>
</asp:Content>
