﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="tdsreports.aspx.cs" Inherits="HRMS.Employee.tdsreports" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <asp:Panel ID="Panel1" runat="server">
        <table>
            <tr>
                <td>
                    Choose Head:
                </td>
                <td colspan="2">
                    <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" Height="122px" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" Width="175px">
                        <asp:ListItem>--select--</asp:ListItem>
                        <asp:ListItem>Rent</asp:ListItem>
                        <asp:ListItem>Salary</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Type of Reports Available :
                </td>
            </tr>
            </table>
            <br />
            <br />
            </asp:Panel>
        <asp:Panel ID="Panel2" runat="server">
    <%-- <table>
            <tr>
                <td>
                    <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" >
<p>
    Show general Details of Deductee(Without Residential Address)
</p><br />
                        <p>

                        </p>

                    </asp:TextBox>
                </td>
            </tr>
        </table>--%>
            <p>
                 Show general Details of Deductee(Without Residential Address)
            </p><br />
            <p>
                Show general Details of Deductee(With Residential Address)
            </p><br />
            <p>
                Show TDS Deducted(Only Summary)
            </p><br />
            <p>Show TDS Deducted(Monthly Details)</p><br />
            <p>
                TDS DEposited
            </p><br />
            <p>
                Challan Wise Deposit Break-up
            </p><br />
            <p>
                Tax Yet To be Deducted
            </p><br />
            <p>
                Refundable
            </p>
            <br />
            <p>
                <asp:Button ID="Button2" runat="server" Text="Show Report" /><asp:Button ID="Button1" runat="server" Text="Close" />
            </p>
            </asp:Panel>
        <asp:Panel ID="Panel3" runat="server">
            <p>
                Show General Details Of Deductees
            </p><br />
            <p>
                Show Payments Made With Tds Deducted(In order Of Date Payment)
            </p><br />
            <p>
                Show Payments Made With Tds Deducted(grouped by deductee)
            </p><br />
            <p>
                Show TDs deposited(Grouped by deductee)
            </p><br />
            <p>
                Show Challlan Wise Deposited Break-Up
            </p><br />
            <p>
                Group Wise TDs Deducted
            </p><br />
            <p>
                TDS Deduction Details
            </p><br />
            <p>
                TDS Deduction Summary
            </p><br />
            <p>
                Show General Details OF Deductees(Type 2)
            </p><br />
            <p>
                Group Wise TDS Deducted(All Category - TYpe 2)
            </p><br />
            <p>
                PAN NUMBER BAlnk
            </p><br />
            <p>
                PAN NUMBER Blank-Letters to Party 
            </p><br />
            
            <p>
                <asp:Button ID="Button3" runat="server" Text="Show Report" /><asp:Button ID="Button4" runat="server" Text="Close" />
            </p>
        </asp:Panel>
    </div>
</asp:Content>
