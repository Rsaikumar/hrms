﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="SuspensionandPunishment.aspx.cs" Inherits="HRMS.Employee.SuspensionandPunishment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function Validate() {
            var ddltypeofpunish = document.getElementById("<%=ddltype.ClientID %>");
            if (ddltypeofpunish.value == "0") {
                //If the "Please Select" option is selected display error.
                alert("Please select any Type!");
                return false;
            }
            return true;
        }
    </script>
    <style>
        .txtalign {
            margin: 0px -1.84375px 0px 40px !important;
            width: 785px !important;
            height: 81px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="#">Payroll</a><i class="fa fa-angle-right"></i> </li>
                        <li><a href="../Payroll/LeaveApplication.aspx" class="current">Suspension and Punishments</a> </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <div class="panel-heading form-group col-md-12">
                            <header class="col-md-6 text-left">
                                <i class="fa fa-user"> Suspension and Punishments</i>
                            </header>
                            <header class="col-md-6 form-group text-right">
                                <asp:LinkButton ID="LinkButton1" OnClick="lnkbackbtn_Click" runat="server"><i class="fa fa-arrow-left"> <b>Back</b></i></asp:LinkButton>
                            </header>
                        </div>
                        <section class="panel-body">
                            <div class="form-group col-md-6" visible="false">
                                <label for="textfield" runat="server" id="lblempcode" class="control-label col-sm-5" style="">
                                    Employee Name
                                </label>
                                <div class="col-sm-7" style="">
                                    <asp:TextBox ID="txtempname" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group col-md-6" visible="false">
                                <label for="textfield" runat="server" id="Label1" class="control-label col-sm-5" style="">
                                    Designation
                                </label>
                                <div class="col-sm-7" style="">
                                    <asp:TextBox ID="txtdesignation" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="textfield" runat="server" id="Label2" class="control-label col-sm-5" style="">
                                    Type
                                </label>
                                <div class="col-sm-7" style="">
                                    <asp:DropDownList ID="ddltype" runat="server" class="form-control">
                                        <asp:ListItem Value="0">--Select--</asp:ListItem>
                                        <asp:ListItem Value="1">Suspension</asp:ListItem>
                                        <asp:ListItem Value="2">Punishment</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-12" visible="false">
                                <label for="textfield" runat="server" class="control-label col-sm-2" style="">
                                    Cause
                                </label>
                                <div class="col-sm-10" style="">
                                    <asp:TextBox ID="txtcause" runat="server" TextMode="MultiLine" class="form-control txtalign"></asp:TextBox>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-12" visible="false">
                                <label for="textfield" runat="server" class="control-label col-sm-2" style="">
                                    Section
                                </label>
                                <div class="col-sm-10" style="">
                                    <asp:TextBox ID="txtsection" runat="server" TextMode="MultiLine" class="form-control txtalign"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="textfield" runat="server" id="Label3" class="control-label col-sm-5" style="">
                                    Duration
                                </label>
                                <div class="col-sm-7" style="">
                                    <asp:TextBox ID="txtduration" runat="server" placeholder="No Of Days" class="form-control" OnTextChanged="txtduration_TextChanged" AutoPostBack="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="textfield" runat="server" id="Label4" class="control-label col-sm-5" style="">
                                    Rejoining Date
                                </label>
                                <div class="col-sm-7" style="">
                                    <asp:TextBox ID="txtrejoindate" runat="server" placeholder="Date Of Rejoining" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-12 text-center">
                                <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary" OnClientClick="return Validate()" Text="Submit" OnClick="btnsubmit_Click" />
                            </div>
                        </section>
                    </section>
                </div>
            </div>
        </section>
    </section>


</asp:Content>
