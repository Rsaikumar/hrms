﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="tdscomputationsalery.aspx.cs" Inherits="HRMS.Employee.tdscomputationsalery" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="col-md-12">
        <div class="col-md-6">
    <div>
        <h4>
            Editing a Deductee under head 'SALARY'
        </h4>
        <table>
            <tr>
                <td>
                </td>
                <td></td>
                <td>(first_Name)</td>
                <td>(Middle_Name)</td>
                <td>(Last_Name)</td>
            </tr>
            <tr>
                <td>
                    Deductee Name :</td>
                    <td> <asp:DropDownList ID="DropDownList1" runat="server">
                        <asp:ListItem>Mr.</asp:ListItem>
                        <asp:ListItem>Miss</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    Date Of Birth :
                </td>
                <td colspan="4">
                    <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Gender:</td>
                <td colspan="4">
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem>Male</asp:ListItem>
                        <asp:ListItem>Female</asp:ListItem>
                    </asp:RadioButtonList></td>
            </tr>
            <tr>
                <td>Father Name:</td>
                <td colspan="4">
                    <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>PAN :</td>
                <td colspan="4">
                    <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox><asp:CheckBox ID="CheckBox1" runat="server" Text="APPLIED" /><asp:Button ID="Button1" runat="server" Text="Verify PAN" /></td>
            </tr>
            <tr>
                <td>
                    Occupation : </td><td colspan="4"> <asp:DropDownList ID="DropDownList4" runat="server"></asp:DropDownList>
                </td>
            </tr>
           
            <tr>
                <td>Date Of Joining :
                </td>
                <td colspan="4">
                    <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Designation :</td>
                <td colspan="4">
                    <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Retirement Date :</td>
                <td colspan="4">
                    <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
               </td>
            </tr>
            <tr>
                <td>Status</td>
                <td colspan="4">
                    <asp:DropDownList ID="DropDownList2" runat="server">
                        <asp:ListItem>Resident</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            
        </table>
        <p>
            <asp:CheckBox ID="CheckBox2" runat="server" />Is Director in the company or has substantial voting right in the Company?</p>
        <p>
            <asp:CheckBox ID="CheckBox3" runat="server" /> Is eligible for deduction u/s 80U(severe disability)</p>
        <p>
            <asp:CheckBox ID="CheckBox4" runat="server" /> Is eligible for deduction u/s 80U(for physically handicapped)</p>
        <p>
            <asp:CheckBox ID="CheckBox5" runat="server" />Is Deductee an Indian Citizen ?</p>
        <p>
            <asp:CheckBox ID="CheckBox6" runat="server" />Is surcharge Applicable ?</p>
    </div>
             </div>
             <div class="col-md-6">
        <h4>
            Address:
        </h4>
        <table>
            <tr>
                <td>Flat/Door/Block.no : </td> <td><asp:TextBox ID="TextBox12" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    Premise/Bldg./Village :</td> <td> <asp:TextBox ID="TextBox13" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Road/Street/Lane/P.O :</td> <td> <asp:TextBox ID="TextBox14" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Area/Location/Taluka :</td>
                <td> <asp:TextBox ID="TextBox10" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    Town/City/District :</td> <td> <asp:TextBox ID="TextBox15" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    State/U.T. :</td> <td> <asp:TextBox ID="TextBox16" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Country :</td> <td> <asp:TextBox ID="TextBox17" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Pin Code :</td>
                <td>
                    <asp:TextBox ID="TextBox21" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
            <td>
                Notes :
            </td>
            <td>
                <asp:TextBox ID="TextBox11" runat="server"></asp:TextBox></td>
        </tr>
            <tr>
                <td>
                    Phone :
                </td>
                <td>
                    <asp:TextBox ID="TextBox18" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                Email Address :</td>
                <td>
                    <asp:TextBox ID="TextBox19" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    Website :
                </td>
                <td>
                    <asp:TextBox ID="TextBox20" runat="server"></asp:TextBox></td>
            </tr>
                </table>

        <p><strong>Keep this deductee in this group :</strong> <asp:DropDownList ID="DropDownList3" runat="server"></asp:DropDownList></p>

        <div>
            <asp:Button ID="Button3" runat="server" Text="Accept" />
            <asp:Button ID="Button2" runat="server" Text="Cancel" /></div>
    </div>
      </div>     
           
    </div>
</asp:Content>
