﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="EmployeeAttendanceReport.aspx.cs" Inherits="HRMS.Employee.EmployeeAttendanceReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="#">Attendance</a><i class="fa fa-angle-right"></i> </li>
                        <li><a href="Attendance.aspx">Attendance List</a> </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <div class="panel-body">
                            <div class="tab-content">
                                <div id="loanapplication" class="tab-pane active">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <section class="panel">
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-4" style="">
                                                            Select Date<span id="Span4" runat="server" style="color: Red; display: inline;">*</span></label>
                                                        <div class="col-sm-7" style="" id="Div2">
                                                            <asp:TextBox ID="txtdate" runat="server" placeholder="Ex:dd-mm-yyyy" required class="form-control"
                                                                MaxLength="20" AutoPostBack="true"></asp:TextBox>
                                                            <cc1:CalendarExtender ID="actdate" PopupButtonID="imgPopup" runat="server" TargetControlID="txtdate"
                                                                Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                            </cc1:CalendarExtender>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 form-group">
                                                        <div class="col-md-4">
                                                            <asp:Label ID="Label3" runat="server" Text="Label">Name</asp:Label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <asp:DropDownList ID="ddlempname" runat="server" class="form-control"
                                                                AutoPostBack="true" required>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 form-group" id="reporting" runat="server">
                                                        <div class="col-md-4">
                                                            <asp:Label ID="Label2" runat="server" Text="Label">Reporting</asp:Label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <asp:TextBox ID="txtreport" runat="server" placeholder="Reporting Authority" class="form-control"
                                                                MaxLength="20" AutoPostBack="true"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 form-group" id="dateofjoin" runat="server">
                                                        <div class="col-md-4">
                                                            <asp:Label ID="Label1" runat="server" Text="Label">Date of joining</asp:Label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <asp:TextBox ID="txtdateofjoing" runat="server" placeholder="Date of joining " class="form-control"
                                                                MaxLength="20" AutoPostBack="true"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="form-group  text-center col-md-12">
                                                        <asp:Button ID="btngetdata" CssClass="btn btn-primary" runat="server" Text="Search" OnClick="btngetdata_Click" />
                                                        <asp:Button CssClass="btn btn-primary" ID="btnBacktoDashboard" Text="Back" runat="server" OnClick="btnBacktoDashboard_Click" />

                                                    </div>
                                                    <div class="col-md-12 ">
                                                        <div style="max-height: 515px; overflow: auto" class="table-responsive">
                                                            <asp:GridView ID="gvdattendance" CssClass="mydatagrid" HeaderStyle-CssClass="headergrid" RowStyle-CssClass="rows"
                                                                runat="server" Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%; overflow: scroll;" EmptyDataText="No records Found" AutoGenerateColumns="false">
                                                                <Columns>
                                                                    <asp:BoundField DataField="EmpId" HeaderText="Id" ItemStyle-Width="200" />
                                                                    <asp:BoundField DataField="Name" HeaderText="Employee Name" ItemStyle-Width="200" />
                                                                    <asp:BoundField DataField="Designation" HeaderText="Designation" ItemStyle-Width="200" />
                                                                    <asp:BoundField DataField="CreatedOn" HeaderText="Date" ItemStyle-Width="200" DataFormatString="{0:dd-MM-yyyy}" />
                                                                    <asp:BoundField DataField="Status" HeaderText="Status" ItemStyle-Width="200" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 form-group" id="status" runat="server">
                                                        <div class="col-md-4">
                                                            <asp:Label ID="Label4" runat="server" Text="Label">Leave Status </asp:Label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <asp:TextBox ID="txtstatus" runat="server" placeholder="Leave Status " class="form-control"
                                                                MaxLength="20" AutoPostBack="true"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 text-center">
                                                        <asp:Button CssClass="btn btn-primary" ID="btnback" Text="Back" runat="server" OnClick="btnback_Click" OnClientClick="return ValidationForDropDowns()" />
                                                    </div>
                                                </section>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>


        </section>

    </section>
</asp:Content>
