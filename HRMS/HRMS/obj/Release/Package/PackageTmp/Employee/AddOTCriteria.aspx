﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="AddOTCriteria.aspx.cs" Inherits="HRMS.Employee.AddOTCriteria" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <link href="../css/Employee/AddOTCriteria.css" rel="stylesheet" />
    <script type="text/javascript">
        function grvOTRate_Validations() {
           var TargetBaseControl = document.getElementById('<%=this.grvOTRate.ClientID%>');
            var TargetChildControl1 = "txtCriteria";
            var TargetChildControl2 = "txtOTRate";
          
           var Inputs = TargetBaseControl.getElementsByTagName("input");

           for (var n = 0; n < Inputs.length; ++n) {
               if (Inputs[n].type == 'text' && Inputs[n].id.indexOf(TargetChildControl1, 0) >= 0) {
                   if (Inputs[n].value == "") {
                       alert('Enter Criteria..'); Inputs[n].focus();
                       return false;
                   }
               }
           }
           for (var n = 0; n < Inputs.length; ++n) {
               if (Inputs[n].type == 'text' && Inputs[n].id.indexOf(TargetChildControl2, 0) >= 0) {
                   if (Inputs[n].value == "") {
                       alert('Enter OT Rate..'); Inputs[n].focus();
                       return false;
                   }
               }
           }
           return true;
       };

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
          <div id="main">
        <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">Payroll</a><i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="OTCriteria.aspx">OT Criteria List</a><i class="fa fa-angle-right"></i>
                    </li>
                    <li><a href="AddOTCriteria.aspx">OT Criteria</a> </li>
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                                <div class="box-title">
                                    <h3>
                                        <i class="fa fa-th-list"></i>OT Criteria</h3>
                                </div>
                                <div class="box-content">
                                    <form action="#" method="POST" class='form-horizontal form-bordered'>

                                           <div class="col-sm-6" style="display:block;">
                                        <asp:Panel ID="Panel1" runat="server" GroupingText="OT Calculations" Style="border-bottom: 0px solid #8F8F8F;">
                                            <div class="col-sm-12" style="margin-bottom: 0.3%;">
                                                <div class="row">
                                                        <asp:GridView ID="grvOTRate" class="OtCriteria table table-hover table-nomargin table-bordered"
                                                        Style="margin-bottom: 0.83%; margin-top: 0.83%;" runat="server"
                                                           ShowFooter="true" AutoGenerateColumns="False">
                                                        <AlternatingRowStyle BackColor="#EEEEEE" />
                                                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                        <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                        <SortedDescendingHeaderStyle BackColor="#000065" />
                                                        <Columns>
                                                             <asp:TemplateField HeaderText="Criteria">
                                                                                <ItemTemplate>
                                                                                    <asp:TextBox ID="txtCriteria" runat="server" placeholder="Criteria" class="form-control"
                                                                                        MaxLength="10" Text='<%#Eval("Ot_Criteria")%>'></asp:TextBox>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="OT Rate">
                                                                <ItemTemplate>
                                                                      
                                                                        <asp:TextBox ID="txtOTRate" runat="server" palceholder="OT Rate" Text='<%#Eval("Ot_Rate")%>' CssClass="form-control"
                                                                            ></asp:TextBox>
                                                                    
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lbRemove" runat="server" OnClick="lbRemove_Click" >Remove</asp:LinkButton>
                                                                </ItemTemplate>
                                                                <FooterStyle HorizontalAlign="Right" />
                                                                <FooterTemplate>
                                                                    <asp:Button ID="btnAddNewRecord" runat="server" Text="Record" class="btn btn-primary"
                                                                         OnClick="btnAddNewRecord_Click" OnClientClick="return grvOTRate_Validations()"
                                                                         />
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                    </div>
                                                </div>
                                                    </asp:Panel>
                                               </div>
                                        <div class="col-sm-6" style="display:block;">
                                                                                <div class="col-sm-12" style="">
                                             <div class="form-group ">
                                            <label for="textfield" class="control-label col-sm-1 lblAUCompanyID" style="">
                                           Select Heads On Which Of Amount Is Calculated Along With Basic<span style="color: #ff0000; font-size: 14px; display:none;">*</span>
                                            </label>
                                            <div class="col-sm-8 tAUCompanyID " style="">
                                                <asp:CheckBoxList ID="chlOTLists" runat="server" class="ListControl" OnSelectedIndexChanged="chlOTLists_SelectedIndexChanged"
                                                     RepeatDirection="vertical" RepeatLayout="Table" RepeatColumns="2" AutoPostBack="true"
                                                    Width="100%" >
                                                
                                                </asp:CheckBoxList>
                                            </div>

                                      </div>
                                       
                                           </div>
                                        <div class="col-sm-12">

                                             <div class="form-group">
                                                <label for="textfield" class="control-label col-sm-1 lAEventName" style="" id="">
                                                   </label>
                                                <div class="col-sm-9 tACal" style="" id="">
                                                    <asp:TextBox ID="txtOTCal" runat="server" ReadOnly="true" placeholder="" class="form-control"
                                                        MaxLength="25"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                                </div>
                                        <div class="col-sm-12">                                           
                                             <div class="col-sm-3 ACButtons">
                                            <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary" OnClick="btnSave_Click" OnClientClick="return grvOTRate_Validations()" />
                                            <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-primary" OnClick="btnUpdate_Click" />
                                            <asp:Button ID="btnCancel" runat="server" Text="Reset" class="btn" OnClick="btnCancel_Click" />

                                        </div>
                                        </div>

                                       

                               
                                       
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    

                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
