﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="Retunsform24Q.aspx.cs" Inherits="HRMS.Employee.Retunsform24Q" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
    <div >
        <h4>Regular Quarterly Return: Form 27EQ(TCS)</h4>
        <table>
            <tr>
                <td>Choose period for return filing</td>
                <td>
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                </td>
                <td>File Creation Date</td>
                <td>
                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Name of the Deducter</td>
                <td>
                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox></td>
            <td>Tax Deduction Account Number *</td>
                <td>
                    <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox></td>
            </tr>
            <tr><td>
                TDS Circle
                </td>
                <td>
                    <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                </td>
                <td>Permanent Account Number</td>
                <td>
                    <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                </td>
            </tr>
        </table>
        <div class="col-md-6">
            <h4>Particulars of the deductor</h4>
        <table>
            <tr>
                <td>
                    Branch/Division
                </td>
                <td>
                    <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Address 1*
                </td>
                <td>
                    <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Address2
                </td>
                <td>
                    <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox>]
                </td>
            </tr>
            <tr>
                <td>
                    Address3
                </td>
                <td>
                    <asp:TextBox ID="TextBox10" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Address4
                </td>
                <td>
                    <asp:TextBox ID="TextBox11" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Address5
                </td>
                <td>
                    <asp:TextBox ID="TextBox12" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    StateCode*
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList1" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    PinCode
                </td>
                <td>
                    <asp:TextBox ID="TextBox13" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Email Id
                </td>
                <td>
                    <asp:TextBox ID="TextBox14" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    STD Code
                </td>
                <td>
                    <asp:TextBox ID="TextBox15" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Telephone
                </td>
                <td>
                    <asp:TextBox ID="TextBox16" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Status Of deducter*
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList2" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Deductor Type :
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList3" runat="server"></asp:DropDownList>
                </td>
            </tr>
        </table>
            <p> Change of Address of Deducter Scince Last return  <asp:CheckBox ID="CheckBox1" runat="server" /></p>
        </div>
          <div class="col-lg-6">
            <h4>Particulars of the Person responsible for deduction of tax</h4>
        <table>
            <tr>
                <td>
                    Name*
                </td>
                <td>
                    <asp:TextBox ID="TextBox17" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Designation*</td>
                <td>
                    <asp:TextBox ID="TextBox27" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    Address 1*
                </td>
                <td>
                    <asp:TextBox ID="TextBox18" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Address2
                </td>
                <td>
                    <asp:TextBox ID="TextBox19" runat="server"></asp:TextBox>]
                </td>
            </tr>
            <tr>
                <td>
                    Address3
                </td>
                <td>
                    <asp:TextBox ID="TextBox20" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Address4
                </td>
                <td>
                    <asp:TextBox ID="TextBox21" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Address5
                </td>
                <td>
                    <asp:TextBox ID="TextBox22" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    StateCode*
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList4" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    PinCode
                </td>
                <td>
                    <asp:TextBox ID="TextBox23" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Email Id
                </td>
                <td>
                    <asp:TextBox ID="TextBox24" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    STD Code
                </td>
                <td>
                    <asp:TextBox ID="TextBox25" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Telephone
                </td>
                <td>
                    <asp:TextBox ID="TextBox26" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Mobile No.
                </td>
                <td>
                    <asp:TextBox ID="TextBox28" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Place
                </td>
                <td>
                    <asp:TextBox ID="TextBox29" runat="server"></asp:TextBox>
                </td>
            </tr>
        </table>
            <p> Change of Address of Responsible person since last return  <asp:CheckBox ID="CheckBox2" runat="server" /></p>
        </div>
        <div>
            <asp:Button ID="Button1" runat="server" Text="Ok" /><asp:Button ID="Button2" runat="server" Text="Cancel" /></div>
    </div>
    </div>
</asp:Content>
