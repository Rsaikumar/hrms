﻿    <%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="PaySlip.aspx.cs" Inherits="HRMS.Employee.Slip" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Assets/js/jquery-2.1.4.min.js"></script>
    <script src="../Assets/Kendo/kendo.we.min.js"></script>
    <script src="../css/Custom/templateLoader.js"></script>
    <style>
        label.margin-pm {
            margin: 10px;
        }

        td.bck-prvn {
            background-color: #cfe3f5;
        }
    </style>
    <script type="text/javascript">
        function PrintPanel() {
            var panel = document.getElementById("<%=dvtableprint.ClientID %>");
            var printWindow = window.open('', '', 'height=200,width=2000');
            printWindow.document.write('<html><head><title></title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
            return false;
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            var templates = loadTemplates('Kendotemp', ['DEMO']);
            var followData = null;
            $.ajax({
                type: "POST",
                url: "PaySlip.aspx/AnalysisReportData",
                data: [],
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (obj) {
                    var lds = obj;
                    var Getdata = lds;
                    BIndData(Getdata);
                }
            });
            function BIndData(Getdata) {
                var template = kendo.template(templates.DEMO);  // kendo.template($("#tempdemo").html());
                var data = Getdata;
                var result = template(data)

                $("#demo").html(result)
            };
        });

        
    </script>




</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../DashBoard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="#">Employee</a><i class="fa fa-angle-right"></i> </li>
                        <li><a href="../Employee/MyProfile.aspx" class="current">My Profile</a> </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <i class="fa fa-user">Employee PaySlips</i>
                        </header>
                        <div class="panel-body">
                                                 <%--     <div class="col-md-12">
                                <div class="col-md-6 form-group">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label3" runat="server" Text="Label">Name</asp:Label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:DropDownList ID="ddlempname" runat="server" class="form-control"
                                          AutoPostBack="true"   required >
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-6 form-group">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label1" runat="server" Text="Label">Month</asp:Label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:DropDownList ID="ddlmonth" runat="server" class="form-control"
                                            AutoPostBack="true">
                                            <asp:ListItem Text="Select Month" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="January" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="February" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="March" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="April" Value="4"></asp:ListItem>
                                            <asp:ListItem Text="May" Value="5"></asp:ListItem>
                                            <asp:ListItem Text="June" Value="6"></asp:ListItem>
                                            <asp:ListItem Text="July" Value="7"></asp:ListItem>
                                            <asp:ListItem Text="August" Value="8"></asp:ListItem>
                                            <asp:ListItem Text="september" Value="9"></asp:ListItem>
                                            <asp:ListItem Text="October" Value="10"></asp:ListItem>
                                            <asp:ListItem Text="November" Value="11"></asp:ListItem>
                                            <asp:ListItem Text="December" Value="12"></asp:ListItem>

                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label2" runat="server" Text="Label">Year</asp:Label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:DropDownList ID="ddlyear" runat="server" class="form-control"
                                         required   AutoPostBack="true">
                                            <asp:ListItem Text="Select Year" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="2017" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="2018" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="2019" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="2020" Value="4"></asp:ListItem>
                                            <asp:ListItem Text="2021" Value="5"></asp:ListItem>
                                            <asp:ListItem Text="2022" Value="6"></asp:ListItem>
                                            <asp:ListItem Text="2023" Value="7"></asp:ListItem>
                                            <asp:ListItem Text="2024" Value="8"></asp:ListItem>
                                            <asp:ListItem Text="2025" Value="9"></asp:ListItem>
                                            <asp:ListItem Text="2026" Value="10"></asp:ListItem>
                                            <asp:ListItem Text="2027" Value="11"></asp:ListItem>

                                        </asp:DropDownList>
                                    </div>
                                </div>
                                  <div class="col-md-12 text-center">
                                <asp:Button CssClass="btn btn-primary" ID="btnsearch" Text="Search" runat="server" OnClick="btnsearch_Click"/>
                                <asp:Button CssClass="btn btn-primary" ID="btnBacktoDashboard" Text="Back" runat="server"/>

                                      </div>
                            </div>--%>

                            <div class="col-md-12">
                               <%-- <div id="dvtables" runat="server">
                                    <table border="1" style="width: 100%; margin-top: 10px;">
                                        <tr>
                                            <td style="text-align: left; background-color: #004d94;" colspan="4">
                                                <label class="control-label margin-pm" style="color: white;">Telangana State Forest Department</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center" colspan="4">
                                                <label class="control-label">PaySlip</label>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="bck-prvn" style="width: 20%;">
                                                <label class="control-label margin-pm">Employee ID</label>
                                            </td>
                                            <td style="text-align: right; width: 30%;">
                                                <label class="control-label margin-pm" id="lblempid" runat="server"></label>
                                            </td>
                                            <td class="bck-prvn" style="width: 20%;">
                                                <label class="control-label margin-pm">Name</label>
                                            </td>
                                            <td style="text-align: right; width: 30%">
                                                <label class="control-label margin-pm" id="lblempname" runat="server"></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="bck-prvn">
                                                <label class="control-label margin-pm">Designation</label>
                                            </td>
                                            <td style="text-align: right">
                                                <label class="control-label margin-pm" id="lbldesignation" runat="server"></label>
                                            </td>
                                            <td class="bck-prvn">
                                                <label class="control-label margin-pm">Date Of Joining</label>
                                            </td>
                                            <td style="text-align: right">
                                                <label class="control-label margin-pm" id="lblempdoj" runat="server"></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="bck-prvn">
                                                <label class="control-label margin-pm">Bank Name</label>
                                            </td>
                                            <td style="text-align: right">
                                                <label class="control-label margin-pm" id="lblbankname" runat="server"></label>
                                            </td>
                                            <td class="bck-prvn">
                                                <label class="control-label margin-pm">Account NO</label>
                                            </td>
                                            <td style="text-align: right">
                                                <label class="control-label margin-pm" id="lblaccno" runat="server"></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="bck-prvn">
                                                <label class="control-label margin-pm">PAN No</label>
                                            </td>
                                            <td style="text-align: right">
                                                <label class="control-label margin-pm" id="lblpanno" runat="server"></label>
                                            </td>
                                            <td colspan="2"></td>
                                        </tr>
                                    </table>

                                    <table border="1" style="width: 100%; margin-top: 20px; margin-bottom: 25px;">
                                        <tr>
                                            <td class="bck-prvn" style="width: 20%;">
                                                <label class="control-label margin-pm">Earnings</label>
                                            </td>
                                            <td style="text-align: right; width: 30%;" class="bck-prvn">
                                                <label class="control-label margin-pm">Amount</label>
                                            </td>
                                            <td class="bck-prvn">
                                                <label class="control-label margin-pm" style="width: 20%;">Deductions</label>
                                            </td>
                                            <td style="text-align: right; width: 30%;" class="bck-prvn">
                                                <label class="control-label margin-pm">Amount</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="control-label margin-pm">Basic Pay</label>
                                            </td>
                                            <td style="text-align: right">
                                                <label class="control-label margin-pm" id="lblbasicamount" runat="server"></label>
                                            </td>
                                            <td>
                                                <label class="control-label margin-pm">TDS</label>
                                            </td>
                                            <td style="text-align: right">
                                                <label class="control-label margin-pm" id="lbltds" runat="server"></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="control-label margin-pm">HRA</label>
                                            </td>
                                            <td style="text-align: right">
                                                <label class="control-label margin-pm" id="lblhra" runat="server"></label>
                                            </td>
                                            <td>
                                                <label class="control-label margin-pm">PF</label>
                                            </td>
                                            <td style="text-align: right">
                                                <label class="control-label margin-pm" id="lblpf" runat="server"></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="control-label margin-pm">DA</label>
                                            </td>
                                            <td style="text-align: right">
                                                <label class="control-label margin-pm" id="lblda" runat="server"></label>
                                            </td>
                                            <td>
                                                <label class="control-label margin-pm">ESI</label>
                                            </td>
                                            <td style="text-align: right">
                                                <label class="control-label margin-pm" id="lblesi" runat="server"></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="control-label margin-pm">CCA</label>
                                            </td>
                                            <td style="text-align: right">
                                                <label class="control-label margin-pm" id="lblcca" runat="server"></label>
                                            </td>
                                            <td>
                                                <label class="control-label margin-pm">Professional Tax</label>
                                            </td>
                                            <td style="text-align: right">
                                                <label class="control-label margin-pm" id="lblptamt" runat="server"></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="control-label margin-pm">GROSS EARNINGS</label>
                                            </td>
                                            <td style="text-align: right">
                                                <label class="control-label margin-pm" id="lblgrossearnings" runat="server"></label>
                                            </td>
                                            <td>
                                                <label class="control-label margin-pm">GROSS DEDUCTONS</label>
                                            </td>
                                            <td style="text-align: right">
                                                <label class="control-label margin-pm" id="lblgrossdeductions" runat="server"></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"></td>
                                            <td>
                                                <label class="control-label margin-pm">NET PAYABLE</label>
                                            </td>
                                            <td style="text-align: right">
                                                <label class="control-label margin-pm" id="lblnetpayable" runat="server"></label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>--%>
                                <div id="dvtableprint" runat="server" class="col-md-12">
                                    <div id="demo"></div>
                                </div>
                                <div class="col-md-12 text-center">
                                    <asp:Button ID="btnprint" runat="server" Text="Print" OnClientClick="PrintPanel()" />
                                </div>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
        </section>
    </section>
</asp:Content>
