﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="TransferOrder.aspx.cs" Inherits="HRMS.Employee.TransferOrder" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.js"></script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>

                        <li><a href="../Employee/TransferOrder.aspx" class="current">Transfer Order</a> </li>
                    </ul>
                </div>
            </div>
        </section>
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        <i class="fa fa-user">Transfer Order</i>
                    </header>
                    <div class="panel-body">
                       
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-4" style="">
                                        Employee Name</label>
                                    <div class="col-sm-8" style="" id="Div13">
                                        <asp:DropDownList ID="ddlemployeename" runat="server" class="form-control DropdownCss" OnSelectedIndexChanged="ddlemployeename_SelectedIndexChanged1" AutoPostBack="true">
                                        </asp:DropDownList>

                                    </div>

                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-4" style="">
                                        Transfer Order Date<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                    <div class="col-sm-8" style="" id="Div66">
                                        <asp:TextBox ID="txttransferorderdate" runat="server" placeholder="Ex:MM-dd-yyyy" class="form-control"
                                            MaxLength="25" TextMode="SingleLine" required></asp:TextBox>
                                        <ajaxToolKit:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="imgPopup"
                                            TargetControlID="txttransferorderdate" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                        </ajaxToolKit:CalendarExtender>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-4" style="">
                                        Designation<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                    <div class="col-sm-8" style="" id="Div2">
                                        <asp:TextBox ID="txtdesignation" runat="server" placeholder=" Designation" class="form-control"
                                            MaxLength="25" TextMode="SingleLine"></asp:TextBox>

                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <h5 style="color: rgb(236, 7, 7);">Old Location Details</h5>
                                <hr />
                         <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-4" style="">
                                        Circle</label>
                                    <div class="col-sm-8" style="" id="Div6">
                                        <asp:TextBox ID="txtcircle" runat="server" placeholder=" Circle" class="form-control"
                                            MaxLength="25" TextMode="SingleLine"></asp:TextBox>

                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-4" style="">
                                        Divisions</label>
                                    <div class="col-sm-8" style="" id="Div7">
                                        <asp:TextBox ID="txtdivisions" runat="server" placeholder=" Divisions" class="form-control"
                                            MaxLength="25" TextMode="SingleLine"></asp:TextBox>

                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-4" style="">
                                        Range</label>
                                    <div class="col-sm-8" style="" id="Div8">
                                        <asp:TextBox ID="txtrange" runat="server" placeholder=" Range" class="form-control"
                                            MaxLength="25" TextMode="SingleLine"></asp:TextBox>

                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-4" style="">
                                        Section</label>
                                    <div class="col-sm-8" style="" id="Div9">
                                        <asp:TextBox ID="txtsection" runat="server" placeholder=" Section" class="form-control"
                                            MaxLength="25" TextMode="SingleLine"></asp:TextBox>

                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-4" style="">
                                        Beat</label>
                                    <div class="col-sm-8" style="" id="Div10">
                                        <asp:TextBox ID="txtbeat" runat="server" placeholder=" Beat" class="form-control"
                                            MaxLength="25" TextMode="SingleLine"></asp:TextBox>

                                    </div>
                                </div>
                                   </ContentTemplate>
                        </asp:UpdatePanel>

                                <div class="clearfix"></div>

                                <h5 style="color: rgb(236, 7, 7);">Transfer Location Details</h5>
                                <hr />
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-4" style="">
                                        Circle</label>
                                    <div class="col-md-8">
                                        <asp:DropDownList ID="ddlnewlocationcircle" runat="server" class="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlnewlocationcircle_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-4" style="">
                                        Divisions</label>
                                    <div class="col-sm-8" style="" id="Div1">
                                        <asp:DropDownList ID="ddlnewdivisions" runat="server" class="form-control DropdownCss" AutoPostBack="true" OnSelectedIndexChanged="ddlnewdivisions_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-4" style="">
                                        Range</label>
                                    <div class="col-sm-8" style="" id="Div3">
                                        <asp:DropDownList ID="ddlnewrange" runat="server" class="form-control DropdownCss" AutoPostBack="true" OnSelectedIndexChanged="ddlnewrange_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-4" style="">
                                        Section</label>
                                    <div class="col-sm-8" style="" id="Div4">
                                        <asp:DropDownList ID="ddlnewsection" runat="server" class="form-control DropdownCss" AutoPostBack="true" OnSelectedIndexChanged="ddlnewsection_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-4" style="">
                                        Beat</label>
                                    <div class="col-sm-8" style="" id="Div5">
                                        <asp:DropDownList ID="ddlnewbeat" runat="server" class="form-control DropdownCss">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 text-center">
                                    <asp:Button ID="btnsave" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnsave_Click1" />

                                </div>

                         </ContentTemplate>
                                </asp:UpdatePanel>
                    </div>
                </section>
            </div>
        </div>
    </section>
      
</asp:Content>
