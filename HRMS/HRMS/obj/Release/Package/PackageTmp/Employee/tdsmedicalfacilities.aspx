﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="tdsmedicalfacilities.aspx.cs" Inherits="HRMS.Employee.tdsmedicalfacilities" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <h4> Medical facilities</h4>
        <asp:Panel ID="Panel1" runat="server">
            <table>
                <tr>
                    <td>
                        Reimbursed by the Employer of amount spentby the employee in obtaining medical treatment for himself or any member of his family from  any doctornot exceedingrs 15000/- in a year
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="Button1" runat="server" Text="Detail" OnClick="Button1_Click" Width="59px" />
                    </td>
                </tr>
                 <tr>
                    <td>
                       Total :
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                    </td>
                    <td>
                       
                    </td>
                </tr>
                 <tr>
                    <td>
                       Taxable :
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                    </td>
                    <td>
                       
                    </td>
                </tr>
            </table>
            
        </asp:Panel>
        <asp:Panel ID="Panel2" runat="server">
            <div class="container">
            <h4> Details</h4>
            <table>
                <tr>
                    <td>
                        April
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        May
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        June
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        July
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        August
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        september
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        October
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox10" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        November
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox11" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        December
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox12" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        January
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox13" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        February
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox14" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        March
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox15" runat="server"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td>
                      Total :
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox16" runat="server"></asp:TextBox>
                    </td>
                </tr>
            </table>
                </div>
        </asp:Panel>
    </div>
</asp:Content>
