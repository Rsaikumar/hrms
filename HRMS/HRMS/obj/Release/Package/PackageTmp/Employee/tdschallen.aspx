﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="tdschallen.aspx.cs" Inherits="HRMS.Employee.tdschallen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

         <div class="container">
       <div class="col-md-12">
           <div class="col-md-2">
                   <%-- * IMPORTANT:please see notes overleaf before filling up yhe challan--%>
               </div>
           <div class="col-md-8" align="center">
               
                    <h4>T.D.S/TCS TAX CHALLAN</h4>
                </div>
           <div class="col-md-2">
                    Single Copy(to be sent to the ZAO)
               </div>
            </div>
      
            <div class="col-md-12">

        <div class="col-md-2">
             Challan No.ITNS
                    <br />
                    ITNS 281
        </div>
        <div class="col-md-8" align="center">
             Tax Applicable(TICK One)*<br />
                    TAX Deducted/Collected At Source From <br />
                    (0020) Companies <asp:CheckBox ID="CheckBox1" runat="server" /> (0021) NON-Comapanies <asp:CheckBox ID="CheckBox2" runat="server" />
        </div>
            <div class="col-md-2">
                  ASSESSMENT YEAR 2011-2012
            </div>
            </div>
              <div class="col-md-12">
        <div class="col-md-2">
            TAX Deduction Account NO.(T.A.N)
           </div>
                  
        </div>
              <div class="col-md-12">
        <div class="col-md-2">
            <asp:TextBox ID="TextBox1" runat="server" Text="MUMS48847B"></asp:TextBox>
           </div>
                  
        </div>
        <div class="col-md-12">
        
            <div class="col-md-2">
         FULL NAME:
           </div>
                  
        </div>
              <div class="col-md-12">
        <div class="col-md-2">
            <asp:TextBox ID="TextBox2" runat="server" Text="SENSYS TECHNOLOGIES PVT LTD" TextMode="MultiLine"></asp:TextBox>
           </div>
                  
        </div>
                   <div class="col-md-12">
        <div class="col-md-2">
          Complete Address With City & State
           </div>
                  
        </div>
              <div class="col-md-12">
        <div class="col-md-2">
            <asp:TextBox ID="TextBox3" runat="server" Text="Goregaon(EAST) MUmbai" TextMode="MultiLine"></asp:TextBox>
           </div>
                  
        </div>
        </div>
    <div class="container">
        <table>
            <tr>
                <td>
                    Nature of Payment<br />
                    DETAILS OF PAYMENT
                </td>
                <td>
                    Code* <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                    <br />
                    Amount(in RS.only)
                </td>
            </tr>

            <tr>
                <td colspan="2"></td>
            </tr>

            <tr>
                <td>Income</td>
                <td>
                    <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox></td>
            </tr>

             <tr>
                <td>Sucharge</td>
                <td>
                    <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox></td>
            </tr>

             <tr>
                <td>Education Cess</td>
                <td>
                    <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox></td>
            </tr>
             <tr>
                <td>Interest</td>
                <td>
                    <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>]</td>
            </tr>
             <tr>
                <td>Penality</td>
                <td>
                    <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox></td>
            </tr>
             <tr>
                <td>Others</td>
                <td>
                    <asp:TextBox ID="TextBox10" runat="server"></asp:TextBox>
                </td>
            </tr>
             <tr>
                <td>Total</td>
                <td>
                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label></td>
            </tr>
        </table>
        <div class="container">
            <div class="col-md-12">
        <div class="col-md-8">
            <h4>Total (in words)</h4>
            <table>
                <tr>
                    <td>CRORES</td>
                    <td>LACS</td>
                    <td>THOUSAND</td>
                    <td>HUNDREDS</td>
                    <td>TENS</td>
                    <td>UNITS</td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="TextBox11" runat="server"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="TextBox12" runat="server"></asp:TextBox></td><td>
                        <asp:TextBox ID="TextBox13" runat="server"></asp:TextBox></td><td>
                        <asp:TextBox ID="TextBox14" runat="server"></asp:TextBox></td><td>
                        <asp:TextBox ID="TextBox15" runat="server"></asp:TextBox></td><td>
                        <asp:TextBox ID="TextBox16" runat="server"></asp:TextBox></td>
                </tr>
            </table>
            <p>paid in cash/Debit to A/C/ Cheque No.</p>
            <p align="right"> Date:__/__/____</p>
            <p>Drawn on:</p>
            <p align="center">(Name of the Bank & Branch)</p>
        </div>
                <div class="col-md-5">
                    <table>
                        <tr>
                            <td>
                                For use in receiving bank
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Debit to A/c Cheque Creditied On
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox17" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>DD      MM       YY</td>
                        </tr>
                        <tr>
                            <td>
                               <p>SPACE FOR BANK SEAL</p>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            </div>
    </div>
            
</asp:Content>
