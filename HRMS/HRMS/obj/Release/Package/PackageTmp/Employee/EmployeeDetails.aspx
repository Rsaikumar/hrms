﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true"
    CodeBehind="EmployeeDetails.aspx.cs" Inherits="HRMS.Employee.EmployeeDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/Employee/Employee.css" rel="stylesheet" />

    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.js"></script>
    <%--<script src="//code.jquery.com/jquery-1.11.2.min.js" type="text/javascript"></script>--%>
    <script type="text/javascript" language="javascript">
        function ValidationForPersonalDetails() {
            if (document.getElementById('<%=txtEmp_Code.ClientID%>').value == "") {
                alert("Please Enter Emp Code.");

                document.getElementById('<%=txtEmp_Code.ClientID%>').focus();

                return false;
            }
            if (document.getElementById('<%=txtFirstName.ClientID%>').value == "") {
                alert("Please Enter Emp First Name.");

                document.getElementById('<%=txtFirstName.ClientID%>').focus();

                return false;
            }
            if (document.getElementById('<%=txtLastName.ClientID%>').value == "") {
                alert("Please Enter Emp Last Name.");

                document.getElementById('<%=txtLastName.ClientID%>').focus();

                return false;
            }
            if (document.getElementById('<%=txtUsername.ClientID%>').value == "") {
                alert("Please Enter User Name.");

                document.getElementById('<%=txtUsername.ClientID%>').focus();

                return false;
            }
            if (document.getElementById('<%=txtPassword.ClientID%>').value == "") {
                alert("Please Enter Password.");

                document.getElementById('<%=txtPassword.ClientID%>').focus();

                return false;
            }

            return true;
        }

    </script>

    <script type="text/javascript">
        function ShowImagePreview(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#<%=userImg.ClientID%>').prop('src', e.target.result)
                        .width(240)
                        .height(150);
                };
                reader.readAsDataURL(input.files[0]);
                }
            }
    </script>

    <script type="text/javascript">
        function UploadFile(fileUpload) {
            if (fileUpload.value != '') {
                document.getElementById("<%=licensetext.ClientID %>").style.backgroundColor = "Purple";
                document.getElementById("<%=licensetext.ClientID %>").style.color = "White";
            }
        }
        function UploadFile1(fileUpload) {
            if (fileUpload.value != '') {
                document.getElementById("<%=aadhartext.ClientID %>").style.backgroundColor = "Purple";
                document.getElementById("<%=aadhartext.ClientID %>").style.color = "White";
            }
        }
        function UploadFile2(fileUpload) {
            if (fileUpload.value != '') {
                document.getElementById("<%=pantext.ClientID %>").style.backgroundColor = "Purple";
                document.getElementById("<%=pantext.ClientID %>").style.color = "White";
            }
        }
        function UploadFile3(fileUpload) {
            if (fileUpload.value != '') {
                document.getElementById("<%=passporttext.ClientID %>").style.backgroundColor = "Purple";
                document.getElementById("<%=passporttext.ClientID %>").style.color = "White";
                // document.getElementById("<%= licensetext.ClientID %>").textContent = document.getElementById("<%= FupLicense.ClientID %>").value;
            }
        }

    </script>

    <style type="text/css">
        #ContentPlaceHolder1_MyTabs_tabOne_calendarDOB_popupDiv, ContentPlaceHolder1_MyTabs_tabsix_grvProfessional_CalendarGrdPeriodFrom_0_popupDiv, #ContentPlaceHolder1_MyTabs_tabThree_grdFamily_CalendarGrdDOB_0_body, #ContentPlaceHolder1_MyTabs_tabOne_CalendarDOAnn_popupDiv, #ContentPlaceHolder1_MyTabs_tabThree_grdFamily_CalendarGrdDOB_0_popupDiv
        {
            z-index: 10000;
        }

        #ContentPlaceHolder1_MyTabs_tabThree_grdFamily_CalendarGrdDOB_0_popupDiv, #ContentPlaceHolder1_MyTabs_tabThree_grdFamily_CalendarGrdDOB_0_container
        {
            z-index: 10000;
        }

        #ContentPlaceHolder1_MyTabs_tabFour_CalendarLicenseExpiryDate_popupDiv, #ContentPlaceHolder1_MyTabs_tabFour_CalendarIssuedDate_popupDiv,
        #ContentPlaceHolder1_MyTabs_tabFour_CalendarExpiryDate_popupDiv, #ContentPlaceHolder1_MyTabs_tabFour_CalendarRenewalDate_popupDiv
        {
            z-index: 10000;
        }

        .table tr th, .table tr td
        {
            padding: 0px;
            border-top: 1px dotted #ddd;
            z-index: 100000;
        }

        #ContentPlaceHolder1_MyTabs_tabsix_grvProfessional_CalendarGrdPeriodTo_0_popupDiv, #ContentPlaceHolder1_MyTabs_tabsix_grvProfessional_CalendarGrdPeriodFrom_0_popupDiv
        {
            z-index: 10000;
        }
    </style>

    <%--<script type="text/javascript">

function CalculateAge(birthday) {

var re=/^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d+$/;

if (birthday.value != '') {

if(re.test(birthday.value ))
{
birthdayDate = new Date(birthday.value);
dateNow = new Date();

var years = dateNow.getFullYear() - birthdayDate.getFullYear();
var months=dateNow.getMonth()-birthdayDate.getMonth();
var days=dateNow.getDate()-birthdayDate.getDate();
if (isNaN(years)) {

document.getElementById('lblAge').innerHTML = '';
document.getElementById('lblError').innerHTML = 'Input date is incorrect!';
return false;

}

else {
document.getElementById('lblError').innerHTML = '';

if(months < 0 || (months == 0 && days < 0)) {
years = parseInt(years) -1;
document.getElementById('txtAge').innerHTML = years + ' Years '
}
else {
    document.getElementById('txtAge').innerHTML = years + ' Years '
}
}
}
else
{
document.getElementById('lblError').innerHTML = 'Date must be mm/dd/yyyy format';
return false;
}
}
}
</script>--%>

    <script type="text/javascript" lang="javascript">

        function ValidTab() {

            //  alert(tabBehavior);
            if (document.getElementById('<%=txtAddress1.ClientID%>').value == "") {
                alert("Please Enter TextBox2 ");
                var tabBehavior = $get('<%=MyTabs.ClientID%>').control;

                tabBehavior.set_activeTabIndex('1');
                //  document.getElementById('<%=txtAddress1.ClientID%>').focus();
                return false;
            }
            if (document.getElementById('<%=txtAddress1.ClientID%>').value == "") {
                alert("Please Enter TextBox3 ");
                var tabBehavior = $get('<%=MyTabs.ClientID%>').control;
                tabBehavior.set_activeTabIndex('2');

                return false;
            }
            return true;
        };
    </script>



    <script type="text/javascript" lang="">
        function ValidationForOfficialTab() {
            if (document.getElementById('<%=ddlGroup.ClientID%>').options[document.getElementById('<%=ddlGroup.ClientID%>').selectedIndex].value == "0") {
                alert("Please Select Group Type");
                document.getElementById('<%=ddlGroup.ClientID%>').focus();
                return false;
            }
            return true;

        }

    </script>

    <script type="text/javascript" lang="javascript">
        function ValidationForCityAndState() {
            if (document.getElementById('<%=txtCityName.ClientID%>').value == "") {
                alert("Please Enter City Name");

                document.getElementById('<%=txtCityName.ClientID%>').focus();

                return false;
            }
            if (document.getElementById('<%=ddlState.ClientID%>').options[document.getElementById('<%=ddlState.ClientID%>').selectedIndex].value == "Select State") {
                alert("Please Select State Name");
                document.getElementById('<%=ddlState.ClientID%>').focus();
                return false;
            }
            return true;
        }

    </script>

    <script type="text/javascript" lang="javascript">
        function ValidationForDeptANDReporting() {

            if (document.getElementById('<%=ddlDepartment.ClientID%>').options[document.getElementById('<%=ddlDepartment.ClientID%>').selectedIndex].value == "Select") {
                alert("Please Select Department..");
                document.getElementById('<%=ddlDepartment.ClientID%>').focus();
                return false;
            }

            if (document.getElementById('<%=ddlReportingAuthority.ClientID%>').options[document.getElementById('<%=ddlReportingAuthority.ClientID%>').selectedIndex].value == "Select") {
                alert("Please Select Reporting Authority.. ");
                document.getElementById('<%=ddlReportingAuthority.ClientID%>').focus();
                return false;
            }
            return true;
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.js"></script>

    <div id="main">
        <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i>
                    </li>
                    <li><a href="">Payroll</a><i class="fa fa-angle-right"></i> </li>
                    <li><a href="EmployeeList.aspx">Employee list</a> <i class="fa fa-angle-right"></i></li>
                    <li><a href="EmployeeDetails.aspx">Employee Details</a> </li>
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>--%>

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                        <div class="box-title">
                            <h3>
                                <i class="fa fa-user"></i>Employee Details</h3>
                        </div>
                        <div class="box-content">
                            <form action="#" method="POST" class='form-horizontal form-bordered'>
                                <div class="col-md-3">
                                    <div class="SliderLeft" style="">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="">
                                                <%-- <img src="../img/administrator-icon.png" id="userImg" alt="">--%>
                                                <asp:Image ID="userImg" Height="130px" Width="300px" runat="server" ImageUrl="~/img/administrator-icon.png" />
                                            </div>
                                            <div>
                                                <span class="btn btn-default btn-file"><span class="fileinput-new">Select Image </span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <%-- <input type="file" id="FileUpload1" runat="server" name="..." />--%>
                                                    <asp:FileUpload ID="fupUserImage" runat="server" onchange="ShowImagePreview(this);" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-9" style="">

                                    <div class=" form-group col-md-5">
                                        <label for="textfield" class="control-label col-sm-3" style="">
                                            Emp Code<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                        <div class="col-sm-9" style="">
                                            <asp:TextBox ID="txtEmp_Code" runat="server" Enabled="false" placeholder="Emp Code" MaxLength="10"
                                                class="form-control" ReadOnly="false" AutoPostBack="true" OnTextChanged="txtEmp_Code_TextChanged"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="textfield" class="control-label col-sm-3" style="">
                                            First Name<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                        <div class="col-sm-9" style="" id="">
                                            <asp:TextBox ID="txtFirstName" runat="server" Enabled="false" placeholder="First Name" class="form-control"
                                                MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-5">
                                        <label for="textfield" class="control-label col-sm-3" style="">
                                            Middle Name</label>
                                        <div class="col-sm-9" style="" id="Div1">
                                            <asp:TextBox ID="txtMiddleName" runat="server" placeholder="Middle Name" Enabled="false" class="form-control"
                                                MaxLength="20"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="textfield" class="control-label col-sm-3" style="">
                                            Last Name<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                        <div class="col-sm-9" style="" id="Div2">
                                            <asp:TextBox ID="txtLastName" runat="server" Enabled="false" placeholder="Last Name" class="form-control"
                                                MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>


                                    <div class="form-group col-md-5">
                                        <label for="textfield" class="control-label col-sm-3" style="">
                                            User Role <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                        <div class="col-sm-9" style="" id="Div3">
                                            <asp:DropDownList ID="ddlUSerRole" Enabled="false" runat="server" class="form-control DropdownCss">
                                                <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                <asp:ListItem Text="APCCF" Value="APCCF"></asp:ListItem>
                                                <asp:ListItem Text="HOFF" Value="HOFF"></asp:ListItem>
                                                <asp:ListItem Text="POA" Value="POA"></asp:ListItem>
                                                <asp:ListItem Text="CCF" Value="CCF"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="textfield" class="control-label col-sm-3" style="">
                                            Username<span style="color: #ff0000; font-size: 14px;">*</span>

                                        </label>
                                        <div class="col-sm-9" style="" id="Div4">
                                            <asp:TextBox ID="txtUsername" runat="server" Enabled="false" placeholder="Username" class="form-control"
                                                MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="textfield" class="control-label col-sm-3" style="">
                                            Password<span style="color: #ff0000; font-size: 14px;">*</span></label><div class="col-sm-9" style="" id="Div5">
                                                <asp:TextBox ID="txtPassword" runat="server" Enabled="false" placeholder="Password" class="form-control"
                                                    MaxLength="50" TextMode="Password"></asp:TextBox>
                                            </div>
                                    </div>


                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group col-md-5">
                                                <label for="textfield" class="control-label col-sm-3" style="">
                                                    Emp Status</label>
                                                <div class="col-sm-9" style="" id="Div6">
                                                    <asp:DropDownList ID="ddlEmpStatus" runat="server" Enabled="false" class="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlEmpStatus_SelectedIndexChanged">
                                                        <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                        <asp:ListItem Text="Active" Value="Active"></asp:ListItem>
                                                        <asp:ListItem Text="On Leave" Value="On Leave"></asp:ListItem>
                                                        <asp:ListItem Text="On Long Leave" Value="On Long Leave"></asp:ListItem>
                                                        <asp:ListItem Text="Suspended" Value="Suspended"></asp:ListItem>
                                                        <asp:ListItem Text="Transferred" Value="Transferred"></asp:ListItem>
                                                        <asp:ListItem Text="Resigned" Value="Resigned"></asp:ListItem>
                                                        <asp:ListItem Text="Retired" Value="Retired"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>


                                            <div class="form-group col-md-6" id="ResignedDate" runat="server" visible="false">
                                                <asp:Label for="textfield" ID="lblResignedDate" runat="server" class="control-label col-md-5 lAResignedDate" Style="">
                                                        <span style="color: #ff0000; font-size: 14px;">*</span></asp:Label>
                                                <div class="col-md-7 tAPassword" style="" id="Div7">
                                                    <asp:TextBox ID="txtRetiredDate" runat="server" placeholder="Ex:dd-MM-YYYY" class="form-control"
                                                        MaxLength="50" TextMode="SingleLine"></asp:TextBox>
                                                    <ajaxToolKit:CalendarExtender ID="CalendarExtender2" PopupButtonID="imgPopup" runat="server"
                                                        TargetControlID="txtRetiredDate" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                    </ajaxToolKit:CalendarExtender>
                                                </div>
                                            </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                        </div>
                    </div>




                    <div class="col-md-12" style="">
                        <%--<asp:UpdatePanel runat="server">
                                        <ContentTemplate>--%>
                        <ajaxToolKit:TabContainer ID="MyTabs" runat="server" CssClass="MyTabStyle" ActiveTabIndex="8" OnActiveTabChanged="MyTabs_ActiveTabChanged" AutoPostBack="true" UseVerticalStripPlacement="true"
                            VerticalStripWidth="200px">
                            <ajaxToolKit:TabPanel runat="server" ID="tabOne" TabIndex="0">
                                <HeaderTemplate>
                                    <i class="fa fa-user"></i>Personal Details
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div class="col-md-10">
                                        <div class="tab-content" id="PersonalDetais">
                                            <asp:Panel ID="Panel1" runat="server" GroupingText="Personal Details" Style="border-bottom: 0px solid #8F8F8F;">
                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                    <ContentTemplate>
                                                        <div class="col-md-12">
                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-sm-3" style="">
                                                                    Date of Birth</label>
                                                                <div class="col-sm-9" style="" id="Div8">
                                                                    <asp:TextBox ID="txtDateOfBirth" ReadOnly="true" runat="server" placeholder="Ex:dd-MM-YYYY" class="form-control"
                                                                        MaxLength="50" OnTextChanged="txtDateOfBirth_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                                    <ajaxToolKit:CalendarExtender ID="calendarDOB" PopupButtonID="imgPopup" runat="server"
                                                                        TargetControlID="txtDateOfBirth" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                                    </ajaxToolKit:CalendarExtender>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-sm-3" style="">
                                                                    Age
                                                                </label>
                                                                <div class="col-sm-9" style="" id="Div9">
                                                                    <asp:TextBox ID="txtAge" runat="server" ReadOnly="true" placeholder="Age" class="form-control" MaxLength="50"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-3" style="">
                                                        Marital Status</label>
                                                    <div class="col-sm-9">
                                                        <asp:DropDownList ID="ddlMaritalStatus" runat="server" Enabled="false" class="form-control ">
                                                            <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                            <asp:ListItem Text="Single" Value="Single"></asp:ListItem>
                                                            <asp:ListItem Text="Married" Value="Married"></asp:ListItem>
                                                            <asp:ListItem Text="Widow" Value="Widow"></asp:ListItem>
                                                            <asp:ListItem Text="Divorced" Value="Divorced"></asp:ListItem>
                                                        </asp:DropDownList>


                                                    </div>

                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-3" style="">
                                                        Gender</label>
                                                    <div class="col-sm-9" style="" id="Div11">
                                                        <asp:RadioButtonList ID="rblGender" Enabled="false" runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Text="Female" Value="Female"></asp:ListItem>
                                                            <asp:ListItem Text="Male" Value="Male"></asp:ListItem>
                                                        </asp:RadioButtonList>

                                                    </div>
                                                </div>


                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-3" style="">
                                                        Blood Group</label>
                                                    <div class="col-sm-9" style="" id="Div12">
                                                        <asp:TextBox ID="txtBloodGroup" runat="server" ReadOnly="true" placeholder="Blood Group" class="form-control"
                                                            MaxLength="15"></asp:TextBox>


                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-3" style="">
                                                        Blood Donor<span style="color: #ff0000; font-size: 14px; visibility: hidden">*</span></label>
                                                    <div class="col-sm-9" style="" id="Div13">
                                                        <asp:CheckBox ID="cbBloodDonor" Enabled="false" runat="server" />


                                                    </div>
                                                </div>




                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-3" style="">
                                                        Religion</label>
                                                    <div class="col-sm-9" style="" id="Div14">
                                                        <asp:DropDownList ID="ddlReligion" runat="server" Enabled="false" class="form-control DropdownCss">
                                                            <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                            <asp:ListItem Text="Hindu" Value="Hindu"></asp:ListItem>
                                                            <asp:ListItem Text="Muslim" Value="Muslim"></asp:ListItem>
                                                            <asp:ListItem Text="Christian" Value="Christian"></asp:ListItem>
                                                        </asp:DropDownList>


                                                    </div>
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-3" style="">
                                                        Nationality</label>
                                                    <div class="col-sm-9" style="" id="Div15">
                                                        <asp:DropDownList ID="ddlNationality" Enabled="false" runat="server" class="form-control DropdownCss">
                                                            <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                            <asp:ListItem Text="Indian" Value="Indian"></asp:ListItem>
                                                            <asp:ListItem Text="OVERSEAS" Value="Overseas"></asp:ListItem>
                                                        </asp:DropDownList>


                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-3" style="">
                                                        Languages Known</label>
                                                    <div class="col-sm-9" id="Div16">
                                                        <asp:TextBox ID="txtLanguagesKnown" ReadOnly="true" runat="server" placeholder="Languages Known" MaxLength="15"
                                                            class="form-control"></asp:TextBox>


                                                    </div>

                                                    <div class="form-group col-md-6" style="visibility: hidden">
                                                        <label for="textfield" class="control-label col-sm-3" style="">
                                                            Date of Anniversary</label>
                                                        <div class="col-sm-9" style="" id="Div17">
                                                            <asp:TextBox ID="txtDateofAnniversary" runat="server" placeholder="Date of Anniversary"
                                                                class="form-control" MaxLength="50"></asp:TextBox>


                                                            <ajaxToolKit:CalendarExtender ID="CalendarDOAnn" PopupButtonID="imgPopup" runat="server"
                                                                TargetControlID="txtDateofAnniversary" Format="dd-MM-yyyy" Enabled="True">
                                                            </ajaxToolKit:CalendarExtender>


                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-6" style="visibility: hidden">
                                                    <label for="textfield" class="control-label col-sm-3" style="">
                                                        Hobbies</label>
                                                    <div class="col-sm-9" style="" id="Div18">
                                                        <asp:TextBox ID="txtHobbies" runat="server" placeholder="Hobbies /Interests" class="form-control"
                                                            MaxLength="15" TextMode="MultiLine"></asp:TextBox>


                                                    </div>
                                                </div>


                                                <div class="form-group col-md-6" style="visibility: hidden">
                                                    <label for="textfield" class="control-label col-sm-3"
                                                        style="">
                                                        Curricular Activities</label>
                                                    <div class="col-sm-9" style="" id="Div19">
                                                        <asp:TextBox ID="txtExtraCurricularActivities" runat="server" placeholder="Extra Curricular Activities"
                                                            class="form-control" MaxLength="15" TextMode="MultiLine"></asp:TextBox>


                                                    </div>
                                                </div>
                                            </asp:Panel>


                                        </div>
                                    </div>



                                </ContentTemplate>
                            </ajaxToolKit:TabPanel>

                            <ajaxToolKit:TabPanel runat="server" ID="tabTwo" TabIndex="1">
                                <HeaderTemplate>
                                    <i class="fa fa-book"></i>Contact Details
                                            
                                </HeaderTemplate>


                                <ContentTemplate>
                                    <div class="col-md-10">
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                            <ContentTemplate>
                                                <div class="tab-content" id="Contact">
                                                    <%-- <div class="tab-pane" id="Contact">--%>
                                                    <asp:Panel ID="Panel3" runat="server" GroupingText="Contact Details"
                                                        Style="border-bottom: 0px solid #8F8F8F;">
                                                        <div class="col-md-12">
                                                            <h5 class="sub-header">Present Address</h5>
                                                        </div>
                                                        <div class="col-md-12">

                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-sm-3" style="">
                                                                    Address 1 <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                                <div class="col-sm-9" style="" id="Div20">
                                                                    <asp:TextBox ID="txtAddress1" runat="server" placeholder="Address 1" class="form-control"
                                                                        MaxLength="250" TextMode="SingleLine"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-sm-3" style="">
                                                                    Address 2</label>
                                                                <div class="col-sm-9" style="" id="Div21">
                                                                    <asp:TextBox ID="txtAddress2" runat="server" placeholder="Address 2" class="form-control"
                                                                        MaxLength="250" TextMode="SingleLine"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-sm-3" style="">
                                                                    City<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                                <div class="col-sm-9" style="" id="Div22">
                                                                    <asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="true" class="form-control DropdownCss"
                                                                        OnSelectedIndexChanged="ddlCity_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="col-sm-0" style="" id="Div23">
                                                                    <asp:Button ID="btnAddCity" class="btnYear" runat="server" Style="" OnClick="btnAddCity_Click" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-sm-3" style="">
                                                                    State</label>
                                                                <div class="col-sm-9" style="" id="Div24">
                                                                    <asp:TextBox ID="txtState" runat="server" placeholder="State" class="form-control"
                                                                        MaxLength="20" TextMode="SingleLine" ReadOnly="true"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-sm-3" style="">
                                                                    Postal Code<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                                <div class="col-sm-9" style="" id="Div25">
                                                                    <asp:TextBox ID="txtPostalCode" runat="server" placeholder="Postal Code" class="form-control"
                                                                        MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-sm-3" style="">
                                                                    Country</label>
                                                                <div class="col-sm-9" style="" id="Div26">
                                                                    <asp:TextBox ID="txtCountry" runat="server" placeholder="Country" class="form-control"
                                                                        MaxLength="20" ReadOnly="true" TextMode="SingleLine"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-12">
                                                                <asp:CheckBox ID="chbPermentAddress" runat="server" OnCheckedChanged="chbPermentAddress_CheckedChanged"
                                                                    Text="If Present Address same as Permanent Address" AutoPostBack="true" />

                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-12">
                                                                <h5 style="color: rgb(236, 7, 7);">Permanent Address</h5>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-sm-3" style="">
                                                                    Address 1 <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                                <div class="col-sm-9" style="" id="Div28">
                                                                    <asp:TextBox ID="txtPAddress1" runat="server" ReadOnly="true" placeholder="Address 1" class="form-control"
                                                                        MaxLength="250" TextMode="SingleLine"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-sm-3" style="">
                                                                    Address 2</label>
                                                                <div class="col-sm-9" style="" id="Div29">
                                                                    <asp:TextBox ID="txtPaddress2" runat="server" ReadOnly="true" placeholder="Address 2" class="form-control"
                                                                        MaxLength="250" TextMode="SingleLine"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-sm-3" style="">
                                                                    City<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                                <div class="col-sm-9" style="" id="Div30">
                                                                    <asp:DropDownList ID="ddlPCity" runat="server" Enabled="false" class="form-control DropdownCss"
                                                                        OnSelectedIndexChanged="ddlPCity_SelectedIndexChanged" AutoPostBack="true">
                                                                        <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-sm-3" style="">
                                                                    State</label>
                                                                <div class="col-sm-9" style="" id="Div31">
                                                                    <asp:TextBox ID="txtPSate" runat="server" ReadOnly="true" placeholder="State" class="form-control"
                                                                        MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-sm-3" style="">
                                                                    Postal Code</label>
                                                                <div class="col-sm-9" style="" id="Div32">
                                                                    <asp:TextBox ID="txtPPalost" runat="server" ReadOnly="true" placeholder="Postal Code" class="form-control"
                                                                        MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-sm-3" style="">
                                                                    Country</label>
                                                                <div class="col-sm-9" style="" id="Div33">
                                                                    <asp:TextBox ID="txtpCountry" runat="server" ReadOnly="true" placeholder="Country" class="form-control"
                                                                        MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-sm-3" style="">
                                                                    Telephone</label>
                                                                <div class="col-sm-9" style="" id="Div34">
                                                                    <asp:TextBox ID="txtTelephone" runat="server" placeholder="Telephone" class="form-control"
                                                                        MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-sm-3" style="">
                                                                    Mobile</label>
                                                                <div class="col-sm-9" style="" id="Div35">
                                                                    <asp:TextBox ID="txtMobile" runat="server" ReadOnly="true" placeholder="Mobile" class="form-control"
                                                                        MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-12">
                                                                <label for="textfield" class="control-label col-sm-3" style="">
                                                                    Email</label>
                                                                <div class="col-sm- 9" style="" id="Div36">
                                                                    <asp:TextBox ID="txtEmail" runat="server" ReadOnly="true" placeholder="Email" class="form-control"
                                                                        MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-12">
                                                                <label for="textfield" class="control-label col-sm-3" style="">
                                                                    Alternative Email</label>
                                                                <div class="col-sm-9" style="" id="Div37">
                                                                    <asp:TextBox ID="txtAlternativeEmail" runat="server" placeholder="Alternative Email"
                                                                        class="form-control" MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h5>Social Network Info</h5>

                                                        <div class="Col-md-12">
                                                            <div class="form-group col-md-12">
                                                                <label for="textfield" class="control-label col-sm-3" style="">
                                                                    Skype ID</label>
                                                                <div class="col-sm-9" style="" id="Div38">
                                                                    <asp:TextBox ID="txtSkypeID" runat="server" placeholder="Skype ID" class="form-control"
                                                                        MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-12">
                                                                <label for="textfield" class="control-label col-sm-3" style="">
                                                                    Linkedin</label>
                                                                <div class="col-sm-9" style="" id="Div39">
                                                                    <asp:TextBox ID="txtLinkedin" runat="server" placeholder="Linkedin" class="form-control"
                                                                        MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="col-md-12" style="display: none;">
                                        <div class="form-actions col-sm-offset-2 col-sm-10">
                                            <asp:Button ID="btnSaveContact" runat="server" Text="Save" class="btn btn-primary" />
                                            <asp:Button ID="btnResetContact" runat="server" Text="Reset" class="btn" />
                                        </div>
                                    </div>


                                </ContentTemplate>


                            </ajaxToolKit:TabPanel>
                            <ajaxToolKit:TabPanel runat="server" ID="tabThree" TabIndex="2">
                                <HeaderTemplate>
                                    <i class="fa fa-bookmark"></i>Personal Identification Data
                                                        
                                            
                                </HeaderTemplate>


                                <ContentTemplate>
                                    <div class="col-md-10">
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                                <div class="tab-pane" id="IPersonal">
                                                    <asp:Panel ID="Panel7" runat="server" GroupingText="Personal Identification Data"
                                                        Style="border-bottom: 0px solid #8F8F8F;">
                                                        <div class="col-md-12">
                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-sm-5" style="">
                                                                    Driving L.No</label>
                                                                <div class="col-sm-7" style="" id="Div40">
                                                                    <asp:TextBox ID="txtDriving_License_Number" runat="server" placeholder="Driving License Number"
                                                                        class="form-control" MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-sm-5" style="">
                                                                    Type
                                                                </label>
                                                                <div class="col-sm-7" style="" id="Div41">
                                                                    <asp:DropDownList ID="ddlType" runat="server" class="form-control DropdownCss">
                                                                        <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                        <asp:ListItem Text="Two Wheeler" Value="Two Wheeler"></asp:ListItem>
                                                                        <asp:ListItem Text="LMV" Value="LMV"></asp:ListItem>
                                                                        <asp:ListItem Text="HMV" Value="HMV"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group col-md-8">
                                                                <label for="textfield" class="control-label col-sm-5" style="">
                                                                    License Expiry Date</label>
                                                                <div class="col-sm-7" style="" id="Div42">

                                                                    <asp:TextBox ID="txtLicenseExpiryDate" runat="server" placeholder="Ex:dd-MM-YYYY"
                                                                        class="form-control" MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                                    <ajaxToolKit:CalendarExtender ID="CalendarLicenseExpiryDate" PopupButtonID="imgPopup" runat="server"
                                                                        TargetControlID="txtLicenseExpiryDate" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                                    </ajaxToolKit:CalendarExtender>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <div class="col-md-12 attach">
                                                                    <span class="btn btn-default btn-file" id="licensetext" runat="server"><span class="fileinput-new" id="text" runat="server">Attach </span>
                                                                        <span class="fileinput-exists"></span>
                                                                        <%-- <input type="file" id="File1" runat="server" name="..." />--%>
                                                                        <asp:FileUpload ID="FupLicense" runat="server" />
                                                                    </span>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group col-md-8">
                                                                <label for="textfield" class="control-label col-sm-5" style="">
                                                                    Aadhar Number</label>
                                                                <div class="col-sm-7" style="" id="Div43">
                                                                    <asp:TextBox ID="txtAadharNumber" runat="server" placeholder="Aadhar Number" class="form-control"
                                                                        MaxLength="250" TextMode="SingleLine"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <div class="col-md-12 attach">
                                                                    <span class="btn btn-default btn-file" id="aadhartext" runat="server"><span class="fileinput-new">Attach </span><span
                                                                        class="fileinput-exists"></span>
                                                                        <%--<input type="file" id="File2" runat="server" name="..." />--%>
                                                                        <asp:FileUpload ID="FupAadhar" runat="server" />
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group col-md-8">
                                                                <label for="textfield" class="control-label col-sm-5" style="">
                                                                    PAN No</label>
                                                                <div class="col-sm-7" style="" id="Div44">
                                                                    <asp:TextBox ID="txtpPANNo" runat="server" placeholder="PAN No" class="form-control"
                                                                        MaxLength="250" TextMode="SingleLine"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <div class="col-md-12 attach">
                                                                    <span class="btn btn-default btn-file" id="pantext" runat="server"><span class="fileinput-new">Attach </span><span
                                                                        class="fileinput-exists"></span>
                                                                        <%--<input type="file" id="File3" runat="server" name="..." />--%>
                                                                        <asp:FileUpload ID="FupPanNo" runat="server" />
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-sm-5" style="">
                                                                    Passport Number</label>
                                                                <div class="col-sm-7" style="" id="Div45">
                                                                    <asp:TextBox ID="txtPassportNumber" runat="server" placeholder="Passport Number"
                                                                        class="form-control" MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-sm-5" style="">
                                                                    Issued Date</label>
                                                                <div class="col-sm-7" style="" id="Div46">
                                                                    <asp:TextBox ID="txtIssuedDate" runat="server" placeholder="Ex:dd-MM-YYYY" class="form-control"
                                                                        MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                                    <ajaxToolKit:CalendarExtender ID="CalendarIssuedDate" PopupButtonID="imgPopup" runat="server"
                                                                        TargetControlID="txtIssuedDate" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                                    </ajaxToolKit:CalendarExtender>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group col-md-8">
                                                                <label for="textfield" class="control-label col-sm-5" style="">
                                                                    Expiry Date</label>
                                                                <div class="col-sm-7" style="" id="Div47">
                                                                    <asp:TextBox ID="txtExpiryDate" runat="server" placeholder="Expiry Date" class="form-control"
                                                                        MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                                    <ajaxToolKit:CalendarExtender ID="CalendarExpiryDate" PopupButtonID="imgPopup" runat="server"
                                                                        TargetControlID="txtExpiryDate" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                                    </ajaxToolKit:CalendarExtender>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <div class="col-md-12">
                                                                    <span class="btn btn-default btn-file" id="passporttext" runat="server"><span class="fileinput-new">Attach </span><span
                                                                        class="fileinput-exists"></span>
                                                                        <%--<input type="file" id="File4" runat="server" name="..." />--%>
                                                                        <asp:FileUpload ID="FupPassport" runat="server" />
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-sm-5" style="">
                                                                    Bank Name<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                                <div class="col-sm-7" style="" id="Div48">
                                                                    <asp:DropDownList ID="ddlPBankName" runat="server" class="form-control DropdownCss">
                                                                        <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-sm-5" style="">
                                                                    Account Number</label>
                                                                <div class="col-sm-7" style="" id="Div49">
                                                                    <asp:TextBox ID="txtPAccountNumber" runat="server" placeholder="Account Number" class="form-control"
                                                                        MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-sm-5" style="">
                                                                    Health Insurance Policy No</label>
                                                                <div class="col-sm-7" style="" id="Div50">
                                                                    <asp:TextBox ID="txtPHealth" runat="server" placeholder="Health / Mediclaim Insurance Policy No"
                                                                        class="form-control" MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-sm-5" style="">
                                                                    Sum Insured</label>
                                                                <div class="col-sm-7" style="" id="Div51">
                                                                    <asp:TextBox ID="txtSumInsured" runat="server" placeholder="Sum Insured" class="form-control"
                                                                        MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group col-md-6">
                                                                <label for="textfield" class="control-label col-sm-5" style="">
                                                                    Renewal Date</label>
                                                                <div class="col-sm-7" style="" id="Div52">
                                                                    <asp:TextBox ID="txtRenewalDate" runat="server" placeholder="Ex:dd-MM-YYYY" class="form-control"
                                                                        MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                                    <ajaxToolKit:CalendarExtender ID="CalendarRenewalDate" PopupButtonID="imgPopup" runat="server"
                                                                        TargetControlID="txtRenewalDate" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                                    </ajaxToolKit:CalendarExtender>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="col-md-12" style="display: none;">

                                        <div class="form-actions col-sm-offset-2 col-sm-10 ACButtons">
                                            <asp:Button ID="btnSavePersonalData" runat="server" Text="Save" class="btn btn-primary" />
                                            <asp:Button ID="btnResetPersonalData" runat="server" Text="Reset" class="btn" />
                                        </div>
                                    </div>


                                </ContentTemplate>


                            </ajaxToolKit:TabPanel>
                            <ajaxToolKit:TabPanel runat="server" ID="tabFour" TabIndex="3">
                                <HeaderTemplate>
                                    <i class="fa fa-pencil-square-o"></i>Family Information
                                            
                                </HeaderTemplate>


                                <ContentTemplate>
                                    <div class="col-md-10">
                                        <div class="tab-pane" id="Emergency">
                                            <asp:Panel ID="pnlFamily" runat="server" GroupingText="Family Information" Style="border-bottom: 0px solid #8F8F8F;">

                                                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                                    <ContentTemplate>
                                                        <div class="col-md-12">
                                                            <di v style="overflow-x: scroll; width: 100%;">
                                                                <asp:GridView ID="grdFamily" class="table table-hover table-nomargin table-bordered CustomerAddress EducationGrid"
                                                                    Style="margin-bottom: 0.83%; margin-top: 0.83%;" runat="server" OnRowDataBound="grdFamily_RowDataBound"
                                                                    ShowFooter="true" AutoGenerateColumns="False" OnRowCreated="grdFamily_RowCreated">
                                                                    <AlternatingRowStyle BackColor="#EEEEEE" />
                                                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                                    <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                                    <SortedDescendingHeaderStyle BackColor="#000065" />
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="Name">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="txtName" runat="server" placeholder="Name" class="form-control"
                                                                                    MaxLength="10" Text='<%# Eval("Member_Name")%>'></asp:TextBox><%--Name--%>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Width="15%" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Relationship">
                                                                            <ItemTemplate>
                                                                                <asp:DropDownList ID="ddlRelationship" runat="server" class="form-control  DropdownCss"
                                                                                    MaxLength="20" DataTextField="Relationship" DataValueField="Relationship">
                                                                                    <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                                    <asp:ListItem Text="Father" Value="Father"></asp:ListItem>
                                                                                    <asp:ListItem Text="Mother" Value="Mother"></asp:ListItem>
                                                                                    <asp:ListItem Text="Spouse" Value="Spouse"></asp:ListItem>
                                                                                    <asp:ListItem Text="Daughter" Value="Daughter"></asp:ListItem>
                                                                                    <asp:ListItem Text="Son" Value="Son"></asp:ListItem>
                                                                                    <asp:ListItem Text="Brother" Value="Brother"></asp:ListItem>
                                                                                    <asp:ListItem Text="Sister" Value="Sister"></asp:ListItem>
                                                                                    <asp:ListItem Text="Others" Value="Others"></asp:ListItem>
                                                                                </asp:DropDownList>
                                                                                <asp:TextBox ID="txtrelation" runat="server" placeholder="Name" class="form-control"
                                                                                    Visible="false" Text='<%# Eval("Member_Relation")%>'></asp:TextBox><%--Relationship--%>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Width="7%" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Date of Birth">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="txtFDateofBirth" runat="server" placeholder="Ex:dd-MM-YYYY" class="form-control"
                                                                                    Text='<%(Eval("Member_DOB"))%>' MaxLength="13"></asp:TextBox>
                                                                                <ajaxToolKit:CalendarExtender ID="CalendarGrdDOB" PopupButtonID="imgPopup" runat="server"
                                                                                    TargetControlID="txtFDateofBirth" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                                                </ajaxToolKit:CalendarExtender>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Width="9%" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Occupation">
                                                                            <ItemTemplate>
                                                                                <asp:DropDownList ID="ddlOccupation" runat="server" class="form-control  DropdownCss"
                                                                                    MaxLength="20" DataTextField="Occupation" DataValueField="Occupation">
                                                                                    <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                                    <asp:ListItem Text="Govt Service" Value="Govt Service"></asp:ListItem>
                                                                                    <asp:ListItem Text="Private Service" Value="Private Service"></asp:ListItem>
                                                                                    <asp:ListItem Text="Self Employed" Value="Self Employed"></asp:ListItem>
                                                                                    <asp:ListItem Text="Housewife" Value="Housewife"></asp:ListItem>
                                                                                    <asp:ListItem Text="Student" Value="Student"></asp:ListItem>
                                                                                    <asp:ListItem Text="Retired" Value="Retired"></asp:ListItem>
                                                                                    <asp:ListItem Text="Others" Value="Others"></asp:ListItem>
                                                                                </asp:DropDownList>
                                                                                <asp:TextBox ID="txtoccupation" runat="server" placeholder="Name" class="form-control"
                                                                                    Visible="false" Text='<%# Eval("Member_Occupation")%>'></asp:TextBox><%--Occupation--%>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Width="10%" />
                                                                        </asp:TemplateField>
                                                                        <%--<asp:TemplateField HeaderText="Contact No">
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="txtFContactNo" runat="server" placeholder="Contact No" class="form-control"
                                                                                                Text='<%# Eval("Member_ContactNo")%>' MaxLength="30"></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                        <HeaderStyle Width="13%" />
                                                                                    </asp:TemplateField>--%>
                                                                        <asp:TemplateField HeaderText="Dependent">
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chbDependent" runat="server" Text="" />
                                                                                <asp:Label ID="lblchkdefault1" runat="server" Visible="false" Text='<%# Eval("Member_Dependent")%>'></asp:Label><%--Dependent--%>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Width="3%" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Nominee">
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chbNominee" runat="server" Text="" AutoPostBack="true" OnCheckedChanged="chbNominee_CheckedChanged" />
                                                                                <%--<asp:RadioButton ID="chbNominee" runat="server" Text="" />--%>
                                                                                <asp:Label ID="lblchkdefault2" runat="server" Visible="false" Text='<%# Eval("Member_Nominee")%>'></asp:Label><%--Nominee--%>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Width="3%" />
                                                                        </asp:TemplateField>

                                                                        <%-- <asp:TemplateField HeaderText="Contact on Emergency">
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="chbContactonEmergency" runat="server" Text="" />
                                                                                            <asp:Label ID="lblchkdefault3" runat="server" Visible="false" Text='<%# Eval("Member_Emergency_Contact")%>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                        <HeaderStyle Width="20%" />
                                                                                    </asp:TemplateField>--%>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lbRemove" runat="server" OnClick="lbRemove_Click">Remove</asp:LinkButton>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Width="3%" />
                                                                            <FooterStyle HorizontalAlign="Right" />
                                                                            <FooterTemplate>
                                                                                <asp:Button ID="btnAddNewRecord" runat="server" Text="Record" class="btn btn-primary"
                                                                                    OnClick="btnAddNewRecord_Click" />
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </di>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </asp:Panel>

                                        </div>
                                    </div>
                                    <div class="col-md-12">

                                        <div class="form-actions col-sm-offset-2 col-sm-10">
                                            <asp:Button ID="btnSaveFamily" runat="server" Text="Save" class="btn btn-primary"
                                                OnClick="btnSaveFamily_Click" />
                                            <asp:Button ID="btnUpdateFamily" runat="server" Text="Update" class="btn btn-primary"
                                                OnClick="btnUpdateFamily_Click" />
                                            <asp:Button ID="btnResetFamily" runat="server" Text="Reset" class="btn" OnClick="btnResetFamily_Click" />
                                        </div>
                                    </div>

                                </ContentTemplate>


                            </ajaxToolKit:TabPanel>
                            <ajaxToolKit:TabPanel runat="server" ID="tabFive" TabIndex="4">
                                <HeaderTemplate>
                                    <i class="fa fa-th-list"></i>Educational Qualifications
                                            
                                </HeaderTemplate>


                                <ContentTemplate>
                                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                        <ContentTemplate>
                                            <div class="col-md-10">
                                                <div class="tab-pane" id="Educational">
                                                    <asp:Panel ID="Panel6" runat="server" GroupingText="Educational Qualifications" Style="border-bottom: 0px solid #8F8F8F;">
                                                        <div class="col-md-12">
                                                            <div class="form-group col-md-12">
                                                                <label for="textfield" class="control-label col-sm-4"
                                                                    style="">
                                                                    Educate</label>
                                                                <div class="col-sm-8" style="" id="Div53">
                                                                    <asp:DropDownList ID="ddlEducate" runat="server" class="form-control DropdownCss">
                                                                        <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                        <asp:ListItem Text="S.S.C" Value="S.S.C"></asp:ListItem>
                                                                        <asp:ListItem Text="Intermediate" Value="Intermediate"></asp:ListItem>
                                                                        <asp:ListItem Text="Elementary school" Value="Elementary school"></asp:ListItem>
                                                                        <asp:ListItem Text="Master Degree" Value="Master Degree"></asp:ListItem>
                                                                        <asp:ListItem Text="Technical School" Value="Technical School"></asp:ListItem>
                                                                        <asp:ListItem Text="Middle School" Value="Middle School"></asp:ListItem>
                                                                        <asp:ListItem Text="Bachalor Degree" Value="Bachalor Degree"></asp:ListItem>

                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="col-md-12" style="overflow-x: scroll; width: 100%;">
                                                            <asp:GridView ID="grdEDucation" class="table table-bordered CustomerAddress EducationGrid" runat="server" ShowFooter="true"
                                                                AutoGenerateColumns="False">

                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Qualification/Course">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtQualification" runat="server" placeholder="Qualification" class="form-control"
                                                                                MaxLength="30" Text='<%# Eval("Qualification_Certification")%>'></asp:TextBox>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="17%" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Institute / University">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtInstitute_University" runat="server" placeholder="Institute / University"
                                                                                class="form-control" Text='<%# Eval("Institute_University")%>' MaxLength="30"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="15%" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Year Of Pass">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtYearOfPass" runat="server" placeholder="Year Of Pass" class="form-control"
                                                                                Text='<%# Eval("Year")%>' MaxLength="10"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="8%" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="% of Marks / GPA">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtMarks_GPA" runat="server" placeholder="% of Marks / GPA" class="form-control"
                                                                                Text='<%# Eval("Percentage")%>' MaxLength="30"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="10%" />
                                                                    </asp:TemplateField>
                                                                    <%--<asp:TemplateField HeaderText="Attach Certificate">
                                                                                <ItemTemplate>
                                                                                    <span class="btn btn-default btn-file"><span class="fileinput-new">Attach </span><span
                                                                                        class="fileinput-exists"></span>
                                                                                        <input type="file" id="File1" runat="server" name="..." />
                                                                                    </span>
                                                                                    <asp:TextBox ID="txtAttach_Certificate" runat="server" placeholder="Attach Certificate"
                                                                                        class="form-control" Text='<%# Eval("Attach_File")%>' MaxLength="30" Visible="false"></asp:TextBox>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="11%" />
                                                                            </asp:TemplateField>--%>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="lbRemoveRow" runat="server" OnClick="lkbRemoveEducationRecord_Click">Remove</asp:LinkButton>
                                                                        </ItemTemplate>
                                                                        <FooterStyle HorizontalAlign="Right" />
                                                                        <FooterTemplate>
                                                                            <asp:Button ID="btnNewRecordEducation" runat="server" Text="Record" class="btn btn-primary"
                                                                                OnClick="btnNewRecordEducation_Click" />
                                                                        </FooterTemplate>
                                                                        <HeaderStyle Width="4%" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </asp:Panel>
                                                    <asp:Panel ID="Panel9" runat="server" GroupingText="Skill Matrix" Style="border-bottom: 0px solid #8F8F8F; display: none;">
                                                        <div class="col-md-12">
                                                            <asp:GridView ID="grvSkill" class="table table-bordered CustomerAddress" runat="server" ShowFooter="true"
                                                                AutoGenerateColumns="False" OnRowDataBound="grvSkill_RowDataBound">

                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Skill">
                                                                        <ItemTemplate>
                                                                            <asp:DropDownList ID="ddlSkill_Name" runat="server" class="form-control  DropdownCss"
                                                                                MaxLength="20" DataTextField="Skill_Name" DataValueField="Skill_Name">
                                                                                <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                                <asp:ListItem Text="Java" Value="Java"></asp:ListItem>
                                                                                <asp:ListItem Text="Dotnet" Value="Dotnet"></asp:ListItem>
                                                                                <asp:ListItem Text="C++" Value="C++"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <asp:TextBox ID="txtSkillName" runat="server" placeholder="Skill Name" class="form-control"
                                                                                MaxLength="30" Text='<%# Eval("Skill_Name")%>' Visible="false"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Level">
                                                                        <ItemTemplate>
                                                                            <asp:DropDownList ID="ddlLevel" runat="server" class="form-control  DropdownCss"
                                                                                MaxLength="20" DataTextField="Skill_Level" DataValueField="Skill_Level"
                                                                                OnSelectedIndexChanged="ddlLevel_SelectedIndexChanged" AutoPostBack="true">
                                                                                <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                                <asp:ListItem Text="Level 1" Value="Level 1"></asp:ListItem>
                                                                                <asp:ListItem Text="Level 2" Value="Level 2"></asp:ListItem>
                                                                                <asp:ListItem Text="Level 3" Value="Level 3"></asp:ListItem>
                                                                                <asp:ListItem Text="Level 4" Value="Level 4"></asp:ListItem>
                                                                                <asp:ListItem Text="Level 5" Value="Level 5"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <asp:TextBox ID="txtLevel" runat="server" placeholder="Level" class="form-control"
                                                                                Text='<%# Eval("Skill_Level")%>' MaxLength="30" Visible="false"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="15%" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Description">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtDescription" runat="server" placeholder="Description" class="form-control"
                                                                                Text='<%# Eval("Description")%>' MaxLength="30"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="lbRemoveRowEducationSkill" runat="server" OnClick="lbRemoveRowEducationSkill_Click">Remove</asp:LinkButton>
                                                                        </ItemTemplate>
                                                                        <FooterStyle HorizontalAlign="Right" />
                                                                        <FooterTemplate>
                                                                            <asp:Button ID="btnNewRecordEducationSkill" runat="server" OnClick="btnNewRecordEducationSkill_Click"
                                                                                Text="Record" class="btn btn-primary" />
                                                                        </FooterTemplate>
                                                                        <HeaderStyle Width="2%" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </asp:Panel>
                                                    <asp:Panel ID="Panel10" runat="server" GroupingText="Certifications Holding" Style="border-bottom: 0px solid #8F8F8F; display: none;">
                                                        <div class="col-md-12">
                                                            <asp:GridView ID="grvCertification" class="table table-bordered CustomerAddress" runat="server"
                                                                ShowFooter="true" AutoGenerateColumns="False">

                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Course / Skill Name">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtCourse_Skill_Name" runat="server" placeholder="Course / Skill Name"
                                                                                class="form-control" MaxLength="30" Text='<%# Eval("Course_Skill_Name")%>'></asp:TextBox>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="18%" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Institute">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtInstitute" runat="server" placeholder="Institute" class="form-control"
                                                                                Text='<%# Eval("Institute")%>' MaxLength="30"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="15%" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Year">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtYear" runat="server" placeholder="Year" class="form-control"
                                                                                Text='<%# Eval("Year")%>' MaxLength="10"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="5%" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Attach Certificate Copy">
                                                                        <ItemTemplate>
                                                                            <span class="btn btn-default btn-file"><span class="fileinput-new">Attach </span><span
                                                                                class="fileinput-exists"></span>
                                                                                <input type="file" id="File11" runat="server" name="..." />
                                                                            </span>
                                                                            <asp:TextBox ID="txtAttach_Certificate_Copy" runat="server" placeholder="Attach Certificate Copy"
                                                                                class="form-control" Text='<%# Eval("Attach_Certificate_Copy")%>' MaxLength="30"
                                                                                Visible="false"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="16%" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="lbRemoveRow" runat="server">Remove</asp:LinkButton>
                                                                        </ItemTemplate>
                                                                        <FooterStyle HorizontalAlign="Right" />
                                                                        <FooterTemplate>
                                                                            <asp:Button ID="btnNewRecordEducation" runat="server" Text="Record" class="btn btn-primary" />
                                                                        </FooterTemplate>
                                                                        <HeaderStyle Width="2%" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <div class="col-md-12">
                                        <div class="form-actions col-sm-offset-2 col-sm-10 ACButtons">
                                            <asp:Button ID="btnSaveEducation" OnClick="btnSaveEducation_Click" OnClientClick="return Save_Validations()"
                                                runat="server" Text="Save" class="btn btn-primary" />
                                            <asp:Button ID="btnUpdateEducation" OnClick="btnUpdateEducation_Click"
                                                runat="server" Text="Update" class="btn btn-primary" /><%--OnClientClick="return Save_Validations()"--%>
                                            <asp:Button ID="btnRestEducation" runat="server" Text="Reset" class="btn" />
                                        </div>
                                    </div>

                                </ContentTemplate>


                            </ajaxToolKit:TabPanel>
                            <ajaxToolKit:TabPanel runat="server" ID="tabsix" TabIndex="5">
                                <HeaderTemplate>
                                    <i class="fa fa-user"></i>Professional Experience
                                            
                                </HeaderTemplate>


                                <ContentTemplate>
                                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                        <ContentTemplate>
                                            <div class="col-md-12">
                                                <div class="tab-pane" id="Professional">
                                                    <asp:Panel ID="Panel11" runat="server" GroupingText="Professional Experience" Style="border-bottom: 0px solid #8F8F8F;">

                                                        <asp:GridView ID="grvProfessional" class="table table-bordered CustomerAddress" runat="server"
                                                            ShowFooter="true" AutoGenerateColumns="False">

                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Name of the Employer">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtEmployer" runat="server" placeholder="Name of the Employer"
                                                                            class="form-control" MaxLength="30" Text='<%# Eval("Emp_Name")%>'></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Width="18%" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Position Held">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtPosition" runat="server" placeholder="Position Held" class="form-control"
                                                                            Text='<%# Eval("Emp_Designation")%>' MaxLength="30"></asp:TextBox>

                                                                    </ItemTemplate>
                                                                    <HeaderStyle Width="10%" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Period From">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtPeriodFrom" runat="server" placeholder="Period From" class="form-control"
                                                                            Text='<%#GetDate(Eval("Period_From"))%>' MaxLength="10" AutoPostBack="true" OnTextChanged="txtPeriodFrom_OnTextChanged"></asp:TextBox>
                                                                        <ajaxToolKit:CalendarExtender ID="CalendarGrdPeriodFrom" PopupButtonID="imgPopup" runat="server"
                                                                            TargetControlID="txtPeriodFrom" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                                        </ajaxToolKit:CalendarExtender>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Width="7%" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Period To">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtPeriodTo" runat="server" placeholder="Period To" class="form-control"
                                                                            Text='<%#GetDate(Eval("Period_To"))%>' MaxLength="10" AutoPostBack="true" OnTextChanged="txtPeriodTo_OnTextChanged"></asp:TextBox>

                                                                        <ajaxToolKit:CalendarExtender ID="CalendarGrdPeriodTo" PopupButtonID="imgPopup" runat="server"
                                                                            TargetControlID="txtPeriodTo" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                                        </ajaxToolKit:CalendarExtender>

                                                                    </ItemTemplate>
                                                                    <HeaderStyle Width="6%" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Total Duration">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtTotalDuration" runat="server" placeholder="Total Duration" class="form-control"
                                                                            Text='<%# Eval("Total_Duration")%>' MaxLength="10"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Width="8%" />
                                                                </asp:TemplateField>

                                                                <%--<asp:TemplateField HeaderText="Attach">
                                                                                <ItemTemplate>
                                                                                    <span class="btn btn-default btn-file"><span class="fileinput-new">Attach </span><span
                                                                                        class="fileinput-exists"></span>
                                                                                        <input type="file" id="File11" runat="server" name="..." />
                                                                                    </span>
                                                                                    <asp:TextBox ID="txtLicense" runat="server" placeholder="Attach"
                                                                                        class="form-control" Text='<%# Eval("Attach_File")%>' MaxLength="30"
                                                                                        Visible="false"></asp:TextBox>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="7%" />
                                                                            </asp:TemplateField>--%>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lbRemoveRowProf" runat="server" OnClick="lbRemoveRowProf_Click">Remove</asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <FooterStyle HorizontalAlign="Right" />
                                                                    <FooterTemplate>
                                                                        <asp:Button ID="btnNewRecordPRof" runat="server" Text="Record" class="btn btn-primary" OnClick="btnNewRecordPRof_Click" />
                                                                    </FooterTemplate>
                                                                    <HeaderStyle Width="2%" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>




                                                    </asp:Panel>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <div class="col-md-12">
                                        <div class="form-actions col-sm-offset-2 col-sm-10">
                                            <asp:Button ID="btnSaveProfession" runat="server" Text="Save" class="btn btn-primary" OnClick="btnSaveProfession_Click" />
                                            <asp:Button ID="btnUpdateProfession" runat="server" Text="Update" class="btn btn-primary" OnClick="btnUpdateProfession_Click" />
                                            <asp:Button ID="btnResetProfession" runat="server" Text="Reset" class="btn" OnClick="btnResetProfession_Click" />
                                        </div>
                                    </div>

                                </ContentTemplate>


                            </ajaxToolKit:TabPanel>
                            <ajaxToolKit:TabPanel runat="server" ID="tabEight" TabIndex="7">
                                <HeaderTemplate>
                                    <i class="fa fa-calendar-check-o"></i>Leave(s) Information
                                            
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div class="col-md-10">
                                        <div class="tab-pane" id="Leave">
                                            <asp:Panel ID="Panel12" runat="server" GroupingText="Leave(s) Information" Style="border-bottom: 0px solid #8F8F8F;">

                                                <div class="col-md-12" style="overflow-x: scroll; width: 100%;">
                                                    <asp:GridView ID="grvLeave" class="table table-hover table-nomargin table-bordered CustomerAddress EducationGrid"
                                                        Style="margin-bottom: 0.83%; margin-top: 0.83%;" runat="server"
                                                        ShowFooter="true" AutoGenerateColumns="False" OnRowCreated="grvLeave_RowCreated" OnRowDataBound="grvLeave_RowDataBound">
                                                        <AlternatingRowStyle BackColor="#EEEEEE" />
                                                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                        <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                        <SortedDescendingHeaderStyle BackColor="#000065" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Leave Name">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtLeaveName" runat="server" placeholder="Leave Name" class="form-control"
                                                                        MaxLength="30" Text='<%# Eval("Leave_Name")%>'></asp:TextBox>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="13%" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Eligible to Avail">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chbEligible" runat="server" Text="" />
                                                                    <asp:TextBox ID="txtEligible" runat="server" placeholder="Eligible to Avail"
                                                                        class="form-control" Text='<%# Eval("Eligible_Avail")%>' MaxLength="30" Visible="false"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="12%" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Opening Balance">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtOpeningBalance" runat="server" placeholder="Opening Balance" class="form-control"
                                                                        Text='<%# Eval("Total_Leaves")%>' MaxLength="10"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="12%" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="This Year Allotment">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtYearAllotment" runat="server" placeholder="This Year Allotment" class="form-control"
                                                                        Text='<%# Eval("Year_Allotment")%>' MaxLength="10"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="14%" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Availed as on Date">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtAvailedasonDate" runat="server" placeholder="Availed as on Date" class="form-control"
                                                                        Text='<%# Eval("Applied_Leaves")%>' MaxLength="10"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="13%" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Encashed This Year">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtEncashedThisYear" runat="server" placeholder="Encashed This Year" class="form-control"
                                                                        Text='<%# Eval("Encashed_Year")%>' MaxLength="30"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="15%" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Balance Available">
                                                                <ItemTemplate>

                                                                    <asp:TextBox ID="txtBalanceAvailable" runat="server" placeholder="Balance Available"
                                                                        class="form-control" Text='<%# Eval("Balance_Leaves")%>' MaxLength="30"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="13%" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lbRemoveRowLeave" runat="server" OnClick="lbRemoveRowLeave_Click">Remove</asp:LinkButton>
                                                                </ItemTemplate>
                                                                <FooterStyle HorizontalAlign="Right" />
                                                                <FooterTemplate>
                                                                    <asp:Button ID="btnNewRecordLeave" runat="server" Text="Record" class="btn btn-primary"
                                                                        OnClick="btnNewRecordLeave_Click" />
                                                                </FooterTemplate>
                                                                <HeaderStyle Width="1%" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-actions col-sm-offset-2 col-sm-10 ACButtons">
                                            <asp:Button ID="btnSaveLeave" OnClientClick="return Save_Validations()" OnClick="btnSaveLeave_Click"
                                                runat="server" Text="Save" class="btn btn-primary" />
                                            <asp:Button ID="btnUpdateLeave" runat="server" Text="Update" class="btn btn-primary" Visible="false" />
                                            <asp:Button ID="btnResetLeave" runat="server" Text="Reset" class="btn" />
                                        </div>
                                    </div>

                                </ContentTemplate>


                            </ajaxToolKit:TabPanel>
                            <ajaxToolKit:TabPanel runat="server" ID="TabPanel1" TabIndex="8">
                                <HeaderTemplate>
                                    <i class="fa fa-bullhorn"></i>Loan(s) Information
                                            
                                </HeaderTemplate>


                                <ContentTemplate>
                                    <div class="col-md-10">
                                        <div class="tab-pane" id="Loans">
                                            <asp:Panel ID="Panel13" runat="server" GroupingText="Loan(s) Information" Style="border-bottom: 0px solid #8F8F8F;">


                                                <div class="col-md-12" style="overflow-x: scroll; width: 100%;">
                                                    <asp:GridView ID="grvLoan" class="table table-hover table-nomargin table-bordered CustomerAddress EducationGrid"
                                                        Style="margin-bottom: 0.83%; margin-top: 0.83%;" runat="server"
                                                        ShowFooter="true" AutoGenerateColumns="False">
                                                        <AlternatingRowStyle BackColor="#EEEEEE" />
                                                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                        <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                        <SortedDescendingHeaderStyle BackColor="#000065" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Loan Type">
                                                                <ItemTemplate>
                                                                    <asp:DropDownList ID="ddlLoanType" runat="server" class="form-control DropdownCss"
                                                                        DataTextField="Loan_Type" DataValueField="Loan_Type">
                                                                    </asp:DropDownList>
                                                                    <asp:TextBox ID="txtLoanType" runat="server" placeholder="Loan Type" class="form-control"
                                                                        MaxLength="30" Text='<%# Eval("Loan_Type")%>' Visible="false"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="17%" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Loan Amount">
                                                                <ItemTemplate>

                                                                    <asp:TextBox ID="txtLoanAmount" runat="server" placeholder="Loan Amount"
                                                                        class="form-control" Text='<%# Eval("Loan_Amount")%>' MaxLength="30"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="15%" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Deduct/Month">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtDeductMonth" runat="server" placeholder="Deduct/Month" class="form-control"
                                                                        Text='<%# Eval("Deduct_Month")%>' MaxLength="10"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="8%" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Balance As on Date">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtBalanceAsonDate" runat="server" placeholder="Balance As on Date" class="form-control"
                                                                        Text='<%# Eval("BalanceAsonDate")%>' MaxLength="10"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="8%" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lbRemoveRowLoane" runat="server">Remove</asp:LinkButton>
                                                                </ItemTemplate>
                                                                <FooterStyle HorizontalAlign="Right" />
                                                                <FooterTemplate>
                                                                    <asp:Button ID="btnNewRecordLoane" runat="server" Text="Record" class="btn btn-primary" />
                                                                </FooterTemplate>
                                                                <HeaderStyle Width="4%" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>

                                    <div class="col-md-12">

                                        <div class="form-actions col-sm-offset-2 col-sm-10 ACButtons">
                                            <asp:Button ID="btnSaveLoan" OnClientClick="return Save_Validations()" runat="server" Text="Save" class="btn btn-primary" />
                                            <asp:Button ID="btnResetLoan" runat="server" Text="Reset" class="btn" />
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </ajaxToolKit:TabPanel>
                            <ajaxToolKit:TabPanel runat="server" ID="TabPanel2" TabIndex="9">
                                <HeaderTemplate>
                                    <i class="fa fa-lock"></i>Key Performance Indicators
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div class="col-md-10">
                                        <div class="tab-pane" id="Key">
                                            <asp:Panel ID="Panel14" runat="server" GroupingText="Key Performance Indicators (KPI)"
                                                Style="border-bottom: 0px solid #8F8F8F;">


                                                <%--<div class="col-md-12" style="overflow-x: scroll; width: 100%;">
                                                                    <asp:GridView ID="grvKey" class="table table-hover table-nomargin table-bordered CustomerAddress EducationGrid"
                                                                        Style="margin-bottom: 0.83%; margin-top: 0.83%;" runat="server"
                                                                        ShowFooter="true" AutoGenerateColumns="False" OnRowDataBound="grvKey_RowDataBound"
                                                                        OnRowCreated="grvKey_RowCreated">
                                                                        <AlternatingRowStyle BackColor="#EEEEEE" />
                                                                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                                        <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                                        <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                                        <SortedDescendingHeaderStyle BackColor="#000065" />
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="Objective / Goal">
                                                                                <ItemTemplate>

                                                                                    <asp:TextBox ID="txtObjective" runat="server" placeholder="Objective / Goal" class="form-control"
                                                                                        MaxLength="30" Text='<%# Eval("Objective")%>'></asp:TextBox>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="16%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Measurement">


                                                                                <ItemTemplate>
                                                                                    <asp:DropDownList ID="ddlMonitoringFrequency" runat="server" class="form-control DropdownCss"
                                                                                        DataTextField="Measurement" DataValueField="Measurement">
                                                                                        <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                                        <asp:ListItem Text="Measure" Value="Measure"></asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                    <asp:TextBox ID="txtMeasurement" runat="server" placeholder="Measurement"
                                                                                        class="form-control" Text='<%# Eval("Measurement")%>' MaxLength="30" Visible="false"></asp:TextBox>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="15%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Monitoring Frequency">
                                                                                <ItemTemplate>
                                                                                    <asp:TextBox ID="txtMonitoring" runat="server" placeholder="Monitoring Frequency" class="form-control"
                                                                                        Text='<%# Eval("Monitoring")%>' MaxLength="10"></asp:TextBox>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="8%" />
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Current Level">
                                                                                <ItemTemplate>
                                                                                    <asp:TextBox ID="txtCurrentLevel" runat="server" placeholder="Current Level" class="form-control"
                                                                                        Text='<%# Eval("Current_Level")%>' MaxLength="10"></asp:TextBox>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="8%" />
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lbRemoveRowLoane" runat="server" OnClick="lbRemoveRowLoane_Click">Remove</asp:LinkButton>
                                                                                </ItemTemplate>
                                                                                <FooterStyle HorizontalAlign="Right" />
                                                                                <FooterTemplate>
                                                                                    <asp:Button ID="btnNewRecordLoane" runat="server" Text="Record" class="btn btn-primary"
                                                                                        OnClick="btnNewRecordLoane_Click" />
                                                                                </FooterTemplate>
                                                                                <HeaderStyle Width="4%" />
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>--%>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    <div class="col-md-12">

                                        <%--  <div class="form-actions col-sm-offset-2 col-sm-10 ACButtons">
                                                        <asp:Button ID="btnSaveKey" OnClientClick="return Save_Validations()"
                                                            runat="server" Text="Save" OnClick="btnSaveKey_Click" class="btn btn-primary" />
                                                        <asp:Button ID="btnResetKey" runat="server" Text="Reset" class="btn" OnClick="btnResetKey_Click" />
                                                    </div>--%>
                                    </div>
                                </ContentTemplate>
                            </ajaxToolKit:TabPanel>
                            <ajaxToolKit:TabPanel runat="server" ID="tabSeven" TabIndex="6">
                                <HeaderTemplate>
                                    <i class="fa fa-clone"></i>Official Information
                                            
                                </HeaderTemplate>


                                <ContentTemplate>
                                    <div class="col-md-10">
                                        <div class="tab-pane" id="Official">
                                            <asp:Panel ID="Panel8" runat="server" GroupingText="Official Information" Style="border-bottom: 0px solid #8F8F8F;">
                                                <div class="col-md-12">
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            Date of Join</label>
                                                        <div class="col-sm-7" style="" id="Div58">
                                                            <asp:TextBox ID="txtDateofJoin" ReadOnly="true" runat="server" placeholder="Ex:dd-MM-YYYY" class="form-control"
                                                                MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                            <%-- <cc1:calendarextender id="CalendarExtender1" popupbuttonid="imgPopup" runat="server"
                                                                                targetcontrolid="txtToDate" popupposition="BottomRight" format="dd-MM-yyyy ">
                                                   </cc1:calendarextender>--%>
                                                            <ajaxToolKit:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="imgPopup"
                                                                TargetControlID="txtDateofJoin" Format="dd-MM-yyyy" PopupPosition="BottomRight">
                                                            </ajaxToolKit:CalendarExtender>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            Job Title / Designation</label>
                                                        <div class="col-sm-7" style="" id="Div59">
                                                            <asp:DropDownList ID="ddlJobTitle" Enabled="false" runat="server" class="form-control DropdownCss">
                                                                <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-sm-0" style="" id="Div61">
                                                            <asp:Button ID="btnJobTitle" class="btnYear" runat="server" Style="" OnClick="btnJobTitle_Click" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            Employee Grade</label>
                                                        <div class="col-sm-7" style="" id="Div60">
                                                            <asp:DropDownList ID="ddlEMPGrade" runat="server" Enabled="false" AutoPostBack="true" OnSelectedIndexChanged="ddlEMPGrade_SelectedIndexChanged"
                                                                class="form-control DropdownCss">
                                                                <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-sm-0" style="" id="Div62">
                                                            <asp:Button ID="btnEMPGrade" class="btnYear" runat="server" Style="" OnClick="btnEMPGrade_Click" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            Department<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                        <div class="col-sm-7" style="" id="Div63">
                                                            <asp:DropDownList ID="ddlDepartment" runat="server" Enabled="false" class="form-control DropdownCss">
                                                                <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                <asp:ListItem Text="HRD" Value="HRD"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-sm-0" style="" id="Div65">
                                                            <asp:Button ID="btnDepartment" class="btnYear" runat="server" Style="" OnClick="btnDepartment_Click" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">

                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            Reporting Authority<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                        <div class="col-sm-7" style="" id="Div66">
                                                            <asp:DropDownList ID="ddlReportingAuthority" runat="server" Enabled="false" class="form-control DropdownCss">
                                                                <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                <asp:ListItem Text="HOFF" Value="HOFF"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <%---- Newly added Field--%>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            Location<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                        <div class="col-sm-7" style="">
                                                            <asp:DropDownList ID="ddlLocation" runat="server" Enabled="false" class="form-control DropdownCss">
                                                                <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                <asp:ListItem Text="hyd" Value="hyd"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-md-7 bAYear" style="" id="Div54">
                                                            <asp:Button ID="btnLocation" class="btnYear" runat="server" Style="" OnClick="btnLocation_Click" />
                                                        </div>

                                                    </div>

                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            Work Type<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                        <div class="col-sm-7" style="">
                                                            <asp:DropDownList ID="ddlWorkType" runat="server" Enabled="false" class="form-control DropdownCss">
                                                                <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                <asp:ListItem Text="Direct" Value="Direct"></asp:ListItem>
                                                                <asp:ListItem Text="InDirect" Value="InDirect"></asp:ListItem>

                                                            </asp:DropDownList>
                                                        </div>

                                                    </div>

                                                    <div class="form-group col-md-6">

                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            Working in Shifts</label>
                                                        <div class="col-sm-7" style="" id="Div69">
                                                            <asp:CheckBox ID="chbWorking" runat="server" Enabled="false" Text="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">

                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            Recruit<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                        <div class="col-sm-7" style="" id="Recruit">
                                                            <asp:DropDownList ID="ddlRecruit" runat="server" Enabled="false" class="form-control DropdownCss">
                                                                <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                <asp:ListItem Text="Direct" Value="Direct"></asp:ListItem>
                                                                <asp:ListItem Text="InDirect" Value="InDirect"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-sm-0" style="" id="Div615">
                                                            <asp:Button ID="btnRecruit" class="btnYear" runat="server" Style="" OnClick="btnRecruit_Click" />
                                                        </div>
                                                    </div>

                                                </div>
                                            </asp:Panel>
                                            <asp:Panel ID="PanelSalary" runat="server" GroupingText="Salary and Perks" Style="border-bottom: 0px solid #8F8F8F;">
                                                <div class="col-md-10">
                                                    <%--<div class="form-group col-md-6">
                                                                    <label for="textfield" class="control-label col-md-5 lABasicAmount" style="">
                                                                        DA<span style="color: #ff0000; font-size: 14px; display: none;">*</span></label>
                                                                    <div class="col-md-7 tABasicAmount" style="" id="Div39">
                                                                        <asp:TextBox ID="TextBox1" runat="server" AutoPostBack="true"
                                                                            OnTextChanged="txtBasicAmount_TextChanged" placeholder="DA" class="form-control"
                                                                            MaxLength="50"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                               
                                                                 <div class="form-group col-md-6">
                                                                    <label for="textfield" class="control-label col-md-5 lABasicAmount" style="">
                                                                        HRA<span style="color: #ff0000; font-size: 14px; display: none;">*</span></label>
                                                                    <div class="col-md-7 tABasicAmount" style="" id="Div40">
                                                                        <asp:TextBox ID="TextBox2" runat="server" AutoPostBack="true"
                                                                            OnTextChanged="txtBasicAmount_TextChanged" placeholder="HRA" class="form-control"
                                                                            MaxLength="50"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                  
                                                                                                                               
                                                                 <div class="form-group col-md-6">
                                                                    <label for="textfield" class="control-label col-md-5 lABasicAmount" style="">
                                                                        CCA<span style="color: #ff0000; font-size: 14px; display: none;">*</span></label>
                                                                    <div class="col-md-7 tABasicAmount" style="" id="Div41">
                                                                        <asp:TextBox ID="TextBox3" runat="server" AutoPostBack="true"
                                                                            OnTextChanged="txtBasicAmount_TextChanged" placeholder="CCA" class="form-control"
                                                                            MaxLength="50"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                    --%>

                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            Basic Amount<span style="color: #ff0000; font-size: 14px; display: none;">*</span></label>
                                                        <div class="col-sm-7" style="" id="Div55">
                                                            <asp:TextBox ID="txtBasicAmount" runat="server" AutoPostBack="true"
                                                                OnTextChanged="txtBasicAmount_TextChanged" ReadOnly="true" placeholder="Basic Amount" class="form-control"
                                                                MaxLength="50"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            Gross Amount</label>
                                                        <div class="col-sm-7" style="" id="Div56">
                                                            <asp:TextBox ID="txtGrossAmount" runat="server" placeholder="Gross Amount" ReadOnly="true" class="form-control"
                                                                MaxLength="20"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            DA<span style="color: #ff0000; font-size: 14px; display: none;">*</span></label>
                                                        <div class="col-sm-7" style="" id="Div57">
                                                            <asp:TextBox ID="txtda" ReadOnly="true" runat="server" AutoPostBack="true"
                                                                placeholder="DA" class="form-control"
                                                                MaxLength="50"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            HRA</label>
                                                        <div class="col-sm-7" style="" id="Div64">
                                                            <asp:TextBox ID="txthra" runat="server" ReadOnly="true" placeholder="HRA" class="form-control"
                                                                MaxLength="20"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div></div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            CCA</label>
                                                        <div class="col-sm-7" style="" id="Div67">
                                                            <asp:TextBox ID="txtcca" runat="server" placeholder="CCA" ReadOnly="true" class="form-control"
                                                                MaxLength="20" OnTextChanged="txtcca_TextChanged"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                </div>


                                                <div class="col-md-12">
                                                    <asp:GridView ID="grvSalary" class="table table-bordered CustomerAddress" runat="server" ShowFooter="true"
                                                        AutoGenerateColumns="False">

                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Head">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtHead" runat="server" placeholder="Head" class="form-control"
                                                                        MaxLength="10" ReadOnly="true" Text='<%# Eval("Head")%>'></asp:TextBox>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="15%" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Amount">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtAmount" runat="server" placeholder="Amount" class="form-control"
                                                                        Text='<%# Eval("Amount")%>' MaxLength="30" AutoPostBack="true" OnTextChanged="txtAmount_TextChanged"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:Label ID="lbltotalAmount" runat="server" Text=""></asp:Label>
                                                                </FooterTemplate>
                                                                <HeaderStyle Width="13%" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lbRemoveSalary" runat="server">Remove</asp:LinkButton>
                                                                </ItemTemplate>
                                                                <FooterStyle HorizontalAlign="Right" />
                                                                <FooterTemplate>
                                                                    <asp:Button ID="btnAddNewRecordSalary" runat="server" Text="Record" class="btn btn-primary" />
                                                                </FooterTemplate>
                                                                <HeaderStyle Width="2%" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </asp:Panel>

                                            <asp:Panel ID="Panel2Deductions" runat="server" GroupingText="Deductions" Style="border-bottom: 0px solid #8F8F8F;">
                                                <div class="col-md-12">
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            PF</label>
                                                        <div class="col-sm-7" style="" id="Div70">
                                                            <asp:CheckBox ID="chbPF" runat="server" Text="" />
                                                        </div>

                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            PF Code</label>
                                                        <div class="col-sm-7" style="" id="Div71">
                                                            <asp:TextBox ID="txtPFCode" runat="server" placeholder="PF Code" class="form-control"
                                                                MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            PF Amount</label>
                                                        <div class="col-sm-7" style="" id="Div68">
                                                            <asp:TextBox ID="txtpfamount" runat="server" placeholder="PF Amount" class="form-control"
                                                                MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            ESI</label>
                                                        <div class="col-sm-7" style="" id="Div72">
                                                            <asp:CheckBox ID="chbESI" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            ESI Code</label>
                                                        <div class="col-sm-7" style="" id="Div73">
                                                            <asp:TextBox ID="txtESICode" runat="server" placeholder="ESI Number" class="form-control"
                                                                MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            ESI Amount</label>
                                                        <div class="col-sm-7" style="" id="Div74">
                                                            <asp:TextBox ID="txtesiamount" runat="server" placeholder="ESI Amount" class="form-control"
                                                                MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            EESI Amount</label>
                                                        <div class="col-sm-7" style="" id="Div75">
                                                            <asp:TextBox ID="txteesi" runat="server" placeholder="EESI Amount" class="form-control"
                                                                MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-md-5 lAESIDispensary" style="">
                                                            ESI Dispensary</label>
                                                        <div class="col-md-7 tAESIDispensary" style="" id="Div76">
                                                            <asp:DropDownList ID="ddlESIDispensary" runat="server" class="form-control DropdownCss">
                                                                <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-md-5 lAProfessional" style="">
                                                            Professional Tax (PT)</label>
                                                        <div class="col-md-7 tAESI" style="" id="Div77">
                                                            <asp:CheckBox ID="chbProfessional" runat="server" Text="" />
                                                        </div>
                                                        <div class="col-md-7 tAESIDispensary" style="" id="Div78">
                                                            <asp:DropDownList ID="ddlptax" runat="server" class="form-control DropdownCss">
                                                                <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                <asp:ListItem Text="15001-20000" Value="150"></asp:ListItem>
                                                                <asp:ListItem Text="20001+" Value="200"></asp:ListItem>
                                                            </asp:DropDownList>

                                                        </div>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-md-5 lAGrossAmount" style="">
                                                            TDS</label>
                                                        <div class="col-md-7 tAGrossAmount" style="" id="Div80">
                                                            <asp:TextBox ID="txttds" runat="server" placeholder="TDS" class="form-control"
                                                                MaxLength="20"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>



                                            <asp:Panel ID="Panel1OtherDeductions" runat="server" GroupingText="Other Deductions Opted By Employee"
                                                Style="border-bottom: 0px solid #8F8F8F;">
                                                <div class="col-md-12">
                                                    <asp:GridView ID="grvOtherDeductions" class="table  table-bordered CustomerAddress" runat="server" ShowFooter="true"
                                                        AutoGenerateColumns="False" OnRowDataBound="grvOtherDeductions_RowDataBound">

                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Head">
                                                                <ItemTemplate>
                                                                    <asp:DropDownList ID="ddlHead" runat="server" class="form-control  DropdownCss" MaxLength="20"
                                                                        DataTextField="Head" DataValueField="Head">
                                                                        <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                        <asp:ListItem Text="Select Head1" Value="Select Head1"></asp:ListItem>
                                                                        <asp:ListItem Text="Select Head2" Value="Select Head2"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:TextBox ID="txtHead1" runat="server" placeholder="Head" class="form-control"
                                                                        MaxLength="10" Text='<%# Eval("Head")%>' Visible="false"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="15%" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Amount">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtAmount1" runat="server" placeholder="Amount" class="form-control"
                                                                        Text='<%# Eval("Amount")%>' MaxLength="30"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="13%" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Frequency">
                                                                <ItemTemplate>
                                                                    <asp:DropDownList ID="ddlFrequency" runat="server" class="form-control  DropdownCss"
                                                                        MaxLength="20" DataTextField="Frequency" DataValueField="Frequency">
                                                                        <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                        <asp:ListItem Text="Monthly" Value="Monthly"></asp:ListItem>
                                                                        <asp:ListItem Text="Quarterly" Value="Quarterly"></asp:ListItem>
                                                                        <asp:ListItem Text="Halfly" Value="Halfly"></asp:ListItem>
                                                                        <asp:ListItem Text="Annually" Value="Annually"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:TextBox ID="txtFrequency" runat="server" placeholder="Frequency" class="form-control"
                                                                        Text='<%# Eval("Frequency")%>' MaxLength="30" Visible="false"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="13%" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lbRemoveOther" runat="server" OnClick="lkbRemoveDeductionsRecord_Click">Remove</asp:LinkButton>
                                                                </ItemTemplate>
                                                                <FooterStyle HorizontalAlign="Right" />
                                                                <FooterTemplate>
                                                                    <asp:Button ID="btnEmployeeDeductionsAddNewRecord" runat="server" Text="Record" OnClick="btnEmployeeDeductionsAddNewRecord_Click"
                                                                        class="btn btn-primary" />
                                                                </FooterTemplate>
                                                                <HeaderStyle Width="2%" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel ID="Panel2Information" runat="server" GroupingText="Information on Company Facilities Availed"
                                                Style="border-bottom: 0px solid #8F8F8F;">
                                                <div class="col-md-12">
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-md-5 lAQuarter" style="">
                                                            Quarter / Accommodation</label>
                                                        <div class="col-md-7 tAQuarter" style="" id="Div82">
                                                            <asp:CheckBox ID="chbQuarter" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-md-5 lAFourWheeler" style="">
                                                            Four Wheeler</label>
                                                        <div class="col-md-7 tAFourWheeler" style="" id="Div83">
                                                            <asp:CheckBox ID="chbFourWheeler" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-md-5 lATwoWheeler" style="">
                                                            Two Wheeler</label>
                                                        <div class="col-md-7 tATwoWheeler" style="" id="Div84">
                                                            <asp:CheckBox ID="chbTwoWheeler" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-md-5 lALaptop" style="">
                                                            Laptop</label>
                                                        <div class="col-md-7 tALaptop" style="" id="Div85">
                                                            <asp:CheckBox ID="chbLaptop" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-md-5 lAMobilePhone" style="">
                                                            Mobile Phone</label>
                                                        <div class="col-md-7 tAMobilePhone" style="" id="Div86">
                                                            <asp:CheckBox ID="chbMobilePhone" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-md-5 lAOtherFacilities" style="">
                                                            Other Facilities</label>
                                                        <div class="col-md-7 tAOtherFacilities" style="" id="Div87">
                                                            <asp:TextBox ID="txtOtherFacilities" runat="server" placeholder="Other Facilities"
                                                                class="form-control" MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-actions col-sm-offset-2 col-sm-10 ACButtons">
                                                        <asp:Button ID="btnSaveOfficial" OnClientClick="return ValidationForDeptANDReporting()" runat="server"
                                                            Text="Save" class="btn btn-primary" OnClick="btnSaveOfficial_Click" />

                                                        <asp:Button ID="btnUpdateOfficial" runat="server"
                                                            Text="Update" class="btn btn-primary" OnClientClick="return ValidationForDeptANDReporting()" OnClick="btnUpdateOfficial_Click" />
                                                        <asp:Button ID="btnResetOfficial" runat="server" Text="Reset" class="btn" />
                                                    </div>
                                                </div>

                                </ContentTemplate>


                            </ajaxToolKit:TabPanel>
                        </ajaxToolKit:TabContainer>
                        <asp:UpdatePanel>
                            <ContentTemplate>

                                <div class="col-md-12" id="PersonalButtons" runat="server">
                                    <div class="form-actions col-sm-offset-2 col-sm-10 ACButtons">
                                        <asp:Button ID="btnSavePersonal" runat="server" Text="Save" OnClick="btnSavePersonal_Click"
                                            OnClientClick="return ValidationForPersonalDetails()" class="btn btn-primary" />
                                        <asp:Button ID="btnUpdatePersonal" runat="server" Text="Update" class="btn btn-primary" OnClick="btnUpdatePersonal_Click" />
                                        <asp:Button ID="btnResetPersonal" runat="server" Text="Reset" class="btn" />

                                    </div>

                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="col-sm-10">
                            <div class="form-actions col-sm-offset-2 col-sm-10 ACButtons">
                                <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary" />
                                <asp:Button ID="btnCancel" runat="server" Text="Reset" class="btn" />
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btnAddressDetails" runat="server" Style="display: none;" />
    <ajaxToolKit:ModalPopupExtender ID="mp1" runat="server" BehaviorID="MPCity" PopupControlID="Panel4" TargetControlID="btnAddressDetails"
        CancelControlID="btnClose" BackgroundCssClass="modalBackground">
    </ajaxToolKit:ModalPopupExtender>
    <asp:Panel ID="Panel4" runat="server" CssClass="modalPopup" align="center" Style="display: none; height: 30%;">
        <div class="modal-header" runat="server" id="Popupheader">
            <h3>City<a class="close-modal" href="#" id="btnClose">&times;</a></h3>
        </div>
        <asp:UpdatePanel ID="UpdatePanel8" runat="server">
            <ContentTemplate>
                <div class="modal-body">
                    <div class="panel-body">
                        <div class="col-md-12" style="margin-bottom: 2%">
                            <div class="form-group col-md-6">
                                <label for="textfield" class="control-label col-md-5  lCYearID" style="">
                                    City ID
                                </label>
                                <div class="col-sm-4 TCYearID" style="margin-left: 0px;">
                                    <asp:TextBox ID="txtCityID" runat="server" class="form-control" placeholder="City ID"
                                        TextMode="SingleLine" MaxLength="10" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="textfield" class="control-label col-md-5 lCYear" style="">
                                    City Name <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                <div class="col-sm-4 TCYear" style="margin-left: 0px;">
                                    <asp:TextBox ID="txtCityName" runat="server" class="form-control" placeholder="City Name"
                                        TextMode="SingleLine"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="">
                            <div class="form-group col-md-6">
                                <label for="textfield" class="control-label col-md-5 lCState" style="">
                                    State <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                <div class="col-sm-4 TState" style="margin-left: 0px;">
                                    <asp:DropDownList ID="ddlState" runat="server" class="form-control DropdownCss">
                                        <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                        <asp:ListItem Text="Andhra Pradesh" Value="Andhra Pradesh"></asp:ListItem>
                                        <asp:ListItem Text="Arunachal Pradesh" Value="Arunachal Pradesh"></asp:ListItem>
                                        <asp:ListItem Text="Assam" Value="Assam"></asp:ListItem>
                                        <asp:ListItem Text="Bihar" Value="Bihar"></asp:ListItem>
                                        <asp:ListItem Text="Chhattisgarh" Value="Chhattisgarh"></asp:ListItem>
                                        <asp:ListItem Text="Goa" Value="Goa"></asp:ListItem>
                                        <asp:ListItem Text="Gujarat" Value="Gujarat"></asp:ListItem>
                                        <asp:ListItem Text="Haryana" Value="Haryana"></asp:ListItem>
                                        <asp:ListItem Text="Himachal Pradesh" Value="Himachal Pradesh"></asp:ListItem>
                                        <asp:ListItem Text="Jammu and Kashmir" Value="Jammu and Kashmir"></asp:ListItem>
                                        <asp:ListItem Text="Jharkhand" Value="Jharkhand"></asp:ListItem>
                                        <asp:ListItem Text="Karnataka" Value="Karnataka"></asp:ListItem>
                                        <asp:ListItem Text="Kerala" Value="Kerala"></asp:ListItem>
                                        <asp:ListItem Text="Madhya Pradesh" Value="Madhya Pradesh"></asp:ListItem>
                                        <asp:ListItem Text="Maharashtra" Value="Maharashtra"></asp:ListItem>
                                        <asp:ListItem Text="Manipur" Value="Manipur"></asp:ListItem>
                                        <asp:ListItem Text="Meghalaya" Value="Meghalaya"></asp:ListItem>
                                        <asp:ListItem Text="Mizoram" Value="Mizoram"></asp:ListItem>
                                        <asp:ListItem Text="Nagaland" Value="Nagaland"></asp:ListItem>
                                        <asp:ListItem Text="Odisha" Value="Odisha"></asp:ListItem>
                                        <asp:ListItem Text="Punjab" Value="Punjab"></asp:ListItem>
                                        <asp:ListItem Text="Rajasthan" Value="Rajasthan"></asp:ListItem>
                                        <asp:ListItem Text="Sikkim" Value="Sikkim"></asp:ListItem>
                                        <asp:ListItem Text="Tamil Nadu" Value="Tamil Nadu"></asp:ListItem>
                                        <asp:ListItem Text="Telangana" Value="Telangana"></asp:ListItem>
                                        <asp:ListItem Text="Tripura" Value="Tripura"></asp:ListItem>
                                        <asp:ListItem Text="Uttar Pradesh" Value="Uttar Pradesh"></asp:ListItem>
                                        <asp:ListItem Text="Uttarakhand" Value="Uttarakhand"></asp:ListItem>
                                        <asp:ListItem Text="West Bengal" Value="West Bengal"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="textfield" class="control-label col-md-5 lCState" style="">
                                    Country <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                <div class="col-sm-4 TState" style="margin-left: 0px;">
                                    <asp:DropDownList ID="ddlCountry" runat="server" class="form-control DropdownCss">
                                        <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                        <asp:ListItem Text="Afghanistan" Value="Afghanistan"></asp:ListItem>
                                        <asp:ListItem Text="Australia" Value="Australia"></asp:ListItem>
                                        <asp:ListItem Text="Bangladesh" Value="Bangladesh"></asp:ListItem>
                                        <asp:ListItem Text="Barbados" Value="Barbados"></asp:ListItem>
                                        <asp:ListItem Text="Brazil" Value="Brazil"></asp:ListItem>
                                        <asp:ListItem Text="Burma" Value="Burma"></asp:ListItem>
                                        <asp:ListItem Text="Canada" Value="Canada"></asp:ListItem>
                                        <asp:ListItem Text="China" Value="China"></asp:ListItem>
                                        <asp:ListItem Text="Colombia" Value="Colombia"></asp:ListItem>
                                        <asp:ListItem Text="Cuba" Value="Cuba"></asp:ListItem>
                                        <asp:ListItem Text="Denmark" Value="Denmark"></asp:ListItem>
                                        <asp:ListItem Text="Dominica" Value="Dominica"></asp:ListItem>
                                        <asp:ListItem Text="Egypt" Value="Egypt"></asp:ListItem>
                                        <asp:ListItem Text="Finland" Value="Finland"></asp:ListItem>
                                        <asp:ListItem Text="France" Value="France"></asp:ListItem>
                                        <asp:ListItem Text="Germany" Value="Germany"></asp:ListItem>
                                        <asp:ListItem Text="Greece" Value="Greece"></asp:ListItem>
                                        <asp:ListItem Text="Hong Kong" Value="Hong Kong"></asp:ListItem>
                                        <asp:ListItem Text="Iceland" Value="Iceland"></asp:ListItem>
                                        <asp:ListItem Text="India" Value="India"></asp:ListItem>
                                        <asp:ListItem Text="Indonesia" Value="Indonesia"></asp:ListItem>
                                        <asp:ListItem Text="Iran" Value="Iran"></asp:ListItem>
                                        <asp:ListItem Text="Iraq" Value="Iraq"></asp:ListItem>
                                        <asp:ListItem Text="Ireland" Value="Ireland"></asp:ListItem>
                                        <asp:ListItem Text="Italy" Value="Italy"></asp:ListItem>
                                        <asp:ListItem Text="Jamaica" Value="Jamaica"></asp:ListItem>
                                        <asp:ListItem Text="Japan" Value="Japan"></asp:ListItem>
                                        <asp:ListItem Text="Jordan" Value="Jordan"></asp:ListItem>
                                        <asp:ListItem Text="Kenya" Value="Kenya"></asp:ListItem>
                                        <asp:ListItem Text="Kuwait" Value="Kuwait"></asp:ListItem>
                                        <asp:ListItem Text="Libya" Value="Libya"></asp:ListItem>
                                        <asp:ListItem Text="Madagascar" Value="Madagascar"></asp:ListItem>
                                        <asp:ListItem Text="Malaysia" Value="Malaysia"></asp:ListItem>
                                        <asp:ListItem Text="Mexico" Value="Mexico"></asp:ListItem>
                                        <asp:ListItem Text="Namibia" Value="Namibia"></asp:ListItem>
                                        <asp:ListItem Text="Nepal" Value="Nepal"></asp:ListItem>
                                        <asp:ListItem Text="Netherlands" Value="Netherlands"></asp:ListItem>
                                        <asp:ListItem Text="New Zealand" Value="New Zealand"></asp:ListItem>
                                        <asp:ListItem Text="Pakistan" Value="Pakistan"></asp:ListItem>
                                        <asp:ListItem Text="Philippines" Value="Philippines"></asp:ListItem>
                                        <asp:ListItem Text="Poland" Value="Poland"></asp:ListItem>
                                        <asp:ListItem Text="Russia" Value="Russia"></asp:ListItem>
                                        <asp:ListItem Text="San Marino" Value="San Marino"></asp:ListItem>
                                        <asp:ListItem Text="Singapore" Value="Singapore"></asp:ListItem>
                                        <asp:ListItem Text="South Africa" Value="South Africa"></asp:ListItem>
                                        <asp:ListItem Text="Spain" Value="Spain"></asp:ListItem>
                                        <asp:ListItem Text="Sri Lanka" Value="Sri Lanka"></asp:ListItem>
                                        <asp:ListItem Text="Switzerland" Value="Switzerland"></asp:ListItem>
                                        <asp:ListItem Text="Thailand " Value="Thailand "></asp:ListItem>
                                        <asp:ListItem Text="Turkey" Value="Turkey"></asp:ListItem>
                                        <asp:ListItem Text="Uganda" Value="Uganda"></asp:ListItem>
                                        <asp:ListItem Text="United Kingdom" Value="United Kingdom"></asp:ListItem>
                                        <asp:ListItem Text="USA" Value="USA"></asp:ListItem>
                                        <asp:ListItem Text="Zambia" Value="Zambia"></asp:ListItem>
                                        <asp:ListItem Text="Zimbabwe" Value="Zimbabwe"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="popbuttons" style="">
                                <asp:Button ID="btnNewAddDetails" class="btn btn-primary" runat="server" Text="Submit"
                                    OnClientClick="return ValidationForCityAndState()" OnClick="btnNewAddDetails_Click" />
                                <asp:Button ID="btnAddReset" runat="server" Text="Reset" class="btn" OnClick="btnAddReset_Click" />
                            </div>
                        </div>

                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <!-- /.panel -->
        <%--</div>--%>
    </asp:Panel>
    <asp:Button ID="btnJob" runat="server" Style="display: none;" />
    <ajaxToolKit:ModalPopupExtender ID="MPJob" runat="server" PopupControlID="PanelJob" TargetControlID="btnJob"
        CancelControlID="btnCloseJob" BackgroundCssClass="modalBackground">
    </ajaxToolKit:ModalPopupExtender>

    <asp:Panel ID="PanelJob" runat="server" CssClass="modalPopup" align="center" Style="display: none;">
        <div class="modal-header" runat="server" id="Popupheader1">
            <h3>Job Title<a class="close-modal" href="#" id="btnCloseJob">&times;</a></h3>
        </div>
        <asp:UpdatePanel ID="UpdatePanel9" runat="server">
            <ContentTemplate>
                <div class="modal-body">
                    <div class="panel-body">
                        <div class="col-md-12" style="">
                            <div class="form-group col-md-6">
                                <label for="textfield" class="control-label col-md-5  lCYearID" style="">
                                    Job ID
                                </label>
                                <div class="col-sm-4 TCYearID" style="margin-left: 0px;">
                                    <asp:TextBox ID="txtJobID" runat="server" class="form-control" placeholder="Job ID"
                                        TextMode="SingleLine" MaxLength="10" AutoPostBack="true"
                                        OnTextChanged="txtJobID_TextChanged"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="textfield" class="control-label col-md-5 lCYear" style="">
                                    Job Name <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                <div class="col-sm-4 TCYear" style="margin-left: 0px;">
                                    <asp:TextBox ID="txtJobTitleName" runat="server" class="form-control" placeholder="Job Title Name"
                                        TextMode="SingleLine" MaxLength="25" AutoPostBack="true"
                                        OnTextChanged="txtJobTitleName_TextChanged"></asp:TextBox>
                                </div>
                            </div>

                            <div class="popbuttons" style="margin-top: 1%">
                                <asp:Button ID="btnJobTitleSave" class="btn btn-primary" runat="server" Text="Submit" OnClick="btnJobTitleSave_Click" />
                                <asp:Button ID="btnJobTitleReset" runat="server" Text="Reset" class="btn" OnClick="btnJobTypeRest_Click" />
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <asp:Button ID="btnGrade" runat="server" Style="display: none;" />
    <ajaxToolKit:ModalPopupExtender ID="MPGrade" runat="server" PopupControlID="PanelGrade" TargetControlID="btnGrade"
        CancelControlID="btnCloseGrade" BackgroundCssClass="modalBackground">
    </ajaxToolKit:ModalPopupExtender>
    <asp:Panel ID="PanelGrade" runat="server" CssClass="modalPopup" align="center" Style="display: none;">
        <div class="modal-header" runat="server" id="Popupheader2">
            <h3>Employee Group<a class="close-modal" href="#" id="btnCloseGrade">&times;</a></h3>
        </div>
        <asp:UpdatePanel ID="UpdatePanel10" runat="server">
            <ContentTemplate>
                <div class="modal-body">
                    <div class="panel-body">
                        <div class="col-md-12" style="">

                            <div class="col-md-12" style="">
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-md-5 lAID" style="width: 15%">
                                        ID
                                    </label>
                                    <div class="col-md-7 tAID" style="">
                                        <asp:TextBox ID="txtID" runat="server" placeholder="ID" MaxLength="50"
                                            class="form-control" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-md-5 lAHeadName" style="width: 16%;">
                                        Group Name<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                    <div class="col-md-7 tAHeadName" style="" id="Div88">
                                        <asp:TextBox ID="txtHeadName" runat="server" placeholder="Group Name" class="form-control"
                                            MaxLength="50" AutoPostBack="true" OnTextChanged="txtHeadName_TextChanged"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-md-5 lAShortName" style="width: 16%;">
                                        Short Name<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                    <div class="col-md-7 tAShortName" style="" id="Div89">
                                        <asp:TextBox ID="txtShortName" runat="server" placeholder="Short Name" class="form-control"
                                            MaxLength="50" AutoPostBack="true" OnTextChanged="txtShortName_TextChanged"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-md-5 lACalculation" style="width: 16%;">
                                        Group Type<span style="color: #ff0000; font-size: 14px; display: inline;">*</span>
                                    </label>
                                    <div class="col-md-7 tACalculation" style="">
                                        <%--  onchange="ddlCalculationValidation()"--%>
                                        <asp:DropDownList ID="ddlGroup" runat="server" class="form-control DropdownCss">
                                            <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                            <asp:ListItem Text="Permanent" Value="Permanent"></asp:ListItem>
                                            <asp:ListItem Text="Contractor virtual" Value="Contractor virtual"></asp:ListItem>
                                            <asp:ListItem Text="Consulate Dated" Value="Consulate Dated"></asp:ListItem>
                                            <asp:ListItem Text="Daily Wise" Value="Daily Wise"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-12" style="">
                                <div class="form-group ">
                                    <label for="textfield" class="control-label col-md-5 lblAUCompanyID" style="width: 18%; margin-left: -5%;">
                                        Applicable Heads
                                    </label>
                                    <div class="col-md-7 tAUCompanyID " style="">
                                        <asp:CheckBoxList ID="chlHeadsLists" runat="server" class="ListControl" RepeatDirection="Horizontal"
                                            RepeatLayout="Table" RepeatColumns="3" OnSelectedIndexChanged="chlHeadsLists_SelectedIndexChanged"
                                            AutoPostBack="true">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-12" style="">
                                <div class="popbuttons" style="">
                                    <asp:Button ID="btnGroupSave" class="btn btn-primary" runat="server" Text="Submit" OnClientClick="return ValidationForOfficialTab()" OnClick="btnGroupSave_Click" />
                                    <asp:Button ID="btnGroupReset" runat="server" Text="Reset" class="btn" OnClick="btnGroupRest_Click" />
                                </div>
                            </div>
                        </div>
                        <!-- /.panel -->
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <asp:Button ID="btnRecruitDetails" runat="server" Style="display: none;" />
    <ajaxToolKit:ModalPopupExtender ID="mpRecruit" runat="server" PopupControlID="RecruitPanel2" TargetControlID="btnRecruitDetails"
        CancelControlID="btnClosRecruit" BackgroundCssClass="modalBackground">
    </ajaxToolKit:ModalPopupExtender>
    <asp:Panel ID="RecruitPanel2" runat="server" CssClass="modalPopup" align="center" Style="display: none;">
        <div class="modal-header" runat="server" id="Div90">
            <h3>Recruit<a class="close-modal" href="#" id="btnClosRecruit">&times;</a></h3>
        </div>
        <asp:UpdatePanel ID="UpdatePanel11" runat="server">
            <ContentTemplate>
                <div class="modal-body">
                    <div class="panel-body">
                        <div class="col-md-12" style="">

                            <div class="col-md-12" style="">
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-md-5  lCRecruitID" style="">
                                        Recruit No
                                    </label>
                                    <div class="col-sm-4 TCYearID" style="margin-left: 0px;">
                                        <asp:TextBox ID="txtRecruitNo" runat="server" class="form-control" placeholder="Recruit No"
                                            TextMode="SingleLine" MaxLength="10"
                                            AutoPostBack="true" OnTextChanged="txtRecruitNo_TextChanged"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-md-5 lCRecruitName" style="">
                                        Recruit Name <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                    <div class="col-sm-4 TCYear" style="margin-left: 0px;">
                                        <asp:TextBox ID="txtRecruitName" runat="server" class="form-control" placeholder="Recruit Name"
                                            TextMode="SingleLine" MaxLength="30"
                                            AutoPostBack="true" OnTextChanged="txtRecruitName_TextChanged"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" style="">
                                <div class="popbuttons" style="">
                                    <asp:Button ID="btnRecruitSave" class="btn btn-primary" runat="server" Text="Submit" OnClick="btnRecruitSave_Click" />
                                    <asp:Button ID="btnRecruitRest" runat="server" Text="Reset" class="btn" OnClick="btnRecruitRest_Click" />

                                </div>
                            </div>
                        </div>
                        <!-- /.panel -->
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <asp:Button ID="btnDeptDetails" runat="server" Style="display: none;" />
    <ajaxToolKit:ModalPopupExtender ID="MPDept" runat="server" PopupControlID="PanelDept" TargetControlID="btnDeptDetails"
        CancelControlID="btnCloseDept" BackgroundCssClass="modalBackground">
    </ajaxToolKit:ModalPopupExtender>
    <asp:Panel ID="PanelDept" runat="server" CssClass="modalPopup" align="center" Style="display: none;">
        <div class="modal-header" runat="server" id="Popupheader3">
            <h3>Department<a class="close-modal" href="#" id="btnCloseDept">&times;</a></h3>
        </div>
        <asp:UpdatePanel ID="UpdatePanel12" runat="server">
            <ContentTemplate>
                <div class="modal-body">
                    <div class="panel-body">
                        <div class="col-md-12" style="">
                            <div class="col-md-12" style="">
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-md-5  lCDeptID" style="">
                                        Department ID
                                    </label>
                                    <div class="col-sm-4 TCYearID" style="margin-left: 0px;">
                                        <asp:TextBox ID="txtDeptID" runat="server" class="form-control" placeholder="Department ID"
                                            TextMode="SingleLine" MaxLength="10" AutoPostBack="true" OnTextChanged="txtDepTID_OnTextChanged"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-md-5 lCDepartName" style="">
                                        Department Name <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                    <div class="col-sm-4 TCYear" style="margin-left: 0px;">
                                        <asp:TextBox ID="txtDepartName" runat="server" class="form-control" placeholder="Department Name"
                                            TextMode="SingleLine" MaxLength="30"
                                            AutoPostBack="true" OnTextChanged="txtDepartName_OnTextChanged"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" style="">
                                <div class="popbuttons" style="">
                                    <asp:Button ID="btnDepartSave" class="btn btn-primary" runat="server" Text="Submit" OnClick="btnDepartSave_Click" />
                                    <asp:Button ID="btnDepartRest" runat="server" Text="Reset" class="btn" OnClick="btnDepartRest_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <%--This Is Related To Location Popup--%>

    <asp:Button ID="btnLocationPop" runat="server" Style="display: none;" />
    <ajaxToolKit:ModalPopupExtender ID="MPLocation" runat="server" PopupControlID="PanelLocation" TargetControlID="btnLocationPop"
        CancelControlID="btnCloseLocation" BackgroundCssClass="modalBackground">
    </ajaxToolKit:ModalPopupExtender>
    <asp:Panel ID="PanelLocation" runat="server" CssClass="modalPopup" align="center" Style="display: none;">
        <div class="modal-header" runat="server" id="Div91">
            <h3>Location<a class="close-modal" href="#" id="btnCloseLocation">&times;</a></h3>
        </div>
        <asp:UpdatePanel ID="UpdatePanel13" runat="server">
            <ContentTemplate>
                <div class="modal-body">
                    <div class="panel-body">
                        <div class="col-md-12" style="">
                            <div class="col-md-12" style="">
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-md-5  lCDeptID" style="">
                                        Location Id
                                    </label>
                                    <div class="col-sm-4 TCYearID" style="margin-left: 0px;">
                                        <asp:TextBox ID="txtLocationId" runat="server" class="form-control" placeholder="Location ID"
                                            TextMode="SingleLine" MaxLength="10"></asp:TextBox><%--AutoPostBack="true" OnTextChanged="txtDepTID_OnTextChanged"--%>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-md-5 lCDepartName" style="">
                                        Location Name <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                    <div class="col-sm-4 TCYear" style="margin-left: 0px;">
                                        <asp:TextBox ID="txtLocationName" runat="server" class="form-control" placeholder="Location Name"
                                            TextMode="SingleLine" MaxLength="50"
                                            AutoPostBack="true" OnTextChanged="txtLocationName_OnTextChanged"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <%--   <div class="col-md-12" style="">
                                        <div class="popbuttons" style="">
                                            <asp:Button ID="btnLocationSave" class="btn btn-primary" runat="server" Text="Submit" OnClick="btnLocationSave_Click" />
                                            <asp:Button ID="btnLocationReset" runat="server" Text="Reset" class="btn" OnClick="btnLocationReset_Click" />
                                        </div>
                                    </div>--%>
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <%--End of This Is Related To Location Popup--%>

    <%--</ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnSavePersonal" />
                </Triggers>
            </asp:UpdatePanel>--%>
    <style>
        .ajax__tab_default .ajax__tab_tab
        {
            margin-right: 4px;
            overflow: hidden;
            text-align: left !important;
            cursor: pointer;
            display: -moz-inline-box;
            display: inline-block;
        }


        .ajax__calendar .ajax__calendar_container
        {
            z-index: 10000;
        }
    </style>
</asp:Content>
