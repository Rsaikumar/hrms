﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="detaildreports.aspx.cs" Inherits="HRMS.Reports.detaildreports" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
        }
        .auto-style2 {
            width: 130px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
   <%-- <script language="javascript">
        window.print();
</script>--%>
    <script type = "text/javascript">
        function PrintPanel() {
            var panel = document.getElementById("<%=pnlContents.ClientID %>");
            var printWindow = window.open('', '', 'height=200,width=2000');
            printWindow.document.write('<html><head><title>DIV Contents</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
            return false;
        }
    </script>

</head>
        <body>            
     <div id="main">
        <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx">
                        <%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i></li>
                    <li><a href="">Employee</a><i class="fa fa-angle-right"></i> </li>
                   
                    <li><a href="">PaySlips</a> </li>
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>
            </div>
         </div>
            <div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                        <div class="box-title">
                            <h3>
                                <i class="fa fa-th-list"></i>Detaild view</h3>
                        </div>
                        <div class="box-content">
                            <form action="#" method="POST" class='form-horizontal form-bordered'>
                                <div class="col-sm-12" style="">
                                    
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lAEmpCode" style="">
                                                Emp Code
                                            </label>
                                            <div class="col-sm-2 tAEmpCode" style="">
                                                <asp:TextBox ID="txtEmpCode" runat="server" placeholder="Emp Code" MaxLength="50"
                                                    class="form-control"  AutoPostBack="true" OnTextChanged="txtEmpCode_TextChanged"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-2 lALeaveName" style="">
                                                        Employee Name<span id="Span1" runat="server" style="color: Red; display: inline;">*</span></label>
                                                    <div class="col-sm-2 tAEmpName" style="" id="">
                                                        <asp:DropDownList ID="ddlEmpName" runat="server"  class="form-control DropdownCss"  TabIndex="1" AutoPostBack="True" OnSelectedIndexChanged="ddlEmpName_SelectedIndexChanged" 
                                                            >
                                                          
                                                          
                                                        </asp:DropDownList>
                                                        
                                                    </div>
                                                </div>
                                      <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-2 lALeaveName" style="">
                                                        Feature<span id="Span3" runat="server" style="color: Red; display: inline;">*</span></label>
                                                    <div class="col-sm-2 tAEmpName" style="" id="Div2">
                                                        <asp:DropDownList ID="ddlfeature" runat="server"  class="form-control DropdownCss"  TabIndex="1" AutoPostBack="True" OnSelectedIndexChanged="ddlfeature_SelectedIndexChanged"
                                                            >
                                                            <asp:ListItem>---select---</asp:ListItem>
                                                            
                                                            <asp:ListItem>PF</asp:ListItem>
                                                            <asp:ListItem>ESI</asp:ListItem>
                                                          <asp:ListItem>salary Report</asp:ListItem>
                                                            <asp:ListItem>APGLI</asp:ListItem>
                                                            <asp:ListItem>GIS</asp:ListItem>
                                                            <asp:ListItem>Leave Report</asp:ListItem>
                                                            <asp:ListItem>Loan Report</asp:ListItem>
                                                            <asp:ListItem>Employee Report</asp:ListItem>
                                                           <%-- <asp:ListItem>monthly payslip</asp:ListItem>--%>
                                                          
                                                        </asp:DropDownList>
                                                        
                                                    </div>
                                                </div>
                                 <%--   <div class="col-sm-12" style="">
                                    <%--<div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lALeaveName" style="">
                                                       Month<span id="Span2" runat="server" style="color: Red; display: inline;">*</span></label>
                                                    <div class="col-sm-2 tAEmpName" style="" id="Div1">
                                                        <asp:DropDownList ID="ddlmonth" runat="server"  class="form-control DropdownCss"  TabIndex="1" AutoPostBack="True" OnSelectedIndexChanged="ddlEmpName_SelectedIndexChanged" 
                                                            >
                                                            <asp:ListItem>---select---</asp:ListItem>
                                                            <asp:ListItem>January</asp:ListItem>
                                                            <asp:ListItem>February</asp:ListItem>
                                                            <asp:ListItem>March</asp:ListItem>
                                                            <asp:ListItem>April</asp:ListItem>
                                                            <asp:ListItem>May</asp:ListItem>
                                                            <asp:ListItem>June</asp:ListItem>
                                                            <asp:ListItem>July</asp:ListItem>
                                                            <asp:ListItem>August</asp:ListItem>
                                                            <asp:ListItem>september</asp:ListItem>
                                                            <asp:ListItem>October</asp:ListItem>
                                                            <asp:ListItem>November</asp:ListItem>
                                                            <asp:ListItem>December</asp:ListItem>
                                                        </asp:DropDownList>
                                                        
                                                    </div>
                                                </div>
                                  
                                        </div>--%>
                                   </div>
                                 
                                      
                             
                               
                                <div class="col-sm-5 ACButtons"  align="center">
                                    <asp:Button ID="btnSave" runat="server" Text="Show" class="btn btn-primary"   />
                                    <%--OnClientClick="return fileUploadValidation()"--%>
                                    <%--<asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-primary" OnClick="btnUpdate_Click" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Reset" class="btn" OnClick="btnCancel_Click" />--%>
                                </div>                            
                        </div>

                  
                </div>
            </div>
        </div>
    </div>
     
           
    <div > 
      <asp:Panel id="pnlContents" runat = "server"> 
          <asp:Panel id="Panel1" runat = "server"> 
               <div id="Div3">
        <%--<div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">Report</a><i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="EmployeeReport.aspx">PF Report</a><i class="fa fa-angle-right"></i>
                    </li>
                   
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>

                 
                   </div>--%>

                 
           <div class="row">
                <div class="col-sm-12">
                    <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                        <div class="box-title">
                            <h3>
                                <i class="fa fa-th-list"></i>PF Report</h3>
                        </div>
                         <div class="box-content">
               <div class="col-sm-12">
                        
                   <div class="col-lg-6">
                       <asp:GridView ID="grdpf" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                                                    Style="margin-bottom: 0.83%; margin-top: 0.83%; width:100%;" runat="server"
                                                                    ShowFooter="True" AutoGenerateColumns="False" DataKeyNames="Sno">
                                                                    <AlternatingRowStyle BackColor="#EEEEEE" />
                                                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                                    <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                                    <SortedDescendingHeaderStyle BackColor="#000065" />
                                                                    <Columns>
                                                                        <asp:BoundField ReadOnly="true" HeaderText="Sno" DataField="Sno" />
                                                                       
                                                                        <asp:TemplateField HeaderText="Employee Code">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("Emp_Code") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Employe Salery">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblbasicamount" runat="server" Text='<%# Eval("Basic_Amount") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="PF Amount">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("PF") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                         <asp:TemplateField HeaderText="Month">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblmonth" runat="server" Text='<%# Eval("MONTHS") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                                                                                          </Columns>
                                                                </asp:GridView>

                    </div>
                   </div>
                             </div>
         </div>
                    </div>
               </div>
          </div>
                    </asp:Panel>
           <asp:Panel id="Panel2" runat = "server"> 
               <div id="Div4">
       <%-- <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">Report</a><i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="EmployeeReport.aspx">ESI Report</a><i class="fa fa-angle-right"></i>
                    </li>
                   
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>

                 
                   </div>--%>
         <div class="row">
                <div class="col-sm-12">
                    <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                        <div class="box-title">
                            <h3>
                                <i class="fa fa-th-list"></i>ESI Report</h3>
                        </div>
                         <div class="box-content">
               <div class="col-sm-12">
                        
                   <div class="col-lg-6">
                       <asp:GridView ID="grdesi" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                                                    Style="margin-bottom: 0.83%; margin-top: 0.83%; width:100%;" runat="server"
                                                                    ShowFooter="True" AutoGenerateColumns="False" DataKeyNames="Sno">
                                                                    <AlternatingRowStyle BackColor="#EEEEEE" />
                                                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                                    <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                                    <SortedDescendingHeaderStyle BackColor="#000065" />
                                                                    <Columns>
                                                                        <asp:BoundField ReadOnly="true" HeaderText="Sno" DataField="Sno" />
                                                                       
                                                                        <asp:TemplateField HeaderText="Employee Code">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("Emp_Code") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Employe Salery">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("Basic_Amount") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="ESI Amount">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("ESI") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Month">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblmonth" runat="server" Text='<%# Eval("MONTHS") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                                                                                          </Columns>
                                                                </asp:GridView>

                    </div>
                   </div>
         </div>
                        </div>
                    </div>
             </div>
        </div>
                    </asp:Panel>
           <asp:Panel id="Panel3" runat = "server"> 
               <div id="Div5">
       <%-- <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">Report</a><i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="EmployeeReport.aspx">Salery Report</a><i class="fa fa-angle-right"></i>
                    </li>
                   
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>

                 
                   </div>--%>

                  <div class="row">
                <div class="col-sm-12">
                    <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                        <div class="box-title">
                            <h3>
                                <i class="fa fa-th-list"></i>Salary Report</h3>
                        </div>
                         <div class="box-content">
               <div class="col-sm-12">
                        
                   <div class="col-lg-6">
                       <asp:GridView ID="grdsaleryreport" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                                                    Style="margin-bottom: 0.83%; margin-top: 0.83%; width:100%;" runat="server"
                                                                    ShowFooter="True" AutoGenerateColumns="False" DataKeyNames="Sno">
                                                                    <AlternatingRowStyle BackColor="#EEEEEE" />
                                                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                                    <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                                    <SortedDescendingHeaderStyle BackColor="#000065" />
                                                                    <Columns>
                                                                        <asp:BoundField ReadOnly="true" HeaderText="Sno" DataField="Sno" />
                                                                       
                                                                        <asp:TemplateField HeaderText="Employee Code">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("Emp_Code") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Employe Salary">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("Basic_Amount") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Credited Date">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("Created_Date") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                                                                                          </Columns>
                                                                </asp:GridView>

                    </div>
                   </div>
       </div>
                        </div>
                    </div>
                      </div>
                    </asp:Panel>

           <asp:Panel id="Panel4" runat = "server"> 
               <div id="Div1">
       <%-- <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">Report</a><i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="EmployeeReport.aspx">APGLI Report</a><i class="fa fa-angle-right"></i>
                    </li>
                   
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>
                   </div>--%>

                 <div class="row">
                <div class="col-sm-12">
                    <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                        <div class="box-title">
                            <h3>
                                <i class="fa fa-th-list"></i>APGLI Report</h3>
                        </div>
                        <div class="box-content">

               <div class="col-sm-12">
                        
                   <div class="col-lg-6">
                       <asp:GridView ID="grdapgli" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                                                    Style="margin-bottom: 0.83%; margin-top: 0.83%; width:100%;" runat="server"
                                                                    ShowFooter="True" AutoGenerateColumns="False" DataKeyNames="Sno">
                                                                    <AlternatingRowStyle BackColor="#EEEEEE" />
                                                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                                    <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                                    <SortedDescendingHeaderStyle BackColor="#000065" />
                                                                    <Columns>
                                                                        <asp:BoundField ReadOnly="true" HeaderText="Sno" DataField="Sno" />
                                                                       
                                                                        <asp:TemplateField HeaderText="Employee Code">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("Emp_Code") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Employe Salery">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("Basic_Amount") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="APGLI Amount">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("APGLI") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                                                                                          </Columns>
                                                                </asp:GridView>

                    </div>
                   </div>
                            </div>
                        </div>
                    </div>
                     </div>
                   
         </div>
                    </asp:Panel>
           <asp:Panel id="Panel5" runat = "server"> 
                <div id="Div7">
       <%-- <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">Report</a><i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="EmployeeReport.aspx">GIS Report</a><i class="fa fa-angle-right"></i>
                    </li>
                   
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>

                 
                   </div>--%>

                 <div class="row">
                <div class="col-sm-12">
                    <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                        <div class="box-title">
                            <h3>
                                <i class="fa fa-th-list"></i>GIS Report</h3>
                        </div>
                        <div class="box-content">
               <div class="col-sm-12">
                        
                   <div class="col-lg-6">
                       <asp:GridView ID="grdgis" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                                                    Style="margin-bottom: 0.83%; margin-top: 0.83%; width:100%;" runat="server"
                                                                    ShowFooter="True" AutoGenerateColumns="False" DataKeyNames="Sno">
                                                                    <AlternatingRowStyle BackColor="#EEEEEE" />
                                                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                                    <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                                    <SortedDescendingHeaderStyle BackColor="#000065" />
                                                                    <Columns>
                                                                        <asp:BoundField ReadOnly="true" HeaderText="Sno" DataField="Sno" />
                                                                       
                                                                        <asp:TemplateField HeaderText="Employee Code">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("Emp_Code") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Employe Salery">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("Basic_Amount") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="GIS Amount">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("GIS") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                                                                                          </Columns>
                                                                </asp:GridView>

                    </div>
                   </div>
                            </div>
                        </div>
                    </div>
                     </div>
                    
         </div>
                    </asp:Panel>
           <asp:Panel id="Panel6" runat = "server"> 
               <div id="Div8">
   <%--  <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <%--<li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">Report</a><i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="LeaveReport.aspx">Leave Report</a><i class="fa fa-angle-right"></i>
                    </li>
                   
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>

                 
                   </div>--%>
                     <div class="row">
                <div class="col-sm-12">
                    <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                        <div class="box-title">
                            <h3>
                                <i class="fa fa-th-list"></i>Leave Report</h3>
                        </div>
                        <div class="box-content">
    <div class="col-sm-12">
                        
                   <div class="col-lg-6">
                                                <asp:GridView ID="grd_LEave_Balance" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                                    Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%;" runat="server"
                                                    ShowFooter="true" AutoGenerateColumns="False" 
                                                    AllowPaging="true" PageSize="3" AllowSorting="true"  OnRowDataBound="grd2Bind"
                                                    DataKeyNames="Emp_Code"
                                                    >
                                                    <AlternatingRowStyle BackColor="#EEEEEE" />
                                                    <FooterStyle BackColor="#EEEEEE" ForeColor="Black" />
                                                    <PagerStyle BackColor="#EEEEEE" ForeColor="Black" HorizontalAlign="Center" />
                                                    <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                    <SortedDescendingHeaderStyle BackColor="#000065" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Emp Code" SortExpression="Emp_Code">
                                                            <ItemTemplate>
                                                                <%--<asp:LinkButton ID="lblEmpCode" runat="server" Text='<%#Eval("Po_No") %>' Style="text-align: center;"
                                                                CausesValidation="false" CommandArgument='<%# Eval("Po_No")%>' CommandName="Vch_No" ></asp:LinkButton>--%>
                                                                <asp:Label ID="lblEmpCode" runat="server" Text='<%# Eval("Emp_Code")%>' Style="text-align: center;"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Emp Name" SortExpression="Emp_Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEmpName" runat="server" Text='<%# Eval("Emp_Name") %>' Style="text-align: center;"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                      <%--  <asp:TemplateField HeaderText="Department" SortExpression="Department">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("Department")%>' Style="text-align: center;"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                       <asp:TemplateField ItemStyle-Width="50%">
                                                            <HeaderTemplate>
                                                                <table class="table table-hover table-nomargin table-bordered CustomerAddress" >
                                                                    <tr>
                                                                        <td style="width: 39%;">
                                                                            Leave Name
                                                                        </td>
                                                                       <%-- <td style="">
                                                                            Leave Type
                                                                        </td>--%>
                                                                        <td style="">
                                                                           Balance Leaves
                                                                        </td>
                                                                       <%-- <td style="">
                                                                            Used Leaves
                                                                        </td>
                                                                         <td style="">
                                                                            Balance Leaves
                                                                        </td>--%>
                                                                    </tr>
                                                                </table>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:GridView ID="grd2" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                                                    Style="margin-bottom: 0.83%; margin-top: 0.83%;" runat="server"
                                                                    AutoGenerateColumns="false" ShowHeader="false">
                                                                    <AlternatingRowStyle BackColor="#EEEEEE" />
                                                    <FooterStyle BackColor="#EEEEEE" ForeColor="Black" />
                                                    <PagerStyle BackColor="#EEEEEE" ForeColor="Black" HorizontalAlign="Center" />
                                                    <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                    <SortedDescendingHeaderStyle BackColor="#000065" />
                                                                    <Columns>
                                                                        <asp:BoundField DataField="Leave_Name" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemStyle Width="39%" />
                                                                        </asp:BoundField>
                                                                        <%--<asp:BoundField DataField="Short_Name" HeaderText="Leave_Type" ItemStyle-HorizontalAlign="Right">
                                                                            <ItemStyle Width="13%" />
                                                                        </asp:BoundField>--%>
                                                                        <asp:BoundField DataField="Noof_Leavs" HeaderText="Noof_Leavs" ItemStyle-HorizontalAlign="Right">
                                                                            <ItemStyle Width="20%" />
                                                                        </asp:BoundField>
                                                                        <%--<asp:BoundField DataField="Used_Leaves" HeaderText="Used_Leaves" ItemStyle-HorizontalAlign="Right">
                                                                            <ItemStyle Width="" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Balance_Leaves" HeaderText="Balance_Leaves" ItemStyle-HorizontalAlign="Right">
                                                                            <ItemStyle Width="" />
                                                                        </asp:BoundField>--%>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="14%" />
                                                            <ItemStyle Width="50%" />
                                                        </asp:TemplateField>

                                                     
                                                    </Columns>
                                                </asp:GridView>

                    </div>
        </div>
                            </div>
                        </div>
                    </div>
                         </div>
                   
    </div>
                    </asp:Panel>
           <asp:Panel id="Panel7" runat = "server"> 
                 <div id="Div9">
   <%--  <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <%--<li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">Report</a><i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="LeaveReport.aspx">Loan Report</a><i class="fa fa-angle-right"></i>
                    </li>
                   
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>

                 
                   </div>--%>
                       <div class="row">
                <div class="col-sm-12">
                    <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                        <div class="box-title">
                            <h3>
                                <i class="fa fa-th-list"></i>Loan Report</h3>
                        </div>
                        <div class="box-content">
    <div class="col-sm-12">
                        
                   <div class="col-lg-6">
                                                <asp:GridView ID="grdloan" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                                    Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%;" runat="server"
                                                    ShowFooter="true" AutoGenerateColumns="False" 
                                                    AllowPaging="true" PageSize="3" AllowSorting="true"  OnRowDataBound="grd2Bind"
                                                    DataKeyNames="Emp_Code"
                                                    >
                                                    <AlternatingRowStyle BackColor="#EEEEEE" />
                                                    <FooterStyle BackColor="#EEEEEE" ForeColor="Black" />
                                                    <PagerStyle BackColor="#EEEEEE" ForeColor="Black" HorizontalAlign="Center" />
                                                    <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                    <SortedDescendingHeaderStyle BackColor="#000065" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Emp Code" SortExpression="Emp_Code">
                                                            <ItemTemplate>
                                                                <%--<asp:LinkButton ID="lblEmpCode" runat="server" Text='<%#Eval("Po_No") %>' Style="text-align: center;"
                                                                CausesValidation="false" CommandArgument='<%# Eval("Po_No")%>' CommandName="Vch_No" ></asp:LinkButton>--%>
                                                                <asp:Label ID="lblEmpCode" runat="server" Text='<%# Eval("Emp_Code")%>' Style="text-align: center;"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Emp Name" SortExpression="Emp_Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEmpName" runat="server" Text='<%# Eval("Emp_Name") %>' Style="text-align: center;"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                      <%--  <asp:TemplateField HeaderText="Department" SortExpression="Department">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("Department")%>' Style="text-align: center;"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                       <asp:TemplateField ItemStyle-Width="50%">
                                                            <HeaderTemplate>
                                                                <table class="table table-hover table-nomargin table-bordered CustomerAddress" >
                                                                    <tr>
                                                                        <td style="width: 39%;">
                                                                           Loan Name
                                                                        </td>
                                                                       <%-- <td style="">
                                                                            Leave Type
                                                                        </td>--%>
                                                                        <td style="">
                                                                           Basic Salery
                                                                        </td>
                                                                         <td style="">
                                                                           Loan Amount
                                                                        </td>
                                                                       <%-- <td style="">
                                                                            Used Leaves
                                                                        </td>
                                                                         <td style="">
                                                                            Balance Leaves
                                                                        </td>--%>
                                                                    </tr>
                                                                </table>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:GridView ID="grd2" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                                                    Style="margin-bottom: 0.83%; margin-top: 0.83%;" runat="server"
                                                                    AutoGenerateColumns="false" ShowHeader="false">
                                                                    <AlternatingRowStyle BackColor="#EEEEEE" />
                                                    <FooterStyle BackColor="#EEEEEE" ForeColor="Black" />
                                                    <PagerStyle BackColor="#EEEEEE" ForeColor="Black" HorizontalAlign="Center" />
                                                    <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                    <SortedDescendingHeaderStyle BackColor="#000065" />
                                                                    <Columns>
                                                                        <asp:BoundField DataField="Type_Of_Loan_Apply" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemStyle Width="39%" />
                                                                        </asp:BoundField>
                                                                        <%--<asp:BoundField DataField="Short_Name" HeaderText="Leave_Type" ItemStyle-HorizontalAlign="Right">
                                                                            <ItemStyle Width="13%" />
                                                                        </asp:BoundField>--%>
                                                                        <asp:BoundField DataField="Basic_Amount" HeaderText="Basic_Amount" ItemStyle-HorizontalAlign="Right">
                                                                            <ItemStyle Width="20%" />
                                                                        </asp:BoundField>
                                                                         <asp:BoundField DataField="Loan_amount_Required" HeaderText="Loan_amount_Required" ItemStyle-HorizontalAlign="Right">
                                                                            <ItemStyle Width="20%" />
                                                                        </asp:BoundField>
                                                                        <%--<asp:BoundField DataField="Used_Leaves" HeaderText="Used_Leaves" ItemStyle-HorizontalAlign="Right">
                                                                            <ItemStyle Width="" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Balance_Leaves" HeaderText="Balance_Leaves" ItemStyle-HorizontalAlign="Right">
                                                                            <ItemStyle Width="" />
                                                                        </asp:BoundField>--%>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="14%" />
                                                            <ItemStyle Width="50%" />
                                                        </asp:TemplateField>

                                                       <%-- <asp:TemplateField HeaderText="No Of Leaves" SortExpression="No_Of_Leaves">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNoOfLeaves" runat="server" Text='<%# Eval("Noof_Leavs")%>' Style="text-align: center;"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Leave Name" SortExpression="Leave_Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLeaveName" runat="server" Text='<%# Eval("Leave_Name")%>' Style="text-align: center;"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Leave Type" SortExpression="Leave_Type">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLeaveType" runat="server" Text='<%# Eval("Short_Name")%>' Style="text-align: center;"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Used Leaves" SortExpression="Used_Leaves">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCLUSed" runat="server" Text='<%# Eval("Used_Leaves")%>' Style="text-align: center;"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Balance Leaves" SortExpression="Balance_Leaves">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCLBalance" runat="server" Text='<%# Eval("Balance_Leaves")%>' Style="text-align: center;"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                    </Columns>
                                                </asp:GridView>

                    </div>
        </div></div>
                        </div>
                    </div>
                           </div>
                     
    </div>
                    </asp:Panel>
           <asp:Panel id="Panel8" runat = "server"> 
                 <div id="Div6">
      <%--  <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <%--<li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">Report</a><i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="EmployeeReport.aspx">Employee Report</a><i class="fa fa-angle-right"></i>
                    </li>
                   
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>

                 
                   </div>--%>

                <div class="row">
                <div class="col-sm-12">
                    <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                        <div class="box-title">
                            <h3>
                                <i class="fa fa-th-list"></i>Salary Report</h3>
                        </div>
                        <div class="box-content">
               <div class="col-sm-12">
                        
                   <div class="col-lg-6">
                       <asp:GridView ID="grdemploye" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                                                    Style="margin-bottom: 0.83%; margin-top: 0.83%; width:100%;" runat="server"
                                                                    ShowFooter="True" AutoGenerateColumns="False" DataKeyNames="Sno">
                                                                    <AlternatingRowStyle BackColor="#EEEEEE" />
                                                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                                    <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                                    <SortedDescendingHeaderStyle BackColor="#000065" />
                                                                    <Columns>
                                                                        <asp:BoundField ReadOnly="true" HeaderText="Sno" DataField="Sno" />
                                                                        <asp:TemplateField HeaderText="Employe Name">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("User_Name") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Employee Code">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("Emp_Code") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Employe Roll">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("Emp_Roll") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Date Of Birth">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label4" runat="server" Text='<%# Eval("Emp_DOB", "{0:dd/MM/yyyy}") %>' Width="100"  DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="false"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                      
                                                                        <asp:TemplateField HeaderText="Age">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label6" runat="server" Text='<%# Eval("Emp_Age") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                      
                                                                        <asp:TemplateField HeaderText="Gender">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label7" runat="server" Text='<%# Eval("Gender") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Blood Group">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label8" runat="server" Text='<%# Eval("Blood_Group") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                      
                                                                        <asp:TemplateField HeaderText="Maritel Status">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label9" runat="server" Text='<%# Eval("Marital_Status") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Bank Name">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label10" runat="server" Text='<%# Eval("Bank_Name") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Account No">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label11" runat="server" Text='<%# Eval("Account_Number") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Education">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label12" runat="server" Text='<%# Eval("Educate") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Phone No">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label13" runat="server" Text='<%# Eval("Telephone") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>

                    </div>
                   </div>
         </div>
    </div>
                    </div>
                     </div>
                     
         </div>
                    </asp:Panel>
           <asp:Panel id="Panel9" runat = "server"> 

                    </asp:Panel>
                    </asp:Panel>
        </div>
                            
            
           
                 <br />
         

    <asp:Button ID="btnPrint" runat="server" Text="Print" OnClientClick = "return PrintPanel();" />
     </body>
</asp:Content>
