﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="Increment.aspx.cs" Inherits="HRMS.Reports.Increment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="main">
        <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">Report</a><i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="Increment.aspx">Increment Report</a><i class="fa fa-angle-right"></i>
                    </li>

                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>


        </div>

        <div class="box box-bordered box-color" style="margin-bottom: 25px;">
            <div class="box-title">
                <h3>
                    <i class="fa fa-th-list"></i>Increments</h3>
            </div>
            <div class="box-content">


                <div class="row">
                    <div class="col-md-12">

                        <div class="form-group col-md-3">
                            <label for="textfield" class="control-label col-sm-3" style="">
                                Circle</label>
                            <div class="col-sm-9" style="" id="Div39">
                                <asp:DropDownList ID="ddlcircle" runat="server" class="form-control DropdownCss" AutoPostBack="true" OnSelectedIndexChanged="ddlcircle_SelectedIndexChanged">
                                    <asp:ListItem Text="---Select---" Value="Select"></asp:ListItem>
                                    <asp:ListItem Text="Adilabad" Value="Adilabad"></asp:ListItem>
                                    <asp:ListItem Text="Kawal Tiger Reserve" Value="Kawal Tiger Reserve"></asp:ListItem>
                                    <asp:ListItem Text="Kothagudem" Value="Kothagudem"></asp:ListItem>
                                    <asp:ListItem Text="Khammam" Value="Khammam"></asp:ListItem>
                                    <asp:ListItem Text="Nizamabad" Value="Nizamabad"></asp:ListItem>
                                    <asp:ListItem Text="Amrabada Tiger Reserve" Value="Amrabada Tiger Reserve"></asp:ListItem>
                                    <asp:ListItem Text="Hyderabad" Value="Hyderabad"></asp:ListItem>
                                    <asp:ListItem Text="Karimnagar" Value="Karimnagar"></asp:ListItem>
                                    <asp:ListItem Text="Warangal" Value="Warangal"></asp:ListItem>
                                    <asp:ListItem Text="Medak" Value="Medak"></asp:ListItem>
                                    <asp:ListItem Text="Mahabubnagar" Value="Mahabubnagar"></asp:ListItem>
                                    <asp:ListItem Text="Ranga Reddy" Value="Ranga Reddy"></asp:ListItem>
                                    <asp:ListItem Text="R & D Circle" Value="R & D Circle"></asp:ListItem>
                                    <asp:ListItem Text="STC Circle" Value="STC Circle"></asp:ListItem>
                                    <asp:ListItem Text="Director Zoo Parks" Value="Director Zoo Parks"></asp:ListItem>

                                </asp:DropDownList>


                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="textfield" class="control-label col-sm-3" style="">
                                District</label>
                            <div class="col-sm-9" style="" id="Div40">
                                <asp:DropDownList ID="ddldistrictforestofficers" runat="server" class="form-control DropdownCss" AutoPostBack="true" OnSelectedIndexChanged="ddldistrictforestofficers_SelectedIndexChanged">
                                    <asp:ListItem Text="---Select---" Value="---Select---"></asp:ListItem>



                                </asp:DropDownList>


                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="textfield" class="control-label col-sm-3" style="">
                                Divisions</label>
                            <div class="col-sm-9" style="" id="Div41">
                                <asp:DropDownList ID="ddlforestdivisions" runat="server" class="form-control DropdownCss" OnTextChanged="ddlforestdivisions_TextChanged" AutoPostBack="true">
                                    <asp:ListItem Text="---Select---" Value="---Select---"></asp:ListItem>

                                </asp:DropDownList>


                            </div>
                        </div>

                        <div class="form-group col-md-3" id="Div1" runat="server">
                            <label for="textfield" class="control-label col-sm-5" style="">
                                Department<span id="Span2" runat="server" style="color: Red; display: inline;">*</span></label>
                            <div class="col-sm-7" style="" id="Div2">
                                <asp:DropDownList ID="ddldepartment" runat="server" class="form-control DropdownCss" OnSelectedIndexChanged="ddldepartment_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem>---Select---</asp:ListItem>
                                    <asp:ListItem>DFO</asp:ListItem>
                                    <asp:ListItem>FDO</asp:ListItem>
                                    <asp:ListItem>PCCF</asp:ListItem>
                                </asp:DropDownList>


                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="col-sm-12">
                    <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                        <div class="box-title">
                            <h3>
                                <i class="fa fa-th-list"></i>Increment Report</h3>
                        </div>
                        <div class="box-content">
                            <div class="col-md-12 table-responsive">
                                <asp:GridView ID="grdincrementreport" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                    Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%;" runat="server"
                                    ShowFooter="True" AutoGenerateColumns="False" DataKeyNames="Sno">
                                    <AlternatingRowStyle BackColor="#EEEEEE" />
                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#000065" />
                                    <Columns>
                                        <asp:BoundField ReadOnly="true" HeaderText="Sno" DataField="Sno" />
                                        <asp:TemplateField HeaderText="Employee ID">
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("Employee_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Employee Name">
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("Employee_Name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Department">
                                            <ItemTemplate>
                                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("Name_Of_Circle") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Designation">
                                            <ItemTemplate>
                                                <asp:Label ID="Label4" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Date Of Birth">
                                            <ItemTemplate>
                                                <%--  <asp:Label ID="Label5" runat="server" Text='<%# Eval("Emp_DOB", "{0:dd/MM/yyyy}") %>'></asp:Label>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Date Of Entry Into Service">
                                            <ItemTemplate>
                                                <asp:Label ID="Label6" runat="server" Text='<%# Eval("Date_Of_entry_Service", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date of Retirement">
                                            <ItemTemplate>
                                                <asp:Label ID="Label7" runat="server" Text='<%# Eval("Date_retirement_death", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Due Date OF Increment">
                                            <ItemTemplate>
                                                <asp:Label ID="Label8" runat="server" Text='<%# Eval("Due_date_increment", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Reason For Not Released">
                                            <ItemTemplate>
                                                <asp:Label ID="Label10" runat="server" Text='<%# Eval("due_leave") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Due Date OF Chenge OF Increments">
                                            <ItemTemplate>
                                                <asp:Label ID="Label11" runat="server" Text='<%# Eval("eol") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Details On Sanction of AAS Incremenet">
                                            <ItemTemplate>
                                                <asp:Label ID="Label12" runat="server" Text='<%# Eval("sgp") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Name Of the employee Added/Deleted">
                                            <ItemTemplate>
                                                <asp:Label ID="Label13" runat="server" Text='<%# Eval("transfer") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Remarks">
                                            <ItemTemplate>
                                                <asp:Label ID="Label14" runat="server" Text='<%# Eval("death") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>


</asp:Content>
