﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="Retirement_Report.aspx.cs" Inherits="HRMS.Reports.Retirement_Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">Report</a><i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="Increment.aspx">Retirement Report</a><i class="fa fa-angle-right"></i>
                    </li>

                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>


        </div>

        <div class="box box-bordered box-color" style="margin-bottom: 25px;">
            <div class="box-title">
                <h3>
                    <i class="fa fa-th-list"></i>Retirement</h3>
            </div>
            <div class="box-content">

                <div class="col-md-12">
                    <div class="form-group col-md-6">
                        <label for="textfield" class="control-label col-sm-5 text-left ">
                            Emp Code
                        </label>
                        <div class="col-sm-7 " style="">
                            <asp:TextBox ID="txtEmpCode" runat="server" placeholder="Emp Code" MaxLength="50"
                                class="form-control"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="textfield" class="control-label col-sm-5" style="">
                            Employee Name<span id="Span1" runat="server" style="color: Red; display: inline;">*</span></label>
                        <div class="col-sm-7 " style="" id="">
                            <asp:DropDownList ID="ddlEmpName" runat="server" class="form-control DropdownCss" TabIndex="1" AutoPostBack="True" OnSelectedIndexChanged="ddlEmpName_SelectedIndexChanged">
                            </asp:DropDownList>

                        </div>
                    </div>


                    <div class="form-group col-md-6">
                        <label for="textfield" class="control-label col-md-5" style="">
                            Form</label>
                        <div class="col-md-7" style="" id="Div8">
                            <asp:TextBox ID="txtfrom" runat="server" placeholder="Ex:dd-MM-YYYY" class="form-control"
                                MaxLength="50"></asp:TextBox>
                            <ajaxToolKit:CalendarExtender ID="calendarDOB" PopupButtonID="imgPopup" runat="server"
                                TargetControlID="txtfrom" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                            </ajaxToolKit:CalendarExtender>
                        </div>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="textfield" class="control-label col-md-5" style="">
                            To</label>
                        <div class="col-md-7" style="" id="Div1">
                            <asp:TextBox ID="txtto" runat="server" placeholder="Ex:dd-MM-YYYY" class="form-control"
                                MaxLength="50"></asp:TextBox>
                            <ajaxToolKit:CalendarExtender ID="CalendarExtender1" PopupButtonID="imgPopup" runat="server"
                                TargetControlID="txtto" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                            </ajaxToolKit:CalendarExtender>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" align="center">
                    <asp:Button ID="btnSave" runat="server" Text="Search" class="btn btn-primary" />

                </div>

                </div>

                <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                    <div class="box-title">
                        <h3>
                            <i class="fa fa-th-list"></i>Retirement Report</h3>
                    </div>
                    <div class="box-content">
                        <div class="col-md-12 table-responsive">
                            <asp:GridView ID="grdincrementreport" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%;" runat="server"
                                ShowFooter="True" AutoGenerateColumns="False" DataKeyNames="Sno">
                                <AlternatingRowStyle BackColor="#EEEEEE" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#000065" />
                                <Columns>
                                    <asp:BoundField ReadOnly="true" HeaderText="Sno" DataField="Sno" />
                                    <asp:TemplateField HeaderText="Employee ID">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("Emp_Code") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee Name">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("User_Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Department">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("User_Role") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Circle">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("circle") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="District">
                                        <ItemTemplate>
                                            <asp:Label ID="Label15" runat="server" Text='<%# Eval("district") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Divisions">
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Eval("Divisions") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Date Of Birth">
                                        <ItemTemplate>
                                              <asp:Label ID="Label5" runat="server" Text='<%# Eval("date_of_birth", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                  <%--  <asp:TemplateField HeaderText="Date Of Entry Into Service">
                                        <ItemTemplate>
                                            <%-- <asp:Label ID="Label6" runat="server" Text='<%# Eval("Date_Of_entry_Service", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>--%>
                                   

                                    <asp:TemplateField HeaderText="Date Of Retirement">
                                        <ItemTemplate>
                                          <%--  <asp:Label ID="Label8" runat="server" Text='<%# Eval("Date_retirement_death", "{0:dd/MM/yyyy}") %>'></asp:Label>--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   
                                  
                                   
                                    
                                   
                                 
                                  
                                </Columns>
                            </asp:GridView>

                        </div>
                    </div>
                </div>
          



            </div>

            
        </div>
 

</asp:Content>
