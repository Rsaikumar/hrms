﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="EmployeReport.aspx.cs" Inherits="HRMS.Payroll.EmployeReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            text-align: left;
            color: #0066FF;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">Report</a><i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="EmployeeReport.aspx">Employee Report</a><i class="fa fa-angle-right"></i>
                    </li>

                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>


        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                    <div class="box-title">
                        <h3>
                            <i class="fa fa-th-list"></i>Employee  Summary</h3>
                    </div>
                    <div class="col-sm-12">

                        <div class="col-lg-6">
                            <asp:GridView ID="grdleaves" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%;" runat="server"
                                ShowFooter="True" AutoGenerateColumns="False" DataKeyNames="Sno">
                                <AlternatingRowStyle BackColor="#EEEEEE" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#000065" />
                                <Columns>
                                    <asp:BoundField ReadOnly="true" HeaderText="Sno" DataField="Sno" />
                                    <asp:TemplateField HeaderText="Employee ID">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee Name">
                                        <ItemTemplate>
                                          <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employe Roll">
                                        <ItemTemplate>
                                           <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date Of Birth">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Age">
                                        <ItemTemplate>
                                          <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Gender">
                                        <ItemTemplate>
                                          <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Blood Group">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Maritel Status">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Bank Name">
                                        <ItemTemplate>
                                           <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Account No">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Education">
                                        <ItemTemplate>
                                           <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Phone No">
                                        <ItemTemplate>
                                          <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</asp:Content>
