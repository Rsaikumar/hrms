﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="services_records.aspx.cs" Inherits="HRMS.Reports.services_records" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx">
                        <%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i></li>
                    <li><a href="">Employee</a><i class="fa fa-angle-right"></i> </li>
                    <%--<li><a href="Leave_Application_List.aspx">Leave Application Details</a> <i class="fa fa-angle-right"></i></li>--%>
                    <li><a href="">service records</a> </li>
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="box box-bordered box-color" style="margin-bottom: 25px;">
            <div class="box-title">
                <h3>
                    <i class="fa fa-th-list"></i>Service Records</h3>
            </div>
            <div class="box-content">
                <form action="#" method="POST" class='form-horizontal form-bordered'>
                    <div class="col-md-12" style="">
                        <div class="form-group col-md-6">
                            <label for="textfield" class="control-label col-sm-5" style="">
                                Emp Code
                            </label>
                            <div class="col-sm-7" style="">
                                <asp:TextBox ID="txtEmpCode" runat="server" placeholder="Emp Code" MaxLength="50"
                                    class="form-control" AutoPostBack="true" OnTextChanged="txtEmpCode_TextChanged"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="textfield" class="control-label col-sm-5" style="">
                                Employee Name<span id="Span1" runat="server" style="color: Red; display: inline;">*</span></label>
                            <div class="col-sm-7" style="" id="">
                                <asp:DropDownList ID="ddlEmpName" runat="server" class="form-control DropdownCss" TabIndex="1" AutoPostBack="True" OnSelectedIndexChanged="ddlEmpName_SelectedIndexChanged">
                                </asp:DropDownList>

                            </div>
                        </div>
                    </div>

                    <div class="col-sm-5" align="center">
                        <asp:Button ID="btnleave" runat="server" Text="Leave's" class="btn btn-primary" OnClick="btnleave_Click" />

                    </div>
                    <div class="col-sm-5 " align="center">
                        <asp:Button ID="btnincerments" runat="server" Text="Increments's" class="btn btn-darkblue" OnClick="btnincerments_Click" />
                    </div>
                </form>
            </div>
        </div>
    </div>
    <asp:Panel ID="pnlleave" runat="server">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                    <div class="box-title">
                        <h3>
                            <i class="fa fa-th-list"></i>Leaves</h3>
                    </div>
                    <div class="box-content">

                        <div align="center">
                            <asp:GridView ID="grdleave" class="table table-bordered CustomerAddress" runat="server" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:TemplateField HeaderText="Date">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("LeaveFrom","{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Leave Type">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("TypeOfLeaveApple") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="No Of Leaves">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("Noof_Days") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Eval("Leave_Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approved By">
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Eval("Modified_By") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Balance Leaves">
                                        <ItemTemplate>
                                            <asp:Label ID="Label6" runat="server" Text='<%# Eval("Balances_Available") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </asp:Panel>


    <br />
    <br />

    <asp:Panel ID="pnlincrement" runat="server">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                    <div class="box-title">
                        <h3>
                            <i class="fa fa-th-list"></i>Increment</h3>
                    </div>
                    <div class="box-content">
                        <div align="center">
                            <asp:GridView ID="grdincrement" class="table table-bordered CustomerAddress" runat="server" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:TemplateField HeaderText="Date">
                                        <ItemTemplate>
                                            <asp:Label ID="Label7" runat="server" Text='<%# Eval("Created_Date","{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Designation">
                                        <ItemTemplate>
                                            <asp:Label ID="Label8" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Pay_Scale">
                                        <ItemTemplate>
                                            <asp:Label ID="Label9" runat="server" Text='<%# Eval("pay_scale") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Basic_Salary">
                                        <ItemTemplate>
                                            <asp:Label ID="Label10" runat="server" Text='<%# Eval("Basic_salary") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approved_By">
                                        <ItemTemplate>
                                            <asp:Label ID="Label11" runat="server" Text='<%# Eval("Approved_By") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </asp:Panel>
</asp:Content>
