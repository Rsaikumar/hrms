﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="monthlypayslip.aspx.cs" Inherits="HRMS.Reports.monthlypayslip" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
        .auto-style1 {
        }
        .auto-style2 {
            width: 130px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
   <%-- <script language="javascript">
        window.print();
</script>--%>
    <script type = "text/javascript">
        function PrintPanel() {
            var panel = document.getElementById("<%=pnlContents.ClientID %>");
            var printWindow = window.open('', '', 'height=200,width=2000');
            printWindow.document.write('<html><head><title>DIV Contents</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
            return false;
        }
    </script>

</head>
        <body>            
     <div id="main">
        <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx">
                        <%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i></li>
                    <li><a href="">Employee</a><i class="fa fa-angle-right"></i> </li>
                   
                    <li><a href="">PaySlips</a> </li>
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>
            </div>
         </div>
            <div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                        <div class="box-title">
                            <h3>
                                <i class="fa fa-th-list"></i>PaySlips</h3>
                        </div>
                        <div class="box-content">
                            <form action="#" method="POST" class='form-horizontal form-bordered'>
                                <div class="col-sm-12" style="">
                                    
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lAEmpCode" style="">
                                                Emp Code
                                            </label>
                                            <div class="col-sm-2 tAEmpCode" style="">
                                                <asp:TextBox ID="txtEmpCode" runat="server" placeholder="Emp Code" MaxLength="50"
                                                    class="form-control"  AutoPostBack="true" OnTextChanged="txtEmpCode_TextChanged"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-2 lALeaveName" style="">
                                                        Employee Name<span id="Span1" runat="server" style="color: Red; display: inline;">*</span></label>
                                                    <div class="col-sm-2 tAEmpName" style="" id="">
                                                        <asp:DropDownList ID="ddlEmpName" runat="server"  class="form-control DropdownCss"  TabIndex="1" AutoPostBack="True" OnSelectedIndexChanged="ddlEmpName_SelectedIndexChanged" 
                                                            >
                                                          
                                                          
                                                        </asp:DropDownList>
                                                        
                                                    </div>
                                                </div>
                                    <div class="col-sm-12" style="">
                                    <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lALeaveName" style="">
                                                       Month<span id="Span2" runat="server" style="color: Red; display: inline;">*</span></label>
                                                    <div class="col-sm-2 tAEmpName" style="" id="Div1">
                                                        <asp:DropDownList ID="ddlmonth" runat="server"  class="form-control DropdownCss"  TabIndex="1" AutoPostBack="True" OnSelectedIndexChanged="ddlEmpName_SelectedIndexChanged" 
                                                            >
                                                            <asp:ListItem>---select---</asp:ListItem>
                                                            <asp:ListItem>January</asp:ListItem>
                                                            <asp:ListItem>February</asp:ListItem>
                                                            <asp:ListItem>March</asp:ListItem>
                                                            <asp:ListItem>April</asp:ListItem>
                                                            <asp:ListItem>May</asp:ListItem>
                                                            <asp:ListItem>June</asp:ListItem>
                                                            <asp:ListItem>July</asp:ListItem>
                                                            <asp:ListItem>August</asp:ListItem>
                                                            <asp:ListItem>september</asp:ListItem>
                                                            <asp:ListItem>October</asp:ListItem>
                                                            <asp:ListItem>November</asp:ListItem>
                                                            <asp:ListItem>December</asp:ListItem>
                                                        </asp:DropDownList>
                                                        
                                                    </div>
                                                </div>
                                    <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-2 lALeaveName" style="">
                                                        Year<span id="Span3" runat="server" style="color: Red; display: inline;">*</span></label>
                                                    <div class="col-sm-2 tAEmpName" style="" id="Div2">
                                                        <asp:DropDownList ID="ddlyear" runat="server"  class="form-control DropdownCss"  TabIndex="1" AutoPostBack="True" OnSelectedIndexChanged="ddlEmpName_SelectedIndexChanged" 
                                                            >
                                                            <asp:ListItem>---select---</asp:ListItem>
                                                            <asp:ListItem>2016</asp:ListItem>
                                                            <asp:ListItem>2015</asp:ListItem>
                                                            <asp:ListItem>2014</asp:ListItem>
                                                          
                                                          
                                                        </asp:DropDownList>
                                                        
                                                    </div>
                                                </div>
                                        </div>
                                   </div>
                                 
                                      
                             
                               
                                <div class="col-sm-5 ACButtons"  align="center">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary" OnClick="btnSave_Click"  />
                                    <%--OnClientClick="return fileUploadValidation()"--%>
                                    <%--<asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-primary" OnClick="btnUpdate_Click" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Reset" class="btn" OnClick="btnCancel_Click" />--%>
                                </div>                            
                        </div>

                  
                </div>
            </div>
        </div>
    </div>
     
           <div class="row">
                <div class="col-sm-12">
                    <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                        <div class="box-title">
                            <h3>
                                <i class="fa fa-th-list"></i><asp:Label ID="lblmonth" runat="server" Text="" ></asp:Label>,<asp:Label ID="lblyear" runat="server" Text=""></asp:Label></h3>
                        </div>
                        <div class="box-content">
    <div > 
      <asp:Panel id="pnlContents" runat = "server"> 
            <div>
                <div>
                  
                <table>
                    <tr>
                        <td class="auto-style1"> 
                            Emp_Code:
                        </td>
                        <td>
                             <asp:Label ID="lblempcode" runat="server" Text=""></asp:Label>
                        </td>
                        <td>

                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                        <td>

                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                        <td>
                            Employee Name:
                        </td>
                        <td>
                            <asp:Label ID="lblempname" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td> 
                            GPF NO :
                        </td>
                        <td>
                            <asp:Label ID="lblgpfno" runat="server" Text=""></asp:Label>
                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                        <td>
                           Designation :
                        </td>
                        <td>
                            <asp:Label ID="lbldesignation" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td> 
                           APGLI NO :
                        </td>
                        <td>
                            <asp:Label ID="lblapglino" runat="server" Text=""></asp:Label>
                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                        <td>
                          PAN NO :
                        </td>
                        <td>
                            <asp:Label ID="lblpanno" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                      <tr>
                        <td> 
                           Bank A/C NO :
                        </td>
                        <td>
                           
                            <asp:Label ID="lblbankacno" runat="server" Text=""></asp:Label>
                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                        <td>
                         Scale:
                        </td>
                        <td>
                            <asp:Label ID="lblscale" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td> 
                           Bank Name :
                        </td>
                        <td>
                           
                            <asp:Label ID="Label3" runat="server" Text="Andhra Bank"></asp:Label>
                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                        <td>
                        
                        </td>
                        <td>
                            
                        </td>
                    </tr>   
                </table>
                </div>

               <div>
                 <hr />
                
                <table>
                    <tr>
                        <td>
                            EARNINGS
                        </td>
                         <td>

                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                        <td>

                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</td>
                        <td>
                            DEDUCTIONS
                        </td>
                         <td>

                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;</td>
                        <td>

                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                        <td>
                            RECOVERIES
                        </td>
                         <td>

                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                        <td>

                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                        <td>
                            DETAILS
                        </td>
                    </tr>
                </table>
                <hr />
                </div>
                <div>
                    <table>
                         <tr>
                        <td> 
                           Basic Pay :
                        </td>
                        <td>
                            <asp:Label ID="lblbasicpay" runat="server" Text='<%# Eval("Basic") %>'></asp:Label>
                        </td>
                        <td>

                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                        <td>

                        </td>
                        <td>
                          APGLI(S) :
                        </td>
                        <td>
                            <asp:Label ID="lblapglis" runat="server" Text='<%# Eval("APGLI")%>'></asp:Label>
                        </td>
                             <td>

                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                        <td>

                        </td>
                             <td>
                                 HBA(P) :
                             </td>
                             <td>
                                 <asp:Label ID="lblhba" runat="server" Text=""></asp:Label>
                             </td>
                    </tr>
                          <tr>
                        <td> 
                          DA :
                        </td>
                        <td>
                            <asp:Label ID="lblda" runat="server"></asp:Label>
                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                        <td>
                         GIS :
                        </td>
                        <td>
                            <asp:Label ID="lblgis" runat="server"></asp:Label>
                        </td>
                             <td>

                        </td>
                        <td>

                        </td>
                             <td>
                                
                            GPFIV_LOAN :
                             </td>
                             <td>
                                 <asp:Label ID="lblgpfivloan" runat="server" Text=""></asp:Label>
                             </td>
                    </tr>
                         <tr>
                        <td> 
                         HRA :
                        </td>
                        <td>
                            <asp:Label ID="lblhra" runat="server" Text='<%# Eval("HRA")%>'></asp:Label>
                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                        <td>
                         PT :
                        </td>
                        <td>
                            <asp:Label ID="lblpt" runat="server" Text='<%# Eval("PTAX")%>'></asp:Label>
                        </td>
                    </tr>
                         <tr>
                        <td> 
                        CCA :
                        </td>
                        <td>
                          
                             <asp:Label ID="lblcca" runat="server" Text='<%# Eval("CCA")%>'></asp:Label>
                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                        <td>
                        GPF 4 :
                        </td>
                        <td>
                           <asp:Label ID="lblgpf4" runat="server"></asp:Label>
                        </td>
                    </tr>
                    </table>
                </div>
                <div>
                    <hr />
                    <table>
                         <tr>
                        <td>
                            GROSSEARNINGS:
                        </td>
                         <td>
                             <asp:Label ID="lblgrossamount" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td>

                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                        <td>
                            DEDUCTIONS
                        </td>
                         <td>

                        </td>
                        <td>

                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                        <td>
                            RECOVERIES
                        </td>
                         <td>

                        </td>
                        <td>

                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                        <td>
                            NET
                        </td>
                             <td>

                        </td>
                        <td>

                            &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                        <td class="auto-style2">
                            NETPAYABLE:
                        </td>
                    </tr>
                        
                    </table>
                    <hr />

                </div>
                <div>
                    <table>
                         <tr>
                        <td> 
                           TOKEN NO:
                        </td>
                        <td>
                            <asp:Label ID="lbltokenno" runat="server" Text=" "></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                        <td>

                        </td>
                        <td>

                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                        <td>
                          Bill Gross Amt Rs :
                        </td>
                        <td>
                           <asp:Label ID="lblbillgrossamount" runat="server" Text=" "></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                             <td>

                        </td>
                        <td>

                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                             <td>
                                 BILL DATE :
                             </td>
                             <td>
                                 <asp:Label ID="lblbilldate" runat="server" Text=""></asp:Label>
                             </td>
                    </tr>
                               <tr>
                        <td> 
                          TOKEN DATE:
                        </td>
                        <td>
                            <asp:Label ID="lbltokendatae" runat="server" Text=" "></asp:Label>
                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                        <td>
                          Bill Net Amt Rs :
                        </td>
                        <td>
                            <asp:Label ID="lblbnars" runat="server" Text=" "></asp:Label>
                        </td>
                             <td>

                        </td>
                        <td>

                        </td>
                             <td>
                                 Deduct Amt Rs :
                             </td>
                             <td>
                                 <asp:Label ID="lbldeductamtrs" runat="server" Text=""></asp:Label>
                             </td>
                    </tr>
                          </table>
                    <hr />
                </div>
            </div>
          </asp:Panel>
        </div>
                            </div>
                        </div>
                    </div>
                </div>
            
           
                 <br />
           <%-- <asp:button id="BtnPrint" runat="server" text="Print" onclientclick="javascript:CallPrint('bill');" xmlns:asp="#unknown" />--%>
            <%--<input type="button" value="Print" onclick="window.open('PaySlips.aspx')" />--%>

    <asp:Button ID="btnPrint" runat="server" Text="Print" OnClientClick = "return PrintPanel();" />
     </body>
</asp:Content>
