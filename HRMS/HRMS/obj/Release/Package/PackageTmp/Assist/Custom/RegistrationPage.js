﻿$(document).ready(function myfunction() {

    $("#<%= txtFirstName.ClientID %>").keypress(function myfunction(event) {

        var regex = new RegExp("^[0-9 ]$");

        var textLength = $("#<%= txtFirstName.ClientID %>").val().length + 1;

        var key = String.fromCharCode(event.charCode ? event.which : event.charCode);

        if (textLength <= 30) {

            if (!regex.test(key)) {

                event.preventDefault();

                return false;

            }

        } else {

            return false;

        }



    });

});
