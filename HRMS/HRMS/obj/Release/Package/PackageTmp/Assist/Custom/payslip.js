﻿var PayslipPage = (function () {


    var BindEvents = function () {

        $('#btnsearch').on('click', function () {
            var templates = loadTemplates('Kendotemp', ['DEMO']);
            var department = $("#ddldepartnemt").val();
            var month = $("#ddlmonth").val();
            $(function () {
                $.ajax({
                    type: "POST",
                    url: "DepartmentWisePayslip.aspx/AnalysisReportData",
                    data: JSON.stringify({
                        depid: +department,
                        month: +month
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (obj) {
                        var lds = obj;
                        var Getdata = lds;
                        BIndData(Getdata);
                    }
                });
            });


        });
    }
    var init = function () {
        BindEvents();

    };
    return {
        init: init,
        //AddOperations: AddOperations
    }
}());




$(document).ready(function () {
    PayslipPage.init();
});