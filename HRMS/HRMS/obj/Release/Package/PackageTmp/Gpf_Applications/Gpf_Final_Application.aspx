﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="Gpf_Final_Application.aspx.cs" Inherits="HRMS.Gpf_Applications.Gpf_Final_Application" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script type="text/javascript">
         function PrintPanel() {
             var panel = document.getElementById("<%=pnlcontents.ClientID %>");
            var printWindow = window.open('', '', 'height=200,width=2000');
            printWindow.document.write('<html><head><title>DIV Contents</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
        <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="Gpf_Final_Application.aspx" class="current"><i class="fa fa-home"></i>FinalApplication</a> </li>
                    </ul>
                    <div class="pull-right pull-module m-bot20" style="">
                    </div>
                </div>
            </div>
            <div class="row">
                <section class="panel" id="pnlcontents" runat="server">
                    <header class="panel-heading">
                        <i class="fa fa-user">Final Application Form</i>
                    </header>
                    <div class="panel-body">
                        <div>
                            <div align="center">
                                <h4>Form of application for Final Payment of General Provident Fund Balance</h4>
                                <h5>(Retirement / Resignation / Removal / Transfer of Balance or Death Cases)</h5>
                                <br />
                                <h5>To be filled in by the Applicant</h5>
                            </div>
                            <div class="">
                                <div align="left">
                                    To<br />
                                    The Accountant Genral(A&E)<br />
                                    Andhra Pradesh & Telangana,<br />
                                    Hyderabad.
                                </div>
                                <div align="center">
                                    <h5>(Through the Head of the office in case of Non-Gazetted and through Head of the Department in case of Gazetted Officer)</h5>
                                </div>
                                <div class="row">
                                    <div class="col-lg-1">
                                        1.
                                    </div>
                                    <div class="col-lg-5">
                                        Name Of the Subscriber<br />
                                        (In capital letters)
                                    </div>
                                    <div class="col-lg-1">
                                        :
                                    </div>
                                    <div class="col-lg-5 form-group">
                                        <asp:TextBox ID="txtnameofsubscriber" CssClass="form-control" runat="server" required></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-1">
                                        2.
                                    </div>
                                    <div class="col-lg-5">
                                        Date Of Birth
                                    </div>
                                    <div class="col-lg-1">
                                        :
                                    </div>
                                    <div class="col-lg-5 form-group">
                                        <asp:TextBox ID="txtdateofbirth" CssClass="form-control" runat="server" required></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender1" PopupButtonID="imgPopup" runat="server" TargetControlID="txtdateofbirth" Format="dd-MM-yyyy" PopupPosition="BottomLeft"></ajaxToolKit:CalendarExtender>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-1">
                                        3.
                                    </div>
                                    <div class="col-lg-5">
                                        Designation and Office to which attached
                                    </div>
                                    <div class="col-lg-1">
                                        :
                                    </div>
                                    <div class="col-lg-5 form-group">
                                        <asp:TextBox ID="txtdesignationandoffice" CssClass="form-control" runat="server" required></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-1">
                                        4.
                                    </div>
                                    <div class="col-lg-5">
                                        G.P.F. Account No. with Departmental suffix
                                    </div>
                                    <div class="col-lg-1">
                                        :
                                    </div>
                                    <div class="col-lg-5 form-group">
                                        <asp:TextBox ID="txtgpfaccountno" CssClass="form-control" runat="server" required></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-1">
                                        5.
                                    </div>
                                    <div class="col-lg-5">
                                        Residential Address of the claimant
                                    </div>
                                    <div class="col-lg-1">
                                        :
                                    </div>
                                    <div class="col-lg-5 form-group">
                                        <asp:TextBox ID="txtresidentialaddress" CssClass="form-control" runat="server" required></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-1">
                                        6.
                                    </div>
                                    <div class="col-lg-5">
                                        Copy of the latest Account slip is enclosed
                                    </div>
                                    <div class="col-lg-1">
                                        :
                                    </div>
                                    <div class="col-lg-5 form-group">
                                        <asp:TextBox ID="txtslipenclosed" CssClass="form-control" runat="server" required></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-1">
                                        7.
                                    </div>
                                    <div class="col-lg-5">
                                        (i)Date of Retirement OR
                                            <br />
                                        (ii)Date of Resignation  OR<br />
                                        (iii)Date of Voluntary Retirement  OR<br />
                                        (iv)Date of Dismissal / Removal / Compulsory  Retirement / Invalidation<br />
                                    </div>
                                    <div class="col-lg-1">
                                        :
                                    </div>
                                    <div class="col-lg-5 form-group">
                                        <asp:TextBox ID="txtdateofretirement" CssClass="form-control" runat="server" required></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender2" PopupButtonID="imgPopup" runat="server" TargetControlID="txtdateofretirement" Format="dd-MM-yyyy" PopupPosition="BottomLeft"></ajaxToolKit:CalendarExtender>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-1">
                                        8.
                                    </div>
                                    <div class="col-lg-5">
                                        Particulars of office worked during the last 3 years:
                                    </div>
                                    <div class="col-lg-1">
                                        :
                                    </div>
                                    <div class="col-lg-5 form-group">
                                        <asp:TextBox ID="txtworkrdyears" CssClass="form-control" runat="server" required></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-1">
                                        9.
                                    </div>
                                    <div class="col-lg-5">
                                        Office / Treasury at which payment is desired
                                    </div>
                                    <div class="col-lg-1">
                                        :
                                    </div>
                                    <div class="col-lg-5 form-group">
                                        <asp:TextBox ID="txttreasury" CssClass="form-control" runat="server" required></asp:TextBox>
                                    </div>payment
                                </div>
                                <div class="row">
                                    <div class="col-lg-1">
                                        10.
                                    </div>
                                    <div class="col-lg-5">
                                        If payment is desired out-side the place of last duty enclose the following documents
                                    </div>
                                    <div class="col-lg-1">
                                        :
                                    </div>
                                    <div class="col-lg-5 form-group">
                                        <asp:TextBox ID="txtdocuments" CssClass="form-control" runat="server" required></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-1">
                                    </div>
                                    <div class="col-lg-5">
                                        a)  Personal marks of identification
                                    </div>
                                    <div class="col-lg-1">
                                        :
                                    </div>
                                    <div class="col-lg-5 form-group">
                                        <asp:TextBox ID="txtpersonalmarks" CssClass="form-control" runat="server" required></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-1">
                                    </div>
                                    <div class="col-lg-5">
                                        b) Two specimen signatures
                                    </div>
                                    <div class="col-lg-1">
                                        :
                                    </div>
                                    <div class="col-lg-5 form-group">
                                        <asp:TextBox ID="txttwospecimen" CssClass="form-control" runat="server" required></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-1">
                                    </div>
                                    <div class="col-lg-5">
                                        c) Left/Right hand thumb impression (in case of illiterate claimants)
                                    </div>
                                    <div class="col-lg-1">
                                        :
                                    </div>
                                    <div class="col-lg-5 form-group">
                                        <asp:TextBox ID="txtthumbimpression" CssClass="form-control" runat="server" required></asp:TextBox>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-1">
                                        11.
                                    </div>
                                    <div>
                                        Certificates
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-lg-1">
                                        (i)
                                    </div>
                                    <div class="col-lg-8">
                                        I have not resigned from Government service to take up appointment in another Department of State Government / Central Government or under a Body Corporate owned or controlled by the State or Central Government.
                                    </div>

                                </div>
                                <br />

                                <div class="row">
                                    <div class="col-lg-1">
                                    </div>
                                    <div class="col-lg-8">
                                        Note:- This Certificate is to be furnished only by a subscriber who resigned from Government service. 
                                            <br />
                                        If resigned to take up appointment elsewhere, the information regarding transfer of balance may be given in the form prescribed in the Annexure.
                                    </div>

                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-lg-1">
                                        (ii)
                                    </div>
                                    <div class="col-lg-8">
                                        I hereby undertake that no appeal shall be preferred by me against my dismissal / removal / compulsory retirement / invalidation.
                <br />
                                        (This certificate is to be furnished only in case of dismissal / removal / compulsory retirement / invalidation).
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-1">
                                        (iii)
                                    </div>
                                    <div class="col-lg-8">
                                        I hereby undertake to refund any excess payment arising out of clerical error in the settlement of G.P.F. claim.
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-1">
                                        12.
                                    </div>
                                    <div class="col-lg-5">
                                        In case of death the following particulars may be furnished
                                    </div>
                                    <div class="col-lg-1">
                                        :
                                    </div>
                                    <div class="col-lg-5 form-group">
                                        <asp:TextBox ID="txtcaseofdeath" CssClass="form-control" runat="server" required></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-1">
                                    </div>
                                    <div class="col-lg-5">
                                        a) Date of death
                                    </div>
                                    <div class="col-lg-1">
                                        :
                                    </div>
                                    <div class="col-lg-5 form-group">
                                        <asp:TextBox ID="txtdateofdeath" CssClass="form-control" runat="server" required></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender3" PopupButtonID="imgPopup" runat="server" TargetControlID="txtdateofdeath" Format="dd-MM-yyyy" PopupPosition="BottomLeft"></ajaxToolKit:CalendarExtender>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-1">
                                    </div>
                                    <div class="col-lg-5">
                                        (b) Religion of deceased Government Servant
                                    </div>
                                    <div class="col-lg-1">
                                        :
                                    </div>
                                    <div class="col-lg-5 form-group">
                                        <asp:TextBox ID="txtreligionofdeceased" CssClass="form-control" runat="server" required></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-1">
                                    </div>
                                    <div class="col-lg-8">
                                        c) Details of the surviving member of the family on the date of death of the subscriber are furnished below:
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-lg-1">
                                        place :
                                    </div>
                                    <div class="col-lg-5 form-group">
                                        <asp:TextBox ID="txtplace" CssClass="form-control" runat="server" required></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-1">
                                        Date :
                                    </div>
                                    <div class="col-lg-5 form-group">
                                        <asp:TextBox ID="txtdate" CssClass="form-control" runat="server" required></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender4" PopupButtonID="imgPopup" runat="server" TargetControlID="txtdate" Format="dd-MM-yyyy" PopupPosition="BottomLeft"></ajaxToolKit:CalendarExtender>
                                    </div>
                                </div>


                                <div align="right">
                                    Signature of the Subscriber/Claimant. 
                                </div>

                            </div>
                        </div>
                        <div class="">
                            <div align="center">
                                <h5>For the use of Head of the Office / Head of the Department</h5>
                            </div>
                            <br />
                            <div class="container">
                                <div align="center">The Final withdrawal application is forwarded to the Accountant-General, Andhra Pradesh, Hyderabad for authorizing the balance.  </div>
                            </div>
                            <br />
                            <br />
                            <div class="row">
                                <div class="col-lg-1">
                                    13.
                                </div>
                                <div class="col-lg-11">
                                    Certified that all the particulars furnished above have been verified with reference to office records and are found correct.
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-lg-1">
                                    14.
                                </div>
                                <div class="col-lg-11">
                                    The last fund deduction was made from his / her pay for the month of                                        
                                    <asp:TextBox ID="txtpaymonthof" CssClass="form-group" runat="server" required></asp:TextBox>
                                    vide this office Bill No.                                    
                                    <asp:TextBox ID="txtofficebillno" CssClass="form-group" runat="server" required></asp:TextBox>
                                    dated                                   
                                    <asp:TextBox ID="txtdated" CssClass="form-group" runat="server" required></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender5" PopupButtonID="imgPopup" runat="server" TargetControlID="txtdated" Format="dd-MM-yyyy" PopupPosition="BottomLeft"></ajaxToolKit:CalendarExtender>
                                    for Rs.                                   
                                    <asp:TextBox ID="txtforrupee" CssClass="form-group" runat="server" required></asp:TextBox>
                                    (Rupees                                   
                                    <asp:TextBox ID="txtrupees" CssClass="form-group" runat="server" required></asp:TextBox>
                                    only) cash voucher No.                                    
                                    <asp:TextBox ID="txtcashvoucherno" CssClass="form-group" runat="server" required></asp:TextBox>
                                    of                                    
                                    <asp:TextBox ID="txtofname" CssClass="form-group" runat="server" required></asp:TextBox>
                                    Treasury, the amount of deduction towards G.P.F. subscription being Rs.                                    
                                    <asp:TextBox ID="txtgpfsubscription" CssClass="form-group" runat="server" required></asp:TextBox>
                                    and recovery on account of refund of advance Rs.                                   
                                    <asp:TextBox ID="txtrefundadvance" CssClass="form-group" runat="server" required></asp:TextBox>
                                    .
                                </div>

                            </div>
                            <br />
                            <div class="row">
                                <div class="col-lg-1">
                                    15.
                                </div>
                                <div class="col-lg-11">
                                    Details of G.P.F. deduction made from the subscriber’s salary during the last 12 months immediately preceding the date of retirement (in the proforma appended to G.O.Ms.No.216, dated 4-6-1986) are enclosed.
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-lg-1">
                                    16.
                                </div>
                                <div class="col-lg-11">
                                    Certified that he / she was neither sanctioned any temporary advances nor any part-final withdrawal from his / her provident fund account during the 12 months immediately preceding the date of his / her quitting service / proceeding on leave preparatory to retirement or thereafter, OR
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-lg-1">
                                    17.
                                </div>
                                <div class="col-lg-11">
                                    Certified that the following temporary advance part-final withdrawals were sanctioned to him/her and drawn from his/her provident fund account during the 12 months immediately preceding the date of his/her quitting service/proceeding on leave preparatory to retirement or thereafter.
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-lg-1">
                                </div>
                                <div class="col-lg-4">
                                    Amount of advance / part-final withdrawal
                                </div>
                                <div class="col-lg-3">
                                    Date
                                </div>
                                <div class="col-lg-4">
                                    Voucher No.
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-lg-1">
                                    1.
                                </div>
                                <div class="col-lg-4 form-group">
                                    <asp:TextBox ID="txtamtpartfinali" runat="server" CssClass="form-control" required> </asp:TextBox>
                                </div>
                                <div class="col-lg-3 form-group">
                                    <asp:TextBox ID="txtamtpartfinalidate" runat="server" CssClass="form-control" required></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender7" PopupButtonID="imgPopup" runat="server" TargetControlID="txtamtpartfinalidate" Format="dd-MM-yyyy" PopupPosition="BottomLeft"></ajaxToolKit:CalendarExtender>
                                </div>
                                <div class="col-lg-4 form-group">
                                    <asp:TextBox ID="txtamtpartfinalivoucherno" runat="server" CssClass="form-control" required></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-lg-1">
                                    2.
                                </div>
                                <div class="col-lg-4 form-group">
                                    <asp:TextBox ID="txtamtpartfinalii" runat="server" CssClass="form-control" required></asp:TextBox>
                                </div>
                                <div class="col-lg-3 form-group">
                                    <asp:TextBox ID="txtamtpartfinaliidate" runat="server" CssClass="form-control" required></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender6" PopupButtonID="imgPopup" runat="server" TargetControlID="txtamtpartfinaliidate" Format="dd-MM-yyyy" PopupPosition="BottomLeft"></ajaxToolKit:CalendarExtender>
                                </div>
                                <div class="col-lg-4 form-group">
                                    <asp:TextBox ID="txtamtpartfinaliivoucherno" runat="server" CssClass="form-control" required></asp:TextBox>
                                </div>
                            </div>
                            <br />

                            <div class="row">
                                <div class="col-lg-1">
                                    18.
                                </div>
                                <div class="col-lg-11">
                                    Certified that no amount was withdrawn / the following amounts were withdrawn from his/her provident fund account during the 12 months immediately preceding the date of his/her quitting service/proceeding on leave preparatory to retirement or thereafter for payment of insurance premia or for the purchase of a new policy.
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-11">
                                    1. Policy No. and name of Insurance Company<br />
                                    2. Sum Assured<br />
                                    3. Particulars of premia paid from G.P.F<br />
                                </div>
                            </div>
                            <div align="right">Yours faithful</div>
                            <div class="row">
                                <div class="col-lg-1">
                                    Sation :
                                </div>
                                <div class="col-lg-5 form-group">
                                    <asp:TextBox ID="txtstation" CssClass="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>
                            <div align="right">
                                ( Signature )<br />
                                with Date and Designation<br />
                                with Postal Address
                            </div>
                        </div>
                        <div class="">
                            <div align="center">
                                <h5>STATEMENT SHOWING DEDUCTIONS FROM G.P.F</h5>
                            </div>
                            <div class="row">
                                <div>
                                    In respect of Sri<br />
                                    S/o.<br />
                                    Account No.<br />
                                    Office of the
                                        <br />
                                </div>
                            </div>
                            <div>
                                <table style="border: thick">
                                    <tr>
                                        <td>Sl.No.</td>
                                        <td>Bill No.</td>
                                        <td>Month & Year</td>
                                        <td>Token No.</td>
                                        <td>Date</td>
                                        <td>Cheque No.</td>
                                        <td>Date</td>
                                        <td>Amt Rs.</td>
                                        <td>Remarks</td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>7</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>8</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>9</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>11</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="">
                            <div align="center">
                                <h4>GENERAL PROVIDENT FUND NOTING OF THE DATE OF BIRTH OF SUBSCRIBERS IN THE GENERAL PROVIDENT FUND LEDGER CARDS / FOLIOS</h4>
                                <h5>Amendment as per G.O.Ms.No.92 Finance and Planning (Fin.Wing.Pen.1) Dated 1st March, 1976.</h5>
                            </div>
                            <table>
                                <tr>
                                    <td>Sl. No.</td>
                                    <td>Name Of the Subscriber</td>
                                    <td>Designation</td>
                                    <td>G.P.F.C.P.F A/c.No. with Dept suffix</td>
                                    <td>Date of Birth</td>
                                    <td>Date of superannuntion</td>
                                    <td>Whether nomination filed or not</td>
                                    <td>REMARKS</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>2</td>
                                    <td>3</td>
                                    <td>4</td>
                                    <td>5</td>
                                    <td>6</td>
                                    <td>7</td>
                                    <td>8</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                            <br />
                            <br />
                            <div align="right">
                                Signature of the Subscriber
                            </div>

                        </div>
                    </div>
                       <div class="col-md-12 text-center" >
                            <asp:Button ID="btnPrint" runat="server" Text="Print" OnClientClick="return PrintPanel();" />
                        <asp:Button ID="btnsave" runat="server" Text="Submit" OnClick="btnsave_Click" /> 
                    </div>
                </section>
            </div>
        </section>
    </section>

</asp:Content>
