﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="CompanyInformation.aspx.cs" Inherits="HRMS.Administrator.CompanyInformation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<script type="text/javascript" language="javascript">
        function Search() {
            if (document.getElementById('<%=txtSearch.ClientID%>').value == "") {
                alert("Please Enter Search Keys for Company Infomation");
                document.getElementById('<%=txtSearch.ClientID%>').focus();
                return false;
            }
            return true;
        }

    </script>--%>
    <script type="text/javascript">
        function SelectedObjRptPopUp(url) {
            window.location = url;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">

            <%--<div class="page-header">
                <div class="pull-left Search_Div" id="btnSearch_Div">
                    <div class="Search_Icon">
                        <asp:TextBox ID="txtSearch" runat="server" placeholder="Search For Company Information.."
                            class="form-control txtSearch"></asp:TextBox>
                    </div>
                </div>
                <div class="pull-left">
                    <asp:Button ID="btnSearch" OnClientClick="return Search()" runat="server" class="search_Btn" OnClick="btnSearch_Click" />
                </div>

                <div class="pull-right">
                    <ul class="stats">
                        <li>
                            <asp:Button ID="btnAdd" runat="server" class="add_Btn" OnClick="btnAdd_Click" ToolTip="Create Company Information Details" />
                        </li>
                        <li>
                            <asp:Button ID="btnPdf" runat="server" class="PDF_Btn" ToolTip="Export Company Information To Pdf" /></li>
                        <li>
                            <asp:Button ID="btnExcel" runat="server" class="Excel_Btn" ToolTip="Export Company Information To Excel" /></li>
                    </ul>
                </div>
            </div>--%>

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="#">Administrator</a><i class="fa fa-angle-right"></i> </li>
                        <li><a href="CompanyInformation.aspx">Company Information List</a> </li>
                        <li><a href="AddCompanyInfomation.aspx" class="current">Company Information </a></li>
                    </ul>
                </div>
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-md-12">
                            <section class="panel">
                                <header class="panel-heading">
                                    <i class="fa fa-th-list"> Company Information</i>
                                </header>
                                <div class="panel-body">
                                    <form action="#" class='form-horizontal form-bordered'>
                                        <div class="col-sm-12" style="margin-bottom: 0.3%;">
                                            <div class="row">
                                                <asp:GridView ID="grdCompanyInformation" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                                    Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%;" runat="server"
                                                    AutoGenerateColumns="false" DataKeyNames="ClientRegistrationNo" EmptyDataText="No Records Found"
                                                    ShowFooter="false" AllowPaging="true"
                                                    PageSize="15" AllowSorting="true"
                                                    OnRowCommand="grdUserRegistration_RowCommand"
                                                    OnSorting="grdUserRegistration_Sorting"
                                                    OnPageIndexChanging="grdUserRegistration_PageIndexChanging"
                                                    OnRowDeleting="grdUserRegistration_RowDeleting"
                                                    OnRowEditing="grdUserRegistration_RowEditing">
                                                    <AlternatingRowStyle BackColor="#EEEEEE" />
                                                    <PagerStyle BackColor="#EEEEEE" ForeColor="Black" HorizontalAlign="Center" />
                                                    <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                    <SortedDescendingHeaderStyle BackColor="#000065" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="S.No" SortExpression="">
                                                            <ItemTemplate>
                                                                <%#Container.DataItemIndex+1 %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Company Id">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblcompanyid" runat="server" placeholder="Company ID" Text='<%# Eval("ClientRegistrationNo")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Company Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblcompanyname" runat="server" placeholder="Company Name" Text='<%# Eval("Company_Name")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtcompanyname" runat="server" placeholder="Company Name" Text='<%# Eval("Company_Name")%>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="State">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblstate" runat="server" placeholder="State" Text='<%# Eval("State")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtstate" runat="server" placeholder="State" Text='<%# Eval("State")%>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="PIN Code">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblpincode" runat="server" placeholder="PIN Code" Text='<%# Eval("Pincode")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtpincode" runat="server" placeholder="PIN Code" Text='<%# Eval("Pincode")%>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="E-Mail">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblemail" runat="server" placeholder="E Mail" Text='<%# Eval("E_Mail")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtemail" runat="server" placeholder="E Mail" Text='<%# Eval("E_Mail")%>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Web URL">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblweburl" runat="server" placeholder="Web URL" Text='<%# Eval("Web_URL")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtweburl" runat="server" placeholder="Web URL" Text='<%# Eval("Web_URL")%>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="TIN No">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbltinno" runat="server" placeholder="TIN NO" Text='<%# Eval("TIN_No")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txttinno" runat="server" placeholder="TIN NO" Text='<%# Eval("TIN_No")%>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lbkEdit" runat="server" CommandName="Edit" Style="color: gray; text-decoration: none; float: left; background-image: url('../img/file_edit.png'); height: 30px; width: 35px; background-position: left; background-repeat: no-repeat;"
                                                                    OnClientClick="return confirm('Are you sure you want to Edit this Record ...?')" CausesValidation="false"
                                                                    CommandArgument='<%#Eval("ClientRegistrationNo") %>'>

                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%-- <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete" Style="color: gray;
                                                    text-decoration: none; float: left; background-image: url('../img/delete.png');
                                                    height: 30px; width: 35px; background-position: left; background-repeat: no-repeat;"
                                                    OnClientClick="return confirm('Are you sure you want to Remove this Record ...?')" CausesValidation="false"
                                                    CommandArgument='<%#Eval("ClientRegistrationNo") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </section>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </section>
    </section>
</asp:Content>
