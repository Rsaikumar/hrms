﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="SalaryDeductions.aspx.cs" Inherits="HRMS.Administrator.SalaryDeductions" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="../Payroll/Leave_Application.aspx" class="current">Salary Deductions</a> </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <i class="fa fa-th-list">Salary Deductions</i>
                        </header>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-md-1" style="">
                                        PF
                                    </label>
                                    <div class="col-md-5" style="">
                                        <asp:TextBox ID="txtpf" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                    <label id="lblda" class="control-label col-md-1">DA</label>
                                    <div class="col-md-5">
                                        <asp:TextBox ID="txtda" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label id="lblhra" class="control-label col-md-1">HRA</label>
                                    <div class="col-md-5">
                                        <asp:TextBox ID="txthra" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <label id="lblcca" class="control-label col-md-1">CCA</label>
                                    <div class="col-md-5">
                                        <asp:TextBox ID="txtcca" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label id="lbleesi" class="control-label col-md-1">EESI</label>
                                    <div class="col-md-5">
                                        <asp:TextBox ID="txteesi" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <label id="lblesi" class="control-label col-md-1">ESI</label>
                                    <div class="col-md-5">
                                        <asp:TextBox ID="txtesi" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 text-center">
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-primary" OnClick="btnSubmit_Click" />
                            </div>
                            </div>
                        </div>
                </div>
                    </section>
        </section>
</asp:Content>
