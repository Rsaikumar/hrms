﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/Admin.Master" AutoEventWireup="true" CodeBehind="LeaveList.aspx.cs" Inherits="HRMS.Administrator.LeaveList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
        <section class="wrapper">

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="../Administrator/EmployeeList.aspx" class="current">Leave List</a> </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <i class="fa fa-th-list">Leave List</i>
                        </header>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div style="max-height: 515px; overflow: auto;" class="table-responsive">
                                    <asp:GridView ID="gdvleaveapproved" CssClass="mydatagrid" HeaderStyle-CssClass="headergrid" RowStyle-CssClass="rows"
                                        runat="server" Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%; overflow: scroll;" AutoGenerateColumns="false">
                                        <Columns>
                                           <asp:BoundField DataField="ID" HeaderText="Leave No" ItemStyle-Width="150">
                                                            <ItemStyle Width="150px"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="empname" HeaderText="Employee Name" ItemStyle-Width="150">
                                                            <ItemStyle Width="150px"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="leavetype" HeaderText="Leave Type" ItemStyle-Width="200">
                                                            <ItemStyle Width="200px"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="fromdate" HeaderText="From Date" DataFormatString="{0:d}" ItemStyle-Width="200">
                                                            <ItemStyle Width="200px"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="todate" HeaderText="To Date" DataFormatString="{0:d}" ItemStyle-Width="200">
                                                            <ItemStyle Width="200px"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="totaldays" HeaderText="No Of Days" ItemStyle-Width="200">
                                                            <ItemStyle Width="200px"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="LeaveStatus" HeaderText="Status" ItemStyle-Width="200">
                                                            <ItemStyle Width="200px"></ItemStyle>
                                                        </asp:BoundField>

                                        </Columns>
                                    </asp:GridView>
                                </div>

                            </div>
                            <div class="col-md-12 text-center">
                                <asp:Button ID="btncancel" runat="server" CssClass=" btn btn-primary" Text="Back" OnClick="btncancel_Click" />
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>

</asp:Content>
