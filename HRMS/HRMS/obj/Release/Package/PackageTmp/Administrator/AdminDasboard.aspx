﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/Admin.Master" AutoEventWireup="true" CodeBehind="AdminDasboard.aspx.cs" Inherits="HRMS.Administrator.AdminDasboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="Dashboard.aspx" class="current"><i class="fa fa-home"></i>Dashboard</a> </li>
                    </ul>
                    <div class="pull-right pull-module m-bot20" style="">
                        <i class="fa fa-calendar padd-r-1"><span class="big badge bg-important" id="MDate"></span><span id="weekT"></span></i>
                    </div>
                </div>
            </div>
            <div class="row">
              
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="panel">
                            <div class="panel-body" style="background:#e0e4f9">
                                <a href="../Administrator/EmployeeList.aspx">
                                    <div class="info-box vd_bg-red">
                                        <i class="fa fa-flag"></i>
                                        <div class="count"><h2><span id="spnempCount" runat="server" class="pull-center" style="color:blue"></span></h2></div>
                                        <div class="boxtitle"><h3>List Of Employees</h3></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                      <div class="col-md-4">
                        <div class="panel">
                            <div class="panel-body" style="background:#e0e4f9">
                                <a href="../Administrator/LoanList.aspx">
                                    <div class="info-box vd_bg-red">
                                        <i class="fa fa-list"></i>
                                        <div class="count"><h2><label id="spnloan" runat="server" class="pull-center" style="color:blue" onclick=""></label></h2></div>
                                        <div class="boxtitle"><h3>List Of Loans</h3></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                     <div class="col-md-4">
                        <div class="panel">
                            <div class="panel-body" style="background:#e0e4f9">
                                <a href="../Administrator/LeaveList.aspx">
                                    <div class="info-box vd_bg-red">
                                        <i class="fa fa-bell"></i>
                                        <div class="count"><h2><span id="spnleavcount" runat="server" class="pull-center" style="color:blue"></span></h2></div>
                                        <div class="boxtitle"><h3>List Of Leaves</h3></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
    <script type="text/javascript">
        tday = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
        tmonth = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

        function GetClock() {
            var d = new Date();
            var nday = d.getDay(), nmonth = d.getMonth(), ndate = d.getDate(), nyear = d.getYear();
            if (nyear < 1000) nyear += 1900;
            var nhour = d.getHours(), nmin = d.getMinutes(), nsec = d.getSeconds(), ap;

            if (nhour == 0) { ap = " AM"; nhour = 12; }
            else if (nhour < 12) { ap = " AM"; }
            else if (nhour == 12) { ap = " PM"; }
            else if (nhour > 12) { ap = " PM"; nhour -= 12; }

            if (nmin <= 9) nmin = "0" + nmin;
            if (nsec <= 9) nsec = "0" + nsec;

            document.getElementById('MDate').innerHTML = "" + tmonth[nmonth] + " " + ndate + ", " + nyear + "";
            document.getElementById('weekT').innerHTML = "" + tday[nday] + "," + nhour + ":" + nmin + ":" + nsec + ap + "";
        }

        window.onload = function () {
            GetClock();
            setInterval(GetClock, 1000);
        }
    </script>
</asp:Content>
