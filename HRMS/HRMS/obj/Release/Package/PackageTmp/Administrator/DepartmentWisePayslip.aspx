﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/Admin.Master" AutoEventWireup="true" CodeBehind="DepartmentWisePayslip.aspx.cs" Inherits="HRMS.Administrator.DepartmentWisePayslip" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Layout/Scripts/Custom/DepartmentPayslip.js"></script>
    <style>
        label.margin-pm {
            margin: 10px;
        }

        td.bck-prvn {
            background-color: #cfe3f5;
        }
    </style>
    <script type="text/javascript">
        function PrintPanel() {
            var panel = document.getElementById("<%=dvtableprint.ClientID %>");
            var printWindow = window.open('', '', 'height=200,width=2000');
            printWindow.document.write('<html><head><title></title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
            return false;
        }
    </script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../DashBoard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="#">Employee</a><i class="fa fa-angle-right"></i> </li>
                        <li><a href="../Employee/MyProfile.aspx" class="current">My Profile</a> </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <i class="fa fa-user">Employee PaySlips</i>
                        </header>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="col-md-6 form-group">
                                    <div class="col-md-4" id="departmentid">
                                        <asp:Label ID="Label3" runat="server" Text="Label">Department</asp:Label>
                                    </div>
                                    <div class="col-md-8">

                                        <asp:DropDownList ID="ddldepartnemt" runat="server" class="form-control"
                                            AutoPostBack="true" required>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label1" runat="server" Text="Label">Month</asp:Label>
                                    </div>
                                    <div class="col-md-8">
                                        
                                        <asp:DropDownList ID="ddlmonth" runat="server" class="form-control"
                                            AutoPostBack="true">
                                            <asp:ListItem Text="Select Month" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="January" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="February" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="March" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="April" Value="4"></asp:ListItem>
                                            <asp:ListItem Text="May" Value="5"></asp:ListItem>
                                            <asp:ListItem Text="June" Value="6"></asp:ListItem>
                                            <asp:ListItem Text="July" Value="7"></asp:ListItem>
                                            <asp:ListItem Text="August" Value="8"></asp:ListItem>
                                            <asp:ListItem Text="september" Value="9"></asp:ListItem>
                                            <asp:ListItem Text="October" Value="10"></asp:ListItem>
                                            <asp:ListItem Text="November" Value="11"></asp:ListItem>
                                            <asp:ListItem Text="December" Value="12"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-12 text-center">
                                    <input type="button" id="btnsearch"class="btn btn-primary" value="Search" />
                                   <%-- <asp:Button CssClass="btn btn-primary" ID="btnsearch" Text="Search" runat="server" />--%>
                                    <asp:Button CssClass="btn btn-primary" ID="btnBacktoDashboard" Text="Back" runat="server" />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="dvtableprint" runat="server" class="col-md-12">
                                    <div id="demo"></div>
                                </div>
                                <div class="col-md-12 text-center">
                                    <asp:Button ID="btnprint" runat="server" Text="Print" OnClientClick="PrintPanel()" />
                                </div>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
        </section>
    </section>
    <script type="text/javascript">
        $(document).ready(function () {

        });

      
    </script>
</asp:Content>
