﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/Admin.Master" AutoEventWireup="true" CodeBehind="AttendanceListView.aspx.cs" Inherits="HRMS.Administrator.AttendanceListView" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="#">Attendance</a><i class="fa fa-angle-right"></i> </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <div class="panel-body">
                            <div class="tab-content">
                                <div id="Attendance" class="tab-pane active">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <section class="panel">
                                                    <header class="panel-heading">
                                                        <i class="fa fa-th-list">Attendance List</i>
                                                    </header>
                                                    <div style="max-height: 515px; overflow: auto;" class="table-responsive">
                                                        <asp:GridView ID="gvdAttendancelist" CssClass="mydatagrid" HeaderStyle-CssClass="headergrid" RowStyle-CssClass="rows"
                                                            runat="server" Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%; overflow: scroll;" EmptyDataText="No records Found" AutoGenerateColumns="false">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Emp Id">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblempid" runat="server" Text='<%#Eval("EmpId") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Name">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblname" runat="server" Text='<%#Eval("EmpName") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Designation">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbldesignation" runat="server" Text='<%#Eval("Designation") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Status">

                                                                    <ItemTemplate>

                                                                        <asp:DropDownList ID="ddlstatus" runat="server">
                                                                            <asp:ListItem Value="1">Present</asp:ListItem>
                                                                            <asp:ListItem Value="2">Absent</asp:ListItem>
                                                                            <asp:ListItem Value="3">Sick Leave</asp:ListItem>

                                                                        </asp:DropDownList>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </section>
                                                <div class="col-md-12 text-center">
                                                    <asp:Button ID="btnsave" runat="server" Text="Update" OnClick="btnsave_Click" CssClass="btn btn-primary" />
                                                    <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="btnBack_Click" CssClass="btn btn-primary" />

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
