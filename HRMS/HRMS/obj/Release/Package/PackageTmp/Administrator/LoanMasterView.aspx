﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/Admin.Master" AutoEventWireup="true" CodeBehind="LoanMasterView.aspx.cs" Inherits="HRMS.Administrator.LoanMasterView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.js"></script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <section id="main-content">
        <section class="wrapper">

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a></li>
                        <li><a href="LoanMasterView.aspx" class="current">Define Loan Types </a></li>
                    </ul>
                </div>
            </div>


            <div class="row">
                <section class="panel">
                    <header class="panel-heading">
                        <%--<i class="fa fa-th-list"> Define Loan Types </i>--%>
                    </header>

                    <div class="panel-body">
                        <form action="#" method="POST" class='form-horizontal form-bordered'>
                            <div class="col-md-12">
                                <div class="row">
                                    <section class="panel">

                                        <div class="panel-body">
                                            <div class="tab-content">

                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                    <ContentTemplate>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-md-5">
                                                                Loan Name<span id="Span1" runat="server" style="color: Red; display: inline;">*</span></label>
                                                            <div class="col-md-7 ">
                                                                <asp:TextBox ID="txtLoanName" runat="server" TabIndex="0" placeholder="Loan Name" class="form-control"
                                                                    MaxLength="50" AutoPostBack="true" required></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-md-5">
                                                                Short Name<span style="color: #ff0000; font-size: 14px; display: inline;">*</span></label>
                                                            <div class="col-md-7" id="Div1">
                                                                <asp:TextBox ID="txtShortName" runat="server" TabIndex="1" placeholder="Short Name" class="form-control"
                                                                    MaxLength="50" AutoPostBack="true" required></asp:TextBox>
                                                            </div>
                                                        </div>

                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-md-5">
                                                        Tenure<span style="color: #ff0000; font-size: 14px; display: inline;">*</span>
                                                    </label>
                                                    <div class="col-md-7">
                                                        <asp:DropDownList ID="ddltenure" runat="server" class="form-control DropdownCss" required>
                                                            <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                            <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                            <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                            <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                            <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                                            <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                                            <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                                            <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                                            <asp:ListItem Text="24" Value="24"></asp:ListItem>

                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                   <div class="form-group col-md-6">
                                                        <span class="col-md-7" margin-bottom="15px" style="display: inline-block; width: 700%;">
                                                            <label for="ContentPlaceHolder1_chbdesignationlist"></label>
                                                        </span>
                                                        <asp:CheckBox ID="chkdesignation" CssClass="col-md-7" OnCheckedChanged="chkdesignation_CheckedChanged" runat="server"
                                                            Text=" Apply to Designation" AutoPostBack="true" />
                                                    </div>
                                             
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-md-5" id="laleid" runat="server">
                                                        Elgibility Amount<span style="color: #ff0000; font-size: 14px; display: inline;">*</span></label>
                                                    <div class="col-md-7" id="Div2">
                                                        <asp:TextBox ID="txtelgibityamunt" runat="server" TabIndex="1" placeholder="  Elgibility Amount" class="form-control"
                                                            MaxLength="50" OnTextChanged="txtelgibityamunt_TextChanged" AutoPostBack="true" required></asp:TextBox>
                                                    </div>
                                                </div>

                                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                    <ContentTemplate>
                                                        <div class="col-md-12">
                                                            <div class="box-body table-responsive text-center ">
                                                                <div class=" form-group ">
                                                                    <asp:GridView ID="gdvdesignationlist" runat="server" AutoGenerateColumns="false" Visible="false" CssClass="mydatagrid" HeaderStyle-CssClass="headergrid" RowStyle-CssClass="rows">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="">
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="chbboxdesignation" runat="server" CssClass="" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="DesignationName" HeaderText="Designation Name" ItemStyle-Width="150" />

                                                                            <asp:TemplateField HeaderText="Elgibility Amount">
                                                                                <ItemTemplate>
                                                                                     <asp:TextBox ID="txtelgibityamountlist" CssClass="form-control" runat="server" Text='<%# Eval("ElgibilityAmount") %>'>
                                                                                                </asp:TextBox>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>

                                                <div class="col-md-12 text-center">
                                                    <asp:Button ID="btnSave" runat="server" Text="Update" class="btn btn-primary" OnClick="btnSave_Click" />
                                                    <asp:Button ID="btndelete" runat="server" Text="Delete" class="btn btn-danger" OnClick="btndelete_Click" OnClientClick="javascript:return confirm('Are you sure you want to delete this class?');" /><%--OnClientClick="return fileUploadValidation()"--%>
                                                    <asp:Button ID="btncancle" runat="server" Text="Cancel" class="btn btn-primary" OnClick="btncancle_Click" /><%--OnClientClick="return fileUploadValidation()"--%>
                                                </div>

                                            </div>

                                        </div>
                                    </section>

                                </div>
                            </div>
                        </form>


                    </div>
                </section>
            </div>
        </section>
    </section>
</asp:Content>
