﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true"
    CodeBehind="AddCompanyInfomation.aspx.cs" Inherits="HRMS.Administrator.AddCompanyInfomation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/Administrator/CompanyInfo.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
        function Customer() {
            if (document.getElementById('<%=txtCompanyname.ClientID%>').value == "") {
                alert("Please Enter Company Name");
                document.getElementById('<%=txtCompanyname.ClientID%>').focus();
                return false;
            } //txtFollowupDate

            if (document.getElementById('<%=txtState.ClientID%>').value == "") {
                alert("Please Enter The State Name");
                document.getElementById('<%=txtState.ClientID%>').focus();
                return false;
            }
            if (document.getElementById('<%=txtCity.ClientID%>').value == "") {
                alert("Please Enter The City Name");
                document.getElementById('<%=txtCity.ClientID%>').focus();
                return false;
            }

            ////  This Is Related To Pin Code Checking
            var Tel = document.getElementById("<%=txtPinCode.ClientID%>");

            if (Tel.value == "") {
                alert("Enter Pincode");
                Tel.focus();
                return false;
            }
            debugger;
            var numPat = /[^0-9]/;
            if (numPat.test(Tel.value)) {
                alert("Pincode must contain only digits");
                Tel.focus();
                return false;
            }
            Tel.value = parseInt(Tel.value);
            var emailPat = /^[\s\S]{6,6}$/;
            var emailid = Tel.value;
            var matchArray = emailPat.test(emailid);
            if (matchArray == false) {
                alert("Pincode allows only digits (6 digits)");
                Tel.focus();
                return false;
            }
            return true;
        }

        // // Checking Tin no
        function TinNo(Phone) {
            var Tel = document.getElementById("<%=txtTINNO.ClientID%>");

            if (document.getElementById("<%=txtTINNO.ClientID%>").value == "") {
                alert("Enter Valid Tin Number");
                document.getElementById("<%=txtTINNO.ClientID%>").value = "";
                document.getElementById("<%=txtTINNO.ClientID%>").focus();
                return false;
            }

            if (document.getElementById("<%=txtTINNO.ClientID%>").value.length == 11) {

                return false;
            }
            else {

                alert("Tin Number must contain 11 digits");

                document.getElementById("<%=txtTINNO.ClientID%>").value = "";
                document.getElementById("<%=txtTINNO.ClientID%>").focus();
                return false;
            }
            return true;
        }

        // // Checking Email ID
        function checkEmail(emailID) {

            var email = emailID;

            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

            if (!filter.test(email.value)) {
                alert('Please provide a valid email address');
                email.focus();
                email.value = "";
                return false;
            }
            else {

                return true;
            }
            return true;
        }
    </script>
    <script language="Javascript" type="text/javascript">

        function onlyAlphabets(e, t) {
            try {
                if (window.event) {
                    var charCode = window.event.keyCode;
                }
                else if (e) {
                    var charCode = e.which;
                }
                else { return true; }
                if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
                    return true;
                else
                    return false;
                alert('Please Enter Alphbates Only');
            }
            catch (err) {
                alert(err.Description);
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right">
                    </i></li>
                    <li>
                        <a href="">Administrator</a><i class="fa fa-angle-right"></i>
                    </li>
                    <li><a href="CompanyInformation.aspx">Company Information List</a> <i class="fa fa-angle-right">
                    </i></li>
                    <li><a href="AddCompanyInfomation.aspx">Company Information</a> </li>
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                                <div class="box-title">
                                    <h3>
                                        <i class="fa fa-th-list"></i>Company Info</h3>
                                </div>
                                <div class="box-content">
                                    <form action="#" method="POST" class='form-horizontal form-bordered'>
                                    <div class="col-sm-12" style="">
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lACompanyId" style="">
                                                Company Id</label>
                                            <div class="col-sm-2 tACompanyId" style="">
                                                <asp:TextBox ID="txtCompanyid" runat="server" placeholder="Company Id" MaxLength="10"
                                                 AutoPostBack="true"    class="form-control" ReadOnly="false" OnTextChanged="txtCompanyid_TextChanged"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lACompanyName" style="">
                                                Company Name<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                            <div class="col-sm-5 tACompanyname" style="" id="">
                                                <asp:TextBox ID="txtCompanyname" runat="server" placeholder="Company Name" class="form-control"
                                               AutoPostBack="true" OnTextChanged="txtCompanyname_TextChanged"      MaxLength="50"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="">
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lAAddress1" style="" id="">
                                                Address 1<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                            <div class="col-sm-2 tAAddress1" style="" id="">
                                                <asp:TextBox ID="txtAddress1" runat="server" placeholder="Address1" class="form-control"
                                                    MaxLength="200" TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lAAddress2" style="" id="">
                                                Address2</label>
                                            <div class="col-sm-2 tAAddress2" style="" id="">
                                                <asp:TextBox ID="txtAddress2" runat="server" placeholder="Address2" class="form-control"
                                                    MaxLength="200" TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lACity" style="" id="">
                                                City<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                            <div class="col-sm-2 tACity" style="" id="">
                                                <asp:TextBox ID="txtCity" runat="server" placeholder="City" class="form-control"
                                                    MaxLength="25"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="">
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lAState" style="" id="">
                                                State<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                            <div class="col-sm-2 tAState" style="" id="">
                                                <asp:TextBox ID="txtState" runat="server" placeholder="State" class="form-control"
                                                    MaxLength="25"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lAPinCode" style="" id="">
                                                Pin Code<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                            <div class="col-sm-2 tAPinCode" style="" id="">
                                                <asp:TextBox ID="txtPinCode" runat="server" placeholder="Pin Code" class="form-control"
                                                    MaxLength="6"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lAPhone" style="" id="">
                                                Phone</label>
                                            <div class="col-sm-2 tAPhone" style="" id="">
                                                <asp:TextBox ID="txtPhone" runat="server" placeholder="Phone Number" class="form-control"
                                                    MaxLength="25"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="">
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lAEmail" style="" id="Label1">
                                                Fax No</label>
                                            <div class="col-sm-2 tAEmail" style="" id="Div1">
                                                <asp:TextBox ID="txtFaxNO" runat="server" placeholder="Fax No" class="form-control"
                                                    MaxLength="25"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lAEmail" style="" id="">
                                                Email</label>
                                            <div class="col-sm-2 tAEmail" style="" id="">
                                                <asp:TextBox ID="txtEmail" runat="server" placeholder="Email ID" class="form-control"
                                                    MaxLength="35" onchange="return checkEmail(this);"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lAWebURL" style="" id="">
                                                Website</label>
                                            <div class="col-sm-2 tAWebURL" style="" id="">
                                                <asp:TextBox ID="txtWeburl" runat="server" placeholder="WEB URL" class="form-control"
                                                    MaxLength="25"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="">
                                        <asp:Panel ID="Panel1" runat="server" GroupingText="Applicable Statutory and Regulatory Requirements" Style="border-bottom: 0px solid #8F8F8F;">
                                            <div class="col-sm-12" style="">
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lAPFCode" style="" id="">
                                                        PF Code<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                    <div class="col-sm-2 tAPFCode" style="" id="">
                                                        <asp:TextBox ID="txtPFCode" runat="server" placeholder="PF Code" class="form-control"
                                                            MaxLength="25"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lAPFRegion" style="" id="">
                                                        PF Region</label>
                                                    <div class="col-sm-2 tAPFRegion" style="" id="">
                                                        <asp:DropDownList ID="ddlPFRegion" runat="server" class="form-control DropdownCss">
                                                            <asp:ListItem Text="Select" Value="Select PF Region"></asp:ListItem>
                                                            <asp:ListItem Text="GUNTUR" Value="GUNTUR"></asp:ListItem>
<asp:ListItem Text="CUDAPA" Value="CUDAPA"></asp:ListItem>
<asp:ListItem Text="RAJAHMUNDRY" Value="RAJAHMUNDRY"></asp:ListItem>
<asp:ListItem Text="VISAKHAPATNAM" Value="VISAKHAPATNAM"></asp:ListItem>
                                                            <asp:ListItem Text="HYDERABAD" Value="HYDERABAD"></asp:ListItem>
                                                           
                                                               <asp:ListItem Text="NIZAMABAD" Value="NIZAMABAD"></asp:ListItem>
                                                               <asp:ListItem Text="KARIMNAGAR" Value="KARIMNAGAR"></asp:ListItem>
                                                               <asp:ListItem Text="WARANGAL" Value="WARANGAL"></asp:ListItem>
                                                             
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lAPFOffice" style="" id="">
                                                        PF Office</label>
                                                    <div class="col-sm-2 tAPFOffice" style="" id="">
                                                        <asp:DropDownList ID="ddlPFOffice" runat="server" class="form-control DropdownCss">
                                                            <asp:ListItem Text="Select" Value="Select PF Office"></asp:ListItem>
                                                            <asp:ListItem Text="GUNTUR DISTRICT" Value="GUNTUR DISTRICT"></asp:ListItem>
                                                            <asp:ListItem Text="PRAKASAM DISTRICT" Value="PRAKASAM DISTRICT"></asp:ListItem>
                                                            <asp:ListItem Text="KRISHNA DISTRICT" Value="KRISHNA DISTRICT"></asp:ListItem>
                                                            <asp:ListItem Text="ANANTAPUR" Value="ANANTAPUR"></asp:ListItem>
                                                            <asp:ListItem Text="KADAPA" Value="KADAPA"></asp:ListItem>
                                                            <asp:ListItem Text="KURNOOL" Value="KURNOOL"></asp:ListItem>
                                                            <asp:ListItem Text="TIRUPATI" Value="TIRUPATI"></asp:ListItem>
                                                            <asp:ListItem Text="BHIMAVARAM" Value="BHIMAVARAM"></asp:ListItem>
                                                            <asp:ListItem Text="ELURU" Value="ELURU"></asp:ListItem>
                                                            <asp:ListItem Text="KAKINADA" Value="KAKINADA"></asp:ListItem>
                                                             <asp:ListItem Text="SRIKAKULAM" Value="SRIKAKULAM"></asp:ListItem>
                                                             <asp:ListItem Text="KUKATPALLI" Value="KUKATPALLI"></asp:ListItem>
                                                            <asp:ListItem Text="PATANCHERU" Value="PATANCHERU"></asp:ListItem>
                                                                <asp:ListItem Text="SIDDIPET" Value="SIDDIPET"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12" style="">
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lAESIRegCode" style="" id="">
                                                        ESI Reg Code</label>
                                                    <div class="col-sm-2 tAESIRegCode" style="" id="">
                                                        <asp:TextBox ID="txtESIRegCode" runat="server" placeholder="ESI Reg Code" class="form-control"
                                                            MaxLength="25"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lAESIBranch" style="" id="">
                                                        ESI Branch</label>
                                                    <div class="col-sm-2 tAESIBranch" style="" id="">
                                                        <asp:TextBox ID="txtESIBranch" runat="server" placeholder="ESI Branch" class="form-control"
                                                            MaxLength="25"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lAPTRegNo" style="" id="">
                                                        PT Reg No</label>
                                                    <div class="col-sm-2 tAPTRegNo" style="" id="">
                                                        <asp:TextBox ID="txtPTRegNo" runat="server" placeholder="PT Reg No" class="form-control"
                                                            MaxLength="25"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12" style="">
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lAPANNo" style="" id="">
                                                        PAN No</label>
                                                    <div class="col-sm-2 tAPANNo" style="" id="">
                                                        <asp:TextBox ID="txtPANNo" runat="server" placeholder="PAN No" class="form-control"
                                                            MaxLength="15"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lAWeekOFF" style="" id="">
                                                        Week OFF<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                    <div class="col-sm-2 tAweek" style="" id="">
                                                        <asp:DropDownList ID="ddlWeekOFF" runat="server" class="form-control DropdownCss">
                                                            <asp:ListItem Text="Select Week OFF" Value="Select Week OFF"></asp:ListItem>
                                                            <asp:ListItem Text="Sunday" Value="Sunday"></asp:ListItem>
                                                            <asp:ListItem Text="Monday" Value="Monday"></asp:ListItem>
                                                            <asp:ListItem Text="Tuesday" Value="Tuesday"></asp:ListItem>
                                                            <asp:ListItem Text="Wednesday" Value="Wednesday"></asp:ListItem>
                                                            <asp:ListItem Text="Thursday" Value="Thursday"></asp:ListItem>
                                                            <asp:ListItem Text="Friday" Value="Friday"></asp:ListItem>
                                                            <asp:ListItem Text="Saturday" Value="Saturday"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                  <div class="form-group">
                                                    <label for="password" class="control-label col-sm-1 clstatus" style="" id="">
                                                        Status</label>
                                                    <div class="col-sm-2 dlstatus" style="" id="">
                                                        <asp:DropDownList ID="ddlstatus" runat="server" maxlength="20" class="form-control">
                                                            <asp:ListItem Text="Active" Value="0" />
                                                            <asp:ListItem Text="De-Active" Value="1" />
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                             </asp:Panel>
                                         <asp:Panel ID="Panel4" runat="server" GroupingText="Other Details" Style="border-bottom: 0px solid #8F8F8F;display:none;">
                                            
                                            <div class="col-sm-12" style="">
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 cTinNo" style="" id="">
                                                        TIN No<span style="color: #ff0000; font-size: 14px;">*</span>
                                                    </label>
                                                    <div class="col-sm-2 ttTinNO" style="" id="">
                                                        <asp:TextBox ID="txtTINNO" runat="server" placeholder="TIN No" onchange="TinNo(this)"
                                                            MaxLength="11" class="form-control"></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender26" runat="server" FilterType="Numbers, Custom"
                                                            ValidChars=" " TargetControlID="txtTINNO" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 cECCNo" style="" id="">
                                                        ECC No</label>
                                                    <div class="col-sm-2 ttECCNo" style="" id="">
                                                        <asp:TextBox ID="txtECCNo" runat="server" placeholder="ECC NO" class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 clcerange" style="" id="">
                                                        CE Range</label>
                                                    <div class="col-sm-2 tcerange" style="" id="">
                                                        <asp:TextBox ID="txtcerange" runat="server" placeholder="CE Range" class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12" style="">
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 clcedivision" style="" id="">
                                                        CE Division</label>
                                                    <div class="col-sm-2 tcedivision" style="" id="">
                                                        <asp:TextBox ID="txtcedivision" runat="server" placeholder="CE Division" class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 clcommissionerate" style=""
                                                        id="">
                                                        CE Commissionerate</label>
                                                    <div class="col-sm-2 tcommissionerate" style="" id="">
                                                        <asp:TextBox ID="txtcommissionerate" runat="server" placeholder="CE Commissionerate"
                                                            class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                              
                                            </div>
                                            <div class="col-sm-12" style="">
                                                <div class="form-group">
                                                    <label for="textfield " class="control-label col-sm-1 clcstno" style="" id="">
                                                        CST No</label>
                                                    <div class="col-sm-2" style="" id="tcstno">
                                                        <asp:TextBox ID="txtcstno" runat="server" placeholder="CST No" class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div class="col-sm-12" style="">
                                        <div class="row">
                                            <asp:Panel ID="Panel2" runat="server" GroupingText="Bank Details" Style="border-bottom: 0px solid #8F8F8F;">
                                                <div class="col-sm-12" style="">
                                                    <asp:GridView ID="grvBankDetail" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                                        Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%;" runat="server"
                                                        OnRowDataBound="grvBankDetail_DataBound" ShowFooter="true" AutoGenerateColumns="False">
                                                        <AlternatingRowStyle BackColor="#EEEEEE" />
                                                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                        <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                        <SortedDescendingHeaderStyle BackColor="#000065" />
                                                        <Columns>
                                                            <%-- <asp:TemplateField HeaderText="Bank ID">
                                                                                <ItemTemplate>
                                                                                    <asp:TextBox ID="txtbankId" runat="server" placeholder="Bank ID" class="form-control"
                                                                                        MaxLength="10" Text='<%# Eval("Bank_ID")%>'></asp:TextBox>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>--%>
                                                            <asp:TemplateField HeaderText="Bank Name">
                                                                <ItemTemplate>
                                                                    <div class="container_Tip" style="" id="tcontainer_Tip">
                                                                        <asp:DropDownList ID="ddlBankName" runat="server" class="form-control  DropdownCss"
                                                                            MaxLength="20" DataTextField="Bank_Name" DataValueField="Bank_ID">
                                                                        </asp:DropDownList>
                                                                        <asp:TextBox ID="txtBank_Name" runat="server" Text='<%# Eval("Bank_Name")%>' CssClass="form-control"
                                                                            Visible="False"></asp:TextBox>
                                                                    </div>
                                                                    <div style="" id="btnadddetails">
                                                                        <asp:Button ID="btnAddDetails" runat="server" class="AddDetails" Text="" OnClick="btnAddDetails_Click" />
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Account Number">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtAccountNumber" runat="server" placeholder="Account Number" class="form-control"
                                                                        Text='<%# Eval("Account_Number")%>' MaxLength="13"></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Branch">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtBranch" runat="server" placeholder="Branch" class="form-control"
                                                                        Text='<%# Eval("Branch")%>' MaxLength="30"></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lbRemove" runat="server" OnClick="lbRemove_Click">Remove</asp:LinkButton>
                                                                </ItemTemplate>
                                                                <FooterStyle HorizontalAlign="Right" />
                                                                <FooterTemplate>
                                                                    <asp:Button ID="btnAddNewRecord" runat="server" Text="Record" class="btn btn-primary"
                                                                        OnClick="btnAddNewRecord_Click" />
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                    <asp:Button ID="btnAddressDetails" runat="server" Style="display: none;" />
                                                    <cc1:ModalPopupExtender ID="mp1" runat="server" PopupControlID="Panel3" TargetControlID="btnAddressDetails"
                                                        CancelControlID="btnClose" BackgroundCssClass="modalBackground">
                                                    </cc1:ModalPopupExtender>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    <div class="form-actions col-sm-offset-2 col-sm-10 ACButtons">
                                        <asp:Button ID="btnSave" OnClientClick="return Customer()" runat="server" Text="Save" class="btn btn-primary" OnClick="btnSave_Click" />
                                         <asp:Button ID="btnUpdate" OnClientClick="return Customer()" runat="server" Text="Save" class="btn btn-primary"  OnClick="btnUpdate_Click"/>
                                        <asp:Button ID="btnCancel" runat="server" Text="Reset" class="btn" onclick="btnCancel_click"/>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="Panel3" runat="server" CssClass="modalPopup" align="center" Style="display: none">
                        <div class="modal-header" runat="server" id="Popupheader">
                            <h3>
                                Bank Details<a class="close-modal" href="#" id="btnClose">&times;</a></h3>
                        </div>
                        <div class="modal-body">
                            <div class="panel-body">
                                <div class="col-sm-12" style="">
                                    <div class="form-group">
                                        <label for="textfield" class="control-label col-sm-1  lCBankID" style="">
                                            Bank ID</label>
                                        <div class="col-sm-4 TCBankID" style="margin-left: 0px;">
                                            <asp:TextBox ID="txtBankID" runat="server" class="form-control" placeholder="Bank ID"
                                                TextMode="SingleLine" MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="textfield" class="control-label col-sm-1 lCBankName" style="">
                                            Bank Name<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                        <div class="col-sm-4 TCBankName" style="margin-left: 0px;">
                                            <asp:TextBox ID="txtBankName" runat="server" class="form-control" placeholder="Bank Name"
                                                TextMode="SingleLine" MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="popbuttons" style="">
                                        <asp:Button ID="btnNewAddBankDetails" class="btn btn-primary" runat="server" Text="Submit"
                                            OnClick="btnNewAddBankDetails_Click" />
                                        <asp:Button ID="btnAddReset" runat="server" Text="Reset" class="btn" OnClick="btnAddReset_Click" />
                                    </div>
                                </div>
                            </div>
                            <!-- /.panel -->
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
