﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="HRMS.Dashboard.Dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.js"></script>

    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="Dashboard.aspx" class="current"><i class="fa fa-home"></i>Dashboard</a> </li>
                    </ul>
                    <%--    <div class="pull-right pull-module m-bot20" style="">
                        <i class="fa fa-calendar padd-r-1"><span class="big badge bg-important" id="MDate"></span><span id="weekT"></span></i>
                    </div>--%>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <div class="panel">
                        <header class="panel-heading">
                            <i class="fa fa-calendar padd-r-1"> Leave Applications</i>
                        </header>
                        <div class="panel-body">
                            <h5><b> Waiting For your Approval</b></h5>
                            <asp:GridView ID="gdvempleave"  CssClass="mydatagrid" HeaderStyle-CssClass="headergrid" RowStyle-CssClass="rows"
                                runat="server" Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%;" AutoGenerateColumns="False" EmptyDataText="No records Found" OnRowDataBound="gdvempleave_RowDataBound" OnSelectedIndexChanged="gdvempleave_SelectedIndexChanged">
                                <Columns>

                                    <asp:BoundField DataField="ID" HeaderText="Leave No" ItemStyle-Width="150">
                                        <ItemStyle Width="150px"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="empname" HeaderText="Employee Name" ItemStyle-Width="150">
                                        <ItemStyle Width="150px"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="leavetype" HeaderText="Leave Type" ItemStyle-Width="200">
                                        <ItemStyle Width="200px"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="From Date">
                                        <ItemTemplate>
                                            <%# Eval("fromdate","{0:dd/MM/yyyy}") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="To Date">
                                        <ItemTemplate>
                                            <%# Eval("todate","{0:dd/MM/yyyy}") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="totaldays" HeaderText="No Of Days" ItemStyle-Width="200">
                                        <ItemStyle Width="200px"></ItemStyle>
                                    </asp:BoundField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="panel">
                        <header class="panel-heading">
                            <i class="fa fa-money padd-r-1"> Loan Applications</i>
                        </header>
                        <div class="panel-body">
                            <h5><b> Waiting for your Approval</b></h5>
                            <asp:GridView ID="gdvemploanapp" class="table table-hover table-nomargin table-bordered CustomerAddress" CssClass="mydatagrid" HeaderStyle-CssClass="header" RowStyle-CssClass="rows"
                                runat="server" Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%;" EmptyDataText="No records Found" AutoGenerateColumns="false" OnRowDataBound="gdvemploanapp_RowDataBound" OnSelectedIndexChanged="gdvemploanapp_SelectedIndexChanged">
                                <Columns>
                                    <asp:BoundField DataField="ID" HeaderText="Loan No" ItemStyle-Width="200" />
                                    <asp:BoundField DataField="empname" HeaderText="Employee Name" ItemStyle-Width="200" />
                                    <asp:BoundField DataField="loanapply" HeaderText="Loan Type" ItemStyle-Width="200" />
                                    <asp:BoundField DataField="Eligibilityamount" HeaderText="Eligibility Amount" ItemStyle-Width="200" />
                                    <asp:BoundField DataField="requiredammount" HeaderText="Amount Required" ItemStyle-Width="200" />
                                    <asp:BoundField DataField="tenure" HeaderText="Tenure" ItemStyle-Width="200" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
    <script type="text/javascript">
        tday = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
        tmonth = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

        function GetClock() {
            var d = new Date();
            var nday = d.getDay(), nmonth = d.getMonth(), ndate = d.getDate(), nyear = d.getYear();
            if (nyear < 1000) nyear += 1900;
            var nhour = d.getHours(), nmin = d.getMinutes(), nsec = d.getSeconds(), ap;

            if (nhour == 0) { ap = " AM"; nhour = 12; }
            else if (nhour < 12) { ap = " AM"; }
            else if (nhour == 12) { ap = " PM"; }
            else if (nhour > 12) { ap = " PM"; nhour -= 12; }

            if (nmin <= 9) nmin = "0" + nmin;
            if (nsec <= 9) nsec = "0" + nsec;

            document.getElementById('MDate').innerHTML = "" + tmonth[nmonth] + " " + ndate + ", " + nyear + "";
            document.getElementById('weekT').innerHTML = "" + tday[nday] + "," + nhour + ":" + nmin + ":" + nsec + ap + "";
        }

        window.onload = function () {
            GetClock();
            setInterval(GetClock, 1000);
        }
    </script>
</asp:Content>
