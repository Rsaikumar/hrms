﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="Loan_Aproval.aspx.cs" Inherits="HRMS.Payroll.Loan_Aproval" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div id="main">
        <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx">
                        <%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i></li>
                    <li><a href="">Payroll</a><i class="fa fa-angle-right"></i> </li>
                    <li><a href="Loan_Application_List.aspx">Loan Application Details</a> <i class="fa fa-angle-right"></i></li>
                    <li><a href="Loan_Application.aspx">Loan Application</a> </li>
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                        <div class="box-title">
                            <h3>
                                <i class="fa fa-th-list"></i>Loan Application</h3>
                        </div>
                        <div class="box-content">
                            <form action="#" method="POST" class='form-horizontal form-bordered'>
                                <div class="col-sm-12" style="">
                                    <div class="col-sm-12" style="">
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lAEmpCode" style="">
                                                Emp Code
                                            </label>
                                            <div class="col-sm-2 tAEmpCode" style="">
                                                <asp:TextBox ID="txtEmpCode" runat="server" placeholder="Emp Code" MaxLength="50"
                                                    class="form-control" ReadOnly="true" AutoPostBack="true" OnTextChanged="txtEmpCode_OnTextChanged"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lALeaveName" style="">
                                                Employee Name<span id="Span2" runat="server" style="color: Red; display: inline;">*</span></label>
                                            <div class="col-sm-5 tAEmpName" style="" id="">
                                                <asp:DropDownList ID="ddlEmpName" runat="server" Visible="false" class="form-control DropdownCss"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlEmpName_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:TextBox ID="txtEmpName" runat="server" placeholder="Emp Name" MaxLength="150"
                                                    class="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lADepartment" style="">
                                                Department<span id="Span3" runat="server" style="color: Red; display: inline;">*</span></label>
                                            <div class="col-sm-2 tADepartment" style="" id="Div1">
                                                
                                                <asp:TextBox ID="txtDepartment" runat="server" placeholder="Department" class="form-control"
                                                    onkeydown="return jsDecimals(event);" MaxLength="50" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lADesignation" style="">
                                                Designation<span id="Span4" runat="server" style="color: Red; display: inline;">*</span></label>
                                            <div class="col-sm-2 tADesignation" style="" id="Div2">
                                                <asp:TextBox ID="txtDesignation" runat="server" placeholder="Designation" class="form-control"
                                                    onkeydown="return jsDecimals(event);" MaxLength="50" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lAReportingAuthority" style="">
                                                Reporting Authority<span id="Span5" runat="server" style="color: Red; display: inline;">*</span></label>
                                            <div class="col-sm-2 tAReportingAuthority" style="" id="Div3">
                                               
                                                <asp:TextBox ID="txtReportingAuthority" runat="server" placeholder="Reporting Authority"
                                                    class="form-control" onkeydown="return jsDecimals(event);" MaxLength="50" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="">
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lALeaveApply" style="">
                                                Type Of Loan Apply<span style="color: Red; display: inline;">*</span></label>
                                            <div class="col-sm-2 tALeaveApply" style="" id="Div4">
                                                <asp:DropDownList ID="ddlLeaveApply" runat="server" ReadOnly="true" class="form-control DropdownCss"
                                                    >
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                         <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lALeaveApply" style="">
                                                Status<span style="color: Red; display: inline;">*</span></label>
                                            <div class="col-sm-2 tALeaveApply" style="" id="Div8">
                                                <asp:DropDownList ID="ddlstatus" runat="server" class="form-control DropdownCss">
                                                    <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                        <asp:ListItem Text="Wating For Approval" Value="Wating For Approval"></asp:ListItem>
                                                        <asp:ListItem Text="Approval" Value="Approval"></asp:ListItem>
                                                        <asp:ListItem Text="Not Approval" Value="Not Approval"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                         <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lADesignation" style="" id="lbltenure">
                                                Tenure<span id="Span6" runat="server" style="color: Red; display: inline;">*</span></label>
                                            <div class="col-sm-2 tADesignation" style="" id="Div5">
                                                <asp:TextBox ID="txttenure" runat="server" placeholder="Tenure" class="form-control"
                                                    onkeydown="return jsDecimals(event);" MaxLength="50" ></asp:TextBox>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="col-sm-12" style="">
                                                      <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lAAllotment" style="">
                                                        Basic Amount<span style="color: #ff0000; font-size: 14px; display: inline;">*</span>
                                                    </label>
                                                    <div class="col-sm-2 tAAllotment" style="">

                                                      <asp:TextBox ID="txtbasicamount" runat="server" placeholder="Basic Amount" class="form-control"
                                                   MaxLength="50" ReadOnly="true"></asp:TextBox>
                                                          
                                                            

                                                        
                                                    </div>
                                                </div>
                                        <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lALeaveName" style="">
                                                        Eligibility Amount<span id="Span1" runat="server" style="color: Red; display: inline;">*</span></label>
                                                    <div class="col-sm-2 tALeaveName" style="" id="Div6">
                                                        <asp:TextBox ID="txtelgamount" runat="server" TabIndex="0" placeholder="Eligibility Amount" class="form-control"
                                                            MaxLength="50" ></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lAID" style="">
                                                       Loan Amount Required
                                                    </label>
                                                    <div class="col-sm-2 tAID" style="">
                                                        <asp:TextBox ID="txtlar" runat="server" placeholder="Enhanced Maximum Celling" MaxLength="50"
                                                            class="form-control" ReadOnly="false"></asp:TextBox>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">

                                                    <label for="textfield" class="control-label col-sm-1 lAShortName" style="">
                                                        Rate Of Intrest<span style="color: #ff0000; font-size: 14px; display: inline;">*</span></label>
                                                    <div class="col-sm-2 tAShortName" style="" id="Div7">
                                                        <asp:TextBox ID="txtroi" runat="server" TabIndex="1" placeholder="Rate Of Intrest" class="form-control"
                                                            MaxLength="50" AutoPostBack="true" ></asp:TextBox>
                                                    </div>
                                                </div> 
                                       
                                            </div>
                              </div>
                                </form>
                                <div class="col-sm-5 ACButtons">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary" OnClick="btnSave_Click" /><%--OnClientClick="return fileUploadValidation()"--%>
                                    <asp:Button ID="btnUpdate" runat="server" Text="Save" class="btn btn-primary" OnClick="btnUpdate_Click" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Reset" class="btn" />
                                </div>
                        </div>

                    </div>
                </div>
            </div>
</asp:Content>
