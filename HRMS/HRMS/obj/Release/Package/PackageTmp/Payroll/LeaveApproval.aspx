﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="LeaveApproval.aspx.cs" Inherits="HRMS.Payroll.LeaveApproval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style>
        .fontweight-pm {
            font-weight: 200;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx" class="current"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="../Payroll/LeaveApproval.aspx">Leave Approval</a><i class="fa fa-angle-right"></i> </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <i class="fa fa-user"> Leave Approval</i>
                        </header>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-5" style="">
                                        Employee Name :</label>
                                    <div class="col-sm-7" style="" id="Div1">
                                        <label class="control-label fontweight-pm" runat="server" id="lblempname"></label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-5" style="">
                                        Department</label>
                                    <div class="col-sm-7" style="" id="">
                                        <label class="control-label fontweight-pm" runat="server" id="lbldept"></label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-5 " style="">
                                        Designation</label>
                                    <div class="col-sm-7" style="" id="">
                                        <label class="control-label fontweight-pm" runat="server" id="lbldesignation"></label>
                                    </div>
                                </div>
                                   <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-5 " style="">
                                     Reporting</label>
                                    <div class="col-sm-7" style="" id="Div4">
                                        <label class="control-label fontweight-pm" runat="server" id="lblreportingauthority"></label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-5" style="">
                                        Leave Type</label>
                                    <div class="col-sm-7" style="" id="">
                                        <label class="control-label fontweight-pm" runat="server" id="lblleavetype"></label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6" id="uploadCertification" visible="false" runat="server">
                                    <label for="textfield" class="control-label col-sm-5 ">
                                        Upload Cretification</label>
                                    <div class="col-sm-7" style="" id="">
                                        <asp:FileUpload ID="fupCertification" runat="server" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-5" style="">
                                        From Date</label>
                                    <div class="col-sm-7 " style="" id="Div2">
                                        <label class="control-label fontweight-pm " runat="server" id="lblfromdate"></label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-5" style="">
                                        To Date</label>
                                    <div class="col-sm-7" style="" id="Div3">
                                        <label class="control-label fontweight-pm" runat="server" id="lbltodate"></label>
                                    </div>
                                </div>
                               
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-5" style="">
                                        No Of Days</label>
                                    <div class="col-sm-7" style="" id="">
                                        <label class="control-label fontweight-pm" runat="server" id="lblnoofdays"></label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-5" style="">
                                        Status
                                    </label>
                                    <div class="col-sm-7">
                                        <label class="control-label fontweight-pm" runat="server" id="lblstatus"></label>
                                    </div>
                                </div>
                                 <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-5" style="">
                                      Applied Date
                                    </label>
                                    <div class="col-sm-7">
                                        <label class="control-label fontweight-pm" runat="server" id="lbldate"></label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6" id="noofLeavesid" runat="server">
                                    <label for="textfield" class="control-label col-sm-5">
                                      No Of Leaves</label>
                                    <div class="col-sm-7" style="" id="Div5">
                                        <label class="control-label fontweight-pm" runat="server"  id="lblnoofleavesavailable"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 text-center">
                                <asp:Button ID="btnapprove" runat="server" CssClass="btn btn-primary" OnClick="btnapprove_Click" Text="Approve" />
                                <asp:Button ID="btnreject" runat="server" CssClass="btn btn-danger" Text="Reject" OnClick="btnreject_Click" />
                                <asp:Button ID="btncancel" runat="server" CssClass=" btn btn-primary" Text="Back" OnClick="btncancel_Click"/>

                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
