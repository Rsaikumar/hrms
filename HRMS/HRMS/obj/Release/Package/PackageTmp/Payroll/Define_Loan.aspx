﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="Define_Loan.aspx.cs" Inherits="HRMS.Payroll.Define_Loan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-sm-12">

                            <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                                <div class="box-title">
                                    <h3>
                                        <i class="fa fa-th-list"></i>Define Loan Types</h3>
                                </div>
                                <div class="box-content">
                                    <form action="#" method="POST" class='form-horizontal form-bordered'>
                                        <div class="col-sm-12" style="">

                                            <div class="col-sm-12" style="">
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lAID" style="">
                                                        ID
                                                    </label>
                                                    <div class="col-sm-2 tAID" style="">
                                                        <asp:TextBox ID="txtID" runat="server" placeholder="ID" MaxLength="50"
                                                            class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lALeaveName" style="">
                                                        Loan Name<span id="Span1" runat="server" style="color: Red; display: inline;">*</span></label>
                                                    <div class="col-sm-2 tALeaveName" style="" id="">
                                                        <asp:TextBox ID="txtLeaveName" runat="server" TabIndex="0" placeholder="Leave Name" class="form-control"
                                                            MaxLength="50" AutoPostBack="true" OnTextChanged="txtLeaveName_TextChanged"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lAShortName" style="">
                                                        Short Name<span style="color: #ff0000; font-size: 14px; display: inline;">*</span></label>
                                                    <div class="col-sm-2 tAShortName" style="" id="Div1">
                                                        <asp:TextBox ID="txtShortName" runat="server" TabIndex="1" placeholder="Short Name" class="form-control"
                                                            MaxLength="50" AutoPostBack="true" OnTextChanged="txtShortName_TextChanged"></asp:TextBox>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="col-sm-12" style="">                                                

                                                <div class="form-group">

                                                    <div class="col-sm-2 tBalances" style="" id="Div2">
                                                        <asp:CheckBox ID="chbCertification" runat="server" Text="" AutoPostBack="true"  />
                                                    </div>
                                                   
                                                   <label for="textfield" class="control-label col-sm-1 lABalances" style="">
                                                       Document Evidence is required..?</label>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lAAllotment" style="">
                                                        Allotment<span style="color: #ff0000; font-size: 14px; display: inline;">*</span>
                                                    </label>
                                                    <div class="col-sm-2 tAAllotment" style="">

                                                        <asp:DropDownList ID="ddlAllotment" runat="server" class="form-control DropdownCss">
                                                            <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                            <asp:ListItem Text="Auto" Value="Auto"></asp:ListItem>
                                                            <asp:ListItem Text="Manual" Value="Manual"></asp:ListItem>

                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                                                             </div>
                                        </div>
                                        <div class="col-sm-12" style="">
                                            <asp:Panel ID="Panel1" runat="server" GroupingText="Loan Limitations:">

                                                <div class="col-sm-12" style="">
                                                   <%-- <div class="form-group">
                                                        <label for="textfield" class="control-label col-sm-1 lblperiod" style="">
                                                            Period
                                                        </label>
                                                        <div class="col-sm-2 tperiod" style="">
                                                            <asp:DropDownList ID="ddlperiod" runat="server" class="form-control DropdownCss">
                                                                <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                <asp:ListItem Text="Monthly" Value="Monthly"></asp:ListItem>
                                                                <asp:ListItem Text="Quarterly" Value="Quarterly"></asp:ListItem>
                                                                <asp:ListItem Text="Halfly" Value="Halfly"></asp:ListItem>
                                                                <asp:ListItem Text="Annually" Value="Annually"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>--%>
                                                        <div class="form-group">
                                                            <label for="textfield" class="control-label col-sm-1 lbltimes" style="">
                                                                No.Of Months
                                                            </label>
                                                            <div class="col-sm-2 ttimes" style="" id="Div9">
                                                                <asp:TextBox ID="txtnooftimes" runat="server" placeholder="No.Of Times" class="form-control"
                                                                    MaxLength="50"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>

                                            </asp:Panel>
                                        </div>
                                        <div class="col-sm-12" style="">
                                            <asp:Panel ID="Panel2" runat="server" GroupingText="Loan Approving Cycle">
                                                <div class="col-sm-12" style="">
                                                    <div class="form-group">
                                                        <asp:CheckBox ID="chktwostages" runat="server" />
                                                        Multi Level Approval
                                                    </div>
                                                    <div class="form-group">
                                                        <asp:CheckBox ID="chkRecommamd" runat="server" AutoPostBack="true" OnCheckedChanged="chkRecommamd_CheckedChanged" />
                                                        Reporting Authority Of Respective Employee 
                                                    </div>

                                                </div>
                                                <div class="col-sm-12" id="recommadDiv" runat="server" style="">
                                                    <div class="form-group">
                                                        <label for="textfield" class="control-label col-sm-1 lblauthority" style="">
                                                            ReCommendation<span style="color: #ff0000; font-size: 14px; display: inline;">*</span>
                                                        </label>
                                                        <div class="col-sm-2 tauthority" style="">
                                                            <asp:DropDownList ID="ddlauthority1" runat="server" class="form-control DropdownCss" AutoPostBack="true"
                                                                OnSelectedIndexChanged="ddlauthority_SelectedIndexChanged">

                                                                <asp:ListItem Text="Select" Value="Select"></asp:ListItem>

                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-sm-2 tuname" style="" id="Div11">
                                                            <asp:TextBox ID="txtusername" runat="server" placeholder="User Name" class="form-control"
                                                                MaxLength="50" ReadOnly="true"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12" style="">
                                                    <div class="form-group">
                                                        <label for="textfield" class="control-label col-sm-1 lblauthority" style="">
                                                            Authorization<span style="color: #ff0000; font-size: 14px; display: inline;">*</span>
                                                        </label>
                                                        <div class="col-sm-2 tauthority" style="">
                                                            <asp:DropDownList ID="ddlauthority2" runat="server" class="form-control DropdownCss" AutoPostBack="true"
                                                                OnSelectedIndexChanged="ddlauthority2_SelectedIndexChanged">

                                                                <asp:ListItem Text="Select" Value="Select"></asp:ListItem>

                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-sm-2 tuname" style="" id="Div12">
                                                            <asp:TextBox ID="txtusername2" runat="server" placeholder="User Name" class="form-control"
                                                                MaxLength="50" ReadOnly="true"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="col-sm-12" style="">
                                                    <div class="form-group">
                                                        <label for="textfield" class="control-label col-sm-1 lblauthority" style="">
                                                            Approval<span style="color: #ff0000; font-size: 14px; display: inline;">*</span>
                                                        </label>
                                                        <div class="col-sm-2 tauthority" style="">
                                                            <asp:DropDownList ID="ddlauthority3" runat="server" class="form-control DropdownCss"
                                                                OnSelectedIndexChanged="ddlauthority3_SelectedIndexChanged">

                                                                <asp:ListItem Text="Select" Value="Select"></asp:ListItem>

                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-sm-2 tuname" style="" id="Div13">
                                                            <asp:TextBox ID="txtusername3" runat="server" placeholder="User Name" class="form-control"
                                                                MaxLength="50" ReadOnly="true"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>

                                            </asp:Panel>
                                        </div>
                                        <div class="col-sm-5 ACButtons">
                                            <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary" OnClientClick="return ControlValid()" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-primary" OnClick="btnUpdate_Click" />
                                            <asp:Button ID="btnCancel" runat="server" Text="Reset" class="btn" OnClick="btnCancel_Click" />
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>



                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>

</asp:Content>
