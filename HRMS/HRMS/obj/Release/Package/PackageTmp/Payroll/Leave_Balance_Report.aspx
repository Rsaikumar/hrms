﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true"
    CodeBehind="Leave_Balance_Report.aspx.cs" Inherits="HRMS.Payroll.Leave_Balance_Report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" lang="">

        function Validation() {

            if (document.getElementById('<%=ddlYear.ClientID%>').options[document.getElementById('<%=ddlYear.ClientID%>').selectedIndex].value == "Select") {
                alert("Please Select Year..");
                document.getElementById('<%=ddlYear.ClientID%>').focus();
                return false;

            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="#">Reports</a><i class="fa fa-angle-right"></i> </li>
                        <li><a href="../Payroll/Leave_Balance_Report.aspx" class="current">Leave Report</a> </li>
                    </ul>
                </div>
            </div>

            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-md-12">
                            <section class="panel">
                                <header class="panel-heading">
                                    <i class="fa fa-th-list"> Leave Balance Report</i>
                                </header>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="form-group col-md-6">
                                            <label for="textfield" class="col-sm-5 text-center">
                                                Year<span style="color: #ff0000; font-size: 14px;">*</span>
                                            </label>
                                            <div class="col-sm-7" style="">
                                                <asp:DropDownList ID="ddlYear" runat="server" class="form-control ">
                                                    <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                    <asp:ListItem Text="2016" Value="2016"></asp:ListItem>
                                                    <asp:ListItem Text="2017" Value="2017"></asp:ListItem>
                                                    <asp:ListItem Text="2018" Value="2018"></asp:ListItem>
                                                    <asp:ListItem Text="2019" Value="2019"></asp:ListItem>
                                                    <asp:ListItem Text="2020" Value="2020"></asp:ListItem>
                                                    <asp:ListItem Text="2021" Value="2021"></asp:ListItem>
                                                    <asp:ListItem Text="2022" Value="2023"></asp:ListItem>
                                                    <asp:ListItem Text="2024" Value="2024"></asp:ListItem>
                                                    <asp:ListItem Text="2025" Value="2025"></asp:ListItem>
                                                    <asp:ListItem Text="2026" Value="2026"></asp:ListItem>
                                                    <asp:ListItem Text="2027" Value="2027"></asp:ListItem>
                                                    <asp:ListItem Text="2028" Value="2028"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6" style="">
                                            <label for="textfield" class="col-sm-5 text-center" style="">
                                                Emp Code</label>
                                            <div class="col-sm-7" style="">
                                                <asp:DropDownList ID="ddlEmpCode" runat="server" class="form-control  ">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6" runat="server" visible="false">
                                            <label for="textfield" class="control-label col-sm-5" style="">
                                                Department
                                            </label>
                                            <div class="col-sm-7" style="">
                                                <asp:DropDownList ID="ddlDepartment" runat="server" class="form-control  ">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <%--<div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lblProducts" style="">
                                                Products
                                            </label>
                                            <div class="col-sm-2 tProducts" style="">
                                                <asp:DropDownList ID="ddlproducts" runat="server" class="form-control  DropdownCss">
                                                </asp:DropDownList>
                                            </div>
                                        </div>--%>
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <asp:Button ID="btnShow" runat="server" Text="Submit" class="btn btn-primary" OnClick="btnShow_Click" OnClientClick="return Validation()" />
                                    </div>
                                    <div class="col-sm-12">
                                        <asp:Panel ID="Panel1" runat="server" Visible="false">
                                           <asp:Button ID="btnExcel" runat="server" class="btn-success text-right" ToolTip="Export To Excel" Text="Excel"
                                                OnClientClick="return pricelistExcel()" OnClick="btnExport_Click" />
                                            <div id="Iscroll" runat="server" style="">
                                                <asp:GridView ID="grd_LEave_Balance" class="table table-bordered CustomerAddress"
                                                    runat="server"
                                                    ShowFooter="true" AutoGenerateColumns="False" OnPageIndexChanging="grd_LEave_Balance_PageIndexChanging"
                                                    AllowPaging="true" PageSize="3" AllowSorting="true"
                                                    OnRowDataBound="grd2Bind" DataKeyNames="Emp_Code">

                                                    <Columns>
                                                       <%-- <asp:TemplateField HeaderText="Emp Code" SortExpression="Emp_Code" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEmpCode" runat="server" Text='<%# Eval("Emp_Code")%>' Style="text-align: center;"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Emp Name" SortExpression="Emp_Name" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEmpName" runat="server" Text='<%# Eval("Emp_Name") %>' Style="text-align: center;"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Department" SortExpression="Department" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("Department")%>' Style="text-align: center;"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                        <asp:TemplateField ItemStyle-Width="50%">
                                                            <HeaderTemplate>
                                                                <table class="table table-hover table-nomargin table-bordered CustomerAddress">
                                                                    <tr>
                                                                        <td style="width: 39%;">Leave Name
                                                                        </td>
                                                                        <%-- <td style="">
                                                                            Leave Type
                                                                        </td>--%>
                                                                        <td style="">Total Leaves
                                                                        </td>
                                                                        <td style="">Used Leaves
                                                                        </td>
                                                                        <td style="">Balance Leaves
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:GridView ID="grd2" class="table table-nomargin table-bordered CustomerAddress"
                                                                    Style="margin-bottom: 0.83%; margin-top: 0.83%;" runat="server"
                                                                    AutoGenerateColumns="false" ShowHeader="false">

                                                                    <Columns>
                                                                        <asp:BoundField DataField="Leave_Name" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemStyle Width="39%" />
                                                                        </asp:BoundField>
                                                                        <%--<asp:BoundField DataField="Short_Name" HeaderText="Leave_Type" ItemStyle-HorizontalAlign="Right">
                                                                            <ItemStyle Width="13%" />
                                                                        </asp:BoundField>--%>
                                                                        <asp:BoundField DataField="Noof_Leavs" HeaderText="Noof_Leavs" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemStyle Width="20%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Used_Leaves" HeaderText="Used_Leaves" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemStyle Width="" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Balance_Leaves" HeaderText="Balance_Leaves" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemStyle Width="" />
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="14%" />
                                                            <ItemStyle Width="50%" />
                                                        </asp:TemplateField>

                                                        <%-- <asp:TemplateField HeaderText="No Of Leaves" SortExpression="No_Of_Leaves">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNoOfLeaves" runat="server" Text='<%# Eval("Noof_Leavs")%>' Style="text-align: center;"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Leave Name" SortExpression="Leave_Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLeaveName" runat="server" Text='<%# Eval("Leave_Name")%>' Style="text-align: center;"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Leave Type" SortExpression="Leave_Type">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLeaveType" runat="server" Text='<%# Eval("Short_Name")%>' Style="text-align: center;"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Used Leaves" SortExpression="Used_Leaves">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCLUSed" runat="server" Text='<%# Eval("Used_Leaves")%>' Style="text-align: center;"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Balance Leaves" SortExpression="Balance_Leaves">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCLBalance" runat="server" Text='<%# Eval("Balance_Leaves")%>' Style="text-align: center;"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnExcel" />
                </Triggers>
            </asp:UpdatePanel>

            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <i class="fa fa-th-list"> Leave Details</i>
                        </header>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <asp:GridView ID="grdleaves" class="table table-nomargin table-bordered CustomerAddress" runat="server"
                                    ShowFooter="false" AutoGenerateColumns="False" DataKeyNames="Sno">
                                    <Columns>
                                        <asp:BoundField ReadOnly="true" HeaderText="Sno" DataField="Sno" />
                                        <asp:TemplateField HeaderText="Leave Code">
                                            <ItemTemplate>
                                                <asp:Label ID="txtLeave_Code" runat="server" Text='<%# Eval("Leave_Code")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Employee Code">
                                            <ItemTemplate>

                                                <asp:Label ID="lblec" runat="server" Text='<%# Eval("Emp_Code")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Employee Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblen" runat="server" Text='<%# Eval("Emp_Name")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Types of leave">
                                            <ItemTemplate>
                                                <asp:Label ID="lbltol" runat="server" Text='<%# Eval("TypeOfLeaveApple")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="No Of Days">
                                            <ItemTemplate>
                                                <asp:Label ID="lblnod" runat="server" Text='<%# Eval("Noof_Days")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Leave Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblls" runat="server" Text='<%# Eval("Leave_Status")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbRemove" OnClick="lbRemove_OnClick" runat="server">View</asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>

                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
