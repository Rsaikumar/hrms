﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true"
    CodeBehind="Leave_Application.aspx.cs" Inherits="HRMS.Payroll.Leave_Application" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/Payroll/Leave_Aplication.css" rel="stylesheet" />
    <style type="text/css">
        #ContentPlaceHolder1_actdate_container, #ContentPlaceHolder1_CalendarExtender2_popupDiv, #ContentPlaceHolder1_CalendarExtender3_popupDiv, #ContentPlaceHolder1_CalendarExtender1_container, #ContentPlaceHolder1_CalendarExtender2_container, #ContentPlaceHolder1_actdate_popupDiv, #ContentPlaceHolder1_CalendarExtender1_popupDiv
        {
            z-index: 10000;
        }

        .DateHide
        {
            display: none;
        }
    </style>
    <script type="text/javascript">
        //disable sunday
        function detect_sunday(sender, args) {
            if (sender._selectedDate.getDay() == 0) {
                sender._selectedDate = new Date();
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format));
                alert("You can't select sunday!");
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="#">Payroll</a><i class="fa fa-angle-right"></i> </li>
                        <li><a href="../Payroll/Leave_Application.aspx" class="current">Leave Application</a> </li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <i class="fa fa-th-list">Employee Registration</i>
                        </header>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-5" style="">
                                        Emp Code
                                    </label>
                                    <div class="col-sm-7" style="">
                                        <asp:TextBox ID="txtEmpCode" runat="server" placeholder="Emp Code" MaxLength="50"
                                            class="form-control" ReadOnly="true" AutoPostBack="true" OnTextChanged="txtEmpCode_OnTextChanged"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-5" style="">
                                        Employee Name<span id="Span1" runat="server" style="color: Red; display: inline;">*</span></label>
                                    <div class="col-sm-7" style="" id="Div1">
                                        <asp:TextBox ID="txtEmpName" runat="server" placeholder="Emp Name" class="form-control" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>


                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-5" style="">
                                        Department<span id="Span3" runat="server" style="color: Red; display: inline;">*</span></label>
                                    <div class="col-sm-7" style="" id="">
                                        <asp:DropDownList ID="ddlDepartment" runat="server" Style="display: none;" class="form-control DropdownCss">
                                        </asp:DropDownList>
                                        <asp:TextBox ID="txtDepartment" runat="server" placeholder="Department" class="form-control"
                                            onkeydown="return jsDecimals(event);" MaxLength="50" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-5 " style="">
                                        Designation<span id="Span4" runat="server" style="color: Red; display: inline;">*</span></label>
                                    <div class="col-sm-7" style="" id="">
                                        <asp:DropDownList ID="ddlDesignation" runat="server" Style="display: none;" class="form-control DropdownCss">
                                        </asp:DropDownList>
                                        <asp:TextBox ID="txtDesignation" runat="server" placeholder="Designation" class="form-control"
                                            onkeydown="return jsDecimals(event);" MaxLength="50" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-md-6" runat="server" visible="false">
                                    <label for="textfield" class="control-label col-sm-5 " style="">
                                        Reporting Authority<span id="Span5" runat="server" style="color: Red; display: inline;">*</span></label>
                                    <div class="col-sm-7 tAReportingAuthority" style="" id="">
                                        <asp:DropDownList ID="ddlReportingAuthority" runat="server" Style="display: none;"
                                            class="form-control DropdownCss">
                                        </asp:DropDownList>
                                        <asp:TextBox ID="txtReportingAuthority" runat="server" placeholder="Reporting Authority"
                                            class="form-control" onkeydown="return jsDecimals(event);" MaxLength="50" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="textfield" class="control-label col-sm-5" style="">
                                        Type Of Leave Apply<span style="color: Red; display: inline;">*</span></label>
                                    <div class="col-sm-7" style="" id="">
                                        <asp:DropDownList ID="ddlLeaveApply" runat="server" class="form-control DropdownCss"
                                            AutoPostBack="True" OnSelectedIndexChanged="ddlLeaveApply_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group col-md-6" id="uploadCertification" visible="false" runat="server">
                                    <label for="textfield" class="control-label col-sm-5 lADesignation">
                                        Upload Cretification<span style="color: Red; display: inline;">*</span></label>
                                    <div class="col-sm-7" style="" id="">
                                        <asp:FileUpload ID="fupCertification" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <div class="col-md-12">
                                        <div class="form-group col-md-6">
                                            <label for="textfield" class="control-label col-sm-5" style="">
                                                Leave Apply From</label>
                                            <div class="col-sm-7 tLApplyFrom" style="" id="Div2">
                                                <asp:TextBox ID="txtFromDate" runat="server" placeholder="Ex:dd-mm-yyyy" class="form-control"
                                                    MaxLength="20" AutoPostBack="true" OnTextChanged="txtFromDate_OnTextChanged"></asp:TextBox>
                                                <cc1:CalendarExtender ID="actdate" PopupButtonID="imgPopup" runat="server" TargetControlID="txtFromDate"
                                                    Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                </cc1:CalendarExtender>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="textfield" class="control-label col-sm-5" style="">
                                                To</label>
                                            <div class="col-sm-7" style="" id="Div3">
                                                <asp:TextBox ID="txtToDate" runat="server" placeholder="Ex:dd-mm-yyyy" class="form-control"
                                                    MaxLength="20" AutoPostBack="true" OnTextChanged="txtToDate_OnTextChanged"></asp:TextBox>
                                                <cc1:CalendarExtender ID="CalendarExtender1" PopupButtonID="imgPopup" runat="server"
                                                    TargetControlID="txtToDate" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                </cc1:CalendarExtender>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="textfield" class="control-label col-sm-5">
                                                Balances Available</label>
                                            <div class="col-sm-7" style="" id="">
                                                <asp:TextBox ID="txtBalancesAvailable" runat="server" placeholder="Balances Available"
                                                    ReadOnly="true" class="form-control" MaxLength="20" onkeydown="return jsDecimals(event);"
                                                    AutoPostBack="True" OnTextChanged="txtBalancesAvailable_TextChanged"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="textfield" class="control-label col-sm-5" style="">
                                                No Of Days</label>
                                            <div class="col-sm-7" style="" id="">
                                                <asp:TextBox ID="txtNoofDays" runat="server" placeholder="No of Days" class="form-control"
                                                    onkeydown="return jsDecimals(event);" MaxLength="20" AutoPostBack="True" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="textfield" class="control-label col-sm-5" style="">
                                                Status<span style="color: #ff0000; font-size: 14px; display: inline;">*</span>
                                            </label>

                                            <div class="col-sm-7" style="">
                                                <asp:TextBox ID="txtstatus" runat="server" placeholder="status" class="form-control"
                                                    onkeydown="return jsDecimals(event);" MaxLength="20" ReadOnly="true"></asp:TextBox>
                                            </div>
                                            <asp:DropDownList ID="ddlStatus" runat="server" Visible="false" class="form-control DropdownCss">
                                                <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                <asp:ListItem Text="Wating For Approval" Value="Wating For Approval"></asp:ListItem>
                                                <%-- <asp:ListItem Text="Approval" Value="Approval"></asp:ListItem>
                                                        <asp:ListItem Text="Not Approval" Value="Not Approval"></asp:ListItem>--%>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-12" align="center">
                                        <asp:Button ID="btnSave" runat="server" Text="Submit" class="btn btn-primary" OnClick="btnSave_Click" /><%--OnClientClick="return fileUploadValidation()"--%>
                                        <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-primary" OnClick="btnUpdate_Click" />
                                    </div>
                                    </div>
                                       
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </section>
        </div>

            </div>

            <%--<asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                                <div class="box-title">
                                    <h3>
                                        <i class="fa fa-th-list"></i>Define Leave Type List</h3>
                                </div>
                                <div class="box-content">
                                    <form action="#" class='form-horizontal form-bordered'>
                                        <div class="col-sm-12" style="margin-bottom: 0.3%;">
                                            <div class="row">
                                                <asp:GridView ID="grdDefineLeaveTypesList" class="table table-nomargin table-bordered CustomerAddress"
                                                    runat="server" AutoGenerateColumns="false" DataKeyNames="Leave_Code" EmptyDataText="No Records Found"
                                                    ShowFooter="false" AllowPaging="true"
                                                    PageSize="15" AllowSorting="true">

                                                    <Columns>
                                                        <asp:TemplateField HeaderText="S.No" SortExpression="">
                                                            <ItemTemplate>
                                                                <%#Container.DataItemIndex+1 %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Leave Code">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLeaveCode" runat="server" Text='<%#Eval("Leave_Code") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Leave Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLeaveName" runat="server" Text='<%#Eval("Leave_Name") %>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Short Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblShortName" runat="server" Text='<%#Eval("Short_Name") %>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>--%>
    </section>
    </section>

</asp:Content>
