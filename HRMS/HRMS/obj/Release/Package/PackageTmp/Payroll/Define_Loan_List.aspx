﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="Define_Loan_List.aspx.cs" Inherits="HRMS.Payroll.Define_Loan_List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <section id="main-content">
        <section class="wrapper">

           <%-- <div class="page-header">
                <div class="pull-right">
                    <ul class="stats">
                        <li>
                            <asp:Button ID="btnAdd" runat="server" class="add_Btn" OnClick="btnAdd_Click" ToolTip="Create Define Leave Types Details" /></li>
                        <li>
                            <asp:Button ID="btnPdf" runat="server" class="PDF_Btn" ToolTip="Export Define Leave Types Details To Pdf" /></li>
                        <li>
                            <asp:Button ID="btnExcel" runat="server" class="Excel_Btn" ToolTip="Export Define Leave Types Details To Excel" /></li>
                    </ul>
                </div>
            </div>--%>

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="#">Payroll</a><i class="fa fa-angle-right"></i> </li>
                        <li><a href="Define_Loan_List.aspx" class="current">Define Loan Types List</a> </li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <i class="fa fa-th-list"> Define Loan Types List</i>
                        </header>
                        <div class="panel-body">

                            <div class="col-sm-12" style="margin-bottom: 0.3%;">
                                <div class="table-responsive">
                                    <asp:GridView ID="grdDefineLeaveTypesList" class="table table-bordered CustomerAddress" runat="server"
                                        AutoGenerateColumns="false" DataKeyNames="Loan_Code" EmptyDataText="No Records Found"
                                        ShowFooter="false" AllowPaging="true"
                                        PageSize="15" AllowSorting="true"
                                        OnRowCommand="grdDefineLeaveTypesList_RowCommand"
                                        OnSorting="grdDefineLeaveTypesList_Sorting"
                                        OnPageIndexChanging="grdDefineLeaveTypesList_PageIndexChanging"
                                        OnRowDeleting="grdDefineLeaveTypesList_RowDeleting"
                                        OnRowEditing="grdDefineLeaveTypesList_RowEditing">

                                        <Columns>
                                            <asp:TemplateField HeaderText="S.No" SortExpression="">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Loan Code">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLeaveCode" runat="server" Text='<%# Eval("Loan_Code") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Loan Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLeaveName" runat="server" Text='<%#Eval("Loan_Name") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Short Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblShortName" runat="server" Text='<%#Eval("Short_Name") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <%--  <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbkEdit" runat="server" CommandName="Edit" Style="color: gray;
                                                    text-decoration: none; float: left; background-image: url('../img/file_edit.png');
                                                    height: 30px; width: 35px; background-position: left; background-repeat: no-repeat;"
                                                    OnClientClick="return confirm('Are you sure you want to Edit this Record ...?')" CausesValidation="false"
                                                    CommandArgument='<%#Eval("Loan_Code") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete" Style="color: gray;
                                                    text-decoration: none; float: left; background-image: url('../img/delete.png');
                                                    height: 30px; width: 35px; background-position: left; background-repeat: no-repeat;"
                                                    OnClientClick="return confirm('Are you sure you want to Remove this Record ...?')" CausesValidation="false"
                                                    CommandArgument='<%#Eval("Loan_Code") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        </Columns>
                                    </asp:GridView>
                                </div>

                            </div>

                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>

</asp:Content>
