﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="Increment_Form.aspx.cs" Inherits="HRMS.Payroll.Increment_Form" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx">
                        <%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i></li>
                    <li><a href="">Increments</a><i class="fa fa-angle-right"></i> </li>
                    <%--<li><a href="Leave_Application_List.aspx">Leave Application Details</a> <i class="fa fa-angle-right"></i></li>--%>
                    <li><a href="">Increments</a> </li>
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                <div class="box-title">
                    <h3>
                        <i class="fa fa-th-list"></i>Increments</h3>
                </div>
                <div class="box-content">

                    <div class="col-md-12">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <div class="form-group col-md-3">
                                    <label for="textfield" class="control-label col-sm-3" style="">
                                        Circle</label>
                                    <div class="col-sm-9" style="" id="Div39">
                                        <asp:DropDownList ID="ddlcircle" runat="server" class="form-control DropdownCss" AutoPostBack="true" OnSelectedIndexChanged="ddlcircle_SelectedIndexChanged" Visible="false">
                                            <asp:ListItem Text="---Select---" Value="Select"></asp:ListItem>
                                            <asp:ListItem Text="Adilabad" Value="Adilabad"></asp:ListItem>
                                            <asp:ListItem Text="Kawal Tiger Reserve" Value="Kawal Tiger Reserve"></asp:ListItem>
                                            <asp:ListItem Text="Kothagudem" Value="Kothagudem"></asp:ListItem>
                                            <asp:ListItem Text="Khammam" Value="Khammam"></asp:ListItem>
                                            <asp:ListItem Text="Nizamabad" Value="Nizamabad"></asp:ListItem>
                                            <asp:ListItem Text="Amrabada Tiger Reserve" Value="Amrabada Tiger Reserve"></asp:ListItem>
                                            <asp:ListItem Text="Hyderabad" Value="Hyderabad"></asp:ListItem>
                                            <asp:ListItem Text="Karimnagar" Value="Karimnagar"></asp:ListItem>
                                            <asp:ListItem Text="Warangal" Value="Warangal"></asp:ListItem>
                                            <asp:ListItem Text="Medak" Value="Medak"></asp:ListItem>
                                            <asp:ListItem Text="Mahabubnagar" Value="Mahabubnagar"></asp:ListItem>
                                            <asp:ListItem Text="Ranga Reddy" Value="Ranga Reddy"></asp:ListItem>
                                            <asp:ListItem Text="R & D Circle" Value="R & D Circle"></asp:ListItem>
                                            <asp:ListItem Text="STC Circle" Value="STC Circle"></asp:ListItem>
                                            <asp:ListItem Text="Director Zoo Parks" Value="Director Zoo Parks"></asp:ListItem>

                                        </asp:DropDownList>
                                        <asp:TextBox ID="txtcicle" runat="server" MaxLength="50"
                                            class="form-control"></asp:TextBox>

                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <div class="form-group col-md-3">
                            <label for="textfield" class="control-label col-sm-3" style="">
                                District</label>
                            <div class="col-sm-9" style="" id="Div40">
                                <asp:DropDownList ID="ddldistrictforestofficers" runat="server" class="form-control DropdownCss" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="ddldistrictforestofficers_SelectedIndexChanged">
                                    <asp:ListItem Text="---Select---" Value="---Select---"></asp:ListItem>



                                </asp:DropDownList>
                                <asp:TextBox ID="txtdfo" runat="server" MaxLength="50"
                                    class="form-control"></asp:TextBox>

                            </div>
                        </div>


                        <div class="form-group col-md-3">
                            <label for="textfield" class="control-label col-sm-3" style="">
                                Divisions</label>
                            <div class="col-sm-9" style="" id="Div41">
                                <asp:DropDownList ID="ddlforestdivisions" runat="server" class="form-control DropdownCss" Visible="false">
                                    <asp:ListItem Text="---Select---" Value="---Select---"></asp:ListItem>

                                </asp:DropDownList>

                                <asp:TextBox ID="txtdivisions" runat="server" MaxLength="50"
                                    class="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-md-3" id="Div1" runat="server">
                            <label for="textfield" class="control-label col-sm-5" style="">
                                Department<span id="Span2" runat="server" style="color: Red; display: inline;"></span></label>
                            <div class="col-sm-7" style="" id="Div2">
                                <asp:DropDownList ID="ddldepartment" runat="server" Visible="false" class="form-control DropdownCss">
                                    <asp:ListItem>---Select---</asp:ListItem>

                                    <asp:ListItem>PCCF</asp:ListItem>
                                </asp:DropDownList>

                                <asp:TextBox ID="txtdeprtment" runat="server" MaxLength="50"
                                    class="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-6" id="empid" runat="server">
                        <label for="textfield" class="control-label col-sm-5" style="">
                            Emp Code
                        </label>
                        <div class="col-sm-7" style="">
                            <asp:TextBox ID="txtEmpCode" runat="server" placeholder="Emp Code" MaxLength="50"
                                class="form-control" OnTextChanged="txtEmpCode_TextChanged" AutoPostBack="true"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group col-md-6" id="empname" runat="server">
                        <label for="textfield" class="control-label col-sm-5" style="">
                            Employee Name/ID<span id="Span1" runat="server" style="color: Red; display: inline;"></span></label>
                        <div class="col-sm-7" style="" id="">

                            <asp:DropDownList ID="ddlemployename" runat="server" class="form-control DropdownCss" AutoPostBack="true" OnSelectedIndexChanged="ddlemployename_SelectedIndexChanged">
                                <asp:ListItem Text="---Select---" Value="---Select---"></asp:ListItem>

                            </asp:DropDownList>

                        </div>
                    </div>




                    <div class="form-group col-md-6" id="Div5" runat="server">
                        <label for="textfield" class="control-label col-sm-5" style="">
                            Designation<span id="Span4" runat="server" style="color: Red; display: inline;"></span></label>
                        <div class="col-sm-7" style="" id="Div6">

                            <asp:TextBox ID="txtdesignation" runat="server" placeholder="Designation" MaxLength="50"
                                class="form-control"></asp:TextBox>

                        </div>
                    </div>

                    <div class="form-group col-md-6" id="Div7" runat="server">
                        <label for="textfield" class="control-label col-sm-5" style="">
                            Date Of Birth<span id="Span5" runat="server" style="color: Red; display: inline;"></span></label>
                        <div class="col-sm-7" style="" id="Div8">

                            <asp:TextBox ID="txtdateofbirth" runat="server" MaxLength="50"
                                class="form-control"></asp:TextBox>
                            <ajaxToolKit:CalendarExtender ID="CalendarExtender3" PopupButtonID="imgPopup" runat="server"
                                TargetControlID="txtdateofbirth" Format="MM-dd-yyyy" PopupPosition="BottomLeft">
                            </ajaxToolKit:CalendarExtender>

                        </div>
                    </div>
                    <div class="form-group col-md-6" id="Div9" runat="server">
                        <label for="textfield" class="control-label col-sm-5" style="">
                            Date Of entry into Service<span id="Span6" runat="server" style="color: Red; display: inline;"></span></label>
                        <div class="col-sm-7" style="" id="Div10">

                            <asp:TextBox ID="txtentryservice" runat="server" MaxLength="50"
                                class="form-control"></asp:TextBox>
                            <ajaxToolKit:CalendarExtender ID="CalendarExtender1" PopupButtonID="imgPopup" runat="server"
                                TargetControlID="txtentryservice" Format="MM-dd-yyyy" PopupPosition="BottomLeft">
                            </ajaxToolKit:CalendarExtender>

                        </div>
                    </div>
                    <div class="form-group col-md-6" id="Div11" runat="server">
                        <label for="textfield" class="control-label col-sm-5" style="">
                            Date Of Retirement/Death<span id="Span7" runat="server" style="color: Red; display: inline;"></span></label>
                        <div class="col-sm-7" style="" id="Div12">

                            <asp:TextBox ID="txtretirement" runat="server" MaxLength="50"
                                class="form-control"></asp:TextBox>
                            <ajaxToolKit:CalendarExtender ID="CalendarExtender2" PopupButtonID="imgPopup" runat="server"
                                TargetControlID="txtretirement" Format="MM-dd-yyyy" PopupPosition="BottomLeft">
                            </ajaxToolKit:CalendarExtender>

                        </div>
                    </div>
                    <div class="form-group col-md-6" id="Div13" runat="server">
                        <label for="textfield" class="control-label col-sm-5" style="">
                            Date of Increment Released/Sanctioned<span id="Span8" runat="server" style="color: Red; display: inline;"></span></label>
                        <div class="col-sm-7" style="" id="Div14">

                            <asp:TextBox ID="txtincrement" runat="server" MaxLength="50"
                                class="form-control"></asp:TextBox>
                            <ajaxToolKit:CalendarExtender ID="CalendarExtender4" PopupButtonID="imgPopup" runat="server"
                                TargetControlID="txtincrement" Format="MM-dd-yyyy" PopupPosition="BottomLeft">
                            </ajaxToolKit:CalendarExtender>

                        </div>
                    </div>

                    <div class="form-group col-md-6" id="Div15" runat="server">
                        <label for="textfield" class="control-label col-sm-5" style="">
                            Reasons for Not Released/Sanctioned<span id="Span9" runat="server" style="color: Red; display: inline;"></span></label>
                        <div class="col-sm-7" style="" id="Div16">

                            <asp:DropDownList ID="ddlresonforpending" runat="server" CssClass="form-control">
                                <asp:ListItem>---Select---</asp:ListItem>
                                <asp:ListItem>On Leave</asp:ListItem>
                                <asp:ListItem>Under Suspension</asp:ListItem>
                                <asp:ListItem>Non-Regularisation</asp:ListItem>
                                <asp:ListItem>Non-Declaration Of Probation</asp:ListItem>
                                <asp:ListItem>Punishments</asp:ListItem>

                            </asp:DropDownList>

                        </div>
                    </div>






                    <div class="form-group col-md-6" id="Div25" runat="server">
                        <label for="textfield" class="control-label col-sm-5" style="">
                            Resons for Change of Due Date Ofd Increments<span id="Span14" runat="server" style="color: Red; display: inline;"></span></label>
                        <div class="col-sm-7" style="" id="Div26">

                            <asp:DropDownList ID="ddlduedateincrementsresons" runat="server" CssClass="form-control">
                                <asp:ListItem>---Select---</asp:ListItem>
                                <asp:ListItem>EOL</asp:ListItem>
                                <asp:ListItem>Promotion</asp:ListItem>
                                <asp:ListItem>Punishments</asp:ListItem>
                                <asp:ListItem>Suspension</asp:ListItem>


                            </asp:DropDownList>

                        </div>
                    </div>


                    <div class="form-group col-md-6" id="Div29" runat="server">
                        <label for="textfield" class="control-label col-sm-5" style="">
                            Due Date Of sanction of AAS Increment(Automatic Advancement Scheme)<span id="Span16" runat="server" style="color: Red; display: inline;"></span></label>
                        <div class="col-sm-7" style="" id="Div30">

                            <asp:DropDownList ID="ddlaas" runat="server" CssClass="form-control">
                                <asp:ListItem>---Select---</asp:ListItem>
                                <asp:ListItem>SGP</asp:ListItem>
                                <asp:ListItem>SAPP-I(A)</asp:ListItem>
                                <asp:ListItem>SAPP-I(B)</asp:ListItem>
                                <asp:ListItem>SAPP-II</asp:ListItem>
                            </asp:DropDownList>

                        </div>
                    </div>
                    <div class="form-group col-md-6" id="Div3" runat="server">
                        <label for="textfield" class="control-label col-sm-5" style="">
                            Date Of AAS Increment Sanctioned<span id="Span3" runat="server" style="color: Red; display: inline;"></span></label>
                        <div class="col-sm-7" style="" id="Div4">

                            <asp:DropDownList ID="ddlaassanctioned" runat="server" CssClass="form-control">
                                <asp:ListItem>---Select---</asp:ListItem>
                                <asp:ListItem>SGP</asp:ListItem>
                                <asp:ListItem>SAPP-I(A)</asp:ListItem>
                                <asp:ListItem>SAPP-I(B)</asp:ListItem>
                                <asp:ListItem>SAPP-II</asp:ListItem>
                            </asp:DropDownList>

                        </div>
                    </div>
                    <div class="form-group col-md-6" id="Div17" runat="server">
                        <label for="textfield" class="control-label col-sm-5" style="">
                            Reasons for not Sanctioning AAS increments<span id="Span10" runat="server" style="color: Red; display: inline;"></span></label>
                        <div class="col-sm-7" style="" id="Div18">

                            <asp:DropDownList ID="ddlnotsanctionaas" runat="server" CssClass="form-control">
                                <asp:ListItem>---Select---</asp:ListItem>
                                <asp:ListItem>EOL</asp:ListItem>
                                <asp:ListItem>Disc Cases Pending</asp:ListItem>
                                <asp:ListItem>Punishments</asp:ListItem>



                            </asp:DropDownList>

                        </div>
                    </div>
                    <div class="form-group col-md-6" id="Div31" runat="server">
                        <label for="textfield" class="control-label col-sm-5" style="">
                            Name Of the Employee Added/Deleted due to<span id="Span17" runat="server" style="color: Red; display: inline;"></span></label>
                        <div class="col-sm-7" style="" id="Div32">

                            <asp:DropDownList ID="ddladded" runat="server" CssClass="form-control">
                                <asp:ListItem>---Select---</asp:ListItem>
                                <asp:ListItem>Transfer</asp:ListItem>
                                <asp:ListItem>Promotion</asp:ListItem>
                                <asp:ListItem>appt</asp:ListItem>
                                <asp:ListItem>retired</asp:ListItem>
                                <asp:ListItem>Death</asp:ListItem>

                            </asp:DropDownList>

                        </div>
                    </div>




                    <div class="form-group col-md-6" id="Div37" runat="server">
                        <label for="textfield" class="control-label col-sm-5" style="">
                            Remarks<span id="Span20" runat="server" style="color: Red; display: inline;"></span></label>
                        <div class="col-sm-7" style="" id="Div38">

                            <asp:TextBox ID="TextBox6" runat="server" placeholder="Remarks" MaxLength="50"
                                class="form-control"></asp:TextBox>

                        </div>
                    </div>


                    <div class="col-md-12 text-center">
                        <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary" />
                        <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-primary" />
                        <asp:Button ID="btnCancel" runat="server" Text="Reset" class="btn" />
                    </div>
                </div>



                <div class="row">
                    <div class="col-sm-12">
                        <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                            <div class="box-title">
                                <h3>
                                    <i class="fa fa-th-list"></i>Increament Details</h3>
                            </div>
                            <div class="box-content">
                                <div class="col-md-12 table-responsive">
                                    <asp:GridView ID="grdincrementreport" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                        Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%;" runat="server"
                                        ShowFooter="True" AutoGenerateColumns="False" DataKeyNames="Sno">
                                        <AlternatingRowStyle BackColor="#EEEEEE" />
                                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                        <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                        <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                        <SortedDescendingHeaderStyle BackColor="#000065" />
                                        <Columns>
                                            <asp:BoundField ReadOnly="true" HeaderText="Sno" DataField="Sno" />
                                            <asp:TemplateField HeaderText="Employee ID">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Employee_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Employee Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("Employee_Name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Department">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("Name_Of_Circle") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Name Of The Employee Retired & retiring before (18) Months">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label15" runat="server" Text='<%# Eval("Employee_Name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Designation">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Date Of Birth">
                                                <ItemTemplate>
                                                    <%--  <asp:Label ID="Label5" runat="server" Text='<%# Eval("Emp_DOB", "{0:dd/MM/yyyy}") %>'></asp:Label>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>




                                            <asp:TemplateField HeaderText="Date Of Retirement/Death">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label8" runat="server" Text='<%# Eval("Date_retirement_death", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="No of Pension Papers Record Cummulative">
                                                <ItemTemplate>
                                                    <%-- <asp:Label ID="Label10" runat="server" Text='<%# Eval("due_leave") %>'></asp:Label>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="No of Pension Papers Balance to be Received">
                                                <ItemTemplate>
                                                    <%-- <asp:Label ID="Label10" runat="server" Text='<%# Eval("due_leave") %>'></asp:Label>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Forwarding Of Pension Papers With in (6) Months">
                                                <ItemTemplate>
                                                    <%-- <asp:Label ID="Label10" runat="server" Text='<%# Eval("due_leave") %>'></asp:Label>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status On Processing PPs">
                                                <ItemTemplate>
                                                    <%--  <asp:Label ID="Label11" runat="server" Text='<%# Eval("receipt_pps") %>'></asp:Label>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Date Of Forwarding PPs to The AG/AD">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label12" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Date Of Receipt of PVR">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label13" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Forwarding Sanction Order,LPC & NDC to the AG/SAD by PSA">
                                                <ItemTemplate>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Weather Autherization on Issued by AG/SAD">
                                                <ItemTemplate>
                                                    <%-- <asp:Label ID="Label15" runat="server" Text='<%# Eval("transfer") %>'></asp:Label>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Resons Not Sending of authorisation">
                                                <ItemTemplate>
                                                    <%--   <asp:Label ID="Label16" runat="server" Text='<%# Eval("transfer") %>'></asp:Label>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Resons Not Sending of authorisation">
                                                <ItemTemplate>
                                                    <%--   <asp:Label ID="Label16" runat="server" Text='<%# Eval("transfer") %>'></asp:Label>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Provisional">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label17" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Anticipatory">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label18" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Family">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label19" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Action Taken On Conclusion of Deptl.proc and Issue Of Autherisation">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label20" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Remarks">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label21" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</asp:Content>
