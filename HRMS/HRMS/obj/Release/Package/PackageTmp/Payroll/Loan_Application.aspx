﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="Loan_Application.aspx.cs" Inherits="HRMS.Payroll.Loan_Application" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="#">Payroll</a><i class="fa fa-angle-right"></i> </li>
                        <li><a href="Define_Leave_List.aspx" class="current">Loan Application</a> </li>
                    </ul>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <i class="fa fa-th-list">Loan Application</i>
                        </header>
                        <div class="panel-body">
                            <div class="col-md-12" style="">
                                <div class="col-md-12" style="">
                                    <div class="form-group col-md-6">
                                        <label for="textfield" class="control-label col-sm-5" style="">
                                            Emp Code
                                        </label>
                                        <div class="col-sm-7" style="">
                                            <asp:TextBox ID="txtEmpCode" runat="server" placeholder="Emp Code" MaxLength="50"
                                                class="form-control" ReadOnly="true" AutoPostBack="true" OnTextChanged="txtEmpCode_OnTextChanged"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="textfield" class="control-label col-sm-5" style="">
                                            Employee Name<span id="Span2" runat="server" style="color: Red; display: inline;">*</span></label>
                                        <div class="col-sm-7 tAEmpName" style="" id="">
                                            <asp:DropDownList ID="ddlEmpName" runat="server" Visible="false" class="form-control"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlEmpName_SelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtEmpName" runat="server" placeholder="Emp Name" MaxLength="150"
                                                class="form-control" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group col-md-6">
                                        <label for="textfield" class="control-label col-sm-5" style="">
                                            Department<span id="Span3" runat="server" style="color: Red; display: inline;">*</span></label>
                                        <div class="col-sm-7 " style="" id="Div1">

                                            <asp:TextBox ID="txtDepartment" runat="server" placeholder="Department" class="form-control"
                                                onkeydown="return jsDecimals(event);" MaxLength="50" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="textfield" class="control-label col-sm-5" style="">
                                            Designation<span id="Span4" runat="server" style="color: Red; display: inline;">*</span></label>
                                        <div class="col-sm-7" style="" id="Div2">
                                            <asp:TextBox ID="txtDesignation" runat="server" placeholder="Designation" class="form-control"
                                                onkeydown="return jsDecimals(event);" MaxLength="50" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="textfield" class="control-label col-sm-5" style="">
                                            Reporting Authority<span id="Span5" runat="server" style="color: Red; display: inline;">*</span></label>
                                        <div class="col-sm-7" style="" id="Div3">

                                            <asp:TextBox ID="txtReportingAuthority" runat="server" placeholder="Reporting Authority"
                                                class="form-control" onkeydown="return jsDecimals(event);" MaxLength="50" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group col-md-6">
                                        <label for="textfield" class="control-label col-sm-5" style="">
                                            Type Of Loan Apply<span style="color: Red; display: inline;">*</span></label>
                                        <div class="col-sm-7" style="" id="Div4">
                                            <asp:DropDownList ID="ddlLeaveApply" runat="server" class="form-control DropdownCss" AutoPostBack="True" OnSelectedIndexChanged="ddlLeaveApply_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="textfield" class="control-label col-sm-5" style="">
                                            Status<span style="color: Red; display: inline;">*</span></label>
                                        <div class="col-sm-7" style="" id="Div8">
                                            <asp:DropDownList ID="ddlstatus" runat="server" class="form-control DropdownCss">
                                                <asp:ListItem Text="Wating For Approval" Value="Wating For Approval"></asp:ListItem>

                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="textfield" class="control-label col-sm-5" style="" id="lbltenure">
                                            Tenure<span id="Span6" runat="server" style="color: Red; display: inline;">*</span></label>
                                        <div class="col-sm-7" style="" id="Div5">
                                            <asp:TextBox ID="txttenure" runat="server" placeholder="Tenure" class="form-control"
                                                onkeydown="return jsDecimals(event);" MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-12" style="">
                                    <div class="form-group col-md-6">
                                        <label for="textfield" class="control-label col-sm-5" style="">
                                            Basic Amount<span style="color: #ff0000; font-size: 14px; display: inline;">*</span>
                                        </label>
                                        <div class="col-sm-7" style="">

                                            <asp:TextBox ID="txtbasicamount" runat="server" placeholder="Basic Amount" class="form-control"
                                                MaxLength="50" ReadOnly="true"></asp:TextBox>

                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="textfield" class="control-label col-sm-5" style="">
                                            Eligibility Amount<span id="Span1" runat="server" style="color: Red; display: inline;">*</span></label>
                                        <div class="col-sm-7" style="" id="Div6">
                                            <asp:TextBox ID="txtelgamount" runat="server" TabIndex="0" placeholder="Eligibility Amount" class="form-control"
                                                MaxLength="50" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="textfield" class="control-label col-sm-5" style="">
                                            Loan Amount Required
                                        </label>
                                        <div class="col-sm-7" style="">
                                            <asp:TextBox ID="txtlar" runat="server" placeholder="Enhanced Maximum Celling" MaxLength="50"
                                                class="form-control" ReadOnly="false"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-6">

                                        <label for="textfield" class="control-label col-sm-5" style="">
                                            Rate Of Intrest<span style="color: #ff0000; font-size: 14px; display: inline;">*</span></label>
                                        <div class="col-sm-7" style="" id="Div7">
                                            <asp:TextBox ID="txtroi" runat="server" TabIndex="1" placeholder="Rate Of Intrest" class="form-control"
                                                MaxLength="50" AutoPostBack="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-7" style="" id="Div9">
                                        <asp:TextBox ID="txtleavecode" runat="server" placeholder="Tenure" class="form-control"
                                            onkeydown="return jsDecimals(event);" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 text-center">
                                <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary" OnClick="btnSave_Click" /><%--OnClientClick="return fileUploadValidation()"--%>
                                <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-primary" OnClick="btnUpdate_Click" />
                                <asp:Button ID="btnCancel" runat="server" Text="Reset" class="btn" />
                            </div>
                        </div>
                    </section>

                </div>
            </div>
        </section>
    </section>
</asp:Content>
