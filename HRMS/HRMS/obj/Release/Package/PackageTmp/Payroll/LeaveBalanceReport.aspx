﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true"
    CodeBehind="LeaveBalanceReport.aspx.cs" Inherits="HRMS.Payroll.Leave_Balance_Report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="#">Reports</a><i class="fa fa-angle-right"></i> </li>
                        <li><a href="../Payroll/Leave_Balance_Report.aspx" class="current">Leave Report</a> </li>
                    </ul>
                </div>
            </div>

        </section>
    </section>
</asp:Content>
