﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="Payrollprocessing.aspx.cs" Inherits="HRMS.Payroll.Payrollprocessing" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx">
                        <%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i></li>
                    <li><a href="">Payroll</a><i class="fa fa-angle-right"></i> </li>
                    <%--<li><a href="Leave_Application_List.aspx">Leave Application Details</a> <i class="fa fa-angle-right"></i></li>--%>
                    <li><a href="">Payroll Processing</a> </li>
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                <div class="box-title">
                    <h3>
                        <i class="fa fa-th-list"></i>Payroll Processing</h3>
                </div>
                <div class="box-content">
                    <form action="#" method="POST" class='form-horizontal form-bordered'>
                        <div class="col-md-12">
                            <div class="form-group col-md-6" id="empid" runat="server">
                                <label for="textfield" class="control-label col-sm-5" style="">
                                    Emp Code
                                </label>
                                <div class="col-sm-7" style="">
                                    <asp:TextBox ID="txtEmpCode" runat="server" placeholder="Emp Code" MaxLength="50"
                                        class="form-control" AutoPostBack="true" OnTextChanged="txtEmpCode_TextChanged"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group col-md-6" id="empname" runat="server">
                                <label for="textfield" class="control-label col-sm-5" style="">
                                    Employee Name<span id="Span1" runat="server" style="color: Red; display: inline;">*</span></label>
                                <div class="col-sm-7" style="" id="">
                                    <asp:DropDownList ID="ddlEmpName" runat="server" class="form-control DropdownCss" TabIndex="1" AutoPostBack="True" OnSelectedIndexChanged="ddlEmpName_SelectedIndexChanged">
                                    </asp:DropDownList>

                                </div>
                            </div>
                        </div>
                        <%-- <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lADepartment" style="">
                                                Month<span id="Span3" runat="server" style="color: Red; display: inline;">*</span></label>
                                           <div class="col-sm-2 tNoofDays" style="" id="Div1">
                                                        <asp:TextBox ID="TextBox3" runat="server" placeholder="Month" class="form-control"
                                                            onkeydown="return jsDecimals(event);" MaxLength="20" AutoPostBack="True" ></asp:TextBox>
                                                    </div>
                                        </div>
                                        <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lANoOfDays" style="">
                                                        year</label>
                                                    <div class="col-sm-2 tNoofDays" style="" id="Div2">
                                                        <asp:TextBox ID="TextBox1" runat="server" placeholder="Year" class="form-control"
                                                            onkeydown="return jsDecimals(event);" MaxLength="20" AutoPostBack="True" ></asp:TextBox>
                                                    </div>
                                                </div>
                                      <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lANoOfDays" style="">
                                                        Present Days</label>
                                                    <div class="col-sm-2 tNoofDays" style="" id="Div3">
                                                        <asp:TextBox ID="TextBox2" runat="server" placeholder="Present Days" class="form-control"
                                                            onkeydown="return jsDecimals(event);" MaxLength="20" AutoPostBack="True" ></asp:TextBox>
                                                    </div>
                                                </div>
                                    </div>--%>
                        <div class="col-md-12 text-center">
                            <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary" OnClick="btnSave_Click" />
                        </div>
                    </form>
                    <div id="Div4" class="col-md-12" style="width: 100%; height: auto; overflow: auto" runat="server">
        <div class="table-responsive">
            <asp:GridView ID="grdpayroll" class="table table-bordered CustomerAddress" runat="server"
                ShowFooter="True" AutoGenerateColumns="False" DataKeyNames="Sno">
                <Columns>
                    <asp:BoundField ReadOnly="true" HeaderText="Sno" DataField="Sno" />

                    <asp:TemplateField HeaderText="Employee Code">
                        <ItemTemplate>
                            <asp:Label ID="lblempcode" runat="server" Text='<%# Eval("Emp_Code")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Month">
                        <ItemTemplate>
                            <asp:Label ID="lblmonth" runat="server" Text=""></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Year">
                        <ItemTemplate>
                            <asp:Label ID="lblyear" runat="server" Text=""></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Present Days">
                        <ItemTemplate>
                            <asp:Label ID="lblpresent" runat="server" Text=""></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Basic">
                        <ItemTemplate>
                            <asp:Label ID="lblbasci" runat="server" Text='<%# Eval("Basic_Amount")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Gross">
                        <ItemTemplate>
                            <asp:Label ID="lblgross" runat="server" Text='<%# Eval("Gross_Amount")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="DA">
                        <ItemTemplate>
                            <asp:Label ID="lblda" runat="server" Text='<%# Eval("DA")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="HRA">
                        <ItemTemplate>
                            <asp:Label ID="lblhra" runat="server" Text='<%# Eval("HRA")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="CCA">
                        <ItemTemplate>
                            <asp:Label ID="lblcca" runat="server" Text='<%# Eval("CCA")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="OA">
                        <ItemTemplate>
                            <asp:Label ID="lbloa" runat="server" Text='<%# Eval("OA")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="PF">
                        <ItemTemplate>
                            <asp:Label ID="lblpf" runat="server" Text='<%# Eval("PF")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ESI">
                        <ItemTemplate>
                            <asp:Label ID="lblesi" runat="server" Text='<%# Eval("ESI")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="EESI">
                        <ItemTemplate>
                            <asp:Label ID="lbleesi" runat="server" Text='<%# Eval("EESI")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="PTAX">
                        <ItemTemplate>
                            <asp:Label ID="lblptax" runat="server" Text='<%# Eval("PTAX")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="TDS">
                        <ItemTemplate>
                            <asp:Label ID="lbltds" runat="server" Text='<%# Eval("TDS")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="APGLI">
                        <ItemTemplate>
                            <asp:Label ID="lblapgli" runat="server" Text='<%# Eval("APGLI")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="GIS">
                        <ItemTemplate>
                            <asp:Label ID="lblgis" runat="server" Text='<%# Eval("GIS")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="MCA">
                        <ItemTemplate>
                            <asp:Label ID="lblmca" runat="server" Text='<%# Eval("MCA")%>'></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="BA">
                        <ItemTemplate>
                            <asp:Label ID="lblba" runat="server" Text='<%# Eval("BA")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>




                </Columns>
            </asp:GridView>

        </div>
    </div>
                </div>

            </div>
        </div>
    </div>
    
</asp:Content>
