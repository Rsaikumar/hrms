﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="DefineLeaves.aspx.cs" Inherits="HRMS.Payroll.Define_Leaves" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/Payroll/Define_leaves.css" rel="stylesheet" />

    <script type="text/javascript" lang="javascript">
        function ControlValid() {
            if (document.getElementById('<%=txtLeaveName.ClientID%>').value == "") {
                alert("Please Enter Leave Name");

                document.getElementById('<%=txtLeaveName.ClientID%>').focus();

                return false;
            }
            if (document.getElementById('<%=txtShortName.ClientID%>').value == "") {
                alert("Please Enter Short Name");

                document.getElementById('<%=txtShortName.ClientID%>').focus();

                return false;
            }
            if (document.getElementById('<%=ddlAllotment.ClientID%>').options[document.getElementById('<%=ddlAllotment.ClientID%>').selectedIndex].value == 0) {
                alert("Please Select Allotment..");
                document.getElementById('<%=ddlAllotment.ClientID%>').focus();
                return false;
            }
            return true;

        }
        function jsDecimals(e) {

            var evt = (e) ? e : window.event;
            var key = (evt.keyCode) ? evt.keyCode : evt.which;
            if (key != null) {
                key = parseInt(key, 10);
                if ((key < 48 || key > 57) && (key < 96 || key > 105)) {
                    if (!jsIsUserFriendlyChar(key, "Decimals")) {
                        return false;
                    }
                }
                else {
                    if (evt.shiftKey) {
                        return false;
                    }
                }
            }
            return true;
        }

        // Function to check for user friendly keys  
        //------------------------------------------
        function jsIsUserFriendlyChar(val, step) {
            // Backspace, Tab, Enter, Insert, and Delete  
            if (val == 8 || val == 9 || val == 13 || val == 45 || val == 46) {
                return true;
            }
            // Ctrl, Alt, CapsLock, Home, End, and Arrows  
            if ((val > 16 && val < 21) || (val > 34 && val < 41)) {
                return true;
            }
            if (step == "Decimals") {
                if (val == 190 || val == 110) {  //Check dot key code should be allowed
                    return true;
                }
            }
            // The rest  
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="#">Payroll</a><i class="fa fa-angle-right"></i> </li>
                        <li><a href="../Payroll/Define_Leaves.aspx" class="current">Define Leaves</a> </li>
                    </ul>
                </div>
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-sm-12">
                            <section class="panel">
                                <header class="panel-heading">
                                    <i class="fa fa-th-list">Define Leave Types</i>
                                </header>

                                <div class="panel-body">
                                    <div class="col-sm-12">
                                        <div class="col-md-6 form-group" style="">
                                            <label for="textfield" class="control-label col-sm-5" style="">
                                                ID
                                            </label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtID" runat="server" placeholder="ID" MaxLength="50"
                                                    class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label for="textfield" class="control-label col-sm-5">
                                                Leave Name<span id="Span1" runat="server" style="color: Red; display: inline;">*</span></label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtLeaveName" runat="server" TabIndex="0" placeholder="Leave Name" class="form-control"
                                                    MaxLength="50" AutoPostBack="true" OnTextChanged="txtLeaveName_TextChanged"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label for="textfield" class="control-label col-sm-5" style="">
                                                Short Name<span style="color: #ff0000; font-size: 14px; display: inline;">*</span></label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtShortName" runat="server" TabIndex="1" placeholder="Short Name" class="form-control"
                                                    MaxLength="50" AutoPostBack="true" OnTextChanged="txtShortName_TextChanged"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <div class="col-md-1">
                                                <asp:CheckBox ID="chbCertification" runat="server" Text="" AutoPostBack="true" OnCheckedChanged="chbEncashment_CheckedChanged" />
                                            </div>
                                            <div>
                                                <label for="textfield" class="control-label col-md-11" style="">
                                                    Document Evidence is required..?</label>
                                            </div>
                                        </div>
                                        <div class=" col-md-6 form-group">
                                            <div class="col-sm-1">
                                                <asp:CheckBox ID="chbEncashment" runat="server" Text="" AutoPostBack="true" OnCheckedChanged="chbEncashment_CheckedChanged" />
                                            </div>
                                            <label for="textfield" class="control-label col-sm-11" style="">
                                                Encashment Allowed..?
                                            </label>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <div class="col-sm-1">
                                                <asp:CheckBox ID="chbBalances" runat="server" Text="" AutoPostBack="true" OnCheckedChanged="chbEncashment_CheckedChanged" />
                                            </div>
                                            <label for="textfield" class="control-label col-sm-11">
                                                Balance Transfer..?
                                            </label>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label for="textfield" class="control-label col-sm-5">
                                                Balance Transfer/Encashing Limit
                                            </label>
                                            <div class="col-sm-7">
                                                <asp:TextBox ID="txtEncashingLimit" runat="server" placeholder="NA" class="form-control" onkeydown="return jsDecimals(event);"
                                                    MaxLength="20">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="textfield" class="control-label col-sm-5">
                                            Allotment<span style="color: #ff0000; font-size: 14px; display: inline;">*</span>
                                        </label>
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlAllotment" runat="server" class="form-control DropdownCss">
                                                <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                <asp:ListItem Text="Auto" Value="Auto"></asp:ListItem>
                                                <asp:ListItem Text="Manual" Value="Manual"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="textfield" class="control-label col-sm-5">
                                            No.of Leaves Per year<span id="tinValue" runat="server" style="color: Red;">*</span>
                                        </label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtPerOfBasic" runat="server" placeholder="No.of Leaves Per year" class="form-control" onkeydown="return jsDecimals(event);"
                                                MaxLength="20"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <asp:Panel runat="server" GroupingText="Leave Limitations">
                                        <div class="col-sm-12" style="">
                                            <div class="col-md-6 form-group">
                                                <label for="textfield" class="control-label col-sm-3" style="">
                                                    Period
                                                </label>
                                                <div class="col-sm-7">
                                                    <asp:DropDownList ID="ddlperiod" runat="server" class="form-control DropdownCss">
                                                        <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                        <asp:ListItem Text="Monthly" Value="Monthly"></asp:ListItem>
                                                        <asp:ListItem Text="Quarterly" Value="Quarterly"></asp:ListItem>
                                                        <asp:ListItem Text="Halfly" Value="Halfly"></asp:ListItem>
                                                        <asp:ListItem Text="Annually" Value="Annually"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class=" col-md-6 form-group">
                                                <label for="textfield" class="control-label col-sm-3">
                                                    No.Of Times
                                                </label>
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txtnooftimes" runat="server" placeholder="No.Of Times" class="form-control"
                                                        MaxLength="50"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-6 form-group">
                                                <label for="textfield" class="control-label col-sm-3">
                                                    No.Of Days
                                                </label>
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txtdays" runat="server" placeholder="No.Of Days" class="form-control"
                                                        MaxLength="50"></asp:TextBox>
                                                </div>
                                            </div>
                                    </asp:Panel>

                                    <div class="col-sm-5 ACButtons">
                                        <asp:Button ID="btnSave" runat="server" Text="Submit" class="btn btn-primary" OnClientClick="return ControlValid()" OnClick="btnSave_Click" />
                                        <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-primary" OnClick="btnUpdate_Click" />
                                    </div>
                                </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </section>
    </section>

</asp:Content>
