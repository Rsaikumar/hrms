﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true"
    CodeBehind="LeaveApplication.aspx.cs" Inherits="HRMS.Payroll.Leave_Application" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/Payroll/Leave_Aplication.css" rel="stylesheet" />
    <style type="text/css">
        #ContentPlaceHolder1_actdate_container, #ContentPlaceHolder1_CalendarExtender2_popupDiv, #ContentPlaceHolder1_CalendarExtender3_popupDiv, #ContentPlaceHolder1_CalendarExtender1_container, #ContentPlaceHolder1_CalendarExtender2_container, #ContentPlaceHolder1_actdate_popupDiv, #ContentPlaceHolder1_CalendarExtender1_popupDiv {
            z-index: 10000;
        }

        .DateHide {
            display: none;
        }
    </style>
    <script type="text/javascript">
        //disable sunday
        function detect_sunday(sender, args) {
            if (sender._selectedDate.getDay() == 0) {
                sender._selectedDate = new Date();
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format));
                alert("You can't select sunday!");
            }
        }
    </script>
    <script type="text/javascript">
        function Validate() {
            var ddlleave = document.getElementById("<%=ddlLeaveApply.ClientID %>");
            if (ddlleave.value == "Select") {
                //If the "Please Select" option is selected display error.
                alert("Please select any Leave Type!");
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="#">Payroll</a><i class="fa fa-angle-right"></i> </li>
                        <li><a href="../Payroll/LeaveApplication.aspx" class="current">Leave Application</a> </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading tab-bg-dark-navy-blue ">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a data-toggle="tab" href="#personal" aria-expanded="false"><i class="fa fa-user">Leave Application</i></a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#services" aria-expanded="true"><i class="fa fa-user">My Requests</i></a>
                                </li>
                            </ul>
                        </header>
                        <div class="panel-body">
                            <div class="tab-content">
                                <div id="personal" class="tab-pane active">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <section class="panel">
                                                    <header class="panel-heading">
                                                        <i class="fa fa-th-list">Leave Application</i>
                                                    </header>
                                                    <section class="panel-body">
                                                        <div class="form-group col-md-6" visible="false">
                                                            <label for="textfield" runat="server" id="lblempcode" class="control-label col-sm-5" style="">
                                                                Emp Code
                                                            </label>
                                                            <div class="col-sm-7" style="">
                                                                <asp:TextBox ID="txtEmpCode" runat="server" placeholder="Emp Code" MaxLength="50"
                                                                    class="form-control" ReadOnly="true" AutoPostBack="true" OnTextChanged="txtEmpCode_OnTextChanged"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-4" style="">
                                                          Name</label>
                                                            <div class="col-sm-8" style="" id="Div1">
                                                                <asp:TextBox ID="txtEmpName" runat="server" placeholder="Emp Name" class="form-control" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-4" style="">
                                                                Department</label>
                                                            <div class="col-sm-8" style="" id="">
                                                                <asp:DropDownList ID="ddlDepartment" runat="server" Style="display: none;" class="form-control DropdownCss">
                                                                </asp:DropDownList>
                                                                <asp:TextBox ID="txtDepartment" runat="server" placeholder="Department" class="form-control"
                                                                    onkeydown="return jsDecimals(event);" MaxLength="50" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-4" style="">
                                                                Designation</label>
                                                            <div class="col-sm-8" style="" id="">
                                                                <asp:DropDownList ID="ddlDesignation" runat="server" Style="display: none;" class="form-control DropdownCss">
                                                                </asp:DropDownList>
                                                                <asp:TextBox ID="txtDesignation" runat="server" placeholder="Designation" class="form-control"
                                                                    onkeydown="return jsDecimals(event);" MaxLength="50" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div id="Div4" class="form-group col-md-6" runat="server">
                                                            <label for="textfield" class="control-label col-sm-4" style="">
                                                                Reporting</label>
                                                            <div class="col-sm-8" style="" id="Div5">
                                                                <asp:TextBox ID="txtreportingauthority" runat="server" placeholder="Ex:dd-mm-yyyy" class="form-control"
                                                                    MaxLength="20" AutoPostBack="true" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </section>
                                                <section class="panel">
                                                    <div class="panel-body">
                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                            <ContentTemplate>
                                                                <div class="form-group col-md-6">
                                                                    <label for="textfield" class="control-label col-sm-4" style="">
                                                                        Leave Type<span style="color: Red; display: inline;">*</span></label>
                                                                    <div class="col-sm-8" style="" id="">
                                                                        <asp:DropDownList ID="ddlLeaveApply" runat="server" class="form-control DropdownCss"
                                                                            AutoPostBack="True" OnSelectedIndexChanged="ddlLeaveApply_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label for="textfield" class="control-label col-sm-4" style="">
                                                                        From Date<span id="Span4" runat="server" style="color: Red; display: inline;">*</span></label>
                                                                    <div class="col-sm-8" style="" id="Div2">
                                                                        <asp:TextBox ID="txtFromDate" runat="server" placeholder="Ex:dd-mm-yyyy" required class="form-control"
                                                                            MaxLength="20" AutoPostBack="true" OnTextChanged="txtFromDate_OnTextChanged"></asp:TextBox>
                                                                        <cc1:CalendarExtender ID="actdate" PopupButtonID="imgPopup" runat="server" TargetControlID="txtFromDate"
                                                                            Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                                        </cc1:CalendarExtender>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label for="textfield" class="control-label col-sm-4" style="">
                                                                        To Date<span id="Span1" runat="server" style="color: Red; display: inline;">*</span></label>
                                                                    <div class="col-sm-8" style="" id="Div3">
                                                                        <asp:TextBox ID="txtToDate" runat="server" required placeholder="Ex:dd-mm-yyyy" class="form-control"
                                                                            MaxLength="20" AutoPostBack="true" OnTextChanged="txtToDate_TextChanged"></asp:TextBox>
                                                                        <cc1:CalendarExtender ID="CalendarExtender1" PopupButtonID="imgPopup" runat="server"
                                                                            TargetControlID="txtToDate" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                                        </cc1:CalendarExtender>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group col-md-6">
                                                                    <label for="textfield" class="control-label col-sm-4" style="">
                                                                        No Of Days</label>
                                                                    <div class="col-sm-8" style="" id="">
                                                                        <asp:TextBox ID="txtNoofDays" runat="server" placeholder="No of Days" class="form-control"
                                                                            onkeydown="return jsDecimals(event);" MaxLength="20" AutoPostBack="True" ReadOnly="true"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-md-6" id="divstatus" runat="server">
                                                                    <label for="textfield" class="control-label col-sm-4" style="">
                                                                        Status<span style="color: #ff0000; font-size: 14px; display: inline;">*</span>
                                                                    </label>
                                                                    <div class="col-sm-8" style="">
                                                                        <asp:TextBox ID="txtstatus" runat="server" placeholder="status" class="form-control"
                                                                            onkeydown="return jsDecimals(event);" MaxLength="20" ReadOnly="true"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-md-6" id="divoofleaves" runat="server">
                                                                    <label for="textfield" class="control-label col-sm-4" style="">
                                                                        No Of Leaves</label>
                                                                    <div class="col-sm-8" style="" id="Div7">
                                                                        <asp:TextBox ID="txtnoofleaves" runat="server" placeholder="No of Leaves" class="form-control"
                                                                            onkeydown="return jsDecimals(event);" MaxLength="20" AutoPostBack="True" ReadOnly="true"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-md-6" id="uploadCertification" visible="false" runat="server">
                                                                    <label for="textfield" class="control-label col-sm-5 ">
                                                                        Upload Cretification<span style="color: Red; display: inline;">*</span></label>
                                                                    <div class="col-sm-7" style="" id="Div6">
                                                                        <asp:FileUpload ID="fupCertification" runat="server" />
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                        <div class="col-md-12 text-center">
                                                            <asp:Button ID="btnSave" runat="server" Text="Submit" OnClientClick="return Validate();" class="btn btn-primary" OnClick="btnSave_Click" />
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                            <div class="col-sm-3">
                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                    <ContentTemplate>
                                                        <div class="panel">
                                                            <header class="panel-heading">
                                                                <i class="fa fa-th-list">Leave Available</i>
                                                            </header>

                                                            <div class="panel-body">
                                                                <div style="max-height: 515px; overflow: auto;" class="table-responsive">

                                                                    <asp:GridView ID="GdvBalanceLeaves" runat="server" EmptyDataText="No records Found" AutoGenerateColumns="false"  CssClass="mydatagrid" HeaderStyle-CssClass="headergrid" RowStyle-CssClass="rows">
                                                                        <Columns>
                                                                            <asp:BoundField DataField="ShortName" HeaderText="Short Name" ItemStyle-Width="150">
                                                                                <ItemStyle Width="150px"></ItemStyle>
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="NoOfLeaves" HeaderText="No Of Leaves" ItemStyle-Width="150">
                                                                                <ItemStyle Width="150px"></ItemStyle>
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                            <%-- <div class="panel-footer">
                                                              <asp:LinkButton ID="lnkbtnpast" runat="server" OnClick="lnkbtnpast_Click"><b>View Past Year Details</b></asp:LinkButton>
                                                            </div>--%>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="services" class="tab-pane">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div style="max-height: 515px; overflow: auto;" class="table-responsive">
                                                <asp:GridView ID="gdvleaveapproved" CssClass="mydatagrid" HeaderStyle-CssClass="headergrid" RowStyle-CssClass="rows"
                                                    runat="server" Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%; overflow: scroll;" EmptyDataText="No records Found" AutoGenerateColumns="False" OnRowDataBound="gdvleaveapproved_RowDataBound" OnSelectedIndexChanged="gdvleaveapproved_SelectedIndexChanged">
                                                    <Columns>
                                                        <asp:BoundField DataField="ID" HeaderText="Leave No" ItemStyle-Width="150">
                                                            <ItemStyle Width="150px"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="empname" HeaderText="Employee Name" ItemStyle-Width="150">
                                                            <ItemStyle Width="150px"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="leavetype" HeaderText="Leave Type" ItemStyle-Width="200">
                                                            <ItemStyle Width="200px"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="fromdate" HeaderText="From Date" DataFormatString="{0:d}" ItemStyle-Width="200">
                                                            <ItemStyle Width="200px"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="todate" HeaderText="To Date" DataFormatString="{0:d}" ItemStyle-Width="200">
                                                            <ItemStyle Width="200px"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="totaldays" HeaderText="No Of Days" ItemStyle-Width="200">
                                                            <ItemStyle Width="200px"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="LeaveStatus" HeaderText="Status" ItemStyle-Width="200">
                                                            <ItemStyle Width="200px"></ItemStyle>
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>

</asp:Content>
