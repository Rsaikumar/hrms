﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="Leave_Allotment.aspx.cs" Inherits="HRMS.Payroll.Leave_Allotment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/Payroll/Leave_Allotment.css" rel="stylesheet" />
    <script type="text/javascript" lang="javascript">
        function ControlValid() {
            if (document.getElementById('<%=txtEmpCode.ClientID%>').value == "") {
                alert("Please Enter the Emp Code");

                document.getElementById('<%=txtEmpCode.ClientID%>').focus();

                return false;
            }
            if (document.getElementById('<%=ddlEmpName.ClientID%>').value == "") {
                alert("Please Select the Emp Name");

                document.getElementById('<%=ddlEmpName.ClientID%>').focus();

                return false;
            }
            if (document.getElementById('<%=ddlYear.ClientID%>').options[document.getElementById('<%=ddlYear.ClientID%>').selectedIndex].value == "Select") {
                alert("Please Select year");
                document.getElementById('<%=ddlYear.ClientID%>').focus();
                return false;
            }
            return true;
        }



    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="#">Payroll</a><i class="fa fa-angle-right"></i> </li>
                        <li><a href="Leave_Allotment.aspx" class="current">Leave Allotment</a> </li>
                    </ul>
                </div>
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-md-12">
                            <section class="panel">
                                <header class="panel-heading">
                                    <i class="fa fa-th-list"> Leave Allotment</i>
                                </header>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="col-md-4 form-group">
                                            <label for="textfield" class="control-label col-sm-5" style="">
                                                Emp Code<span id="Span2" runat="server" style="color: Red; display: inline;">*</span>
                                            </label>
                                            <div class="col-sm-7 tEmpCode" style="">
                                                <asp:TextBox ID="txtEmpCode" runat="server" placeholder="Emp Code" MaxLength="50"
                                                    class="form-control" ReadOnly="true" TabIndex="0"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-8 form-group">
                                            <label for="textfield" class="control-label col-sm-3" style="">
                                                Employee Name<span id="Span1" runat="server" style="color: Red; display: inline;">*</span></label>
                                            <div class="col-sm-9 tAEmpName" style="" id="">
                                                <asp:DropDownList ID="ddlEmpName" runat="server" class="form-control DropdownCss" TabIndex="1" AutoPostBack="True" OnSelectedIndexChanged="ddlEmpName_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-4 form-group">
                                            <label for="textfield" class="control-label col-sm-5" style="">
                                                Select Year <span style="color: #ff0000; font-size: 14px; display: inline;">*</span></label>
                                            <div class="col-sm-7 tASelectYear" style="" id="">
                                                <asp:DropDownList ID="ddlYear" runat="server" class="form-control DropdownCss" TabIndex="2"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                                                    <asp:ListItem Text="Select Year" Value="Select Year"></asp:ListItem>
                                                    <asp:ListItem Text="2016" Value="2016"></asp:ListItem>
                                                    <asp:ListItem Text="2017" Value="2017"></asp:ListItem>
                                                    <asp:ListItem Text="2018" Value="2018"></asp:ListItem>
                                                    <asp:ListItem Text="2019" Value="2019"></asp:ListItem>
                                                    <asp:ListItem Text="2020" Value="2020"></asp:ListItem>
                                                    <asp:ListItem Text="2021" Value="2021"></asp:ListItem>
                                                    <asp:ListItem Text="2022" Value="2022"></asp:ListItem>
                                                    <asp:ListItem Text="2023" Value="2023"></asp:ListItem>
                                                    <asp:ListItem Text="2024" Value="2024"></asp:ListItem>
                                                    <asp:ListItem Text="2025" Value="2025"></asp:ListItem>
                                                    <asp:ListItem Text="2026" Value="2026"></asp:ListItem>
                                                    <asp:ListItem Text="2027" Value="2027"></asp:ListItem>
                                                    <asp:ListItem Text="2028" Value="2028"></asp:ListItem>
                                                    <asp:ListItem Text="2029" Value="2029"></asp:ListItem>
                                                    <asp:ListItem Text="2030" Value="2030"></asp:ListItem>
                                                    <asp:ListItem Text="2031" Value="2031"></asp:ListItem>
                                                    <asp:ListItem Text="2032" Value="2032"></asp:ListItem>
                                                    <asp:ListItem Text="2033" Value="2033"></asp:ListItem>
                                                    <asp:ListItem Text="2034" Value="2034"></asp:ListItem>
                                                    <asp:ListItem Text="2035" Value="2035"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-8 form-group">
                                            <div class="col-sm-12">
                                                <asp:RadioButtonList RepeatDirection="Horizontal" class="ListControl" ID="RBLNewAllotment" AutoPostBack="true" OnSelectedIndexChanged="RBLNewAllotment_OnSelectedIndexChanged" runat="Server">
                                                    <asp:ListItem Text="New Allotment" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Opening Balances" Value="2"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <asp:Panel ID="Panel2" runat="server" GroupingText="Leave Details" Style="border-bottom: 0px solid #8F8F8F;">
                                                    <div class="col-sm-12" style="margin-bottom: 0.3%;">
                                                        <div class="row">
                                                            <asp:GridView ID="grdLeaveInfo" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                                                Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 50%;" runat="server"
                                                                ShowFooter="true" AutoGenerateColumns="False">
                                                                <AlternatingRowStyle BackColor="#EEEEEE" />
                                                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                                <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                                <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                                <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                                <SortedDescendingHeaderStyle BackColor="#000065" />
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Leave Name">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtDate" runat="server" class="form-control"
                                                                                MaxLength="10" Text='<%#Eval("Leave_Name")%>' ReadOnly="true"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="No of Leaves">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtEventName" runat="server" Text='<%#Eval("Leave_Year")%>' CssClass="form-control"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                     
                                        <div class="col-md-12 text-center">
                                            <asp:Button ID="btnSave" runat="server" Text="Submit" class="btn btn-primary" OnClientClick="return ControlValid()" OnClick="btnSave_Click1" />
                                            <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-primary" OnClientClick="return ControlValid()" OnClick="btnUpdate_Click" />
                                            <asp:Button ID="btnCancel" runat="server" Text="Reset" class="btn" OnClick="btnCancel_Click" />
                                        </div>
                                               </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </section>
    </section>
</asp:Content>
