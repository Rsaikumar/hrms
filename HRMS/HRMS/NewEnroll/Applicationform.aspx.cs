﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using DAL;
using BAL;
namespace HRMS.NewEnroll
{
    public partial class Applicationform : System.Web.UI.Page
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                empcode();
                string Eid = Request.QueryString["Email"].ToString();
                txtemail.Text = Eid;
                txtemail.ReadOnly = true;
                txtfname.ReadOnly = true;
                txtmname.ReadOnly = true;
                txtlname.ReadOnly = true;
                txtmobile.ReadOnly = true;
                //getting already existing data from DB of user
                string constr = ConfigurationManager.ConnectionStrings["Dbconnection"].ConnectionString;

                SqlConnection con = new SqlConnection(constr);
                DataTable dt = new DataTable();
                con.Open();
                SqlDataReader myReader = null;
                SqlCommand myCommand = new SqlCommand("select * from HRMS_Employee_Master_PersonalDetails where Permanent_Email='" + Eid.ToString() + "'", con);

                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    txtfname.Text = (myReader["Emp_First_Name"].ToString());
                    txtmname.Text = (myReader["Emp_Middle_Name"].ToString());
                    txtlname.Text = (myReader["Emp_Last_Name"].ToString());
                    txtdob.Text = (myReader["Emp_DOB"].ToString());
                    txttelephone.Text = (myReader["Telephone"].ToString());
                    txtmobile.Text = (myReader["Permanent_Mobile"].ToString());

                }
                //End
            }
        }
        public void empcode()
        {
            var objHeaderInfo = (from header in objDAL.HRMS_Employee_Master_PersonalDetails
                                 select header).ToList();

            int i = Convert.ToInt32(objHeaderInfo.Count) + 1;
            txtempcode.Text = "EMP-" + i;
        }
       
        public void empdetails()
        {
            
           

            HRMS_Employee_Master_PersonalDetail hemp = (from item in objDAL.HRMS_Employee_Master_PersonalDetails
                                                      where item.Permanent_Email == txtemail.Text
                                                      select item).FirstOrDefault();
            
            hemp.Emp_Code=txtempcode.Text;
           hemp.Emp_First_Name= txtfname.Text;
           hemp.Emp_Middle_Name = txtmname.Text;
           hemp.Emp_Last_Name= txtlname.Text;
           hemp.Password= txtcpwd.Text;
           hemp.Emp_DOB= Convert.ToDateTime(txtdob.Text);
           hemp.Emp_Age=Convert.ToDecimal(txtage.Text);
           hemp.Gender= rbtngen.Text;
           hemp.Emp_Roll = "CCF";
           hemp.Emp_Status = "Active";
           hemp.Creation_Company = "000004";
           hemp.Blood_Group= txtbg.Text;
           hemp.Marital_Status= ddlmaritalsts.SelectedItem.Text;
           hemp.Nationality= ddlnat.SelectedItem.Text;
           hemp.Religion= ddlreligion.SelectedItem.Text;
           hemp.Languages_Known= txtklang.Text;
           hemp.Aadhar_Number= txtaadharcd.Text;
           hemp.Pan_Number= txtpanno.Text;
           hemp.Present_Address1= txtadd1.Text;
           hemp.Present_Address2= txtadd2.Text;
           hemp.Present_City= txtcity.Text;
           hemp.Present_State= txtstate.Text;
           hemp.Present_Pincode= txtpincode.Text;
           hemp.Present_Country= txtcountry.Text;
           hemp.Permanent_Address1= txtpadd1.Text;
           hemp.Permanent_Address2= txtpadd2.Text;
           hemp.Permanent_City= txtpcity.Text;
           hemp.Permanent_State= txtpstate.Text;
           hemp.Permanent_Pincode= txtppincodecode.Text;
           hemp.Permanent_Country= txtpcountry.Text;
           hemp.Permanent_Mobile= txtmobile.Text;
           hemp.Educate= ddleduqlf.SelectedItem.Text;
           hemp.Telephone= txttelephone.Text;
           hemp.User_Name = txtfname.Text;
          


            HRMS_Employee_Master_Professional_Experience hempe=new HRMS_Employee_Master_Professional_Experience();
            
                hempe.Emp_Code= txtempcode.Text;
                      hempe.Emp_Name=txtnoemployer.Text ;
                       hempe.Emp_Designation= txtpostionheld.Text;
                       hempe.Period_From= Convert.ToDateTime(txtperiodfrom.Text);
                       hempe.Period_To= Convert.ToDateTime(txtperiodto.Text);
                        hempe.Total_Duration= txttduration.Text;
            objDAL.HRMS_Employee_Master_Professional_Experiences.InsertOnSubmit(hempe);
            HRMS_Employee_Master_Official_Information hemoi=new HRMS_Employee_Master_Official_Information();

            hemoi. Emp_Code= txtempcode.Text;
                      
             hemoi.Mobile_Phone= txtmobile.Text;
            objDAL.HRMS_Employee_Master_Official_Informations.InsertOnSubmit(hemoi);
           
            objDAL.SubmitChanges();
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Datasaved Succesfully');", true);

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            
        }
        public void sendmail()
        {
            using (MailMessage mm = new MailMessage("sycon225@gmail.com", txtemail.Text))
            {
                mm.Subject = "Verification Mail;";
                string body = "Hello " + txtfname.Text.Trim() + ",";
                body += "<br /><br />'"+txtfname.Text+"' have been completed with his details.. Please check the details before Accepting";
               // body += "<br /><a href = '" + Request.Url.AbsoluteUri.Replace("verification.aspx", "Applicationform.aspx?Email=" + txtemail.Text) + "'>Click here to continue...</a>";
                body += "<br /><br />Thanks";
                mm.Body = body;
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential("sycon225@gmail.com", "9949996035");
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                
                smtp.Send(mm);
            }
        }

        protected void btnSubmit_Click1(object sender, EventArgs e)
        {

            empdetails();
        }
    }
}