﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Text;
using System.Net;
using System.Net.Mail;
using DAL;



namespace HRMS.NewEnroll
{
    public partial class verification : System.Web.UI.Page
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnverify_Click(object sender, EventArgs e)
        {

            HRMS_Employee_Master_PersonalDetail hemp = new HRMS_Employee_Master_PersonalDetail();


            hemp.Emp_First_Name = txtfname.Text;
            hemp.Emp_Middle_Name = txtmname.Text;
            hemp.Emp_Last_Name = txtlname.Text;
            hemp.Emp_DOB = Convert.ToDateTime(txtdob.Text);
            hemp.Permanent_Mobile = txtmno.Text;
            hemp.Permanent_Email = txtemail.Text;
            objDAL.HRMS_Employee_Master_PersonalDetails.InsertOnSubmit(hemp);
            objDAL.SubmitChanges();


           



            using (MailMessage mm = new MailMessage("syscon225@gmail.com", txtemail.Text))
            {

                mm.Subject = "Verification Mail;";
                string body = "Hello " + txtfname.Text.Trim() + ",";
                body += "<br /><br />Please click the following link to Complete your form";
                body += "<br /><a href = '" + Request.Url.AbsoluteUri.Replace("verification.aspx", "Applicationform.aspx?Email=" + txtemail.Text) + "'>Click here to continue...</a>";
                body += "<br /><br />Thanks";
                mm.Body = body;
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential("syscon225@gmail.com", "9949996035");
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;

                smtp.Send(mm);
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('verification link has been sent to your mailid, Thank you');", true);
                txtdob.Text = "";
                txtemail.Text = "";
                txtfname.Text = "";
                txtlname.Text = "";
                txtmname.Text = "";
                txtmno.Text = "";
            }
        }
    }
}
