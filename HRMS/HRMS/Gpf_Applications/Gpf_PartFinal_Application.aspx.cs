﻿
using System;
using System.Web.UI;
using BAL;
using System.Linq;
using DAL;
using System.Globalization;

namespace HRMS.Gpf_Applications
{
    public partial class Gpf_PartFinal_Application : System.Web.UI.Page
    {
        GpfPartFinalApplicationBAL objlapp = new GpfPartFinalApplicationBAL();
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            saving();
        }
        private void saving()
        {
            try
            {
                objlapp.NameOfSubscriber = txtnamesub.Text;
                objlapp.Designation = txtdesignation.Text;
                objlapp.Pay = txtpay.Text;
                objlapp.BalanceOn = Convert.ToDecimal(txtbalancedate.Text);
                objlapp.SecondPartFinalDrawl = Convert.ToDecimal(txtsecondpastfinddrawl.Text);
                objlapp.AlreadyTakenAmount = Convert.ToDecimal(txtalreadydrawal.Text);
                objlapp.BalanceOfTakenAmount = Convert.ToDecimal(txtbalanceoftakenamount.Text);
                objlapp.RequiredAmount = Convert.ToDecimal(txtrequiredamount.Text);
                objlapp.AmountRequiredHouse = Convert.ToDecimal(txtamounthousesites.Text);
                objlapp.Purpose = txtpurpose.Text;
                objlapp.ForPurchasing =txtforpurchasing.Text;
                objlapp.ForConstructing = txtforconstructing.Text;
                objlapp.ForRedemption = txtforredemption.Text;
                objlapp.ForMakingAdditions = txtformakingadditions.Text;

                objlapp.ForHouseSite = txtforhousesite.Text;
                objlapp.ForRepaymet = txtforrepayment.Text;
                objlapp.LoanUnder = txtloanunder.Text;
                objlapp.HigherEducation = txthighereducation.Text;
                objlapp.RelationWithdrawal = txtrelationwithdrawal.Text;
                objlapp.Institution =txtinstitution.Text;
                objlapp.SchoolYears = txtschoolyears.Text;
                objlapp.WithdrawalCurrentYear = DateTime.ParseExact(txtwithdrawalcurrentyear.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                objlapp.DateWithdrawal = DateTime.ParseExact(txtdatewithdrawal.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                   
                objlapp.MarriageBetrothal =txtmariagebetrothal.Text;
                objlapp.RelationDependent = txtrelationdependent.Text;

                objlapp.PresentWithDrawal = txtpresentwithdrawal.Text;
                objlapp.ActualDate = DateTime.ParseExact(txtactualdate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
          
                objlapp.MedicalExpenses = Convert.ToDecimal(txtmedicalexpenses.Text);
                objlapp.TotalService = Convert.ToDecimal(txttotalservice.Text);
                objlapp.PeriodOfService = DateTime.ParseExact(txtperiodofservice.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                objlapp.DateSuperannuation = DateTime.ParseExact(txtdatesuperannuation.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                objlapp.ActualCost = Convert.ToDecimal(txtactualcost.Text);
                objlapp.AnticipatedCost = Convert.ToDecimal(txtanticipatedcost.Text);
                objlapp.AnticipatedCostOfAdditions = Convert.ToDecimal(txtanticipatedcostadditions.Text);
                objlapp.ForRedemptionHouse = txtforredemptionhouse.Text;
                objlapp.LoanTakendateAmount =txtloantakendateamount.Text;
                objlapp.ExpensesRequired = Convert.ToDecimal(txtexpensesrequired.Text);
                objlapp.AmountMarriage = Convert.ToDecimal(txtamountmarriage.Text);
                objlapp.AmountHouseSites = Convert.ToDecimal(txtamounthousesites.Text);
                objlapp.AmountProvidentFund = Convert.ToDecimal(txtamountprovidentfund.Text);
                objlapp.DesignationNo =Convert.ToInt32( txtoffdesignationno.Text);

                objlapp.DatedThe = DateTime.ParseExact(txtdatedthe.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                   
                objlapp.Particularsverified = txtforward.Text;
                objlapp.RecordNo = Convert.ToInt32( txtrecordno.Text);

                objlapp.CreatedBy = Session["User_Name"].ToString();
                objlapp.CreatedOn = System.DateTime.Now;
                objlapp.ModifiedBy = Session["User_Name"].ToString();
                objlapp.ModifiedOn = System.DateTime.Now;

                var ra = (from item in ObjDAL.HRMSEmployeeMasterOfficialInformationDetails
                          where item.ID ==Convert.ToInt32( Session["Emp_Code"].ToString())
                          select item).FirstOrDefault();

                objlapp.ReportingAuthority = ra.ReportingAuthority;
                objlapp.parameter = 1;


                if (objlapp.InsertHRMSGpfPartFinalApplication() > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('saved Details sucessfully');window.location ='../Dashboard/Dashboard.aspx';", true);
                    
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Details Not Saved');", true);
                }
            }
            catch (Exception ex)
            {

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            { }
        }
    }
}




