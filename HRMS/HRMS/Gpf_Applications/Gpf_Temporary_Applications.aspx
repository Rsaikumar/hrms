﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="Gpf_Temporary_Applications.aspx.cs" Inherits="HRMS.Gpf_Applications.Gpf_Temporary_Applications" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script type="text/javascript">
         function PrintPanel() {
             var panel = document.getElementById("<%=pnlcontents.ClientID %>");
            var printWindow = window.open('', '', 'height=200,width=2000');
            printWindow.document.write('<html><head><title>DIV Contents</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        
                        <li><a href="Gpf_Temporary_Applications.aspx" class="current"><i class="fa fa-home"></i>TemporaryApplication</a> </li>
                    </ul>
                    <div class="pull-right pull-module m-bot20" style="">
                    </div>
                </div>
            </div>
            <div class="row">
                <section class="panel" id="pnlcontents" runat="server">
                    <header class="panel-heading">
                        <i class="fa fa-user"> Temporary Application Form</i>
                    </header>

                    <div class="panel-body">
                        <div>
                            <div align="center">
                                <h2>APPENDIX I</h2>
                                <h4>(FROM (1))</h4>
                                <h5>(See Rule - 14)</h5>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-1">
                                    1.
                                </div>
                                <div class="col-md-5">
                                    Name Of the Subscriber
                                </div>
                                <div class="col-md-1">
                                    :
                                </div>
                                <div class="col-md-5 form-group">
                                    <asp:TextBox ID="txtnamesub" CssClass="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">
                                    2.
                                </div>
                                <div class="col-md-5">
                                    Account No
                                </div>
                                <div class="col-md-1">
                                    :
                                </div>
                                <div class="col-md-5 form-group">
                                    <asp:TextBox ID="txtaccountno" CssClass="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">
                                    3.
                                </div>
                                <div class="col-md-5">
                                    Designation
                                </div>
                                <div class="col-md-1">
                                    :
                                </div>
                                <div class="col-md-5 form-group">
                                    <asp:TextBox ID="txtdesignation" CssClass="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">
                                    4.
                                </div>
                                <div class="col-md-5">
                                    Pay
                                </div>
                                <div class="col-md-1">
                                    :
                                </div>
                                <div class="col-md-5 form-group">
                                    <asp:TextBox ID="txtpay" CssClass="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">
                                    5.
                                </div>
                                <div class="col-md-5">
                                    Balance of credit of the subscriber on the date of application
                                </div>
                                <div class="col-md-1">
                                    :
                                </div>
                                <div class="col-md-5 form-group">
                                    <asp:TextBox ID="txtdateofapplication" CssClass="form-control" runat="server" required></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender1" PopupButtonID="imgPopup" runat="server" TargetControlID="txtdateofapplication" Format="dd-MM-yyyy" PopupPosition="BottomLeft"></ajaxToolKit:CalendarExtender>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">
                                    6.
                                </div>
                                <div class="col-md-5">
                                    Amount of advance outstanding if any and the purpose for which advance was taken them.
                                </div>
                                <div class="col-md-1">
                                    :
                                </div>
                                <div class="col-md-5 form-group">
                                    <asp:TextBox ID="txtamttaken" CssClass="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">
                                    7.
                                </div>
                                <div class="col-md-5">
                                    Amount of Advance required
                                </div>
                                <div class="col-md-1">
                                    :
                                </div>
                                <div class="col-md-5 form-group">
                                    <asp:TextBox ID="txtamtadreq" CssClass="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">
                                    8.
                                </div>
                                <div class="col-md-5">
                                    Purpose for which the advance required
                                </div>
                                <div class="col-md-1">
                                    :
                                </div>
                                <div class="col-md-5 form-group">
                                    <asp:TextBox ID="txtpuradreq" CssClass="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">
                                    9.
                                </div>
                                <div class="col-md-5">
                                    Amount of the consolidated Advance terms 6 & 7 & Number of monthly installments in which the consolidated advance is proposed to be repaid
                                </div>
                                <div class="col-md-1">
                                    :
                                </div>
                                <div class="col-md-5 form-group">
                                    <asp:TextBox ID="txtprusrepaid" CssClass="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">
                                    10.
                                </div>
                                <div class="col-md-5">
                                    Full particulars of the pecuniary circumtances of the subcriber justifying the application for the temporary withdrawal
                                </div>
                                <div class="col-md-1">
                                    :
                                </div>
                                <div class="col-md-5 form-group">
                                    <asp:TextBox ID="txttemwithdrawal" CssClass="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <br />
                            <div class="col-md-12 text-right">
                                <h5>SIGNATURE OF THE APPLICANT</h5>
                            </div>
                        </div>
                    </div>
                      <div class="col-md-12 text-center" >
                            <asp:Button ID="btnPrint" runat="server" Text="Print" OnClientClick="return PrintPanel();" />
                        <asp:Button ID="btnsave" runat="server" Text="Submit" OnClick="btnsave_Click" /> 
                    </div>
                </section>
            </div>
        </section>
    </section>
</asp:Content>
