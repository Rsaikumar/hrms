﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="Gpf_PartFinal_Application.aspx.cs" Inherits="HRMS.Gpf_Applications.Gpf_PartFinal_Application" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script type="text/javascript">
         function PrintPanel() {
             var panel = document.getElementById("<%=pnlContents.ClientID %>");
            var printWindow = window.open('', '', 'height=200,width=2000');
            printWindow.document.write('<html><head><title>DIV Contents</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="Dashboard.aspx" class="current"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="#">PartFinalApplication</a><i class="fa fa-angle-right"></i> </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <section class="panel" id="pnlContents" runat="server">
                    <header class="panel-heading">
                        <i class="fa fa-user">Part-Final Application Form</i>
                    </header>
                    <div class="panel-body">
                        <div>
                            <div align="center">
                                <h4>APPENDIX - O</h4>
                                <h4>FROM OF APPLICATION FOR PART-FINAL WITHDRAWAL OF MONEY FROM</h4>
                                <h4>THE PROVIDENT FUND FOR HOUSE BUILDING PURCHASE OF REDEMPTION</h4>
                                <h4>OF HOUSES AND HOUSES-SITES, HIGHER EDUCATION PURPOSES OR MARRIAGE OR</h4>
                                <h4>MARRIAGE OR MEDICAL EXPENSES</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                1.
                            </div>
                            <div class="col-md-5">
                                Name Of the Subscriber
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtnamesub" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                2.
                            </div>
                            <div class="col-md-5">
                                Designation
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtdesignation" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                3.
                            </div>
                            <div class="col-md-5">
                                Pay
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtpay" CssClass="form-control" runat="server" required> </asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                4.
                            </div>
                            <div class="col-md-5">
                                Name Of the provident Fund and Account Number
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtnameoftheprovident" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                5.(a)
                            </div>
                            <div class="col-md-5">
                                Balance at the credit of the subscriber on the date of application. ( amount actually subscribed by her along with interest due thereon in the case of GPF Subscriber)
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtbalancedate" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                Note: In case of a part final withdrawal for house building and marriage purposes. as well as purchase of house/house-site, the total balanced at the credit of all the Provident fund should be taken into account if the subscriber,subcribe to more than one provident fund.
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtsecondpastfinddrawl" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                (b)
                            </div>
                            <div class="col-md-5">
                                If it is a second part final drawal for the purpose of carrying ou additions and alterations to or reconstructions of a house acquried with the help of withdrawal already made or which may be made in future from the Provident Fund.
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtalreadydrawal" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                (i)Amount of part-final withdrawal already taken
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtbalanceoftakenamount" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                (ii) Balance at credit at the time of making the first part-final withdrawal
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtrequiredamount" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                6.
                            </div>
                            <div class="col-md-5">
                                Amount required in the case of construction of house or for the purchase of a house site, the number of  installments  in which it is required should be stated.
                            </div>
                            <div class="col-md-1 form-group">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtamtreqhouse" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                7.
                            </div>
                            <div class="col-md-5">
                                The purpose for which the amount is required
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtpurpose" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                (a)
                            </div>
                            <div class="col-md-5">
                                If it is for house building or for the purpose purchasing a  house site or for express  purpose of repayment of any loan taken under: 
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtpurposepurchasing" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                (i) For purchasing a house
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtforpurchasing" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                (ii) For constructing, reconstructing a house
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtforconstructing" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                (iii) For redemption of a house
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtforredemption" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                (iv) For making additions or alterations to a house
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtformakingadditions" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                (v) For purchase of a house site
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtforhousesite" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                (vi) For repayment of a loan expressly taken for the purchase of a house-site
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtforrepayment" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                (vii) For repayment of any loan taken under
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtloanunder" CssClass="form-control" runat="server" required> </asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                (b)
                            </div>
                            <div class="col-md-5">
                                If it is for higher education
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txthighereducation" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                (i) Relationship with the person (who is actually dependent on the subscriber) for whom the withdrawal is required
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtrelationwithdrawal" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                (ii) The specific course taken by the person and the Name and  place of the institution
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtinstitution" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                (iii) Whether the course is for more than three years and beyond the High School stage
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtschoolyears" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                (v) Whether this is the first or the second with-drawal for the current year
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtwithdrawalcurrentyear" CssClass="form-control" runat="server" required></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender1" PopupButtonID="imgPopup" runat="server" TargetControlID="txtwithdrawalcurrentyear" Format="dd-MM-yyyy" PopupPosition="BottomLeft"></ajaxToolKit:CalendarExtender>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                (vi) The date of previous withdrawal or advance, if any, taken for this purpose.
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtdatewithdrawal" CssClass="form-control" runat="server" required></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender2" PopupButtonID="imgPopup" runat="server" TargetControlID="txtdatewithdrawal" Format="dd-MM-yyyy" PopupPosition="BottomLeft"></ajaxToolKit:CalendarExtender>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                (c)
                            </div>
                            <div class="col-md-5">
                                If it is for marriage or betrothal ceremony expenses
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtmariagebetrothal" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                (i) Whether for the marriage or betrothal ceremony of the subscriber’s daughter/son or for any other female relation dependent on the subscriber, who has no daughter
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtrelationdependent" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                (ii)Whether any advance under ordinary rules has been drawn in respect of the betrothal ceremony or marriage for which the present withdrawal is sought for
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtpresentwithdrawal" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                (iii)Actual date fixed for celebration of the betrothal ceremony or marriage
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtactualdate" CssClass="form-control" runat="server" required></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender3" PopupButtonID="imgPopup" runat="server" TargetControlID="txtactualdate" Format="dd-MM-yyyy" PopupPosition="BottomLeft"></ajaxToolKit:CalendarExtender>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                (d)
                            </div>
                            <div class="col-md-5">
                                If it is for medical expenses
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtmedicalexpenses" CssClass="form-control" runat="server" required> </asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                8.
                            </div>
                            <div class="col-md-5">
                                (i) Total  service including broken periods, if any, 
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txttotalservice" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                (ii) Period of service required on the date of application for attaining the age of superannuation
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtperiodofservice" CssClass="form-control" runat="server" required></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender4" PopupButtonID="imgPopup" runat="server" TargetControlID="txtperiodofservice" Format="dd-MM-yyyy" PopupPosition="BottomLeft"></ajaxToolKit:CalendarExtender>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                (iii) The date superannuation 
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtdatesuperannuation" CssClass="form-control" runat="server" required></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender5" PopupButtonID="imgPopup" runat="server" TargetControlID="txtdatesuperannuation" Format="dd-MM-yyyy" PopupPosition="BottomLeft"></ajaxToolKit:CalendarExtender>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                9.
                            </div>
                            <div class="col-md-5">
                                (a)(i) Actual cost acquiring the house or house site
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtactualcost" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                (ii)Anticipated cost of house proposed to be built/re-built
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtanticipatedcost" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                (iii)Anticipated cost of additions or alterations to be  made to the house
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtanticipatedcostadditions" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                (vii) Actual amount required for redemption of the house or house site
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtforredemptionhouse" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                (viii) Total amount and date of loan taken for the  purchase of house- site and the amount outstanding against that loan  on date
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtloantakendateamount" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                (b) Particulars of expenses required to be incurred on the higher education
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtexpensesrequired" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                (c) Amount required for meeting marriage expenses, indicating the number of marriages to be celebrated.
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtamountmarriage" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                10.
                            </div>
                            <div class="col-md-5">
                                Amount of installment or installments last taken,if any, For house building or / and purchase of house-sites  (State particulars of amount, dates on which taken). 
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtamounthousesites" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                11.
                            </div>
                            <div class="col-md-5">
                                Amount, if any,  received already from Govt. for purchase of house-site of house building purpose other than from the Provident Fund Account.
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtamountprovidentfund" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="col-md-12 text-right">
                            <h5>SIGNATURE OF APPLICANT</h5>
                            <h5>DESIGNATION</h5>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                Recommeneded/ Not Recommended,
                            </div>
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                Signature of the Head of the Office or Drawing Office
                            </div>
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                Designation no:<asp:TextBox ID="txtoffdesignationno" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5 form-group">
                                Dated the
                    <asp:TextBox ID="txtdatedthe" runat="server" CssClass="form-control" required></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender6" PopupButtonID="imgPopup" runat="server" TargetControlID="txtdatedthe" Format="dd-MM-yyyy" PopupPosition="BottomLeft"></ajaxToolKit:CalendarExtender>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                                The above particulars have been verified to be correct  Fowarded to the  (in triplicate).
                            </div>
                            <div class="col-md-1">
                                :
                            </div>
                            <div class="col-md-5 form-group">
                                <asp:TextBox ID="txtforward" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                        </div>
                        <br />
                        <div>
                            <div>
                                (I) The part-final withdrawal has been recommended under rule 15() of GPF rules; 
                <br />
                                (II) I have satisfied myself that conditions prescribed in rules of GPF. referred to above have been fulfilled and that the subscriber has produced to me the necessary deed and papers which are enclosed; 
                <br />
                                (III)The applicant has signed the undertaking prescribed in rule and that the same is forwarded herewith; 
                <br />
                                (IV) I have verified the progress of construction of the house and that the 2nd/3rd 4th installment of the withdrawal may be paid; 
                <br />
                                (V) I have satisfied myself that the applicant has not take any loan/assistance under(any scheme sponsored by the or from any other Govt. source and that the necessary note has been made regarding the verification of the requirements laid down in Rule 15() of GPF rules in case of complete repayment of loan during the service of subscriber; 
                <br />
                                (VI) No part final withdrawal has been granted previously to the subscriber for the same purpose; and,  
                <br />
                                (VII) In addition to the part-final withdrawal. No temporary advance has been granted to the subscriber for the same purpose now. 
                <br />
                                <br />
                                Note : Delete the certificate not applicable. (Certificate in items(ii)-(iii)and(iv) above are not necessary if the part-final withdrawal is for Higher Education or for betrothal ceremony or marriage purpose)
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="col-md-12 text-right">
                            <h5>SIGNATURE</h5>
                            <h5>DESIGNATION</h5>
                        </div>
                        <br />
                        <br />
                        <br />
                        <div class="row">
                            <div class="col-md-1">
                                Rc.No.
                            </div>
                            <div class="col-md-5">
                                <asp:TextBox ID="txtrecordno" CssClass="form-control" runat="server" required></asp:TextBox>
                            </div>
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                Date:
                            </div>
                            <div class="col-md-5">
                                <asp:TextBox ID="txtdate" CssClass="form-control" runat="server" required></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender7" PopupButtonID="imgPopup" runat="server" TargetControlID="txtdate" Format="dd-MM-yyyy" PopupPosition="BottomLeft"></ajaxToolKit:CalendarExtender>
                            </div>
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-5">
                            </div>
                        </div>
                        <div class="row col-md-12">
                            Sanction of the GPF PFW under rule of GPF Rules, hereby accorded for payment of Rs.
                <asp:TextBox ID="txtgpfpayment" BorderStyle="None" runat="server" required></asp:TextBox>(Rupees<asp:TextBox ID="txtrupeee" Width="100px" BorderStyle="None" runat="server" required></asp:TextBox>)
                                the amount shall be drawn in lumpsum for his GPF A/c.   
                        </div>
                        <div class="row col-md-12 text-right">
                            <h5>SIGNATURE</h5>
                            <h5>DESIGNATION</h5>
                        </div>
                    </div>
                     <div class="col-md-12 text-center" >
                            <asp:Button ID="btnPrint" runat="server" Text="Print" OnClientClick="return PrintPanel();" />
                        <asp:Button ID="btnsave" runat="server" Text="Submit" OnClick="btnsave_Click"/>
                    </div>
                </section>
            </div>
        </section>
    </section>
</asp:Content>
