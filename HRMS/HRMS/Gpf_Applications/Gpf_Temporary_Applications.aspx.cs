﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BAL;
using System.Globalization;

namespace HRMS.Gpf_Applications
{
    public partial class Gpf_Temporary_Applications : System.Web.UI.Page
    {
        GpfTemporaryApplicationBAL objlapp = new GpfTemporaryApplicationBAL();
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        private void saving()
        {
            try
            {
                objlapp.NameOfSubscriber = txtnamesub.Text;

                objlapp.AccountNo = Convert.ToInt32(txtaccountno.Text);
                objlapp.Designation = txtdesignation.Text;
                objlapp.Pay = txtpay.Text;

                objlapp.DateOfApplication = DateTime.ParseExact(txtdateofapplication.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                objlapp.AmountTaken = Convert.ToDecimal(txtamttaken.Text);
                objlapp.AmountRequired = Convert.ToDecimal(txtamtadreq.Text);
                objlapp.PurposeRequired = txtpuradreq.Text;
                objlapp.AmountRepaid = Convert.ToDecimal(txtprusrepaid.Text);
                objlapp.TemporaryWithdrawl = txttemwithdrawal.Text;
                objlapp.CreatedBy = Session["User_Name"].ToString();
                objlapp.CreatedOn = System.DateTime.Now;
                objlapp.ModifiedBy = Session["User_Name"].ToString();
                objlapp.ModifiedOn = System.DateTime.Now;

                var ra = (from item in ObjDAL.HRMSEmployeeMasterOfficialInformationDetails
                          where item.ID ==Convert.ToInt32( Session["Emp_Code"].ToString())
                          select item).FirstOrDefault();

                
                    objlapp.ReportingAuthority = ra.ReportingAuthority;
                    objlapp.Parameter = 1;

               
                if (objlapp.InsertHRMSGpfTempoaryApplication() > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('saved Details sucessfully');window.location ='../Dashboard/Dashboard.aspx';", true);
                   
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Details Not Saved');", true);
                }
            }
            catch (Exception ex)
            {

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            { }
        }
        protected void cleartext()
        {
            txttemwithdrawal.Text = string.Empty;
            txtprusrepaid.Text = string.Empty;
            txtpuradreq.Text = string.Empty;
            txtamtadreq.Text = string.Empty;
            txtamttaken.Text = string.Empty;
            txtdateofapplication.Text = string.Empty;
            txtpay.Text = string.Empty;
            txtdesignation.Text = string.Empty;
            txtaccountno.Text = string.Empty;
            txtnamesub.Text = string.Empty;
            //   txtamtpartfinaliivoucherno.Text = string.Empty;

        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            saving();
            cleartext();
        }

    }
}