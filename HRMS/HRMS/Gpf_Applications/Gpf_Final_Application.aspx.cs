﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BAL;
using DAL;
using System.Globalization;

namespace HRMS.Gpf_Applications
{
    public partial class Gpf_Final_Application : System.Web.UI.Page
    {
        GpfFinalApplicationBAL objlapp = new GpfFinalApplicationBAL();
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            saving();
            cleartext();
        }
        private void saving()
        {
            try
            {
                objlapp.NameOfSubscriber = txtnameofsubscriber.Text;

                objlapp.DateOfBirth = DateTime.ParseExact(txtdateofbirth.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                objlapp.DesignationandOffice = txtdesignationandoffice.Text;
                objlapp.GPFAccountNo = txtgpfaccountno.Text;
                objlapp.ResidentialAddress = txtresidentialaddress.Text;
                objlapp.SlipEnclosed = txtslipenclosed.Text;
                objlapp.DateOfRetirement = DateTime.ParseExact(txtdateofretirement.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                objlapp.WorkedLastYears = txtworkrdyears.Text;
                objlapp.TreasuryPayment = txttreasury.Text;
                objlapp.Documents = txtdocuments.Text;
                objlapp.PersonalMarks = txtpersonalmarks.Text;
                objlapp.ToSpecimen = txttwospecimen.Text;
                objlapp.ThumbImpression = txtthumbimpression.Text;
                objlapp.CauseOfDeath = txtcaseofdeath.Text;
                objlapp.DateOfDeath = DateTime.ParseExact(txtdateofdeath.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                objlapp.ReligionOfDeceased = txtreligionofdeceased.Text;
                objlapp.Place = txtplace.Text;
                objlapp.Date = DateTime.ParseExact(txtdate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                objlapp.PayMonthOf = txtpaymonthof.Text;
                objlapp.OfficeBillNo = txtofficebillno.Text;
                objlapp.Dated = DateTime.ParseExact(txtdated.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                objlapp.ForRupee = Convert.ToDecimal(txtforrupee.Text);
                objlapp.Rupees = Convert.ToDecimal(txtrupees.Text);
                objlapp.CashVoucherNo = txtcashvoucherno.Text;
                objlapp.OfName = txtofname.Text;
                objlapp.GPFSubscription = Convert.ToDecimal(txtgpfsubscription.Text);
                objlapp.RefundAdvance = Convert.ToDecimal(txtrefundadvance.Text);
                objlapp.AmountPartFinalI = Convert.ToDecimal(txtamtpartfinali.Text);
                objlapp.AmountPartFinaldate = DateTime.ParseExact(txtamtpartfinalidate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                objlapp.AmountPartFinalVoucherNo = txtamtpartfinalivoucherno.Text;
                objlapp.AmountPartFinalII = Convert.ToDecimal(txtamtpartfinalii.Text);
                objlapp.AmountPartFinaldateII = DateTime.ParseExact(txtamtpartfinaliidate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                objlapp.AmountPartFinalIIVoucherNo = txtamtpartfinaliivoucherno.Text;
                objlapp.NameOfSubscriber = txtnameofsubscriber.Text;
                objlapp.Station = txtstation.Text;


                objlapp.CreatedBy = Session["User_Name"].ToString();
                objlapp.CreatedOn = System.DateTime.Now;
                objlapp.ModifiedBy = Session["User_Name"].ToString();
                objlapp.ModifiedOn = System.DateTime.Now;
                var ra = (from item in ObjDAL.HRMSEmployeeMasterOfficialInformationDetails
                          where item.ID ==Convert.ToInt32( Session["Emp_Code"].ToString())
                          select item).FirstOrDefault();


                objlapp.ReportingAuthority = ra.ReportingAuthority;
                objlapp.Parameter = 1;

                if (objlapp.InsertHRMSGpfFinalApplication() > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('saved Details sucessfully');window.location ='../Dashboard/Dashboard.aspx';", true);
                    
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Details Not Saved');", true);
                }
            }
            catch (Exception ex)
            {

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            { }
        }
        protected void cleartext()
        {
            txtamtpartfinaliivoucherno.Text = string.Empty;
            txtamtpartfinaliidate.Text = string.Empty;
            txtamtpartfinalii.Text = string.Empty;
            txtamtpartfinalivoucherno.Text = string.Empty;
            txtamtpartfinalidate.Text = string.Empty;
            txtamtpartfinali.Text = string.Empty;
            txtrefundadvance.Text = string.Empty;
            txtgpfsubscription.Text = string.Empty;
            txtofname.Text = string.Empty;
            txtcashvoucherno.Text = string.Empty;
            txtrupees.Text = string.Empty;
            txtforrupee.Text = string.Empty;
            txtdated.Text = string.Empty;
            txtofficebillno.Text = string.Empty;
            txtpaymonthof.Text = string.Empty;
            txtdate.Text = string.Empty;
            txtplace.Text = string.Empty;
            txtreligionofdeceased.Text = string.Empty;
            txtdateofdeath.Text = string.Empty;
            txtcaseofdeath.Text = string.Empty;
            txtthumbimpression.Text = string.Empty;
            txttwospecimen.Text = string.Empty;
            txtpersonalmarks.Text = string.Empty;
            txtdocuments.Text = string.Empty;
            txttreasury.Text = string.Empty;
            txtworkrdyears.Text = string.Empty;
            txtdateofretirement.Text = string.Empty;
            txtslipenclosed.Text = string.Empty;
            txtresidentialaddress.Text = string.Empty;
            txtgpfaccountno.Text = string.Empty;
            txtdesignationandoffice.Text = string.Empty;
            txtdateofbirth.Text = string.Empty;





        }
    }
}