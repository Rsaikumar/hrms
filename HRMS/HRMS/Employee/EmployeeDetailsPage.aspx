﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="EmployeeDetailsPage.aspx.cs" Inherits="HRMS.Employee.EmployeeRegistrationPage" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/Employee/Employee.css" rel="stylesheet" />

    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.js"></script>
    <%--<script src="//code.jquery.com/jquery-1.11.2.min.js" type="text/javascript"></script>--%>

    <script type="text/javascript">
        function ShowImagePreview(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    Username
                    $('#<%=userImg.ClientID%>').prop('src', e.target.result)
                        .width(240)
                        .height(150);
                };
                reader.readAsDataURL(input.files[0]);
                }
            }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.js"></script>

    <div id="main">
        <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i>
                    </li>
                    <li><a href="#">Payroll</a><i class="fa fa-angle-right"></i> </li>
                    <li><a href="EmployeeList.aspx">Employee list</a> <i class="fa fa-angle-right"></i></li>
                    <li><a href="EmployeeDetails.aspx">Employee Details</a> </li>
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>--%>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                        <div class="box-title">
                            <h3>
                                <i class="fa fa-user"></i>Employee Registration</h3>
                        </div>
                        <div class="box-content">
                            <form action="#" method="POST" class='form-horizontal form-bordered'>
                                <div class="col-md-3">
                                    <div class="SliderLeft" style="">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="">
                                                <%-- <img src="../img/administrator-icon.png" id="userImg" alt="">--%>
                                                <asp:Image ID="userImg" Height="130px" Width="300px" runat="server" ImageUrl="~/img/administrator-icon.png" />
                                            </div>
                                            <div>
                                                <span class="btn btn-default btn-file"><span class="fileinput-new">Select Image </span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <%-- <input type="file" id="FileUpload1" runat="server" name="..." />--%>
                                                    <asp:FileUpload ID="fupUserImage" runat="server" onchange="ShowImagePreview(this);" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-9" style="">

                                    <div class=" form-group col-md-5">
                                        <label for="textfield" class="control-label col-sm-3" style="">
                                            Emp Id<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                        <div class="col-sm-9" style="">
                                            <asp:TextBox ID="txtEmp_Code" runat="server" placeholder="Emp Code" MaxLength="10"
                                                class="form-control" ReadOnly="false" AutoPostBack="true" OnTextChanged="txtEmp_Code_TextChanged"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="textfield" class="control-label col-sm-3" style="">
                                            First Name<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                        <div class="col-sm-9" style="" id="">
                                            <asp:TextBox ID="txtFirstName" runat="server" placeholder="First Name" class="form-control"
                                                MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-5">
                                        <label for="textfield" class="control-label col-sm-3" style="">
                                            Middle Name</label>
                                        <div class="col-sm-9" style="" id="Div1">
                                            <asp:TextBox ID="txtMiddleName" runat="server" placeholder="Middle Name" class="form-control"
                                                MaxLength="20"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="textfield" class="control-label col-sm-3" style="">
                                            Last Name<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                        <div class="col-sm-9" style="" id="Div2">
                                            <asp:TextBox ID="txtLastName" runat="server" placeholder="Last Name" class="form-control"
                                                MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>


                                    <div class="form-group col-md-5">
                                        <label for="textfield" class="control-label col-sm-3" style="">
                                            User Role <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                        <div class="col-sm-9" style="" id="Div3">
                                            <asp:DropDownList ID="ddlUSerRole" runat="server" class="form-control DropdownCss">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="textfield" class="control-label col-sm-3" style="">
                                            User Sub-Role <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                        <div class="col-sm-9" style="" id="Div27">
                                            <asp:DropDownList ID="ddlusersubrole" runat="server" class="form-control DropdownCss">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group col-md-5">
                                        <label for="textfield" class="control-label col-sm-3" style="">
                                            Username<span style="color: #ff0000; font-size: 14px;">*</span>
                                        </label>
                                        <div class="col-sm-9" style="" id="Div4">
                                            <asp:TextBox ID="txtUsername" runat="server" placeholder="Username" class="form-control"
                                                MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="textfield" class="control-label col-sm-3" style="">
                                            Password<span style="color: #ff0000; font-size: 14px;">*</span></label><div class="col-sm-9" style="" id="Div5">
                                                <asp:TextBox ID="txtPassword" runat="server" placeholder="Password" class="form-control"
                                                    MaxLength="50" TextMode="Password"></asp:TextBox>
                                            </div>
                                    </div>


                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>

                                            <div class="form-group col-md-5">
                                                <label for="textfield" class="control-label col-sm-3" style="">
                                                    Emp Status</label>
                                                <div class="col-sm-9" style="" id="Div6">
                                                    <asp:DropDownList ID="ddlEmpStatus" runat="server" class="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlEmpStatus_SelectedIndexChanged">
                                                        <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                        <asp:ListItem Text="Active" Value="Active"></asp:ListItem>
                                                        <asp:ListItem Text="On Leave" Value="On Leave"></asp:ListItem>
                                                        <asp:ListItem Text="On Long Leave" Value="On Long Leave"></asp:ListItem>
                                                        <asp:ListItem Text="Suspended" Value="Suspended"></asp:ListItem>
                                                        <asp:ListItem Text="Transferred" Value="Transferred"></asp:ListItem>
                                                        <asp:ListItem Text="Resigned" Value="Resigned"></asp:ListItem>
                                                        <asp:ListItem Text="Retired" Value="Retired"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>


                                            <div class="form-group col-md-6" id="ResignedDate" runat="server" visible="false">
                                                <asp:Label for="textfield" ID="lblResignedDate" runat="server" class="control-label col-md-5 lAResignedDate" Style="">
                                                        <span style="color: #ff0000; font-size: 14px;">*</span></asp:Label>
                                                <div class="col-md-7 " style="" id="Div7">
                                                    <asp:TextBox ID="txtRetiredDate" runat="server" placeholder="Ex:MM-dd-yyyy" class="form-control"
                                                        MaxLength="50" TextMode="SingleLine"></asp:TextBox>
                                                    <ajaxToolKit:CalendarExtender ID="CalendarExtender2" PopupButtonID="imgPopup" runat="server"
                                                        TargetControlID="txtRetiredDate" Format="MM-dd-yyyy" PopupPosition="BottomLeft">
                                                    </ajaxToolKit:CalendarExtender>

                                                </div>
                                            </div>
                                            </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <%--<asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>--%>

                                    <div class="col-md-12">
                                        <div class="form-group col-md-4">
                                            <label for="textfield" class="control-label col-sm-3" style="">
                                                Circle</label>
                                            <div class="col-sm-9" style="" id="Div10">
                                                <asp:DropDownList ID="ddlcircle" runat="server" class="form-control DropdownCss" AutoPostBack="true" OnSelectedIndexChanged="ddlcircle_SelectedIndexChanged">
                                                    <%-- <asp:ListItem Text="---Select---" Value="Select"></asp:ListItem>
                                                    <asp:ListItem Text="Adilabad" Value="Adilabad"></asp:ListItem>
                                                    <asp:ListItem Text="Kawal Tiger Reserve" Value="Kawal Tiger Reserve"></asp:ListItem>
                                                    <asp:ListItem Text="Kothagudem" Value="Kothagudem"></asp:ListItem>
                                                    <asp:ListItem Text="Khammam" Value="Khammam"></asp:ListItem>
                                                    <asp:ListItem Text="Nizamabad" Value="Nizamabad"></asp:ListItem>
                                                    <asp:ListItem Text="Amrabada Tiger Reserve" Value="Amrabada Tiger Reserve"></asp:ListItem>
                                                    <asp:ListItem Text="Hyderabad" Value="Hyderabad"></asp:ListItem>
                                                    <asp:ListItem Text="Karimnagar" Value="Karimnagar"></asp:ListItem>
                                                    <asp:ListItem Text="Warangal" Value="Warangal"></asp:ListItem>
                                                    <asp:ListItem Text="Medak" Value="Medak"></asp:ListItem>
                                                    <asp:ListItem Text="Mahabubnagar" Value="Mahabubnagar"></asp:ListItem>
                                                    <asp:ListItem Text="Ranga Reddy" Value="Ranga Reddy"></asp:ListItem>
                                                    <asp:ListItem Text="R & D Circle" Value="R & D Circle"></asp:ListItem>
                                                    <asp:ListItem Text="STC Circle" Value="STC Circle"></asp:ListItem>
                                                    <asp:ListItem Text="Director Zoo Parks" Value="Director Zoo Parks"></asp:ListItem>--%>
                                                </asp:DropDownList>


                                            </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="textfield" class="control-label col-sm-3" style="">
                                                District</label>
                                            <div class="col-sm-9" style="" id="Div13">
                                                <asp:DropDownList ID="ddldistrictforestofficers" runat="server" class="form-control DropdownCss" AutoPostBack="true" OnSelectedIndexChanged="ddldistrictforestofficers_SelectedIndexChanged">
                                                    <%-- <asp:ListItem Text="---Select---" Value="---Select---"></asp:ListItem>--%>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="textfield" class="control-label col-sm-3" style="">
                                                Divisions</label>
                                            <div class="col-sm-9" style="" id="Div17">
                                                <asp:DropDownList ID="ddlforestdivisions" runat="server" class="form-control DropdownCss" AutoPostBack="true" OnSelectedIndexChanged="ddlforestdivisions_SelectedIndexChanged">
                                                    <%-- <asp:ListItem Text="---Select---" Value="---Select---"></asp:ListItem>--%>
                                                </asp:DropDownList>


                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group col-md-4">
                                            <label for="textfield" class="control-label col-sm-3" style="">
                                                Range</label>
                                            <div class="col-sm-9" style="" id="Div18">
                                                <asp:DropDownList ID="ddlrange" runat="server" class="form-control DropdownCss" AutoPostBack="true" OnSelectedIndexChanged="ddlrange_SelectedIndexChanged">
                                                    <%--<asp:ListItem Text="---Select---" Value="Select"></asp:ListItem>
                                                    <asp:ListItem Text="Adilabad" Value="Adilabad"></asp:ListItem>
                                                    <asp:ListItem Text="Kawal Tiger Reserve" Value="Kawal Tiger Reserve"></asp:ListItem>
                                                    <asp:ListItem Text="Kothagudem" Value="Kothagudem"></asp:ListItem>
                                                    <asp:ListItem Text="Khammam" Value="Khammam"></asp:ListItem>
                                                    <asp:ListItem Text="Nizamabad" Value="Nizamabad"></asp:ListItem>
                                                    <asp:ListItem Text="Amrabada Tiger Reserve" Value="Amrabada Tiger Reserve"></asp:ListItem>
                                                    <asp:ListItem Text="Hyderabad" Value="Hyderabad"></asp:ListItem>
                                                    <asp:ListItem Text="Karimnagar" Value="Karimnagar"></asp:ListItem>
                                                    <asp:ListItem Text="Warangal" Value="Warangal"></asp:ListItem>
                                                    <asp:ListItem Text="Medak" Value="Medak"></asp:ListItem>
                                                    <asp:ListItem Text="Mahabubnagar" Value="Mahabubnagar"></asp:ListItem>
                                                    <asp:ListItem Text="Ranga Reddy" Value="Ranga Reddy"></asp:ListItem>
                                                    <asp:ListItem Text="R & D Circle" Value="R & D Circle"></asp:ListItem>
                                                    <asp:ListItem Text="STC Circle" Value="STC Circle"></asp:ListItem>
                                                    <asp:ListItem Text="Director Zoo Parks" Value="Director Zoo Parks"></asp:ListItem>--%>
                                                </asp:DropDownList>


                                            </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="textfield" class="control-label col-sm-3" style="">
                                                Section</label>
                                            <div class="col-sm-9" style="" id="Div19">
                                                <asp:DropDownList ID="ddlsection" runat="server" class="form-control DropdownCss" AutoPostBack="true" OnSelectedIndexChanged="ddlsection_SelectedIndexChanged">
                                                    <%--     <asp:ListItem Text="---Select---" Value="---Select---"></asp:ListItem>--%>
                                                </asp:DropDownList>


                                            </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="textfield" class="control-label col-sm-3" style="">
                                                Beat</label>
                                            <div class="col-sm-9" style="" id="Div23">
                                                <asp:DropDownList ID="ddlbeat" runat="server" class="form-control DropdownCss">
                                                    <%--<asp:ListItem Text="---Select---" Value="---Select---"></asp:ListItem>--%>
                                                </asp:DropDownList>


                                            </div>
                                        </div>
                                    </div>
                                    <%--  </ContentTemplate>
                                    </asp:UpdatePanel>--%>

                                    <div class="col-md-12">


                                        <asp:Panel ID="Panel1" runat="server" GroupingText="Personal Details" Style="border-bottom: 0px solid #8F8F8F;">

                                            <div class="col-md-12">
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-md-3" style="">
                                                        Date of Birth</label>
                                                    <div class="col-md-9" style="" id="Div8">
                                                        <asp:TextBox ID="txtDateOfBirth" runat="server" placeholder="MM-dd-yyyy" class="form-control"
                                                            MaxLength="50"></asp:TextBox>
                                                        <ajaxToolKit:CalendarExtender ID="calendarDOB" PopupButtonID="imgPopup" runat="server"
                                                            TargetControlID="txtDateOfBirth" Format="MM-dd-yyyy" PopupPosition="BottomLeft">
                                                        </ajaxToolKit:CalendarExtender>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-3" style="">
                                                        Age
                                                    </label>
                                                    <div class="col-sm-9" style="" id="Div9">
                                                        <asp:TextBox ID="txtAge" runat="server" placeholder="Age" class="form-control" MaxLength="50"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-3">
                                                        Marital Status</label>
                                                    <div class="col-sm-9">
                                                        <asp:DropDownList ID="ddlMaritalStatus" runat="server" class="form-control ">
                                                            <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                            <asp:ListItem Text="Single" Value="Single"></asp:ListItem>
                                                            <asp:ListItem Text="Married" Value="Married"></asp:ListItem>
                                                            <asp:ListItem Text="Widow" Value="Widow"></asp:ListItem>
                                                            <asp:ListItem Text="Divorced" Value="Divorced"></asp:ListItem>
                                                        </asp:DropDownList>


                                                    </div>

                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-3">
                                                        Gender</label>
                                                    <div class="col-sm-9" style="" id="Div11">
                                                        <asp:RadioButtonList ID="rblGender" runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Text="Female" Value="Female"></asp:ListItem>
                                                            <asp:ListItem Text="Male" Value="Male"></asp:ListItem>
                                                        </asp:RadioButtonList>

                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>

                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-3">
                                                        Blood Group</label>
                                                    <div class="col-sm-9" style="" id="Div12">
                                                        <asp:TextBox ID="txtBloodGroup" runat="server" placeholder="Blood Group" class="form-control"
                                                            MaxLength="15"></asp:TextBox>


                                                    </div>
                                                </div>


                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-3" style="">
                                                        Religion</label>
                                                    <div class="col-sm-9" style="" id="Div14">
                                                        <asp:DropDownList ID="ddlReligion" runat="server" class="form-control DropdownCss">
                                                            <asp:ListItem Text="---Select---" Value="---Select---"></asp:ListItem>
                                                            <asp:ListItem Text="Hindu" Value="Hindu"></asp:ListItem>
                                                            <asp:ListItem Text="Muslim" Value="Muslim"></asp:ListItem>
                                                            <asp:ListItem Text="Christian" Value="Christian"></asp:ListItem>
                                                        </asp:DropDownList>


                                                    </div>
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-3" style="">
                                                        Nationality</label>
                                                    <div class="col-sm-9" style="" id="Div15">
                                                        <asp:DropDownList ID="ddlNationality" runat="server" class="form-control DropdownCss">
                                                            <asp:ListItem Text="---Select---" Value="Select"></asp:ListItem>
                                                            <asp:ListItem Text="Indian" Value="Indian"></asp:ListItem>
                                                            <asp:ListItem Text="OVERSEAS" Value="Overseas"></asp:ListItem>
                                                        </asp:DropDownList>


                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-3" style="">
                                                        Languages Known</label>
                                                    <div class="col-sm-9" id="Div16">
                                                        <asp:TextBox ID="txtLanguagesKnown" runat="server" placeholder="Languages Known" MaxLength="15"
                                                            class="form-control"></asp:TextBox>


                                                    </div>


                                                </div>
                                            </div>

                                        </asp:Panel>


                                    </div>





                                    <div class="col-md-12">

                                        <div class="tab-content" id="Contact">
                                            <%-- <div class="tab-pane" id="Contact">--%>
                                            <asp:Panel ID="Panel3" runat="server" GroupingText="Contact Details"
                                                Style="border-bottom: 0px solid #8F8F8F;">
                                                <div class="col-md-12">
                                                    <h5 style="color: rgb(236, 7, 7);">Present Address</h5>
                                                </div>
                                                <div class="col-md-12">

                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-3" style="">
                                                            Address 1 <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                        <div class="col-sm-9" style="" id="Div20">
                                                            <asp:TextBox ID="txtAddress1" runat="server" placeholder="Address 1" class="form-control"
                                                                MaxLength="250" TextMode="SingleLine"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-3" style="">
                                                            Address 2</label>
                                                        <div class="col-sm-9" style="" id="Div21">
                                                            <asp:TextBox ID="txtAddress2" runat="server" placeholder="Address 2" class="form-control"
                                                                MaxLength="250" TextMode="SingleLine"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-3" style="">
                                                            City<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                        <div class="col-sm-9" style="" id="Div22">
                                                            <asp:DropDownList ID="ddlCity" runat="server" class="form-control DropdownCss">
                                                                <asp:ListItem Text="---Select---" Value="---Select---"></asp:ListItem>
                                                                <asp:ListItem Text="Hyderabad" Value="Hyderabad"></asp:ListItem>
                                                            </asp:DropDownList>

                                                        </div>

                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-3" style="">
                                                            State</label>
                                                        <div class="col-sm-9" style="" id="Div24">
                                                            <asp:TextBox ID="txtState" runat="server" placeholder="State" class="form-control"
                                                                MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-3" style="">
                                                            Postal Code<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                        <div class="col-sm-9" style="" id="Div25">
                                                            <asp:TextBox ID="txtPostalCode" runat="server" placeholder="Postal Code" class="form-control"
                                                                MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-3" style="">
                                                            Country</label>
                                                        <div class="col-sm-9" style="" id="Div26">
                                                            <asp:TextBox ID="txtCountry" runat="server" placeholder="Country" class="form-control"
                                                                MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="col-md-12">
                                                    <div class="col-md-6">
                                                        <h5 style="color: rgb(236, 7, 7);">Permanent Address</h5>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <asp:CheckBox ID="chbPermentAddress" runat="server" OnCheckedChanged="chbPermentAddress_CheckedChanged"
                                                            Text="If Present Address same as Permanent Address" AutoPostBack="true" />

                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-3" style="">
                                                            Address 1 <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                        <div class="col-sm-9" style="" id="Div28">
                                                            <asp:TextBox ID="txtPAddress1" runat="server" placeholder="Address 1" class="form-control"
                                                                MaxLength="250" TextMode="SingleLine"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-3" style="">
                                                            Address 2</label>
                                                        <div class="col-sm-9" style="" id="Div29">
                                                            <asp:TextBox ID="txtPaddress2" runat="server" placeholder="Address 2" class="form-control"
                                                                MaxLength="250" TextMode="SingleLine"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-3" style="">
                                                            City<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                        <div class="col-sm-9" style="" id="Div30">
                                                            <asp:DropDownList ID="ddlPCity" runat="server" class="form-control DropdownCss">
                                                                <asp:ListItem Text="---Select---" Value="---Select---"></asp:ListItem>
                                                                <asp:ListItem Text="Hyderabad" Value="Hyderabad"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-3" style="">
                                                            State</label>
                                                        <div class="col-sm-9" style="" id="Div31">
                                                            <asp:TextBox ID="txtPSate" runat="server" placeholder="State" class="form-control"
                                                                MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-3" style="">
                                                            Postal Code</label>
                                                        <div class="col-sm-9" style="" id="Div32">
                                                            <asp:TextBox ID="txtPPalost" runat="server" placeholder="Postal Code" class="form-control"
                                                                MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-3" style="">
                                                            Country</label>
                                                        <div class="col-sm-9" style="" id="Div33">
                                                            <asp:TextBox ID="txtpCountry" runat="server" placeholder="Country" class="form-control"
                                                                MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-3" style="">
                                                            Telephone</label>
                                                        <div class="col-sm-9" style="" id="Div34">
                                                            <asp:TextBox ID="txtTelephone" runat="server" placeholder="Telephone" class="form-control"
                                                                MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-3" style="">
                                                            Mobile</label>
                                                        <div class="col-sm-9" style="" id="Div35">
                                                            <asp:TextBox ID="txtMobile" runat="server" placeholder="Mobile" class="form-control"
                                                                MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                </div>
                                                <h5>Social Network Info</h5>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-3" style="">
                                                        Email</label>
                                                    <div class="col-sm-9" style="" id="Div36">
                                                        <asp:TextBox ID="txtEmail" runat="server" placeholder="Email" class="form-control"
                                                            MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-3" style="">
                                                        Alternative Email</label>
                                                    <div class="col-sm-9" style="" id="Div37">
                                                        <asp:TextBox ID="txtAlternativeEmail" runat="server" placeholder="Alternative Email"
                                                            class="form-control" MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-3" style="">
                                                        Skype ID</label>
                                                    <div class="col-sm-9" style="" id="Div38">
                                                        <asp:TextBox ID="txtSkypeID" runat="server" placeholder="Skype ID" class="form-control"
                                                            MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-3" style="">
                                                        Linkedin</label>
                                                    <div class="col-sm-9" style="" id="Div39">
                                                        <asp:TextBox ID="txtLinkedin" runat="server" placeholder="Linkedin" class="form-control"
                                                            MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                    </div>
                                                </div>

                                            </asp:Panel>
                                        </div>

                                    </div>

                                    <div class="col-md-12">


                                        <asp:Panel ID="Panel7" runat="server" GroupingText="Personal Identification Data"
                                            Style="border-bottom: 0px solid #8F8F8F;">
                                            <div class="col-md-12">
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        Driving L.No</label>
                                                    <div class="col-sm-7" style="" id="Div40">
                                                        <asp:TextBox ID="txtDriving_License_Number" runat="server" placeholder="Driving License Number"
                                                            class="form-control" MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        Type
                                                    </label>
                                                    <div class="col-sm-7" style="" id="Div41">
                                                        <asp:DropDownList ID="ddlType" runat="server" class="form-control DropdownCss">
                                                            <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                            <asp:ListItem Text="Two Wheeler" Value="Two Wheeler"></asp:ListItem>
                                                            <asp:ListItem Text="LMV" Value="LMV"></asp:ListItem>
                                                            <asp:ListItem Text="HMV" Value="HMV"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        License Expiry Date</label>
                                                    <div class="col-sm-7" style="" id="Div42">

                                                        <asp:TextBox ID="txtLicenseExpiryDate" runat="server" placeholder="Ex:MM-dd-yyyy"
                                                            class="form-control" MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                        <ajaxToolKit:CalendarExtender ID="CalendarLicenseExpiryDate" PopupButtonID="imgPopup" runat="server"
                                                            TargetControlID="txtLicenseExpiryDate" Format="MM-dd-yyyy" PopupPosition="BottomLeft">
                                                        </ajaxToolKit:CalendarExtender>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <div class="col-md-12 attach">
                                                        <span class="btn btn-default btn-file" id="licensetext" runat="server"><span class="fileinput-new" id="text" runat="server">Attach </span>
                                                            <span class="fileinput-exists"></span>
                                                            <%-- <input type="file" id="File1" runat="server" name="..." />--%>
                                                            <asp:FileUpload ID="FupLicense" runat="server" />
                                                        </span>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        Aadhar Number</label>
                                                    <div class="col-sm-7" style="" id="Div43">
                                                        <asp:TextBox ID="txtAadharNumber" runat="server" placeholder="Aadhar Number" class="form-control"
                                                            MaxLength="250" TextMode="SingleLine"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <div class="col-md-12 attach">
                                                        <span class="btn btn-default btn-file" id="aadhartext" runat="server"><span class="fileinput-new">Attach </span><span
                                                            class="fileinput-exists"></span>
                                                            <%--<input type="file" id="File2" runat="server" name="..." />--%>
                                                            <asp:FileUpload ID="FupAadhar" runat="server" />
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        PAN No</label>
                                                    <div class="col-sm-7" style="" id="Div44">
                                                        <asp:TextBox ID="txtpPANNo" runat="server" placeholder="PAN No" class="form-control"
                                                            MaxLength="250" TextMode="SingleLine"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <div class="col-md-12 attach">
                                                        <span class="btn btn-default btn-file" id="pantext" runat="server"><span class="fileinput-new">Attach </span><span
                                                            class="fileinput-exists"></span>
                                                            <%--<input type="file" id="File3" runat="server" name="..." />--%>
                                                            <asp:FileUpload ID="FupPanNo" runat="server" />
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        Passport Number</label>
                                                    <div class="col-sm-7" style="" id="Div45">
                                                        <asp:TextBox ID="txtPassportNumber" runat="server" placeholder="Passport Number"
                                                            class="form-control" MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        Issued Date</label>
                                                    <div class="col-sm-7" style="" id="Div46">
                                                        <asp:TextBox ID="txtIssuedDate" runat="server" placeholder="Ex:MM-dd-yyyy" class="form-control"
                                                            MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                        <ajaxToolKit:CalendarExtender ID="CalendarIssuedDate" PopupButtonID="imgPopup" runat="server"
                                                            TargetControlID="txtIssuedDate" Format="MM-dd-yyyy" PopupPosition="BottomLeft">
                                                        </ajaxToolKit:CalendarExtender>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        Expiry Date</label>
                                                    <div class="col-sm-7" style="" id="Div47">
                                                        <asp:TextBox ID="txtExpiryDate" runat="server" placeholder="Expiry Date" class="form-control"
                                                            MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                        <ajaxToolKit:CalendarExtender ID="CalendarExpiryDate" PopupButtonID="imgPopup" runat="server"
                                                            TargetControlID="txtExpiryDate" Format="MM-dd-yyyy" PopupPosition="BottomLeft">
                                                        </ajaxToolKit:CalendarExtender>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <div class="col-md-6">
                                                        <span class="btn btn-default btn-file" id="passporttext" runat="server"><span class="fileinput-new">Attach </span><span
                                                            class="fileinput-exists"></span>
                                                            <%--<input type="file" id="File4" runat="server" name="..." />--%>
                                                            <asp:FileUpload ID="FupPassport" runat="server" />
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        Bank Name<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                    <div class="col-sm-7" style="" id="Div48">
                                                        <asp:DropDownList ID="ddlPBankName" runat="server" class="form-control DropdownCss">
                                                            <asp:ListItem Text="---Select---" Value="---Select---"></asp:ListItem>
                                                            <asp:ListItem Text="SBI" Value="SBI"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        Account Number</label>
                                                    <div class="col-sm-7" style="" id="Div49">
                                                        <asp:TextBox ID="txtPAccountNumber" runat="server" placeholder="Account Number" class="form-control"
                                                            MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        Health Insurance Policy No</label>
                                                    <div class="col-sm-7" style="" id="Div50">
                                                        <asp:TextBox ID="txtPHealth" runat="server" placeholder="Health / Mediclaim Insurance Policy No"
                                                            class="form-control" MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        Sum Insured</label>
                                                    <div class="col-sm-7" style="" id="Div51">
                                                        <asp:TextBox ID="txtSumInsured" runat="server" placeholder="Sum Insured" class="form-control"
                                                            MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        Renewal Date</label>
                                                    <div class="col-sm-7" style="" id="Div52">
                                                        <asp:TextBox ID="txtRenewalDate" runat="server" placeholder="Ex:MM-dd-yyyy" class="form-control"
                                                            MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                        <ajaxToolKit:CalendarExtender ID="CalendarRenewalDate" PopupButtonID="imgPopup" runat="server"
                                                            TargetControlID="txtRenewalDate" Format="MM-dd-yyyy" PopupPosition="BottomLeft">
                                                        </ajaxToolKit:CalendarExtender>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>


                                    </div>

                                    <div class="col-md-12" runat="server" visible="false">
                                        <div class="tab-pane" id="Emergency">
                                            <asp:Panel ID="pnlFamily" runat="server" GroupingText="Family Information" Style="border-bottom: 0px solid #8F8F8F;">

                                                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                                    <ContentTemplate>
                                                        <div class="col-md-12">
                                                            <div style="overflow-x: scroll; width: 100%;">
                                                                <asp:GridView ID="grdFamily" class="table table-hover table-nomargin table-bordered CustomerAddress EducationGrid"
                                                                    Style="margin-bottom: 0.83%; margin-top: 0.83%;" runat="server" OnRowDataBound="grdFamily_RowDataBound"
                                                                    ShowFooter="true" AutoGenerateColumns="False" OnRowCreated="grdFamily_RowCreated">

                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="Name">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="txtName" runat="server" placeholder="Name" class="form-control"
                                                                                    MaxLength="10" Text=""></asp:TextBox><%--Name--%>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Width="15%" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Relationship">
                                                                            <ItemTemplate>
                                                                                <asp:DropDownList ID="ddlRelationship" runat="server" class="form-control  DropdownCss"
                                                                                    MaxLength="20" DataTextField="Relationship" DataValueField="Relationship">
                                                                                    <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                                    <asp:ListItem Text="Father" Value="Father"></asp:ListItem>
                                                                                    <asp:ListItem Text="Mother" Value="Mother"></asp:ListItem>
                                                                                    <asp:ListItem Text="Spouse" Value="Spouse"></asp:ListItem>
                                                                                    <asp:ListItem Text="Daughter" Value="Daughter"></asp:ListItem>
                                                                                    <asp:ListItem Text="Son" Value="Son"></asp:ListItem>
                                                                                    <asp:ListItem Text="Brother" Value="Brother"></asp:ListItem>
                                                                                    <asp:ListItem Text="Sister" Value="Sister"></asp:ListItem>
                                                                                    <asp:ListItem Text="Others" Value="Others"></asp:ListItem>
                                                                                </asp:DropDownList>
                                                                                <asp:TextBox ID="txtrelation" runat="server" placeholder="Name" class="form-control"
                                                                                    Visible="false" Text=""></asp:TextBox><%--Relationship--%>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Width="7%" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Date of Birth">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="txtFDateofBirth" runat="server" placeholder="Ex:MM-dd-yyyy" class="form-control"
                                                                                    Text="" MaxLength="13"></asp:TextBox><%--Date_of_Birth--%>
                                                                                <ajaxToolKit:CalendarExtender ID="CalendarGrdDOB" PopupButtonID="imgPopup" runat="server"
                                                                                    TargetControlID="txtFDateofBirth" Format="MM-dd-yyyy" PopupPosition="BottomLeft">
                                                                                </ajaxToolKit:CalendarExtender>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Width="9%" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Occupation">
                                                                            <ItemTemplate>
                                                                                <asp:DropDownList ID="ddlOccupation" runat="server" class="form-control  DropdownCss"
                                                                                    MaxLength="20" DataTextField="Occupation" DataValueField="Occupation">
                                                                                    <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                                    <asp:ListItem Text="Govt Service" Value="Govt Service"></asp:ListItem>
                                                                                    <asp:ListItem Text="Private Service" Value="Private Service"></asp:ListItem>
                                                                                    <asp:ListItem Text="Self Employed" Value="Self Employed"></asp:ListItem>
                                                                                    <asp:ListItem Text="Housewife" Value="Housewife"></asp:ListItem>
                                                                                    <asp:ListItem Text="Student" Value="Student"></asp:ListItem>
                                                                                    <asp:ListItem Text="Retired" Value="Retired"></asp:ListItem>
                                                                                    <asp:ListItem Text="Others" Value="Others"></asp:ListItem>
                                                                                </asp:DropDownList>
                                                                                <asp:TextBox ID="txtoccupation" runat="server" placeholder="Name" class="form-control"
                                                                                    Visible="false" Text=""></asp:TextBox><%--Occupation--%>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Width="10%" />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Dependent">
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chbDependent" runat="server" Text="" />
                                                                                <asp:Label ID="lblchkdefault1" runat="server" Visible="false" Text='<%# Eval("Member_Dependent")%>'></asp:Label><%--Dependent--%>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Width="3%" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Nominee">
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chbNominee" runat="server" Text="" AutoPostBack="true" OnCheckedChanged="chbNominee_CheckedChanged" />
                                                                                <%--<asp:RadioButton ID="chbNominee" runat="server" Text="" />--%>
                                                                                <asp:Label ID="lblchkdefault2" runat="server" Visible="false" Text='<%# Eval("Member_Nominee")%>'></asp:Label><%--Nominee--%>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Width="3%" />
                                                                        </asp:TemplateField>


                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lbRemove" runat="server" OnClick="lbRemove_Click">Remove</asp:LinkButton>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Width="3%" />
                                                                            <FooterStyle HorizontalAlign="Right" />
                                                                            <FooterTemplate>
                                                                                <asp:Button ID="btnAddNewRecord" runat="server" Text="Record" class="btn btn-primary"
                                                                                    OnClick="btnAddNewRecord_Click" />
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                                </di>
                                                            </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </asp:Panel>

                                        </div>
                                    </div>


                                    <div class="col-md-12" runat="server" visible="false">

                                        <asp:Panel ID="Panel6" runat="server" GroupingText="Educational Qualifications" Style="border-bottom: 0px solid #8F8F8F;">
                                            <div class="col-md-12">
                                                <div class="form-group col-md-12">
                                                    <label for="textfield" class="control-label col-sm-4"
                                                        style="">
                                                        Educate</label>
                                                    <div class="col-sm-8" style="" id="Div53">
                                                        <asp:DropDownList ID="ddlEducate" runat="server" class="form-control DropdownCss">
                                                            <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                            <asp:ListItem Text="S.S.C" Value="S.S.C"></asp:ListItem>
                                                            <asp:ListItem Text="Intermediate" Value="Intermediate"></asp:ListItem>
                                                            <asp:ListItem Text="Elementary school" Value="Elementary school"></asp:ListItem>
                                                            <asp:ListItem Text="Master Degree" Value="Master Degree"></asp:ListItem>
                                                            <asp:ListItem Text="Technical School" Value="Technical School"></asp:ListItem>
                                                            <asp:ListItem Text="Middle School" Value="Middle School"></asp:ListItem>
                                                            <asp:ListItem Text="Bachalor Degree" Value="Bachalor Degree"></asp:ListItem>

                                                        </asp:DropDownList>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-12" style="overflow-x: scroll; width: 100%;">
                                                <asp:GridView ID="grdEDucation" class="table table-bordered CustomerAddress EducationGrid" runat="server" ShowFooter="true"
                                                    AutoGenerateColumns="False">

                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Qualification/Course">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtQualification" runat="server" placeholder="Qualification" class="form-control"
                                                                    MaxLength="30" Text='<%# Eval("Qualification_Certification")%>'></asp:TextBox>
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="17%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Institute / University">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtInstitute_University" runat="server" placeholder="Institute / University"
                                                                    class="form-control" Text='<%# Eval("Institute_University")%>' MaxLength="30"></asp:TextBox>
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="15%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Year Of Pass">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtYearOfPass" runat="server" placeholder="Year Of Pass" class="form-control"
                                                                    Text='<%# Eval("Year")%>' MaxLength="10"></asp:TextBox>
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="8%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="% of Marks / GPA">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtMarks_GPA" runat="server" placeholder="% of Marks / GPA" class="form-control"
                                                                    Text='<%# Eval("Percentage")%>' MaxLength="30"></asp:TextBox>
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="10%" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lbRemoveRow" runat="server" OnClick="lkbRemoveEducationRecord_Click">Remove</asp:LinkButton>
                                                            </ItemTemplate>
                                                            <FooterStyle HorizontalAlign="Right" />
                                                            <FooterTemplate>
                                                                <asp:Button ID="btnNewRecordEducation" runat="server" Text="Record" class="btn btn-primary"
                                                                    OnClick="btnNewRecordEducation_Click" />
                                                            </FooterTemplate>
                                                            <HeaderStyle Width="4%" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </asp:Panel>
                                        <asp:Panel ID="Panel9" runat="server" GroupingText="Skill Matrix" Style="border-bottom: 0px solid #8F8F8F; display: none;">
                                            <div class="col-md-12">
                                                <asp:GridView ID="grvSkill" class="table table-bordered CustomerAddress" runat="server" ShowFooter="true"
                                                    AutoGenerateColumns="False" OnRowDataBound="grvSkill_RowDataBound">

                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Skill">
                                                            <ItemTemplate>
                                                                <asp:DropDownList ID="ddlSkill_Name" runat="server" class="form-control  DropdownCss"
                                                                    MaxLength="20" DataTextField="Skill_Name" DataValueField="Skill_Name">
                                                                    <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                    <asp:ListItem Text="Java" Value="Java"></asp:ListItem>
                                                                    <asp:ListItem Text="Dotnet" Value="Dotnet"></asp:ListItem>
                                                                    <asp:ListItem Text="C++" Value="C++"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:TextBox ID="txtSkillName" runat="server" placeholder="Skill Name" class="form-control"
                                                                    MaxLength="30" Text='<%# Eval("Skill_Name")%>' Visible="false"></asp:TextBox>
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="25%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Level">
                                                            <ItemTemplate>
                                                                <asp:DropDownList ID="ddlLevel" runat="server" class="form-control  DropdownCss"
                                                                    MaxLength="20" DataTextField="Skill_Level" DataValueField="Skill_Level"
                                                                    OnSelectedIndexChanged="ddlLevel_SelectedIndexChanged" AutoPostBack="true">
                                                                    <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                    <asp:ListItem Text="Level 1" Value="Level 1"></asp:ListItem>
                                                                    <asp:ListItem Text="Level 2" Value="Level 2"></asp:ListItem>
                                                                    <asp:ListItem Text="Level 3" Value="Level 3"></asp:ListItem>
                                                                    <asp:ListItem Text="Level 4" Value="Level 4"></asp:ListItem>
                                                                    <asp:ListItem Text="Level 5" Value="Level 5"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:TextBox ID="txtLevel" runat="server" placeholder="Level" class="form-control"
                                                                    Text='<%# Eval("Skill_Level")%>' MaxLength="30" Visible="false"></asp:TextBox>
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="15%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Description">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtDescription" runat="server" placeholder="Description" class="form-control"
                                                                    Text='<%# Eval("Description")%>' MaxLength="30"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lbRemoveRowEducationSkill" runat="server" OnClick="lbRemoveRowEducationSkill_Click">Remove</asp:LinkButton>
                                                            </ItemTemplate>
                                                            <FooterStyle HorizontalAlign="Right" />
                                                            <FooterTemplate>
                                                                <asp:Button ID="btnNewRecordEducationSkill" runat="server" OnClick="btnNewRecordEducationSkill_Click"
                                                                    Text="Record" class="btn btn-primary" />
                                                            </FooterTemplate>
                                                            <HeaderStyle Width="2%" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </asp:Panel>
                                        <asp:Panel ID="Panel10" runat="server" GroupingText="Certifications Holding" Style="border-bottom: 0px solid #8F8F8F; display: none;">
                                            <div class="col-md-12">
                                                <asp:GridView ID="grvCertification" class="table table-bordered CustomerAddress" runat="server"
                                                    ShowFooter="true" AutoGenerateColumns="False">

                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Course / Skill Name">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtCourse_Skill_Name" runat="server" placeholder="Course / Skill Name"
                                                                    class="form-control" MaxLength="30" Text='<%# Eval("Course_Skill_Name")%>'></asp:TextBox>
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="18%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Institute">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtInstitute" runat="server" placeholder="Institute" class="form-control"
                                                                    Text='<%# Eval("Institute")%>' MaxLength="30"></asp:TextBox>
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="15%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Year">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtYear" runat="server" placeholder="Year" class="form-control"
                                                                    Text='<%# Eval("Year")%>' MaxLength="10"></asp:TextBox>
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="5%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Attach Certificate Copy">
                                                            <ItemTemplate>
                                                                <span class="btn btn-default btn-file"><span class="fileinput-new">Attach </span><span
                                                                    class="fileinput-exists"></span>
                                                                    <input type="file" id="File11" runat="server" name="..." />
                                                                </span>
                                                                <asp:TextBox ID="txtAttach_Certificate_Copy" runat="server" placeholder="Attach Certificate Copy"
                                                                    class="form-control" Text='<%# Eval("Attach_Certificate_Copy")%>' MaxLength="30"
                                                                    Visible="false"></asp:TextBox>
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="16%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lbRemoveRow" runat="server">Remove</asp:LinkButton>
                                                            </ItemTemplate>
                                                            <FooterStyle HorizontalAlign="Right" />
                                                            <FooterTemplate>
                                                                <asp:Button ID="btnNewRecordEducation" runat="server" Text="Record" class="btn btn-primary" />
                                                            </FooterTemplate>
                                                            <HeaderStyle Width="2%" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </asp:Panel>

                                    </div>


                                    <div class="col-md-12" runat="server" visible="false">

                                        <asp:Panel ID="Panel11" runat="server" GroupingText="Professional Experience" Style="border-bottom: 0px solid #8F8F8F;">

                                            <asp:GridView ID="grvProfessional" class="table table-bordered CustomerAddress" runat="server"
                                                ShowFooter="true" AutoGenerateColumns="False">

                                                <Columns>
                                                    <asp:TemplateField HeaderText="Name of the Employer">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtEmployer" runat="server" placeholder="Name of the Employer"
                                                                class="form-control" MaxLength="30" Text='<%# Eval("Emp_Name")%>'></asp:TextBox>
                                                        </ItemTemplate>
                                                        <HeaderStyle Width="18%" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Position Held">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtPosition" runat="server" placeholder="Position Held" class="form-control"
                                                                Text='<%# Eval("Emp_Designation")%>' MaxLength="30"></asp:TextBox>

                                                        </ItemTemplate>
                                                        <HeaderStyle Width="10%" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Period From">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtPeriodFrom" runat="server" placeholder="Period From" class="form-control"
                                                                Text='<%#(Eval("Period_From"))%>' MaxLength="10" AutoPostBack="true" OnTextChanged="txtPeriodFrom_OnTextChanged"></asp:TextBox>
                                                            <ajaxToolKit:CalendarExtender ID="CalendarGrdPeriodFrom" PopupButtonID="imgPopup" runat="server"
                                                                TargetControlID="txtPeriodFrom" Format="MM-dd-yyyy" PopupPosition="BottomLeft">
                                                            </ajaxToolKit:CalendarExtender>
                                                        </ItemTemplate>
                                                        <HeaderStyle Width="7%" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Period To">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtPeriodTo" runat="server" placeholder="Period To" class="form-control"
                                                                Text='<%#(Eval("Period_To"))%>' MaxLength="10" AutoPostBack="true" OnTextChanged="txtPeriodTo_OnTextChanged"></asp:TextBox>

                                                            <ajaxToolKit:CalendarExtender ID="CalendarGrdPeriodTo" PopupButtonID="imgPopup" runat="server"
                                                                TargetControlID="txtPeriodTo" Format="MM-dd-yyyy" PopupPosition="BottomLeft">
                                                            </ajaxToolKit:CalendarExtender>

                                                        </ItemTemplate>
                                                        <HeaderStyle Width="6%" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Total Duration">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtTotalDuration" runat="server" placeholder="Total Duration" class="form-control"
                                                                Text='<%# Eval("Total_Duration")%>' MaxLength="10"></asp:TextBox>
                                                        </ItemTemplate>
                                                        <HeaderStyle Width="8%" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lbRemoveRowProf" runat="server" OnClick="lbRemoveRowProf_Click">Remove</asp:LinkButton>
                                                        </ItemTemplate>
                                                        <FooterStyle HorizontalAlign="Right" />
                                                        <FooterTemplate>
                                                            <asp:Button ID="btnNewRecordPRof" runat="server" Text="Record" class="btn btn-primary" OnClick="btnNewRecordPRof_Click" />
                                                        </FooterTemplate>
                                                        <HeaderStyle Width="2%" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>




                                        </asp:Panel>
                                    </div>







                                    <div class="col-md-12">

                                        <asp:Panel ID="Panel8" runat="server" GroupingText="Official Information" Style="border-bottom: 0px solid #8F8F8F;">
                                            <div class="col-md-12">
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        Date of Join</label>
                                                    <div class="col-sm-7" style="" id="Div58">
                                                        <asp:TextBox ID="txtDateofJoin" runat="server" placeholder="Ex:MM-dd-yyyy" class="form-control"
                                                            MaxLength="25" TextMode="SingleLine"></asp:TextBox>

                                                        <ajaxToolKit:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="imgPopup"
                                                            TargetControlID="txtDateofJoin" Format="MM-dd-yyyy" PopupPosition="BottomRight">
                                                        </ajaxToolKit:CalendarExtender>
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-6" runat="server" visible="false">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        Job Title / Designation</label>
                                                    <div class="col-sm-7" style="">
                                                        <asp:DropDownList ID="ddlJobTitle" runat="server" class="form-control DropdownCss">
                                                            <asp:ListItem Text="---Select---" Value="---Select---"></asp:ListItem>
                                                            <asp:ListItem Text="APCCF" Value="APCCF"></asp:ListItem>
                                                            <asp:ListItem Text="HOFF" Value="HOFF"></asp:ListItem>
                                                            <asp:ListItem Text="POA" Value="POA"></asp:ListItem>
                                                            <asp:ListItem Text="CCF" Value="CCF"></asp:ListItem>
                                                            <asp:ListItem Text="DFO" Value="DFO"></asp:ListItem>
                                                            <asp:ListItem Text="FDO" Value="FDO"></asp:ListItem>
                                                            <asp:ListItem Text="PCCF" Value="PCCF"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>


                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        Employee Grade</label>
                                                    <div class="col-sm-7" style="" id="Div60">
                                                        <asp:DropDownList ID="ddlEMPGrade" runat="server"
                                                            class="form-control DropdownCss">
                                                            <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                </div>
                                                <div class="form-group col-md-6" runat="server" visible="false">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        Department<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                    <div class="col-sm-7" style="" id="Div63">
                                                        <asp:DropDownList ID="ddlDepartment" runat="server" class="form-control DropdownCss">
                                                            <asp:ListItem Text="---Select---" Value="---Select---"></asp:ListItem>
                                                            <asp:ListItem Text="HRD" Value="HRD"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                </div>


                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        Reporting Authority<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                    <div class="col-sm-7" style="" id="Div66">
                                                        <asp:DropDownList ID="ddlReportingAuthority" runat="server" class="form-control DropdownCss">
                                                            <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                            <asp:ListItem Text="HOFF" Value="HOFF"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>


                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        Location<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                    <div class="col-sm-7" style="">
                                                        <asp:DropDownList ID="ddlLocation" runat="server" class="form-control DropdownCss">
                                                            <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                            <asp:ListItem Text="hyd" Value="hyd"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>


                                                </div>


                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        Work Type<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                    <div class="col-sm-7" style="">
                                                        <asp:DropDownList ID="ddlWorkType" runat="server" class="form-control DropdownCss">
                                                            <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                            <asp:ListItem Text="Direct" Value="Direct"></asp:ListItem>
                                                            <asp:ListItem Text="InDirect" Value="InDirect"></asp:ListItem>

                                                        </asp:DropDownList>
                                                    </div>

                                                </div>




                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        Recruit<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                    <div class="col-sm-7" style="" id="Recruit">
                                                        <asp:DropDownList ID="ddlRecruit" runat="server" class="form-control DropdownCss">
                                                            <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                            <asp:ListItem Text="Direct" Value="Direct"></asp:ListItem>
                                                            <asp:ListItem Text="InDirect" Value="InDirect"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                </div>
                                                <div class="form-group col-md-6">

                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        Working in Shifts</label>
                                                    <div class="col-sm-7" style="" id="Div69">
                                                        <asp:CheckBox ID="chbWorking" runat="server" Text="" />
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <asp:Panel ID="PanelSalary" runat="server" GroupingText="Salary and Perks" Style="border-bottom: 0px solid #8F8F8F;">
                                            <div class="col-md-10">

                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        Basic Amount<span style="color: #ff0000; font-size: 14px; display: none;">*</span></label>
                                                    <div class="col-sm-7" style="" id="Div55">
                                                        <asp:TextBox ID="txtBasicAmount" runat="server" AutoPostBack="true"
                                                            OnTextChanged="txtBasicAmount_TextChanged" placeholder="Basic Amount" class="form-control"
                                                            MaxLength="50"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        Gross Amount</label>
                                                    <div class="col-sm-7" style="" id="Div56">
                                                        <asp:TextBox ID="txtGrossAmount" runat="server" placeholder="Gross Amount" class="form-control"
                                                            MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        DA<span style="color: #ff0000; font-size: 14px; display: none;">*</span></label>
                                                    <div class="col-sm-7" style="" id="Div57">
                                                        <asp:TextBox ID="txtda" runat="server" AutoPostBack="true"
                                                            placeholder="DA" class="form-control"
                                                            MaxLength="50"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        HRA</label>
                                                    <div class="col-sm-7" style="" id="Div64">
                                                        <asp:TextBox ID="txthra" runat="server" placeholder="HRA" class="form-control"
                                                            MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div></div>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        CCA</label>
                                                    <div class="col-sm-7" style="" id="Div67">
                                                        <asp:TextBox ID="txtcca" runat="server" placeholder="CCA" class="form-control"
                                                            MaxLength="20" OnTextChanged="txtcca_TextChanged"></asp:TextBox>
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="col-md-12">
                                                <asp:GridView ID="grvSalary" class="table table-bordered CustomerAddress" runat="server" ShowFooter="true"
                                                    AutoGenerateColumns="False">

                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Head">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtHead" runat="server" placeholder="Head" class="form-control"
                                                                    MaxLength="10" Text='<%# Eval("Head")%>'></asp:TextBox>
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="15%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Amount">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtAmount" runat="server" placeholder="Amount" class="form-control"
                                                                    Text='<%# Eval("Amount")%>' MaxLength="30" AutoPostBack="true" OnTextChanged="txtAmount_TextChanged"></asp:TextBox>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Label ID="lbltotalAmount" runat="server" Text=""></asp:Label>
                                                            </FooterTemplate>
                                                            <HeaderStyle Width="13%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lbRemoveSalary" runat="server">Remove</asp:LinkButton>
                                                            </ItemTemplate>
                                                            <FooterStyle HorizontalAlign="Right" />
                                                            <FooterTemplate>
                                                                <asp:Button ID="btnAddNewRecordSalary" runat="server" Text="Record" class="btn btn-primary" />
                                                            </FooterTemplate>
                                                            <HeaderStyle Width="2%" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </asp:Panel>

                                        <asp:Panel ID="Panel2Deductions" runat="server" GroupingText="Deductions" Style="border-bottom: 0px solid #8F8F8F;">
                                            <div class="col-md-12">
                                                <div class="form-group col-md-4">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        PF</label>
                                                    <div class="col-sm-7" style="" id="Div70">
                                                        <asp:CheckBox ID="chbPF" runat="server" Text="" />
                                                    </div>

                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        PF Code</label>
                                                    <div class="col-sm-7" style="" id="Div71">
                                                        <asp:TextBox ID="txtPFCode" runat="server" placeholder="PF Code" class="form-control"
                                                            MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        PF Amount</label>
                                                    <div class="col-sm-7" style="" id="Div68">
                                                        <asp:TextBox ID="txtpfamount" runat="server" placeholder="PF Amount" class="form-control"
                                                            MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        ESI</label>
                                                    <div class="col-sm-7" style="" id="Div72">
                                                        <asp:CheckBox ID="chbESI" runat="server" Text="" />
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        ESI Code</label>
                                                    <div class="col-sm-7" style="" id="Div73">
                                                        <asp:TextBox ID="txtESICode" runat="server" placeholder="ESI Number" class="form-control"
                                                            MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        ESI Amount</label>
                                                    <div class="col-sm-7" style="" id="Div74">
                                                        <asp:TextBox ID="txtesiamount" runat="server" placeholder="ESI Amount" class="form-control"
                                                            MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        EESI Amount</label>
                                                    <div class="col-sm-7" style="" id="Div75">
                                                        <asp:TextBox ID="txteesi" runat="server" placeholder="EESI Amount" class="form-control"
                                                            MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-md-5 lAESIDispensary" style="">
                                                        ESI Dispensary</label>
                                                    <div class="col-md-7" id="Div76">
                                                        <asp:DropDownList ID="ddlESIDispensary" runat="server" class="form-control DropdownCss">
                                                            <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-md-5" style="">
                                                        Professional Tax (PT)</label>
                                                    <div class="col-md-1" style="" id="Div77">
                                                        <asp:CheckBox ID="chbProfessional" runat="server" Text="" />
                                                    </div>
                                                    <div class="col-md-6" style="" id="Div78">
                                                        <asp:DropDownList ID="ddlptax" runat="server" class="form-control DropdownCss">
                                                            <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                            <asp:ListItem Text="15001-20000" Value="150"></asp:ListItem>
                                                            <asp:ListItem Text="20001+" Value="200"></asp:ListItem>
                                                        </asp:DropDownList>

                                                    </div>
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-md-5" style="">
                                                        TDS</label>
                                                    <div class="col-md-7" style="" id="Div80">
                                                        <asp:TextBox ID="txttds" runat="server" placeholder="TDS" class="form-control"
                                                            MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>

                                    </div>
                                    <div class="col-md-12">

                                        <asp:Panel ID="Panel2Information" runat="server" GroupingText="Information on Company Facilities Availed">
                                            <div class="col-md-12">
                                                <div class="form-group col-md-4">
                                                    <label for="textfield" class="control-label col-md-5">
                                                        Accommodation</label>
                                                    <div class="col-md-7 " style="" id="Div82">
                                                        <asp:CheckBox ID="chbQuarter" runat="server" Text="" />
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="textfield" class="control-label col-md-5">
                                                        Four Wheeler</label>
                                                    <div class="col-md-7" style="" id="Div83">
                                                        <asp:CheckBox ID="chbFourWheeler" runat="server" Text="" />
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="textfield" class="control-label col-md-5">
                                                        Two Wheeler</label>
                                                    <div class="col-md-7" style="" id="Div84">
                                                        <asp:CheckBox ID="chbTwoWheeler" runat="server" Text="" />
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="textfield" class="control-label col-md-5">
                                                        Laptop</label>
                                                    <div class="col-md-7" style="" id="Div85">
                                                        <asp:CheckBox ID="chbLaptop" runat="server" Text="" />
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="textfield" class="control-label col-md-5">
                                                        Mobile Phone</label>
                                                    <div class="col-md-7" style="" id="Div86">
                                                        <asp:CheckBox ID="chbMobilePhone" runat="server" Text="" />
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label for="textfield" class="control-label col-md-5">
                                                        Other Facilities</label>
                                                    <div class="col-md-7" style="" id="Div87">
                                                        <asp:TextBox ID="txtOtherFacilities" runat="server" placeholder="Other Facilities"
                                                            class="form-control" MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>






                                    <div class="col-md-12">

                                        <asp:Panel ID="Panel4" runat="server" align="center" Style="display: none; height: 30%;">
                                            <div class="modal-header" runat="server" id="Popupheader">
                                                <h3>City<a class="close-modal" href="#" id="btnClose">&times;</a></h3>
                                            </div>
                                            <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                                <ContentTemplate>
                                                    <div class="modal-body">
                                                        <div class="panel-body">
                                                            <div class="col-md-12">
                                                                <div class="form-group col-md-6">
                                                                    <label for="textfield" class="control-label col-md-5" style="">
                                                                        City ID
                                                                    </label>
                                                                    <div class="col-sm-7" style="margin-left: 0px;">
                                                                        <asp:TextBox ID="txtCityID" runat="server" class="form-control" placeholder="City ID"
                                                                            TextMode="SingleLine" MaxLength="10"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label for="textfield" class="control-label col-md-5" style="">
                                                                        City Name <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                                    <div class="col-sm-7" style="margin-left: 0px;">
                                                                        <asp:TextBox ID="txtCityName" runat="server" class="form-control" placeholder="City Name"
                                                                            TextMode="SingleLine"></asp:TextBox>
                                                                    </div>
                                                                </div>


                                                                <div class="form-group col-md-6">
                                                                    <label for="textfield" class="control-label col-md-5" style="">
                                                                        State <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                                    <div class="col-sm-7" style="margin-left: 0px;">
                                                                        <asp:DropDownList ID="ddlState" runat="server" class="form-control DropdownCss">
                                                                            <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                            <asp:ListItem Text="Andhra Pradesh" Value="Andhra Pradesh"></asp:ListItem>
                                                                            <asp:ListItem Text="Arunachal Pradesh" Value="Arunachal Pradesh"></asp:ListItem>
                                                                            <asp:ListItem Text="Assam" Value="Assam"></asp:ListItem>
                                                                            <asp:ListItem Text="Bihar" Value="Bihar"></asp:ListItem>
                                                                            <asp:ListItem Text="Chhattisgarh" Value="Chhattisgarh"></asp:ListItem>
                                                                            <asp:ListItem Text="Goa" Value="Goa"></asp:ListItem>
                                                                            <asp:ListItem Text="Gujarat" Value="Gujarat"></asp:ListItem>
                                                                            <asp:ListItem Text="Haryana" Value="Haryana"></asp:ListItem>
                                                                            <asp:ListItem Text="Himachal Pradesh" Value="Himachal Pradesh"></asp:ListItem>
                                                                            <asp:ListItem Text="Jammu and Kashmir" Value="Jammu and Kashmir"></asp:ListItem>
                                                                            <asp:ListItem Text="Jharkhand" Value="Jharkhand"></asp:ListItem>
                                                                            <asp:ListItem Text="Karnataka" Value="Karnataka"></asp:ListItem>
                                                                            <asp:ListItem Text="Kerala" Value="Kerala"></asp:ListItem>
                                                                            <asp:ListItem Text="Madhya Pradesh" Value="Madhya Pradesh"></asp:ListItem>
                                                                            <asp:ListItem Text="Maharashtra" Value="Maharashtra"></asp:ListItem>
                                                                            <asp:ListItem Text="Manipur" Value="Manipur"></asp:ListItem>
                                                                            <asp:ListItem Text="Meghalaya" Value="Meghalaya"></asp:ListItem>
                                                                            <asp:ListItem Text="Mizoram" Value="Mizoram"></asp:ListItem>
                                                                            <asp:ListItem Text="Nagaland" Value="Nagaland"></asp:ListItem>
                                                                            <asp:ListItem Text="Odisha" Value="Odisha"></asp:ListItem>
                                                                            <asp:ListItem Text="Punjab" Value="Punjab"></asp:ListItem>
                                                                            <asp:ListItem Text="Rajasthan" Value="Rajasthan"></asp:ListItem>
                                                                            <asp:ListItem Text="Sikkim" Value="Sikkim"></asp:ListItem>
                                                                            <asp:ListItem Text="Tamil Nadu" Value="Tamil Nadu"></asp:ListItem>
                                                                            <asp:ListItem Text="Telangana" Value="Telangana"></asp:ListItem>
                                                                            <asp:ListItem Text="Tripura" Value="Tripura"></asp:ListItem>
                                                                            <asp:ListItem Text="Uttar Pradesh" Value="Uttar Pradesh"></asp:ListItem>
                                                                            <asp:ListItem Text="Uttarakhand" Value="Uttarakhand"></asp:ListItem>
                                                                            <asp:ListItem Text="West Bengal" Value="West Bengal"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group col-md-6">
                                                                    <label for="textfield" class="control-label col-md-5" style="">
                                                                        Country <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                                    <div class="col-sm-7" style="margin-left: 0px;">
                                                                        <asp:DropDownList ID="ddlCountry" runat="server" class="form-control DropdownCss">
                                                                            <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                            <asp:ListItem Text="Afghanistan" Value="Afghanistan"></asp:ListItem>
                                                                            <asp:ListItem Text="Australia" Value="Australia"></asp:ListItem>
                                                                            <asp:ListItem Text="Bangladesh" Value="Bangladesh"></asp:ListItem>
                                                                            <asp:ListItem Text="Barbados" Value="Barbados"></asp:ListItem>
                                                                            <asp:ListItem Text="Brazil" Value="Brazil"></asp:ListItem>
                                                                            <asp:ListItem Text="Burma" Value="Burma"></asp:ListItem>
                                                                            <asp:ListItem Text="Canada" Value="Canada"></asp:ListItem>
                                                                            <asp:ListItem Text="China" Value="China"></asp:ListItem>
                                                                            <asp:ListItem Text="Colombia" Value="Colombia"></asp:ListItem>
                                                                            <asp:ListItem Text="Cuba" Value="Cuba"></asp:ListItem>
                                                                            <asp:ListItem Text="Denmark" Value="Denmark"></asp:ListItem>
                                                                            <asp:ListItem Text="Dominica" Value="Dominica"></asp:ListItem>
                                                                            <asp:ListItem Text="Egypt" Value="Egypt"></asp:ListItem>
                                                                            <asp:ListItem Text="Finland" Value="Finland"></asp:ListItem>
                                                                            <asp:ListItem Text="France" Value="France"></asp:ListItem>
                                                                            <asp:ListItem Text="Germany" Value="Germany"></asp:ListItem>
                                                                            <asp:ListItem Text="Greece" Value="Greece"></asp:ListItem>
                                                                            <asp:ListItem Text="Hong Kong" Value="Hong Kong"></asp:ListItem>
                                                                            <asp:ListItem Text="Iceland" Value="Iceland"></asp:ListItem>
                                                                            <asp:ListItem Text="India" Value="India"></asp:ListItem>
                                                                            <asp:ListItem Text="Indonesia" Value="Indonesia"></asp:ListItem>
                                                                            <asp:ListItem Text="Iran" Value="Iran"></asp:ListItem>
                                                                            <asp:ListItem Text="Iraq" Value="Iraq"></asp:ListItem>
                                                                            <asp:ListItem Text="Ireland" Value="Ireland"></asp:ListItem>
                                                                            <asp:ListItem Text="Italy" Value="Italy"></asp:ListItem>
                                                                            <asp:ListItem Text="Jamaica" Value="Jamaica"></asp:ListItem>
                                                                            <asp:ListItem Text="Japan" Value="Japan"></asp:ListItem>
                                                                            <asp:ListItem Text="Jordan" Value="Jordan"></asp:ListItem>
                                                                            <asp:ListItem Text="Kenya" Value="Kenya"></asp:ListItem>
                                                                            <asp:ListItem Text="Kuwait" Value="Kuwait"></asp:ListItem>
                                                                            <asp:ListItem Text="Libya" Value="Libya"></asp:ListItem>
                                                                            <asp:ListItem Text="Madagascar" Value="Madagascar"></asp:ListItem>
                                                                            <asp:ListItem Text="Malaysia" Value="Malaysia"></asp:ListItem>
                                                                            <asp:ListItem Text="Mexico" Value="Mexico"></asp:ListItem>
                                                                            <asp:ListItem Text="Namibia" Value="Namibia"></asp:ListItem>
                                                                            <asp:ListItem Text="Nepal" Value="Nepal"></asp:ListItem>
                                                                            <asp:ListItem Text="Netherlands" Value="Netherlands"></asp:ListItem>
                                                                            <asp:ListItem Text="New Zealand" Value="New Zealand"></asp:ListItem>
                                                                            <asp:ListItem Text="Pakistan" Value="Pakistan"></asp:ListItem>
                                                                            <asp:ListItem Text="Philippines" Value="Philippines"></asp:ListItem>
                                                                            <asp:ListItem Text="Poland" Value="Poland"></asp:ListItem>
                                                                            <asp:ListItem Text="Russia" Value="Russia"></asp:ListItem>
                                                                            <asp:ListItem Text="San Marino" Value="San Marino"></asp:ListItem>
                                                                            <asp:ListItem Text="Singapore" Value="Singapore"></asp:ListItem>
                                                                            <asp:ListItem Text="South Africa" Value="South Africa"></asp:ListItem>
                                                                            <asp:ListItem Text="Spain" Value="Spain"></asp:ListItem>
                                                                            <asp:ListItem Text="Sri Lanka" Value="Sri Lanka"></asp:ListItem>
                                                                            <asp:ListItem Text="Switzerland" Value="Switzerland"></asp:ListItem>
                                                                            <asp:ListItem Text="Thailand " Value="Thailand "></asp:ListItem>
                                                                            <asp:ListItem Text="Turkey" Value="Turkey"></asp:ListItem>
                                                                            <asp:ListItem Text="Uganda" Value="Uganda"></asp:ListItem>
                                                                            <asp:ListItem Text="United Kingdom" Value="United Kingdom"></asp:ListItem>
                                                                            <asp:ListItem Text="USA" Value="USA"></asp:ListItem>
                                                                            <asp:ListItem Text="Zambia" Value="Zambia"></asp:ListItem>
                                                                            <asp:ListItem Text="Zimbabwe" Value="Zimbabwe"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>

                                                                <div class="popbuttons" style="">
                                                                    <asp:Button ID="btnNewAddDetails" class="btn btn-primary" runat="server" Text="Submit"
                                                                        OnClientClick="return ValidationForCityAndState()" OnClick="btnNewAddDetails_Click" />
                                                                    <asp:Button ID="btnAddReset" runat="server" Text="Reset" class="btn" OnClick="btnAddReset_Click" />
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <!-- /.panel -->
                                            <%--</div>--%>
                                        </asp:Panel>
                                    </div>
                                    <div class="col-md-12">
                                        <asp:Panel ID="PanelJob" runat="server" CssClass="modalPopup" align="center" Style="display: none;">
                                            <div class="modal-header" runat="server" id="Popupheader1">
                                                <h3>Job Title<a class="close-modal" href="#" id="btnCloseJob">&times;</a></h3>
                                            </div>
                                            <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                                <ContentTemplate>
                                                    <div class="modal-body">
                                                        <div class="panel-body">
                                                            <div class="col-md-12" style="">
                                                                <div class="form-group col-md-6">
                                                                    <label for="textfield" class="control-label col-md-5  lCYearID" style="">
                                                                        Job ID
                                                                    </label>
                                                                    <div class="col-sm-4 TCYearID" style="margin-left: 0px;">
                                                                        <asp:TextBox ID="txtJobID" runat="server" class="form-control" placeholder="Job ID"
                                                                            TextMode="SingleLine" MaxLength="10" AutoPostBack="true"
                                                                            OnTextChanged="txtJobID_TextChanged"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label for="textfield" class="control-label col-md-5 lCYear" style="">
                                                                        Job Name <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                                    <div class="col-sm-4 TCYear" style="margin-left: 0px;">
                                                                        <asp:TextBox ID="txtJobTitleName" runat="server" class="form-control" placeholder="Job Title Name"
                                                                            TextMode="SingleLine" MaxLength="25" AutoPostBack="true"
                                                                            OnTextChanged="txtJobTitleName_TextChanged"></asp:TextBox>
                                                                    </div>
                                                                </div>

                                                                <div class="popbuttons" style="margin-top: 1%">
                                                                    <asp:Button ID="btnJobTitleSave" class="btn btn-primary" runat="server" Text="Submit" />
                                                                    <asp:Button ID="btnJobTitleReset" runat="server" Text="Reset" class="btn" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /.panel -->
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </asp:Panel>

                                    </div>

                                    <div class="col-md-12">
                                        <asp:Panel ID="PanelGrade" runat="server" CssClass="modalPopup" align="center" Style="display: none;">
                                            <div class="modal-header" runat="server" id="Popupheader2">
                                                <h3>Employee Group<a class="close-modal" href="#" id="btnCloseGrade">&times;</a></h3>
                                            </div>
                                            <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                                <ContentTemplate>
                                                    <div class="modal-body">
                                                        <div class="panel-body">
                                                            <div class="col-md-12" style="">

                                                                <div class="col-md-12" style="">
                                                                    <div class="form-group col-md-6">
                                                                        <label for="textfield" class="control-label col-md-5 lAID" style="width: 15%">
                                                                            ID
                                                                        </label>
                                                                        <div class="col-md-7 tAID" style="">
                                                                            <asp:TextBox ID="txtID" runat="server" placeholder="ID" MaxLength="50"
                                                                                class="form-control"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label for="textfield" class="control-label col-md-5 lAHeadName" style="width: 16%;">
                                                                            Group Name<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                                        <div class="col-md-7 tAHeadName" style="" id="Div88">
                                                                            <asp:TextBox ID="txtHeadName" runat="server" placeholder="Group Name" class="form-control"
                                                                                MaxLength="50" AutoPostBack="true" OnTextChanged="txtHeadName_TextChanged"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label for="textfield" class="control-label col-md-5 lAShortName" style="width: 16%;">
                                                                            Short Name<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                                        <div class="col-md-7 tAShortName" style="" id="Div89">
                                                                            <asp:TextBox ID="txtShortName" runat="server" placeholder="Short Name" class="form-control"
                                                                                MaxLength="50" AutoPostBack="true" OnTextChanged="txtShortName_TextChanged"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group col-md-6">
                                                                        <label for="textfield" class="control-label col-md-5 lACalculation" style="width: 16%;">
                                                                            Group Type<span style="color: #ff0000; font-size: 14px; display: inline;">*</span>
                                                                        </label>
                                                                        <div class="col-md-7 tACalculation" style="">
                                                                            <%--  onchange="ddlCalculationValidation()"--%>
                                                                            <asp:DropDownList ID="ddlGroup" runat="server" class="form-control DropdownCss">
                                                                                <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                                <asp:ListItem Text="Permanent" Value="Permanent"></asp:ListItem>
                                                                                <asp:ListItem Text="Contractor virtual" Value="Contractor virtual"></asp:ListItem>
                                                                                <asp:ListItem Text="Consulate Dated" Value="Consulate Dated"></asp:ListItem>
                                                                                <asp:ListItem Text="Daily Wise" Value="Daily Wise"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div class="col-md-12" style="">
                                                                    <div class="form-group ">
                                                                        <label for="textfield" class="control-label col-md-5 lblAUCompanyID" style="width: 18%; margin-left: -5%;">
                                                                            Applicable Heads
                                                                        </label>
                                                                        <div class="col-md-7 tAUCompanyID " style="">
                                                                            <asp:CheckBoxList ID="chlHeadsLists" runat="server" class="ListControl" RepeatDirection="Horizontal"
                                                                                RepeatLayout="Table" RepeatColumns="3" OnSelectedIndexChanged="chlHeadsLists_SelectedIndexChanged"
                                                                                AutoPostBack="true">
                                                                            </asp:CheckBoxList>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div class="col-md-12" style="">
                                                                    <div class="popbuttons" style="">
                                                                        <asp:Button ID="btnGroupSave" class="btn btn-primary" runat="server" Text="Submit" OnClientClick="return ValidationForOfficialTab()" OnClick="btnGroupSave_Click" />
                                                                        <asp:Button ID="btnGroupReset" runat="server" Text="Reset" class="btn" OnClick="btnGroupRest_Click" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- /.panel -->
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </asp:Panel>

                                    </div>
                                    <div class="col-md-12">
                                        <asp:Panel ID="RecruitPanel2" runat="server" CssClass="modalPopup" align="center" Style="display: none;">
                                            <div class="modal-header" runat="server" id="Div90">
                                                <h3>Recruit<a class="close-modal" href="#" id="btnClosRecruit">&times;</a></h3>
                                            </div>
                                            <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                                <ContentTemplate>
                                                    <div class="modal-body">
                                                        <div class="panel-body">
                                                            <div class="col-md-12" style="">

                                                                <div class="col-md-12" style="">
                                                                    <div class="form-group col-md-6">
                                                                        <label for="textfield" class="control-label col-md-5  lCRecruitID" style="">
                                                                            Recruit No
                                                                        </label>
                                                                        <div class="col-sm-4 TCYearID" style="margin-left: 0px;">
                                                                            <asp:TextBox ID="txtRecruitNo" runat="server" class="form-control" placeholder="Recruit No"
                                                                                TextMode="SingleLine" MaxLength="10"
                                                                                AutoPostBack="true" OnTextChanged="txtRecruitNo_TextChanged"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label for="textfield" class="control-label col-md-5 lCRecruitName" style="">
                                                                            Recruit Name <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                                        <div class="col-sm-4 TCYear" style="margin-left: 0px;">
                                                                            <asp:TextBox ID="txtRecruitName" runat="server" class="form-control" placeholder="Recruit Name"
                                                                                TextMode="SingleLine" MaxLength="30"
                                                                                AutoPostBack="true" OnTextChanged="txtRecruitName_TextChanged"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12" style="">
                                                                    <div class="popbuttons" style="">
                                                                        <asp:Button ID="btnRecruitSave" class="btn btn-primary" runat="server" Text="Submit" OnClick="btnRecruitSave_Click" />
                                                                        <asp:Button ID="btnRecruitRest" runat="server" Text="Reset" class="btn" OnClick="btnRecruitRest_Click" />

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- /.panel -->
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </asp:Panel>

                                    </div>

                                    <div class="col-md-12">
                                        <asp:Panel ID="PanelDept" runat="server" CssClass="modalPopup" align="center" Style="display: none;">
                                            <div class="modal-header" runat="server" id="Popupheader3">
                                                <h3>Department<a class="close-modal" href="#" id="btnCloseDept">&times;</a></h3>
                                            </div>
                                            <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                                                <ContentTemplate>
                                                    <div class="modal-body">
                                                        <div class="panel-body">
                                                            <div class="col-md-12" style="">
                                                                <div class="col-md-12" style="">
                                                                    <div class="form-group col-md-6">
                                                                        <label for="textfield" class="control-label col-md-5  lCDeptID" style="">
                                                                            Department ID
                                                                        </label>
                                                                        <div class="col-sm-4 TCYearID" style="margin-left: 0px;">
                                                                            <asp:TextBox ID="txtDeptID" runat="server" class="form-control" placeholder="Department ID"
                                                                                TextMode="SingleLine" MaxLength="10" AutoPostBack="true" OnTextChanged="txtDepTID_OnTextChanged"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label for="textfield" class="control-label col-md-5 lCDepartName" style="">
                                                                            Department Name <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                                        <div class="col-sm-4 TCYear" style="margin-left: 0px;">
                                                                            <asp:TextBox ID="txtDepartName" runat="server" class="form-control" placeholder="Department Name"
                                                                                TextMode="SingleLine" MaxLength="30"
                                                                                AutoPostBack="true" OnTextChanged="txtDepartName_OnTextChanged"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12" style="">
                                                                    <div class="popbuttons" style="">
                                                                        <asp:Button ID="btnDepartSave" class="btn btn-primary" runat="server" Text="Submit" OnClick="btnDepartSave_Click" />
                                                                        <asp:Button ID="btnDepartRest" runat="server" Text="Reset" class="btn" OnClick="btnDepartRest_Click" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /.panel -->
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </asp:Panel>

                                    </div>


                                    <div class="col-md-12">
                                        <asp:Panel ID="PanelLocation" runat="server" CssClass="modalPopup" align="center" Style="display: none;">
                                            <div class="modal-header" runat="server" id="Div91">
                                                <h3>Location<a class="close-modal" href="#" id="btnCloseLocation">&times;</a></h3>
                                            </div>
                                            <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                                                <ContentTemplate>
                                                    <div class="modal-body">
                                                        <div class="panel-body">
                                                            <div class="col-md-12" style="">
                                                                <div class="col-md-12" style="">
                                                                    <div class="form-group col-md-6">
                                                                        <label for="textfield" class="control-label col-md-5  lCDeptID" style="">
                                                                            Location Id
                                                                        </label>
                                                                        <div class="col-sm-4 TCYearID" style="margin-left: 0px;">
                                                                            <asp:TextBox ID="txtLocationId" runat="server" class="form-control" placeholder="Location ID"
                                                                                TextMode="SingleLine" MaxLength="10"></asp:TextBox><%--AutoPostBack="true" OnTextChanged="txtDepTID_OnTextChanged"--%>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label for="textfield" class="control-label col-md-5 lCDepartName" style="">
                                                                            Location Name <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                                        <div class="col-sm-4 TCYear" style="margin-left: 0px;">
                                                                            <asp:TextBox ID="txtLocationName" runat="server" class="form-control" placeholder="Location Name"
                                                                                TextMode="SingleLine" MaxLength="50"
                                                                                AutoPostBack="true" OnTextChanged="txtLocationName_OnTextChanged"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <%--   <div class="col-md-12" style="">
                                        <div class="popbuttons" style="">
                                            <asp:Button ID="btnLocationSave" class="btn btn-primary" runat="server" Text="Submit" OnClick="btnLocationSave_Click" />
                                            <asp:Button ID="btnLocationReset" runat="server" Text="Reset" class="btn" OnClick="btnLocationReset_Click" />
                                        </div>
                                    </div>--%>
                                                            </div>
                                                        </div>
                                                        <!-- /.panel -->
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </asp:Panel>

                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="col-md-12 text-center">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary" OnClick="btnSave_Click" /><%--OnClientClick="return fileUploadValidation()"--%>
                                        <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-primary" />
                                        <asp:Button ID="btnCancel" runat="server" Text="Reset" class="btn" />
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</asp:Content>

