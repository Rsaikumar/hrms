﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" 
    CodeBehind="AddEmployeGroup.aspx.cs" Inherits="HRMS.Employee.AddEmployeGroup" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/Employee/EmployeeGroup.css" rel="stylesheet" />
    <script type="text/javascript" lang="javascript">
        function ControlValid() {
            if (document.getElementById('<%=txtHeadName.ClientID%>').value == "") {
                alert("Please Enter Group Name");

                document.getElementById('<%=txtHeadName.ClientID%>').focus();

                return false;
            }
            if (document.getElementById('<%=txtShortName.ClientID%>').value == "") {
                alert("Please Enter Short Name");

                document.getElementById('<%=txtShortName.ClientID%>').focus();

                return false;
            }
            if (document.getElementById('<%=ddlGroup.ClientID%>').options[document.getElementById('<%=ddlGroup.ClientID%>').selectedIndex].value == "Select") {
                alert("Please Select Group Type..");
                document.getElementById('<%=ddlGroup.ClientID%>').focus();
                return false;
            }
            return true;
        }
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">Payroll</a><i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="EmployeeGroup.aspx">Employee Group List</a><i class="fa fa-angle-right"></i>
                    </li>
                    <li><a href="AddEmployeGroup.aspx">Employee Group</a> </li>
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-sm-12">

                            <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                                <div class="box-title">
                                    <h3>
                                        <i class="fa fa-th-list"></i>Employee Group</h3>
                                </div>
                                <div class="box-content">
                                    <form action="#" method="POST" class='form-horizontal form-bordered'>
                                        <div class="col-sm-12" style="">

                                            <div class="col-sm-12" style="">
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lAID" style="">
                                                        ID
                                                    </label>
                                                    <div class="col-sm-2 tAID" style="">
                                                        <asp:TextBox ID="txtID" runat="server" placeholder="ID" MaxLength="50"
                                                            class="form-control"  ReadOnly="true"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lAHeadName" style="">
                                                        Group Name<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                    <div class="col-sm-2 tAHeadName" style="" id="">
                                                        <asp:TextBox ID="txtHeadName" runat="server" placeholder="Group Name" class="form-control"
                                                            MaxLength="50" AutoPostBack="true" OnTextChanged="txtHeadName_TextChanged" ></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lAShortName" style="">
                                                        Short Name<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                    <div class="col-sm-2 tAShortName" style="" id="">
                                                        <asp:TextBox ID="txtShortName" runat="server" placeholder="Short Name" class="form-control"
                                                            MaxLength="50" AutoPostBack="true" OnTextChanged="txtShortName_TextChanged" ></asp:TextBox>
                                                    </div>
                                                </div>
                                               
                                            </div>
                                            <div class="Col-sm-12">
                                                 <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lACalculation" style="">
                                                        Group Type<span style="color: #ff0000; font-size: 14px;display:inline;">*</span>
                                                    </label>
                                                    <div class="col-sm-2 tACalculation" style="">
                                                      <%--  onchange="ddlCalculationValidation()"--%>
                                                        <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="true" class="form-control DropdownCss"  
                                                             >
                                                            <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                            <asp:ListItem Text="Permanent" Value="Permanent"></asp:ListItem>
                                                            <asp:ListItem Text="Contractor virtual" Value="Contractor virtual"></asp:ListItem>
                                                            <asp:ListItem Text="Consulate Dated" Value="Consulate Dated"></asp:ListItem>
                                                            <asp:ListItem Text="Daily Wise" Value="Daily Wise"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>

                                            </div>
                                             <div class="col-sm-12" style="">
                                             <div class="form-group ">
                                            <label for="textfield" class="control-label col-sm-1 lblAUCompanyID" style="">
                                           Applicable Heads
                                            </label>
                                            <div class="col-sm-8 tAUCompanyID " style="">
                                                <asp:CheckBoxList ID="chlHeadsLists" runat="server" class="ListControl" RepeatDirection="Horizontal"
                                                     RepeatLayout="Table" RepeatColumns="3" OnSelectedIndexChanged="chlHeadsLists_SelectedIndexChanged"
                                                        AutoPostBack="true" Width="100%" >                                                
                                                </asp:CheckBoxList>
                                            </div>

                                      </div>
                                       
                                           </div>
                                        </div>
                                       

                                        <div class="form-actions col-sm-offset-2 col-sm-10 ACButtons">
                                            <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary" OnClientClick="return ControlValid()"  OnClick="btnSave_Click" />
                                            <asp:Button ID="btnUpdate" runat="server" Text="Save" class="btn btn-primary" OnClick="btnUpdate_Click"  />
                                            <asp:Button ID="btnCancel" runat="server" Text="Reset" class="btn" OnClick="btnCancel_Click" />
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
