﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="EmployeeRegistrations.aspx.cs" Inherits="HRMS.Administrator.EmployeeFullDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/Employee/Employee.css" rel="stylesheet" />

    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.js"></script>
    <%--<script src="//code.jquery.com/jquery-1.11.2.min.js" type="text/javascript"></script>--%>

    <script type="text/javascript">
        function ShowImagePreview(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#<%=userImg.ClientID%>').prop('src', e.target.result)
                        .width(240)
                        .height(150);
                };
                reader.readAsDataURL(input.files[0]);
                }
            }
            $(document).ready(function () {
                $(".txtOnly").keypress(function (e) {
                    var key = e.keyCode;
                    if (key < 48 || (key > 57 && key < 65) || (key > 90 && key < 97) || key > 122 || (key >= 48 && key <= 57)) {
                        e.preventDefault();
                    }
                });
            });

            $(document).ready(function () {
                $(".txtno").keypress(function (e) {
                    var key = e.keyCode;
                    if (keyCode > 31 && (keyCode < 48 || keyCode > 57)) {
                        e.preventDefault();
                    }
                });
            });

    </script>

    <style type="text/css">
        .RadioButtonWidth label {
            margin-right: 30px;
        }
    </style>

    <style>
        .ajax__calendar .ajax__calendar_container {
            border: 1px solid #646464;
            background-color: #ffffff;
            color: #000000;
            z-index: 9999 !important;
            position: absolute !important;
        }

        .ajax__tab_default .ajax__tab_tab {
            text-align: left !important;
        }
    </style>
    <script src="../Assist/Custom/RegistrationPage.js"></script>

    <script>

        function ValidationForDropDowns() {

            var email = document.getElementById("<%=txtEmail.ClientID%>");
            var filter = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;
            if (!filter.test(email.value)) {
                alert('Please provide a valid email address');
                email.focus;
                return false;
            }
            return true;

            if (document.getElementById('<%=ddlusersubrole.ClientID%>').options[document.getElementById('<%=ddlusersubrole.ClientID%>').selectedIndex].value == "0") {
                alert("Please Select User Designation ");
                document.getElementById('<%=ddlusersubrole.ClientID%>').focus();
                return false;
            }

            if (document.getElementById('<%=ddlUSerRole.ClientID%>').options[document.getElementById('<%=ddlUSerRole.ClientID%>').selectedIndex].value == "0") {
                alert("Please Select Department ");
                document.getElementById('<%=ddlUSerRole.ClientID%>').focus();
                return false;
            }


            if (document.getElementById('<%=ddlMaritalStatus.ClientID%>').options[document.getElementById('<%=ddlMaritalStatus.ClientID%>').selectedIndex].value == "0") {
                alert("Please Select Martial Status.. ");
                document.getElementById('<%=ddlMaritalStatus.ClientID%>').focus();
                return false;
            }


            if (document.getElementById('<%=ddlNationality.ClientID%>').options[document.getElementById('<%=ddlNationality.ClientID%>').selectedIndex].value == "0") {
                alert("Please Select Nationality.. ");
                document.getElementById('<%=ddlNationality.ClientID%>').focus();
                return false;
            }

            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.js"></script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="#">Administrator</a><i class="fa fa-angle-right"></i> </li>
                        <li><a href="../Employee/EmployeeDetailsList.aspx" class="current">Employee Registration</a> </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading tab-bg-dark-navy-blue ">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a data-toggle="tab" href="#personal" aria-expanded="false"><i class="fa fa-user">Personal</i></a>
                                </li>
                            </ul>
                        </header>
                        <div class="panel-body">
                            <div class="tab-content">
                                <div id="personal" class="tab-pane active">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5 style="color: rgb(15, 51, 224); font-size: 16px;"><b>Personal Infomation</b></h5>
                                            <hr />
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <div class=" form-group col-md-12" id="iddiv" runat="server">
                                                    <label for="textfield" class="control-label col-sm-4" style="">
                                                        Emp Id<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                    <div class="col-sm-8" style="">
                                                        <asp:TextBox ID="txtEmp_Code" runat="server" placeholder="Emp Code" MaxLength="10"
                                                            class="form-control" ReadOnly="false" AutoPostBack="true" OnTextChanged="txtEmp_Code_TextChanged"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label for="textfield" class="control-label col-sm-4" style="">
                                                        First Name<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                    <div class="col-sm-8" style="" id="">
                                                        <asp:TextBox ID="txtFirstName" runat="server" placeholder="First Name" class="form-control txtOnly"
                                                            MaxLength="50" required></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label for="textfield" class="control-label col-sm-4" style="">
                                                        Middle Name</label>
                                                    <div class="col-sm-8" style="" id="Div1">
                                                        <asp:TextBox ID="txtMiddleName" runat="server" placeholder="Middle Name" class="form-control txtOnly"
                                                            MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label for="textfield" class="control-label col-sm-4" style="">
                                                        Last Name<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                    <div class="col-sm-8" style="" id="Div2">
                                                        <asp:TextBox ID="txtLastName" runat="server" placeholder="Last Name" class="form-control txtOnly"
                                                            MaxLength="50" required></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="SliderLeft" style="">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="">
                                                            <asp:Image ID="userImg" Height="130px" Width="200px" runat="server" ImageUrl="~/img/administrator-icon.png" />
                                                        </div>
                                                        <span class="btn btn-default btn-file"><span class="fileinput-new">Select Image </span>
                                                            <span class="fileinput-exists">Change</span>
                                                            <asp:FileUpload ID="fupUserImage" runat="server" Width="180px" onchange="ShowImagePreview(this);" />
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group col-md-6" style="margin-left: 1px">
                                                <label for="textfield" class="control-label col-sm-4" style="">
                                                    Email</label>
                                                <div class="col-sm-8" style="" id="Div36">
                                                    <asp:TextBox ID="txtEmail" runat="server" placeholder="Email" class="form-control"
                                                        MaxLength="20" required TextMode="SingleLine"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="textfield" class="control-label col-sm-4" style="">
                                                    Phone No.</label>
                                                <div class="col-sm-8" style="" id="Div35">
                                                    <asp:TextBox ID="txtMobile" required runat="server" placeholder="Mobile" class="form-control"
                                                        MaxLength="10" TextMode="SingleLine" onkeydown="return (!(event.keyCode>=65) && event.keyCode!=32);"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="col-md-6 form-group">
                                                <label for="textfield" class="control-label col-sm-4" style="">
                                                    Date of Birth</label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtDateOfBirth" runat="server" required placeholder="MM-dd-yyyy" class="form-control"
                                                        MaxLength="50"></asp:TextBox>
                                                    <ajaxToolKit:CalendarExtender ID="calendarDOB" PopupButtonID="imgPopup" runat="server"
                                                        TargetControlID="txtDateOfBirth" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                    </ajaxToolKit:CalendarExtender>
                                                </div>
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <label for="textfield" class="control-label col-sm-4">
                                                    Gender</label>
                                                <div class="col-sm-8">
                                                    <asp:RadioButtonList ID="rblGender" runat="server" CssClass="RadioButtonWidth" RepeatColumns="3" RepeatDirection="Horizontal">
                                                        <asp:ListItem Text="Female" Value="Female"></asp:ListItem>
                                                        <asp:ListItem Text="Male" Value="Male"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-6 form-group">
                                                <label for="textfield" class="control-label col-sm-4">
                                                    Marital Status</label>
                                                <div class="col-sm-8">
                                                    <asp:DropDownList ID="ddlMaritalStatus" runat="server" class="form-control ">
                                                        <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                        <asp:ListItem Text="Single" Value="Single"></asp:ListItem>
                                                        <asp:ListItem Text="Married" Value="Married"></asp:ListItem>
                                                        <asp:ListItem Text="Widow" Value="Widow"></asp:ListItem>
                                                        <asp:ListItem Text="Divorced" Value="Divorced"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <label for="textfield" class="control-label col-sm-4">
                                                    Blood Group</label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtBloodGroup" runat="server" required placeholder="Blood Group" class="form-control"
                                                        MaxLength="15"></asp:TextBox>
                                                </div>
                                            </div>


                                            <%--    <div class="col-md-6 form-group">
                                                <label for="textfield" class="control-label col-sm-4" style="">
                                                    Username<span style="color: #ff0000; font-size: 14px;">*</span>
                                                </label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtUsername" required runat="server" placeholder="Username" class="form-control"
                                                        MaxLength="50"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <label for="textfield" class="control-label col-sm-4" style="">
                                                    Password<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtPassword" runat="server" required placeholder="Password" class="form-control"
                                                        MaxLength="50" TextMode="Password"></asp:TextBox>
                                                </div>
                                            </div>--%>
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                <ContentTemplate>
                                                    <%--     <div class="col-md-6 form-group">
                                                        <label for="textfield" class="control-label col-sm-4">
                                                            Emp Status</label>
                                                        <div class="col-sm-8">
                                                            <asp:DropDownList ID="ddlEmpStatus" runat="server" class="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlEmpStatus_SelectedIndexChanged">
                                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                                <asp:ListItem Text="Active" Value="Active"></asp:ListItem>
                                                                <asp:ListItem Text="On Leave" Value="On Leave"></asp:ListItem>
                                                                <asp:ListItem Text="On Long Leave" Value="On Long Leave"></asp:ListItem>
                                                                <asp:ListItem Text="Suspended" Value="Suspended"></asp:ListItem>
                                                                <asp:ListItem Text="Transferred" Value="Transferred"></asp:ListItem>
                                                                <asp:ListItem Text="Resigned" Value="Resigned"></asp:ListItem>
                                                                <asp:ListItem Text="Retired" Value="Retired"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>--%>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <%--     <div class="col-md-6 form-group">
                                                <label for="textfield" class="control-label col-sm-4" style="">
                                                    Age
                                                </label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtAge" runat="server" required placeholder="Age" class="form-control" MaxLength="2" onkeydown="return (!(event.keyCode>=65) && event.keyCode!=32);"></asp:TextBox>
                                                </div>
                                            </div>--%>
                                            <div class="clearfix"></div>
                                            <%--       <div class="col-md-6 form-group">
                                                <label for="textfield" class="control-label col-sm-4" style="">
                                                    Religion</label>
                                                <div class="col-sm-8">
                                                    <asp:DropDownList ID="ddlReligion" runat="server" class="form-control DropdownCss">
                                                        <asp:ListItem Text="---Select---" Value="---Select---"></asp:ListItem>
                                                        <asp:ListItem Text="Hindu" Value="Hindu"></asp:ListItem>
                                                        <asp:ListItem Text="Muslim" Value="Muslim"></asp:ListItem>
                                                        <asp:ListItem Text="Christian" Value="Christian"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>--%>
                                            <div class="col-md-6 form-group">
                                                <label for="textfield" class="control-label col-sm-4" style="">
                                                    Nationality</label>
                                                <div class="col-sm-8">
                                                    <asp:DropDownList ID="ddlNationality" runat="server" class="form-control DropdownCss">
                                                        <asp:ListItem Text="---Select---" Value="Select"></asp:ListItem>
                                                        <asp:ListItem Text="Indian" Value="Indian"></asp:ListItem>
                                                        <asp:ListItem Text="OVERSEAS" Value="Overseas"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <%--               <div class="col-md-6 form-group">
                                                <label for="textfield" class="control-label col-sm-4" style="">
                                                    Languages Known</label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtLanguagesKnown" required runat="server" placeholder="Languages Known" MaxLength="15"
                                                        class="form-control"></asp:TextBox>
                                                </div>
                                            </div>--%>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-12">
                                                <h5 style="color: rgb(15, 51, 224); font-size: 16px;"><b>Address Infomation</b></h5>
                                                <hr />
                                            </div>
                                            <div class="col-md-12">
                                                <h5 style="color: rgb(15, 51, 224); font-size: 15px;"><b>Present Address</b></h5>
                                            </div>
                                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                <ContentTemplate>
                                                    <div class="col-md-12">
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-3" style="">
                                                                Address 1 <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                            <div class="col-sm-9" style="" id="Div20">
                                                                <asp:TextBox ID="txtAddress1" runat="server" required placeholder="Address 1" class="form-control"
                                                                    MaxLength="250" TextMode="SingleLine"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-3" style="">
                                                                Address 2</label>
                                                            <div class="col-sm-9" style="" id="Div21">
                                                                <asp:TextBox ID="txtAddress2" runat="server" required placeholder="Address 2" class="form-control"
                                                                    MaxLength="250" TextMode="SingleLine"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-3" style="">
                                                                City<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                            <div class="col-sm-9" style="" id="Div22">
                                                                <asp:TextBox ID="txtcity" runat="server" required placeholder="City" class="form-control"></asp:TextBox>

                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-3" style="">
                                                                State</label>
                                                            <div class="col-sm-9" style="" id="Div24">
                                                                <asp:TextBox ID="txtState" required runat="server" placeholder="State" class="form-control"
                                                                    MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-3" style="">
                                                                Postal Code<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                            <div class="col-sm-9" style="" id="Div25">
                                                                <asp:TextBox ID="txtPostalCode" required runat="server" placeholder="Postal Code" class="form-control"
                                                                    MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-3" style="">
                                                                Country</label>
                                                            <div class="col-sm-9" style="" id="Div26">
                                                                <asp:TextBox ID="txtCountry" required runat="server" placeholder="Country" class="form-control txtOnly"
                                                                    MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="col-md-12">
                                                            <h5 style="color: rgb(15, 51, 224); font-size: 15px;"><b>Permanent Address</b></h5>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <asp:CheckBox ID="chbPermentAddress" runat="server" OnCheckedChanged="chbPermentAddress_CheckedChanged"
                                                                Text="If Present Address same as Permanent Address" AutoPostBack="true" />
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-3" style="">
                                                                Address 1 <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                            <div class="col-sm-9" style="" id="Div28">
                                                                <asp:TextBox ID="txtPAddress1" runat="server" placeholder="Address 1" class="form-control"
                                                                    MaxLength="250" TextMode="SingleLine"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-3" style="">
                                                                Address 2</label>
                                                            <div class="col-sm-9" style="" id="Div29">
                                                                <asp:TextBox ID="txtPaddress2" runat="server" placeholder="Address 2" class="form-control"
                                                                    MaxLength="250" TextMode="SingleLine"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-3" style="">
                                                                City<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                            <div class="col-sm-9" style="" id="Div30">
                                                                <asp:TextBox ID="txtpcity" runat="server" placeholder="City" class="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-3" style="">
                                                                State</label>
                                                            <div class="col-sm-9" style="" id="Div31">
                                                                <asp:TextBox ID="txtPSate" runat="server" placeholder="State" class="form-control"
                                                                    MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-3" style="">
                                                                Postal Code</label>
                                                            <div class="col-sm-9" style="" id="Div32">
                                                                <asp:TextBox ID="txtPPalost" runat="server" placeholder="Postal Code" class="form-control"
                                                                    MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-3" style="">
                                                                Country</label>
                                                            <div class="col-sm-9" style="" id="Div33">
                                                                <asp:TextBox ID="txtpCountry" runat="server" placeholder="Country" class="form-control txtOnly"
                                                                    MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-5" style="" id="lbladdressprof" runat="server">
                                                                Address Proof :</label>
                                                            <div class="col-sm-7" style="" id="Div3">
                                                                <span class="btn btn-default btn-file" id="Span1" runat="server" style="width: 100%"><span class="fileinput-new">Upload </span><span
                                                                    class="fileinput-exists"></span>
                                                                    <%--<input type="file" id="File4" runat="server" name="..." />--%>
                                                                    <asp:FileUpload ID="FileUpload1" runat="server" />
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6" id="dividtelephone">
                                                            <label for="textfield" class="control-label col-sm-3" style="">
                                                                Telephone</label>
                                                            <div class="col-sm-9" style="" id="Div34">
                                                                <asp:TextBox ID="txtTelephone" onkeydown="return (!(event.keyCode>=65) && event.keyCode!=32);" runat="server" placeholder="Telephone" class="form-control txtno"
                                                                    MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-12">
                                                <h5 style="color: rgb(15, 51, 224); font-size: 16px;"><b>Official Infomation</b></h5>
                                                <hr />
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-4" style="">
                                                        Date of Join</label>
                                                    <div class="col-sm-8" style="" id="Div58">
                                                        <asp:TextBox ID="txtDateofJoin" runat="server" placeholder="Ex:MM-dd-yyyy" class="form-control"
                                                            MaxLength="25" required TextMode="SingleLine" OnTextChanged="txtDateofJoin_TextChanged" AutoPostBack="true" data-required-msg="Please Select the Join Date"></asp:TextBox>
                                                        <ajaxToolKit:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="imgPopup"
                                                            TargetControlID="txtDateofJoin" Format="dd-MM-yyyy" PopupPosition="BottomRight">
                                                        </ajaxToolKit:CalendarExtender>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="textfield" class="control-label col-sm-4">
                                                        Designation <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList ID="ddlusersubrole" runat="server" class="form-control DropdownCss" OnSelectedIndexChanged="ddlusersubrole_SelectedIndexChanged" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <asp:Label ID="lblcasualid" runat="server" class="control-label col-sm-4">
                                                <span style="color: #ff0000; font-size: 14px;"></span></asp:Label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtcasualleave" runat="server" placeholder="CasualLeave" class="form-control"
                                                            MaxLength="50" ReadOnly="true"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <asp:Label ID="lblearnedid" for="textfield" runat="server" class="control-label col-sm-4">
                                                   <span style="color: #ff0000; font-size: 14px;"></span></asp:Label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtearnedleave" runat="server" placeholder="EarnedLeave" class="form-control"
                                                            MaxLength="50" ReadOnly="true"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-4" style="">
                                                        Reporting<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                    <div class="col-sm-8" style="" id="Div66">
                                                        <asp:DropDownList ID="ddlReportingAuthority" runat="server" class="form-control DropdownCss">
                                                            <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="HOFF" Value="1"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="textfield" class="control-label col-sm-4">Department<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList ID="ddlUSerRole" runat="server" class="form-control DropdownCss">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <%--   <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        Bank Name<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                    <div class="col-sm-7" style="" id="Div48">
                                                        <asp:DropDownList ID="ddlPBankName" runat="server" class="form-control DropdownCss">
                                                            <asp:ListItem Text="---Select---" Value="---Select---"></asp:ListItem>
                                                            <asp:ListItem Text="SBI" Value="SBI"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>--%>
                                            </div>

                                            <div class="col-md-12">

                                                <%--   <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        Account Number</label>
                                                    <div class="col-sm-7" style="" id="Div49">
                                                        <asp:TextBox ID="txtPAccountNumber" onkeydown="return (!(event.keyCode>=65) && event.keyCode!=32);" runat="server" placeholder="Account Number" class="form-control"
                                                            MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        Health Insurance Policy No</label>
                                                    <div class="col-sm-7" style="" id="Div50">
                                                        <asp:TextBox ID="txtPHealth" runat="server" placeholder="Health / Mediclaim Insurance Policy No"
                                                            class="form-control" MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                    </div>
                                                </div>--%>
                                            </div>
                                            <div class="col-md-12">

                                                <%--   <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-5" style="">
                                                        Sum Insured</label>
                                                    <div class="col-sm-7" style="" id="Div51">
                                                        <asp:TextBox ID="txtSumInsured" runat="server" placeholder="Sum Insured" class="form-control"
                                                            MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                    </div>
                                                </div>--%>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-12">
                                                <h5 style="color: rgb(15, 51, 224); font-size: 16px;"><b>Salary</b></h5>
                                                <hr />
                                            </div>
                                        </div>
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                            <ContentTemplate>
                                                <div class="col-md-12">
                                                    <%--  <div class="col-md-12">
                                                        <h5 style="color: rgb(15, 51, 224); font-size: 15px;"><b>Earnings</b></h5>
                                                        <hr />
                                                    </div>--%>
                                                    <%--  <div class="form-group col-md-6">
                                                        <asp:CheckBox ID="chbbasicsalary" runat="server"
                                                            Text="If We Want to Change the Percentage" AutoPostBack="true" />
                                                    </div>--%>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            Gross Amount<span style="color: #ff0000; font-size: 14px; display: none;">*</span></label>
                                                        <div class="col-sm-7" style="" id="Div59">
                                                            <asp:TextBox ID="txtgrossamount" required runat="server" AutoPostBack="true" placeholder="Gross Amount" class="form-control" MaxLength="50" onkeydown="return (!(event.keyCode>=65) && event.keyCode!=32);" OnTextChanged="txtgrossamount_TextChanged"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-12">
                                                        <h5 style="color: rgb(15, 51, 224); font-size: 16px;"><b>Earnings And Deductions</b></h5>

                                                    </div>
                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                        <ContentTemplate>
                                                            <div class="col-md-6">

                                                                <div class="box-body table-responsive text-center ">
                                                                    <div class=" form-group ">
                                                                        <asp:GridView ID="gvdearnings" runat="server" AutoGenerateColumns="false">
                                                                            <FooterStyle BackColor="#ffffff" />
                                                                            <HeaderStyle CssClass="header" BackColor="#00677d" Font-Bold="True" ForeColor="#FFFFFF"></HeaderStyle>
                                                                            <PagerStyle CssClass="pager" BackColor="#ffffff" ForeColor="Black" HorizontalAlign="Right"></PagerStyle>
                                                                            <RowStyle CssClass="rows" BackColor="#ffffff"></RowStyle>
                                                                            <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True" />
                                                                            <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                                                            <SortedAscendingHeaderStyle BackColor="#848384" />
                                                                            <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                                                            <SortedDescendingHeaderStyle BackColor="#575357" />
                                                                            <AlternatingRowStyle BackColor="White" />
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="">
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="chbboxdesignation" runat="server" AutoPostBack="true" CssClass="" OnCheckedChanged="chbboxdesignation_CheckedChanged" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="Name" HeaderText="Name Of Earnings" ItemStyle-Width="150" />

                                                                                <asp:TemplateField HeaderText="Value">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="txtvalue" AutoPostBack="true" CssClass="form-control" runat="server"  Text='<%# Eval("Value") %>'>
                                                                                        </asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <%--  <asp:BoundField DataField="Value" HeaderText=" Percentage" ItemStyle-Width="150" />--%>

                                                                                <asp:TemplateField HeaderText="Earnings Amount">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="txtearningamount" AutoPostBack="true" CssClass="form-control" runat="server" ReadOnly="true">
                                                                                        </asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <div class="row">
                                                        <div class="col-md-6">

                                                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                                                <ContentTemplate>
                                                                    <div class="col-md-12">
                                                                        <div class="box-body table-responsive text-center ">
                                                                            <div class=" form-group ">
                                                                                <asp:GridView ID="gvddeductions" runat="server" AutoGenerateColumns="false">
                                                                                    <FooterStyle BackColor="#ffffff" />
                                                                                    <HeaderStyle CssClass="header" BackColor="#00677d" Font-Bold="True" ForeColor="#FFFFFF"></HeaderStyle>
                                                                                    <PagerStyle CssClass="pager" BackColor="#ffffff" ForeColor="Black" HorizontalAlign="Right"></PagerStyle>
                                                                                    <RowStyle CssClass="rows" BackColor="#ffffff"></RowStyle>
                                                                                    <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True" />
                                                                                    <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                                                                    <SortedAscendingHeaderStyle BackColor="#848384" />
                                                                                    <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                                                                    <SortedDescendingHeaderStyle BackColor="#575357" />
                                                                                    <AlternatingRowStyle BackColor="White" />
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="">
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="chboxdeductions" runat="server" AutoPostBack="true" CssClass="" OnCheckedChanged="chboxdeductions_CheckedChanged" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="Name" HeaderText="Name Of Deductions" ItemStyle-Width="150" />

                                                                                        <asp:TemplateField HeaderText="Value">
                                                                                            <ItemTemplate>
                                                                                                <asp:TextBox ID="txtdedvalue" AutoPostBack="true" CssClass="form-control" runat="server" ReadOnly="true" Text='<%# Eval("Value") %>'>
                                                                                                </asp:TextBox>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                     <%--   <asp:BoundField DataField="Value" HeaderText=" Percentage" ItemStyle-Width="150" />--%>
                                                                                        <asp:TemplateField HeaderText="Deduction Amount">
                                                                                            <ItemTemplate>
                                                                                                <asp:TextBox ID="txtdeductions" AutoPostBack="true" CssClass="form-control" runat="server" ReadOnly="true">
                                                                                                </asp:TextBox>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>

                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-4" style="">
                                                                Basic Amount</label>
                                                            <div class="col-sm-8">
                                                                <asp:TextBox ID="txtbasicamount" runat="server" ReadOnly="true" placeholder="Basic Amount" class="form-control"
                                                                    MaxLength="20"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-md-5" style="">
                                                                Net Amount</label>
                                                            <div class="col-md-7" style="" id="Div77">
                                                                <asp:TextBox ID="txtnetamount" runat="server" ReadOnly="true" placeholder="Net Amount" class="form-control"
                                                                    MaxLength="20"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <%-- <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            DA%</label>
                                                        <div class="col-sm-7">
                                                            <asp:TextBox ID="txtdaper" runat="server" ReadOnly="true" placeholder="DA Percentage" class="form-control"
                                                                MaxLength="20"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            DA Amount<span style="color: #ff0000; font-size: 14px; display: none;">*</span></label>
                                                        <div class="col-sm-7" style="" id="Div62">
                                                            <asp:TextBox ID="txtda" runat="server" AutoPostBack="true"
                                                                placeholder="DA Amount" ReadOnly="true" class="form-control"
                                                                MaxLength="50"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            HRA %</label>
                                                        <div class="col-sm-7">
                                                            <asp:TextBox ID="txthraper" runat="server" ReadOnly="true" placeholder="HRA %" class="form-control"
                                                                MaxLength="20"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            HRA</label>
                                                        <div class="col-sm-7" style="" id="Div64">
                                                            <asp:TextBox ID="txthra" runat="server" ReadOnly="true" placeholder="HRA Amount" class="form-control"
                                                                MaxLength="20"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            CCA %</label>
                                                        <div class="col-sm-7">
                                                            <asp:TextBox ID="txtccaper" runat="server" ReadOnly="true" placeholder="CCA Percentage" class="form-control"
                                                                MaxLength="20"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            CCA</label>
                                                        <div class="col-sm-7" style="" id="Div67">
                                                            <asp:TextBox ID="txtcca" runat="server" ReadOnly="true" placeholder="CCA" class="form-control"
                                                                MaxLength="20"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            Basic Amount</label>
                                                        <div class="col-sm-7">
                                                            <asp:TextBox ID="txtbasicamount" runat="server" ReadOnly="true" placeholder="Basic Amount" class="form-control"
                                                                MaxLength="20"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <h5 style="color: rgb(15, 51, 224); font-size: 15px;"><b>Deductions</b></h5>
                                                        <hr />
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            PF %</label>
                                                        <div class="col-sm-7" style="" id="Div71">
                                                            <asp:TextBox ID="txtPFper" runat="server" ReadOnly="true" placeholder="PF %" class="form-control"
                                                                MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            PF Amount</label>
                                                        <div class="col-sm-7" style="" id="Div68">
                                                            <asp:TextBox ID="txtpfamount" runat="server" ReadOnly="true" placeholder="PF Amount" class="form-control"
                                                                MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            ESI %</label>
                                                        <div class="col-sm-7" style="" id="Div73">
                                                            <asp:TextBox ID="txtESIper" runat="server" ReadOnly="true" placeholder="ESI Percentage" class="form-control"
                                                                MaxLength="25" TextMode="SingleLine" AutoPostBack="true"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            ESI Amount</label>
                                                        <div class="col-sm-7">
                                                            <asp:TextBox ID="txtesiamount" runat="server" ReadOnly="true" placeholder="ESI Amount" class="form-control"
                                                                MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-5" style="">
                                                            TDS %</label>
                                                        <div class="col-sm-7">
                                                            <asp:TextBox ID="txttdsper" runat="server" ReadOnly="true" OnTextChanged="txttdsper_TextChanged" placeholder="TDS Percentage" class="form-control"
                                                                MaxLength="25" TextMode="SingleLine"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-md-5" style="">
                                                            TDS Amount</label>
                                                        <div class="col-md-7" style="" id="Div80">
                                                            <asp:TextBox ID="txttds" runat="server" ReadOnly="true" placeholder="TDS Amount" class="form-control"
                                                                MaxLength="20"></asp:TextBox>
                                                        </div>
                                                    </div>--%>

                                                    <div class="col-md-12 text-center">
                                                        <asp:Button ID="btnsave" runat="server" Text="Submit" CssClass="btn btn-primary" OnClientClick="return ValidationForDropDowns() " OnClick="btnsave_Click1" />
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
