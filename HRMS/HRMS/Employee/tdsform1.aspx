﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="tdsform1.aspx.cs" Inherits="HRMS.Employee.tdsform1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        
    <div class="container">
      <div class="col-md-6"> 
          <asp:Panel ID="Panel1" runat="server">
          <h3>Creating Deductor</h3>
        
        <table>
            <tr>
                <td class="auto-style1">Deducter Name :</td>
                    <td> <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="auto-style1">
                    Status of Deductor :</td>
                    <td><asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                        <asp:ListItem>Government</asp:ListItem>
                        <asp:ListItem>Others</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>Deductor Type :</td> <td><asp:DropDownList ID="DropDownList1" runat="server">
                    <asp:ListItem>company-1</asp:ListItem>
                    <asp:ListItem>company-2</asp:ListItem>
                    <asp:ListItem>company-3</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr><td colspan="2">Applicability of Gratuity Act, 1972? <asp:CheckBox ID="CheckBox1" runat="server" /></td></tr>
           <tr>
               <td>Date From which applicable..</td>
           </tr>
            <tr>
                <td colspan="2">
                    PAN:<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox><asp:CheckBox ID="CheckBox2" runat="server" /> APPLIED
                </td>
            </tr>
            <tr>
                <td>Tax Deduction No. </td> <td><asp:TextBox ID="TextBox3" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>TDS Cricle :</td> <td> <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Assessing Officer : </td> <td><asp:TextBox ID="TextBox5" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Tax Collection No. : </td> <td><asp:TextBox ID="TextBox6" runat="server"></asp:TextBox></td>
            </tr>
          </table>
        
        </asp:Panel> 
            </div>
        <asp:Panel ID="Panel2" runat="server">
        <div class="col-md-6">
        <h4>  CIT (TDS)</h4>
            <table>
              
                <tr>
                    <td>Address 1 :</td> <td> <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox></td>
                </tr>
            <tr>
                <td>Address 2 : </td> <td><asp:TextBox ID="TextBox8" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Address 3 :</td> <td> <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>City : </td> <td><asp:TextBox ID="TextBox10" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    Pincode : </td> <td><asp:TextBox ID="TextBox11" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Flat/Door/Block.no : </td> <td><asp:TextBox ID="TextBox12" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    Premise/Bldg./Village :</td> <td> <asp:TextBox ID="TextBox13" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Road/Street/Lane/P.O :</td> <td> <asp:TextBox ID="TextBox14" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Town/City/District :</td> <td> <asp:TextBox ID="TextBox15" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    State/U.T. :</td> <td> <asp:TextBox ID="TextBox16" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Country :</td> <td> <asp:TextBox ID="TextBox17" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Pincode :</td> <td> <asp:TextBox ID="TextBox18" runat="server"></asp:TextBox></td>
            </tr>
            </table>
            
        <h4>Deducter Level Security :</h4>
        <table>
                        
            <tr>
                <td>Password :</td> <td> <asp:TextBox ID="TextBox19" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>ConFirm Password :</td> <td> <asp:TextBox ID="TextBox20" runat="server"></asp:TextBox></td>
            </tr>
            </table>
        <h4>Module Level Security</h4>
        <table>
            <tr>
                <td>Salary :</td>
                <td>Other than salary :</td>
            </tr>
            <tr>
                <td>Password <asp:TextBox ID="TextBox21" runat="server"></asp:TextBox></td>
                <td>password <asp:TextBox ID="TextBox22" runat="server"></asp:TextBox></td>
            
            </tr>
            <tr>
                <td>Confirm&nbsp;&nbsp; <asp:TextBox ID="TextBox23" runat="server"></asp:TextBox></td>
                <td>Confirm&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBox24" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="Button1" runat="server" Text="Accept" /></td>
                <td>
                    <asp:Button ID="Button2" runat="server" Text="Cancel" /></td>
            </tr>
        </table>
             </div>
        </asp:Panel>
       
    </div>
    </div>
</asp:Content>
