﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Employee
{
    public partial class tdsreports : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack == true)
            {
                Panel2.Visible = false;
                Panel3.Visible = false;
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownList1.SelectedItem.Text == "Rent")
            {
                Panel3.Visible = true;
                Panel2.Visible = false;
            }
            else if (DropDownList1.SelectedItem.Text == "Salary")
            {
                Panel3.Visible = false;
                Panel2.Visible = true;

            }

        }
    }
}