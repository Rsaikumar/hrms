﻿using BAL;
using DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Employee
{
    public partial class EmployeeList : System.Web.UI.Page
    {
        // // // Object Creation For Database
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();
        StringBuilder strScript = new StringBuilder();

        SqlConnection objSqlConnection;
        SqlCommand objSqlCommand;
        SqlDataAdapter da;


        // // /// Object Creation For Item Group Domain
        AdditionsMasterBAL objSCM_HeaderBAL = new AdditionsMasterBAL();
        EmployeeBAL objempBAL = new EmployeeBAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_Type"].ToString() == "CCF")
            {
                Response.Redirect("~/Employee/EmployeeDetails.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    if (Session["ClientRegistrationNo"] == null)
                    {
                        Response.Redirect("~/Default.aspx");
                    }
                    txtSearch.Focus();

                    //Bind User Registration
                    Bind_Grid();

                    // empCountDetails();

                    //display chart
                    Bind_PieChart_Employee((string)Session["ClientRegistrationNo"]);
                }
            }
        }
        // counting employee details
        public void empCountDetails()
        {
            string Cmpy = (string)Session["ClientRegistrationNo"];
          var counts=  objDAL.Sp_HRMS_Employee_Count_Details(Cmpy).ToList();

            if(counts.Count()>0)
            {
                lblEmpCount.Text = counts[0].Total_EMP.ToString();
                lblMaleCount.Text= counts[0].MALE_Count.ToString();
                lblFemaleCount.Text= counts[0].FEMALE_Count.ToString();
                decimal Total = Convert.ToDecimal(counts[0].Total_EMP.ToString());
                decimal male = Convert.ToDecimal(counts[0].MALE_Count.ToString());
                decimal female = Convert.ToDecimal(counts[0].FEMALE_Count.ToString());
                decimal MaleRatio = decimal.Multiply( decimal.Divide(male, Total),100);
                decimal FemaleRatio = decimal.Multiply(decimal.Divide(female, Total),100);
                lblRationMF.Text= MaleRatio.ToString("0.00")+":"+ string.Format("{0:0.00}", FemaleRatio); //FemaleRatio.ToString();
                lblJoin.Text = counts[0].New_Count.ToString();
                lblResigned.Text= counts[0].Resigned.ToString("0.00");

                decimal rate =Convert.ToDecimal(counts[0].Resigned) / Convert.ToDecimal(counts[0].Total_EMP);
                lblRate.Text = rate.ToString("0.00");

                

            }
        }

        #region Button Events

        // // // Navigation to User Registration Form For Created New User 
        protected void btnAdd_Click(object sender, EventArgs e)
        {

            Response.Redirect("~/Employee/EmployeeDetails.aspx");

        }

        /// <summary>
        /// Search For UserRegistration Details
        /// </summar
        /// 

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {

                string StrSearch = txtSearch.Text.Trim().ToLower();
                string CompanyName = Session["ClientRegistrationNo"].ToString().Trim().ToLower();
                //var objHeaderSearch = objempBAL.GetData_search_GroupInfo(StrSearch, CompanyName);
                string query = "%" + StrSearch + "%";
                var objHeaderSearch = objDAL.sp_HRMS_Employee_Personal_Official_Search(query, CompanyName).ToList();
               // return qry;
                if (objHeaderSearch != null)
                {
                    grdEmpList.DataSource = objHeaderSearch;
                    grdEmpList.DataBind();
                }
                else
                {
                    SetInitialRow();
                }


            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.ToString() + "');", true);
            }
        }

        /// <summary>
        /// Navigation for User Header create
        /// </summar
        /// 
        protected void lkbAddEmployee_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Employee/EmployeeDetails.aspx");
        }




        #endregion

        #region Methods

        String StrDate = "";
        // // Show the date format dd-mm-yy in sale Order Grids
        public string GetDate(object Dt)
        {

            StrDate = string.Format("{0:dd-MM-yyyy}", Dt);
            return StrDate;
        }
        /// <summary>
        /// Bind Grid Based on User Registration Details
        /// Created By Divya
        /// </summary>
        /// <param name="sender"></param>
        public void Bind_Grid()
        {
            try
            {
               
                string Cration_Company = (string)Session["ClientRegistrationNo"]; //"000001";
                
                var qry = objDAL.sp_HRMS_Employee_Personal_Official_GridData(Cration_Company);
              
                var distinctList = qry.GroupBy(x => x.Emp_Code)
                         .Select(g => g.First()).OrderByDescending(x => x.Emp_Code)
                         .ToList();
                
                if (qry!= null)
                {
                    grdEmpList.DataSource = distinctList;
                    grdEmpList.DataBind();
                }
                else
                {
                    // // // // Grid view using setting initial rows displaying Empty Header Displying
                    SetInitialRow();
                }

            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(\"" + Error + "\");", true);

            }
            finally
            {
            }
        }


        /// <summary>
        /// Setting Initial Empty Row Data Into Header Details 
        /// </summary>
        /// <param name="sender"></param>
        public void SetInitialRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("Emp_Code", typeof(string)));
            dt.Columns.Add(new DataColumn("Name", typeof(string)));
            dt.Columns.Add(new DataColumn("Department", typeof(string)));
            dt.Columns.Add(new DataColumn("Employee_Grade", typeof(string)));
            dt.Columns.Add(new DataColumn("Designation", typeof(string)));
            dt.Columns.Add(new DataColumn("Gender", typeof(string)));
            dt.Columns.Add(new DataColumn("Emp_Status", typeof(string)));



            dr = dt.NewRow();

            dr["Emp_Code"] = string.Empty;
            dr["Name"] = string.Empty;
            dr["Department"] = string.Empty;
            dr["Employee_Grade"] = string.Empty;
            dr["Designation"] = string.Empty;
            dr["Gender"] = string.Empty;
            dr["Emp_Status"] = string.Empty;


            dt.Rows.Add(dr);

            grdEmpList.DataSource = dt;
            grdEmpList.DataBind();

            int columncount = 7;
            grdEmpList.Rows[0].Cells.Clear();
            grdEmpList.Rows[0].Cells.Add(new TableCell());
            grdEmpList.Rows[0].Cells[0].ColumnSpan = columncount;
            grdEmpList.Rows[0].Cells[0].Text = "No Records Found";
            //  grdStorageLocation.HeaderStyle.HorizontalAlign = "center";
            grdEmpList.Rows[0].Cells[0].ForeColor = System.Drawing.Color.Red;
        }

        /// <summary>
        ///  The Following Method is used for Delete Grid records to Data From User Registration  Table
        ///  Created By Divya
        /// </summary>
        public void Delete_HeaderDetails(string AdditionCode, string Creation_Company)
        {
            try
            {
                //HRMS_Additions_Master a = objSCM_HeaderBAL.Delete_Addition_Header(AdditionCode, Session["ClientRegistrationNo"].ToString());
                HRMS_Employee_Master_PersonalDetail SUM = (from ST in objDAL.HRMS_Employee_Master_PersonalDetails
                                             where ST.Creation_Company == Creation_Company && ST.Emp_Code == AdditionCode
                                             select ST).ToList().FirstOrDefault();
                objDAL.HRMS_Employee_Master_PersonalDetails.DeleteOnSubmit(SUM);
                objDAL.SubmitChanges();

                if (SUM.Emp_Code!=null)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Header Details Deleted Successfully ');", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Header Details Already Existing In Header Page ');", true);
                }
                Bind_Grid();
                txtSearch.Focus();

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.ToString() + "');", true);
            }
            finally
            {
            }
        }


        /// <summary>
        /// Sale Details Binding Date base Pie chart 
        /// </summary>
        /// <param name="Parameter"></param>
        /// <param name="Branch_Id"></param>
        /// <param name="CreationCompany"></param>
        public void Bind_PieChart_Employee(string CreationCompany)
        {
            try
            {
                string objConfig = ConfigurationManager.ConnectionStrings["Dbconnection"].ToString();

                // SqlConnection objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Dbconnection"].ConnectionString);
                objSqlConnection = new SqlConnection(objConfig);
                objSqlCommand = new SqlCommand();
                objSqlCommand.Connection = objSqlConnection;

                objSqlConnection.Open();
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.CommandText = "Sp_HRMS_Employee_Count_Details";
              //  objSqlCommand.Parameters.AddWithValue("@Parameter", Parameter);
              //  objSqlCommand.Parameters.AddWithValue("@Branch_Id", (string)Session["BranchID"]);
                objSqlCommand.Parameters.AddWithValue("@Creation_Company", (string)Session["ClientRegistrationNo"]);

                
                da = new SqlDataAdapter(objSqlCommand);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    // strScript.Append(@"<script type='text/javascript'>  
                    // google.load('visualization', '1', {packages: ['corechart']}); </script>  

                    // <script type='text/javascript'>  

                    // function drawChart() {         
                    // var data = google.visualization.arrayToDataTable([  
                    // ['Task', 'Hours of Day'],");

                    lblEmpCount.Text = dt.Rows[0]["Total_EMP"].ToString();
                    lblMaleCount.Text = dt.Rows[0]["MALE_Count"].ToString();
                    lblFemaleCount.Text = dt.Rows[0]["FEMALE_Count"].ToString();
                   

                   // lblRationMF.Text = MaleRatio.ToString("0.00") + ":" + string.Format("{0:0.00}", FemaleRatio); //FemaleRatio.ToString();
                    lblJoin.Text = dt.Rows[0]["New_Count"].ToString();
                    lblResigned.Text = Convert.ToDecimal(dt.Rows[0]["Resigned"]).ToString("0.00");

                    decimal rate = Convert.ToDecimal(dt.Rows[0]["Resigned"]) / Convert.ToDecimal(dt.Rows[0]["Total_EMP"]);
                    lblRate.Text = rate.ToString("0.00");

                    decimal Total = Convert.ToDecimal(dt.Rows[0]["Total_EMP"].ToString());
                    decimal male = Convert.ToDecimal(dt.Rows[0]["MALE_Count"].ToString());
                    decimal female = Convert.ToDecimal(dt.Rows[0]["FEMALE_Count"].ToString());
                    decimal MaleRatio = decimal.Multiply(decimal.Divide(male, Total), 100);
                    decimal FemaleRatio = decimal.Multiply(decimal.Divide(female, Total), 100);
                    // strScript.Append("['" + dt.Rows[0]["MALE_Count"].ToString() + "'," + dt.Rows[0]["FEMALE_Count"].ToString() + "],");

                    // //  foreach (DataRow row in dt.Rows)
                    // // {
                    // //decimal Total = Convert.ToDecimal(row["Total_EMP"].ToString());
                    // //decimal male = Convert.ToDecimal(row["MALE_Count"].ToString());
                    // //decimal female = Convert.ToDecimal(row["FEMALE_Count"].ToString());
                    // //decimal MaleRatio = decimal.Multiply(decimal.Divide(male, Total), 100);
                    // //decimal FemaleRatio = decimal.Multiply(decimal.Divide(female, Total), 100);
                    // //strScript.Append("['" + row["MALE_Count"].ToString() + "'," + row["FEMALE_Count"].ToString() + "],");
                    // //// strScript.Append("['" + (Convert.ToDecimal(row["MALE_Count"].ToString())/Convert.ToDecimal(row["Total_EMP"].ToString()))*100 + "'," + (Convert.ToDecimal(row["MALE_Count"].ToString()) / Convert.ToDecimal(row["Total_EMP"].ToString())) * 100 + "],");
                    // //  strScript.Append("['" + MaleRatio.ToString("0.00") + "'," + FemaleRatio.ToString("0.00") + "],");
                    // //  }
                    // strScript.Remove(strScript.Length - 1, 1);
                    // strScript.Append("]);");

                    // strScript.Append(@" var options = { backgroundColor: 'transparent',
                    //'width':500,
                    //'height':300,    
                    //                 title: 'Employee Ratio',            
                    //                 is3D: true,          
                    //                 };   ");

                    // strScript.Append(@"var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));          
                    //             chart.draw(data, options);        
                    //             }    
                    //         google.setOnLoadCallback(drawChart);  
                    //         ");
                    // // strScript.Append(@"Total Amount='" + dt.Rows[0]["Total Amount"].ToString() + "'");
                    // strScript.Append(" </script>");

                    // ltScripts.Text = strScript.ToString();

                    strScript.Append(@"<script type='text/javascript'> google.load( 'visualization', '1', {packages:['corechart']});

            google.setOnLoadCallback(drawChart1);

            function drawChart1() {
            var data = new google.visualization.DataTable();

            data.addColumn('string', 'Total Employee');
            data.addColumn('number', 'Male');
            data.addColumn('number', 'Female');
 
            data.addRows(" + dt.Rows.Count + ");");

                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        //strScript.Append("data.setValue( " + i + "," + 0 + "," + "'" + dt.Rows[i]["Total_EMP"].ToString() + "');");
                        strScript.Append("data.setValue( " + i + "," + 1 + "," + "'" + MaleRatio.ToString("0.00") + "');");
                        strScript.Append("data.setValue(" + i + "," + 2 + "," + FemaleRatio.ToString("0.00") + ") ;");
                        
                    }

                    strScript.Append(" var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));");
                    strScript.Append(" chart.draw(data,{isStacked:true, width:500, height:350, hAxis: {showTextEvery:1, slantedText:true}});}");
                    strScript.Append("</script>");
                    ltScripts.Text = strScript.ToString();
                    txtTotalSale.Text = "Total Employees=" + dt.Rows[0]["Total_EMP"].ToString();
                    
                }
                else
                {

                    strScript.Append(@"<script type='text/javascript'>  
                    google.load('visualization', '1', {packages: ['corechart']}); </script>  
                      
                    <script type='text/javascript'>  
                     
                    function drawChart() {         
                    var data = google.visualization.arrayToDataTable([  
                    ['Task', 'Hours of Day'],");
                    int i = 10000;

                    strScript.Append("['" + (string)Session["ClientRegistrationNo"] + "'," + i + "],");

                    strScript.Remove(strScript.Length - 1, 1);
                    strScript.Append("]);");

                    strScript.Append(@" var options = {     
                                    title: 'Employee Ratio',            
                                    is3D: true,          
                                    };   ");

                    strScript.Append(@"var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));          
                                chart.draw(data, options);        
                                }    
                            google.setOnLoadCallback(drawChart);  
                            ");
                    strScript.Append(" </script>");

                    ltScripts.Text = strScript.ToString();
                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(\"" + Error + "\");", true);

            }
            finally
            {

            }
        }
        #endregion

        #region User Registration Details Bind For Grid view


        /// <summary>
        /// Row Command Grid view  For UserRegistration Details
        /// Created By Divya
        /// </summar
        /// 

        protected void grdHeaderList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string AdditionCode = e.CommandArgument.ToString();
            if (e.CommandName == "Edit")
            {

                string path = "~/Employee/EmployeeDetails.aspx";
                Session["EmployeeDetailsCode"] = AdditionCode;
                Response.Redirect(path);

            }
            if (e.CommandName == "Delete")
            {

                string Creation_Company = Session["ClientRegistrationNo"].ToString();

             Delete_HeaderDetails(AdditionCode, Creation_Company);

            }


        }


        /// <summary>
        /// Paging For User Registration Details
        /// </summar
        /// 
        protected void grdHeaderList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdEmpList.PageIndex = e.NewPageIndex;
            Bind_Grid();
        }


        /// <summary>
        /// Row Delte Grid view  For UserRegistration
        /// </summary>
        /// 
        protected void grdHeaderList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        /// <summary>
        /// Row Editing Grid view  For UserRegistration
        /// </summary>
        /// 

        protected void grdHeaderList_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        /// <summary>
        /// Row Sorting Grid view  For UserRegistration Details
        /// </summar
        /// 

        protected void grdHeaderList_Sorting(object sender, GridViewSortEventArgs e)
        {
            //string Sortdir = GetSortDirection(e.SortExpression);
            //string SortExp = e.SortExpression;
            //String Creation_Company = Session["ClientRegistrationNo"].ToString();
            //List<Administrator_UserRegistration> list = objSCM_userRegistration.Get_UserRegistration(Creation_Company);


            //if (Sortdir == "ASC")
            //{
            //    list = Sort<Administrator_UserRegistration>(list, SortExp, SortDirection.Ascending);
            //}
            //else
            //{
            //    list = Sort<Administrator_UserRegistration>(list, SortExp, SortDirection.Descending);
            //}
            //this.grdUserRegistration.DataSource = list;
            //this.grdUserRegistration.DataBind();
        }

        //private List<Administrator_UserRegistration> Sort<TKey>(List<Administrator_UserRegistration> list, string sortBy, SortDirection direction)
        //{
        //    PropertyInfo property = list.GetType().GetGenericArguments()[0].GetProperty(sortBy);
        //    if (direction == SortDirection.Ascending)
        //    {
        //        return list.OrderBy(e => property.GetValue(e, null)).ToList<Administrator_UserRegistration>();
        //    }
        //    else
        //    {
        //        return list.OrderByDescending(e => property.GetValue(e, null)).ToList<Administrator_UserRegistration>();
        //    }
        //}

        //  GetSortDirection Method it using PRoduct Grid Sorting  
        //private string GetSortDirection(string column)
        //{
        //    string sortDirection = "ASC";
        //    string sortExpression = ViewState["SortExpression"] as string;
        //    if (sortExpression != null)
        //    {
        //        if (sortExpression == column)
        //        {
        //            string lastDirection = ViewState["SortDirection"] as string;
        //            if ((lastDirection != null) && (lastDirection == "ASC"))
        //            {
        //                sortDirection = "DESC";
        //            }
        //        }
        //    }
        //    ViewState["SortDirection"] = sortDirection;
        //    ViewState["SortExpression"] = column;
        //    return sortDirection;
        //}

        #endregion



        #region Permissions

        // // // Add Functioanlity Permissions
        public void AdminPermissionAdd()
        {
            try
            {
                string Cmpy = (string)Session["ClientRegistrationNo"];
                string Brnach = (string)Session["BranchID"];
                string UserType = (string)Session["User_Type"];
                string UserName = (string)Session["User_Name"];
                var objUserPermissions = (from UserPerm in objDAL.Administrator_UserRegistrations
                                          where UserPerm.Branch_ID == Brnach
                                          && UserPerm.Login_Id == UserName && UserPerm.User_Type == UserType
                                          select UserPerm).ToList();
                if (objUserPermissions.Count > 0)
                {
                    string[] CmpName = objUserPermissions[0].AllowCompanyListID.Split(',');
                    int count = 0;

                    for (int i = 0; i < CmpName.Length; i++)
                    {
                        if (CmpName[i] == Cmpy)
                        {
                            string[] AllowCreate = objUserPermissions[0].Administrator_Create.Split(',');

                            for (int j = 0; j < AllowCreate.Length; j++)
                            {



                                string abc = AllowCreate[j];
                                switch (abc)
                                {
                                    case "User Registration":
                                        {
                                            count = count + 1;
                                            break;
                                        }

                                    case "":
                                        {
                                            //   count = count + 1;
                                            break;
                                        }


                                }

                            }

                        }

                    }
                    if (count > 0)
                    {
                        Response.Redirect("AddUserRegistration.aspx");
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Sorry You Do not have Permissions for Creating new User Registration  ..!');", true);
                    }
                }


            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        // // // Modify Functionality Permissions
        public void AdminPermissionModify(string strEdit)
        {
            try
            {
                string Cmpy = (string)Session["ClientRegistrationNo"];
                string Brnach = (string)Session["BranchID"];
                string UserType = (string)Session["User_Type"];
                string UserName = (string)Session["User_Name"];
                var objUserPermissions = (from UserPerm in objDAL.Administrator_UserRegistrations
                                          where UserPerm.Branch_ID == Brnach
                                          && UserPerm.Login_Id == UserName && UserPerm.User_Type == UserType
                                          select UserPerm).ToList();
                if (objUserPermissions.Count > 0)
                {
                    string[] CmpName = objUserPermissions[0].AllowCompanyListID.Split(',');
                    int count = 0;

                    for (int i = 0; i < CmpName.Length; i++)
                    {
                        if (CmpName[i] == Cmpy)
                        {
                            string[] AllowCreate = objUserPermissions[0].Administrator_Modify.Split(',');

                            for (int j = 0; j < AllowCreate.Length; j++)
                            {


                                string abc = AllowCreate[j];
                                switch (abc)
                                {
                                    case "User Registration":
                                        {
                                            count = count + 1;
                                            break;
                                        }

                                    case "":
                                        {
                                            // count = count + 1;
                                            break;
                                        }


                                }

                            }

                        }

                    }
                    if (count > 0)
                    {
                        string path = "AddUserRegistration.aspx?id=" + strEdit;
                        Response.Redirect(path);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Sorry You Do not have Permissions for Editing User Registration Details..!');", true);
                    }
                }


            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        // // // Delete Functionality Permissions
        public void AdminPermissionDelete(string strDelete)
        {
            //try
            //{
            //    string Cmpy = (string)Session["ClientRegistrationNo"];
            //    string Brnach = (string)Session["BranchID"];
            //    string UserType = (string)Session["User_Type"];
            //    string UserName = (string)Session["User_Name"];
            //    var objUserPermissions = (from UserPerm in objDAL.Administrator_UserRegistrations
            //                              where UserPerm.Branch_ID == Brnach
            //                              && UserPerm.Login_Id == UserName && UserPerm.User_Type == UserType
            //                              select UserPerm).ToList();
            //    if (objUserPermissions.Count > 0)
            //    {
            //        string[] CmpName = objUserPermissions[0].AllowCompanyListID.Split(',');
            //        int count = 0;

            //        for (int i = 0; i < CmpName.Length; i++)
            //        {
            //            if (CmpName[i] == Cmpy)
            //            {
            //                string[] AllowCreate = objUserPermissions[0].Administrator_Modify.Split(',');

            //                for (int j = 0; j < AllowCreate.Length; j++)
            //                {
            //                    //if (AllowCreate[j] == "Company Information")
            //                    //{
            //                    //    Response.Redirect("AddCompanyInformation.aspx");
            //                    //}



            //                    string abc = AllowCreate[j];
            //                    switch (abc)
            //                    {
            //                        case "User Registration":
            //                            {
            //                                count = count + 1;
            //                                break;
            //                            }

            //                        case "":
            //                            {
            //                                //count = count + 1;
            //                                break;
            //                            }


            //                    }

            //                }

            //            }

            //        }
            //        if (count > 0)
            //        {
            //            string Creation_Company = Session["ClientRegistrationNo"].ToString();

            //            Delete_UserRegistrationDetails(strDelete, Creation_Company);
            //        }
            //        else
            //        {
            //            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Sorry You Do not have Permissions For Deleteing User Registration Details..!');", true);
            //        }
            //    }


            //}
            //catch (Exception ex)
            //{
            //}
            //finally
            //{
            //}
        }
        #endregion
    }
}