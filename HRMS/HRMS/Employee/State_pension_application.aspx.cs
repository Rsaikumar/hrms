﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BAL;
using DAL;
using System.Globalization;

namespace HRMS.Employee
{
    public partial class State_Pension_Application : System.Web.UI.Page
    {
        StatePensionApplicationBAL objlapp = new StatePensionApplicationBAL();
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();


        protected void Page_Load(object sender, EventArgs e)
        {
            method();

            if (Convert.ToString(Session["PensionNo"]) != string.Empty)
            {
                pensiongetdeails();
            }
        }
        public void method()
        {
            try
            {
                string CreationCompany = Session["ClientRegistrationNo"].ToString();
                string Emp_Code = (from em in ObjDAL.HRMS_Employee_Master_PersonalDetails where em.User_Name == Session["User_Name"].ToString() select em.Emp_Code).FirstOrDefault();

                var basicamount = (from amount in ObjDAL.HRMS_Employee_Official_Info_Head_Details
                                   where amount.Creation_Company == CreationCompany && amount.Emp_Code == Emp_Code
                                   select amount.Basic_Amount).FirstOrDefault();

                double a = 0.05;
                double b = 0.03;

                double abc = (Math.Round(Convert.ToDouble(basicamount) * Convert.ToDouble(a)));
                txtfromname.Text = abc.ToString();
                double bcd = (Math.Round(Convert.ToDouble(basicamount) * Convert.ToDouble(b)));
                txtaddress.Text = bcd.ToString();
                var loantype = (from loan in ObjDAL.HRMSLoanApplications
                                where loan.CreationCompany == CreationCompany && loan.EmpCode == Emp_Code
                                select loan.TypeOfLoanApply).FirstOrDefault();
                if (loantype == "HBA")
                {
                    var loandetails = (from loan in ObjDAL.HRMSLoanApplications
                                       where loan.CreationCompany == CreationCompany && loan.EmpCode == Emp_Code && loan.TypeOfLoanApply == "HBA"
                                       select loan).ToList();

                    txthbaprinciple.Text = loandetails[0].LoanAmountRequired.ToString();
                    txthbaintrest.Text = loandetails[0].RateOfInterest.ToString();


                    double hba = (Math.Round((Convert.ToDouble(txthbaprinciple.Text) * Convert.ToDouble(txthbaintrest.Text) / 100) + Convert.ToDouble(txthbaprinciple.Text)));
                    txthbatotal.Text = hba.ToString();
                }
                else if (loantype == "MC")
                {
                    var loandetailmc = (from loan in ObjDAL.HRMSLoanApplications
                                        where loan.CreationCompany == CreationCompany && loan.EmpCode == Emp_Code && loan.TypeOfLoanApply == "MC"
                                        select loan).ToList();

                    txtmotarprinciple.Text = loandetailmc[0].LoanAmountRequired.ToString();
                    txtmotarintrest.Text = loandetailmc[0].RateOfInterest.ToString();


                    double mc = (Math.Round((Convert.ToDouble(txtmotarprinciple.Text) * Convert.ToDouble(txtmotarintrest.Text) / 100) + Convert.ToDouble(txtmotarprinciple.Text)));
                    txtmotartotal.Text = mc.ToString();
                }

                else if (loantype == "MA")
                {
                    var loandetailma = (from loan in ObjDAL.HRMSLoanApplications
                                        where loan.CreationCompany == CreationCompany && loan.EmpCode == Emp_Code && loan.TypeOfLoanApply == "MA"
                                        select loan).ToList();

                    txtmariageprinciple.Text = loandetailma[0].LoanAmountRequired.ToString();
                    txtmariageintrest.Text = loandetailma[0].RateOfInterest.ToString();


                    double ma = (Math.Round((Convert.ToDouble(txtmariageprinciple.Text) * Convert.ToDouble(txtmariageintrest.Text) / 100) + Convert.ToDouble(txtmariageprinciple.Text)));
                    txtmariagetotal.Text = ma.ToString();
                }

                else if (loantype == "FA")
                {
                    var loandetailfa = (from loan in ObjDAL.HRMSLoanApplications
                                        where loan.CreationCompany == CreationCompany && loan.EmpCode == Emp_Code && loan.TypeOfLoanApply == "FA"
                                        select loan).ToList();

                    txtfestivalprinciple.Text = loandetailfa[0].LoanAmountRequired.ToString();
                    txtfestivalintrest.Text = loandetailfa[0].RateOfInterest.ToString();


                    double fa = (Math.Round((Convert.ToDouble(txtfestivalprinciple.Text) * Convert.ToDouble(txtfestivalintrest.Text) / 100) + Convert.ToDouble(txtfestivalprinciple.Text)));
                    txtfestivaltotal.Text = fa.ToString();
                }
                else if (loantype == "EA")
                {
                    var loandetailea = (from loan in ObjDAL.HRMSLoanApplications
                                        where loan.CreationCompany == CreationCompany && loan.EmpCode == Emp_Code && loan.TypeOfLoanApply == "EA"
                                        select loan).ToList();

                    educationprinciple.Text = loandetailea[0].LoanAmountRequired.ToString();
                    educationintrest.Text = loandetailea[0].RateOfInterest.ToString();


                    double ea = (Math.Round((Convert.ToDouble(educationprinciple.Text) * Convert.ToDouble(educationintrest.Text) / 100) + Convert.ToDouble(educationprinciple.Text)));
                    educationtotal.Text = ea.ToString();
                }
                else if (loantype == "CA")
                {
                    var loandetailca = (from loan in ObjDAL.HRMSLoanApplications
                                        where loan.CreationCompany == CreationCompany && loan.EmpCode == Emp_Code && loan.TypeOfLoanApply == "CA"
                                        select loan).ToList();

                    txtcomputerprinciple.Text = loandetailca[0].LoanAmountRequired.ToString();
                    txtcomputerintrest.Text = loandetailca[0].RateOfInterest.ToString();


                    double ca = (Math.Round((Convert.ToDouble(txtcomputerprinciple.Text) * Convert.ToDouble(txtcomputerintrest.Text) / 100) + Convert.ToDouble(txtcomputerprinciple.Text)));
                    txtcomputertotal.Text = ca.ToString();
                }
                else
                {
                    txthbaintrest.Text = "0";
                    txthbaprinciple.Text = "0";
                    txthbatotal.Text = "0";
                    txtmotarintrest.Text = "0";
                    txtmotarprinciple.Text = "0";
                    txtmotartotal.Text = "0";
                    txtmariageprinciple.Text = "0";
                    txtmariageintrest.Text = "0";
                    txtmariagetotal.Text = "0";
                    txtcomputerintrest.Text = "0";
                    txtcomputerprinciple.Text = "0";
                    txtcomputertotal.Text = "0";
                    txtfestivalintrest.Text = "0";
                    txtfestivalprinciple.Text = "0";
                    txtfestivaltotal.Text = "0";
                    educationintrest.Text = "0";
                    educationprinciple.Text = "0";
                    educationtotal.Text = "0";
                }

                var employeddetails = (from loan in ObjDAL.HRMS_Employee_Master_PersonalDetails
                                       where loan.Creation_Company == CreationCompany && loan.Emp_Code == Emp_Code
                                       select loan).ToList();
                txtgvtserventname.Text = employeddetails[0].User_Name.ToString();
                txtpostheld.Text = employeddetails[0].User_Name.ToString();
                txtparmanetaddress.Text = employeddetails[0].Permanent_Address1.ToString();
                txtafterretirementaddress.Text = employeddetails[0].Permanent_Address2.ToString();
                txtbankname.Text = employeddetails[0].Bank_Name.ToString();
                txtbankacountno.Text = employeddetails[0].Account_Number.ToString();
            }
            catch (Exception ex)
            {

            }
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            saving();
        }
        private void saving()
        {
            try
            {
                objlapp.Circle = txtcircle.Text;
                objlapp.District = txtdistrict.Text;
                objlapp.SubDistrict = txtsubdistrict.Text;
                objlapp.EmployeeName = txtemployeename.Text;
                objlapp.ForwardServiceName = txtforwardservcename.Text;
                objlapp.ExpiredWhileService = DateTime.ParseExact(txtexpiredservice.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                objlapp.Receipt = txtreceipt.Text;
                objlapp.CopyToName = txtcopyname.Text;
                objlapp.CopyAddress = txtcopyaddress.Text;
                objlapp.GovernmentServiceName = txtgvtserventname.Text;
                objlapp.PostHeld = DateTime.ParseExact(txtpostheld.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                objlapp.PermanentAddress = txtparmanetaddress.Text;
                objlapp.AddressAfterRetirement = txtafterretirementaddress.Text;
                objlapp.PensionDisbusingAutority = txtpansionauthority.Text;

                objlapp.BankAccount = txtbankacountno.Text;
                objlapp.BankName = txtbankname.Text;
                objlapp.ApplicationOfPension = DateTime.ParseExact(txtappfpensionrecived.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                objlapp.LateName = txtlatename.Text;
                objlapp.LateGuardianShip = txtlateguardname.Text;
                objlapp.DateOfEntering = DateTime.ParseExact(txtentering.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                objlapp.DateOfRetirement = DateTime.ParseExact(txtdateofretirement.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                objlapp.FamilyPension = Convert.ToDecimal(txtfamilypension.Text);
                objlapp.NormalFamilyPension = Convert.ToDecimal(txtnormalfamilypension.Text);
                objlapp.HousePrinciple = Convert.ToDecimal(txthbaprinciple.Text);
                objlapp.HouseInterest = Convert.ToDecimal(txthbaintrest.Text);
                objlapp.HouseTotal = Convert.ToDecimal(txthbatotal.Text);
                objlapp.MotorPrinciple = Convert.ToDecimal(txtmotarprinciple.Text);
                objlapp.MotorInterest = Convert.ToDecimal(txtmotarintrest.Text);
                objlapp.MotorTotal = Convert.ToDecimal(txtmotartotal.Text);
                objlapp.MarriagePrinciple = Convert.ToDecimal(txtmariageprinciple.Text);
                objlapp.MarriageInterest = Convert.ToDecimal(txtmotarintrest.Text);
                objlapp.MarriageTotal = Convert.ToDecimal(txtmariagetotal.Text);
                objlapp.FestivalInterest = Convert.ToDecimal(txtfestivalintrest.Text);
                objlapp.FestivalPrinciple = Convert.ToDecimal(txtfestivalprinciple.Text);
                objlapp.FestivalTotal = Convert.ToDecimal(txtfestivaltotal.Text);
                objlapp.ComputerInterest = Convert.ToDecimal(txtcomputerintrest.Text);
                objlapp.ComputerPrinciple = Convert.ToDecimal(txtcomputerprinciple.Text);
                objlapp.ComputerTotal = Convert.ToDecimal(txtcomputertotal.Text);
                objlapp.NominationDate = DateTime.ParseExact(txtnominationon.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                objlapp.DateThis = DateTime.ParseExact(txtdatedthis.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                objlapp.DateOf = DateTime.ParseExact(txtdayof.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                objlapp.Year = txtyear.Text;
                objlapp.Place = txtplace.Text;
                objlapp.FromAddress = txtaddress.Text;
                objlapp.FromName = txtfromname.Text;
                objlapp.AuthorityName = txtnameofauthority.Text;
                objlapp.AuthorityAddress = txtauthoityaddress.Text;
                objlapp.RetiringBenfitsName = txtretiringbenefits.Text;
                objlapp.Designation = txtpendeignation.Text;
                objlapp.AsDesignationName = txtasdesignation.Text;
                objlapp.CreatedBy = Session["User_Name"].ToString();
                objlapp.CreatedOn = System.DateTime.Now;
                objlapp.ModifiedBy = Session["User_Name"].ToString();
                objlapp.ModifiedOn = System.DateTime.Now;
                objlapp.Parameter = 1;
                if (objlapp.InsertHRMSStatePensionApplication() > 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Saved Successfully ');", true);
                   
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Details Not Saved');", true);
                }
            }
            catch (Exception ex)
            {

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            { }
        }


        public void pensiongetdeails()
        {
            var pensiondetails = (from item in ObjDAL.HRMSStatePensionApplications
                                  select item).ToList();

            if (pensiondetails.Count > 0)
            {
                txtcircle.Text = pensiondetails[0].Circle;
                txtdistrict.Text = pensiondetails[0].District;
                txtsubdistrict.Text = pensiondetails[0].SubDistrict;
                txtemployeename.Text = pensiondetails[0].EmployeeName;
                txtforwardservcename.Text = pensiondetails[0].ForwardServiceName;
                txtexpiredservice.Text = Convert.ToDateTime(pensiondetails[0].ExpiredWhileService).ToString("dd-MM-yyyy");
                txtreceipt.Text = pensiondetails[0].Receipt;
                txtcopyname.Text = pensiondetails[0].CopyToName;
                txtgvtserventname.Text = pensiondetails[0].GovernmentServiceName;
                txtpostheld.Text = Convert.ToDateTime(pensiondetails[0].PostHeld).ToString("dd-MM-yyyy");
                txtparmanetaddress.Text = pensiondetails[0].PermanentAddress;
                txtafterretirementaddress.Text = pensiondetails[0].AddressAfterRetirement;
                txtpansionauthority.Text = pensiondetails[0].PensionDisbusingAutority;
                txtbankacountno.Text = pensiondetails[0].BankAccount;
                txtbankname.Text = pensiondetails[0].BankName;
                txtappfpensionrecived.Text = Convert.ToDateTime(pensiondetails[0].ApplicationOfPension).ToString("dd-MM-yyyy");
                txtlatename.Text = pensiondetails[0].LateName;
                txtlateguardname.Text = pensiondetails[0].LateGuardianShip;
                txtentering.Text = Convert.ToDateTime(pensiondetails[0].DateOfEntering).ToString("dd-MM-yyyy");
                txtdateofretirement.Text = Convert.ToDateTime(pensiondetails[0].DateOfRetirement).ToString("dd-MM-yyyy");
                txtfamilypension.Text = pensiondetails[0].FamilyPension.ToString();
                txtnormalfamilypension.Text = pensiondetails[0].NormalFamilyPension.ToString();
                txthbaprinciple.Text = pensiondetails[0].HousePrinciple.ToString();
                txthbaintrest.Text = pensiondetails[0].HouseInterest.ToString();
                txthbatotal.Text = pensiondetails[0].HouseTotal.ToString();
                txtmotarprinciple.Text = pensiondetails[0].MotorPrinciple.ToString();
                txtmotarintrest.Text = pensiondetails[0].MotorInterest.ToString();
                txtmotartotal.Text = pensiondetails[0].MotorTotal.ToString();
                txtmariageintrest.Text = pensiondetails[0].MarriageInterest.ToString();
                txtmariageprinciple.Text = pensiondetails[0].MarriagePrinciple.ToString();
                txtmariagetotal.Text = pensiondetails[0].MarriageTotal.ToString();
                txtfestivalintrest.Text = pensiondetails[0].FestivalInterest.ToString();
                txtfestivalprinciple.Text = pensiondetails[0].FestivalPrinciple.ToString();
                txtfestivaltotal.Text = pensiondetails[0].FestivalTotal.ToString();
                txtcomputerintrest.Text = pensiondetails[0].ComputerInterest.ToString();
                txtcomputerprinciple.Text = pensiondetails[0].ComputerPrinciple.ToString();
                txtcomputertotal.Text = pensiondetails[0].ComputerTotal.ToString();
                txtnominationon.Text = pensiondetails[0].NominationDate.ToString();
                txtdatedthis.Text = Convert.ToDateTime(pensiondetails[0].DateThis).ToString("dd-MM-yyyy");
                txtdayof.Text = Convert.ToDateTime(pensiondetails[0].DateOf).ToString("dd-MM-yyyy");
                txtyear.Text = pensiondetails[0].Year;
                txtplace.Text = pensiondetails[0].Place;
                txtfromname.Text = pensiondetails[0].FromName;
                txtaddress.Text = pensiondetails[0].FromAddress;
                txtauthoityaddress.Text = pensiondetails[0].AuthorityAddress;
                txtnameofauthority.Text = pensiondetails[0].AuthorityName;
                txtretiringbenefits.Text = pensiondetails[0].RetiringBenfitsName;
                txtpendeignation.Text = pensiondetails[0].Designation;
                txtasdesignation.Text = pensiondetails[0].AsDesignationName;






            }
        }

    }
}