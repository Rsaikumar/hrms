﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using BAL;
using DAL;

namespace HRMS.Employee
{
    public partial class PaySlips : System.Web.UI.Page
    {
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();
        Leave_AllotmentBLL objLeaveAllotmentBLL = new Leave_AllotmentBLL();
        EmployeeOfficialInfoBAL objeoinfo = new EmployeeOfficialInfoBAL();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {


               // txtEmpCode.Focus();
                Emp_NameBinding();

            }
        }
        public void Bind_Grid1()
        {

            try
            {
                string Cmpy = (string)Session["ClientRegistrationNo"];
                string Brnach = (string)Session["BranchID"];
                string UserType = (string)Session["User_Type"];
                string UserName = (string)Session["User_Name"];
                string Cration_Company = (string)Session["CreationCompany"]; //"000001";

                //if (Session["User_Type"].ToString() == "CCF")
                //{
        
                //string EMP_Code = (from EM in ObjDAL.HRMS_Employee_Master_PersonalDetails where EM.User_Name == UserName select EM.Emp_Code).FirstOrDefault();
               

                var objHeaderInfo = (from header in ObjDAL.HRMSEmployeeMasterSalaryDetails
                                     where header.EmpID == txtEmpCode.Text
                                     select header).ToList();
                if (objHeaderInfo.Count > 0)
                {
                    lblbasicpay.Text = Convert.ToDecimal(objHeaderInfo[0].BasicAmount).ToString();
                    lblda.Text = Convert.ToDecimal(objHeaderInfo[0].DA).ToString();
                    lblhra.Text = Convert.ToDecimal(objHeaderInfo[0].HRA).ToString();
                    lblcca.Text = Convert.ToDecimal(objHeaderInfo[0].CCA).ToString();
                    lblgrossamount.Text = Convert.ToDecimal(objHeaderInfo[0].GrossAmount).ToString();

                    var Deduction = (from header in ObjDAL.HRMSEmployeeMasterDeductionsDetails
                                         where header.EmpID == txtEmpCode.Text
                                         select header).ToList();
                    if(Deduction.Count > 0)
                    {
                        decimal tds = Convert.ToDecimal(Deduction[0].TDSAmount);
                        decimal pf = Convert.ToDecimal(Deduction[0].PFAmount);
                        decimal esi = Convert.ToDecimal(Deduction[0].ESIAmount);
                        decimal pt = Convert.ToDecimal(Deduction[0].ProfessionalTaxAmount);
                        decimal ga = Convert.ToDecimal(objHeaderInfo[0].GrossAmount);

                        decimal total = tds + pf + esi + pt;

                        decimal gross = ga - total;

                        lbldeductions.Text = total.ToString();
                        lblpf.Text = pf.ToString();
                        lblesi.Text = esi.ToString();
                        lbltds.Text = tds.ToString();
                        lblptamt.Text = pt.ToString();
                        lblnetpayable.Text = gross.ToString();

                    }

                   





                    // lblbillgrossamount.Text = Convert.ToDecimal(objHeaderInfo[0].Gross_Amount).ToString();
                    //lblapglis.Text = Convert.ToDecimal(objHeaderInfo[0].APGLI).ToString();
                    //lblgis.Text = Convert.ToDecimal(objHeaderInfo[0].GIS).ToString();
                    //lblapglino.Text = Convert.ToDecimal(objHeaderInfo[0].APGLI).ToString();
                    //lblpt.Text = Convert.ToDecimal(objHeaderInfo[0].PTAX).ToString();
                    //lblhba.Text = Convert.ToDecimal(objHeaderInfo[0].HBA).ToString();

                    var objHeadInfo = (from header in ObjDAL.HRMSEmplyeeMasterPersonalIdentificationDetails
                                       where header.EmpID == txtEmpCode.Text
                                       select header).ToList();

                    if (objHeadInfo.Count > 0)
                    {
                        lblpanno.Text = objHeadInfo[0].PanNo;
                        lblbankacno.Text = objHeadInfo[0].AccountNumber;
                        lblbankname.Text = objHeadInfo[0].BankName;

                        var Objdesignation = (from header in ObjDAL.HRMSEmployeeMasterRegistrationDetails
                                           where header.ID == Convert.ToInt32(txtEmpCode.Text)
                                           select header).ToList();

                        if(Objdesignation.Count > 0)
                        {
                            var Objofsr = (from header in ObjDAL.HRMSRoleMasters
                                                  where header.RoleId == Objdesignation[0].UserSubRole
                                           select header).ToList();
                            if(Objofsr.Count > 0)
                            {
                                lbldesignation.Text = Objofsr[0].RoleName;
                            }
                        }




                        // grdpayroll.DataSource = objHeaderInfo;
                    }
                    else
                    {
                        // // // Grid view using setting initial rows displaying Empty Header Displying
                        // setinitialrow();
                    }


                    //List<HRMS_Officer_Role> objrole = (from or in ObjDAL.HRMS_Officer_Roles
                    //                                   where or.RoleID == objEmpRegistration[0].UserRole
                    //                                   select or).ToList();

                    //lbluserrole.Text = Convert.ToString(objrole[0].RoleShortName);


                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }
        }

     
        protected void txtEmpCode_TextChanged(object sender, EventArgs e)
        {
            string CreationCompany = Session["ClientRegistrationNo"].ToString();
            string Emp_Code = (txtEmpCode.Text.ToLower().Trim() == "") ? "" : txtEmpCode.Text.ToLower().Trim();
            var objEmPCodeDUP = (from itmes in ObjDAL.HRMS_Employee_Master_PersonalDetails
                                 where itmes.Creation_Company == CreationCompany && itmes.Emp_Code.ToLower().Trim() == Emp_Code
                                 select new
                                 {
                                     Emp_name = itmes.Emp_First_Name + " " + itmes.Emp_Last_Name + " " + itmes.Emp_Middle_Name,
                                     Emp_Code = itmes.Emp_Code
                                 }).ToList();
            if (objEmPCodeDUP.Count > 0)
            {

                ddlEmpName.SelectedItem.Text = objEmPCodeDUP[0].Emp_name;

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Emp Code is Not Valid');", true);
                txtEmpCode.Text = "";
                txtEmpCode.Focus();
                ddlEmpName.SelectedValue = "0";


            }
        }
        protected void ddlEmpName_SelectedIndexChanged(object sender, EventArgs e)
        {
            string CreationCompany = (string)Session["ClientRegistrationNo"].ToString();
            txtEmpCode.Text = ddlEmpName.SelectedValue;

        }
        public void Emp_NameBinding()

        {
            int Emp_Code = (from em in ObjDAL.HRMSEmployeeMasterRegistrationDetails where em.UserName == Session["User_Name"].ToString() select em.ID).FirstOrDefault();
          //  string CreationCompany = Session["ClientRegistrationNo"].ToString();

            var objEmPCodeDUP = (from itmes in ObjDAL.USP_gettingemployeenameandid(Emp_Code)
                                 select itmes).ToList();
                               
            if (objEmPCodeDUP.Count > 0)
            {
                ddlEmpName.DataSource = objEmPCodeDUP;
                ddlEmpName.DataTextField = "empname";
                ddlEmpName.DataValueField = "empid";
                ddlEmpName.DataBind();
                ddlEmpName.Items.Insert(0, new ListItem("--Select--", "0"));
            }

        }
        public void payslipbindempdetails()
        {

            string Cmpy = (string)Session["ClientRegistrationNo"];
            string Brnach = (string)Session["BranchID"];
            string UserType = (string)Session["User_Type"];
            string UserName = (string)Session["User_Name"];
            string Cration_Company = (string)Session["CreationCompany"]; //"000001";

            if (Session["User_Type"].ToString() == "CCF")
            {
                string EMP_Code = (from EM in ObjDAL.HRMS_Employee_Master_PersonalDetails where EM.User_Name == UserName select EM.Emp_Code).FirstOrDefault();
                var objHeaderInfo1 = (from header in ObjDAL.HRMS_Employee_Official_Info_Head_Details
                                      where header.Creation_Company == Cmpy && header.Emp_Code == EMP_Code
                                      select header).ToList();
                if (objHeaderInfo1.Count > 0)
                {

                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            lblempcode.Text = txtEmpCode.Text;
            lblempname.Text = ddlEmpName.SelectedItem.Text;
            lblbilldate.Text = DateTime.Now.ToString("MM-dd-yyyy");
            lbltokendatae.Text = DateTime.Now.ToString("MM-dd-yyyy");
            lbldesignation.Text = "CCF";
            Bind_Grid1();



        }
    }
}
