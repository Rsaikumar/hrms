﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true"
    CodeBehind="OTCriteria.aspx.cs" Inherits="HRMS.Employee.OTCriteria" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" lang="javascript">
        function Search() {
            if (document.getElementById('<%=txtSearch.ClientID%>').value == "") {
                alert("Search Box Can't be Empty ... ");
                document.getElementById('<%=txtSearch.ClientID%>').focus();
                return false;
            }
            return true;
        }
   
    </script>
    <link href="../css/Employee/OTCriteria.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <div id="main">
                <div class="container-fluid">
                    <div class="page-header">
                        <div class="pull-left Search_Div" id="btnSearch_Div">
                            <div class="Search_Icon">
                                <asp:TextBox ID="txtSearch" runat="server" placeholder="Search For OT Criteria.."
                                    class="form-control txtSearch"></asp:TextBox>
                            </div>
                        </div>
                        <div class="pull-left">
                            <asp:Button ID="btnSearch" OnClientClick="return Search()" runat="server" class="search_Btn"
                                OnClick="btnSearch_Click" />
                        </div>
                        <div class="pull-right">
                            <ul class="stats">
                                <li>
                                    <asp:Button ID="btnAdd" runat="server" class="add_Btn" OnClick="btnAdd_Click" ToolTip="Create OT Criteria Details" /></li>
                                <li>
                                    <asp:Button ID="btnPdf" runat="server" class="PDF_Btn" ToolTip="Export OT Criteria To Pdf" /></li>
                                <li>
                                    <asp:Button ID="btnExcel" runat="server" class="Excel_Btn" ToolTip="Export OT Criteria To Excel" /></li>
                            </ul>
                        </div>
                    </div>
                    <div class="breadcrumbs">
                        <ul>
                            <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i>
                            </li>
                            <li><a href="">Payroll</a><i class="fa fa-angle-right"></i> </li>
                            <li><a href="OTCriteria.aspx">OT Criteria List</a><i class="fa fa-angle-right"></i>
                            </li>
                            <li><a href=""></a></li>
                            <li>
                                <asp:LinkButton Text="OT Criteria" ID="lkbOTCriteria" runat="server" OnClick="lkbOTCriteria_Click"></asp:LinkButton>
                                <%--<a href="AddUserRegistration.aspx">User Registration</a>--%>
                            </li>
                        </ul>
                        <div class="close-bread">
                            <a href="#"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                                        <div class="box-title">
                                            <h3>
                                                <i class="fa fa-th-list"></i>OT Criteria</h3>
                                        </div>
                                        <div class="box-content">
                                            <form action="#" class='form-horizontal form-bordered'>
                                            <div class="col-sm-12" style="margin-bottom: 0.3%;">
                                                <div id="Iscroll" runat="server" style="">
                                                    <%----%>
                                                    <asp:GridView ID="grdOTMaster" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                                        Style="margin-bottom: 0.83%; margin-top: 0.83%;  font-weight: 100;"
                                                        runat="server" ShowFooter="true" AutoGenerateColumns="False" OnPageIndexChanging="grdOTMaster_PageIndexChanging"
                                                        OnSorting="grdOTMaster_OnSorting" PageSize="15" AllowSorting="true" OnRowDataBound="grd2Bind"
                                                        OnRowCommand="grdOTMaster_RowCommand" AllowPaging="true" DataKeyNames="OT_Code">
                                                        <AlternatingRowStyle BackColor="#EEEEEE" />
                                                        <FooterStyle BackColor="#EEEEEE" ForeColor="Black" />
                                                        <PagerStyle BackColor="#EEEEEE" ForeColor="Black" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                        <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                        <SortedDescendingHeaderStyle BackColor="#000065" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="OT Code">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOTCode" runat="server" Text='<%#Eval("OT_Code")%>' Style="text-align: center;"></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle  />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Ot Head Names">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOtHeadNames" runat="server" Text='<%#Eval("Ot_Head_Names") %>'
                                                                        Style="text-align: center;"></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    <table class="table-nomargin table-bordered" style="width:100%;">
                                                                        <tr>
                                                                            <td style="">
                                                                                Ot Criteria
                                                                            </td>
                                                                            <td style="">
                                                                                Ot Rate
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:GridView ID="grdOTChild" class="table table-hover table-nomargin table-bordered CustomerAddress otCriteria"
                                                                        Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%;" runat="server"
                                                                        AutoGenerateColumns="false" ShowHeader="false">
                                                                        <AlternatingRowStyle BackColor="#EEEEEE" />
                                                                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                                        <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                                        <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                                        <SortedDescendingHeaderStyle BackColor="#000065" />
                                                                        <Columns>
                                                                            <asp:BoundField DataField="Ot_Criteria" ItemStyle-HorizontalAlign="Right">
                                                                                <ItemStyle  />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="Ot_Rate" ItemStyle-HorizontalAlign="Right">
                                                                                <ItemStyle  />
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </ItemTemplate>
                                                                <HeaderStyle  />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lbkEdit" runat="server" CommandName="Edit" Style="color: gray;
                                                                        text-decoration: none; float: left; background-image: url('../img/file_edit.png');
                                                                        height: 30px; width: 35px; background-position: left; background-repeat: no-repeat;"
                                                                        OnClientClick="return confirm('Are you sure you want to Edit this Record ...?')"
                                                                        CausesValidation="false" CommandArgument='<%#Eval("OT_Code") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                          <%--  <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete" Style="color: gray;
                                                                        text-decoration: none; float: left; background-image: url('../img/delete.png');
                                                                        height: 30px; width: 35px; background-position: left; background-repeat: no-repeat;"
                                                                        OnClientClick="return confirm('Are you sure you want to Remove this Record ...?')"
                                                                        CausesValidation="false" CommandArgument='<%#Eval("OT_Code") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
