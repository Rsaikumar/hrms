﻿using BAL;
using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Employee
{
    public partial class AddOTCriteria : System.Web.UI.Page
    {
        ERP_DatabaseDataContext objdb = new ERP_DatabaseDataContext();
        OTCriteriaMaster objOTCriteriaBAL = new OTCriteriaMaster();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_Type"].ToString() == "Employees")
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('You Don't have Permissions To Create..');", true);
                Response.Redirect("~/Dashboard/Dashboard.aspx");
            }
            else
            {

                if (!IsPostBack)
                {
                    if (Session["ClientRegistrationNo"] == null)
                    {
                        Response.Redirect("~/Default.aspx");
                    }

                    //OTList();

                    if ((string)Session["otCode"] != null)
                    {

                        //  Request.QueryString.Clear();
                        string ClientRegistrationNo = Session["ClientRegistrationNo"].ToString();
                        string otcode = (string)Session["otCode"];
                        Session["updateOtCode"] = otcode;
                        //string LoginID = Request.QueryString["id"];
                        Edit_HeaderDetails(otcode, ClientRegistrationNo);
                    }
                    else
                    {
                        //Set initial Bank Detils Grid Display
                        SetInitialRow();
                        btnUpdate.Visible = false;
                    }
                }
            }
        }


        #region Methods

        // Editing Header Details

        public void Edit_HeaderDetails(string otcode, string CreationCompany)
        {
            try
            {

                var objHeaderID = objOTCriteriaBAL.Edit_OT_Criteria(otcode, CreationCompany);
                if (objHeaderID.Count > 0)
                {
                    txtOTCal.Text = objHeaderID[0].Ot_Head_Names.ToString();
                    grvOTRate.DataSource = objHeaderID;
                    grvOTRate.DataBind();


                    string shortnames = objHeaderID[0].Ot_Head_Names.ToString();
                    if (shortnames.Length > 1)
                    {
                        string[] items = shortnames.Split(',');
                        for (int i = 0; i < chlOTLists.Items.Count; i++)
                        {
                            //    checkedListBox1.SetItemChecked(i, false);//First uncheck the old value!

                            for (int x = 0; x < items.Length; x++)
                            {

                                if (chlOTLists.Items[i].Text == items[x])
                                {
                                    chlOTLists.Items[i].Selected = true;
                                }

                            }
                        }
                    }

                    DataTable dt1 = new DataTable();

                    dt1.Columns.Add("Ot_Criteria", typeof(string));
                    dt1.Columns.Add("Ot_Rate", typeof(string));

                    for (int i = 0; i < grvOTRate.Rows.Count; i++)
                    {
                        TextBox txtCriteria = grvOTRate.Rows[i].FindControl("txtCriteria") as TextBox;
                        TextBox txtOTRate = grvOTRate.Rows[i].FindControl("txtOTRate") as TextBox;
                        //txtCriteria.Text = objHeaderID[i].Ot_Criteria.ToString();
                        //txtOTRate.Text = objHeaderID[i].Ot_Rate.ToString();
                        string criteria = txtCriteria.Text;
                        decimal otrate = Convert.ToDecimal(txtOTRate.Text);

                        dt1.Rows.Add(criteria, otrate);

                    }
                    ViewState["OTCurrentTable"] = dt1;
                    if (dt1.Rows.Count > 0)
                    {
                        grvOTRate.DataSource = dt1;
                        grvOTRate.DataBind();
                    }
                    btnSave.Visible = false;
                    btnUpdate.Visible = true;

                    Session["otCode"] = null;


                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('No Data Found ');", true);
                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }

        //Update Header Details
        public void btnUpdate_Click(object sender, EventArgs e)
        {
            Update_Method();

        }

        //Updating Method
        public void Update_Method()
        {


            try
            {
                int count = 0;
                string updateOtcode = (string)Session["updateOtCode"];
                //objOTCriteriaBAL.Addition_Code = txtID.Text;
                //objOTCriteriaBAL.Head_Name = txtHeadName.Text;
                //objOTCriteriaBAL.Short_Name = txtShortName.Text;
                //objOTCriteriaBAL.Caluculation = ddlCaluculation.SelectedValue;
                //objOTCriteriaBAL.Per_Of_Basic = Convert.ToDecimal(txtPerOfBasic.Text);
                if (updateOtcode != null)
                {
                    List<HRMS_OT_Criteria_Master> SUM = (from ST in objdb.HRMS_OT_Criteria_Masters
                                                         where ST.Creation_Company == Session["ClientRegistrationNo"].ToString()
                                                         && ST.OT_Code == updateOtcode
                                                         select ST).ToList();
                    objdb.HRMS_OT_Criteria_Masters.DeleteAllOnSubmit(SUM);
                    objdb.SubmitChanges();

                    if (grvOTRate.Rows.Count != 0)
                    {
                        foreach (GridViewRow objrow in grvOTRate.Rows)
                        {
                            if (objrow.RowType == DataControlRowType.DataRow)
                            {
                                TextBox txtCriteria = objrow.FindControl("txtCriteria") as TextBox;
                                TextBox txtOTRate = objrow.FindControl("txtOTRate") as TextBox;

                                objOTCriteriaBAL.OT_Code = updateOtcode;
                                objOTCriteriaBAL.Ot_Head_Names = txtOTCal.Text;

                                objOTCriteriaBAL.Ot_Criteria = txtCriteria.Text;
                                objOTCriteriaBAL.Ot_Rate = Convert.ToDecimal(txtOTRate.Text);




                                objOTCriteriaBAL.Creation_Company = Session["ClientRegistrationNo"].ToString();
                                objOTCriteriaBAL.Created_By = Session["User_Name"].ToString();
                                objOTCriteriaBAL.Created_Date = System.DateTime.Now;
                                objOTCriteriaBAL.Modified_By = Session["User_Name"].ToString();
                                objOTCriteriaBAL.Modified_Date = System.DateTime.Now;


                                objOTCriteriaBAL.Parameter = 1;
                                objOTCriteriaBAL.Insert_HRMS_OT_Criteria_Master();
                            }
                            count = count + 1;
                            if (count == grvOTRate.Rows.Count)
                            {
                                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);
                                ClearAllFields();
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('No Data Found ');", true);

                            }
                            //if (objOTCriteriaBAL.Insert_HRMS_OT_Criteria_Master() != 0)
                            //    {
                            //        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('User Registration Data Saved Successfully');", true);
                            //        ClearAllFields();
                            //    }
                            //    else
                            //    {
                            //        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('User Registration Data Not Saved');", true);
                            //    }
                        }
                    }
                }

            }
            catch (Exception ex)
            {

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved ');", true);
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            {

            }

        }
        /// <summary>
        /// Get Data From Header Master
        /// </summary>
        /// <param name="sender"></param>
        /// 

        // binding checkboxes in checkbox list

        //public void OTList()
        //{
        //    try
        //    {
        //        string Creation_Company = Session["ClientRegistrationNo"].ToString();
        //        string Company_Name = Session["CreationCompany"].ToString();

        //        List<HRMS_Additions_Master> objShortInfoDetails = (from Short in objdb.HRMS_Additions_Masters
        //                                                           where Short.Creation_Company == Creation_Company
        //                                                           select Short).ToList();
        //        //List<SCM_Administrator_CompanyInfo> objCompanyInfoDetails = ObjSCM_CompanyInfo.GetData_CompanyInfo(Creation_Company);
        //        if (objShortInfoDetails.Count > 0)
        //        {

        //            chlOTLists.DataSource = objShortInfoDetails;

        //            chlOTLists.DataTextField = "Short_Name";
        //            chlOTLists.DataValueField = "Short_Name";
        //            chlOTLists.DataBind();
        //            //ddlProdGroup.Items.Insert(0, new ListItem("--Group--", "0")); //updated code
        //        }
        //        else
        //        {
        //            chlOTLists.Items.Insert(0, new ListItem(Company_Name, Company_Name)); //updated code
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        string Error = ex.ToString();
        //        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + Error + "');", true);
        //    }
        //    finally
        //    {
        //    }
        //}


        /// <summary>
        /// SetInitialRow
        /// </summary>
        public void SetInitialRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("Ot_Criteria", typeof(string)));
            dt.Columns.Add(new DataColumn("Ot_Rate", typeof(string)));
            dr = dt.NewRow();
            dr["Ot_Criteria"] = string.Empty;
            dr["Ot_Rate"] = string.Empty;

            dt.Rows.Add(dr);
            ViewState["OTCurrentTable"] = dt;
            grvOTRate.DataSource = dt;
            grvOTRate.DataBind();
        }
        #endregion

        #region Events
        // checkbox list changed event
        protected void chlOTLists_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Create the list to store.
            List<String> StrList = new List<string>();
            // Loop through each item.
            foreach (ListItem item in chlOTLists.Items)
            {
                if (item.Selected)
                {
                    // If the item is selected, add the value to the list.
                    StrList.Add(item.Value);
                }
                else
                {
                    // Item is not selected, do something else.
                }
            }
            // Join the string together using the ; delimiter.
            String Str = String.Join(",", StrList.ToArray());
            txtOTCal.Text = Str;

        }

        // reset event
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearAllFields();

        }

        // clear all fields
        private void ClearAllFields()
        {
            txtOTCal.Text = "";
            for (int items = 0; items < chlOTLists.Items.Count; items++)
            {
                chlOTLists.ClearSelection();
            }
            // Set instial Row 
            SetInitialRow();

            btnSave.Visible = true;
            btnUpdate.Visible = false;

        }

        //saving data
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int Count = 0;

                var objOT_code = objdb.Sp_HRMS_OT_Criteria_Master_OTCode(Session["ClientRegistrationNo"].ToString()).ToList();


                if (grvOTRate.Rows.Count != 0)
                {
                    foreach (GridViewRow objrow in grvOTRate.Rows)
                    {
                        if (objrow.RowType == DataControlRowType.DataRow)
                        {
                            TextBox txtCriteria = objrow.FindControl("txtCriteria") as TextBox;
                            TextBox txtOTRate = objrow.FindControl("txtOTRate") as TextBox;

                            objOTCriteriaBAL.OT_Code = objOT_code[0].OTCode.ToString();
                            objOTCriteriaBAL.Ot_Head_Names = txtOTCal.Text;

                            objOTCriteriaBAL.Ot_Criteria = txtCriteria.Text;
                            objOTCriteriaBAL.Ot_Rate = Convert.ToDecimal(txtOTRate.Text);
                            objOTCriteriaBAL.Created_By = Session["User_Name"].ToString();
                            objOTCriteriaBAL.Created_Date = System.DateTime.Now;
                            objOTCriteriaBAL.Creation_Company = Session["ClientRegistrationNo"].ToString();
                            objOTCriteriaBAL.Modified_By = Session["User_Name"].ToString();
                            objOTCriteriaBAL.Modified_Date = System.DateTime.Now;
                            objOTCriteriaBAL.Parameter = 1;



                            objOTCriteriaBAL.Insert_HRMS_OT_Criteria_Master();

                            //if (objOTCriteriaBAL.Insert_HRMS_OT_Criteria_Master() != 0)
                            //{
                            //    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);
                            //    ClearAllFields();
                            //}
                            //else
                            //{
                            //    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' No Data to Save..');", true);

                            //}
                        }
                        Count = Count + 1;
                        if (Count == grvOTRate.Rows.Count)
                        {
                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);
                            ClearAllFields();
                        }
                        else
                        {


                        }
                    }
                }



            }
            catch (Exception ex)
            { string msg = ex.Message.ToString(); }
        }
        #endregion

        #region GridEvents
        protected void btnAddNewRecord_Click(object sender, EventArgs e)
        {
            // Adding Empty Row Data 

            AddNewRowToGrid();

            int aa = (int)grvOTRate.Rows.Count;
            TextBox txtCriteria = (TextBox)grvOTRate.Rows[aa - 1].FindControl("txtCriteria") as TextBox;
            //DropDownList ddlLoanType = (DropDownList)grvLoan.Rows[aa - 1].FindControl("ddlLoanType") as DropDownList;
            txtCriteria.Focus();
        }

        public void AddNewRowToGrid()
        {

            int rowIndex = 0;

            if (ViewState["OTCurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["OTCurrentTable"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {

                        TextBox txtCriteria = (TextBox)grvOTRate.Rows[rowIndex].FindControl("txtCriteria");
                        TextBox txtOTRate = (TextBox)grvOTRate.Rows[rowIndex].FindControl("txtOTRate");

                        drCurrentRow = dtCurrentTable.NewRow();
                        dtCurrentTable.Rows[i - 1]["Ot_Criteria"] = txtCriteria.Text;

                        dtCurrentTable.Rows[i - 1]["Ot_Rate"] = txtOTRate.Text;


                        rowIndex += 1;
                    }

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["OTCurrentTable"] = dtCurrentTable;
                    grvOTRate.DataSource = dtCurrentTable;
                    grvOTRate.DataBind();

                }
            }
            else
            {
                Response.Write("ViewState is null");
            }


        }

        // Removing Row Data 
        protected void lbRemove_Click(object sender, EventArgs e)
        {
            AddRemoveNewRowToGrid();

            LinkButton lb = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow.RowIndex;
            if (ViewState["OTCurrentTable"] != null)
            {

                DataTable dt = (DataTable)ViewState["OTCurrentTable"];
                if (dt.Rows.Count > 1)
                {
                    //Remove the Selected Row data and reset row number
                    dt.Rows.Remove(dt.Rows[rowID]);

                }

                //Store the current data in ViewState for future reference
                ViewState["OTCurrentTable"] = dt;

                grvOTRate.DataSource = dt;
                grvOTRate.DataBind();
            }
        }

        public void AddRemoveNewRowToGrid()
        {

            int rowIndex = 0;

            if (ViewState["OTCurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["OTCurrentTable"];
                DataRow drCurrentRow = null;
                int index = dtCurrentTable.Rows.Count;

                if (dtCurrentTable.Rows.Count > 0)
                {

                    TextBox txtCriteria = (TextBox)grvOTRate.Rows[index - 1].FindControl("txtCriteria");

                    TextBox txtOTRate = (TextBox)grvOTRate.Rows[index - 1].FindControl("txtOTRate");

                    drCurrentRow = dtCurrentTable.NewRow();
                    dtCurrentTable.Rows[index - 1]["Ot_Criteria"] = txtCriteria.Text;

                    dtCurrentTable.Rows[index - 1]["Ot_Rate"] = txtOTRate.Text;


                    rowIndex += 1;

                    ViewState["OTCurrentTable"] = dtCurrentTable;
                    grvOTRate.DataSource = dtCurrentTable;
                    grvOTRate.DataBind();

                }
            }
            else
            {
                Response.Write("ViewState is null");
            }



        }





        #endregion
    }
}