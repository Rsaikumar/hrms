﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="AddAddiction_Heads.aspx.cs" Inherits="HRMS.Employee.AddAddiction_Heads" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <link href="../css/Employee/Additions.css" rel="stylesheet"/>
      <script type="text/javascript" lang="javascript">
          function ControlValid()
          {
              if (document.getElementById('<%=txtHeadName.ClientID%>').value == "") {
                alert("Please Enter Head Name");

                document.getElementById('<%=txtHeadName.ClientID%>').focus();

                return false;
              }
              if (document.getElementById('<%=txtShortName.ClientID%>').value == "") {
                alert("Please Enter Short Name");

                document.getElementById('<%=txtShortName.ClientID%>').focus();

                return false;
            }
            if (document.getElementById('<%=ddlCaluculation.ClientID%>').options[document.getElementById('<%=ddlCaluculation.ClientID%>').selectedIndex].value == "Select") {
                alert("Please Select Calculation..");
                document.getElementById('<%=ddlCaluculation.ClientID%>').focus();
                return false;
            }
            return true;
          }
        function ddlCalculationValidation() {
            if (document.getElementById('<%=ddlCaluculation.ClientID%>').options[document.getElementById('<%=ddlCaluculation.ClientID%>').selectedIndex].value == "Select") {
                alert("Please Select Calculation Field..");
                document.getElementById('tinValue').style.display = 'none';
                document.getElementById('<%=txtPerOfBasic.ClientID%>').readOnly = true;
                document.getElementById('<%=ddlCaluculation.ClientID%>').focus();
                return false;
                
            }
            if (document.getElementById('<%=ddlCaluculation.ClientID%>').options[document.getElementById('<%=ddlCaluculation.ClientID%>').selectedIndex].value == "Fixed") {
                //  alert("Please Select TIN No");

                document.getElementById('tinValue').style.display = 'none';
                document.getElementById('<%=txtPerOfBasic.ClientID%>').readOnly = true;
                document.getElementById('<%=btnSave.ClientID%>').focus();
                return false;
            }
            if (document.getElementById('<%=ddlCaluculation.ClientID%>').options[document.getElementById('<%=ddlCaluculation.ClientID%>').selectedIndex].value == "% Of Basic") {
                //  alert("Please Select TIN No");

                document.getElementById('tinValue').style.display = 'initial';
                document.getElementById('<%=txtPerOfBasic.ClientID%>').readOnly = false;
                document.getElementById('<%=txtPerOfBasic.ClientID%>').focus();
                return false;
            }
           
            return true;
        }

      
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i></li>
                     <li>
                        <a href="">Payroll</a><i class="fa fa-angle-right"></i>
                    </li>
                    <li><a href="HeaderList.aspx">Head List</a> <i class="fa fa-angle-right"></i></li>
                    <li><a href="AddAddiction_Heads.aspx">Head</a> </li>
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-sm-12">

                            <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                                <div class="box-title">
                                    <h3>
                                        <i class="fa fa-th-list"></i>Head</h3>
                                </div>
                                <div class="box-content">
                                    <form action="#" method="POST" class='form-horizontal form-bordered'>
                                        <div class="col-sm-12" style="">

                                            <div class="col-sm-12" style="">
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lAID" style="">
                                                        ID
                                                    </label>
                                                    <div class="col-sm-2 tAID" style="">
                                                        <asp:TextBox ID="txtID" runat="server" placeholder="ID" MaxLength="50"
                                                            class="form-control" ReadOnly="true"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lAHeadName" style="">
                                                        Head Name<span id="Span1" runat="server" style="color: Red; display: inline;">*</span></label>
                                                    <div class="col-sm-2 tAHeadName" style="" id="">
                                                        <asp:TextBox ID="txtHeadName" runat="server" placeholder="Head Name" class="form-control"
                                                            MaxLength="50" AutoPostBack="true" OnTextChanged="txtHeadName_OnTextChanged"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lAShortName" style="">
                                                        Short Name<span style="color: #ff0000; font-size: 14px;display:inline;">*</span></label>
                                                    <div class="col-sm-2 tAShortName" style="" id="">
                                                        <asp:TextBox ID="txtShortName" runat="server" placeholder="Short Name" class="form-control"
                                                            MaxLength="50" AutoPostBack="true" OnTextChanged="txtShortName_OnTextChanged"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12" style="">
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lACalculation" style="">
                                                        Calculation<span style="color: #ff0000; font-size: 14px;display:inline;">*</span>
                                                    </label>
                                                    <div class="col-sm-2 tACalculation" style="">
                                                      <%--  onchange="ddlCalculationValidation()"--%>
                                                        <asp:DropDownList ID="ddlCaluculation" runat="server" AutoPostBack="true" class="form-control DropdownCss"  
                                                             OnSelectedIndexChanged="ddlCaluculation_SelectedIndexChanged">
                                                            <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                            <asp:ListItem Text="Fixed" Value="Fixed"></asp:ListItem>
                                                            <asp:ListItem Text="% Of Basic" Value="% Of Basic"></asp:ListItem>
                                                           <%-- <asp:ListItem Text="Base On Attendance" Value="Base On Attendance"></asp:ListItem>--%>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                  
                                                <div class="form-group">
                                                    <label for="textfield" class="control-label col-sm-1 lAPerBasic" style="">
                                                        % Of Basic<span id="tinValue" runat="server" style="color: Red; display: none;">*</span></label>
                                                    <div class="col-sm-2 tAPerBasic" style="" id="">
                                                        <asp:TextBox ID="txtPerOfBasic" runat="server" placeholder="% Of Basic" class="form-control"
                                                            MaxLength="50"></asp:TextBox>
                                                    </div>
                                                </div>
                                              <div class="form-group">
                                                                           
                                                                            <div class="col-sm-2 tBloodDonor" style="" id="">
                                                                                <asp:CheckBox ID="chbBaseOnAttendance" runat="server" Text="" />
                                                                            </div>
                                                   <label for="textfield" class="control-label col-sm-1 lABloodDonor" style="">
                                                                                Calaculate Based on Attendance</label>
                                                                        </div>
                                            </div>
                                        </div>  
                                        <div class="col-sm-2 ACButtons">
                                            <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary" OnClientClick="return ControlValid()" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnUpdate" runat="server" Text="Save" class="btn btn-primary" OnClick="btnUpdate_Click" />
                                            <asp:Button ID="btnCancel" runat="server" Text="Reset" class="btn" OnClick="btnCancel_Click" />
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
