﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BAL;
using DAL;
using System.Data.Linq.SqlClient;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using System.IO;

namespace HRMS.Employee
{
    public partial class EmployeeDetailsPage : System.Web.UI.Page
    {
        string objConfig = ConfigurationManager.ConnectionStrings["Dbconnection"].ToString();

        CityMasterBAL objCityBAL = new CityMasterBAL();
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();
        HRMS_City_Master objCityTable = new HRMS_City_Master();
        DepartmentMasterBAL objDeptBAL = new DepartmentMasterBAL();
        EmployeeGroupMasterBAL objEmpGroupBAL = new EmployeeGroupMasterBAL();
        RecruitMasterBAL objRecruitBAL = new RecruitMasterBAL();
        Employee_Location_BAL ObjEmpLocation = new Employee_Location_BAL();

        JobTypeBAL objJobTypeBAL = new JobTypeBAL();
        EmployeeBAL objEmpBAL = new EmployeeBAL();
        EmployeeOfficialInfoBAL objeoinfo = new EmployeeOfficialInfoBAL();
        EmployeeBAL Total1EmpDetailsBAL = new EmployeeBAL();

        // // // Bank Details Master
        BankMasterBAL objBankBAL = new BankMasterBAL();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindRole();
                BindSubRole();
            }
            SetInitialRow();
            SetInitialRowEducation();
            SetInitialRowgrvProfessional();
            if (Session["User_Type"].ToString() == "CCF")
            {


                grvSalary.Visible = false;

                //// // Paste The Employee Binding Methods
                string Creation_Company = Session["ClientRegistrationNo"].ToString();
                string empcode = Session["Emp_Code"].ToString();
                Edit_Employee_Details(empcode, Creation_Company);
                Edit_Employee_Education_Cer_Details(empcode, Creation_Company);
                Edit_Employee_Education_Skill_Details(empcode, Creation_Company);
                Edit_Employee_Family_Details(empcode, Creation_Company);

                Edit_Employee_Official_Details(empcode, Creation_Company);
                Edit_Employee_Professional_Details(empcode, Creation_Company);
            }
            else
            {

                //fupUserImage.Attributes["onchange"] = "ImageUpload(this)";
                FupLicense.Attributes["onchange"] = "UploadFile(this)";
                FupAadhar.Attributes["onchange"] = "UploadFile1(this)";
                FupPanNo.Attributes["onchange"] = "UploadFile2(this)";
                FupPassport.Attributes["onchange"] = "UploadFile3(this)";
                if (!IsPostBack)
                {
                    Bind_Circle();
                    Bind_Distric();
                    //Bind_divisions();
                    //Bind_range();
                    //Bind_section();
                    //Bind_beat();
                    if (Session["ClientRegistrationNo"] == null)
                    {
                        Response.Redirect("~/Default.aspx");
                    }


                    OTList();
                    //MyTabs.ActiveTab = tabOne;
                    //tabOne.CssClass = "ajax__tab_active";
                    // MyTabs.Visible = true;


                    AuthorityBinding();
                    // City DropDownList Binding
                    CitiesBind();
                    // Dept DropDownList Binding
                    DeptsBind();
                    // Group DropDownList Binding
                    GroupsBind();
                    // Job title DropDownList Binding

                    //Set initial Family Detils Grid Display
                    SetInitialRow();
                    //Binding bank Names

                    //Binding Recruit names
                    RecruitsBind();
                    //SET INITIAL EDUCATION
                    SetInitialRowEducation();

                    //Set Initial Skill
                    SetInitialRowEducationSkill();

                    //set initial Certifications
                    SetInitialRowCertifications();

                    //Deduction
                    SetInitialRowDeduction();
                    //other Deduction


                    // Proffessional


                    //Leavs


                    // // // Bind Location Names
                    BindLocationDetails();

                    /// Key Details

                    //SetInitialRowKey();

                    //Editing Employee Details..

                    if ((string)Session["EmployeeDetailsCode"] != null)
                    {
                        //  Request.QueryString.Clear();
                        string empcode = (string)Session["EmployeeDetailsCode"];
                        Session["EmployeeFamilyDetailsCode"] = empcode;
                        Session["UpdateEmployeeDetailsCode"] = empcode;
                        //string LoginID = Request.QueryString["id"];
                        string ClientRegistrationNo = Session["ClientRegistrationNo"].ToString();
                        Edit_Employee_Details(empcode, ClientRegistrationNo);
                    }
                    else
                    {

                    }
                }
            }
        }

        public void Bind_Circle()
        {
            try
            {
                var objHeaderInfo = (from header in ObjDAL.circle_msts
                                     orderby header.circle_name ascending
                                     select new { circle_name = header.circle_name, circle_cd = header.circle_cd }).Distinct().ToList();
                //"(" + header.circle_cd + ")" +
                if (objHeaderInfo.Count > 0)
                {

                    ddlcircle.DataSource = objHeaderInfo;
                    ddlcircle.DataTextField = "circle_name";
                    ddlcircle.DataValueField = "circle_cd";
                    ddlcircle.DataBind();
                    ddlcircle.Items.Insert(0, new ListItem("---Select---", "0"));
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }
        }
        public void Bind_Distric()
        {
            try
            {
                var objHeaderInfo = (from header in ObjDAL.district_msts
                                     orderby header.distric_name ascending
                                     select new { distric_name = header.distric_name, distric_cd = header.distric_cd }).Distinct().ToList();
                if (objHeaderInfo.Count > 0)
                {

                    ddldistrictforestofficers.DataSource = objHeaderInfo;

                    ddldistrictforestofficers.DataTextField = "distric_name";
                    ddldistrictforestofficers.DataValueField = "distric_cd";
                    ddldistrictforestofficers.DataBind();
                    ddldistrictforestofficers.Items.Insert(0, new ListItem("---Select---", "0"));
                }
                else
                {

                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }

        }
        public void Bind_divisions()
        {
            try
            {
                var objHeaderInfo = (from header in ObjDAL.division_msts
                                     where header.circle_cd == ddlcircle.SelectedValue
                                     orderby header.div_name ascending
                                     select new { div_name = header.div_name, circle_cd = header.circle_cd }).Distinct().ToList();
                if (objHeaderInfo.Count > 0)
                {

                    ddlforestdivisions.DataSource = objHeaderInfo;

                    ddlforestdivisions.DataTextField = "div_name";
                    ddlforestdivisions.DataValueField = "circle_cd";
                    ddlforestdivisions.DataBind();
                    ddlforestdivisions.Items.Insert(0, new ListItem("---Select---", "0"));
                }
                else
                {

                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }

        }
        public void Bind_range()
        {
            try
            {
                var objHeaderInfo = (from header in ObjDAL.sp_Divides(ddlforestdivisions.SelectedValue)
                                     select header).Distinct().ToList();
                //new { range_name = header.range_name, div_cd = header.div_cd }
                if (objHeaderInfo.Count > 0)
                {

                    ddlrange.DataSource = objHeaderInfo;

                    ddlrange.DataTextField = "range_name";
                    ddlrange.DataValueField = "div_cd";
                    ddlrange.DataBind();
                    ddlrange.Items.Insert(0, new ListItem("---Select---", "0"));
                }
                else
                {

                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }

        }
        public void Bind_section()
        {
            try
            {


                var objHeaderInfo = (from header in ObjDAL.sp_Sections(ddlrange.SelectedValue)

                                     select header).Distinct().ToList();
                //new { section_name = header.section_name, range_cd = header.range_cd }
                if (objHeaderInfo.Count > 0)
                {

                    ddlsection.DataSource = objHeaderInfo;

                    ddlsection.DataTextField = "section_name";
                    ddlsection.DataValueField = "range_cd";
                    ddlsection.DataBind();
                    ddlsection.Items.Insert(0, new ListItem("---Select---", "0"));
                }
                else
                {

                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }

        }
        public void Bind_beat()
        {
            try
            {


                var objHeaderInfo = (from header in ObjDAL.sp_beats(ddlsection.SelectedValue)

                                     select header).Distinct().ToList();
                //new { beat_name = header.beat_name, section_cd = header.section_cd }
                if (objHeaderInfo.Count > 0)
                {

                    ddlbeat.DataSource = objHeaderInfo;

                    ddlbeat.DataTextField = "beat_name";
                    ddlbeat.DataValueField = "section_cd";
                    ddlbeat.DataBind();
                    ddlbeat.Items.Insert(0, new ListItem("---Select---", "0"));
                }
                else
                {

                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }

        }
        //private void EmployeeLoginStatusMethod()
        //{
        //    btnAddCity.Visible = false;
        //    btnAddReset.Visible = false;
        //    btnAddressDetails.Visible = false;
        //    btnDepartment.Visible = false;
        //    btnDepartRest.Visible = false;
        //    btnDepartSave.Visible = false;
        //    btnDeptDetails.Visible = false;
        //    btnEMPGrade.Visible = false;
        //    btnGrade.Visible = false;
        //    btnGroupReset.Visible = false;
        //    btnGroupSave.Visible = false;
        //    btnJob.Visible = false;
        //    btnJobTitle.Visible = false;
        //    btnJobTitleReset.Visible = false;
        //    btnJobTitleSave.Visible = false;
        //    btnLocation.Visible = false;
        //    btnLocationPop.Visible = false;
        //    //btnLocationReset.Visible = false;
        //    //btnLocationSave.Visible = false;
        //    btnNewAddDetails.Visible = false;
        //    btnRecruit.Visible = false;
        //    btnRecruitDetails.Visible = false;
        //    btnRecruitRest.Visible = false;
        //    btnRecruitSave.Visible = false;

        //}

        String StrDate = "";
        // // Show the date format dd-mm-yy in sale Order Grids
        public string GetDate(object Dt)
        {

            StrDate = string.Format("{0:dd-MM-yyyy}", Dt);
            return StrDate;
        }

        /// <summary>
        /// Editing Employee Details based on EmpCode in EmployeeList
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        public void Edit_Employee_Details(string empcode, string Creation_Company)
        {
            try
            {

                List<HRMS_Employee_Master_PersonalDetail> objEmpDetails = objEmpBAL.Get_Employee_Details(empcode, Creation_Company);

                if (objEmpDetails.Count > 0)
                {
                    txtEmp_Code.Text = objEmpDetails[0].Emp_Code;
                    txtFirstName.Text = objEmpDetails[0].Emp_First_Name;
                    txtMiddleName.Text = objEmpDetails[0].Emp_Middle_Name;
                    txtLastName.Text = objEmpDetails[0].Emp_Last_Name;
                    ddlUSerRole.SelectedValue = objEmpDetails[0].Emp_Roll;
                    txtUsername.Text = objEmpDetails[0].User_Name;
                    if (objEmpDetails[0].Emp_Image != "")
                    {
                        userImg.ImageUrl = objEmpDetails[0].Emp_Image;
                        Session["DbImagePath"] = objEmpDetails[0].Emp_Image;
                    }
                    else
                    {
                        userImg.ImageUrl = "";
                    }
                    if (objEmpDetails[0].PresentAsPrev != null)
                    {
                        if (objEmpDetails[0].PresentAsPrev.Trim().ToLower() == "true")
                        {
                            chbPermentAddress.Checked = true;
                        }
                        else
                        {
                            chbPermentAddress.Checked = false;
                        }
                    }
                    txtPassword.TextMode = TextBoxMode.SingleLine;
                    txtPassword.Text = objEmpDetails[0].Password;
                    ddlEmpStatus.SelectedValue = objEmpDetails[0].Emp_Status;

                    if (ddlEmpStatus.SelectedValue == "Resigned" || ddlEmpStatus.SelectedValue == "Retired")
                    {
                        lblResignedDate.Text = ddlEmpStatus.SelectedValue + " Date";
                        ResignedDate.Visible = true;
                        txtRetiredDate.Text = GetDate(objEmpDetails[0].Resigned_Date);
                    }

                    txtDateOfBirth.Text = GetDate(objEmpDetails[0].Emp_DOB);
                    txtAge.Text = objEmpDetails[0].Emp_Age.ToString();

                    if (objEmpDetails[0].Gender.ToLower().Trim() == "male")
                    {
                        rblGender.SelectedIndex = 1;

                    }
                    if (objEmpDetails[0].Gender.ToLower().Trim() == "female")
                    { rblGender.SelectedIndex = 0; }
                    txtBloodGroup.Text = objEmpDetails[0].Blood_Group;
                    if (objEmpDetails[0].Blood_Donor.ToLower().Trim() == "true")

                        ddlMaritalStatus.SelectedValue = objEmpDetails[0].Marital_Status;
                    ddlNationality.SelectedValue = objEmpDetails[0].Nationality;
                    ddlReligion.SelectedValue = objEmpDetails[0].Religion;
                    txtLanguagesKnown.Text = objEmpDetails[0].Languages_Known;

                    txtAddress1.Text = objEmpDetails[0].Present_Address1;
                    txtAddress2.Text = objEmpDetails[0].Present_Address2;
                    ddlCity.SelectedValue = objEmpDetails[0].Present_City;
                    txtState.Text = objEmpDetails[0].Present_State;
                    txtPostalCode.Text = objEmpDetails[0].Present_Pincode;
                    txtCountry.Text = objEmpDetails[0].Present_Country;
                    txtPAddress1.Text = objEmpDetails[0].Permanent_Address1;
                    txtPaddress2.Text = objEmpDetails[0].Permanent_Address2;
                    ddlPCity.SelectedValue = objEmpDetails[0].Permanent_City;
                    txtPSate.Text = objEmpDetails[0].Permanent_State;
                    txtPPalost.Text = objEmpDetails[0].Permanent_Pincode;
                    txtpCountry.Text = objEmpDetails[0].Permanent_Country;
                    //txtTelephone.Text= objEmpDetails[0].
                    txtMobile.Text = objEmpDetails[0].Permanent_Mobile;
                    txtEmail.Text = objEmpDetails[0].Permanent_Email;
                    txtAlternativeEmail.Text = objEmpDetails[0].Alternative_Email;
                    txtSkypeID.Text = objEmpDetails[0].Skype_Id;
                    txtLinkedin.Text = objEmpDetails[0].Linkedin_Id;
                    txtDriving_License_Number.Text = objEmpDetails[0].Driving_Licenece_Number;
                    ddlType.SelectedValue = objEmpDetails[0].Licence_Type;
                    txtLicenseExpiryDate.Text = (GetDate(objEmpDetails[0].Licence_Exp_Date) == "01-01-2001") ? "" : GetDate(objEmpDetails[0].Licence_Exp_Date);
                    txtAadharNumber.Text = objEmpDetails[0].Aadhar_Number;
                    txtpPANNo.Text = objEmpDetails[0].Pan_Number;
                    txtPassportNumber.Text = objEmpDetails[0].Passport_Number;
                    txtIssuedDate.Text = (GetDate(objEmpDetails[0].Issued_Date) == "01-01-2001") ? "" : GetDate(objEmpDetails[0].Issued_Date);//GetDate(objEmpDetails[0].Issued_Date);
                    txtExpiryDate.Text = (GetDate(objEmpDetails[0].Exp_Date) == "01-01-2001") ? "" : GetDate(objEmpDetails[0].Exp_Date);//GetDate(objEmpDetails[0].Exp_Date);
                    txtRetiredDate.Text = (GetDate(objEmpDetails[0].Resigned_Date) == "01-01-2001") ? "" : GetDate(objEmpDetails[0].Exp_Date);
                    ddlPBankName.SelectedValue = objEmpDetails[0].Bank_Name;
                    txtPAccountNumber.Text = objEmpDetails[0].Account_Number;
                    txtPHealth.Text = objEmpDetails[0].Health_Insurance_Poly_No;
                    txtSumInsured.Text = objEmpDetails[0].Sum_Insured;
                    txtRenewalDate.Text = (GetDate(objEmpDetails[0].Renewal_Date) == "01-01-2001") ? "" : GetDate(objEmpDetails[0].Renewal_Date);//GetDate(objEmpDetails[0].Renewal_Date);


                    //ddlYear.SelectedValue = objHolidayDetails[0].Year.ToString();
                    //txtDate.Text = GetDate(objHolidayDetails[0].Date).ToString();
                    //txtEventName.Text = objHolidayDetails[0].Event_Name.ToString();
                    //btnUpdate.Visible = true;
                    //btnSave.Visible = false;
                    if (Session["User_Type"].ToString() == "Employees")
                    {
                        //
                    }
                    else
                    {

                        Session["EmployeeDetailsCode"] = null;
                        Session["EmployeeFamilyDetailsCode"] = txtEmp_Code.Text;
                    }
                }
                else
                {
                    //ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('No Data Found ');", true);

                    if (Session["User_Type"].ToString() == "Employees")
                    {
                        //
                    }
                    else
                    {

                    }
                }

            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }

        /// <summary>
        /// Pop up Display for City 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddCity_Click(object sender, EventArgs e)
        {

            txtCityName.Focus();
        }


        /// <summary>
        /// Pop up Display for Job Title 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnJobTitle_Click(object sender, EventArgs e)
        {

            txtJobTitleName.Focus();
        }

        /// <summary>
        /// Pop up Display for Group 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnEMPGrade_Click(object sender, EventArgs e)
        {

            txtHeadName.Focus();
        }

        /// <summary>
        /// Popup Adding New Location
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnLocation_Click(object sender, EventArgs e)
        {
            // this.MPLocation.Show();
            txtLocationName.Focus();
            //this.MPGrade.Show();
            //txtHeadName.Focus();
        }
        /// <summary>
        /// Pop up Display for Department 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDepartment_Click(object sender, EventArgs e)
        {
            // this.MPDept.Show();
            txtDepartName.Focus();
        }

        /// <summary>
        /// Pop up Display for Reporting 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnReporting_Click(object sender, EventArgs e)
        {
            // this.MPJob.Show();
            txtJobTitleName.Focus();
        }


        /// <summary>
        /// Adding City Name to City DropDown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNewAddDetails_Click(object sender, EventArgs e)
        {
            if (txtCityName.Text != "" && ddlState.SelectedValue != "Select")
            {
                String Creation_Company = Session["ClientRegistrationNo"].ToString();
                string cityname = txtCityName.Text;
                List<HRMS_City_Master> ObjGroup = (from master in ObjDAL.HRMS_City_Masters
                                                   where master.Creation_Company == Creation_Company &&
                                                         master.City_Name.ToLower().Trim() == cityname.ToLower().Trim()
                                                   select master).ToList();
                if (ObjGroup.Count > 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Enter City Name Already Exist..');", true);

                    txtCityName.Text = "";
                    ddlState.SelectedValue = "Select";
                    txtCityName.Focus();
                }
                else
                {
                    CityDetailsSave();
                    if (ddlCity.Items.FindByValue(txtCityName.Text) == null && ddlPCity.Items.FindByValue(txtCityName.Text) == null)
                    {
                        ddlCity.Items.Insert(1, new ListItem(txtCityName.Text, txtCityName.Text));
                        ddlPCity.Items.Insert(1, new ListItem(txtCityName.Text, txtCityName.Text));
                        txtCityName.Text = "";
                        ddlState.SelectedValue = "Select";
                        ddlCountry.SelectedValue = "Select";
                        //this.mp1.Show();
                        txtCityName.Focus();
                    }
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Plz Enter Valid Data..');", true);
                // this.mp1.Show();
                txtCityName.Focus();
            }
        }

        #region Location Popup Details

        // // // Checking For Location Names Duplicate
        public void txtLocationName_OnTextChanged(object sender, EventArgs e)
        {

        }

        // // // This Event Is Used For Location Names Saving
        public void btnLocationSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["User_Type"].ToString() == "Employees")
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('You Don't Have Permission To Create ..');", true);
                }
                else
                {
                    if (txtLocationName.Text != "")
                    {
                        // // // This Method Is used For Saving
                        LocationDetailsSave();
                        // // // This Method is used For Binding The Location Names
                        BindLocationDetails();
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Plz Enter Valid Data..');", true);
                        //  this.MPLocation.Show();
                        txtLocationName.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {
            }
        }

        // // // The Following Methos Is Used For Location Details Saving
        public void LocationDetailsSave()
        {
            try
            {
                ObjEmpLocation.Location_Id = txtLocationId.Text;
                ObjEmpLocation.Location_Name = txtLocationName.Text;
                ObjEmpLocation.Created_By = Session["User_Name"].ToString();
                ObjEmpLocation.Created_Date = System.DateTime.Now;
                ObjEmpLocation.Creation_Company = Session["ClientRegistrationNo"].ToString();
                ObjEmpLocation.Modified_By = Session["User_Name"].ToString();
                ObjEmpLocation.Modified_Date = System.DateTime.Now;
                ObjEmpLocation.Parameter = 1;
                if (ObjEmpLocation.Insert_EmpLocations() != 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);
                    //this.MPLocation.Show();
                    txtLocationId.Text = "";
                    txtLocationName.Text = "";
                    txtLocationName.Focus();
                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }

        // /// The Following Method is Used For BindThe Location Names

        public void BindLocationDetails()
        {
            try
            {
                String Creation_Company = Session["ClientRegistrationNo"].ToString();
                List<HRMS_Employee_Work_Location> objGroup = ObjEmpLocation.GetData_LocationMaster(Creation_Company);

                if (objGroup.Count > 0)
                {
                    ddlLocation.DataSource = objGroup;
                    ddlLocation.DataTextField = "Location_Name";
                    ddlLocation.DataValueField = "Location_Id";
                    ddlLocation.DataBind();
                    ddlLocation.Items.Insert(0, new ListItem("Select", "Select")); //updated code
                }
                else
                {
                    ddlLocation.Items.Insert(0, new ListItem("Select", "Select")); //updated code
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            {

            }
        }
        // // // This Event Is used For Reset The All Fields in Location Popup
        public void btnLocationReset_Click(object sender, EventArgs e)
        {

        }
        //  
        #endregion

        #region Department Popup Details
        /// <summary>
        /// duplicate checking for Dept ID
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtDepTID_OnTextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtDeptID.Text != "")
                {
                    String Creation_Company = Session["ClientRegistrationNo"].ToString();
                    string deptid = txtDeptID.Text;
                    List<HRMS_Department_Master> ObjGroup = (from master in ObjDAL.HRMS_Department_Masters
                                                             where master.Creation_Company == Creation_Company &&
                                                             master.Department_Code.ToLower().Trim() == deptid.ToLower().Trim()
                                                             select master).ToList();
                    if (ObjGroup.Count > 0)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Department ID Already Exist..');", true);
                        // this.MPDept.Show();
                        txtDeptID.Text = "";
                        txtDeptID.Focus();
                    }
                    else
                    {
                        // this.MPDept.Show();
                        txtDepartName.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }

        /// <summary>
        /// duplicate checking for Dept Name
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtDepartName_OnTextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtDepartName.Text != "")
                {
                    String Creation_Company = Session["ClientRegistrationNo"].ToString();
                    string deptname = txtDepartName.Text;
                    List<HRMS_Department_Master> ObjGroup = (from master in ObjDAL.HRMS_Department_Masters
                                                             where master.Creation_Company == Creation_Company &&
                                                             master.Department_Name.ToLower().Trim() == deptname.ToLower().Trim()
                                                             select master).ToList();
                    if (ObjGroup.Count > 0)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Department Name Already Exist..');", true);
                        //  this.MPDept.Show();
                        txtDepartName.Text = "";
                        txtDepartName.Focus();
                    }
                    else
                    {
                        // this.MPDept.Show();
                        btnDepartSave.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }
        /// <summary>
        /// Adding Department Names and Checking Duplication 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDepartSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtDepartName.Text != "" && txtDeptID.Text != "")
                {

                    DepartmentDetailsSave();

                    if (ddlDepartment.Items.FindByValue(txtDepartName.Text) == null)
                    {
                        ddlDepartment.Items.Insert(1, new ListItem(txtDepartName.Text, txtDepartName.Text));
                        txtDepartName.Text = "";
                        ddlDepartment.SelectedValue = "Select";
                        // this.MPDept.Show();
                        txtDepartName.Focus();
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Plz Enter Valid Data..');", true);
                    // this.MPDept.Show();
                    txtDepartName.Focus();
                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }


        /// <summary>
        /// Inserting Department To Department_Master Table
        /// </summary>

        public void DepartmentDetailsSave()
        {
            try
            {
                objDeptBAL.Department_Code = txtDeptID.Text;
                objDeptBAL.Department_Name = txtDepartName.Text;
                objDeptBAL.Created_By = Session["User_Name"].ToString();
                objDeptBAL.Created_Date = System.DateTime.Now;
                objDeptBAL.Creation_Company = Session["ClientRegistrationNo"].ToString();
                objDeptBAL.Modified_By = Session["User_Name"].ToString();
                objDeptBAL.Modified_Date = System.DateTime.Now;
                objDeptBAL.Parameter = 1;
                if (objDeptBAL.Insert_Department_Master() != 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);
                    // this.MPDept.Show();
                    txtDeptID.Text = "";
                    txtDepartName.Text = "";
                    txtDeptID.Focus();
                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }

        #endregion

        #region Recruit popup details


        #endregion

        #region Employee Group Popup Details

        /// <summary>
        /// Adding Group Names
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnGroupSave_Click(object sender, EventArgs e)
        {
            try
            {


                if (txtHeadName.Text != "" && txtShortName.Text != "")
                {

                    GroupDetailsSave();

                    if (ddlEMPGrade.Items.FindByValue(txtHeadName.Text) == null)
                    {
                        ddlEMPGrade.Items.Insert(1, new ListItem(txtHeadName.Text, txtHeadName.Text));
                        txtHeadName.Text = "";
                        ddlEMPGrade.SelectedValue = "Select";
                        //
                        txtHeadName.Focus();

                    }



                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Plz Enter Valid Data..');", true);
                    //  
                    txtHeadName.Focus();
                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }

        // Group name text changed event
        protected void txtHeadName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtHeadName.Text != "")
                {
                    String Creation_Company = Session["ClientRegistrationNo"].ToString();
                    string groupname = txtHeadName.Text;
                    List<HRMS_Group_Master> ObjGroup = (from master in ObjDAL.HRMS_Group_Masters
                                                        where master.Creation_Company == Creation_Company &&
                                                             master.Group_Name.ToLower().Trim() == groupname.ToLower().Trim()
                                                        select master).ToList();
                    if (ObjGroup.Count > 0)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Group Name Already Exist..');", true);

                        txtHeadName.Text = "";
                        txtHeadName.Focus();
                    }
                    else
                    {
                        //
                        txtShortName.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }

        }

        // Short name text changed event
        protected void txtShortName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtShortName.Text != "")
                {
                    String Creation_Company = Session["ClientRegistrationNo"].ToString();
                    string shortname = txtShortName.Text;
                    List<HRMS_Group_Master> ObjGroup = (from master in ObjDAL.HRMS_Group_Masters
                                                        where master.Creation_Company == Creation_Company &&
                                                             master.Short_Name.ToLower().Trim() == shortname.ToLower().Trim()
                                                        select master).ToList();
                    if (ObjGroup.Count > 0)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Short Name Already Exist..');", true);
                        //
                        txtShortName.Text = "";
                        txtShortName.Focus();
                    }
                    else
                    {
                        //
                        ddlGroup.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }

        }

        /// <summary>
        /// ddlEMPGrade_SelectedIndexChanged event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void ddlEMPGrade_SelectedIndexChanged(object sender, EventArgs e)
        {
            string groupname = ddlEMPGrade.SelectedValue;
            string creation_Company = Session["ClientRegistrationNo"].ToString();

            List<HRMS_Group_Master> row = (from grup in ObjDAL.HRMS_Group_Masters
                                           where grup.Group_Name == groupname &&
                                           grup.Creation_Company == creation_Company
                                           select grup).ToList();
            if (row.Count > 0)
            {
                string[] heads = row[0].Applicable_Heads.Split(',');



                DataTable dt = new DataTable();

                DataRow dr = null;

                dt.Columns.Add(new DataColumn("Head", typeof(string)));
                dt.Columns.Add(new DataColumn("Amount", typeof(string)));
                foreach (string head in heads)
                {
                    List<HRMS_Additions_Master> rowaddition = (from addtn in ObjDAL.HRMS_Additions_Masters
                                                               where addtn.Short_Name == head && addtn.Creation_Company
                                                               == creation_Company
                                                               select addtn).ToList();

                    if (rowaddition.Count > 0)
                    {
                        dr = dt.NewRow();
                        dr["Head"] = rowaddition[0].Short_Name.ToString();
                        dr["Amount"] = rowaddition[0].Per_Of_Basic.ToString();//(rowaddition[0].Per_Of_Basic.ToString()=="0.00")?" ":
                        dt.Rows.Add(dr);
                    }
                    else
                    {
                        dr = dt.NewRow();
                        dr["Head"] = head;
                        dr["Amount"] = "";
                        dt.Rows.Add(dr);
                    }

                }

                grvSalary.DataSource = dt;
                grvSalary.DataBind();
            }
            else
            { SetInitialRowDeduction(); }
        }



        /// <summary>
        /// Inserting Group Name To Group_Master Table
        /// </summary>

        public void GroupDetailsSave()
        {
            try
            {
                string heads = (string)ViewState["Applicable_Heads"];
                objEmpGroupBAL.Group_Code = "";
                objEmpGroupBAL.Group_Name = txtHeadName.Text;
                objEmpGroupBAL.Short_Name = txtShortName.Text;
                objEmpGroupBAL.Group_type = ddlGroup.SelectedValue;
                objEmpGroupBAL.Applicable_Heads = heads;
                objEmpGroupBAL.Created_By = Session["User_Name"].ToString();
                objEmpGroupBAL.Created_Date = System.DateTime.Now;
                objEmpGroupBAL.Creation_Company = Session["ClientRegistrationNo"].ToString();
                objEmpGroupBAL.Modified_By = Session["User_Name"].ToString();
                objEmpGroupBAL.Modified_Date = System.DateTime.Now;
                objEmpGroupBAL.Parameter = 1;
                if (objEmpGroupBAL.Insert_Group_Master() != 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);
                    GroupMasterClearFields();

                }
                else
                { }
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }
            finally
            { }
        }

        /// <summary>
        /// Clear Employee Group Details
        /// </summary>
        private void GroupMasterClearFields()
        {
            txtID.Text = "";
            txtHeadName.Text = "";
            txtShortName.Text = "";
            ddlGroup.SelectedIndex = 0;
            for (int items = 0; items < chlHeadsLists.Items.Count; items++)
            {
                chlHeadsLists.ClearSelection();
            }
        }

        #endregion
        //txtBasicAmount_TextChanged Basic Amount Textbox event
        public void txtBasicAmount_TextChanged(object sender, EventArgs e)
        {
            double i = 0.034;

            double j = 0.3;
            double k = 0.2;
            double a = 0.12;
            double b = 0.0475;
            double c = 0.0175;
            double da = (Math.Round(Convert.ToDouble(txtBasicAmount.Text) * Convert.ToDouble(i)));
            //Convert.ToDouble(txtda.Text) = (Convert.ToDouble(txtBasicAmount.Text)) * 3.4"%");
            txtda.Text = da.ToString();
            double hra = (Math.Round(Convert.ToDouble(txtBasicAmount.Text) * Convert.ToDouble(j)));
            txthra.Text = hra.ToString();
            double cca = (Math.Round(Convert.ToDouble(txtBasicAmount.Text) * Convert.ToDouble(k)));
            txtcca.Text = cca.ToString();
            double ga = Math.Round(Convert.ToDouble(txtBasicAmount.Text) + Convert.ToDouble(txtda.Text) + Convert.ToDouble(txthra.Text) + Convert.ToDouble(txtcca.Text));
            txtGrossAmount.Text = ga.ToString();
            double pf = Math.Round((Convert.ToDouble(txtBasicAmount.Text) + Convert.ToDouble(txtda.Text)) * Convert.ToDouble(a));
            txtpfamount.Text = pf.ToString();
            double eesi = Math.Round((Convert.ToDouble(ga)) * Convert.ToDouble(b));
            txteesi.Text = eesi.ToString();
            double esi = Math.Round((Convert.ToDouble(ga)) * Convert.ToDouble(c));
            txtesiamount.Text = eesi.ToString();


            double total1;
            double TD;
            double x = 0.4;
            double y = 1600;
            total1 = (Math.Round(Convert.ToDouble(txtBasicAmount.Text) * Convert.ToDouble(x)) + Convert.ToDouble(y));
            TD = (ga - total1) * 12;
            if (TD >= 0 && TD <= 250000)
            {
                txttds.Text = "0.00";
            }
            else if (TD >= 250001 && TD <= 500000)
            {
                double p = 0.1;
                double tds1 = (Math.Round(Convert.ToDouble(TD) * Convert.ToDouble(p)));
                txttds.Text = tds1.ToString();
            }
            else if (TD >= 500001 && TD <= 1000000)
            {
                double q = 0.2;
                double tds2 = (Math.Round(Convert.ToDouble(TD) * Convert.ToDouble(q)));
                txttds.Text = tds2.ToString();
            }
            else if (TD >= 1000001)
            {
                double r = 0.3;
                double tds3 = (Math.Round(Convert.ToDouble(TD) * Convert.ToDouble(r)));
                txttds.Text = tds3.ToString();
            }

        }
        public void txtcca_TextChanged(object sender, EventArgs e)
        {

            //double tb = Math.Round(Convert.ToDouble(txtGrossAmount.Text) + Convert.ToDouble(txtcca.Text));
            //txtGrossAmount.Text = tb.ToString();

        }
        //grvSalary Grid Txtbox Changed Event
        public void txtAmount_TextChanged(object sender, EventArgs e)
        {
            Grid_Footer_Calc();
        }

        //Footer Calculations
        public void Grid_Footer_Calc()
        {
            decimal totalamount = 0;
            string basicamount = txtBasicAmount.Text;
            foreach (GridViewRow objrow in grvSalary.Rows)
            {
                TextBox txtAmount = (TextBox)objrow.FindControl("txtAmount");
                decimal PresentActualAmount = (txtAmount.Text == "") ? Convert.ToDecimal("00") : Convert.ToDecimal(txtAmount.Text);
                totalamount = totalamount + PresentActualAmount;
            }
            Label lbltotalAmount = ((Label)grvSalary.FooterRow.FindControl("lbltotalAmount"));
            lbltotalAmount.Text = totalamount.ToString();
            txtGrossAmount.Text = (totalamount + Convert.ToDecimal(basicamount)).ToString();
        }

        /// <summary>
        /// Adding Job Title / Designation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void btnJobTitleSave_Click(object sender, EventArgs e)
        //{
        //    if (txtJobTitleName.Text != "" && txtJobID.Text != "")
        //    {

        //        JobTypeDetailsSave();
        //        if (ddlJobTitle.Items.FindByValue(txtJobTitleName.Text) == null)
        //        {
        //            ddlJobTitle.Items.Insert(1, new ListItem(txtJobTitleName.Text, txtJobTitleName.Text));
        //            txtJobTitleName.Text = "";
        //            //ddlJobTitle.SelectedValue = "Select";
        //          //  this.MPJob.Show();
        //            txtJobTitleName.Focus();
        //        }



        //    }
        //    else
        //    {
        //        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Plz Enter Valid Data..');", true);
        //      //  this.MPJob.Show();
        //        txtJobTitleName.Focus();
        //    }
        //}

        //Jobid textchange event in official information
        protected void txtJobID_TextChanged(object sender, EventArgs e)
        {
            if (txtJobID.Text != "")
            {
                String Creation_Company = Session["ClientRegistrationNo"].ToString();
                string jobid = txtJobID.Text;
                List<HRMS_JobType_Master> ObjGroup = (from master in ObjDAL.HRMS_JobType_Masters
                                                      where master.Creation_Company == Creation_Company &&
                                                      master.Job_Id.ToLower().Trim() == jobid.ToLower().Trim()
                                                      select master).ToList();
                if (ObjGroup.Count > 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Job ID Already Exist..');", true);
                    // this.MPJob.Show();
                    txtJobID.Text = "";
                    txtJobID.Focus();

                }
                else
                {
                    // this.MPJob.Show();
                    txtJobTitleName.Focus();
                }
            }
        }

        //JobName textchange event in official information
        protected void txtJobTitleName_TextChanged(object sender, EventArgs e)
        {
            if (txtJobTitleName.Text != "")
            {
                String Creation_Company = Session["ClientRegistrationNo"].ToString();
                string JobTitleName = txtJobTitleName.Text;
                List<HRMS_JobType_Master> ObjGroup = (from master in ObjDAL.HRMS_JobType_Masters
                                                      where master.Creation_Company == Creation_Company &&
                                                      master.Job_Name.ToLower().Trim() == JobTitleName.ToLower().Trim()
                                                      select master).ToList();
                if (ObjGroup.Count > 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Job Name Already Exist..');", true);
                    // this.MPJob.Show();
                    txtJobTitleName.Text = "";
                    txtJobTitleName.Focus();
                    //  this.MPJob.Show();
                }
                else
                {
                    //  this.MPJob.Show();
                    btnJobTitleSave.Focus();
                }
            }
        }


        /// <summary>
        /// Clearing City Name TextBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddReset_Click(object sender, EventArgs e)
        {
            //  this.mp1.Show();
            txtCityName.Text = "";
            ddlState.SelectedValue = "Select State";
            txtCityName.Focus();
        }

        /// <summary>
        /// Clearing Department Name TextBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDepartRest_Click(object sender, EventArgs e)
        {
            // this.MPDept.Show();
            txtDepartName.Text = "";
            txtDepartName.Focus();
        }

        /// <summary>
        /// Clearing Job title TextBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnJobTypeRest_Click(object sender, EventArgs e)
        {
            // this.MPJob.Show();
            txtJobTitleName.Text = "";
            txtJobTitleName.Focus();
        }

        /// <summary>
        /// Clearing Group Name TextBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnGroupRest_Click(object sender, EventArgs e)
        {
            //
            GroupMasterClearFields();
        }

        /// <summary>
        /// Bind State Based On City
        /// </summary>
        protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            string city = ddlCity.SelectedValue;
            string creationCompany = Session["ClientRegistrationNo"].ToString();
            if (city != "Select")
            {
                List<HRMS_City_Master> objrow = (from state in ObjDAL.HRMS_City_Masters
                                                 where state.Creation_Company == creationCompany && state.City_Name == city
                                                 select state).ToList();
                if (objrow.Count > 0)
                {
                    txtState.Text = objrow[0].State_Name;
                    txtCountry.Text = objrow[0].Country;
                    txtPostalCode.Focus();
                }
                else
                {
                    txtState.Text = "";
                    txtCountry.Text = "";
                    ddlCity.Focus();
                }
            }
            else
            {
                txtState.Text = "";
                txtCountry.Text = "";
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Plz Select City..');", true);
                ddlCity.Focus();
            }
        }

        protected void ddlPCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            string city = ddlPCity.SelectedValue;
            string creationCompany = Session["ClientRegistrationNo"].ToString();
            if (city != "Select")
            {
                List<HRMS_City_Master> objrow = (from state in ObjDAL.HRMS_City_Masters
                                                 where state.Creation_Company == creationCompany && state.City_Name == city
                                                 select state).ToList();
                if (objrow.Count > 0)
                {
                    txtPSate.Text = objrow[0].State_Name;
                    txtpCountry.Text = objrow[0].Country;
                }
                else
                {
                    txtPSate.Text = "";
                    txtpCountry.Text = "";
                }
            }
            else
            {
                txtPSate.Text = "";
                txtpCountry.Text = "";
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Plz Select City..');", true);
                ddlPCity.Focus();

            }
        }


        #region Recruit Details

        #region Event
        /// <summary>
        /// Recruit Popup Functionality
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRecruit_Click(object sender, EventArgs e)
        {
            //this.mpRecruit.Show();
            txtRecruitNo.Focus();
        }

        // Recruit No text changed event
        protected void txtRecruitNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtRecruitNo.Text != "")
                {
                    String Creation_Company = Session["ClientRegistrationNo"].ToString();
                    string recruitno = txtRecruitNo.Text;
                    List<HRMS_Recruit_Master> ObjGroup = (from master in ObjDAL.HRMS_Recruit_Masters
                                                          where master.Creation_Company == Creation_Company &&
                                                             master.Recruit_Code.ToLower().Trim() == recruitno.ToLower().Trim()
                                                          select master).ToList();
                    if (ObjGroup.Count > 0)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Recruit No  Already Exist..');", true);
                        // this.mpRecruit.Show();
                        txtRecruitNo.Text = "";
                        txtRecruitNo.Focus();
                    }
                    else
                    {
                        // this.mpRecruit.Show();
                        txtRecruitName.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }

        // Recruit Name text changed event
        protected void txtRecruitName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtRecruitName.Text != "")
                {
                    String Creation_Company = Session["ClientRegistrationNo"].ToString();
                    string recruitname = txtRecruitName.Text;
                    List<HRMS_Recruit_Master> ObjGroup = (from master in ObjDAL.HRMS_Recruit_Masters
                                                          where master.Creation_Company == Creation_Company &&
                                                             master.Recruit_Name.ToLower().Trim() == recruitname.ToLower().Trim()
                                                          select master).ToList();
                    if (ObjGroup.Count > 0)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Recruit Name  Already Exist..');", true);
                        // this.mpRecruit.Show();
                        txtRecruitName.Text = "";
                        txtRecruitName.Focus();
                    }
                    else
                    {
                        // this.mpRecruit.Show();
                        btnRecruitSave.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }


        /// <summary>
        /// Save Data Recruit Details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRecruitSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtRecruitNo.Text != "" && txtRecruitName.Text != "")
                {
                    SaveRecruit();
                    if (ddlRecruit.Items.FindByValue(txtDepartName.Text) == null)
                    {
                        ddlRecruit.Items.Insert(1, new ListItem(txtRecruitName.Text, txtRecruitName.Text));
                        txtRecruitName.Text = "";
                        ddlRecruit.SelectedValue = "Select";
                        // this.mpRecruit.Show();
                        txtRecruitName.Focus();
                    }
                }
                else
                {

                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Plz Enter Valid Data..');", true);
                    //  this.mpRecruit.Show();
                    txtRecruitName.Focus();
                }
            }

            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }

        /// <summary>
        /// Resset Data Recruit Details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRecruitRest_Click(object sender, EventArgs e)
        {
            txtRecruitNo.Text = txtRecruitName.Text = "";
            // this.mpRecruit.Show();
            txtRecruitNo.Focus();

        }
        #endregion

        #region Methods
        /// <summary>
        /// Data Save Functionality for Recruit Method
        /// </summary>
        public void SaveRecruit()
        {
            try
            {
                objRecruitBAL.Recruit_Code = txtRecruitNo.Text;
                objRecruitBAL.Recruit_Name = txtRecruitName.Text;
                objRecruitBAL.Created_By = Session["User_Name"].ToString();
                objRecruitBAL.Created_Date = System.DateTime.Now;
                objRecruitBAL.Creation_Company = Session["ClientRegistrationNo"].ToString();
                objRecruitBAL.Modified_By = Session["User_Name"].ToString();
                objRecruitBAL.Modified_Date = System.DateTime.Now;
                objRecruitBAL.Parameter = 1;
                if (objRecruitBAL.Insert_Recruit_Master() != 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);
                    // this.mpRecruit.Show();
                    txtRecruitNo.Text = "";
                    txtRecruitName.Text = "";
                    txtRecruitNo.Focus();
                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }

        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Certifications Details
        /// </summary>
        public void SetInitialRowCertifications()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("Course_Skill_Name", typeof(string)));
            dt.Columns.Add(new DataColumn("Institute", typeof(string)));
            dt.Columns.Add(new DataColumn("Year", typeof(string)));
            dt.Columns.Add(new DataColumn("Attach_Certificate_Copy", typeof(string)));



            dr = dt.NewRow();
            dr["Course_Skill_Name"] = string.Empty;
            dr["Institute"] = string.Empty;
            dr["Attach_Certificate_Copy"] = string.Empty;


            dt.Rows.Add(dr);
            ViewState["CurrentTable"] = dt;
            grvCertification.DataSource = dt;
            grvCertification.DataBind();
        }




        /// <summary>
        /// Deductions
        /// </summary>
        public void SetInitialRowDeduction()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            // dt.Columns.Add(new DataColumn("Id", typeof(int)));
            //  dt.Columns.Add(new DataColumn("Bank_ID", typeof(string)));
            dt.Columns.Add(new DataColumn("Head", typeof(string)));
            dt.Columns.Add(new DataColumn("Amount", typeof(string)));



            dr = dt.NewRow();
            dr["Head"] = string.Empty;
            dr["Amount"] = string.Empty;


            dt.Rows.Add(dr);
            ViewState["CurrentTable"] = dt;
            grvSalary.DataSource = dt;
            grvSalary.DataBind();
        }






        #endregion

        #region Database Methods
        /// <summary>
        /// Inserting CityName And StateName To City_Master Table
        /// </summary>

        public void CityDetailsSave()
        {
            objCityBAL.City_Code = "";
            objCityBAL.City_Name = txtCityName.Text;
            objCityBAL.State_Name = ddlState.SelectedValue;
            objCityBAL.Country = ddlCountry.SelectedValue;
            objCityBAL.Created_By = Session["User_Name"].ToString();
            objCityBAL.Created_Date = System.DateTime.Now;
            objCityBAL.Creation_Company = Session["ClientRegistrationNo"].ToString();
            objCityBAL.Modified_By = Session["User_Name"].ToString();
            objCityBAL.Modified_Date = System.DateTime.Now;
            objCityBAL.Parameter = 1;
            if (objCityBAL.Insert_City_Master() != 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);
                //  this.mp1.Show();
            }
        }




        /// <summary>
        /// Inserting Job Name To JobType_Master Table
        /// </summary>

        public void JobTypeDetailsSave()
        {
            objJobTypeBAL.Job_Id = txtJobID.Text;
            objJobTypeBAL.Job_Name = txtJobTitleName.Text;
            objJobTypeBAL.Created_By = Session["User_Name"].ToString();
            objJobTypeBAL.Created_Date = System.DateTime.Now;
            objJobTypeBAL.Creation_Company = Session["ClientRegistrationNo"].ToString();
            objJobTypeBAL.Modified_By = Session["User_Name"].ToString();
            objJobTypeBAL.Modified_Date = System.DateTime.Now;
            objJobTypeBAL.Parameter = 1;
            if (objJobTypeBAL.Insert_JobType_Master() != 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);
                //this.MPJob.Show();
                txtJobID.Text = "";
                txtJobTitleName.Text = "";
                txtJobID.Focus();
            }
        }

        /// <summary>
        /// Binding CityName to City DropDownList Based On City_Master Table
        /// </summary>

        public void CitiesBind()
        {
            String Creation_Company = Session["ClientRegistrationNo"].ToString();
            List<HRMS_City_Master> objGroup = objCityBAL.GetData_CityMaster(Creation_Company);

            if (objGroup.Count > 0)
            {
                ddlCity.DataSource = objGroup;
                ddlPCity.DataSource = objGroup;
                ddlCity.DataTextField = "City_Name";
                ddlCity.DataValueField = "City_Name";
                ddlPCity.DataTextField = "City_Name";
                ddlPCity.DataValueField = "City_Name";
                ddlCity.DataBind();
                ddlPCity.DataBind();
                ddlCity.Items.Insert(0, new ListItem("Select", "Select"));
                ddlPCity.Items.Insert(0, new ListItem("Select", "Select"));
            }
            else
            {

            }
        }



        // Binding Recruit dropdownlist in offical information
        public void RecruitsBind()
        {
            String Creation_Company = Session["ClientRegistrationNo"].ToString();
            List<HRMS_Recruit_Master> objGroup = objRecruitBAL.GetData_RecruitMaster(Creation_Company);

            if (objGroup.Count > 0)
            {
                ddlRecruit.DataSource = objGroup;
                ddlRecruit.DataTextField = "Recruit_Name";
                ddlRecruit.DataValueField = "Recruit_Name";
                ddlRecruit.DataBind();
                ddlRecruit.Items.Insert(0, new ListItem("Select", "Select"));
            }
            else
            {

            }
        }

        /// <summary>
        /// Binding Department to Department DropDownList Based On Department_Master Table
        /// </summary>

        public void DeptsBind()
        {
            String Creation_Company = Session["ClientRegistrationNo"].ToString();
            List<HRMS_Department_Master> objGroup = objDeptBAL.GetData_DeptMaster(Creation_Company);

            if (objGroup.Count > 0)
            {
                ddlDepartment.DataSource = objGroup;
                ddlDepartment.DataTextField = "Department_Name";
                ddlDepartment.DataValueField = "Department_Name";
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("Select", "Select"));
            }
            else
            {

            }
        }

        /// <summary>
        /// Binding Group to Group DropDownList Based On Group_Master Table
        /// </summary>

        public void GroupsBind()
        {
            String Creation_Company = Session["ClientRegistrationNo"].ToString();
            List<HRMS_Group_Master> objGroup = objEmpGroupBAL.GetData_GroupMaster(Creation_Company);

            if (objGroup.Count > 0)
            {
                ddlEMPGrade.DataSource = objGroup;
                ddlEMPGrade.DataTextField = "Group_Name";
                ddlEMPGrade.DataValueField = "Group_Name";
                ddlEMPGrade.DataBind();
                ddlEMPGrade.Items.Insert(0, new ListItem("Select", "Select"));
            }
            else
            {

            }
        }

        /// <summary>
        /// Binding JobName to Job type DropDownList Based On JobType_Master Table
        /// </summary>

        public void AuthorityBinding()
        {
            var emps = (from emp in ObjDAL.HRMS_Employee_Master_PersonalDetails
                        where emp.Creation_Company == Session["ClientRegistrationNo"].ToString()
                        select new
                        {
                            Emp_name = emp.Emp_First_Name + " " + emp.Emp_Middle_Name + " " + emp.Emp_Last_Name

                        }).Distinct().ToList();
            ddlReportingAuthority.DataSource = emps;
            ddlReportingAuthority.DataTextField = "Emp_name";
            ddlReportingAuthority.DataValueField = "Emp_name";
            ddlReportingAuthority.DataBind();
            ddlReportingAuthority.Items.Insert(0, new ListItem("Select", "Select"));


        }




        #endregion
        #region Personal Details

        //protected void Linkbutton1_Click(object sender, EventArgs e)
        //{
        //    string filename = fupUserImage.PostedFile.FileName;
        //    var FileExtension = Path.GetExtension(fupUserImage.PostedFile.FileName).ToString();
        //    if (fupUserImage.PostedFile != null)
        //    {
        //        if (FileExtension.ToString().ToLower() == ".jpg" || FileExtension.ToString().ToLower() == ".png")
        //        {
        //            string path = Server.MapPath("~/UserImages/");
        //            string fulpath = path + filename;
        //            FupLicense.SaveAs(fulpath);
        //            userImg.ImageUrl = fulpath;
        //            ViewState["UserImagePath"] = fulpath;
        //        }
        //    }
        //}

        //Get Data To Bind UserDetails
        private void GetPersonalData()
        {
            string ClientRegistrationNo = Session["ClientRegistrationNo"].ToString();
            // var query = ObjDAL.HRMS_Employee_Master_PersonalDetails.OrderByDescending(x => x.Emp_Code).FirstOrDefault();
            var query = (from person in ObjDAL.HRMS_Employee_Master_PersonalDetails
                         where person.Creation_Company == ClientRegistrationNo && person.Emp_Code == txtEmp_Code.Text
                         select person).ToList();
            //var itemList = (from t in ctn.Items
            //                where !t.Items && t.DeliverySelection
            //                select t).OrderByDescending();


            //  txtEmp_Code.Text = query.Emp_Code;
            txtFirstName.Text = query[0].Emp_First_Name;
            txtMiddleName.Text = query[0].Emp_Middle_Name;
            txtLastName.Text = query[0].Emp_Last_Name;
            ddlUSerRole.SelectedValue = query[0].Emp_Roll;
            userImg.ImageUrl = query[0].Emp_Image;
            ddlEmpStatus.SelectedValue = query[0].Emp_Status;
        }
        // // // The Following Click Event Is used For Save The Personal Details To The Employee
        protected void btnSavePersonal_Click(object sender, EventArgs e)
        {
            SavingPersonal();
        }

        private void SavingPersonal()
        {
            try
            {


                if (txtDateOfBirth.Text != "")
                {
                    string d1 = txtDateOfBirth.Text.Substring(0, 2);
                    string m1 = txtDateOfBirth.Text.Substring(3, 2);
                    string y1 = txtDateOfBirth.Text.Substring(6, 4);


                    DateTime DOB = DateTime.ParseExact(txtDateOfBirth.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);
                    //objEmpBAL.Emp_DOB = (txtDateOfBirth.Text == "") ? DateTime.Parse("01-01-2001") : DOB;
                    objEmpBAL.Emp_DOB = DOB;
                }
                else
                {
                    objEmpBAL.Emp_DOB = DateTime.Parse("01-01-2001");
                }



                if (txtLicenseExpiryDate.Text != "")
                {
                    string d3 = txtLicenseExpiryDate.Text.Substring(0, 2);
                    string m3 = txtLicenseExpiryDate.Text.Substring(3, 2);
                    string y3 = txtLicenseExpiryDate.Text.Substring(6, 4);
                    DateTime LicenceExpiryDate = DateTime.ParseExact(txtLicenseExpiryDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);
                    objEmpBAL.Licence_Exp_Date = LicenceExpiryDate;
                }
                else
                {
                    objEmpBAL.Licence_Exp_Date = DateTime.Parse("01-01-2001");
                }
                if (txtIssuedDate.Text != "")
                {
                    string d4 = txtIssuedDate.Text.Substring(0, 2);
                    string m4 = txtIssuedDate.Text.Substring(3, 2);
                    string y4 = txtIssuedDate.Text.Substring(6, 4);
                    DateTime IssuedDate = DateTime.ParseExact(txtIssuedDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);
                    objEmpBAL.Issued_Date = IssuedDate;
                }
                else
                {
                    objEmpBAL.Issued_Date = DateTime.Parse("01-01-2001");
                }
                if (txtExpiryDate.Text != "")
                {
                    string d5 = txtExpiryDate.Text.Substring(0, 2);
                    string m5 = txtExpiryDate.Text.Substring(3, 2);
                    string y5 = txtExpiryDate.Text.Substring(6, 4);
                    DateTime expiryDate = DateTime.ParseExact(txtExpiryDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);
                    objEmpBAL.Exp_Date = expiryDate;
                }
                else
                {
                    objEmpBAL.Exp_Date = DateTime.Parse("01-01-2001");
                }
                if (txtRenewalDate.Text != "")
                {
                    string d6 = txtRenewalDate.Text.Substring(0, 2);
                    string m6 = txtRenewalDate.Text.Substring(3, 2);
                    string y6 = txtRenewalDate.Text.Substring(6, 4);
                    DateTime renewalDate = DateTime.ParseExact(txtRenewalDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);
                    objEmpBAL.Renewal_Date = renewalDate;
                }
                else
                {
                    objEmpBAL.Renewal_Date = DateTime.Parse("01-01-2001");
                }

                objEmpBAL.Emp_Code = txtEmp_Code.Text;
                if (fupUserImage.HasFile)
                {
                    string imgname = fupUserImage.FileName;
                    fupUserImage.PostedFile.SaveAs(Server.MapPath("~/EmployeesImages/" + imgname));
                    objEmpBAL.Emp_Image = "~/EmployeesImages/" + imgname;

                }
                else
                {
                    objEmpBAL.Emp_Image = "";
                }
                objEmpBAL.Emp_First_Name = txtFirstName.Text;
                objEmpBAL.Emp_Middle_Name = txtMiddleName.Text;
                objEmpBAL.Emp_Last_Name = txtLastName.Text;
                objEmpBAL.Emp_Roll = ddlUSerRole.SelectedValue;
                objEmpBAL.User_Name = txtUsername.Text;
                objEmpBAL.Password = txtPassword.Text;
                objEmpBAL.Emp_Status = ddlEmpStatus.SelectedValue;

                objEmpBAL.Emp_Age = Convert.ToDecimal(txtAge.Text);

                // objEmpBAL.Emp_Status = ddlEmpStatus.SelectedValue;
                objEmpBAL.Gender = rblGender.SelectedValue;
                objEmpBAL.Blood_Group = txtBloodGroup.Text;


                objEmpBAL.Marital_Status = ddlMaritalStatus.SelectedValue;
                objEmpBAL.Nationality = ddlNationality.SelectedValue;
                objEmpBAL.Religion = ddlReligion.SelectedValue;
                objEmpBAL.Languages_Known = txtLanguagesKnown.Text;

                if (chbPermentAddress.Checked)
                {
                    objEmpBAL.PresentAsPrev = "true";
                }
                else { objEmpBAL.PresentAsPrev = "false"; }
                //objEmpBAL.Emp_Status = ddlEmpStatus.SelectedValue;

                objEmpBAL.Driving_Licenece_Number = txtDriving_License_Number.Text;
                objEmpBAL.Licence_Type = ddlType.SelectedValue;
                if (txtRetiredDate.Text != "")
                {
                    string d6 = txtRetiredDate.Text.Substring(0, 2);
                    string m6 = txtRetiredDate.Text.Substring(3, 2);
                    string y6 = txtRetiredDate.Text.Substring(6, 4);
                    DateTime ResignedDate = DateTime.ParseExact(txtRetiredDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);
                    objEmpBAL.Resigned_Date = ResignedDate;
                }
                else
                {
                    objEmpBAL.Resigned_Date = DateTime.Parse("01-01-2001");
                }




                //Personal Identification Attached Files
                if (FupLicense.HasFile == true)
                {
                    string filename = FupLicense.PostedFile.FileName;
                    var FileExtension = Path.GetExtension(FupLicense.PostedFile.FileName).ToString();
                    if (FileExtension.ToString().ToLower() == ".jpg" || FileExtension.ToString().ToLower() == ".pdf")
                    {
                        string path = Server.MapPath("~/Attachments/");
                        FupLicense.SaveAs(path + filename);
                        string LicenceAttachurl = "~/Attachments/" + filename;
                        objEmpBAL.Licence_Attachment = LicenceAttachurl.ToString();
                        // text.InnerText = filename.ToString();
                    }
                    else { ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('plz upload image files or pdf files only..');", true); }

                }
                else
                {
                    objEmpBAL.Licence_Attachment = "";
                }

                objEmpBAL.Aadhar_Number = txtAadharNumber.Text;
                if (FupAadhar.HasFile == true)
                {
                    string filename1 = FupAadhar.PostedFile.FileName;
                    var FileExtension1 = Path.GetExtension(FupAadhar.PostedFile.FileName).ToString();
                    if (FileExtension1.ToString().ToLower() == ".jpg" || FileExtension1.ToString().ToLower() == ".pdf")
                    {
                        string path1 = Server.MapPath("~/Attachments/");
                        FupAadhar.SaveAs(path1 + filename1);
                        string aadharAttachurl = "~/Attachments/" + filename1;
                        objEmpBAL.Aadhar_Attachment = aadharAttachurl.ToString();
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('plz upload image files or pdf files only..');", true);
                    }
                }
                else
                {
                    objEmpBAL.Aadhar_Attachment = "";
                }
                objEmpBAL.Pan_Number = txtpPANNo.Text;

                if (FupPanNo.HasFile == true)
                {
                    string filename2 = FupPanNo.PostedFile.FileName;
                    var FileExtension2 = Path.GetExtension(FupPanNo.PostedFile.FileName).ToString();
                    if (FileExtension2.ToString().ToLower() == ".jpg" || FileExtension2.ToString().ToLower() == ".pdf")
                    {
                        string path2 = Server.MapPath("~/Attachments/");
                        FupPanNo.SaveAs(path2 + filename2);
                        string PanAttachurl = "~/Attachments/" + filename2;
                        objEmpBAL.Pan_Attachment = PanAttachurl.ToString();
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('plz upload image files or pdf files only..');", true);
                    }
                }
                else
                { objEmpBAL.Pan_Attachment = ""; }
                objEmpBAL.Passport_Number = txtPassportNumber.Text;


                if (FupPassport.HasFile == true)
                {
                    string filename3 = FupPassport.PostedFile.FileName;
                    var FileExtension3 = Path.GetExtension(FupPassport.PostedFile.FileName).ToString();
                    if (FileExtension3.ToString().ToLower() == ".jpg" || FileExtension3.ToString().ToLower() == ".pdf")
                    {
                        string path3 = Server.MapPath("~/Attachments/");
                        FupPassport.SaveAs(path3 + filename3);
                        string PassportAttachurl = "~/Attachments/" + filename3;
                        objEmpBAL.Passport_Attachment = PassportAttachurl.ToString();
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('plz upload image files or pdf files only..');", true);
                    }

                }
                else
                { objEmpBAL.Passport_Attachment = ""; }
                objEmpBAL.Bank_Name = ddlPBankName.SelectedValue;
                objEmpBAL.Account_Number = txtPAccountNumber.Text;
                objEmpBAL.Health_Insurance_Poly_No = txtPHealth.Text;
                objEmpBAL.Sum_Insured = txtSumInsured.Text;

                objEmpBAL.Present_Address1 = txtAddress1.Text;
                objEmpBAL.Present_Address2 = txtAddress2.Text;
                objEmpBAL.Present_City = ddlCity.SelectedValue;
                objEmpBAL.Present_State = txtState.Text;
                objEmpBAL.Present_Pincode = txtPostalCode.Text;
                objEmpBAL.Present_Country = txtCountry.Text;
                objEmpBAL.Permanent_Address1 = txtPAddress1.Text;
                objEmpBAL.Permanent_Address2 = txtPaddress2.Text;
                objEmpBAL.Permanent_City = ddlPCity.SelectedValue;
                objEmpBAL.Permanent_Country = txtpCountry.Text;
                objEmpBAL.Permanent_Pincode = txtPPalost.Text;
                objEmpBAL.Permanent_State = txtPSate.Text;
                objEmpBAL.Permanent_Mobile = txtMobile.Text;
                objEmpBAL.Permanent_Email = txtEmail.Text;
                objEmpBAL.Alternative_Email = txtAlternativeEmail.Text;
                objEmpBAL.Skype_Id = txtSkypeID.Text;
                objEmpBAL.Linkedin_Id = txtLinkedin.Text;

                objEmpBAL.Educate = ddlEducate.SelectedValue;
                objEmpBAL.Telephone = "";

                objEmpBAL.Created_By = Session["User_Name"].ToString();
                objEmpBAL.Created_Date = System.DateTime.Now;
                objEmpBAL.Creation_Company = Session["ClientRegistrationNo"].ToString();
                objEmpBAL.Modified_By = Session["User_Name"].ToString();
                objEmpBAL.Modified_Date = System.DateTime.Now;
                var topEmpCode = (from person in ObjDAL.HRMS_Employee_Master_PersonalDetails
                                  where person.Emp_Code == txtEmp_Code.Text &&
                                  person.Creation_Company == Session["ClientRegistrationNo"].ToString()
                                  select person).ToList();
                //topEmpCode = null;
                if (topEmpCode.Count > 0)
                {
                    if (txtEmp_Code.Text == topEmpCode[0].Emp_Code)
                    {
                        objEmpBAL.Parameter = 2;
                        objEmpBAL.Emp_Code = txtEmp_Code.Text;
                    }
                    else
                    {

                        objEmpBAL.Emp_Code = txtEmp_Code.Text;
                    }
                }
                else
                {

                    objEmpBAL.Emp_Code = txtEmp_Code.Text;
                }
                if (objEmpBAL.Insert_Employee_Details() != 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);
                    GetPersonalData();
                    //PersonalButtons.Visible = false;

                }
            }
            catch (Exception ex)
            {
                string abc = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + abc + "');", true);


            }
        }
        // PermentAddress_CheckedChanged event
        protected void chbPermentAddress_CheckedChanged(object sender, EventArgs e)
        {
            if (chbPermentAddress.Checked)
            {
                txtPAddress1.Text = txtAddress1.Text;
                txtPaddress2.Text = txtAddress2.Text;
                ddlPCity.SelectedValue = ddlCity.SelectedValue;
                txtPSate.Text = txtState.Text;
                txtPPalost.Text = txtPostalCode.Text;
                txtpCountry.Text = txtCountry.Text;
                txtTelephone.Focus();
            }
            else
            {
                txtPAddress1.Text = "";
                txtPaddress2.Text = "";
                ddlPCity.SelectedValue = "Select";
                txtPSate.Text = "";
                txtPPalost.Text = "";
                txtpCountry.Text = "";
                txtPAddress1.Focus();
            }
        }

        // updating personal details to the employee
        protected void btnUpdatePersonal_Click(object sender, EventArgs e)
        {
            UpdateEmpPersonal();

        }

        public void UpdateEmpPersonal()
        {
            try
            {

                if (txtDateOfBirth.Text != "")
                {
                    string d1 = txtDateOfBirth.Text.Substring(0, 2);
                    string m1 = txtDateOfBirth.Text.Substring(3, 2);
                    string y1 = txtDateOfBirth.Text.Substring(6, 4);


                    DateTime DOB = DateTime.ParseExact(txtDateOfBirth.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);
                    //objEmpBAL.Emp_DOB = (txtDateOfBirth.Text == "") ? DateTime.Parse("01-01-2001") : DOB;
                    objEmpBAL.Emp_DOB = DOB;
                }
                else
                {
                    objEmpBAL.Emp_DOB = DateTime.Parse("01-01-2001");
                }


                if (txtLicenseExpiryDate.Text != "")
                {
                    string d3 = txtLicenseExpiryDate.Text.Substring(0, 2);
                    string m3 = txtLicenseExpiryDate.Text.Substring(3, 2);
                    string y3 = txtLicenseExpiryDate.Text.Substring(6, 4);
                    DateTime LicenceExpiryDate = DateTime.ParseExact(txtLicenseExpiryDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);
                    objEmpBAL.Licence_Exp_Date = LicenceExpiryDate;
                }
                else
                {
                    objEmpBAL.Licence_Exp_Date = DateTime.Parse("01-01-2001");
                }
                if (txtIssuedDate.Text != "")
                {
                    string d4 = txtIssuedDate.Text.Substring(0, 2);
                    string m4 = txtIssuedDate.Text.Substring(3, 2);
                    string y4 = txtIssuedDate.Text.Substring(6, 4);
                    DateTime IssuedDate = DateTime.ParseExact(txtIssuedDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);
                    objEmpBAL.Issued_Date = IssuedDate;
                }
                else
                {
                    objEmpBAL.Issued_Date = DateTime.Parse("01-01-2001");
                }
                if (txtExpiryDate.Text != "")
                {
                    string d5 = txtExpiryDate.Text.Substring(0, 2);
                    string m5 = txtExpiryDate.Text.Substring(3, 2);
                    string y5 = txtExpiryDate.Text.Substring(6, 4);
                    DateTime expiryDate = DateTime.ParseExact(txtExpiryDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);
                    objEmpBAL.Exp_Date = expiryDate;
                }
                else
                {
                    objEmpBAL.Exp_Date = DateTime.Parse("01-01-2001");
                }
                if (txtRenewalDate.Text != "")
                {
                    string d6 = txtRenewalDate.Text.Substring(0, 2);
                    string m6 = txtRenewalDate.Text.Substring(3, 2);
                    string y6 = txtRenewalDate.Text.Substring(6, 4);
                    DateTime renewalDate = DateTime.ParseExact(txtRenewalDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);
                    objEmpBAL.Renewal_Date = renewalDate;
                }
                else
                {
                    objEmpBAL.Renewal_Date = DateTime.Parse("01-01-2001");
                }

                objEmpBAL.Emp_Code = txtEmp_Code.Text;
                if (fupUserImage.HasFile)
                {
                    string imagename = fupUserImage.PostedFile.FileName;
                    fupUserImage.PostedFile.SaveAs(Server.MapPath("~/EmployeesImages/" + imagename));
                    objEmpBAL.Emp_Image = "~/EmployeesImages/" + imagename;
                }
                else
                {
                    if (Session["DbImagePath"] != null)
                    { objEmpBAL.Emp_Image = (string)Session["DbImagePath"]; }
                    else { objEmpBAL.Emp_Image = ""; }
                }
                if (txtRetiredDate.Text != "")
                {
                    string d6 = txtRetiredDate.Text.Substring(0, 2);
                    string m6 = txtRetiredDate.Text.Substring(3, 2);
                    string y6 = txtRetiredDate.Text.Substring(6, 4);
                    DateTime ResignedDate = DateTime.ParseExact(txtRetiredDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);
                    objEmpBAL.Resigned_Date = ResignedDate;
                }
                else
                {
                    objEmpBAL.Resigned_Date = DateTime.Parse("01-01-2001");
                }
                objEmpBAL.Emp_First_Name = txtFirstName.Text;
                objEmpBAL.Emp_Middle_Name = txtMiddleName.Text;
                objEmpBAL.Emp_Last_Name = txtLastName.Text;
                objEmpBAL.Emp_Roll = ddlUSerRole.SelectedValue;
                objEmpBAL.User_Name = txtUsername.Text;
                objEmpBAL.Password = txtPassword.Text;

                objEmpBAL.Emp_Status = ddlEmpStatus.SelectedValue;

                objEmpBAL.Emp_Age = Convert.ToDecimal(txtAge.Text);

                // objEmpBAL.Emp_Status = ddlEmpStatus.SelectedValue;
                objEmpBAL.Gender = rblGender.SelectedValue;
                objEmpBAL.Blood_Group = txtBloodGroup.Text;

                objEmpBAL.Marital_Status = ddlMaritalStatus.SelectedValue;
                objEmpBAL.Nationality = ddlNationality.SelectedValue;
                objEmpBAL.Religion = ddlReligion.SelectedValue;
                objEmpBAL.Languages_Known = txtLanguagesKnown.Text;


                objEmpBAL.Driving_Licenece_Number = txtDriving_License_Number.Text;
                objEmpBAL.Licence_Type = ddlType.SelectedValue;


                //Personal Identification Attached Files
                if (FupLicense.HasFile == true)
                {
                    string filename = FupLicense.PostedFile.FileName;
                    var FileExtension = Path.GetExtension(FupLicense.PostedFile.FileName).ToString();
                    if (FileExtension.ToString().ToLower() == ".jpg" || FileExtension.ToString().ToLower() == ".pdf")
                    {
                        string path = Server.MapPath("~/Attachments/");
                        FupLicense.SaveAs(path + filename);
                        string LicenceAttachurl = "~/Attachments/" + filename;
                        objEmpBAL.Licence_Attachment = LicenceAttachurl.ToString();
                        // text.InnerText = filename.ToString();
                    }
                    else { ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('plz upload image files or pdf files only..');", true); }

                }
                else
                {
                    objEmpBAL.Licence_Attachment = "";
                }

                objEmpBAL.Aadhar_Number = txtAadharNumber.Text;
                if (FupAadhar.HasFile == true)
                {
                    string filename1 = FupAadhar.PostedFile.FileName;
                    var FileExtension1 = Path.GetExtension(FupAadhar.PostedFile.FileName).ToString();
                    if (FileExtension1.ToString().ToLower() == ".jpg" || FileExtension1.ToString().ToLower() == ".pdf")
                    {
                        string path1 = Server.MapPath("~/Attachments/");
                        FupAadhar.SaveAs(path1 + filename1);
                        string aadharAttachurl = "~/Attachments/" + filename1;
                        objEmpBAL.Aadhar_Attachment = aadharAttachurl.ToString();
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('plz upload image files or pdf files only..');", true);
                    }
                }
                else
                {
                    objEmpBAL.Aadhar_Attachment = "";
                }
                objEmpBAL.Pan_Number = txtpPANNo.Text;

                if (FupPanNo.HasFile == true)
                {
                    string filename2 = FupPanNo.PostedFile.FileName;
                    var FileExtension2 = Path.GetExtension(FupPanNo.PostedFile.FileName).ToString();
                    if (FileExtension2.ToString().ToLower() == ".jpg" || FileExtension2.ToString().ToLower() == ".pdf")
                    {
                        string path2 = Server.MapPath("~/Attachments/");
                        FupPanNo.SaveAs(path2 + filename2);
                        string PanAttachurl = "~/Attachments/" + filename2;
                        objEmpBAL.Pan_Attachment = PanAttachurl.ToString();
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('plz upload image files or pdf files only..');", true);
                    }
                }
                else
                { objEmpBAL.Pan_Attachment = ""; }
                objEmpBAL.Passport_Number = txtPassportNumber.Text;


                if (FupPassport.HasFile == true)
                {
                    string filename3 = FupPassport.PostedFile.FileName;
                    var FileExtension3 = Path.GetExtension(FupPassport.PostedFile.FileName).ToString();
                    if (FileExtension3.ToString().ToLower() == ".jpg" || FileExtension3.ToString().ToLower() == ".pdf")
                    {
                        string path3 = Server.MapPath("~/Attachments/");
                        FupPassport.SaveAs(path3 + filename3);
                        string PassportAttachurl = "~/Attachments/" + filename3;
                        objEmpBAL.Passport_Attachment = PassportAttachurl.ToString();
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('plz upload image files or pdf files only..');", true);
                    }

                }
                else
                { objEmpBAL.Passport_Attachment = ""; }
                objEmpBAL.Bank_Name = ddlPBankName.SelectedValue;
                objEmpBAL.Account_Number = txtPAccountNumber.Text;
                objEmpBAL.Health_Insurance_Poly_No = txtPHealth.Text;
                objEmpBAL.Sum_Insured = txtSumInsured.Text;

                objEmpBAL.Present_Address1 = txtAddress1.Text;
                objEmpBAL.Present_Address2 = txtAddress2.Text;
                objEmpBAL.Present_City = ddlCity.SelectedValue;
                objEmpBAL.Present_State = txtState.Text;
                objEmpBAL.Present_Pincode = txtPostalCode.Text;
                objEmpBAL.Present_Country = txtCountry.Text;
                objEmpBAL.Permanent_Address1 = txtPAddress1.Text;
                objEmpBAL.Permanent_Address2 = txtPaddress2.Text;
                if (chbPermentAddress.Checked)
                {
                    objEmpBAL.PresentAsPrev = "true";
                }
                else { objEmpBAL.PresentAsPrev = "false"; }
                objEmpBAL.Permanent_City = ddlPCity.SelectedValue;
                objEmpBAL.Permanent_Country = txtpCountry.Text;
                objEmpBAL.Permanent_Pincode = txtPPalost.Text;
                objEmpBAL.Permanent_State = txtPSate.Text;
                objEmpBAL.Permanent_Mobile = txtMobile.Text;
                objEmpBAL.Permanent_Email = txtEmail.Text;
                objEmpBAL.Alternative_Email = txtAlternativeEmail.Text;
                objEmpBAL.Skype_Id = txtSkypeID.Text;
                objEmpBAL.Linkedin_Id = txtLinkedin.Text;



                objEmpBAL.Created_By = Session["User_Name"].ToString();
                objEmpBAL.Created_Date = System.DateTime.Now;
                objEmpBAL.Creation_Company = Session["ClientRegistrationNo"].ToString();
                objEmpBAL.Modified_By = Session["User_Name"].ToString();
                objEmpBAL.Modified_Date = System.DateTime.Now;

                objEmpBAL.Parameter = 2;


                if (objEmpBAL.Insert_Employee_Details() != 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Employee personal Data Saved Successfully');", true);
                    //ClearAllFields();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Employee personal Data Data Not Saved');", true);
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }
        }

        string Password;
        // Displaying age in age textbox
        //protected void txtDateOfBirth_TextChanged(object sender, EventArgs e)
        //{
        //    Password = txtPassword.Text;
        //    txtPassword.TextMode = TextBoxMode.SingleLine;
        //    if (txtDateOfBirth.Text != "")
        //    {
        //        int currentyear = Convert.ToInt32(DateTime.Now.Year.ToString());

        //        string d1 = txtDateOfBirth.Text.Substring(0, 2);
        //        string m1 = txtDateOfBirth.Text.Substring(3, 2);
        //        string y1 = txtDateOfBirth.Text.Substring(6, 4);

        //        int dobYear = Convert.ToInt32(y1);

        //        if ((currentyear - 18) > dobYear)
        //        {
        //            int currentAge = currentyear - dobYear;

        //            txtAge.Text = currentAge.ToString();

        //        }
        //        else
        //        {
        //            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Plz Enter Valid Dath Of Birth..');", true);
        //            txtDateOfBirth.Text = "";
        //            txtAge.Text = "";
        //            txtDateOfBirth.Focus();

        //        }

        //    }
        //    else
        //    {
        //        txtAge.Text = "";
        //    }
        //    // txtPassword.TextMode = TextBoxMode.Password;
        //    txtPassword.Text = Password;

        //}
        #endregion

        #region Employee Family Information
        //Bank names binding in ddlPBankName
        /// <summary>
        /// Binding Bank Names
        /// </summary>
        /// <param name="ddlBankName"></param>
        public void Bind_BankName()
        {
            try
            {
                String Creation_Company = Session["ClientRegistrationNo"].ToString();
                List<HRMS_Bank_Master> objGroup = objBankBAL.GetData_BankMaster(Creation_Company);

                if (objGroup.Count > 0)
                {
                    ddlPBankName.DataSource = objGroup;
                    ddlPBankName.DataTextField = "Bank_Name";
                    ddlPBankName.DataValueField = "Bank_Name";
                    ddlPBankName.DataBind();
                    ddlPBankName.Items.Insert(0, new ListItem("Select", "Select")); //updated code
                }
                else
                {
                    ddlPBankName.Items.Insert(0, new ListItem("Select", "Select")); //updated code
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            {

            }
        }

        /// <summary>
        /// Family Details
        /// </summary>
        public void SetInitialRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            // dt.Columns.Add(new DataColumn("Id", typeof(int)));
            //  dt.Columns.Add(new DataColumn("Bank_ID", typeof(string)));
            dt.Columns.Add(new DataColumn("Member_Name", typeof(string)));
            dt.Columns.Add(new DataColumn("Member_Relation", typeof(string)));
            dt.Columns.Add(new DataColumn("Member_DOB", typeof(string)));

            dt.Columns.Add(new DataColumn("Member_Occupation", typeof(string)));
            //dt.Columns.Add(new DataColumn("Member_ContactNo", typeof(string)));
            dt.Columns.Add(new DataColumn("Member_Dependent", typeof(string)));
            dt.Columns.Add(new DataColumn("Member_Nominee", typeof(string)));
            // dt.Columns.Add(new DataColumn("Member_Emergency_Contact", typeof(string)));



            dr = dt.NewRow();
            dr["Member_Name"] = string.Empty;
            dr["Member_Relation"] = string.Empty;
            dr["Member_DOB"] = string.Empty;

            dr["Member_Occupation"] = string.Empty;
            //dr["Member_ContactNo"] = string.Empty;
            dr["Member_Dependent"] = string.Empty;
            dr["Member_Nominee"] = string.Empty;

            //dr["Member_Emergency_Contact"] = string.Empty;

            dt.Rows.Add(dr);
            ViewState["FamilyCurrentTable"] = dt;

            grdFamily.DataSource = dt;
            grdFamily.DataBind();
        }

        // Grid Row bound
        protected void grdFamily_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TextBox txtName = (TextBox)e.Row.FindControl("txtName");
                DropDownList ddlRelationship = (DropDownList)e.Row.FindControl("ddlRelationship");
                DropDownList ddlOccupation = (DropDownList)e.Row.FindControl("ddlOccupation");
                GridViewRow gr = grdFamily.SelectedRow;
                CheckBox chbDependent = (CheckBox)e.Row.FindControl("chbDependent");
                CheckBox chbNominee = (CheckBox)e.Row.FindControl("chbNominee");
                //CheckBox chbContactonEmergency = (CheckBox)e.Row.FindControl("chbContactonEmergency");
                Label lbldependent = (Label)e.Row.FindControl("lblchkdefault1");
                Label lblNominee = (Label)e.Row.FindControl("lblchkdefault2");
                Label lblEmergency = (Label)e.Row.FindControl("lblchkdefault3");
                //TextBox txtFContactNo = (TextBox)e.Row.FindControl("txtFContactNo");
                TextBox txtFDateofBirth = (TextBox)e.Row.FindControl("txtFDateofBirth");

                TextBox box = (TextBox)e.Row.FindControl("txtrelation");
                TextBox box1 = (TextBox)e.Row.FindControl("txtoccupation");

                ddlRelationship.SelectedValue = box.Text;
                ddlOccupation.SelectedValue = box1.Text;
                if (lbldependent.Text.ToLower() == "true")
                { chbDependent.Checked = true; }
                else
                { chbDependent.Checked = false; }
                if (lblNominee.Text.ToLower() == "true")
                { chbNominee.Checked = true; }
                else
                { chbNominee.Checked = false; }
                //if (lblEmergency.Text.ToLower() == "true")
                //{ chbContactonEmergency.Checked = true; }
                //else
                //{ chbContactonEmergency.Checked = false; }


            }
        }
        /// <summary>
        /// Adding Family Information in grid 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddNewRecord_Click(object sender, EventArgs e)
        {
            // Adding Empty Row Data into Family Details


            int Count = (int)grdFamily.Rows.Count;
            TextBox txtName = (TextBox)grdFamily.Rows[Count - 1].FindControl("txtName") as TextBox;
            DropDownList ddlRelationship = (DropDownList)grdFamily.Rows[Count - 1].FindControl("ddlRelationship") as DropDownList;
            TextBox txtFDateofBirth = (TextBox)grdFamily.Rows[Count - 1].FindControl("txtFDateofBirth") as TextBox;
            DropDownList ddlOccupation = (DropDownList)grdFamily.Rows[Count - 1].FindControl("ddlOccupation") as DropDownList;
            // TextBox txtFContactNo = (TextBox)grdFamily.Rows[Count - 1].FindControl("txtFContactNo") as TextBox;
            CheckBox chbDependent = (CheckBox)grdFamily.Rows[Count - 1].FindControl("chbDependent") as CheckBox;
            CheckBox chbNominee = (CheckBox)grdFamily.Rows[Count - 1].FindControl("chbNominee") as CheckBox;
            // CheckBox chbContactonEmergency = (CheckBox)grdFamily.Rows[Count - 1].FindControl("chbContactonEmergency") as CheckBox;

            if (txtName.Text == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Enter Name');", true);
                txtName.Focus();

            }
            else if (ddlRelationship.SelectedValue == "Select")
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Select Relation Ship');", true);
                ddlRelationship.Focus();
            }
            else if (txtFDateofBirth.Text == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Select Date ');", true);
                txtFDateofBirth.Focus();
            }
            else if (ddlOccupation.SelectedValue == "Select")
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Select Occupation');", true);
                ddlOccupation.Focus();
            }
            //else if (txtFContactNo.Text == "")
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Enter Contact No.');", true);
            //    txtFContactNo.Focus();
            //}

            else
            {

                AddNewRowToGrid();

                int aa = (int)grdFamily.Rows.Count;
                TextBox box = (TextBox)grdFamily.Rows[aa - 1].FindControl("txtName") as TextBox;
                box.Focus();
            }


        }

        //Add New Record
        public void AddNewRowToGrid()
        {
            int rowIndex = 0;

            if (ViewState["FamilyCurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["FamilyCurrentTable"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        TextBox txtName = (TextBox)grdFamily.Rows[rowIndex].FindControl("txtName");
                        DropDownList ddlRelationship = (DropDownList)grdFamily.Rows[rowIndex].FindControl("ddlRelationship");
                        TextBox txtFDateofBirth = (TextBox)grdFamily.Rows[rowIndex].FindControl("txtFDateofBirth");
                        DropDownList ddlOccupation = (DropDownList)grdFamily.Rows[rowIndex].FindControl("ddlOccupation");
                        // TextBox txtFContactNo = (TextBox)grdFamily.Rows[rowIndex].FindControl("txtFContactNo");
                        CheckBox chbDependent = (CheckBox)grdFamily.Rows[rowIndex].FindControl("chbDependent");
                        CheckBox chbNominee = (CheckBox)grdFamily.Rows[rowIndex].FindControl("chbNominee");
                        //CheckBox chbContactonEmergency = (CheckBox)grdFamily.Rows[rowIndex].FindControl("chbContactonEmergency");
                        Label lbldependent = (Label)grdFamily.Rows[rowIndex].FindControl("lblchkdefault1");
                        Label lblNominee = (Label)grdFamily.Rows[rowIndex].FindControl("lblchkdefault2");
                        Label lblEmergency = (Label)grdFamily.Rows[rowIndex].FindControl("lblchkdefault3");
                        drCurrentRow = dtCurrentTable.NewRow();
                        dtCurrentTable.Rows[i - 1]["Member_Name"] = txtName.Text;
                        dtCurrentTable.Rows[i - 1]["Member_Relation"] = ddlRelationship.SelectedValue;
                        dtCurrentTable.Rows[i - 1]["Member_DOB"] = txtFDateofBirth.Text;
                        dtCurrentTable.Rows[i - 1]["Member_Occupation"] = ddlOccupation.SelectedValue;
                        // dtCurrentTable.Rows[i - 1]["Member_ContactNo"] = txtFContactNo.Text;
                        if (chbDependent.Checked)
                        {
                            dtCurrentTable.Rows[i - 1]["Member_Dependent"] = chbDependent.Checked;
                        }
                        else
                        {
                            chbDependent.Checked = false;
                            dtCurrentTable.Rows[i - 1]["Member_Dependent"] = chbDependent.Checked;
                        }
                        if (chbNominee.Checked)
                        { dtCurrentTable.Rows[i - 1]["Member_Nominee"] = chbNominee.Checked; }
                        else
                        {
                            chbNominee.Checked = false;
                            dtCurrentTable.Rows[i - 1]["Member_Nominee"] = chbNominee.Checked;
                        }

                        //if (chbContactonEmergency.Checked)
                        //{
                        //    dtCurrentTable.Rows[i - 1]["Member_Emergency_Contact"] = chbContactonEmergency.Checked;
                        //}
                        //else
                        //{
                        //    chbContactonEmergency.Checked = false;
                        //    dtCurrentTable.Rows[i - 1]["Member_Emergency_Contact"] = chbContactonEmergency.Checked;
                        //}
                        rowIndex += 1;
                    }

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["FamilyCurrentTable"] = dtCurrentTable;
                    grdFamily.DataSource = dtCurrentTable;
                    grdFamily.DataBind();

                }
            }
            else
            {
                Response.Write("ViewState is null");
            }

        }

        //Set Previous Record on Grid view
        private void SetPreviousData()
        {

            int rowIndex = 0;
            if (ViewState["FamilyCurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["FamilyCurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        //Set the Previous Selected Items on Each DropDownList on Postbacks


                        TextBox txtName = (TextBox)grdFamily.Rows[rowIndex].Cells[0].FindControl("txtName");

                        DropDownList ddlRelationship = (DropDownList)grdFamily.Rows[rowIndex].Cells[1].FindControl("ddlRelationship");
                        TextBox txtFDateofBirth = (TextBox)grdFamily.Rows[rowIndex].Cells[2].FindControl("txtFDateofBirth");
                        DropDownList ddlOccupation = (DropDownList)grdFamily.Rows[rowIndex].Cells[3].FindControl("ddlOccupation");
                        //TextBox txtFContactNo = (TextBox)grdFamily.Rows[rowIndex].Cells[4].FindControl("txtFContactNo");
                        CheckBox chbDependent = (CheckBox)grdFamily.Rows[rowIndex].Cells[5].FindControl("chbDependent");
                        CheckBox chbNominee = (CheckBox)grdFamily.Rows[rowIndex].Cells[6].FindControl("chbNominee");
                        // CheckBox chbContactonEmergency = (CheckBox)grdFamily.Rows[rowIndex].Cells[7].FindControl("chbContactonEmergency");

                        if (i < dt.Rows.Count - 1)
                        {
                            txtName.Text = dt.Rows[i]["Name"].ToString();
                            ddlRelationship.Text = dt.Rows[i]["Relationship"].ToString();
                            txtFDateofBirth.Text = dt.Rows[i]["Date_of_Birth"].ToString();
                            ddlOccupation.Text = dt.Rows[i]["Occupation"].ToString();
                            // txtFContactNo.Text = dt.Rows[i]["Contact_No"].ToString();
                            if (dt.Rows[i]["Dependent"].ToString().Trim().ToLower() == "true")
                            {
                                chbDependent.Checked = true;
                            }
                            else
                            {
                                chbDependent.Checked = false;
                            }
                            if (dt.Rows[i]["Nominee"].ToString().Trim().ToLower() == "true")
                            {
                                chbNominee.Checked = true;
                            }
                            else
                            {
                                chbNominee.Checked = false;
                            }
                            //if (dt.Rows[i]["Contact_On_Emergency"].ToString().Trim().ToLower() == "true")
                            //{
                            //    chbContactonEmergency.Checked = true;
                            //}
                            //else
                            //{
                            //    chbContactonEmergency.Checked = false;
                            //}


                        }

                        rowIndex++;
                    }
                }
            }
        }

        /// <summary>
        /// Remove Bank Name in Grid view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbRemove_Click(object sender, EventArgs e)
        {
            AddRemoveNewRowToGrid();
            //SetRemovePreviousData();
            LinkButton lb = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow.RowIndex;
            if (ViewState["FamilyCurrentTable"] != null)
            {

                DataTable dt = (DataTable)ViewState["FamilyCurrentTable"];
                if (dt.Rows.Count > 1)
                {
                    //Remove the Selected Row data and reset row number
                    dt.Rows.Remove(dt.Rows[rowID]);
                }

                //Store the current data in ViewState for future reference
                ViewState["FamilyCurrentTable"] = dt;
                //   LinkButton AddressDetails = (LinkButton)grvCustomerAddressDetail.Rows[rowID].FindControl("lbRemove");
                // AddressDetails.Focus();

                grdFamily.DataSource = dt;
                grdFamily.DataBind();


            }

        }
        // // add new row remove
        public void AddRemoveNewRowToGrid()
        {
            int rowIndex = 0;

            if (ViewState["FamilyCurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["FamilyCurrentTable"];
                DataRow drCurrentRow = null;
                int index = dtCurrentTable.Rows.Count;

                if (dtCurrentTable.Rows.Count > 0)
                {

                    TextBox txtName = (TextBox)grdFamily.Rows[index - 1].FindControl("txtName");
                    DropDownList ddlRelationship = (DropDownList)grdFamily.Rows[index - 1].FindControl("ddlRelationship");
                    TextBox txtFDateofBirth = (TextBox)grdFamily.Rows[index - 1].FindControl("txtFDateofBirth");
                    DropDownList ddlOccupation = (DropDownList)grdFamily.Rows[index - 1].FindControl("ddlOccupation");
                    // TextBox txtFContactNo = (TextBox)grdFamily.Rows[index - 1].FindControl("txtFContactNo");
                    CheckBox chbDependent = (CheckBox)grdFamily.Rows[index - 1].FindControl("chbDependent");
                    CheckBox chbNominee = (CheckBox)grdFamily.Rows[index - 1].FindControl("chbNominee");
                    //CheckBox chbContactonEmergency = (CheckBox)grdFamily.Rows[index - 1].FindControl("chbContactonEmergency");
                    Label lbldependent = (Label)grdFamily.Rows[index - 1].FindControl("lblchkdefault1");
                    Label lblNominee = (Label)grdFamily.Rows[index - 1].FindControl("lblchkdefault2");
                    Label lblEmergency = (Label)grdFamily.Rows[index - 1].FindControl("lblchkdefault3");

                    drCurrentRow = dtCurrentTable.NewRow();
                    dtCurrentTable.Rows[index - 1]["Member_Name"] = txtName.Text;
                    dtCurrentTable.Rows[index - 1]["Member_Relation"] = ddlRelationship.SelectedValue;
                    dtCurrentTable.Rows[index - 1]["Member_DOB"] = txtFDateofBirth.Text;
                    dtCurrentTable.Rows[index - 1]["Member_Occupation"] = ddlOccupation.SelectedValue;
                    // dtCurrentTable.Rows[index - 1]["Member_ContactNo"] = txtFContactNo.Text;
                    if (lbldependent.Text.ToLower() == "true")
                    { dtCurrentTable.Rows[index - 1]["Member_Dependent"] = true; }
                    else
                    { dtCurrentTable.Rows[index - 1]["Member_Dependent"] = false; }
                    // dtCurrentTable.Rows[index - 1]["Dependent"] = (lbldependent.Text.ToLower() == "true") ? true : false; 

                    if (lblNominee.Text.ToLower() == "true")
                    {
                        dtCurrentTable.Rows[index - 1]["Member_Nominee"] = true;
                    }
                    else
                    { dtCurrentTable.Rows[index - 1]["Member_Nominee"] = false; }

                    //if (lblEmergency.Text.ToLower() == "true")
                    //{
                    //    dtCurrentTable.Rows[index - 1]["Member_Emergency_Contact"] = true;
                    //}
                    //else
                    //{ dtCurrentTable.Rows[index - 1]["Member_Emergency_Contact"] = false; }

                    rowIndex += 1;

                    ViewState["FamilyCurrentTable"] = dtCurrentTable;
                    grdFamily.DataSource = dtCurrentTable;
                    grdFamily.DataBind();

                }

            }
            else
            {
                Response.Write("ViewState is null");
            }


        }

        protected void grdFamily_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataTable dt = (DataTable)ViewState["FamilyCurrentTable"];
                LinkButton lb = (LinkButton)e.Row.FindControl("lbRemove");
                if (lb != null)
                {
                    if (dt.Rows.Count > 1)
                    {
                        if (e.Row.RowIndex == dt.Rows.Count - 1)
                        {
                            lb.Visible = false;
                        }
                    }
                    else
                    {
                        lb.Visible = false;
                    }
                }
            }
        }

        //chbNominee_CheckedChanged Nominee Restriction
        protected void chbNominee_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox ddl = (CheckBox)sender;
                GridViewRow gvr = (GridViewRow)ddl.NamingContainer;
                int index = gvr.RowIndex;
                CheckBox ctrl = (CheckBox)grdFamily.Rows[index].FindControl("chbNominee");

                if (ctrl.Checked == true)
                {
                    // int rowIndex = 0;

                    CheckBox chknominee = (CheckBox)ctrl;
                    TextBox txtName = (TextBox)grdFamily.Rows[index].FindControl("txtName");
                    DropDownList ddlRelationship = (DropDownList)grdFamily.Rows[index].FindControl("ddlRelationship");
                    TextBox txtFDateofBirth = (TextBox)grdFamily.Rows[index].FindControl("txtFDateofBirth");
                    DropDownList ddlOccupation = (DropDownList)grdFamily.Rows[index].FindControl("ddlOccupation");
                    //TextBox txtFContactNo = (TextBox)grdFamily.Rows[index].FindControl("txtFContactNo");
                    CheckBox chbDependent = (CheckBox)grdFamily.Rows[index].FindControl("chbDependent");
                    CheckBox chbNominee = (CheckBox)grdFamily.Rows[index].FindControl("chbNominee");
                    //CheckBox chbContactonEmergency = (CheckBox)grdFamily.Rows[index].FindControl("chbContactonEmergency");


                    //string ProdCode=ddlSubCategory.SelectedItem.Value;
                    //String Creation_Company=(string)Session["ClientRegistrationNo"];
                    //List<SCM_Master_Product> ObjProductName = ObjSCM_Product.Check_ProductCode(ProdCode, Creation_Company);

                    DataTable DT = (DataTable)ViewState["FamilyCurrentTable"];
                    int i = 0;
                    foreach (DataRow dr in DT.Rows)
                    {
                        if (dr["Member_Nominee"].ToString().Trim().ToLower() == "true")
                        {
                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Only One Nominee u Have To Select.');", true);
                            txtName.Text = "";
                            ddlRelationship.SelectedValue = "Select";
                            txtFDateofBirth.Text = "";
                            ddlOccupation.SelectedValue = "Select";
                            // txtFContactNo.Text = "";
                            chbDependent.Checked = false;
                            chbNominee.Checked = false;
                            //chbContactonEmergency.Checked = false;
                            txtName.Focus();
                            i++;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }

        }
        //Saving Event
        protected void btnSaveFamily_Click(object sender, EventArgs e)
        {
            SavingFamily();
        }
        //Update Event
        protected void btnUpdateFamily_Click(object sender, EventArgs e)
        {
            string Creation_Company = Session["ClientRegistrationNo"].ToString();
            string Emp_Code = (string)Session["EmployeeFamilyDetailsCode"];
            List<HRMS_Employee_Master_FamilyInformation> SUM = (from ST in ObjDAL.HRMS_Employee_Master_FamilyInformations
                                                                where ST.Creation_Company == Creation_Company && ST.Emp_Code == Emp_Code
                                                                select ST).ToList();
            ObjDAL.HRMS_Employee_Master_FamilyInformations.DeleteAllOnSubmit(SUM);
            ObjDAL.SubmitChanges();
            SavingFamily();
        }

        private void SavingFamily()
        {
            try
            {



                if (grdFamily.Rows.Count != 0)
                {
                    foreach (GridViewRow objrow in grdFamily.Rows)
                    {
                        if (objrow.RowType == DataControlRowType.DataRow)
                        {
                            TextBox txtName = objrow.FindControl("txtName") as TextBox;
                            DropDownList ddlRelationship = objrow.FindControl("ddlRelationship") as DropDownList;
                            TextBox txtFDateofBirth = objrow.FindControl("txtFDateofBirth") as TextBox;
                            DropDownList ddlOccupation = objrow.FindControl("ddlOccupation") as DropDownList;
                            //TextBox txtFContactNo = objrow.FindControl("txtFContactNo") as TextBox;
                            CheckBox chbDependent = objrow.FindControl("chbDependent") as CheckBox;
                            CheckBox chbNominee = objrow.FindControl("chbNominee") as CheckBox;
                            // CheckBox chbContactonEmergency = objrow.FindControl("chbContactonEmergency") as CheckBox;

                            string d1 = txtFDateofBirth.Text.Substring(0, 2);
                            string m1 = txtFDateofBirth.Text.Substring(3, 2);
                            string y1 = txtFDateofBirth.Text.Substring(6, 4);


                            DateTime DOB = DateTime.ParseExact(txtFDateofBirth.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);
                            objEmpBAL.Emp_Code = txtEmp_Code.Text;
                            objEmpBAL.Member_Name = txtName.Text;
                            objEmpBAL.Member_Relation = ddlRelationship.SelectedValue;
                            objEmpBAL.Member_DOB = (txtFDateofBirth.Text == "") ? DateTime.Parse("01-01-2001") : DOB;
                            objEmpBAL.Member_Occupation = ddlOccupation.SelectedValue;
                            objEmpBAL.Member_ContactNo = "";// txtFContactNo.Text;
                            if (chbDependent.Checked)
                            { objEmpBAL.Member_Dependent = "true"; }
                            else
                            { objEmpBAL.Member_Dependent = "false"; }

                            if (chbNominee.Checked)
                            { objEmpBAL.Member_Nominee = "true"; }
                            else
                            { objEmpBAL.Member_Nominee = "false"; }

                            //if (chbContactonEmergency.Checked)
                            //{ objEmpBAL.Member_Emergency_Contact = "true"; }
                            //else
                            //{ objEmpBAL.Member_Emergency_Contact = "false"; }

                            objEmpBAL.Member_Emergency_Contact = "";

                            objEmpBAL.Created_By = Session["User_Name"].ToString();
                            objEmpBAL.Created_Date = System.DateTime.Now;
                            objEmpBAL.Creation_Company = Session["ClientRegistrationNo"].ToString();
                            objEmpBAL.Modified_By = Session["User_Name"].ToString();
                            objEmpBAL.Modified_Date = System.DateTime.Now;
                            objEmpBAL.Parameter = 1;
                            if (objEmpBAL.Insert_Employee_Family_Information() != 0)
                            {
                                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);

                            }
                        }
                    }

                }
                //SetInitialRow();

            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }

        protected void btnResetFamily_Click(object sender, EventArgs e)
        {
            SetInitialRow();
            txtEmp_Code.Text = "";
            txtFirstName.Text = "";
            txtMiddleName.Text = "";
            ddlUSerRole.SelectedValue = "Select";
            ddlEmpStatus.SelectedValue = "Select";
        }


        //Editing family Details
        public void Edit_Employee_Family_Details(string empcode, string Creation_Company)
        {
            try
            {

                List<HRMS_Employee_Master_FamilyInformation> objEmpDetails = objEmpBAL.Get_Employee_Family_Details(empcode, Creation_Company);

                if (objEmpDetails.Count > 0)
                {
                    grdFamily.DataSource = objEmpDetails;
                    grdFamily.DataBind();

                    DataTable dt = new DataTable();
                    dt.Columns.Add(new DataColumn("Member_Name", typeof(string)));
                    dt.Columns.Add(new DataColumn("Member_Relation", typeof(string)));
                    dt.Columns.Add(new DataColumn("Member_DOB", typeof(string)));

                    dt.Columns.Add(new DataColumn("Member_Occupation", typeof(string)));
                    // dt.Columns.Add(new DataColumn("Member_ContactNo", typeof(string)));
                    dt.Columns.Add(new DataColumn("Member_Dependent", typeof(string)));
                    dt.Columns.Add(new DataColumn("Member_Nominee", typeof(string)));
                    //dt.Columns.Add(new DataColumn("Member_Emergency_Contact", typeof(string)));

                    for (int i = 0; i < grdFamily.Rows.Count; i++)
                    {
                        TextBox txtName = grdFamily.Rows[i].FindControl("txtName") as TextBox;
                        DropDownList ddlRelationship = grdFamily.Rows[i].FindControl("ddlRelationship") as DropDownList;
                        TextBox txtFDateofBirth = grdFamily.Rows[i].FindControl("txtFDateofBirth") as TextBox;
                        DropDownList ddlOccupation = grdFamily.Rows[i].FindControl("ddlOccupation") as DropDownList;
                        // TextBox txtFContactNo = grdFamily.Rows[i].FindControl("txtFContactNo") as TextBox;
                        CheckBox chbDependent = grdFamily.Rows[i].FindControl("chbDependent") as CheckBox;
                        CheckBox chbNominee = grdFamily.Rows[i].FindControl("chbNominee") as CheckBox;
                        //CheckBox chbContactonEmergency = grdFamily.Rows[i].FindControl("chbContactonEmergency") as CheckBox;



                        string name = txtName.Text;
                        string relation = ddlRelationship.SelectedValue;
                        string birth = txtFDateofBirth.Text;
                        string occupation = ddlOccupation.SelectedValue;
                        string contact = "";// txtFContactNo.Text;
                        string dependent, Nominee, Emergency;
                        if (chbDependent.Checked)
                        { dependent = "true"; }
                        else
                        { dependent = "false"; }
                        if (chbNominee.Checked)
                        { Nominee = "true"; }
                        else
                        { Nominee = "false"; }
                        //if (chbContactonEmergency.Checked)
                        //{ Emergency = "true"; }
                        //else
                        //{ Emergency = "false"; }

                        //TextBox MinPriceAllowedBranchLevel = grdProductItems.Rows[i].FindControl("txtMinPriceAllowedBranchLevel") as TextBox;
                        dt.Rows.Add(name, relation, birth, occupation, contact, dependent, Nominee, "");
                    }

                    ViewState["FamilyCurrentTable"] = dt;
                    if (Session["User_Type"].ToString() == "Employees")
                    {

                    }
                    else
                    {

                        Session["EmployeeDetailsCode"] = null;
                    }
                }
                else
                {
                    //ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('No Data Found ');", true);
                    if (Session["User_Type"].ToString() == "Employees")
                    {

                    }
                    else
                    {

                    }

                }



            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }

        #endregion

        #region employee key performance details
        /// <summary>
        /// Key Details
        /// </summary>
        //public void SetInitialRowKey()
        //{
        //    DataTable dt = new DataTable();
        //    DataRow dr = null;

        //    dt.Columns.Add(new DataColumn("Objective", typeof(string)));
        //    dt.Columns.Add(new DataColumn("Measurement", typeof(string)));

        //    dt.Columns.Add(new DataColumn("Monitoring", typeof(string)));
        //    dt.Columns.Add(new DataColumn("Current_Level", typeof(string)));



        //    dr = dt.NewRow();
        //    dr["Objective"] = string.Empty;
        //    dr["Measurement"] = string.Empty;
        //    dr["Monitoring"] = string.Empty;

        //    dr["Current_Level"] = string.Empty;



        //    dt.Rows.Add(dr);
        //    ViewState["KeyCurrentTable"] = dt;
        //    grvKey.DataSource = dt;
        //    grvKey.DataBind();
        //}

        ///// <summary>
        ///// Add New Row For GrdKey
        ///// </summary>
        //protected void btnNewRecordKey_Click(object sender, EventArgs e)
        //{
        //    // Adding Empty Row Data into Sale Order Item Grid Details

        //    AddNewRowToGridKey();

        //    int aa = (int)grvKey.Rows.Count;
        //    TextBox box = (TextBox)grvKey.Rows[aa - 1].FindControl("txtObjective") as TextBox;
        //    DropDownList ddlMonitoringFrequency = (DropDownList)grvKey.Rows[aa - 1].FindControl("ddlMonitoringFrequency") as DropDownList;
        //    box.Focus();
        //}
        //public void AddNewRowToGridKey()
        //{

        //    int rowIndex = 0;

        //    if (ViewState["KeyCurrentTable"] != null)
        //    {
        //        DataTable dtCurrentTable = (DataTable)ViewState["KeyCurrentTable"];
        //        DataRow drCurrentRow = null;

        //        if (dtCurrentTable.Rows.Count > 0)
        //        {
        //            for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
        //            {
        //                TextBox txtObjective = (TextBox)grvKey.Rows[rowIndex].FindControl("txtObjective");
        //                DropDownList ddlMonitoringFrequency = (DropDownList)grvKey.Rows[rowIndex].FindControl("ddlMonitoringFrequency");
        //                TextBox txtMonitoring = (TextBox)grvKey.Rows[rowIndex].FindControl("txtMonitoring");
        //                TextBox txtCurrentLevel = (TextBox)grvKey.Rows[rowIndex].FindControl("txtCurrentLevel");
        //                drCurrentRow = dtCurrentTable.NewRow();
        //                dtCurrentTable.Rows[i - 1]["Objective"] = txtObjective.Text;
        //                dtCurrentTable.Rows[i - 1]["Measurement"] = ddlMonitoringFrequency.SelectedValue;
        //                dtCurrentTable.Rows[i - 1]["Monitoring"] = txtMonitoring.Text;
        //                dtCurrentTable.Rows[i - 1]["Current_Level"] = txtCurrentLevel.Text;
        //                rowIndex += 1;
        //            }

        //            dtCurrentTable.Rows.Add(drCurrentRow);
        //            ViewState["KeyCurrentTable"] = dtCurrentTable;
        //            grvKey.DataSource = dtCurrentTable;
        //            grvKey.DataBind();

        //        }
        //    }
        //    else
        //    {
        //        Response.Write("ViewState is null");
        //    }


        //}

        //// Removing Row Data into Sale Order Item Grid Details
        //protected void lbRemoveRowKey_Click(object sender, EventArgs e)
        //{
        //    AddRemoveNewRowToGridKey();
        //    //  SetRemovePreviousData();
        //    LinkButton lb = (LinkButton)sender;
        //    GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
        //    int rowID = gvRow.RowIndex;
        //    if (ViewState["KeyCurrentTable"] != null)
        //    {

        //        DataTable dt = (DataTable)ViewState["KeyCurrentTable"];
        //        if (dt.Rows.Count > 1)
        //        {
        //            //Remove the Selected Row data and reset row number
        //            dt.Rows.Remove(dt.Rows[rowID]);

        //        }

        //        //Store the current data in ViewState for future reference
        //        ViewState["KeyCurrentTable"] = dt;

        //        grvKey.DataSource = dt;
        //        grvKey.DataBind();
        //    }
        //}

        //public void AddRemoveNewRowToGridKey()
        //{

        //    int rowIndex = 0;

        //    if (ViewState["KeyCurrentTable"] != null)
        //    {
        //        DataTable dtCurrentTable = (DataTable)ViewState["KeyCurrentTable"];
        //        DataRow drCurrentRow = null;
        //        int index = dtCurrentTable.Rows.Count;

        //        if (dtCurrentTable.Rows.Count > 0)
        //        {
        //            TextBox txtObjective = (TextBox)grvKey.Rows[index - 1].FindControl("txtObjective");
        //            //TextBox txtMeasurement = (TextBox)grvKey.Rows[index - 1].FindControl("txtMeasurement");
        //            DropDownList ddlMonitoringFrequency = (DropDownList)grvKey.Rows[index - 1].FindControl("ddlMonitoringFrequency");
        //            TextBox txtMonitoring = (TextBox)grvKey.Rows[index - 1].FindControl("txtMonitoring");
        //            TextBox txtCurrentLevel = (TextBox)grvKey.Rows[index - 1].FindControl("txtCurrentLevel");
        //            drCurrentRow = dtCurrentTable.NewRow();
        //            dtCurrentTable.Rows[index - 1]["Objective"] = txtObjective.Text;
        //            dtCurrentTable.Rows[index - 1]["Measurement"] = ddlMonitoringFrequency.SelectedValue;
        //            dtCurrentTable.Rows[index - 1]["Monitoring"] = txtMonitoring.Text;
        //            dtCurrentTable.Rows[index - 1]["Current_Level"] = txtCurrentLevel.Text;

        //            rowIndex += 1;

        //            ViewState["KeyCurrentTable"] = dtCurrentTable;
        //            grvKey.DataSource = dtCurrentTable;
        //            grvKey.DataBind();

        //        }
        //    }
        //    else
        //    {
        //        Response.Write("ViewState is null");
        //    }

        //    //    SetPreviousData();

        //}

        //protected void grvKey_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        DataTable dt = (DataTable)ViewState["KeyCurrentTable"];
        //        LinkButton lb = (LinkButton)e.Row.FindControl("lbRemoveRowLoane");
        //        if (lb != null)
        //        {
        //            if (dt.Rows.Count > 1)
        //            {
        //                if (e.Row.RowIndex == dt.Rows.Count - 1)
        //                {
        //                    lb.Visible = false;
        //                }
        //            }
        //            else
        //            {
        //                lb.Visible = false;
        //            }
        //        }
        //    }
        //}
        ////GridKey DataBound
        //protected void grvKey_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        DropDownList ddlMonitoringFrequency = (DropDownList)e.Row.FindControl("ddlMonitoringFrequency");
        //        TextBox txtMeasurement = (TextBox)e.Row.FindControl("txtMeasurement");

        //        ddlMonitoringFrequency.SelectedValue = txtMeasurement.Text;
        //    }
        //}

        //// Data Saving In Db

        //protected void btnSaveKey_Click(object sender, EventArgs e)
        //{
        //    try
        //    {



        //        if (grvKey.Rows.Count != 0)
        //        {
        //            foreach (GridViewRow objrow in grvKey.Rows)
        //            {
        //                if (objrow.RowType == DataControlRowType.DataRow)
        //                {
        //                    TextBox txtObjective = objrow.FindControl("txtObjective") as TextBox;
        //                    DropDownList ddlMonitoringFrequency = objrow.FindControl("ddlMonitoringFrequency") as DropDownList;
        //                    TextBox txtMonitoring = objrow.FindControl("txtMonitoring") as TextBox;
        //                    TextBox txtCurrentLevel = objrow.FindControl("txtCurrentLevel") as TextBox;
        //                    objEmpBAL.Emp_Code = txtEmp_Code.Text;
        //                    objEmpBAL.Objective_Goal = txtObjective.Text;
        //                    objEmpBAL.Measurement = ddlMonitoringFrequency.SelectedValue;
        //                    objEmpBAL.Monitoring_Frequency = txtMonitoring.Text;
        //                    objEmpBAL.Current_Level = txtCurrentLevel.Text;
        //                    objEmpBAL.Created_By = Session["User_Name"].ToString();
        //                    objEmpBAL.Created_Date = System.DateTime.Now;
        //                    objEmpBAL.Creation_Company = Session["ClientRegistrationNo"].ToString();
        //                    objEmpBAL.Modified_By = Session["User_Name"].ToString();
        //                    objEmpBAL.Modified_Date = System.DateTime.Now;
        //                    objEmpBAL.Parameter = 1;
        //                    if (objEmpBAL.Insert_Employee_KeyPer_Information() != 0)
        //                    {
        //                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);

        //                    }
        //                }
        //            }

        //        }
        //        SetInitialRowKey();

        //    }
        //    catch (Exception ex)
        //    {
        //        string msg = ex.Message;
        //    }
        //}

        //protected void btnResetKey_Click(object sender, EventArgs e)
        //{
        //    SetInitialRowKey();
        //    txtEmp_Code.Text = "";
        //    txtFirstName.Text = "";
        //    txtMiddleName.Text = "";
        //    ddlUSerRole.SelectedValue = "Select";
        //    ddlEmpStatus.SelectedValue = "Select";
        //}

        #endregion


        #region Professional Details (Created by Divya)

        /// <summary>
        /// Professional Details Grid Adding New Record 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNewRecordPRof_Click(object sender, EventArgs e)
        {
            // Adding Empty Row Data into Customer Address Details

            int Count = (int)grvProfessional.Rows.Count;

            TextBox Employer = (TextBox)grvProfessional.Rows[Count - 1].FindControl("txtEmployer") as TextBox;
            TextBox Position = (TextBox)grvProfessional.Rows[Count - 1].FindControl("txtPosition") as TextBox;
            TextBox PeriodFrom = (TextBox)grvProfessional.Rows[Count - 1].FindControl("txtPeriodFrom") as TextBox;
            TextBox PeriodTo = (TextBox)grvProfessional.Rows[Count - 1].FindControl("txtPeriodTo") as TextBox;
            TextBox TotalDuration = (TextBox)grvProfessional.Rows[Count - 1].FindControl("txtTotalDuration") as TextBox;
            TextBox Attach = (TextBox)grvProfessional.Rows[Count - 1].FindControl("txtLicense") as TextBox;


            //   MyTabs.ActiveTab = tabFour;

            //  MyTabs.Visible = true;

            if (Employer.Text == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Enter Name of the Employer');", true);
                Employer.Focus();
            }
            else if (Position.Text == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Enter Position Held');", true);
                Position.Focus();

            }
            else if (PeriodFrom.Text == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Enter Professional Period From Date');", true);
                PeriodFrom.Focus();
            }
            else if (PeriodTo.Text == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Enter Professional Period To Date');", true);
                PeriodTo.Focus();
            }

            else
            {

                AddNewRowToProfessionalGrid();

                int aa = (int)grvProfessional.Rows.Count;
                TextBox ProfessionalEmployer = (TextBox)grvProfessional.Rows[aa - 1].FindControl("txtEmployer") as TextBox;
                ProfessionalEmployer.Focus();
            }
        }

        /// <summary>
        /// Grid Fileds Removing Fro Professional Experinces Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbRemoveRowProf_Click(object sender, EventArgs e)
        {
            AddRemoveNewRowToProfessionalGrid();
            //  SetRemovePreviousData();
            LinkButton lb = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow.RowIndex;
            if (ViewState["ProfessionalTable"] != null)
            {

                DataTable dt = (DataTable)ViewState["ProfessionalTable"];
                if (dt.Rows.Count > 1)
                {
                    //Remove the Selected Row data and reset row number
                    dt.Rows.Remove(dt.Rows[rowID]);
                }

                //Store the current data in ViewState for future reference
                ViewState["ProfessionalTable"] = dt;

                grvProfessional.DataSource = dt;
                grvProfessional.DataBind();


            }

        }

        #region Grid Details For Professional (Methods)

        /// <summary>
        /// Professional Details
        /// </summary>
        public void SetInitialRowgrvProfessional()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("Emp_Name", typeof(string)));
            dt.Columns.Add(new DataColumn("Emp_Designation", typeof(string)));

            dt.Columns.Add(new DataColumn("Period_From", typeof(string)));
            dt.Columns.Add(new DataColumn("Period_To", typeof(string)));

            dt.Columns.Add(new DataColumn("Total_Duration", typeof(string)));

            // dt.Columns.Add(new DataColumn("Attach_File", typeof(string)));


            dr = dt.NewRow();
            dr["Emp_Name"] = string.Empty;
            dr["Emp_Designation"] = string.Empty;
            dr["Period_From"] = string.Empty;

            dr["Period_To"] = string.Empty;
            dr["Total_Duration"] = string.Empty;

            //dr["Attach_File"] = string.Empty;





            dt.Rows.Add(dr);
            ViewState["ProfessionalTable"] = dt;
            grvProfessional.DataSource = dt;
            grvProfessional.DataBind();
        }



        /// <summary>
        /// Add New Record For Professional Grid
        /// </summary>
        public void AddNewRowToProfessionalGrid()
        {
            int rowIndex = 0;
            if (ViewState["ProfessionalTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["ProfessionalTable"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        TextBox Employer = (TextBox)grvProfessional.Rows[rowIndex].Cells[0].FindControl("txtEmployer");
                        TextBox Position = (TextBox)grvProfessional.Rows[rowIndex].Cells[1].FindControl("txtPosition");
                        TextBox PeriodFrom = (TextBox)grvProfessional.Rows[rowIndex].Cells[2].FindControl("txtPeriodFrom");
                        TextBox PeriodTo = (TextBox)grvProfessional.Rows[rowIndex].Cells[3].FindControl("txtPeriodTo");
                        TextBox TotalDuration = (TextBox)grvProfessional.Rows[rowIndex].Cells[4].FindControl("txtTotalDuration");
                        // TextBox Attach = (TextBox)grvProfessional.Rows[rowIndex].Cells[5].FindControl("txtLicense");


                        drCurrentRow = dtCurrentTable.NewRow();
                        dtCurrentTable.Rows[i - 1]["Emp_Name"] = Employer.Text;
                        dtCurrentTable.Rows[i - 1]["Emp_Designation"] = Position.Text;
                        dtCurrentTable.Rows[i - 1]["Period_From"] = PeriodFrom.Text;
                        dtCurrentTable.Rows[i - 1]["Period_To"] = PeriodTo.Text;
                        dtCurrentTable.Rows[i - 1]["Total_Duration"] = TotalDuration.Text;
                        // dtCurrentTable.Rows[i - 1]["Attach_File"] = Attach.Text;


                        rowIndex += 1;
                    }

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["ProfessionalTable"] = dtCurrentTable;
                    grvProfessional.DataSource = dtCurrentTable;
                    grvProfessional.DataBind();

                }
            }
            else
            {
                Response.Write("ViewState is null");
            }

            SetPreviousDataProfessional();
        }

        /// <summary>
        /// Set Previous Data Professional Grid 
        /// </summary>
        public void SetPreviousDataProfessional()
        {
            int rowIndex = 0;
            if (ViewState["ProfessionalTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["ProfessionalTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        //Set the Previous Selected Items on Each DropDownList on Postbacks


                        TextBox Employer = (TextBox)grvProfessional.Rows[rowIndex].Cells[0].FindControl("txtEmployer");
                        TextBox Position = (TextBox)grvProfessional.Rows[rowIndex].Cells[1].FindControl("txtPosition");
                        TextBox PeriodFrom = (TextBox)grvProfessional.Rows[rowIndex].Cells[2].FindControl("txtPeriodFrom");
                        TextBox PeriodTo = (TextBox)grvProfessional.Rows[rowIndex].Cells[3].FindControl("txtPeriodTo");
                        TextBox TotalDuration = (TextBox)grvProfessional.Rows[rowIndex].Cells[4].FindControl("txtTotalDuration");
                        // TextBox Attach = (TextBox)grvProfessional.Rows[rowIndex].Cells[5].FindControl("txtLicense");


                        if (i < dt.Rows.Count - 1)
                        {
                            Employer.Text = dt.Rows[i]["Emp_Name"].ToString();
                            Position.Text = dt.Rows[i]["Emp_Designation"].ToString();
                            PeriodFrom.Text = dt.Rows[i]["Period_From"].ToString();
                            PeriodTo.Text = dt.Rows[i]["Period_To"].ToString();
                            TotalDuration.Text = dt.Rows[i]["Total_Duration"].ToString();
                            // Attach.Text = dt.Rows[i]["Attach_File"].ToString();


                        }

                        rowIndex++;
                    }
                }
            }

        }


        /// <summary>
        /// Removing added  Row Data Into Professional Details 
        /// Created By Divya
        /// </summary>
        /// <param name="sender"></param>

        // // add new row remove
        public void AddRemoveNewRowToProfessionalGrid()
        {

            int rowIndex = 0;

            if (ViewState["ProfessionalTable"] != null)
            {
                DataTable dtCurrentTable = null;
                dtCurrentTable = (DataTable)ViewState["ProfessionalTable"];
                DataRow drCurrentRow = null;
                int index = dtCurrentTable.Rows.Count;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    TextBox Employer = (TextBox)grvProfessional.Rows[index - 1].Cells[0].FindControl("txtEmployer");
                    TextBox Position = (TextBox)grvProfessional.Rows[index - 1].Cells[1].FindControl("txtPosition");
                    TextBox PeriodFrom = (TextBox)grvProfessional.Rows[index - 1].Cells[2].FindControl("txtPeriodFrom");
                    TextBox PeriodTo = (TextBox)grvProfessional.Rows[index - 1].Cells[3].FindControl("txtPeriodTo");
                    TextBox TotalDuration = (TextBox)grvProfessional.Rows[index - 1].Cells[4].FindControl("txtTotalDuration");
                    //TextBox Attach = (TextBox)grvProfessional.Rows[index - 1].Cells[5].FindControl("txtLicense");




                    drCurrentRow = dtCurrentTable.NewRow();
                    dtCurrentTable.Rows[index - 1]["Emp_Name"] = Employer.Text;
                    dtCurrentTable.Rows[index - 1]["Emp_Designation"] = Position.Text;
                    dtCurrentTable.Rows[index - 1]["Period_From"] = PeriodFrom.Text;
                    dtCurrentTable.Rows[index - 1]["Period_To"] = PeriodTo.Text;
                    dtCurrentTable.Rows[index - 1]["Total_Duration"] = TotalDuration.Text;
                    //dtCurrentTable.Rows[index - 1]["Attach_File"] = Attach.Text;


                    rowIndex += 1;
                    //  }

                    // dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["ProfessionalTable"] = dtCurrentTable;
                    grvProfessional.DataSource = dtCurrentTable;
                    grvProfessional.DataBind();

                }
            }
            else
            {
                Response.Write("ViewState is null");
            }

            SetPreviousProfessionalData();

        }

        /// <summary>
        /// Set reomve Row Data Into Professional Details 
        /// Created By Divya
        /// </summary>
        /// <param name="sender"></param>

        // set remove row 
        private void SetPreviousProfessionalData()
        {

            int rowIndex = 0;
            if (ViewState["ProfessionalTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["ProfessionalTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        //Set the Previous Selected Items on Each DropDownList on Postbacks

                        TextBox Employer = (TextBox)grvProfessional.Rows[rowIndex].Cells[0].FindControl("txtEmployer");
                        TextBox Position = (TextBox)grvProfessional.Rows[rowIndex].Cells[1].FindControl("txtPosition");
                        TextBox PeriodFrom = (TextBox)grvProfessional.Rows[rowIndex].Cells[2].FindControl("txtPeriodFrom");
                        TextBox PeriodTo = (TextBox)grvProfessional.Rows[rowIndex].Cells[3].FindControl("txtPeriodTo");
                        TextBox TotalDuration = (TextBox)grvProfessional.Rows[rowIndex].Cells[4].FindControl("txtTotalDuration");
                        //TextBox Attach = (TextBox)grvProfessional.Rows[rowIndex].Cells[5].FindControl("txtLicense");


                        if (i < dt.Rows.Count - 1)
                        {
                            Employer.Text = dt.Rows[i]["Emp_Name"].ToString();
                            Position.Text = dt.Rows[i]["Emp_Designation"].ToString();
                            PeriodFrom.Text = dt.Rows[i]["Period_From"].ToString();
                            PeriodTo.Text = dt.Rows[i]["Period_To"].ToString();
                            TotalDuration.Text = dt.Rows[i]["Total_Duration"].ToString();
                            //Attach.Text = dt.Rows[i]["Attach_File"].ToString();

                        }

                        rowIndex++;
                    }
                }
            }


        }

        /// <summary>
        /// Clear All Fields For Professional Experinces
        /// </summary>
        public void ClearAllFieldsProfessional()
        {
            SetInitialRowgrvProfessional();

        }
        #endregion

        #region Grid Details For Professional (Event)

        /// <summary>
        /// Period From Date Text Change Event 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtPeriodFrom_OnTextChanged(object sender, EventArgs e)
        {
            try
            {
                TextBox txtval = (TextBox)sender;
                GridViewRow gvr = (GridViewRow)txtval.NamingContainer;
                int index = gvr.RowIndex;

                TextBox PeriodFrom = (TextBox)grvProfessional.Rows[index].FindControl("txtPeriodFrom");
                TextBox PeriodTo = (TextBox)grvProfessional.Rows[index].FindControl("txtPeriodTo");
                TextBox TotalDuration = (TextBox)grvProfessional.Rows[index].FindControl("txtTotalDuration") as TextBox;

                if (PeriodFrom.Text != "" && PeriodTo.Text != "")
                {
                    string d1 = PeriodFrom.Text.Substring(0, 2);
                    string m1 = PeriodFrom.Text.Substring(3, 2);
                    string y1 = PeriodFrom.Text.Substring(6, 4);
                    DateTime FromYear = Convert.ToDateTime(m1 + '-' + d1 + '-' + y1);
                    //objEmpBAL.Period_From = PeriodFromDate;

                    string d2 = PeriodTo.Text.Substring(0, 2);
                    string m2 = PeriodTo.Text.Substring(3, 2);
                    string y2 = PeriodTo.Text.Substring(6, 4);
                    DateTime ToYear = Convert.ToDateTime(m2 + '-' + d2 + '-' + y2);

                    //years
                    int Years = ToYear.Year - FromYear.Year;
                    //months
                    int month = ToYear.Month - FromYear.Month;

                    //Total Months
                    int TotalMonths = (Years * 12) + month;

                    if (Years > 0)
                    {
                        //Assining values to td tags
                        TotalDuration.Text = Years + "." + month + "Months";
                    }
                    else
                    {
                        //Assining values to td tags
                        TotalDuration.Text = month + "Months";
                    }

                    //      tdMonths.InnerText = Convert.ToString(TotalMonths);
                    //   tdDays.InnerText = Convert.ToString(Days);
                    //  tdHrs.InnerText = Convert.ToString(TotalHours);
                    //  tdminuts.InnerText = Convert.ToString(TotalMinutes);
                    //  tdseconds.InnerText = Convert.ToString(TotalSeconds);
                    //  tdmileSec.InnerText = Convert.ToString(TotalMileSeconds);
                    //  tblResults.Visible = true;
                }
            }
            catch (Exception ex)
            {
                string abc = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + abc + "');", true);
            }
            finally
            {

            }
        }


        /// <summary>
        /// Period to Date Text Change Event 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtPeriodTo_OnTextChanged(object sender, EventArgs e)
        {
            try
            {
                TextBox txtval = (TextBox)sender;
                GridViewRow gvr = (GridViewRow)txtval.NamingContainer;
                int index = gvr.RowIndex;

                TextBox PeriodFrom = (TextBox)grvProfessional.Rows[index].FindControl("txtPeriodFrom");
                TextBox PeriodTo = (TextBox)grvProfessional.Rows[index].FindControl("txtPeriodTo");
                TextBox TotalDuration = (TextBox)grvProfessional.Rows[index].FindControl("txtTotalDuration") as TextBox;

                if (PeriodFrom.Text != "" && PeriodTo.Text != "")
                {
                    string d1 = PeriodFrom.Text.Substring(0, 2);
                    string m1 = PeriodFrom.Text.Substring(3, 2);
                    string y1 = PeriodFrom.Text.Substring(6, 4);
                    DateTime FromYear = Convert.ToDateTime(m1 + '-' + d1 + '-' + y1);
                    //objEmpBAL.Period_From = PeriodFromDate;

                    string d2 = PeriodTo.Text.Substring(0, 2);
                    string m2 = PeriodTo.Text.Substring(3, 2);
                    string y2 = PeriodTo.Text.Substring(6, 4);
                    DateTime ToYear = Convert.ToDateTime(m2 + '-' + d2 + '-' + y2);
                    //  objEmpBAL.Period_To = PeriodToDate;
                    //Storing input Dates
                    // DateTime FromYear = Convert.ToDateTime(PeriodFrom.Text);
                    // DateTime ToYear = Convert.ToDateTime(PeriodTo.Text);

                    //Creating object of TimeSpan Class
                    //   TimeSpan objTimeSpan = ToYear - FromYear;
                    //years
                    int Years = ToYear.Year - FromYear.Year;
                    //months
                    int month = ToYear.Month - FromYear.Month;
                    //TotalDays
                    //  double Days = Convert.ToDouble(objTimeSpan.TotalDays);
                    //Total Months
                    int TotalMonths = (Years * 12) + month;
                    //Total Hours
                    //   double TotalHours = objTimeSpan.TotalHours;
                    //Total Minutes
                    //  double TotalMinutes = objTimeSpan.TotalMinutes;
                    //Total Seconds
                    //  double TotalSeconds = objTimeSpan.TotalSeconds;
                    //Total Mile Seconds
                    //  double TotalMileSeconds = objTimeSpan.TotalMilliseconds;
                    if (Years > 0)
                    {
                        //Assining values to td tags
                        TotalDuration.Text = Years + "." + month + "Months";
                    }
                    else
                    {
                        //Assining values to td tags
                        TotalDuration.Text = month + "Months";
                    }

                    //      tdMonths.InnerText = Convert.ToString(TotalMonths);
                    //   tdDays.InnerText = Convert.ToString(Days);
                    //  tdHrs.InnerText = Convert.ToString(TotalHours);
                    //  tdminuts.InnerText = Convert.ToString(TotalMinutes);
                    //  tdseconds.InnerText = Convert.ToString(TotalSeconds);
                    //  tdmileSec.InnerText = Convert.ToString(TotalMileSeconds);
                    //  tblResults.Visible = true;
                }
            }
            catch (Exception ex)
            {
                string abc = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + abc + "');", true);
            }
            finally
            {

            }
        }

        #endregion

        #region Professional Events

        /// <summary>
        /// Professional Data Saving Event 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveProfession_Click(object sender, EventArgs e)
        {
            SavingProfession();
        }
        protected void btnUpdateProfession_Click(object sender, EventArgs e)
        {
            string Creation_Company = Session["ClientRegistrationNo"].ToString();
            string Emp_Code = (string)Session["EmployeeFamilyDetailsCode"];
            var SUM = (from ST in ObjDAL.HRMS_Employee_Master_Professional_Experiences
                       where ST.Creation_Company == Creation_Company && ST.Emp_Code == Emp_Code
                       select ST).ToList();
            ObjDAL.HRMS_Employee_Master_Professional_Experiences.DeleteAllOnSubmit(SUM);
            ObjDAL.SubmitChanges();
            SavingProfession();
        }

        private void SavingProfession()
        {
            try
            {
                int Count = 0;
                foreach (GridViewRow objrow in grvProfessional.Rows)
                {
                    if (objrow.RowType == DataControlRowType.DataRow)
                    {
                        // TextBox Prod_Code = objrow.FindControl("txtProductName") as TextBox;

                        TextBox Employer = objrow.FindControl("txtEmployer") as TextBox;

                        TextBox Position = objrow.FindControl("txtPosition") as TextBox;
                        TextBox PeriodFrom = objrow.FindControl("txtPeriodFrom") as TextBox;
                        TextBox PeriodTo = objrow.FindControl("txtPeriodTo") as TextBox;
                        TextBox TotalDuration = objrow.FindControl("txtTotalDuration") as TextBox;
                        TextBox Attach = objrow.FindControl("txtLicense") as TextBox;

                        if (Employer.Text != "")
                        {
                            objEmpBAL.Emp_Code = txtEmp_Code.Text;
                            objEmpBAL.Emp_Name = Employer.Text;
                            objEmpBAL.Emp_Designation = Position.Text;

                            string d1 = PeriodFrom.Text.Substring(0, 2);
                            string m1 = PeriodFrom.Text.Substring(3, 2);
                            string y1 = PeriodFrom.Text.Substring(6, 4);
                            DateTime PeriodFromDate = Convert.ToDateTime(m1 + '-' + d1 + '-' + y1);
                            objEmpBAL.Period_From = PeriodFromDate;

                            string d2 = PeriodTo.Text.Substring(0, 2);
                            string m2 = PeriodTo.Text.Substring(3, 2);
                            string y2 = PeriodTo.Text.Substring(6, 4);
                            DateTime PeriodToDate = Convert.ToDateTime(m1 + '-' + d1 + '-' + y1);
                            objEmpBAL.Period_To = PeriodToDate;

                            objEmpBAL.Total_Duration = TotalDuration.Text;
                            objEmpBAL.Attach_File = "";
                            objEmpBAL.Creation_Company = Session["ClientRegistrationNo"].ToString();
                            objEmpBAL.Created_By = Session["User_Name"].ToString();
                            objEmpBAL.Created_Date = System.DateTime.Now;
                            objEmpBAL.Modified_By = Session["User_Name"].ToString();
                            objEmpBAL.Modified_Date = System.DateTime.Now;
                            objEmpBAL.Parameter = 1;
                            objEmpBAL.Insert_Update_ProfessionalExperience();
                            Count = Count + 1;
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Employeer Name SHould Not be Empty..');", true);

                        }







                        if (Count == grvProfessional.Rows.Count)
                        {
                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Saved Successfully');", true);
                            //ClearAllFieldsProfessional();

                        }
                        else
                        {
                            //ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data not Saved ');", true);

                        }


                    }


                }

            }
            catch (Exception ex)
            {
                string abc = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + abc + "');", true);
            }

            finally
            {

            }
        }
        /// <summary>
        /// Reset Button Event For Professional Expernices
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnResetProfession_Click(object sender, EventArgs e)
        {
            try
            {
                ClearAllFieldsProfessional();
            }
            catch (Exception ex)
            {
                string abc = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + abc + "');", true);
            }
            finally
            {

            }
        }


        #endregion


        //Editing Professional Details
        public void Edit_Employee_Professional_Details(string empcode, string Creation_Company)
        {
            try
            {

                List<HRMS_Employee_Master_Professional_Experience> objEmpDetails = objEmpBAL.Get_Employee_Professional_Details(empcode, Creation_Company);

                if (objEmpDetails.Count > 0)
                {
                    grvProfessional.DataSource = objEmpDetails;
                    grvProfessional.DataBind();

                    DataTable dt = new DataTable();
                    dt.Columns.Add(new DataColumn("Emp_Name", typeof(string)));
                    dt.Columns.Add(new DataColumn("Emp_Designation", typeof(string)));
                    dt.Columns.Add(new DataColumn("Period_From", typeof(string)));

                    dt.Columns.Add(new DataColumn("Period_To", typeof(string)));
                    dt.Columns.Add(new DataColumn("Total_Duration", typeof(string)));


                    for (int i = 0; i < grvProfessional.Rows.Count; i++)
                    {
                        TextBox txtEmployer = grvProfessional.Rows[i].FindControl("txtEmployer") as TextBox;
                        TextBox txtPosition = grvProfessional.Rows[i].FindControl("txtPosition") as TextBox;
                        TextBox txtPeriodFrom = grvProfessional.Rows[i].FindControl("txtPeriodFrom") as TextBox;
                        TextBox txtPeriodTo = grvProfessional.Rows[i].FindControl("txtPeriodTo") as TextBox;
                        TextBox txtTotalDuration = grvProfessional.Rows[i].FindControl("txtTotalDuration") as TextBox;



                        string name = txtEmployer.Text;
                        string position = txtPosition.Text;
                        string from = txtPeriodFrom.Text;
                        string to = txtPeriodTo.Text;
                        string due = txtTotalDuration.Text;



                        //TextBox MinPriceAllowedBranchLevel = grdProductItems.Rows[i].FindControl("txtMinPriceAllowedBranchLevel") as TextBox;
                        dt.Rows.Add(name, position, from, to, due);

                    }
                    ViewState["ProfessionalTable"] = dt;
                    if (Session["User_Type"].ToString() == "Employees")
                    {

                    }
                    else
                    {

                        Session["EmployeeFamilyDetailsCode"] = null;
                    }
                }
                //btnUpdatePersonal.Visible = true;
                //btnSavePersonal.Visible = false;
                //Session["EmployeeDetailsCode"] = null;

                else
                {
                    //ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('No Data Found ');", true);
                    if (Session["User_Type"].ToString() == "Employees")
                    {

                    }
                    else
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }

        #endregion


        // binding checkboxes in checkbox list based on ShortNames In Additions Master table
        public void OTList()
        {
            try
            {
                string Creation_Company = Session["ClientRegistrationNo"].ToString();
                // string Company_Name = Session["Creation_Company"].ToString();

                List<HRMS_Additions_Master> objShortInfoDetails = (from Short in ObjDAL.HRMS_Additions_Masters
                                                                   where Short.Creation_Company == Creation_Company
                                                                   select Short).ToList();
                if (objShortInfoDetails.Count > 0)
                {
                    chlHeadsLists.DataSource = objShortInfoDetails;
                    chlHeadsLists.DataTextField = "Short_Name";
                    chlHeadsLists.DataValueField = "Short_Name";
                    chlHeadsLists.DataBind();
                }
                else
                {
                    chlHeadsLists.Items.Insert(0, new ListItem("", "")); //updated code
                }
            }
            catch (Exception ex)
            {
                string Error = ex.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + Error + "');", true);
            }
            finally
            {
            }
        }

        // checkbox list changed event
        protected void chlHeadsLists_SelectedIndexChanged(object sender, EventArgs e)
        {

            // Create the list to store.
            List<String> StrList = new List<string>();
            // Loop through each item.
            foreach (ListItem item in chlHeadsLists.Items)
            {
                if (item.Selected)
                {
                    // If the item is selected, add the value to the list.
                    StrList.Add(item.Value);
                }
                else
                {
                    // Item is not selected, do something else.
                }
            }
            // Join the string together using the ; delimiter.
            String Str = String.Join(",", StrList.ToArray());
            ViewState["Applicable_Heads"] = Str;


        }

        #region Educational Qualifications

        #region Educational Details

        /// <summary>
        /// Education Details
        /// </summary>grvSkill
        public void SetInitialRowEducation()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("Qualification_Certification", typeof(string)));
            dt.Columns.Add(new DataColumn("Institute_University", typeof(string)));
            dt.Columns.Add(new DataColumn("Year", typeof(string)));
            dt.Columns.Add(new DataColumn("Percentage", typeof(string)));
            // dt.Columns.Add(new DataColumn("Attach_File", typeof(string)));

            dr = dt.NewRow();
            dr["Qualification_Certification"] = string.Empty;
            dr["Institute_University"] = string.Empty;
            dr["Year"] = string.Empty;
            dr["Percentage"] = string.Empty;
            //dr["Attach_File"] = string.Empty;


            dt.Rows.Add(dr);
            ViewState["CurrentTableEducation"] = dt;
            grdEDucation.DataSource = dt;
            grdEDucation.DataBind();
        }

        // // // This Is Related To Grid View
        protected void btnNewRecordEducation_Click(object sender, EventArgs e)
        {
            // Adding Empty Row Data into Customer Address Details

            int Count = (int)grdEDucation.Rows.Count;
            //DropDownList BankName = (DropDownList)grdEDucation.Rows[Count - 1].FindControl("ddlBankName") as DropDownList;
            //TextBox AccountNumber = (TextBox)grdEDucation.Rows[Count - 1].FindControl("txtAccountNumber") as TextBox;
            //TextBox Branch = (TextBox)grdEDucation.Rows[Count - 1].FindControl("txtBranch") as TextBox;

            //Set the Previous Selected Items on Each DropDownList on Postbacks
            TextBox BankName = (TextBox)grdEDucation.Rows[Count - 1].FindControl("txtQualification");
            TextBox AccountNumber = (TextBox)grdEDucation.Rows[Count - 1].FindControl("txtInstitute_University");
            TextBox Branch = (TextBox)grdEDucation.Rows[Count - 1].FindControl("txtYearOfPass");
            TextBox PAN = (TextBox)grdEDucation.Rows[Count - 1].FindControl("txtMarks_GPA");
            TextBox Attachment = (TextBox)grdEDucation.Rows[Count - 1].FindControl("txtAttach_Certificate");

            if (BankName.Text == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Select Bank Name');", true);
                BankName.Focus();

                //txtBankName.Focus();
            }
            else if (AccountNumber.Text == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Enter Account Number');", true);
                AccountNumber.Focus();
            }
            else if (Branch.Text == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Enter Branch');", true);
                Branch.Focus();
            }

            else
            {
                AddEducationNewRowToGrid();
                int aa = (int)grdEDucation.Rows.Count;
                TextBox box = (TextBox)grdEDucation.Rows[aa - 1].FindControl("txtbranchId") as TextBox;
                BankName.Focus();
            }
        }

        // // // Add New Record To Existing Records
        //Add New Record

        public void AddEducationNewRowToGrid()
        {
            int rowIndex = 0;
            if (ViewState["CurrentTableEducation"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTableEducation"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //Set the Previous Selected Items on Each DropDownList on Postbacks
                        TextBox BankName = (TextBox)grdEDucation.Rows[rowIndex].Cells[0].FindControl("txtQualification");
                        TextBox AccountNumber = (TextBox)grdEDucation.Rows[rowIndex].Cells[1].FindControl("txtInstitute_University");
                        TextBox Branch = (TextBox)grdEDucation.Rows[rowIndex].Cells[2].FindControl("txtYearOfPass");
                        TextBox PAN = (TextBox)grdEDucation.Rows[rowIndex].Cells[3].FindControl("txtMarks_GPA");
                        // TextBox Attachment = (TextBox)grdEDucation.Rows[rowIndex].Cells[4].FindControl("txtAttach_Certificate");

                        drCurrentRow = dtCurrentTable.NewRow();
                        dtCurrentTable.Rows[i - 1]["Qualification_Certification"] = BankName.Text;
                        dtCurrentTable.Rows[i - 1]["Institute_University"] = AccountNumber.Text;
                        dtCurrentTable.Rows[i - 1]["Year"] = Branch.Text;
                        dtCurrentTable.Rows[i - 1]["Percentage"] = PAN.Text;
                        // dtCurrentTable.Rows[i - 1]["Attach_File"] = "";
                        rowIndex += 1;
                    }

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["CurrentTableEducation"] = dtCurrentTable;
                    grdEDucation.DataSource = dtCurrentTable;
                    grdEDucation.DataBind();

                }
            }
            else
            {
                Response.Write("ViewState is null");
            }
            SetPreviousEducationData();
        }

        // // // Set To Previous Record To Gridview
        //Set Previous Record on Grid view
        private void SetPreviousEducationData()
        {
            int rowIndex = 0;
            if (ViewState["CurrentTableEducation"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTableEducation"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        //Set the Previous Selected Items on Each DropDownList on Postbacks
                        TextBox BankName = (TextBox)grdEDucation.Rows[rowIndex].Cells[0].FindControl("txtQualification");
                        TextBox AccountNumber = (TextBox)grdEDucation.Rows[rowIndex].Cells[1].FindControl("txtInstitute_University");
                        TextBox Branch = (TextBox)grdEDucation.Rows[rowIndex].Cells[2].FindControl("txtYearOfPass");
                        TextBox PAN = (TextBox)grdEDucation.Rows[rowIndex].Cells[3].FindControl("txtMarks_GPA");
                        //TextBox Attachment = (TextBox)grdEDucation.Rows[rowIndex].Cells[4].FindControl("txtAttach_Certificate");
                        if (i < dt.Rows.Count - 1)
                        {
                            BankName.Text = dt.Rows[i]["Qualification_Certification"].ToString();
                            AccountNumber.Text = dt.Rows[i]["Institute_University"].ToString();
                            Branch.Text = dt.Rows[i]["Year"].ToString();
                            PAN.Text = dt.Rows[i]["Percentage"].ToString();
                            // Attachment.Text = dt.Rows[i]["Attach_File"].ToString();
                        }
                        rowIndex++;
                    }
                }
            }
        }

        ///Remove Row Data Into purchase Order Item Grid Details 

        protected void lkbRemoveEducationRecord_Click(object sender, EventArgs e)
        {
            AddEducationRemoveNewRowToGrid();
            LinkButton lb = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow.RowIndex;
            if (ViewState["CurrentTableEducation"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTableEducation"];
                if (dt.Rows.Count > 1)
                {
                    dt.Rows.Remove(dt.Rows[rowID]);    //Remove the Selected Row data and reset row number
                }
                ViewState["CurrentTableEducation"] = dt;     //Store the current data in ViewState for future reference
                grdEDucation.DataSource = dt;
                grdEDucation.DataBind();
                //Grid_Footer_Calc();
                int aa = (int)grdEDucation.Rows.Count;
                TextBox box = (TextBox)grdEDucation.Rows[aa - 1].FindControl("txtQualification") as TextBox;
                if (box.Text == "")
                {
                    box.Focus();
                }
                else
                {
                    Button btn = (Button)grdEDucation.FooterRow.FindControl("btnNewRecordEducation") as Button;
                    btn.Focus();
                }
            }
        }

        public void AddEducationRemoveNewRowToGrid()
        {
            int rowIndex = 0;
            if (ViewState["CurrentTableEducation"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTableEducation"];
                DataRow drCurrentRow = null;
                int index = dtCurrentTable.Rows.Count;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    TextBox txtProduct_Code = (TextBox)grdEDucation.Rows[index - 1].Cells[0].FindControl("txtQualification");
                    TextBox ddlProduct_Name = (TextBox)grdEDucation.Rows[index - 1].Cells[1].FindControl("txtInstitute_University");
                    TextBox txtProduct_Specification = (TextBox)grdEDucation.Rows[index - 1].Cells[2].FindControl("txtYearOfPass");
                    TextBox txtUOM = (TextBox)grdEDucation.Rows[index - 1].Cells[3].FindControl("txtMarks_GPA");
                    //TextBox txtQty = (TextBox)grdEDucation.Rows[index - 1].Cells[4].FindControl("txtAttach_Certificate");

                    drCurrentRow = dtCurrentTable.NewRow();
                    dtCurrentTable.Rows[index - 1]["Qualification_Certification"] = txtProduct_Code.Text;
                    dtCurrentTable.Rows[index - 1]["Institute_University"] = ddlProduct_Name.Text;
                    dtCurrentTable.Rows[index - 1]["Year"] = txtProduct_Specification.Text;
                    dtCurrentTable.Rows[index - 1]["Percentage"] = txtUOM.Text;
                    //dtCurrentTable.Rows[index - 1]["Attach_File"] = txtQty.Text;

                    rowIndex += 1;
                    ViewState["CurrentTableEducation"] = dtCurrentTable;
                    grdEDucation.DataSource = dtCurrentTable;
                    grdEDucation.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }
            //SetInitialRowEducation();
        }


        //Editing Educational Cer Details
        public void Edit_Employee_Education_Cer_Details(string empcode, string Creation_Company)
        {
            try
            {

                List<HRMS_Employee_Master_Educational_Certification_Detail> objEmpDetails = objEmpBAL.Get_Employee_Eductional_Cer_Details(empcode, Creation_Company);

                if (objEmpDetails.Count > 0)
                {
                    grdEDucation.DataSource = objEmpDetails;
                    grdEDucation.DataBind();

                    DataTable dt = new DataTable();
                    dt.Columns.Add(new DataColumn("Qualification_Certification", typeof(string)));
                    dt.Columns.Add(new DataColumn("Institute_University", typeof(string)));
                    dt.Columns.Add(new DataColumn("Year", typeof(string)));

                    dt.Columns.Add(new DataColumn("Percentage", typeof(string)));



                    for (int i = 0; i < grdEDucation.Rows.Count; i++)
                    {
                        TextBox txtQualification = grdEDucation.Rows[i].FindControl("txtQualification") as TextBox;
                        TextBox txtInstitute_University = grdEDucation.Rows[i].FindControl("txtInstitute_University") as TextBox;
                        TextBox txtYearOfPass = grdEDucation.Rows[i].FindControl("txtYearOfPass") as TextBox;
                        TextBox txtMarks_GPA = grdEDucation.Rows[i].FindControl("txtMarks_GPA") as TextBox;

                        string qualification = txtQualification.Text;
                        string institute = txtInstitute_University.Text;
                        string year = txtYearOfPass.Text;
                        string GPA = txtMarks_GPA.Text;




                        //TextBox MinPriceAllowedBranchLevel = grdProductItems.Rows[i].FindControl("txtMinPriceAllowedBranchLevel") as TextBox;
                        dt.Rows.Add(qualification, institute, year, GPA);
                    }

                    ViewState["CurrentTableEducation"] = dt;
                    if (Session["User_Type"].ToString() == "Employees")
                    {

                    }
                    else
                    {

                        Session["EmployeeFamilyDetailsCode"] = null;
                    }
                }
                //btnUpdatePersonal.Visible = true;
                //btnSavePersonal.Visible = false;
                //Session["EmployeeDetailsCode"] = null;

                else
                {
                    //ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('No Data Found ');", true);
                    SetInitialRowCertifications();
                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }
        #endregion

        #region Skill Matrix Grid Operations
        /// <summary>
        /// Skill Details Set Initial Row
        /// </summary>
        public void SetInitialRowEducationSkill()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("Skill_Name", typeof(string)));
            dt.Columns.Add(new DataColumn("Skill_Level", typeof(string)));
            dt.Columns.Add(new DataColumn("Description", typeof(string)));

            dr = dt.NewRow();
            dr["Skill_Name"] = string.Empty;
            dr["Skill_Level"] = string.Empty;
            dr["Description"] = string.Empty;

            dt.Rows.Add(dr);
            ViewState["CurrentTableEducationSkill"] = dt;
            grvSkill.DataSource = dt;
            grvSkill.DataBind();
        }
        // Grid Row bound
        protected void grvSkill_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TextBox txtSkillName = (TextBox)e.Row.FindControl("txtSkillName");
                DropDownList ddlSkill_Name = (DropDownList)e.Row.FindControl("ddlSkill_Name");
                DropDownList ddlLevel = (DropDownList)e.Row.FindControl("ddlLevel");
                GridViewRow gr = grvSkill.SelectedRow;

                TextBox txtLevel = (TextBox)e.Row.FindControl("txtLevel");

                //TextBox box = (TextBox)e.Row.FindControl("txtrelation");
                //TextBox box1 = (TextBox)e.Row.FindControl("txtoccupation");

                ddlSkill_Name.SelectedValue = txtSkillName.Text;
                ddlLevel.SelectedValue = txtLevel.Text;


            }
        }
        //Editing Educational Skill Details
        public void Edit_Employee_Education_Skill_Details(string empcode, string Creation_Company)
        {
            try
            {

                List<HRMS_Employee_Master_Educational_Skill_Detail> objEmpDetails = objEmpBAL.Get_Employee_Eductional_Skill_Details(empcode, Creation_Company);

                if (objEmpDetails.Count > 0)
                {
                    grvSkill.DataSource = objEmpDetails;
                    grvSkill.DataBind();

                    DataTable dt = new DataTable();
                    dt.Columns.Add(new DataColumn("Skill_Name", typeof(string)));
                    dt.Columns.Add(new DataColumn("Skill_Level", typeof(string)));
                    dt.Columns.Add(new DataColumn("Description", typeof(string)));


                    for (int i = 0; i < grvSkill.Rows.Count; i++)
                    {
                        DropDownList ddlSkill_Name = grdEDucation.Rows[i].FindControl("ddlSkill_Name") as DropDownList;
                        DropDownList ddlLevel = grdEDucation.Rows[i].FindControl("ddlLevel") as DropDownList;
                        TextBox txtDescription = grdEDucation.Rows[i].FindControl("txtDescription") as TextBox;

                        string skill = ddlSkill_Name.SelectedValue;
                        string level = ddlLevel.SelectedValue;
                        string desr = txtDescription.Text;
                        dt.Rows.Add(skill, level, desr);
                    }

                    ViewState["CurrentTableEducationSkill"] = dt;
                    if (Session["User_Type"].ToString() == "Employees")
                    {

                    }
                    else
                    {


                        Session["EmployeeFamilyDetailsCode"] = null;
                    }
                }
                //btnUpdatePersonal.Visible = true;
                //btnSavePersonal.Visible = false;
                //Session["EmployeeDetailsCode"] = null;

                else
                {
                    //ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('No Data Found ');", true);
                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }

        // // // This Is Related To Grid View
        protected void btnNewRecordEducationSkill_Click(object sender, EventArgs e)
        {
            // Adding Empty Row Data into Customer Address Details

            int Count = (int)grvSkill.Rows.Count;

            //Set the Previous Selected Items on Each DropDownList on Postbacks
            DropDownList BankName = (DropDownList)grvSkill.Rows[Count - 1].FindControl("ddlSkill_Name");
            DropDownList AccountNumber = (DropDownList)grvSkill.Rows[Count - 1].FindControl("ddlLevel");
            TextBox Branch = (TextBox)grvSkill.Rows[Count - 1].FindControl("txtDescription");
            //TextBox PAN = (TextBox)grdEDucation.Rows[Count - 1].FindControl("txtMarks_GPA");
            //TextBox Attachment = (TextBox)grdEDucation.Rows[Count - 1].FindControl("txtAttach_Certificate");

            //if (BankName.SelectedItem.Text == "")
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Select Skill Name');", true);
            //    BankName.Focus();
            //    this.mp1.Show();
            //    //txtBankName.Focus();
            //}
            //else if (AccountNumber.SelectedItem.Text == "")
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Select Skill Level');", true);
            //    AccountNumber.Focus();
            //}
            //else if (Branch.Text == "")
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Enter Skill Description');", true);
            //    Branch.Focus();
            //}

            //else
            //{
            AddEducationSkillNewRowToGrid();
            int aa = (int)grvSkill.Rows.Count;
            //DropDownList box = (DropDownList)grdEDucation.Rows[aa - 1].FindControl("ddlSkill_Name") as DropDownList;
            //box.Focus();
            // }
        }

        // // // Add New Record To Existing Records
        //Add New Record

        public void AddEducationSkillNewRowToGrid()
        {
            int rowIndex = 0;
            if (ViewState["CurrentTableEducationSkill"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTableEducationSkill"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //Set the Previous Selected Items on Each DropDownList on Postbacks
                        DropDownList BankName = (DropDownList)grvSkill.Rows[rowIndex].Cells[0].FindControl("ddlSkill_Name");
                        DropDownList AccountNumber = (DropDownList)grvSkill.Rows[rowIndex].Cells[1].FindControl("ddlLevel");
                        TextBox Branch = (TextBox)grvSkill.Rows[rowIndex].Cells[2].FindControl("txtDescription");
                        //TextBox PAN = (TextBox)grdEDucation.Rows[rowIndex].Cells[3].FindControl("txtMarks_GPA");
                        //TextBox Attachment = (TextBox)grdEDucation.Rows[rowIndex].Cells[4].FindControl("txtAttach_Certificate");

                        drCurrentRow = dtCurrentTable.NewRow();
                        dtCurrentTable.Rows[i - 1]["Skill_Name"] = BankName.SelectedValue;
                        dtCurrentTable.Rows[i - 1]["Skill_Level"] = AccountNumber.SelectedValue;
                        dtCurrentTable.Rows[i - 1]["Description"] = Branch.Text;
                        //dtCurrentTable.Rows[i - 1]["Percentage"] = PAN.Text;
                        //dtCurrentTable.Rows[i - 1]["Attach_File"] = Attachment.Text;
                        rowIndex += 1;
                    }

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["CurrentTableEducationSkill"] = dtCurrentTable;
                    grvSkill.DataSource = dtCurrentTable;
                    grvSkill.DataBind();

                }
            }
            else
            {
                Response.Write("ViewState is null");
            }
            SetPreviousEducationSkillData();
        }

        // // // Set To Previous Record To Gridview
        //Set Previous Record on Grid view
        private void SetPreviousEducationSkillData()
        {
            int rowIndex = 0;
            if (ViewState["CurrentTableEducationSkill"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTableEducationSkill"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        //Set the Previous Selected Items on Each DropDownList on Postbacks
                        DropDownList BankName = (DropDownList)grvSkill.Rows[rowIndex].Cells[0].FindControl("ddlSkill_Name");
                        DropDownList AccountNumber = (DropDownList)grvSkill.Rows[rowIndex].Cells[1].FindControl("ddlLevel");
                        TextBox Branch = (TextBox)grvSkill.Rows[rowIndex].Cells[2].FindControl("txtDescription");
                        //TextBox PAN = (TextBox)grdEDucation.Rows[rowIndex].Cells[3].FindControl("txtMarks_GPA");
                        //TextBox Attachment = (TextBox)grdEDucation.Rows[rowIndex].Cells[4].FindControl("txtAttach_Certificate");
                        if (i < dt.Rows.Count - 1)
                        {
                            BankName.Text = dt.Rows[i]["Skill_Name"].ToString();
                            AccountNumber.Text = dt.Rows[i]["Skill_Level"].ToString();
                            Branch.Text = dt.Rows[i]["Description"].ToString();
                            //PAN.Text = dt.Rows[i]["Percentage"].ToString();
                            //Attachment.Text = dt.Rows[i]["Attach_File"].ToString();
                        }
                        rowIndex++;
                    }
                }
            }
        }

        ///Remove Row Data Into purchase Order Item Grid Details 

        protected void lbRemoveRowEducationSkill_Click(object sender, EventArgs e)
        {
            AddEducationSkillRemoveNewRowToGrid();
            LinkButton lb = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow.RowIndex;
            if (ViewState["CurrentTableEducationSkill"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTableEducationSkill"];
                if (dt.Rows.Count > 1)
                {
                    dt.Rows.Remove(dt.Rows[rowID]);    //Remove the Selected Row data and reset row number
                }
                ViewState["CurrentTableEducationSkill"] = dt;     //Store the current data in ViewState for future reference
                grvSkill.DataSource = dt;
                grvSkill.DataBind();
                //Grid_Footer_Calc();
                int aa = (int)grvSkill.Rows.Count;
                DropDownList box = (DropDownList)grvSkill.Rows[aa - 1].FindControl("ddlSkill_Name") as DropDownList;
                if (box.SelectedValue == "0")
                {
                    box.Focus();
                }
                else
                {
                    Button btn = (Button)grvSkill.FooterRow.FindControl("btnNewRecordEducationSkill") as Button;
                    btn.Focus();
                }
            }
        }

        public void AddEducationSkillRemoveNewRowToGrid()
        {
            int rowIndex = 0;
            if (ViewState["CurrentTableEducationSkill"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTableEducationSkill"];
                DataRow drCurrentRow = null;
                int index = dtCurrentTable.Rows.Count;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    TextBox txtProduct_Code = (TextBox)grvSkill.Rows[index - 1].Cells[0].FindControl("txtSkillName");
                    DropDownList ddlProduct_Code = (DropDownList)grvSkill.Rows[index - 1].Cells[0].FindControl("ddlSkill_Name");
                    TextBox ddlProduct_Name = (TextBox)grvSkill.Rows[index - 1].Cells[1].FindControl("txtLevel");
                    DropDownList txtProduct_Name = (DropDownList)grvSkill.Rows[index - 1].Cells[1].FindControl("ddlLevel");
                    TextBox txtProduct_Specification = (TextBox)grvSkill.Rows[index - 1].Cells[2].FindControl("txtDescription");

                    drCurrentRow = dtCurrentTable.NewRow();
                    dtCurrentTable.Rows[index - 1]["Skill_Name"] = txtProduct_Code.Text;
                    dtCurrentTable.Rows[index - 1]["Skill_Name"] = ddlProduct_Code.SelectedValue;//.Text;
                    dtCurrentTable.Rows[index - 1]["Skill_Level"] = ddlProduct_Name.Text;
                    dtCurrentTable.Rows[index - 1]["Skill_Level"] = txtProduct_Name.SelectedValue;
                    dtCurrentTable.Rows[index - 1]["Description"] = txtProduct_Specification.Text;

                    rowIndex += 1;
                    ViewState["CurrentTableEducationSkill"] = dtCurrentTable;
                    grvSkill.DataSource = dtCurrentTable;
                    grvSkill.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }
            //SetInitialRowEducation();
        }
        #endregion

        //Level DropDownList Selected Index Changed
        protected void ddlLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddl = (DropDownList)sender;
                GridViewRow gvr = (GridViewRow)ddl.NamingContainer;
                int index = gvr.RowIndex;
                DropDownList ctrl = (DropDownList)grvSkill.Rows[index].FindControl("ddlLevel");

                if (ctrl.SelectedValue == "Level 1")
                {
                    DropDownList ddlSkill_Name = (DropDownList)grvSkill.Rows[index].FindControl("ddlSkill_Name");
                    TextBox txtDescription = (TextBox)grvSkill.Rows[index].FindControl("txtDescription");
                    DropDownList ddlLevel = (DropDownList)grvSkill.Rows[index].FindControl("ddlLevel");
                    TextBox txtSkillName = (TextBox)grvSkill.Rows[index].FindControl("txtSkillName");
                    TextBox txtLevel = (TextBox)grvSkill.Rows[index].FindControl("txtLevel");

                    txtDescription.Text = "No Competence / Skill";
                }
                else if (ctrl.SelectedValue == "Level 2")
                {
                    DropDownList ddlSkill_Name = (DropDownList)grvSkill.Rows[index].FindControl("ddlSkill_Name");
                    TextBox txtDescription = (TextBox)grvSkill.Rows[index].FindControl("txtDescription");
                    DropDownList ddlLevel = (DropDownList)grvSkill.Rows[index].FindControl("ddlLevel");
                    TextBox txtSkillName = (TextBox)grvSkill.Rows[index].FindControl("txtSkillName");
                    TextBox txtLevel = (TextBox)grvSkill.Rows[index].FindControl("txtLevel");

                    txtDescription.Text = "Basic Competence / Skill";
                }
                else if (ctrl.SelectedValue == "Level 3")
                {
                    DropDownList ddlSkill_Name = (DropDownList)grvSkill.Rows[index].FindControl("ddlSkill_Name");
                    TextBox txtDescription = (TextBox)grvSkill.Rows[index].FindControl("txtDescription");
                    DropDownList ddlLevel = (DropDownList)grvSkill.Rows[index].FindControl("ddlLevel");
                    TextBox txtSkillName = (TextBox)grvSkill.Rows[index].FindControl("txtSkillName");
                    TextBox txtLevel = (TextBox)grvSkill.Rows[index].FindControl("txtLevel");

                    txtDescription.Text = "Medium Competence / Skill";
                }
                else if (ctrl.SelectedValue == "Level 4")
                {
                    DropDownList ddlSkill_Name = (DropDownList)grvSkill.Rows[index].FindControl("ddlSkill_Name");
                    TextBox txtDescription = (TextBox)grvSkill.Rows[index].FindControl("txtDescription");
                    DropDownList ddlLevel = (DropDownList)grvSkill.Rows[index].FindControl("ddlLevel");
                    TextBox txtSkillName = (TextBox)grvSkill.Rows[index].FindControl("txtSkillName");
                    TextBox txtLevel = (TextBox)grvSkill.Rows[index].FindControl("txtLevel");

                    txtDescription.Text = "Advanced Competence / Skill";
                }
                else if (ctrl.SelectedValue == "Level 5")
                {
                    DropDownList ddlSkill_Name = (DropDownList)grvSkill.Rows[index].FindControl("ddlSkill_Name");
                    TextBox txtDescription = (TextBox)grvSkill.Rows[index].FindControl("txtDescription");
                    DropDownList ddlLevel = (DropDownList)grvSkill.Rows[index].FindControl("ddlLevel");
                    TextBox txtSkillName = (TextBox)grvSkill.Rows[index].FindControl("txtSkillName");
                    TextBox txtLevel = (TextBox)grvSkill.Rows[index].FindControl("txtLevel");

                    txtDescription.Text = "Expert";
                }
                else
                { }
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }

        }

        // // Save The Education Details
        //
        protected void btnSaveEducation_Click(object sender, EventArgs e)
        {
            EduParameter = 1;
            if (txtEmp_Code.Text != "" && ddlEducate.SelectedIndex != 0)
            {
                string empcode = txtEmp_Code.Text;
                string company = Session["ClientRegistrationNo"].ToString();

                HRMS_Employee_Master_PersonalDetail emp = ObjDAL.HRMS_Employee_Master_PersonalDetails
                    .Where(x => x.Emp_Code == empcode && x.Creation_Company == company).SingleOrDefault();
                emp.Educate = ddlEducate.SelectedItem.Text;
                ObjDAL.SubmitChanges();
            }
            EducationSaving();
        }
        protected void btnUpdateEducation_Click(object sender, EventArgs e)
        {
            string Creation_Company = Session["ClientRegistrationNo"].ToString();
            string Emp_Code = txtEmp_Code.Text;
            var SUM = (from ST in ObjDAL.HRMS_Employee_Master_Educational_Certification_Details
                       where ST.Creation_Company == Creation_Company && ST.Emp_Code == Emp_Code
                       select ST).ToList();
            ObjDAL.HRMS_Employee_Master_Educational_Certification_Details.DeleteAllOnSubmit(SUM);
            ObjDAL.SubmitChanges();
            //var SUM1 = (from ST in ObjDAL.HRMS_Employee_Master_Educational_Skill_Details
            //            where ST.Creation_Company == Creation_Company && ST.Emp_Code == Emp_Code
            //            select ST).ToList();
            //ObjDAL.HRMS_Employee_Master_Educational_Skill_Details.DeleteAllOnSubmit(SUM1);
            //ObjDAL.SubmitChanges();
            EduParameter = 1;
            EducationSaving();

            #region Oldcode
            //string Creation_Company = Session["ClientRegistrationNo"].ToString();
            //string Emp_Code = (string)Session["EmployeeFamilyDetailsCode"];
            //var SUM = (from ST in ObjDAL.HRMS_Employee_Master_Educational_Certification_Details
            //           where ST.Creation_Company == Creation_Company && ST.Emp_Code == Emp_Code
            //           select ST).ToList();
            //ObjDAL.HRMS_Employee_Master_Educational_Certification_Details.DeleteAllOnSubmit(SUM);
            //ObjDAL.SubmitChanges();
            //var SUM1 = (from ST in ObjDAL.HRMS_Employee_Master_Educational_Skill_Details
            //            where ST.Creation_Company == Creation_Company && ST.Emp_Code == Emp_Code
            //            select ST).ToList();
            //ObjDAL.HRMS_Employee_Master_Educational_Skill_Details.DeleteAllOnSubmit(SUM1);
            //ObjDAL.SubmitChanges();
            //EduParameter = 1;
            //EducationSaving();
            #endregion
        }

        /// <summary>
        /// This Method Saving The Data
        /// </summary>
        /// <param name="sender"></param>
        int EduParameter;
        // // // Education Details Saving
        private void EducationSaving()
        {
            SqlTransaction transaction = null;
            SqlConnection objSqlConnection = null;
            bool debitResult = false;
            bool creditResult = false;
            try
            {
                objSqlConnection = new SqlConnection(objConfig);
                objSqlConnection.Open();

                int a1 = (int)grdEDucation.Rows.Count;
                //DropDownList box11 = (DropDownList)grvBankDetail.Rows[a1].FindControl("ddlBankName") as DropDownList;
                //TextBox box12 = (TextBox)grvBankDetail.Rows[a1].FindControl("txtAccountNumber") as TextBox;
                //TextBox box13 = (TextBox)grvBankDetail.Rows[a1].FindControl("txtBranch") as TextBox;
                //transaction = objSqlConnection.BeginTransaction();
                if (grdEDucation.Rows.Count > 0)
                {
                    if (grdEDucation.Rows.Count != 0)
                    {
                        foreach (GridViewRow objrow in grdEDucation.Rows)
                        {
                            using (SqlCommand objSqlCommand = objSqlConnection.CreateCommand())
                            {
                                // Only look in data rows, ignore header and footer rows
                                if (objrow.RowType == DataControlRowType.DataRow)
                                {
                                    //string EmpCode = txtEmp_Code.Text;//objrow.FindControl("ddlBankName") as TextBox;
                                    TextBox Qualification = objrow.FindControl("txtQualification") as TextBox;
                                    TextBox University = objrow.FindControl("txtInstitute_University") as TextBox;
                                    TextBox Year = objrow.FindControl("txtYearOfPass") as TextBox;
                                    TextBox Percentage = objrow.FindControl("txtMarks_GPA") as TextBox;
                                    TextBox Attach_File = objrow.FindControl("txtAttach_Certificate") as TextBox;

                                    if (Qualification.Text != "")
                                    {
                                        //   TotalEmpDetailsBAL.Voucher_No = Voucher_ID;
                                        Total1EmpDetailsBAL.Emp_Code = txtEmp_Code.Text;
                                        Total1EmpDetailsBAL.Qualification_Certification = Qualification.Text;
                                        Total1EmpDetailsBAL.Institute_University = University.Text;
                                        Total1EmpDetailsBAL.Year = Year.Text;
                                        Total1EmpDetailsBAL.Percentage = Convert.ToDecimal(Percentage.Text);
                                        Total1EmpDetailsBAL.Attach_File = "";
                                        Total1EmpDetailsBAL.EmpParameter = EduParameter;

                                        Total1EmpDetailsBAL.Creation_Company = (string)Session["ClientRegistrationNo"];
                                        Total1EmpDetailsBAL.Created_By = (string)Session["User_Name"];
                                        Total1EmpDetailsBAL.Created_Date = Convert.ToDateTime(DateTime.Now.ToString());
                                        Total1EmpDetailsBAL.Modified_By = (string)Session["User_Name"];
                                        Total1EmpDetailsBAL.Modified_Date = Convert.ToDateTime(DateTime.Now.ToString());

                                        objSqlCommand.CommandType = CommandType.StoredProcedure;
                                        objSqlCommand.CommandText = "Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_Educational_Certification_Details";
                                        objSqlCommand.Parameters.AddWithValue("@Emp_Code", Total1EmpDetailsBAL.Emp_Code);
                                        objSqlCommand.Parameters.AddWithValue("@Qualification_Certification", Total1EmpDetailsBAL.Qualification_Certification);
                                        objSqlCommand.Parameters.AddWithValue("@Institute_University", Total1EmpDetailsBAL.Institute_University);
                                        objSqlCommand.Parameters.AddWithValue("@Year", Total1EmpDetailsBAL.Year);
                                        objSqlCommand.Parameters.AddWithValue("@Percentage", Total1EmpDetailsBAL.Percentage);
                                        objSqlCommand.Parameters.AddWithValue("@Attach_File", Total1EmpDetailsBAL.Attach_File);

                                        objSqlCommand.Parameters.AddWithValue("@Creation_Company", Total1EmpDetailsBAL.Creation_Company);
                                        objSqlCommand.Parameters.AddWithValue("@Created_By", Total1EmpDetailsBAL.Created_By);
                                        objSqlCommand.Parameters.AddWithValue("@Created_Date", Total1EmpDetailsBAL.Created_Date);
                                        objSqlCommand.Parameters.AddWithValue("@Modified_By", Total1EmpDetailsBAL.Modified_By);
                                        objSqlCommand.Parameters.AddWithValue("@Modified_Date", Total1EmpDetailsBAL.Modified_Date);
                                        objSqlCommand.Parameters.AddWithValue("@Parameter", Total1EmpDetailsBAL.EmpParameter);
                                        objSqlCommand.Transaction = transaction;
                                        if (objSqlCommand.ExecuteNonQuery() != 0)
                                        { //debitResult = true;  
                                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Saved Successfully');", true);
                                        }
                                        else
                                        {
                                            transaction.Rollback();
                                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // // // // The Following Code Is Used For Education Skill Details Entry Related
                    if (grvSkill.Rows.Count != 0)
                    {
                        foreach (GridViewRow objrow in grvSkill.Rows)
                        {
                            using (SqlCommand objSqlCommand = objSqlConnection.CreateCommand())
                            {
                                // Only look in data rows, ignore header and footer rows
                                if (objrow.RowType == DataControlRowType.DataRow)
                                {
                                    //string EmpCode = txtEmp_Code.Text;//objrow.FindControl("ddlBankName") as TextBox;
                                    DropDownList skill = objrow.FindControl("ddlSkill_Name") as DropDownList;
                                    DropDownList level = objrow.FindControl("ddlLevel") as DropDownList;
                                    TextBox descrp = objrow.FindControl("txtDescription") as TextBox;
                                    //TextBox Attach_File = objrow.FindControl("txtAccountNumber") as TextBox;


                                    if (skill.SelectedValue != "Select")
                                    {
                                        //   TotalEmpDetailsBAL.Voucher_No = Voucher_ID;
                                        Total1EmpDetailsBAL.Emp_Code = txtEmp_Code.Text;
                                        Total1EmpDetailsBAL.Skill_Name = skill.SelectedValue;
                                        Total1EmpDetailsBAL.Skill_Level = level.SelectedValue;
                                        Total1EmpDetailsBAL.Description = descrp.Text;
                                        Total1EmpDetailsBAL.EmpParameter = EduParameter;
                                        Total1EmpDetailsBAL.Creation_Company = (string)Session["ClientRegistrationNo"];
                                        Total1EmpDetailsBAL.Created_By = (string)Session["User_Name"];
                                        Total1EmpDetailsBAL.Created_Date = Convert.ToDateTime(DateTime.Now.ToString());
                                        Total1EmpDetailsBAL.Modified_By = (string)Session["User_Name"];
                                        Total1EmpDetailsBAL.Modified_Date = Convert.ToDateTime(DateTime.Now.ToString());

                                        objSqlCommand.CommandType = CommandType.StoredProcedure;
                                        objSqlCommand.CommandText = "Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_Educational_Skill_Details";
                                        objSqlCommand.Parameters.AddWithValue("@Emp_Code", Total1EmpDetailsBAL.Emp_Code);
                                        objSqlCommand.Parameters.AddWithValue("@Skill_Name", Total1EmpDetailsBAL.Skill_Name);
                                        objSqlCommand.Parameters.AddWithValue("@Skill_Level", Total1EmpDetailsBAL.Skill_Level);
                                        objSqlCommand.Parameters.AddWithValue("@Description", Total1EmpDetailsBAL.Description);
                                        objSqlCommand.Parameters.AddWithValue("@Creation_Company", Total1EmpDetailsBAL.Creation_Company);
                                        objSqlCommand.Parameters.AddWithValue("@Created_By", Total1EmpDetailsBAL.Created_By);
                                        objSqlCommand.Parameters.AddWithValue("@Created_Date", Total1EmpDetailsBAL.Created_Date);
                                        objSqlCommand.Parameters.AddWithValue("@Modified_By", Total1EmpDetailsBAL.Modified_By);
                                        objSqlCommand.Parameters.AddWithValue("@Modified_Date", Total1EmpDetailsBAL.Modified_Date);
                                        objSqlCommand.Parameters.AddWithValue("@Parameter", Total1EmpDetailsBAL.EmpParameter);
                                        objSqlCommand.Transaction = transaction;
                                        if (objSqlCommand.ExecuteNonQuery() != 0)
                                        { //creditResult = true;
                                            //  ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Saved Successfully');", true);
                                        }
                                        else
                                        {
                                            //transaction.Rollback();
                                            //ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //if (debitResult)
                    //{
                    //    //transaction.Commit();

                    //    //EducationClearAllFields();
                    //}
                    //else
                    //{
                    //    transaction.Rollback();
                    //    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
                    //}
                }
                // }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Enter Bank Details');", true);
                    DropDownList box11 = (DropDownList)grdEDucation.Rows[a1 - 1].FindControl("ddlBankName") as DropDownList;
                    box11.Focus();
                }
            }
            catch (Exception ex)
            {
                Session["Error"] = ex.ToString();
            }

            finally
            {
            }
        }

        // // // Clear The Data After Saving
        public void EducationClearAllFields()
        {
            SetInitialRowEducation();
            SetInitialRowEducationSkill();
        }



        #endregion

        #region Official Information

        //Editing Professional Details
        public void Edit_Employee_Official_Details(string empcode, string Creation_Company)
        {
            try
            {

                List<HRMS_Employee_Master_Official_Information> objEmpDetails = objEmpBAL.Get_Employee_Official_Details(empcode, Creation_Company);



                if (objEmpDetails.Count > 0)
                {

                    txtDateofJoin.Text = (GetDate(objEmpDetails[0].Date_Of_Join) == "01-01-2001") ? "" : GetDate(objEmpDetails[0].Date_Of_Join);
                    ddlJobTitle.SelectedItem.Text = objEmpDetails[0].Designation;
                    ddlEMPGrade.SelectedItem.Text = objEmpDetails[0].Employee_Grade;
                    ddlDepartment.SelectedItem.Text = objEmpDetails[0].Department;
                    ddlReportingAuthority.SelectedItem.Text = objEmpDetails[0].Reporting_Authority;
                    ddlRecruit.SelectedItem.Text = objEmpDetails[0].Recruit;
                    ddlWorkType.SelectedItem.Text = objEmpDetails[0].Work_Type;
                    ddlESIDispensary.SelectedItem.Text = objEmpDetails[0].ESI_Dispensary;

                    if (objEmpDetails[0].Working_Shifts.ToLower().Trim() == "true")
                        chbWorking.Checked = true;
                    else
                        chbWorking.Checked = false;
                    if (objEmpDetails[0].PF.Trim().ToLower() == "false")
                        chbPF.Checked = false;
                    else chbPF.Checked = true;
                    if (objEmpDetails[0].ESI.Trim().ToLower() == "false")
                        chbESI.Checked = false;
                    else chbESI.Checked = true;
                    if (objEmpDetails[0].Profisional_Tax.Trim().ToLower() == "false")
                        chbProfessional.Checked = false;
                    else chbProfessional.Checked = true;
                    //if (objEmpDetails[0].TDS.Trim().ToLower() == "false")
                    //    chbTDS.Checked = false;
                    //else chbTDS.Checked = true;
                    txtPFCode.Text = objEmpDetails[0].PF_Code;
                    txtESICode.Text = objEmpDetails[0].ESI_Code;
                    ddlESIDispensary.SelectedValue = "Select";
                    txtOtherFacilities.Text = objEmpDetails[0].Other_Facilities;
                    if (objEmpDetails[0].Quarter_Accommodation.Trim().ToLower() == "false")
                        chbQuarter.Checked = false;
                    else chbQuarter.Checked = true;
                    if (objEmpDetails[0].Four_Wheeler.Trim().ToLower() == "false")
                        chbFourWheeler.Checked = false;
                    else chbFourWheeler.Checked = true;
                    if (objEmpDetails[0].Two_Wheeler.Trim().ToLower() == "false")
                        chbTwoWheeler.Checked = false;
                    else chbTwoWheeler.Checked = true;
                    if (objEmpDetails[0].Laptop.Trim().ToLower() == "false")
                        chbLaptop.Checked = false;
                    else chbLaptop.Checked = true;
                    if (objEmpDetails[0].Mobile_Phone.Trim().ToLower() == "false")
                        chbMobilePhone.Checked = false;
                    else chbMobilePhone.Checked = true;

                    //Session["EmployeeDetailsCode"] = null;
                }

                var objEmpHead = (from deduct in ObjDAL.HRMS_Employee_Official_Info_Head_Details
                                  where deduct.Emp_Code == empcode && deduct.Creation_Company == Creation_Company
                                  select new
                                  {
                                      Head = deduct.Heads,
                                      Amount = deduct.Amount,
                                      basicAmt = deduct.Basic_Amount,
                                      grossAmt = deduct.Gross_Amount,
                                      da = deduct.DA,
                                      hra = deduct.HRA,
                                      cca = deduct.CCA

                                  }).ToList();



                if (objEmpHead.Count > 0)
                {
                    grvSalary.DataSource = objEmpHead;
                    grvSalary.DataBind();

                    DataTable dt = new DataTable();
                    dt.Columns.Add(new DataColumn("Head", typeof(string)));
                    dt.Columns.Add(new DataColumn("Amount", typeof(string)));
                    for (int i = 0; i < grvSalary.Rows.Count; i++)
                    {
                        TextBox txtHead = grvSalary.Rows[i].FindControl("txtHead") as TextBox;
                        TextBox txtAmount = grvSalary.Rows[i].FindControl("txtAmount") as TextBox;



                        string head = txtHead.Text;
                        string amount = txtAmount.Text;



                        //TextBox MinPriceAllowedBranchLevel = grdProductItems.Rows[i].FindControl("txtMinPriceAllowedBranchLevel") as TextBox;
                        dt.Rows.Add(head, amount);

                    }
                    txtBasicAmount.Text = objEmpHead[0].basicAmt.ToString();
                    txtGrossAmount.Text = objEmpHead[0].grossAmt.ToString();
                    if (Session["User_Type"].ToString() == "Employees")
                    {

                    }
                    else
                    {

                    }
                    ViewState["CurrentTable"] = dt;
                    //Session["EmployeeFamilyDetailsCode"] = null;

                }

                else
                {
                    //ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('No Data Found ');", true);
                    if (Session["User_Type"].ToString() == "Employees")
                    {

                    }
                    else
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }





        ///Remove Row Data Into purchase Order Item Grid Details 


        // Grid Row bound




        public void btnSaveOfficial_Click(object sender, EventArgs e)
        {
            OfficialParameter = 1;
            OfficialDataSave();
            headdatasave();
        }
        public void headdatasave()
        {
            SqlTransaction transaction = null;
            SqlConnection objSqlConnection = null;
            string empcode = (string)Session["EmployeeFamilyDetailsCode"];
            string creationCompany = (string)Session["ClientRegistrationNo"];
            objSqlConnection = new SqlConnection(objConfig);
            objSqlConnection.Open();
        }
        public void btnUpdateOfficial_Click(object sender, EventArgs e)
        {
            string empcode = (string)Session["EmployeeFamilyDetailsCode"];
            string creationCompany = (string)Session["ClientRegistrationNo"];
            List<HRMS_Employee_Master_Official_Information> officaldata = (from offc in ObjDAL.HRMS_Employee_Master_Official_Informations
                                                                           where offc.Emp_Code == empcode && offc.Creation_Company == creationCompany
                                                                           select offc).ToList();
            ObjDAL.HRMS_Employee_Master_Official_Informations.DeleteAllOnSubmit(officaldata);
            ObjDAL.SubmitChanges();
            List<HRMS_Employee_Master_Employee_Deduction> Deductdata = (from Deduct in ObjDAL.HRMS_Employee_Master_Employee_Deductions
                                                                        where Deduct.Emp_Code == empcode && Deduct.Creation_Company == creationCompany
                                                                        select Deduct).ToList();
            ObjDAL.HRMS_Employee_Master_Employee_Deductions.DeleteAllOnSubmit(Deductdata);
            ObjDAL.SubmitChanges();
            List<HRMS_Employee_Official_Info_Head_Detail> Headdata = (from Head in ObjDAL.HRMS_Employee_Official_Info_Head_Details
                                                                      where Head.Emp_Code == empcode && Head.Creation_Company == creationCompany
                                                                      select Head).ToList();
            ObjDAL.HRMS_Employee_Official_Info_Head_Details.DeleteAllOnSubmit(Headdata);
            ObjDAL.SubmitChanges();
            OfficialParameter = 1;

            OfficialDataSave();
        }
        // // // // Save The Data To Employee Official Data
        int OfficialParameter;
        public void OfficialDataSave()
        {
            SqlTransaction transaction = null;
            SqlConnection objSqlConnection = null;
            bool debitResult = false;
            bool creditResult = false;
            bool debitResult1 = false;
            try
            {
                objSqlConnection = new SqlConnection(objConfig);
                objSqlConnection.Open();



                transaction = objSqlConnection.BeginTransaction();

                // // // Assign The Data To Domain Objects
                Total1EmpDetailsBAL.OfficialEmpCode = txtEmp_Code.Text;
                if (txtDateofJoin.Text != "")
                {
                    string dj = txtDateofJoin.Text.Substring(0, 2);
                    string mj = txtDateofJoin.Text.Substring(3, 2);
                    string yj = txtDateofJoin.Text.Substring(6, 4);
                    DateTime DateofJoin = DateTime.ParseExact(txtDateofJoin.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(m4 + '-' + d4 + '-' + y4);
                    Total1EmpDetailsBAL.Date_Of_Join = DateofJoin;
                }
                else
                {
                    Total1EmpDetailsBAL.Date_Of_Join = DateTime.Parse("01-01-2001");
                }

                Total1EmpDetailsBAL.Desgnation = (ddlJobTitle.SelectedValue == "") ? "" : ddlJobTitle.SelectedValue;
                Total1EmpDetailsBAL.Employee_Grade = (ddlEMPGrade.SelectedValue == "") ? "" : ddlEMPGrade.SelectedValue;
                Total1EmpDetailsBAL.Department = (ddlDepartment.SelectedValue == "") ? "" : ddlDepartment.SelectedValue;
                Total1EmpDetailsBAL.Location = (ddlLocation.SelectedValue == "") ? "" : ddlLocation.SelectedValue;
                Total1EmpDetailsBAL.Reporting_Authority = (ddlReportingAuthority.SelectedValue == "") ? "" : ddlReportingAuthority.SelectedValue;
                Total1EmpDetailsBAL.Working_Shifts = chbWorking.Checked.ToString();
                Total1EmpDetailsBAL.PF = chbPF.Checked.ToString();
                Total1EmpDetailsBAL.PF_Code = (txtPFCode.Text == "") ? "" : txtPFCode.Text;
                Total1EmpDetailsBAL.ESI = (chbESI.Checked.ToString());
                Total1EmpDetailsBAL.ESI_Code = (txtESICode.Text == "") ? "" : txtESICode.Text;
                Total1EmpDetailsBAL.ESI_Dispensary = (ddlESIDispensary.SelectedValue == "") ? "" : ddlESIDispensary.SelectedValue;
                Total1EmpDetailsBAL.Profisional_Tax = chbProfessional.Checked.ToString();
                //Total1EmpDetailsBAL.TDS = chbTDS.Checked.ToString();
                Total1EmpDetailsBAL.Quarter_Accommodation = chbQuarter.Checked.ToString();
                Total1EmpDetailsBAL.Four_Wheeler = chbFourWheeler.Checked.ToString();
                Total1EmpDetailsBAL.Two_Wheeler = chbTwoWheeler.Checked.ToString();
                Total1EmpDetailsBAL.Laptop = chbLaptop.Checked.ToString();
                Total1EmpDetailsBAL.Mobile_Phone = chbMobilePhone.Checked.ToString();
                Total1EmpDetailsBAL.Other_Facilities = (txtOtherFacilities.Text == "") ? "" : txtOtherFacilities.Text;
                Total1EmpDetailsBAL.Creation_Company = Session["ClientRegistrationNo"].ToString();
                Total1EmpDetailsBAL.Created_By = Session["User_Name"].ToString();
                Total1EmpDetailsBAL.Created_Date = System.DateTime.Now;
                Total1EmpDetailsBAL.Modified_By = Session["User_Name"].ToString();
                Total1EmpDetailsBAL.Modified_Date = System.DateTime.Now;
                Total1EmpDetailsBAL.WorkType = ddlWorkType.SelectedValue;
                Total1EmpDetailsBAL.Recruit = ddlRecruit.SelectedValue;
                Total1EmpDetailsBAL.OfficialParameter = OfficialParameter; // // // This Can Be Used For Operation Related 1=Insert                   
                //transaction = objSqlConnection.BeginTransaction();
                using (SqlCommand objSqlCommand = objSqlConnection.CreateCommand())
                {
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.CommandText = "Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_Official_Information";
                    objSqlCommand.Parameters.AddWithValue("@Emp_Code", Total1EmpDetailsBAL.OfficialEmpCode);
                    objSqlCommand.Parameters.AddWithValue("@Date_Of_Join", Total1EmpDetailsBAL.Date_Of_Join);
                    objSqlCommand.Parameters.AddWithValue("@Designation", Total1EmpDetailsBAL.Desgnation);
                    objSqlCommand.Parameters.AddWithValue("@Employee_Grade", Total1EmpDetailsBAL.Employee_Grade);
                    objSqlCommand.Parameters.AddWithValue("@Department", Total1EmpDetailsBAL.Department);

                    objSqlCommand.Parameters.AddWithValue("@Location", Total1EmpDetailsBAL.Location); // // Newly Added Field

                    objSqlCommand.Parameters.AddWithValue("@Reporting_Authority", Total1EmpDetailsBAL.Reporting_Authority);
                    objSqlCommand.Parameters.AddWithValue("@Working_Shifts", Total1EmpDetailsBAL.Working_Shifts);
                    objSqlCommand.Parameters.AddWithValue("@PF", Total1EmpDetailsBAL.PF);
                    objSqlCommand.Parameters.AddWithValue("@PF_Code", Total1EmpDetailsBAL.PF_Code);
                    objSqlCommand.Parameters.AddWithValue("@ESI", Total1EmpDetailsBAL.ESI);//10
                    objSqlCommand.Parameters.AddWithValue("@ESI_Code", Total1EmpDetailsBAL.ESI_Code);
                    objSqlCommand.Parameters.AddWithValue("@ESI_Dispensary", Total1EmpDetailsBAL.ESI_Dispensary);
                    objSqlCommand.Parameters.AddWithValue("@Profisional_Tax", Total1EmpDetailsBAL.Profisional_Tax);
                    objSqlCommand.Parameters.AddWithValue("@TDS", Total1EmpDetailsBAL.TDS);
                    objSqlCommand.Parameters.AddWithValue("@Quarter_Accommodation", Total1EmpDetailsBAL.Quarter_Accommodation);
                    objSqlCommand.Parameters.AddWithValue("@Four_Wheeler", Total1EmpDetailsBAL.Four_Wheeler);
                    objSqlCommand.Parameters.AddWithValue("@Two_Wheeler", Total1EmpDetailsBAL.Two_Wheeler);
                    objSqlCommand.Parameters.AddWithValue("@Laptop", Total1EmpDetailsBAL.Laptop);
                    objSqlCommand.Parameters.AddWithValue("@Mobile_Phone", Total1EmpDetailsBAL.Mobile_Phone);
                    objSqlCommand.Parameters.AddWithValue("@Other_Facilities", Total1EmpDetailsBAL.Other_Facilities);

                    objSqlCommand.Parameters.AddWithValue("@Creation_Company", Total1EmpDetailsBAL.Creation_Company);
                    objSqlCommand.Parameters.AddWithValue("@Created_By", Total1EmpDetailsBAL.Created_By);
                    objSqlCommand.Parameters.AddWithValue("@Created_Date", Total1EmpDetailsBAL.Created_Date);
                    objSqlCommand.Parameters.AddWithValue("@Modified_By", Total1EmpDetailsBAL.Modified_By);
                    objSqlCommand.Parameters.AddWithValue("@Modified_Date", Total1EmpDetailsBAL.Modified_Date);//10
                    objSqlCommand.Parameters.AddWithValue("@Parameter", Total1EmpDetailsBAL.OfficialParameter);
                    objSqlCommand.Parameters.AddWithValue("@Work_Type", Total1EmpDetailsBAL.WorkType);
                    objSqlCommand.Parameters.AddWithValue("@Recruit", Total1EmpDetailsBAL.Recruit);

                    objSqlCommand.Transaction = transaction;
                    if (objSqlCommand.ExecuteNonQuery() != 0)
                    {
                        debitResult = true;
                    }
                }
                //if (grvSalary.Rows.Count != 0)
                //{
                //    foreach (GridViewRow objrow in grvSalary.Rows)
                //    {
                SqlCommand objsqlcommand1;
                //objsqlcommand1.Transaction = transaction;
                using (objsqlcommand1 = objSqlConnection.CreateCommand())
                {
                    //if (objrow.RowType == DataControlRowType.DataRow)
                    //{
                    //TextBox txtHead = objrow.FindControl("txtHead") as TextBox;
                    //TextBox txtAmount = objrow.FindControl("txtAmount") as TextBox;
                    //if (txtHead.Text != "")
                    //{

                    objeoinfo.Emp_Code = txtEmp_Code.Text;
                    objeoinfo.Heads = "";
                    objeoinfo.Amount = 0;
                    objeoinfo.Basic_Amount = (txtBasicAmount.Text == "") ? Convert.ToDecimal("0") : Convert.ToDecimal(txtBasicAmount.Text);
                    objeoinfo.Gross_Amount = (txtGrossAmount.Text == "") ? Convert.ToDecimal("0") : Convert.ToDecimal(txtGrossAmount.Text);
                    objeoinfo.DA = (txtda.Text == "") ? Convert.ToDecimal("0") : Convert.ToDecimal(txtda.Text);
                    objeoinfo.HRA = (txthra.Text == "") ? Convert.ToDecimal("0") : Convert.ToDecimal(txthra.Text);
                    objeoinfo.CCA = (txtcca.Text == "") ? Convert.ToDecimal("0") : Convert.ToDecimal(txtcca.Text);
                    objeoinfo.OA = Convert.ToDecimal("0");
                    objeoinfo.PF = (txtpfamount.Text == "") ? Convert.ToDecimal("0") : Convert.ToDecimal(txtpfamount.Text);
                    objeoinfo.EESI = (txteesi.Text == "") ? Convert.ToDecimal("0") : Convert.ToDecimal(txteesi.Text);
                    objeoinfo.ESI = (txtesiamount.Text == "") ? Convert.ToDecimal("0") : Convert.ToDecimal(txtesiamount.Text);
                    objeoinfo.PTAX = (ddlptax.SelectedValue == "") ? Convert.ToDecimal("0") : Convert.ToDecimal(ddlptax.SelectedValue);
                    objeoinfo.TDS = Convert.ToDecimal("0");
                    objeoinfo.APGLI = Convert.ToDecimal("0");
                    objeoinfo.GIS = Convert.ToDecimal("0");
                    objeoinfo.HBA = Convert.ToDecimal("0");
                    objeoinfo.MCA = Convert.ToDecimal("0");
                    objeoinfo.BA = Convert.ToDecimal("0");
                    objeoinfo.LOAN = Convert.ToDecimal("0");
                    objeoinfo.OTHERS = Convert.ToDecimal("0");
                    objeoinfo.NET = Convert.ToDecimal("0");
                    objeoinfo.Criteria = "";
                    objeoinfo.Salary_Range = "";
                    objeoinfo.Parameter = Convert.ToInt32(1);
                    objeoinfo.Creation_Company = (string)Session["ClientRegistrationNo"];
                    objeoinfo.Created_By = (string)Session["User_Name"];
                    objeoinfo.Created_Date = Convert.ToDateTime(DateTime.Now.ToString());
                    objeoinfo.Modified_By = (string)Session["User_Name"];
                    objeoinfo.Modified_Date = Convert.ToDateTime(DateTime.Now.ToString());

                    objsqlcommand1.CommandType = CommandType.StoredProcedure;
                    objsqlcommand1.CommandText = "Sp_HRMS_Insert_Select_Update_HRMS_Employee_Official_Info_Head_Details";
                    objsqlcommand1.Parameters.AddWithValue("@Emp_Code", objeoinfo.Emp_Code);
                    objsqlcommand1.Parameters.AddWithValue("@Heads", objeoinfo.Heads);
                    objsqlcommand1.Parameters.AddWithValue("@Amount", objeoinfo.Amount);
                    objsqlcommand1.Parameters.AddWithValue("@Basic_Amount", objeoinfo.Basic_Amount);
                    objsqlcommand1.Parameters.AddWithValue("@Gross_Amount", objeoinfo.Gross_Amount);
                    objsqlcommand1.Parameters.AddWithValue("@DA", objeoinfo.DA);
                    objsqlcommand1.Parameters.AddWithValue("@HRA", objeoinfo.HRA);
                    objsqlcommand1.Parameters.AddWithValue("@CCA", objeoinfo.CCA);
                    objsqlcommand1.Parameters.AddWithValue("@OA", objeoinfo.OA);
                    objsqlcommand1.Parameters.AddWithValue("@PF", objeoinfo.PF);
                    objsqlcommand1.Parameters.AddWithValue("@EESI", objeoinfo.EESI);
                    objsqlcommand1.Parameters.AddWithValue("@ESI", objeoinfo.ESI);
                    objsqlcommand1.Parameters.AddWithValue("@PTAX", objeoinfo.PTAX);
                    objsqlcommand1.Parameters.AddWithValue("@TDS", objeoinfo.TDS);
                    objsqlcommand1.Parameters.AddWithValue("@APGLI", objeoinfo.APGLI);
                    objsqlcommand1.Parameters.AddWithValue("@GIS", objeoinfo.GIS);
                    objsqlcommand1.Parameters.AddWithValue("@HBA", objeoinfo.HBA);
                    objsqlcommand1.Parameters.AddWithValue("@MCA", objeoinfo.MCA);
                    objsqlcommand1.Parameters.AddWithValue("@BA", objeoinfo.BA);
                    objsqlcommand1.Parameters.AddWithValue("@LOAN", objeoinfo.LOAN);
                    objsqlcommand1.Parameters.AddWithValue("@OTHERS", objeoinfo.OTHERS);
                    objsqlcommand1.Parameters.AddWithValue("@NET", objeoinfo.NET);
                    objsqlcommand1.Parameters.AddWithValue("@Criteria", objeoinfo.Criteria);
                    objsqlcommand1.Parameters.AddWithValue("@Salary_Range", objeoinfo.Salary_Range);
                    objsqlcommand1.Parameters.AddWithValue("@Creation_Company", objeoinfo.Creation_Company);
                    objsqlcommand1.Parameters.AddWithValue("@Created_By", objeoinfo.Created_By);
                    objsqlcommand1.Parameters.AddWithValue("@Created_Date", objeoinfo.Created_Date);
                    objsqlcommand1.Parameters.AddWithValue("@Modified_By", objeoinfo.Modified_By);
                    objsqlcommand1.Parameters.AddWithValue("@Modified_Date", objeoinfo.Modified_Date);
                    objsqlcommand1.Parameters.AddWithValue("@Parameter", objeoinfo.Parameter);

                    objsqlcommand1.Transaction = transaction;
                    if (objsqlcommand1.ExecuteNonQuery() != 0)
                    {
                        creditResult = true;
                    }
                }




                if (debitResult || creditResult || debitResult1)
                {
                    transaction.Commit();
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Saved Successfully');", true);
                }
                else
                {
                    transaction.Rollback();
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
                }
            }
            catch (Exception ex)
            {
                Session["Error"] = ex.ToString();
            }

            finally
            {
            }
        }
        #endregion

        #region Employee Loan Information
        /// <summary>
        /// Loans Details
        /// </summary>

        //GridLoan DataBound



        protected void grvLoan_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataTable dt = (DataTable)ViewState["LoanCurrentTable"];
                LinkButton lb = (LinkButton)e.Row.FindControl("lbRemoveRowLoane");
                if (lb != null)
                {
                    if (dt.Rows.Count > 1)
                    {
                        if (e.Row.RowIndex == dt.Rows.Count - 1)
                        {
                            lb.Visible = false;
                        }
                    }
                    else
                    {
                        lb.Visible = false;
                    }
                }
            }
        }


        #endregion


        #region  Tab Active Index Changing Event



        #endregion

        #region Employee Leave Information
        /// <summary>
        /// Leave Details
        /// </summary>


        /// <summary>
        /// Add New Row For GrdLeave
        /// </summary>


        // Removing Row Data 


        //GridLeave DataBound
        protected void grvLeave_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chbEligible = (CheckBox)e.Row.FindControl("chbEligible");
                TextBox txtEligible = (TextBox)e.Row.FindControl("txtEligible");

                if (txtEligible.Text.ToLower() == "true")
                { chbEligible.Checked = true; }
                else
                { chbEligible.Checked = false; }

            }
        }

        protected void grvLeave_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //    if (e.Row.RowType == DataControlRowType.DataRow)
            //    {
            //        DataTable dt = (DataTable)ViewState["LeaveCurrentTable"];
            //        LinkButton lb = (LinkButton)e.Row.FindControl("lbRemoveRowLeave");
            //        if (lb != null)
            //        {
            //            if (dt.Rows.Count > 1)
            //            {
            //                if (e.Row.RowIndex == dt.Rows.Count - 1)
            //                {
            //                    lb.Visible = false;
            //                }
            //            }
            //            else
            //            {
            //                lb.Visible = false;
            //            }
            //        }
            //    }
        }

        //saving Leave Information


        //Editing family Details


        #endregion

        protected void txtEmp_Code_TextChanged(object sender, EventArgs e)
        {

            Session["EmployeeFamilyDetailsCode"] = txtEmp_Code.Text;
            Edit_Employee_Details(txtEmp_Code.Text, Session["ClientRegistrationNo"].ToString());
            Edit_Employee_Professional_Details(txtEmp_Code.Text, Session["ClientRegistrationNo"].ToString());
            Edit_Employee_Official_Details(txtEmp_Code.Text, Session["ClientRegistrationNo"].ToString());


            txtFirstName.Focus();
        }




        #region Divya Added Extra Client Requiments
        /// <summary>
        /// Status Drop Down
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlEmpStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlEmpStatus.SelectedValue == "Resigned" || ddlEmpStatus.SelectedValue == "Retired")
                {
                    lblResignedDate.Text = ddlEmpStatus.SelectedValue + " Date";
                    ResignedDate.Visible = true;
                }
                else
                {
                    ResignedDate.Visible = false;
                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }
        #endregion

        protected void ddlcircle_SelectedIndexChanged(object sender, EventArgs e)
        {
            Bind_divisions();
            //if (ddlcircle.SelectedItem.Text == "Adilabad")
            //{
            //    ddldistrictforestofficers.Items.Clear();
            //    ddldistrictforestofficers.Items.Add("---Select---");
            //    ddldistrictforestofficers.Items.Add("Adilabad");
            //    ddldistrictforestofficers.Items.Add("Komarambheem");


            //}
            //else if (ddlcircle.SelectedItem.Text == "Kawal Tiger Reserve")
            //{
            //    ddldistrictforestofficers.Items.Clear();
            //    ddldistrictforestofficers.Items.Add("---Select---");
            //    ddldistrictforestofficers.Items.Add("Mancherial");
            //    ddldistrictforestofficers.Items.Add("Nirmal(New)");

            //}
            //else if (ddlcircle.SelectedItem.Text == "Kothagudem")
            //{
            //    ddldistrictforestofficers.Items.Clear();
            //    ddldistrictforestofficers.Items.Add("---Select---");
            //    ddldistrictforestofficers.Items.Add("Kothagudem");
            //    ddldistrictforestofficers.Items.Add("Mahabubabad");

            //}
            //else if (ddlcircle.SelectedItem.Text == "Khammam")
            //{
            //    ddldistrictforestofficers.Items.Clear();
            //    ddldistrictforestofficers.Items.Add("---Select---");
            //    ddldistrictforestofficers.Items.Add("Khammam");
            //    ddldistrictforestofficers.Items.Add("Suryapet");

            //}
            //else if (ddlcircle.SelectedItem.Text == "Nizamabad")
            //{
            //    ddldistrictforestofficers.Items.Clear();
            //    ddldistrictforestofficers.Items.Add("---Select---");
            //    ddldistrictforestofficers.Items.Add("Nizamabad");
            //    ddldistrictforestofficers.Items.Add("Kamareddy");

            //}
            //else if (ddlcircle.SelectedItem.Text == "Amrabada Tiger Reserve")
            //{
            //    ddldistrictforestofficers.Items.Clear();
            //    ddldistrictforestofficers.Items.Add("---Select---");
            //    ddldistrictforestofficers.Items.Add("Nagarkarnool");
            //    ddldistrictforestofficers.Items.Add("Nalgonda");

            //}
            //else if (ddlcircle.SelectedItem.Text == "Hyderabad")
            //{
            //    ddldistrictforestofficers.Items.Clear();
            //    ddldistrictforestofficers.Items.Add("---Select---");
            //    ddldistrictforestofficers.Items.Add("Hyderabad");
            //    ddldistrictforestofficers.Items.Add("Medchal");
            //    ddldistrictforestofficers.Items.Add("Yadadri");

            //}
            //else if (ddlcircle.SelectedItem.Text == "Karimnagar")
            //{
            //    ddldistrictforestofficers.Items.Clear();
            //    ddldistrictforestofficers.Items.Add("---Select---");
            //    ddldistrictforestofficers.Items.Add("Jagital(New)");
            //    ddldistrictforestofficers.Items.Add("Karimnagar + Sircilla");
            //    ddldistrictforestofficers.Items.Add("Peddapally");

            //}
            //else if (ddlcircle.SelectedItem.Text == "Warangal")
            //{

            //    ddldistrictforestofficers.Items.Add("Bhupalpally");
            //    ddldistrictforestofficers.Items.Add("Warngal(U) + Jangoan");
            //    ddldistrictforestofficers.Items.Add("Warngal(R)");

            //}
            //else if (ddlcircle.SelectedItem.Text == "Medak")
            //{
            //    ddldistrictforestofficers.Items.Clear();
            //    ddldistrictforestofficers.Items.Add("---Select---");
            //    ddldistrictforestofficers.Items.Add("Siddipet");
            //    ddldistrictforestofficers.Items.Add("Medak");
            //    ddldistrictforestofficers.Items.Add("Sangareddy");

            //}
            //else if (ddlcircle.SelectedItem.Text == "Mahabubnagar")
            //{
            //    ddldistrictforestofficers.Items.Clear();
            //    ddldistrictforestofficers.Items.Add("---Select---");
            //    ddldistrictforestofficers.Items.Add("Mahabubnagar");
            //    ddldistrictforestofficers.Items.Add("Gadwal + Wanaparthy");

            //}
            //else if (ddlcircle.SelectedItem.Text == "Ranga Reddy")
            //{
            //    ddldistrictforestofficers.Items.Clear();
            //    ddldistrictforestofficers.Items.Add("---Select---");

            //    ddldistrictforestofficers.Items.Add("Ranga Reddy");
            //    ddldistrictforestofficers.Items.Add("Vikarabad");

            //}
            //else
            //{

            //}

        }

        protected void ddldistrictforestofficers_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddldistrictforestofficers.SelectedItem.Text == "Adilabad")
            //{
            //    ddlforestdivisions.Items.Clear();
            //    ddlforestdivisions.Items.Add("---Select---");
            //    ddlforestdivisions.Items.Add("Adilabad");
            //    ddlforestdivisions.Items.Add("Utnoor FDPT");
            //    ddlforestdivisions.Items.Add("Echoda");
            //}
            //else if (ddldistrictforestofficers.SelectedItem.Text == "Komarambheem")
            //{
            //    ddlforestdivisions.Items.Clear();
            //    ddlforestdivisions.Items.Add("---Select---");
            //    ddlforestdivisions.Items.Add("Asifabad");
            //    ddlforestdivisions.Items.Add("Khagaznagar");

            //}
            //else if (ddldistrictforestofficers.SelectedItem.Text == "Mancherial")
            //{
            //    ddlforestdivisions.Items.Clear();
            //    ddlforestdivisions.Items.Add("---Select---");
            //    ddlforestdivisions.Items.Add("Jannaram FDPT");
            //    ddlforestdivisions.Items.Add("Chennur");
            //    ddlforestdivisions.Items.Add("Bellampally");
            //    ddlforestdivisions.Items.Add("Mancherial");
            //}
            //else if (ddldistrictforestofficers.SelectedItem.Text == "Nirmal(New)")
            //{
            //    ddlforestdivisions.Items.Clear();
            //    ddlforestdivisions.Items.Add("---Select---");
            //    ddlforestdivisions.Items.Add("Nirmal");

            //    ddlforestdivisions.Items.Add("Khanapur");
            //}
            //else if (ddldistrictforestofficers.SelectedItem.Text == "Kothagudem")
            //{
            //    ddlforestdivisions.Items.Clear();
            //    ddlforestdivisions.Items.Add("---Select---");
            //    ddlforestdivisions.Items.Add("Kothagudem");
            //    ddlforestdivisions.Items.Add("Yellanadu");
            //    ddlforestdivisions.Items.Add("Paloncha");
            //    ddlforestdivisions.Items.Add("Mangur");
            //    ddlforestdivisions.Items.Add("WLM Kinnerasani");
            //    ddlforestdivisions.Items.Add("Bhadrachalam");
            //}
            //else if (ddldistrictforestofficers.SelectedItem.Text == "Mahabubabad")
            //{
            //    ddlforestdivisions.Items.Clear();
            //    ddlforestdivisions.Items.Add("---Select---");
            //    ddlforestdivisions.Items.Add("Mahabubabad");
            //    ddlforestdivisions.Items.Add("WL Gudur");

            //}
            //else if (ddldistrictforestofficers.SelectedItem.Text == "Khammam")
            //{
            //    ddlforestdivisions.Items.Clear();
            //    ddlforestdivisions.Items.Add("---Select---");
            //    ddlforestdivisions.Items.Add("Khammam");

            //    ddlforestdivisions.Items.Add("Sathupally");
            //}
            //else if (ddldistrictforestofficers.SelectedItem.Text == "Nizamabad")
            //{
            //    ddlforestdivisions.Items.Clear();
            //    ddlforestdivisions.Items.Add("---Select---");
            //    ddlforestdivisions.Items.Add("Nizamabad");

            //    ddlforestdivisions.Items.Add("Armoor");
            //}
            //else if (ddldistrictforestofficers.SelectedItem.Text == "Kamareddy")
            //{
            //    ddlforestdivisions.Items.Clear();
            //    ddlforestdivisions.Items.Add("---Select---");
            //    ddlforestdivisions.Items.Add("Kamareddy");

            //    ddlforestdivisions.Items.Add("Banswada");
            //}
            //else if (ddldistrictforestofficers.SelectedItem.Text == "Nagarkarnool")
            //{
            //    ddlforestdivisions.Items.Clear();
            //    ddlforestdivisions.Items.Add("---Select---");
            //    ddlforestdivisions.Items.Add("Achampet");

            //    ddlforestdivisions.Items.Add("Amarbad");
            //}
            //else if (ddldistrictforestofficers.SelectedItem.Text == "Nalgonda")
            //{
            //    ddlforestdivisions.Items.Clear();
            //    ddlforestdivisions.Items.Add("---Select---");
            //    ddlforestdivisions.Items.Add("Nalgonda");

            //    ddlforestdivisions.Items.Add("Nagarjunsagar WL");
            //}
            //else if (ddldistrictforestofficers.SelectedItem.Text == "Bhupalpally")
            //{
            //    ddlforestdivisions.Items.Clear();
            //    ddlforestdivisions.Items.Add("---Select---");
            //    ddlforestdivisions.Items.Add("Bhupalpally");
            //    ddlforestdivisions.Items.Add("Mahadevpur");
            //    ddlforestdivisions.Items.Add("Mulugu");
            //    ddlforestdivisions.Items.Add("Tadval");
            //    ddlforestdivisions.Items.Add("WLM Eturungaram");
            //    ddlforestdivisions.Items.Add("Venkatapuram");
            //}
            //else if (ddldistrictforestofficers.SelectedItem.Text == "Ranga Reddy")
            //{
            //    ddlforestdivisions.Items.Clear();
            //    ddlforestdivisions.Items.Add("---Select---");
            //    ddlforestdivisions.Items.Add("Shamshabad");

            //    ddlforestdivisions.Items.Add("Amangal");
            //}

            //else
            //{

            //}

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                employee_detail_information edi = new employee_detail_information();

                edi.Emp_Code = txtEmp_Code.Text;
                edi.First_Name = txtFirstName.Text;
                edi.Middle_Name = txtMiddleName.Text;
                edi.Last_Name = txtLastName.Text;
                edi.User_Role = Convert.ToInt32(ddlUSerRole.SelectedValue);
                edi.User_Name = txtUsername.Text;
                edi.password = txtPassword.Text;
                edi.Status = ddlEmpStatus.SelectedItem.Text;
                edi.circle = ddlcircle.SelectedItem.Text;
                edi.district = ddldistrictforestofficers.SelectedItem.Text;
                edi.Divisions = ddlforestdivisions.SelectedItem.Text;
                edi.date_of_birth = (txtDateOfBirth.Text == "") ? DateTime.Now : Convert.ToDateTime(txtDateOfBirth.Text);
                edi.age = (txtAge.Text == "") ? 0 : Convert.ToInt32(txtAge.Text);
                edi.marital_status = ddlMaritalStatus.SelectedItem.Text;
                edi.gender = rblGender.SelectedItem.Text;
                edi.Blood_group = txtBloodGroup.Text;
                edi.Religion = ddlReligion.Text;
                edi.Nationality = ddlNationality.SelectedItem.Text;
                edi.adrressone = txtAddress1.Text;
                edi.addresstwo = txtAddress2.Text;
                edi.city = ddlCity.SelectedItem.Text;
                edi.state = txtState.Text;
                edi.postalcode = txtPostalCode.Text;
                edi.country = txtCountry.Text;
                edi.padrressone = txtPAddress1.Text;
                edi.paddresstwo = txtPaddress2.Text;
                edi.pcity = ddlPCity.SelectedItem.Text;
                edi.pstate = txtPSate.Text;
                edi.ppostalcode = txtPPalost.Text;
                edi.pcountry = txtpCountry.Text;
                edi.mobile_phone = txtMobile.Text;
                edi.Email = txtEmail.Text;
                edi.Alternate_email = txtAlternativeEmail.Text;
                edi.skype_ID = txtSkypeID.Text;
                edi.Linkedin = txtLinkedin.Text;
                edi.Driving_Licence_No = txtDriving_License_Number.Text;
                edi.Type = ddlType.SelectedItem.Text;
                edi.licence_expiry_date = (txtLicenseExpiryDate.Text == "") ? DateTime.Now : Convert.ToDateTime(txtLicenseExpiryDate.Text);
                edi.adhar_no = txtAadharNumber.Text;
                edi.pan_no = txtpPANNo.Text;
                edi.passport_no = txtPassportNumber.Text;
                edi.isssued_date = (txtLicenseExpiryDate.Text == "") ? DateTime.Now : Convert.ToDateTime(txtLicenseExpiryDate.Text);
                edi.expiry_date = (txtExpiryDate.Text == "") ? DateTime.Now : Convert.ToDateTime(txtExpiryDate.Text);
                edi.bank_name = ddlPBankName.Text;
                edi.account_no = txtPAccountNumber.Text;
                edi.health_insurence_policy_no = txtPHealth.Text;
                edi.sum_insured = txtSumInsured.Text;
                edi.Renewal_date = Convert.ToDateTime(txtLicenseExpiryDate.Text);
                edi.date_of_joining = (txtDateofJoin.Text == "") ? DateTime.Now : Convert.ToDateTime(txtDateofJoin.Text);
                edi.designation = ddlJobTitle.Text;
                edi.employe_grade = ddlEMPGrade.Text;
                edi.department_tsfd = ddlDepartment.SelectedItem.Text;
                edi.reporting_authority = ddlReportingAuthority.SelectedItem.Text;
                edi.location = ddlLocation.SelectedItem.Text;
                edi.work_type = ddlWorkType.SelectedItem.Text;
                edi.recruit = ddlRecruit.SelectedItem.Text;
                edi.working_in_shifts = (chbWorking.Checked == true) ? "yes" : "No";
                edi.basic_amount = (txtBasicAmount.Text == "") ? 0 : Convert.ToDecimal(txtBasicAmount.Text);
                edi.gross_amount = (txtGrossAmount.Text == "") ? 0 : Convert.ToDecimal(txtGrossAmount.Text);
                edi.da = (txtda.Text == "") ? 0 : Convert.ToDecimal(txtda.Text);
                edi.hra = (txthra.Text == "") ? 0 : Convert.ToDecimal(txthra.Text);
                edi.cca = (txtcca.Text == "") ? 0 : Convert.ToDecimal(txtcca.Text);
                edi.pf = (chbPF.Checked == true) ? "yes" : "No";
                edi.pf_code = txtPFCode.Text;
                edi.pf_amount = (txtpfamount.Text == "") ? 0 : Convert.ToDecimal(txtpfamount.Text);
                edi.esi = (chbESI.Checked == true) ? "yes" : "No";
                edi.esi_code = txtESICode.Text;
                edi.esi_amount = (txtesiamount.Text == "") ? 0 : Convert.ToDecimal(txtesiamount.Text);
                edi.eesi_amount = (txteesi.Text == "") ? 0 : Convert.ToDecimal(txteesi.Text);
                //edi.esi_dispensary =ddlESIDispensary.SelectedItem.Text;
                edi.pt = ddlptax.SelectedItem.Text;
                edi.tds = txttds.Text;
                edi.accomdation = (chbQuarter.Checked == true) ? "yes" : "No";
                edi.four_wheeler = (chbFourWheeler.Checked == true) ? "yes" : "No";
                edi.two_wheeler = (chbTwoWheeler.Checked == true) ? "yes" : "No";
                edi.laptop = (chbLaptop.Checked == true) ? "yes" : "No";
                //  edi.mobile_phone=txtmob
                edi.other_facilities = txtOtherFacilities.Text;
                edi.cretaed_date = DateTime.Now;
                edi.created_by = Session["Emp_Code"].ToString();
                // edi.created_by=
                ObjDAL.employee_detail_informations.InsertOnSubmit(edi);
                ObjDAL.SubmitChanges();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Saved');", true);
            }
            catch (Exception)
            {

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Insert All Details');", true);
            }

        }

        protected void ddlforestdivisions_SelectedIndexChanged(object sender, EventArgs e)
        {
            Bind_range();
        }

        protected void ddlrange_SelectedIndexChanged(object sender, EventArgs e)
        {
            Bind_section();
        }

        protected void ddlsection_SelectedIndexChanged(object sender, EventArgs e)
        {
            Bind_beat();
        }
        //Binding Roles According to criteria of employee Role
        public void BindRole()
        {
            try
            {
                var objHeaderInfo = (from header in ObjDAL.HRMS_Officer_Roles
                                     where header.RoleID >= Convert.ToInt32(Session["role"].ToString())
                                     select header).ToList();
                if (objHeaderInfo.Count > 0)
                {
                    ddlUSerRole.DataSource = objHeaderInfo;
                    ddlUSerRole.DataTextField = "RoleShortName";
                    ddlUSerRole.DataValueField = "RoleID";
                    ddlUSerRole.DataBind();
                    ddlUSerRole.Items.Insert(0, new ListItem("---Select---", "0"));
                }
                else
                {
                    ddlUSerRole.Items.Insert(0, new ListItem("---Select---", "0"));
                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }

        }

        //Binding SubRoles 

        public void BindSubRole()
        {
            try
            {
                var objHeaderInfo = (from header in ObjDAL.HRMS_Officer_Sub_Roles
                                     select header).ToList();
                if (objHeaderInfo.Count > 0)
                {
                    ddlusersubrole.DataSource = objHeaderInfo;
                    ddlusersubrole.DataTextField = "Sub_RoleShortName";
                    ddlusersubrole.DataValueField = "Sub_RoleID";
                    ddlusersubrole.DataBind();
                    ddlusersubrole.Items.Insert(0, new ListItem("---Select---", "0"));
                }
                else
                {
                    ddlusersubrole.Items.Insert(0, new ListItem("---Select---", "0"));
                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }

        }
    }
}
