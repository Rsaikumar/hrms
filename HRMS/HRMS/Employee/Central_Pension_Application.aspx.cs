﻿using System;
using System.Web.UI;
using DAL;
using BAL;
using System.Globalization;

namespace HRMS.Employee
{
    public partial class pensionapplicationcentral : System.Web.UI.Page
    {

        CentralPensionApplicationBAL objBAl = new CentralPensionApplicationBAL();

        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            saving();
        }

        private void saving()
        {
            try
            {
                objBAl.ReceivedFrom = txtackrecfromname.Text;
                objBAl.AccountGeneral = txtackadddesignation.Text;
                objBAl.CommutatedPensionValue = Convert.ToDecimal(txtcommutedvalue.Text);
                objBAl.AmountOfResiduary = Convert.ToDecimal(txtamtofresiduary.Text);
                objBAl.ApplicationLetterNo = Convert.ToInt32(txtapplicationletterno.Text);
                objBAl.ApplicatioDate = DateTime.ParseExact(txtapplicationdate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                objBAl.ReceiptPartII = DateTime.ParseExact(txtrecepiton.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                objBAl.CommutedDebitAccount = Convert.ToInt32(txtcommuteddebitedaccount.Text);
                objBAl.NominateeName = txtnominateename.Text;
                objBAl.OfficerName = txtofficername.Text;
                objBAl.OfficerDesignation = txtofficerdesignation.Text;
                objBAl.OfficerAddress = txtofficeraddress.Text;
                objBAl.Nomination = DateTime.ParseExact(txtnaminationdate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                objBAl.NominationCancel = DateTime.ParseExact(txtnominationcancellationdate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                objBAl.Form1Number = Convert.ToInt32(txtformino.Text);
                objBAl.Form1Designation = txtformdesignation.Text;
                objBAl.EarlierNominationOn = DateTime.ParseExact(txtearliernominationdate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                objBAl.NotApplicant = DateTime.ParseExact(txtnotapplicantdate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                objBAl.DayOf = DateTime.ParseExact(txtdayof.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                objBAl.WitnessSignature1 = txtwitnesssignature1.Text;
                objBAl.WitnessSignature2 = txtwitnesssignature2.Text;
                objBAl.SignatureOfGovServant = txtsignatureofgovt.Text;
                objBAl.NominatedBy = txtnominationby.Text;
                objBAl.NominatedDesignation = txtnomineedesignation.Text;
                objBAl.OfficeOfTheOfficer = txtofficeof.Text;
                objBAl.DesignationOfTheOfficer = txtdesinationofofficer.Text;
                objBAl.FormIVName = txtform4name.Text;
                objBAl.FormIVEarlierNominationOn = DateTime.ParseExact(txtform4nominationon.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                objBAl.HeadOfficerName = txtheadofficername.Text;
                objBAl.HeadOfficerDesignation = txtheadofficerdesignation.Text;
                objBAl.HeadOfficerAddress = txtheadofficeraddress.Text;
                objBAl.FormIVNominationDate = DateTime.ParseExact(txtform4nominationdate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                objBAl.FormIVNumber = Convert.ToInt32(txtform4no.Text);
                objBAl.DatedThe = DateTime.ParseExact(txtdatedthe.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                objBAl.OfficerPost = txtofficerpost.Text;
                objBAl.NomineeName = txtnomineename.Text;
                objBAl.NomineeDesignation = txtnomineedesignation.Text;
                objBAl.TodayDate = DateTime.ParseExact(txttodaydate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                objBAl.PresentDesignation = txtpresentdesignation.Text;
                objBAl.ApplicantSignture = txtapplicantsignature.Text;
                objBAl.ApplicantDesignation = txtapplicantdesignation.Text;
                objBAl.NameOfPensioner = txtnameofpensioner.Text;
                objBAl.PensionerDesignation = txtpensionerdesignation.Text;
                objBAl.PersonName = txtpersonname.Text;
                objBAl.CreatedBy = Session["User_Name"].ToString();
                objBAl.CreatedOn = System.DateTime.Now;
                objBAl.ModifiedBy = Session["User_Name"].ToString();
                objBAl.ModifiedOn = System.DateTime.Now;
                objBAl.Parameter = 1;
                if (objBAl.InsertHRMSCentralPensionApplication() > 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Saved Successfully ');", true);
                   
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Details Not Saved');", true);
                }
            }
            catch (Exception ex)
            {

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            { }
        }
    }
}