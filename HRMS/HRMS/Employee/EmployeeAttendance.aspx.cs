﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Employee
{
    public partial class EmployeeAttendance : System.Web.UI.Page
    {

        #region Declarations

        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                btnback.Visible = false;
                ddlbind();
            }
        }

        public void ddlbind()
        {
            try
            {
                int empcode = Convert.ToInt32(Session["Emp_Code"].ToString());
                
             var   master = (from item in ObjDAL.HRMSEmployeeMasterRegistrationDetails
                          join per in ObjDAL.HRMSEmplyeeMasterPersonalDetails on item.ID equals Convert.ToInt32(per.EmpId)
                          where item.CreatedBy == Convert.ToString(empcode)
                             select per.FirstName + " " + per.MiddleName + " " + per.LastName).ToList();

            //var    master1 = (from item in ObjDAL.HRMSEmployeeMasterRegistrationDetails
            //              join per in ObjDAL.HRMSEmplyeeMasterPersonalDetails on item.ID equals Convert.ToInt32(per.EmpId)
            //              where item.ID == empcode
            //              select per.FirstName + per.MiddleName + per.LastName).ToList();

                

                if (master.Count > 0)
                {
                    ddlempname.DataSource = master ;
                    ddlempname.DataBind();
                    ddlempname.Items.Insert(0, new ListItem("--Select--", "0"));
                }
            }
            catch (Exception)
            {

            }
        }


        protected void btnsearch_Click(object sender, EventArgs e)
        {
            int empcode = Convert.ToInt32(Session["Emp_Code"].ToString());
            var Month = ddlmonth.SelectedItem.Value;
            var Year = ddlyear.SelectedItem.Text;
            var name = ddlempname.SelectedItem.Text;

            var attendence = (from atd in ObjDAL.AttendanceLists
                              where  atd.CreatedOn.Value.Month == Convert.ToInt32(Month)
                              && atd.CreatedOn.Value.Year == Convert.ToInt32(Year) && atd.Name == name

                              select atd).Distinct().ToList();


            if (attendence.Count > 0)
            {
                gvdattendance.DataSource = attendence;
                gvdattendance.DataBind();
                btnback.Visible = true;
                btnBacktoDashboard.Visible = false;
                ddlmonth.SelectedIndex = 0;
                ddlyear.SelectedIndex = 0;
            }
            else
            {
                gvdattendance.DataSource = attendence;
                gvdattendance.DataBind();
            }
        }

        protected void btnback_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Dashboard/Dashboard.aspx");
        }



        protected void btnBacktoDashboard_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Dashboard/Dashboard.aspx");

        }

        protected void ddlempname_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

    }
}