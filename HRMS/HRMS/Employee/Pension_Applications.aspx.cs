﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Employee
{
    public partial class Pension_Applications : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnsumit_Click(object sender, EventArgs e)
        {
            if (ddlpensionlist.SelectedValue == "0")
            {
                Response.Redirect("State_pension_application.aspx");
            }
            else if (ddlpensionlist.SelectedValue == "1")
            {
                Response.Redirect("Central_Pension_Application.aspx");
            }
            else
            {

            }
        }
    }
}