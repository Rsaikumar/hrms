﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using System.Globalization;

namespace HRMS.Employee
{
    public partial class EmployeeAttendanceReport : System.Web.UI.Page
    {

        #region Declarations

        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlbind();
                // Attendancebind();
                dateofjoin.Visible = false;
                reporting.Visible = false;
                status.Visible = false;
                btnback.Visible = false;

            }
        }

        //public void Attendancebind()
        //{

        //    var objbindattendancegrid = (from ST in ObjDAL.USP_GetEmployeeDetailsAttendaceListReport()
        //                                 select ST).ToList();
        //    if (objbindattendancegrid.Count() >= 0)
        //    {
        //        gdvAttendance.DataSource = objbindattendancegrid;
        //        gdvAttendance.DataBind();
        //    }

        //    else
        //    {
        //        gdvAttendance.DataSource = objbindattendancegrid;
        //        gdvAttendance.DataBind();
        //    }
        //}


        //protected void btngetdata_Click(object sender, EventArgs e)
        //{
        //    Attendancebind();
        //}




        public void ddlbind()
        {
            try
            {
                int empcode = Convert.ToInt32(Session["Emp_Code"].ToString());

                var master = (from item in ObjDAL.HRMSEmployeeMasterRegistrationDetails
                              join per in ObjDAL.HRMSEmplyeeMasterPersonalDetails on item.ID equals Convert.ToInt32(per.EmpId)
                              where item.CreatedBy == Convert.ToString(empcode) || per.EmpId == Convert.ToString(empcode)
                              select per.FirstName + " " + per.MiddleName + " " + per.LastName).ToList();


                if (master.Count > 0)
                {
                    ddlempname.DataSource = master;
                    ddlempname.DataBind();
                    ddlempname.Items.Insert(0, new ListItem("--Select--", "0"));
                }
            }
            catch (Exception)
            {

            }
        }

        protected void btngetdata_Click(object sender, EventArgs e)
        {
            int empcode = Convert.ToInt32(Session["Emp_Code"].ToString());

            DateTime date = DateTime.ParseExact(txtdate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);

            String dy = date.Day.ToString();
            String mn = date.Month.ToString();
            String yy = date.Year.ToString();

            var name = ddlempname.SelectedItem.Text;

            var attendence = (from atd in ObjDAL.AttendanceLists
                              where atd.CreatedOn == date
                              && atd.CreatedOn == date && atd.Name == name
                              select atd).Distinct().ToList();

            var data = (from atd in ObjDAL.HRMSEmployeeMasterOfficialInformationDetails
                        where atd.EmpId == attendence[0].EmpId

                        select atd).Distinct().FirstOrDefault();

            txtdateofjoing.Text = Convert.ToDateTime(data.DateOfJoining).ToString("dd-MM-yyyy");

            var emp = (from per in ObjDAL.HRMSEmplyeeMasterPersonalDetails
                       where per.EmpId == data.ReportingAuthority

                       select per).Distinct().ToList();
            txtreport.Text = emp[0].FirstName + "" + emp[0].MiddleName + "" + emp[0].LastName;

            if (attendence[0].Status == "Absent")
            {
                var leave = (from atd in ObjDAL.HRMSLeaveApplications
                             where atd.EmpCode == attendence[0].EmpId && atd.LeaveFrom < date && atd.LeaveTo > date
                             select atd).Distinct().ToList();
                txtstatus.Text = leave[0].LeaveStatus;
                status.Visible = true;

            }
            if (attendence.Count > 0)
            {
                gvdattendance.DataSource = attendence;
                gvdattendance.DataBind();
                dateofjoin.Visible = true;
                reporting.Visible = true;
                btnback.Visible = true;
                btnBacktoDashboard.Visible = false;
            }
            else
            {
                gvdattendance.DataSource = attendence;
                gvdattendance.DataBind();
            }
        }

        protected void btnback_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Dashboard/Dashboard.aspx");
        }

        protected void btnBacktoDashboard_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Dashboard/Dashboard.aspx");
        }

    }
}