﻿using DAL;
using BAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace HRMS.Employee
{
    public partial class AddDeductions : System.Web.UI.Page
    {
        string objConfig = ConfigurationManager.ConnectionStrings["Dbconnection"].ToString();
        ERP_DatabaseDataContext objdb = new ERP_DatabaseDataContext();
        DeductionsBAL objDeductBAL = new DeductionsBAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            GrdOther.Visible = false;
            if (!IsPostBack)
            {
                if (Session["User_Type"].ToString() == "User")
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('You Don't have Permissions To Create..');", true);
                    Response.Redirect("~/Dashboard/Dashboard.aspx");
                }
                {
                    if (Session["ClientRegistrationNo"] == null)
                    {
                        Response.Redirect("~/Default.aspx");
                    }
                    //Income Tax(TDS)
                    SetInitialRow();

                    // Professional Tax (PT)
                    SetInitialRowPT();
                    //Other 
                    SetInitialRowOthers();

                    //OTList();

                    GetDeductionData();
                }
            }
        }



        #region Private Methods
        /// <summary>
        /// Income Tax(TDS)
        /// </summary>
        public void SetInitialRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("Define_Range_From", typeof(string)));
            dt.Columns.Add(new DataColumn("Define_Range_To", typeof(string)));
            //dt.Columns.Add(new DataColumn("TO", typeof(string)));
            dt.Columns.Add(new DataColumn("Define_Amount", typeof(string)));

            dr = dt.NewRow();
            dr["Define_Range_From"] = string.Empty;
            dr["Define_Range_To"] = string.Empty;
            // dr["TO"] = string.Empty;
            dr["Define_Amount"] = string.Empty;

            dt.Rows.Add(dr);
            ViewState["Current_Range"] = dt;
            GrdDefineRange.DataSource = dt;
            GrdDefineRange.DataBind();
        }

        /// <summary>
        /// Professional Tax (PT)
        /// </summary>
        public void SetInitialRowPT()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("Define_Range_PT_From", typeof(string)));
            dt.Columns.Add(new DataColumn("Define_Range_PT_To", typeof(string)));
            //dt.Columns.Add(new DataColumn("TO", typeof(string)));
            dt.Columns.Add(new DataColumn("Define_PT_Amount", typeof(string)));

            dr = dt.NewRow();
            dr["Define_Range_PT_From"] = string.Empty;
            dr["Define_Range_PT_To"] = string.Empty;
            // dr["TO"] = string.Empty;
            dr["Define_PT_Amount"] = string.Empty;

            dt.Rows.Add(dr);
            ViewState["Current_RangePT"] = dt;
            grdPT.DataSource = dt;
            grdPT.DataBind();
        }


        /// <summary>
        /// Others Details Grid
        /// </summary>
        public void SetInitialRowOthers()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("Define_Range_Other_HeadName", typeof(string)));
            dt.Columns.Add(new DataColumn("Define_Range_Criteria", typeof(string)));
            dt.Columns.Add(new DataColumn("Salary_Range", typeof(string)));
            dt.Columns.Add(new DataColumn("OtherAmount", typeof(string)));

            dr = dt.NewRow();
            dr["Define_Range_Other_HeadName"] = string.Empty;
            dr["Define_Range_Criteria"] = string.Empty;
            dr["Salary_Range"] = string.Empty;
            dr["OtherAmount"] = string.Empty;

            dt.Rows.Add(dr);
            ViewState["Current_Range_Other"] = dt;
            GrdOther.DataSource = dt;
            GrdOther.DataBind();
        }

        // Grid Row bound
        protected void GrdOther_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TextBox txtCriteria = (TextBox)e.Row.FindControl("txtCriteria");
                DropDownList ddlCriteria = (DropDownList)e.Row.FindControl("ddlCriteria");

                TextBox txtSalAmount = (TextBox)e.Row.FindControl("txtSalAmount");
                DropDownList ddlSalRange = (DropDownList)e.Row.FindControl("ddlSalRange");

                GridViewRow gr = GrdOther.SelectedRow;
                ddlCriteria.SelectedValue = txtCriteria.Text;
                ddlSalRange.SelectedValue = txtSalAmount.Text;
            }
        }
        #endregion

        #region popup button click events
        protected void btnPIDeduct_Click(object sender, EventArgs e)
        {
            this.MPDept.Show();
            btnPFDedctSave.Visible = true;
            btnESIDedctSave.Visible = false;
            btnTDSDedctSave.Visible = false;
            btnPTDedctSave.Visible = false;
            btnOtherDedctSave.Visible = false;
        }
        protected void btnESIDeduct_Click(object sender, EventArgs e)
        {
            this.MPDept.Show();
            btnPFDedctSave.Visible = false;
            btnESIDedctSave.Visible = true;
            btnTDSDedctSave.Visible = false;
            btnPTDedctSave.Visible = false;
            btnOtherDedctSave.Visible = false;
        }
        protected void btnTaxDeduct_Click(object sender, EventArgs e)
        {
            this.MPDept.Show();
            btnPFDedctSave.Visible = false;
            btnESIDedctSave.Visible = false;
            btnTDSDedctSave.Visible = true;
            btnPTDedctSave.Visible = false;
            btnOtherDedctSave.Visible = false;
        }
        protected void btnPTDeduct_Click(object sender, EventArgs e)
        {
            this.MPDept.Show();
            btnPFDedctSave.Visible = false;
            btnESIDedctSave.Visible = false;
            btnTDSDedctSave.Visible = false;
            btnPTDedctSave.Visible = true;
            btnOtherDedctSave.Visible = false;
        }
        protected void btnOtherDeduct_Click(object sender, EventArgs e)
        {
            this.MPDept.Show();
            btnPFDedctSave.Visible = false;
            btnESIDedctSave.Visible = false;
            btnTDSDedctSave.Visible = false;
            btnPTDedctSave.Visible = false;
            btnOtherDedctSave.Visible = true;
        }


        protected void btnPFDedctSave_Click(object sender, EventArgs e)
        {

            // Create the list to store.
            List<String> StrList = new List<string>();
            // Loop through each item.
            foreach (ListItem item in chlOTLists.Items)
            {
                if (item.Selected)
                {
                    // If the item is selected, add the value to the list.
                    StrList.Add(item.Value);
                }
                else
                {
                    // Item is not selected, do something else.
                }
            }
            // Join the string together using the ; delimiter.
            String Str = String.Join(",", StrList.ToArray());
            txtDeducton.Text = Str;
            for (int items = 0; items < chlOTLists.Items.Count; items++)
            {
                chlOTLists.ClearSelection();
            }

        }

        protected void btnESIDedctSave_Click(object sender, EventArgs e)
        {
            // Create the list to store.
            List<String> StrList = new List<string>();
            // Loop through each item.
            foreach (ListItem item in chlOTLists.Items)
            {
                if (item.Selected)
                {
                    // If the item is selected, add the value to the list.
                    StrList.Add(item.Value);
                }
                else
                {
                    // Item is not selected, do something else.
                }
            }
            // Join the string together using the ; delimiter.
            String Str = String.Join(",", StrList.ToArray());
            txtESIDeduct.Text = Str;
            for (int items = 0; items < chlOTLists.Items.Count; items++)
            {
                chlOTLists.ClearSelection();
            }

        }
        protected void btnTDSDedctSave_Click(object sender, EventArgs e)
        {
            // Create the list to store.
            List<String> StrList = new List<string>();
            // Loop through each item.
            foreach (ListItem item in chlOTLists.Items)
            {
                if (item.Selected)
                {
                    // If the item is selected, add the value to the list.
                    StrList.Add(item.Value);
                }
                else
                {
                    // Item is not selected, do something else.
                }

            }
            // Join the string together using the ; delimiter.
            String Str = String.Join(",", StrList.ToArray());
            txtTaxDeduct.Text = Str;
            for (int items = 0; items < chlOTLists.Items.Count; items++)
            {
                chlOTLists.ClearSelection();
            }
        }
        protected void btnPTDedctSave_Click(object sender, EventArgs e)
        {
            // Create the list to store.
            List<String> StrList = new List<string>();
            // Loop through each item.
            foreach (ListItem item in chlOTLists.Items)
            {
                if (item.Selected)
                {
                    // If the item is selected, add the value to the list.
                    StrList.Add(item.Value);
                }
                else
                {
                    // Item is not selected, do something else.
                }
            }
            // Join the string together using the ; delimiter.
            String Str = String.Join(",", StrList.ToArray());
            txtPTDeduct.Text = Str;
            for (int items = 0; items < chlOTLists.Items.Count; items++)
            {
                chlOTLists.ClearSelection();
            }

        }
        protected void btnOtherDedctSave_Click(object sender, EventArgs e)
        {
            // Create the list to store.
            List<String> StrList = new List<string>();
            // Loop through each item.
            foreach (ListItem item in chlOTLists.Items)
            {
                if (item.Selected)
                {
                    // If the item is selected, add the value to the list.
                    StrList.Add(item.Value);
                }
                else
                {
                    // Item is not selected, do something else.
                }
            }
            // Join the string together using the ; delimiter.
            String Str = String.Join(",", StrList.ToArray());
            txtOtherDeduct.Text = Str;
            for (int items = 0; items < chlOTLists.Items.Count; items++)
            {
                chlOTLists.ClearSelection();
            }
        }
        #endregion

        #region Database Button Click Events
        int Parameter;
        // Save Button Click Event
        public void btnSave_Click(object sender, EventArgs e)
        {
            Parameter = 1;
            Saving();

        }

        /// <summary>
        /// Save The Data
        /// </summary>
        /// <param name="sender"></param>
        private void Saving()
        {
            SqlTransaction transaction = null;
            SqlConnection objSqlConnection = null;
            bool debitResult = false;
            bool creditResult = false;
            bool Debit1Result = false;
            bool credit1Result = false;
            try
            {
                objSqlConnection = new SqlConnection(objConfig);
                objSqlConnection.Open();
                int a1 = (int)GrdDefineRange.Rows.Count;
                int a2 = (int)grdPT.Rows.Count;
                int a3 = (int)GrdOther.Rows.Count;

                objDeductBAL.Creation_Company = Session["ClientRegistrationNo"].ToString();
                objDeductBAL.Created_By = Session["User_Name"].ToString();
                objDeductBAL.Created_Date = System.DateTime.Now;
                objDeductBAL.Modified_By = Session["User_Name"].ToString();
                objDeductBAL.Modified_Date = System.DateTime.Now;
                objDeductBAL.Parameter = Parameter;

                objDeductBAL.PF_Code = "";

                // // // Assign The Data To Domain Objects

                //ObjAdmComp.ClientRegistrationNo = txtCompanyid.Text;
                objDeductBAL.PF_Percentage = (txtPercentage.Text == "") ? Convert.ToDecimal(0) : Convert.ToDecimal(txtPercentage.Text);
                objDeductBAL.PF_Emp_Contribution = (txtLimitforEmpContr.Text == "") ? Convert.ToDecimal(0) : Convert.ToDecimal(txtLimitforEmpContr.Text);
                objDeductBAL.PF_Deduct = (txtDeducton.Text == "") ? "" : txtDeducton.Text;
                objDeductBAL.ESI_Percentage = (txtESIPercentage.Text == "") ? Convert.ToDecimal(0) : Convert.ToDecimal(txtESIPercentage.Text);
                objDeductBAL.ESI_Deduct = (txtESIDeduct.Text == "") ? "" : txtESIDeduct.Text;

                transaction = objSqlConnection.BeginTransaction();
                using (SqlCommand objSqlCommand = objSqlConnection.CreateCommand())
                {
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.CommandText = "Sp_HRMS_Insert_Select_Update_HRMS_Deductions_PensionFund_Details_1";
                    objSqlCommand.Parameters.AddWithValue("@PF_Code", objDeductBAL.PF_Code);
                    objSqlCommand.Parameters.AddWithValue("@PF_Percentage", objDeductBAL.PF_Percentage);
                    objSqlCommand.Parameters.AddWithValue("@PF_Emp_Contribution", objDeductBAL.PF_Emp_Contribution);
                    objSqlCommand.Parameters.AddWithValue("@PF_Deduct", objDeductBAL.PF_Deduct);
                    objSqlCommand.Parameters.AddWithValue("@ESI_Percentage", objDeductBAL.ESI_Percentage);
                    objSqlCommand.Parameters.AddWithValue("@ESI_Deduct", objDeductBAL.ESI_Deduct);

                    objSqlCommand.Parameters.AddWithValue("@Creation_Company", objDeductBAL.Creation_Company);
                    objSqlCommand.Parameters.AddWithValue("@Created_By", objDeductBAL.Created_By);
                    objSqlCommand.Parameters.AddWithValue("@Created_Date", objDeductBAL.Created_Date);
                    objSqlCommand.Parameters.AddWithValue("@Modified_By", objDeductBAL.Modified_By);
                    objSqlCommand.Parameters.AddWithValue("@Modified_Date", objDeductBAL.Modified_Date);//10
                    objSqlCommand.Parameters.AddWithValue("@Parameter", objDeductBAL.Parameter);

                    objSqlCommand.Transaction = transaction;
                    if (objSqlCommand.ExecuteNonQuery() != 0)
                    { debitResult = true; }
                }

                if (GrdDefineRange.Rows.Count != 0)
                {
                    foreach (GridViewRow objrow in GrdDefineRange.Rows)
                    {
                        using (SqlCommand objSqlCommand = objSqlConnection.CreateCommand())
                        {
                            // Only look in data rows, ignore header and footer rows
                            if (objrow.RowType == DataControlRowType.DataRow)
                            {
                                TextBox txtIncomeslabFrom = objrow.FindControl("txtIncomeslabFrom") as TextBox;
                                TextBox txtIncomeslabTo = objrow.FindControl("txtIncomeslabTo") as TextBox;
                                TextBox txtDePercentage = objrow.FindControl("txtDePercentage") as TextBox;

                                if (txtIncomeslabFrom.Text != "")
                                {
                                    //   ObjAdmComp.Voucher_No = Voucher_ID;
                                    objDeductBAL.Define_Range_From = (txtIncomeslabFrom.Text == "") ? Convert.ToDecimal(0) : Convert.ToDecimal(txtIncomeslabFrom.Text); //ddl1.SelectedItem.Value.ToString();
                                    objDeductBAL.Define_Range_To = (txtIncomeslabTo.Text == "") ? Convert.ToDecimal(0) : Convert.ToDecimal(txtIncomeslabTo.Text);
                                    objDeductBAL.Define_Amount = (txtDePercentage.Text == "") ? Convert.ToDecimal(0) : Convert.ToDecimal(txtDePercentage.Text);
                                    objDeductBAL.Deduct_On_Heads = (txtTaxDeduct.Text == "") ? "" : txtTaxDeduct.Text;
                                    objDeductBAL.Parameter = Parameter;
                                    objDeductBAL.Creation_Company = (string)Session["ClientRegistrationNo"];
                                    objDeductBAL.Created_By = (string)Session["User_Name"];
                                    objDeductBAL.Created_Date = Convert.ToDateTime(DateTime.Now.ToString());
                                    objDeductBAL.Modified_By = (string)Session["User_Name"];
                                    objDeductBAL.Modified_Date = Convert.ToDateTime(DateTime.Now.ToString());

                                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                                    objSqlCommand.CommandText = "Sp_HRMS_Insert_Select_Update_HRMS_Deductions_IncomeTax_Details";
                                    objSqlCommand.Parameters.AddWithValue("@PF_Code", objDeductBAL.PF_Code);
                                    objSqlCommand.Parameters.AddWithValue("@Define_Range_From", objDeductBAL.Define_Range_From);
                                    objSqlCommand.Parameters.AddWithValue("@Define_Range_To", objDeductBAL.Define_Range_To);
                                    objSqlCommand.Parameters.AddWithValue("@Define_Amount", objDeductBAL.Define_Amount);
                                    objSqlCommand.Parameters.AddWithValue("@Deduct_On_Heads", objDeductBAL.Deduct_On_Heads);
                                    objSqlCommand.Parameters.AddWithValue("@Creation_Company", objDeductBAL.Creation_Company);
                                    objSqlCommand.Parameters.AddWithValue("@Created_By", objDeductBAL.Created_By);
                                    objSqlCommand.Parameters.AddWithValue("@Created_Date", objDeductBAL.Created_Date);
                                    objSqlCommand.Parameters.AddWithValue("@Modified_By", objDeductBAL.Modified_By);
                                    objSqlCommand.Parameters.AddWithValue("@Modified_Date", objDeductBAL.Modified_Date);
                                    objSqlCommand.Parameters.AddWithValue("@Parameter", objDeductBAL.Parameter);
                                    objSqlCommand.Transaction = transaction;
                                    if (objSqlCommand.ExecuteNonQuery() != 0)
                                    { creditResult = true; }
                                }
                            }
                        }
                    }

                    if (grdPT.Rows.Count != 0)
                    {
                        foreach (GridViewRow objrow in grdPT.Rows)
                        {
                            using (SqlCommand objSqlCommand = objSqlConnection.CreateCommand())
                            {
                                // Only look in data rows, ignore header and footer rows
                                if (objrow.RowType == DataControlRowType.DataRow)
                                {
                                    TextBox txtIncomeslabPTFrom = objrow.FindControl("txtIncomeslabPTFrom") as TextBox;
                                    TextBox txtIncomeslabPTTo = objrow.FindControl("txtIncomeslabPTTo") as TextBox;
                                    TextBox txtAmount = objrow.FindControl("txtAmount") as TextBox;

                                    if (txtIncomeslabPTFrom.Text != "")
                                    {
                                        //   ObjAdmComp.Voucher_No = Voucher_ID;
                                        objDeductBAL.Define_Range_PT_From = (txtIncomeslabPTFrom.Text == "") ? Convert.ToDecimal(0) : Convert.ToDecimal(txtIncomeslabPTFrom.Text); //ddl1.SelectedItem.Value.ToString();
                                        objDeductBAL.Define_Range_PT_To = (txtIncomeslabPTTo.Text == "") ? Convert.ToDecimal(0) : Convert.ToDecimal(txtIncomeslabPTTo.Text);
                                        objDeductBAL.Define_PT_Amount = (txtAmount.Text == "") ? Convert.ToDecimal(0) : Convert.ToDecimal(txtAmount.Text);
                                        objDeductBAL.Deduct_On_PT_Heads = (txtPTDeduct.Text == "") ? "" : txtPTDeduct.Text;
                                        objDeductBAL.Parameter = Parameter;
                                        objDeductBAL.Creation_Company = (string)Session["ClientRegistrationNo"];
                                        objDeductBAL.Created_By = (string)Session["User_Name"];
                                        objDeductBAL.Created_Date = Convert.ToDateTime(DateTime.Now.ToString());
                                        objDeductBAL.Modified_By = (string)Session["User_Name"];
                                        objDeductBAL.Modified_Date = Convert.ToDateTime(DateTime.Now.ToString());

                                        objSqlCommand.CommandType = CommandType.StoredProcedure;
                                        objSqlCommand.CommandText = "Sp_HRMS_Insert_Select_Update_HRMS_Deductions_ProfessionalTax_PT_Details";
                                        objSqlCommand.Parameters.AddWithValue("@PF_Code", objDeductBAL.PF_Code);
                                        objSqlCommand.Parameters.AddWithValue("@Define_Range_PT_From", objDeductBAL.Define_Range_PT_From);
                                        objSqlCommand.Parameters.AddWithValue("@Define_Range_PT_To", objDeductBAL.Define_Range_PT_To);
                                        objSqlCommand.Parameters.AddWithValue("@Define_PT_Amount", objDeductBAL.Define_PT_Amount);
                                        objSqlCommand.Parameters.AddWithValue("@Deduct_On_PT_Heads", objDeductBAL.Deduct_On_PT_Heads);
                                        objSqlCommand.Parameters.AddWithValue("@Creation_Company", objDeductBAL.Creation_Company);
                                        objSqlCommand.Parameters.AddWithValue("@Created_By", objDeductBAL.Created_By);
                                        objSqlCommand.Parameters.AddWithValue("@Created_Date", objDeductBAL.Created_Date);
                                        objSqlCommand.Parameters.AddWithValue("@Modified_By", objDeductBAL.Modified_By);
                                        objSqlCommand.Parameters.AddWithValue("@Modified_Date", objDeductBAL.Modified_Date);
                                        objSqlCommand.Parameters.AddWithValue("@Parameter", objDeductBAL.Parameter);
                                        objSqlCommand.Transaction = transaction;
                                        if (objSqlCommand.ExecuteNonQuery() != 0)
                                        {
                                            Debit1Result = true;
                                        }
                                    }
                                }
                            }
                        }
                        if (GrdOther.Rows.Count != 0)
                        {
                            foreach (GridViewRow objrow in GrdOther.Rows)
                            {
                                using (SqlCommand objSqlCommand = objSqlConnection.CreateCommand())
                                {
                                    // Only look in data rows, ignore header and footer rows
                                    if (objrow.RowType == DataControlRowType.DataRow)
                                    {
                                        TextBox txtHead_Name = objrow.FindControl("txtHead_Name") as TextBox;
                                        DropDownList ddlCriteria = objrow.FindControl("ddlCriteria") as DropDownList;
                                        DropDownList ddlSalRange = objrow.FindControl("ddlSalRange") as DropDownList;// // // Newely Added Field
                                        TextBox txtAmount = objrow.FindControl("txtAmount") as TextBox;

                                        if (txtHead_Name.Text != "")
                                        {
                                            //   ObjAdmComp.Voucher_No = Voucher_ID;
                                            objDeductBAL.Define_Range_Other_HeadName = (txtHead_Name.Text == "") ? "" : txtHead_Name.Text;
                                            objDeductBAL.Define_Range_Criteria = ddlCriteria.SelectedValue.ToString();
                                            objDeductBAL.Salary_Range = ddlSalRange.SelectedValue.ToString();
                                            objDeductBAL.Define_Amount = (txtAmount.Text == "") ? Convert.ToDecimal(0) : Convert.ToDecimal(txtAmount.Text);
                                            objDeductBAL.Deduct_On_Other_Heads = (txtOtherDeduct.Text == "") ? "" : txtOtherDeduct.Text;
                                            objDeductBAL.Parameter = Parameter;
                                            objDeductBAL.Creation_Company = (string)Session["ClientRegistrationNo"];
                                            objDeductBAL.Created_By = (string)Session["User_Name"];
                                            objDeductBAL.Created_Date = Convert.ToDateTime(DateTime.Now.ToString());
                                            objDeductBAL.Modified_By = (string)Session["User_Name"];
                                            objDeductBAL.Modified_Date = Convert.ToDateTime(DateTime.Now.ToString());

                                            objSqlCommand.CommandType = CommandType.StoredProcedure;
                                            objSqlCommand.CommandText = "Sp_HRMS_Insert_Select_Update_HRMS_Deductions_Other_Details";
                                            objSqlCommand.Parameters.AddWithValue("@PF_Code", objDeductBAL.PF_Code);
                                            objSqlCommand.Parameters.AddWithValue("@Define_Range_Other_HeadName", objDeductBAL.Define_Range_Other_HeadName);
                                            objSqlCommand.Parameters.AddWithValue("@Define_Range_Criteria", objDeductBAL.Define_Range_Criteria);
                                            objSqlCommand.Parameters.AddWithValue("@Define_Amount", objDeductBAL.Define_Amount);
                                            objSqlCommand.Parameters.AddWithValue("@Deduct_On_Other_Heads", objDeductBAL.Deduct_On_Other_Heads);
                                            objSqlCommand.Parameters.AddWithValue("@Salary_Range", objDeductBAL.Salary_Range);
                                            objSqlCommand.Parameters.AddWithValue("@Creation_Company", objDeductBAL.Creation_Company);
                                            objSqlCommand.Parameters.AddWithValue("@Created_By", objDeductBAL.Created_By);
                                            objSqlCommand.Parameters.AddWithValue("@Created_Date", objDeductBAL.Created_Date);
                                            objSqlCommand.Parameters.AddWithValue("@Modified_By", objDeductBAL.Modified_By);
                                            objSqlCommand.Parameters.AddWithValue("@Modified_Date", objDeductBAL.Modified_Date);
                                            objSqlCommand.Parameters.AddWithValue("@Parameter", objDeductBAL.Parameter);
                                            objSqlCommand.Transaction = transaction;
                                            if (objSqlCommand.ExecuteNonQuery() != 0)
                                            {
                                                credit1Result = true;
                                            }
                                        }
                                    }
                                }
                            }

                            if (debitResult && creditResult && Debit1Result && credit1Result)
                            {
                                transaction.Commit();

                                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Saved Successfully');", true);

                                //ClearAllFields();
                            }
                            else
                            {
                                transaction.Rollback();
                                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Enter Bank Details');", true);
                            TextBox txtHead_Name = (TextBox)GrdOther.Rows[a3 - 1].FindControl("txtHead_Name") as TextBox;
                            txtHead_Name.Focus();
                        }

                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Enter Bank Details');", true);
                        TextBox txtIncomeslabPTFrom = (TextBox)grdPT.Rows[a2 - 1].FindControl("txtIncomeslabPTFrom") as TextBox;
                        txtIncomeslabPTFrom.Focus();
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Enter Bank Details');", true);
                    TextBox txtIncomeslabFrom = (TextBox)GrdDefineRange.Rows[a1 - 1].FindControl("txtIncomeslabFrom") as TextBox;
                    txtIncomeslabFrom.Focus();
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
                Session["Error"] = ex.Message.ToString();
            }

            finally
            {
            }
        }

        public void ClearAllFields()
        {
            //txtDeducton.Text = txtESIDeduct.Text = txtESIEmpLimit.Text = txtESIPercentage.Text = txtInShortName.Text = txtLimitforEmpContr.Text =
            //    txtOtherDeduct.Text = txtPercentage.Text = txtPTDeduct.Text = txtShortName.Text = txtTaxDeduct.Text = "";

        }
        //Update Button event
        public void btnUpdate_Click(object sender, EventArgs e)
        {
            Parameter = 1;
            List<HRMS_Deductions_IncomeTax_Detail> SUM = (from ST in objdb.HRMS_Deductions_IncomeTax_Details
                                                          where ST.Creation_Company == Session["ClientRegistrationNo"].ToString()
                                                          && ST.PF_Code == (string)Session["pfcode"]
                                                          select ST).ToList();
            objdb.HRMS_Deductions_IncomeTax_Details.DeleteAllOnSubmit(SUM);
            objdb.SubmitChanges();
            List<HRMS_Deductions_Other_Detail> SUM1 = (from ST in objdb.HRMS_Deductions_Other_Details
                                                       where ST.Creation_Company == Session["ClientRegistrationNo"].ToString()
                                                          && ST.PF_Code == (string)Session["pfcode"]
                                                       select ST).ToList();
            objdb.HRMS_Deductions_Other_Details.DeleteAllOnSubmit(SUM1);
            objdb.SubmitChanges();
            List<HRMS_Deductions_ProfessionalTax_PT_Detail> SUM2 = (from ST in objdb.HRMS_Deductions_ProfessionalTax_PT_Details
                                                                    where ST.Creation_Company == Session["ClientRegistrationNo"].ToString()
                                                          && ST.PF_Code == (string)Session["pfcode"]
                                                                    select ST).ToList();
            objdb.HRMS_Deductions_ProfessionalTax_PT_Details.DeleteAllOnSubmit(SUM2);
            objdb.SubmitChanges();
            List<HRMS_Deductions_PensionFund_Detail> SUM3 = (from ST in objdb.HRMS_Deductions_PensionFund_Details
                                                             where ST.Creation_Company == Session["ClientRegistrationNo"].ToString()
                                                          && ST.PF_Code == (string)Session["pfcode"]
                                                             select ST).ToList();
            objdb.HRMS_Deductions_PensionFund_Details.DeleteAllOnSubmit(SUM3);
            objdb.SubmitChanges();
            Saving();

        }

        #endregion

        #region Public Methods
        // binding checkboxes in checkbox list
        //public void OTList()
        //{
        //    try
        //    {
        //        string Creation_Company = Session["ClientRegistrationNo"].ToString();
        //        string Company_Name = Session["CreationCompany"].ToString();

        //        List<HRMS_Additions_Master> objShortInfoDetails = (from Short in objdb.HRMS_Additions_Masters
        //                                                           where Short.Creation_Company == Creation_Company
        //                                                           select Short).ToList();
        //        //List<SCM_Administrator_CompanyInfo> objCompanyInfoDetails = ObjSCM_CompanyInfo.GetData_CompanyInfo(Creation_Company);
        //        if (objShortInfoDetails.Count > 0)
        //        {

        //            chlOTLists.DataSource = objShortInfoDetails;

        //            chlOTLists.DataTextField = "Short_Name";
        //            chlOTLists.DataValueField = "Short_Name";
        //            chlOTLists.DataBind();
        //            //ddlProdGroup.Items.Insert(0, new ListItem("--Group--", "0")); //updated code
        //        }
        //        else
        //        {
        //            chlOTLists.Items.Insert(0, new ListItem(Company_Name, Company_Name)); //updated code
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        string Error = ex.ToString();
        //        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + Error + "');", true);
        //    }
        //    finally
        //    {
        //    }
        //}


        public void GetDeductionData()
        {
            var pf = objdb.HRMS_Deductions_PensionFund_Details.OrderByDescending(x => x.PF_Code).ToList().FirstOrDefault();

            if (pf != null)
            {
                string pfcode = pf.PF_Code;
                Session["pfcode"] = pfcode;
                string Creation_Company = Session["ClientRegistrationNo"].ToString();

                var data = objdb.sp_Get_All_Deductions_Page_Data(pfcode, Creation_Company).ToList();
                if (data.Count > 0)
                {
                    txtPercentage.Text = data[0].PF_Percentage.ToString();
                    txtLimitforEmpContr.Text = data[0].PF_Emp_Contribution.ToString();
                    txtDeducton.Text = data[0].PF_Deduct.ToString();
                    txtESIPercentage.Text = data[0].ESI_Percentage.ToString();
                    txtESIDeduct.Text = data[0].ESI_Deduct.ToString();


                    var distinctGrdDefine = data.GroupBy(x => x.Define_Range_From)
                        .Select(g => g.First())
                        .ToList();

                    GrdDefineRange.DataSource = distinctGrdDefine;
                    GrdDefineRange.DataBind();

                    txtTaxDeduct.Text = data[0].Deduct_On_Heads.ToString();

                    var distinctGrdOther = data.GroupBy(x => x.Define_Range_Other_HeadName)
                       .Select(g => g.First())
                       .ToList();
                    GrdOther.DataSource = distinctGrdOther;
                    GrdOther.DataBind();
                    txtOtherDeduct.Text = data[0].Deduct_On_Other_Heads;

                    var distinctGrdPT = data.GroupBy(x => x.Define_Range_PT_From)
                      .Select(g => g.First())
                      .ToList();
                    grdPT.DataSource = distinctGrdPT;
                    grdPT.DataBind();
                    txtPTDeduct.Text = data[0].Deduct_On_PT_Heads;

                    btnUpdate.Visible = true;
                    btnSave.Visible = false;
                }
                else
                {
                    btnUpdate.Visible = false;
                    btnSave.Visible = true;
                }
            }
            else
            {
                btnUpdate.Visible = false;
                btnSave.Visible = true;
            }
        }

        #endregion


        #region GridEvents
        #region GrdDefineRange Grid Events
        protected void btnAddNewRecord_Click(object sender, EventArgs e)
        {
            // Adding Empty Row Data 

            AddNewRowToGrid();

            int aa = (int)GrdDefineRange.Rows.Count;
            TextBox txtIncomeslabFrom = (TextBox)GrdDefineRange.Rows[aa - 1].FindControl("txtIncomeslabFrom") as TextBox;
            //DropDownList ddlLoanType = (DropDownList)grvLoan.Rows[aa - 1].FindControl("ddlLoanType") as DropDownList;
            txtIncomeslabFrom.Focus();
        }

        public void AddNewRowToGrid()
        {

            int rowIndex = 0;

            if (ViewState["Current_Range"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["Current_Range"];

                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {

                        TextBox txtIncomeslabFrom = (TextBox)GrdDefineRange.Rows[rowIndex].FindControl("txtIncomeslabFrom");
                        TextBox txtIncomeslabTo = (TextBox)GrdDefineRange.Rows[rowIndex].FindControl("txtIncomeslabTo");
                        TextBox txtPercentage = (TextBox)GrdDefineRange.Rows[rowIndex].FindControl("txtPercentage");
                        drCurrentRow = dtCurrentTable.NewRow();
                        dtCurrentTable.Rows[i - 1]["Define_Range_From"] = txtIncomeslabFrom.Text;

                        dtCurrentTable.Rows[i - 1]["Define_Range_To"] = txtIncomeslabTo.Text;

                        dtCurrentTable.Rows[i - 1]["Define_Amount"] = txtPercentage.Text;
                        rowIndex += 1;
                    }

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["Current_Range"] = dtCurrentTable;
                    GrdDefineRange.DataSource = dtCurrentTable;
                    GrdDefineRange.DataBind();

                }
            }
            else
            {
                Response.Write("ViewState is null");
            }


        }

        // Removing Row Data 
        protected void lbRemove_Click(object sender, EventArgs e)
        {
            AddRemoveNewRowToGrid();

            LinkButton lb = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow.RowIndex;
            if (ViewState["Current_Range"] != null)
            {

                DataTable dt = (DataTable)ViewState["Current_Range"];
                if (dt.Rows.Count > 1)
                {
                    //Remove the Selected Row data and reset row number
                    dt.Rows.Remove(dt.Rows[rowID]);

                }

                //Store the current data in ViewState for future reference
                ViewState["Current_Range"] = dt;

                GrdDefineRange.DataSource = dt;
                GrdDefineRange.DataBind();
            }
        }

        public void AddRemoveNewRowToGrid()
        {

            int rowIndex = 0;

            if (ViewState["Current_Range"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["Current_Range"];
                DataRow drCurrentRow = null;
                int index = dtCurrentTable.Rows.Count;

                if (dtCurrentTable.Rows.Count > 0)
                {

                    TextBox txtIncomeslabFrom = (TextBox)GrdDefineRange.Rows[index - 1].FindControl("txtIncomeslabFrom");

                    TextBox txtIncomeslabTo = (TextBox)GrdDefineRange.Rows[index - 1].FindControl("txtIncomeslabTo");

                    TextBox txtPercentage = (TextBox)GrdDefineRange.Rows[index - 1].FindControl("txtPercentage");
                    drCurrentRow = dtCurrentTable.NewRow();
                    dtCurrentTable.Rows[index - 1]["Define_Range_From"] = txtIncomeslabFrom.Text;

                    dtCurrentTable.Rows[index - 1]["Define_Range_To"] = txtIncomeslabTo.Text;

                    dtCurrentTable.Rows[index - 1]["Define_Amount"] = txtPercentage.Text;
                    rowIndex += 1;

                    ViewState["Current_Range"] = dtCurrentTable;
                    GrdDefineRange.DataSource = dtCurrentTable;
                    GrdDefineRange.DataBind();

                }
            }
            else
            {
                Response.Write("ViewState is null");
            }



        }
        #endregion

        #region grdPT Grid Events
        protected void btnPTAddNewRecord_Click(object sender, EventArgs e)
        {
            // Adding Empty Row Data 

            PTAddNewRowToGrid();

            int aa = (int)grdPT.Rows.Count;
            TextBox txtIncomeslabPTFrom = (TextBox)grdPT.Rows[aa - 1].FindControl("txtIncomeslabPTFrom") as TextBox;
            //DropDownList ddlLoanType = (DropDownList)grvLoan.Rows[aa - 1].FindControl("ddlLoanType") as DropDownList;
            txtIncomeslabPTFrom.Focus();
        }

        public void PTAddNewRowToGrid()
        {

            int rowIndex = 0;

            if (ViewState["Current_RangePT"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["Current_RangePT"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {

                        TextBox txtIncomeslabPTFrom = (TextBox)grdPT.Rows[rowIndex].FindControl("txtIncomeslabPTFrom");
                        TextBox txtIncomeslabPTTo = (TextBox)grdPT.Rows[rowIndex].FindControl("txtIncomeslabPTTo");
                        TextBox txtAmount = (TextBox)grdPT.Rows[rowIndex].FindControl("txtAmount");
                        drCurrentRow = dtCurrentTable.NewRow();
                        dtCurrentTable.Rows[i - 1]["Define_Range_PT_From"] = txtIncomeslabPTFrom.Text;

                        dtCurrentTable.Rows[i - 1]["Define_Range_PT_To"] = txtIncomeslabPTTo.Text;

                        dtCurrentTable.Rows[i - 1]["Define_PT_Amount"] = txtAmount.Text;
                        rowIndex += 1;
                    }

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["Current_RangePT"] = dtCurrentTable;
                    grdPT.DataSource = dtCurrentTable;
                    grdPT.DataBind();

                }
            }
            else
            {
                Response.Write("ViewState is null");
            }


        }

        // Removing Row Data 
        protected void lbPTRemove_Click(object sender, EventArgs e)
        {
            PTAddRemoveNewRowToGrid();

            LinkButton lb = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow.RowIndex;
            if (ViewState["Current_RangePT"] != null)
            {

                DataTable dt = (DataTable)ViewState["Current_RangePT"];
                if (dt.Rows.Count > 1)
                {
                    //Remove the Selected Row data and reset row number
                    dt.Rows.Remove(dt.Rows[rowID]);

                }

                //Store the current data in ViewState for future reference
                ViewState["Current_RangePT"] = dt;

                grdPT.DataSource = dt;
                grdPT.DataBind();
            }
        }

        public void PTAddRemoveNewRowToGrid()
        {

            int rowIndex = 0;

            if (ViewState["Current_RangePT"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["Current_RangePT"];
                DataRow drCurrentRow = null;
                int index = dtCurrentTable.Rows.Count;

                if (dtCurrentTable.Rows.Count > 0)
                {

                    TextBox txtIncomeslabPTFrom = (TextBox)grdPT.Rows[index - 1].FindControl("txtIncomeslabPTFrom");

                    TextBox txtIncomeslabPTTo = (TextBox)grdPT.Rows[index - 1].FindControl("txtIncomeslabPTTo");

                    TextBox txtAmount = (TextBox)grdPT.Rows[index - 1].FindControl("txtAmount");
                    drCurrentRow = dtCurrentTable.NewRow();
                    dtCurrentTable.Rows[index - 1]["Define_Range_PT_From"] = txtIncomeslabPTFrom.Text;

                    dtCurrentTable.Rows[index - 1]["Define_Range_PT_To"] = txtIncomeslabPTTo.Text;

                    dtCurrentTable.Rows[index - 1]["Define_PT_Amount"] = txtAmount.Text;
                    rowIndex += 1;

                    ViewState["Current_RangePT"] = dtCurrentTable;
                    grdPT.DataSource = dtCurrentTable;
                    grdPT.DataBind();

                }
            }
            else
            {
                Response.Write("ViewState is null");
            }



        }
        #endregion

        #region GrdOther Grid Events
        protected void btnOTAddNewRecord_Click(object sender, EventArgs e)
        {
            // Adding Empty Row Data 

            OTAddNewRowToGrid();

            int aa = (int)GrdOther.Rows.Count;
            TextBox txtHead_Name = (TextBox)GrdOther.Rows[aa - 1].FindControl("txtHead_Name") as TextBox;
            //DropDownList ddlLoanType = (DropDownList)grvLoan.Rows[aa - 1].FindControl("ddlLoanType") as DropDownList;
            txtHead_Name.Focus();
        }

        public void OTAddNewRowToGrid()
        {

            int rowIndex = 0;

            if (ViewState["Current_Range_Other"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["Current_Range_Other"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {

                        TextBox txtHead_Name = (TextBox)GrdOther.Rows[rowIndex].FindControl("txtHead_Name");

                        DropDownList ddlCriteria = (DropDownList)GrdOther.Rows[rowIndex].FindControl("ddlCriteria");
                        TextBox txtCriteria = (TextBox)GrdOther.Rows[rowIndex].FindControl("txtCriteria");

                        DropDownList ddlSalRange = (DropDownList)GrdOther.Rows[rowIndex].FindControl("ddlSalRange");
                        TextBox txtSalRange = (TextBox)GrdOther.Rows[rowIndex].FindControl("txtSalAmount");

                        TextBox txtAmount = (TextBox)GrdOther.Rows[rowIndex].FindControl("txtAmount");
                        drCurrentRow = dtCurrentTable.NewRow();
                        dtCurrentTable.Rows[i - 1]["Define_Range_Other_HeadName"] = txtHead_Name.Text;

                        dtCurrentTable.Rows[i - 1]["Define_Range_Criteria"] = ddlCriteria.SelectedValue;

                        dtCurrentTable.Rows[i - 1]["Salary_Range"] = ddlSalRange.SelectedValue; // // Newely Added Field

                        dtCurrentTable.Rows[i - 1]["OtherAmount"] = txtAmount.Text;
                        rowIndex += 1;
                    }

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["Current_Range_Other"] = dtCurrentTable;
                    GrdOther.DataSource = dtCurrentTable;
                    GrdOther.DataBind();

                }
            }
            else
            {
                Response.Write("ViewState is null");
            }


        }

        // Removing Row Data 
        protected void lbOTRemove_Click(object sender, EventArgs e)
        {
            OTAddRemoveNewRowToGrid();

            LinkButton lb = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow.RowIndex;
            if (ViewState["Current_Range_Other"] != null)
            {

                DataTable dt = (DataTable)ViewState["Current_Range_Other"];
                if (dt.Rows.Count > 1)
                {
                    //Remove the Selected Row data and reset row number
                    dt.Rows.Remove(dt.Rows[rowID]);

                }

                //Store the current data in ViewState for future reference
                ViewState["Current_Range_Other"] = dt;

                GrdOther.DataSource = dt;
                GrdOther.DataBind();
            }
        }

        public void OTAddRemoveNewRowToGrid()
        {

            int rowIndex = 0;

            if (ViewState["Current_Range_Other"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["Current_Range_Other"];
                DataRow drCurrentRow = null;
                int index = dtCurrentTable.Rows.Count;

                if (dtCurrentTable.Rows.Count > 0)
                {

                    TextBox txtHead_Name = (TextBox)GrdOther.Rows[index - 1].FindControl("txtHead_Name");

                    DropDownList ddlCriteria = (DropDownList)GrdOther.Rows[index - 1].FindControl("ddlCriteria");
                    TextBox txtCriteria = (TextBox)GrdOther.Rows[index - 1].FindControl("txtCriteria");

                    DropDownList ddlSalRange = (DropDownList)GrdOther.Rows[index - 1].FindControl("ddlSalRange");
                    TextBox txtSalRange = (TextBox)GrdOther.Rows[index - 1].FindControl("txtSalAmount");

                    TextBox txtAmount = (TextBox)GrdOther.Rows[index - 1].FindControl("txtAmount");
                    drCurrentRow = dtCurrentTable.NewRow();
                    dtCurrentTable.Rows[index - 1]["Define_Range_Other_HeadName"] = txtHead_Name.Text;

                    dtCurrentTable.Rows[index - 1]["Define_Range_Criteria"] = ddlCriteria.SelectedValue;

                    dtCurrentTable.Rows[index - 1]["Salary_Range"] = ddlSalRange.SelectedValue; // // Newely Added Field

                    dtCurrentTable.Rows[index - 1]["OtherAmount"] = txtAmount.Text;
                    rowIndex += 1;

                    ViewState["Current_Range_Other"] = dtCurrentTable;
                    GrdOther.DataSource = dtCurrentTable;
                    GrdOther.DataBind();

                }
            }
            else
            {
                Response.Write("ViewState is null");
            }
        }
        #endregion

        protected void ddlSalRange_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSalRange.SelectedItem.Text == "13000-16400")
            {
                txtAmount.Text = "500.00";
            }
            else if (ddlSalRange.SelectedItem.Text == "16401-21230")
            {
                txtAmount.Text = "650.00";
            }
            else if (ddlSalRange.SelectedItem.Text == "21231-28940")
            {
                txtAmount.Text = "850.00";
            }
            else if (ddlSalRange.SelectedItem.Text == "28941-35120")
            {
                txtAmount.Text = "1150.00";
            }
            else if (ddlSalRange.SelectedItem.Text == "35121-48600")
            {
                txtAmount.Text = "1400.00";
            }
            else if (ddlSalRange.SelectedItem.Text == "48601+")
            {
                txtAmount.Text = "2000.00";
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('please select any salary range...');", true);
            }
        }


        #endregion


    }
}