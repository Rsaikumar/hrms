﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="MonthlyPayslips.aspx.cs" Inherits="HRMS.Employee.MonthlyPayslips" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Assets/js/jquery-2.1.4.min.js"></script>
    <script src="../css/Custom/templateLoader.js"></script>
    <style>
        label.margin-pm {
            margin: 10px;
        }

        td.bck-prvn {
            background-color: #cfe3f5;
        }
    </style>
    <script type="text/javascript">
        function PrintPanel() {
            var panel = document.getElementById("<%=Divprint.ClientID %>");
            var printWindow = window.open('', '', 'height=200,width=2000');
            printWindow.document.write('<html><head><title></title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
            return false;
        }
    </script>

    <style>
        .background {
            background-color: #cfe3f5;
            font-family: Arial;
            color: black;
            height: 22px;
            text-align: center;
            font-size: 14px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../DashBoard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="#">Employee</a><i class="fa fa-angle-right"></i> </li>
                        <li><a href="../Employee/MyProfile.aspx" class="current">My Profile</a> </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <i class="fa fa-user">Employee PaySlips</i>
                        </header>
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="col-md-6 form-group">
                                            <div class="col-md-4">
                                                <asp:Label ID="Label3" runat="server" Text="Label">Name</asp:Label>
                                            </div>
                                            <div class="col-md-8">
                                                <asp:DropDownList ID="ddlempname" runat="server" class="form-control"
                                                    AutoPostBack="true" required>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-6 form-group">
                                            <div class="col-md-4">
                                                <asp:Label ID="Label1" runat="server" Text="Label">Month</asp:Label>
                                            </div>
                                            <div class="col-md-8">
                                                <asp:DropDownList ID="ddlmonth" runat="server" class="form-control"
                                                    AutoPostBack="true">
                                                    <asp:ListItem Text="Select Month" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="January" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="February" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="March" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="April" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="May" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="June" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="July" Value="7"></asp:ListItem>
                                                    <asp:ListItem Text="August" Value="8"></asp:ListItem>
                                                    <asp:ListItem Text="september" Value="9"></asp:ListItem>
                                                    <asp:ListItem Text="October" Value="10"></asp:ListItem>
                                                    <asp:ListItem Text="November" Value="11"></asp:ListItem>
                                                    <asp:ListItem Text="December" Value="12"></asp:ListItem>

                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <div class="col-md-4">
                                                <asp:Label ID="Label2" runat="server" Text="Label">Year</asp:Label>
                                            </div>
                                            <div class="col-md-8">
                                                <asp:DropDownList ID="ddlyear" runat="server" class="form-control"
                                                    required AutoPostBack="true">
                                                    <asp:ListItem Text="Select Year" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="2017" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2018" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="2019" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="2020" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="2021" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="2022" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="2023" Value="7"></asp:ListItem>
                                                    <asp:ListItem Text="2024" Value="8"></asp:ListItem>
                                                    <asp:ListItem Text="2025" Value="9"></asp:ListItem>
                                                    <asp:ListItem Text="2026" Value="10"></asp:ListItem>
                                                    <asp:ListItem Text="2027" Value="11"></asp:ListItem>

                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-12 text-center">
                                            <asp:Button CssClass="btn btn-primary" ID="btnsearch" Text="Search" runat="server" OnClick="btnsearch_Click1" />
                                            <asp:Button CssClass="btn btn-primary" ID="btnBacktoDashboard" Text="Back" runat="server" OnClick="btnBacktoDashboard_Click" />
                                        </div>
                                    </div>
                                    <div id="Divprint" runat="server" class="col-md-12">

                                        <div class="col-md-12">
                                            <div id="dvtables" runat="server">
                                                <table border="1" style="width: 100%; margin-top: 10px;">
                                                    <tr>
                                                        <td style="text-align: left; background-color: #004d94;" colspan="4">
                                                            <label class="control-label margin-pm" style="color: white;">Siri Technologies</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center" colspan="4">
                                                            <h6>Payslip</h6>
                                                            <label class="control-label" id="lblpayslipid" runat="server" title="Payslip"></label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="bck-prvn" style="width: 20%;">
                                                            <label class="control-label margin-pm">Name</label>
                                                        </td>
                                                        <td style="text-align: right; width: 30%">
                                                            <label class="control-label margin-pm" id="lblempname" runat="server"></label>
                                                        </td>
                                                        <td class="bck-prvn" style="width: 20%;">
                                                            <label class="control-label margin-pm">Monthly Days</label>
                                                        </td>
                                                        <td style="text-align: right">
                                                            <label class="control-label margin-pm" id="lblmonthlydays" runat="server"></label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="bck-prvn" style="width: 20%;">
                                                            <label class="control-label margin-pm">Employee ID</label>
                                                        </td>
                                                        <td style="text-align: right; width: 30%;">
                                                            <label class="control-label margin-pm" id="lblempid" runat="server"></label>
                                                        </td>
                                                        <td class="bck-prvn" style="width: 20%;">
                                                            <label class="control-label margin-pm">No. Of Working Days</label>
                                                        </td>
                                                        <td style="text-align: right">
                                                            <label class="control-label margin-pm" id="lblnoofworkingdays" runat="server"></label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="bck-prvn">
                                                            <label class="control-label margin-pm">Basic Amount(₹)</label>
                                                        </td>
                                                        <td style="text-align: right">
                                                            <label class="control-label margin-pm" id="lblbasicamountno" runat="server"></label>
                                                        </td>
                                                        <td class="bck-prvn" style="width: 20%;">
                                                            <label class="control-label margin-pm">No. Of Absents days</label>
                                                        </td>
                                                        <td style="text-align: right">
                                                            <label class="control-label margin-pm" id="lblabsentdays" runat="server"></label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="">
                                                            <label class="control-label margin-pm"></label>
                                                        </td>
                                                        <td style="text-align: right">
                                                            <label class="control-label margin-pm"></label>
                                                        </td>
                                                        <td class="bck-prvn" style="width: 20%;">
                                                            <label class="control-label margin-pm">OT Hrs</label>
                                                        </td>
                                                        <td style="text-align: right">
                                                            <label class="control-label margin-pm" id="lblothrs" runat="server"></label>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td class="bck-prvn">
                                                            <label class="control-label margin-pm ">Earnings</label>
                                                        </td>
                                                        <td style="text-align: right" class="bck-prvn">
                                                            <label class="control-label margin-pm ">Amount(₹)</label>
                                                        </td>
                                                        <td class="bck-prvn">
                                                            <label class="control-label margin-pm ">Deductions</label>
                                                        </td>
                                                        <td style="text-align: right" class="bck-prvn">
                                                            <label class="control-label margin-pm ">Amount(₹)</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <div class="box-body table-responsive text-center">
                                                                <div class=" form-group ">
                                                                    <asp:GridView ID="gvdearnings" runat="server" AutoGenerateColumns="false" RowStyle-CssClass="rows" Width="100%">
                                                                        <Columns>
                                                                            <asp:BoundField DataField="Name" ItemStyle-Width="174" />
                                                                            <asp:BoundField DataField="Amount" ItemStyle-Width="260" ItemStyle-HorizontalAlign="Right" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </td>

                                                        <td colspan="2">
                                                            <div class="box-body table-responsive text-center">
                                                                <div class=" form-group ">
                                                                    <asp:GridView ID="gvddeductions" runat="server" AutoGenerateColumns="false"  RowStyle-CssClass="rows" Width="100%">
                                                                        <Columns>
                                                                            <asp:BoundField DataField="Name" ItemStyle-Width="174" />
                                                                            <asp:BoundField DataField="Amount" ItemStyle-Width="260" ItemStyle-HorizontalAlign="Right" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <table style="width: 100%; margin-top: 20px; margin-bottom: 20px;">
                                                                <tr>
                                                                    <td class="bck-prvn" style="width: 20%;">
                                                                        <label class="control-label margin-pm">OT :</label>
                                                                    </td>
                                                                    <td class="bck-prvn">
                                                                        <label class="control-label margin-pm" id="lblotcal" runat="server"></label>
                                                                    </td>
                                                                    <td style="text-align: right; width: 30%;" class="bck-prvn">
                                                                        <label class="control-label margin-pm" id="lblOTamount" runat="server"></label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="bck-prvn" style="width: 20%;">
                                                                        <label class="control-label margin-pm">Total</label>
                                                                    </td>
                                                                    <td class="bck-prvn"></td>
                                                                    <td style="text-align: right; width: 30%;" class="bck-prvn">
                                                                        <label class="control-label margin-pm" id="lblearningtotal" runat="server"></label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td colspan="2">
                                                            <table style="width: 100%; margin-top: 20px; margin-bottom: 20px;">

                                                                <tr>
                                                                    <td class="bck-prvn" style="width: 20%;">
                                                                        <label class="control-label margin-pm">LOP</label>
                                                                    </td>
                                                                    <td style="text-align: right; width: 30%;" class="bck-prvn">
                                                                        <label class="control-label margin-pm" id="lbllossofpay" runat="server"></label>
                                                                    </td>

                                                                </tr>
                                                                <tr>
                                                                    <td class="bck-prvn" style="width: 20%;">
                                                                        <label class="control-label margin-pm">Total</label>
                                                                    </td>
                                                                    <td style="text-align: right; width: 30%;" class="bck-prvn">
                                                                        <label class="control-label margin-pm" id="lbldeductiontotal" runat="server"></label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="bck-prvn">
                                                            <label class="control-label margin-pm">Gross Amount(₹)</label>
                                                        </td>
                                                        <td style="text-align: right">
                                                            <label class="control-label margin-pm" id="lblgrossamount" runat="server"></label>
                                                        </td>
                                                        <td class="bck-prvn" style="width: 20%;">
                                                            <label class="control-label margin-pm">Net Amount(₹)</label>
                                                        </td>
                                                        <td style="text-align: right; width: 30%">
                                                            <label class="control-label margin-pm" id="lblnetamount" runat="server"></label>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                    </tr>
                                                </table>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </section>
                </div>
            </div>

            <div class="col-md-12 text-center">
                <asp:Button ID="btnprint" runat="server" Text="Print" OnClientClick="PrintPanel()" />
            </div>

        </section>
    </section>
</asp:Content>
