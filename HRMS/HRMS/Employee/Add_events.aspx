﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="Add_events.aspx.cs" Inherits="HRMS.Employee.Add_events" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      

    <%--------------------------------------------------------------------------------------------%>

    <div id="main">
        <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx">
                        <%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i></li>
                    <li><a href="">Calender</a><i class="fa fa-angle-right"></i> </li>
                    <%--<li><a href="Leave_Application_List.aspx">Leave Application Details</a> <i class="fa fa-angle-right"></i></li>--%>
                    <li><a href="">Add Events</a> </li>
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>
            </div>
         </div>


            <div class="row">
                <div class="col-sm-12">
                    <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                        <div class="box-title">
                            <h3>
                                <i class="fa fa-th-list"></i>Add Events</h3>
                        </div>
                        <div class="box-content">

                             <div class="container">
           <br />
    select date:<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <asp:Calendar ID="Calendar1" runat="server" OnSelectionChanged="Calendar1_SelectionChanged" BackColor="White" BorderColor="White" BorderWidth="1px" Font-Names="Verdana" Font-Size="9pt" ForeColor="Black" Height="190px" NextPrevFormat="FullMonth" Width="350px">
            <DayHeaderStyle Font-Bold="True" Font-Size="8pt" />
            <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="#333333" VerticalAlign="Bottom" />
            <OtherMonthDayStyle ForeColor="#999999" />
            <SelectedDayStyle BackColor="#333399" ForeColor="White" />
            <TitleStyle BackColor="White" BorderColor="Black" BorderWidth="4px" Font-Bold="True" Font-Size="12pt" ForeColor="#333399" />
            <TodayDayStyle BackColor="#CCCCCC" />
           </asp:Calendar>
           <br />
        write somthing:<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
        <asp:Button ID="Button1" runat="server" Text="insert" OnClick="Button1_Click" /><asp:Label ID="lbl" runat="server" Text=""></asp:Label>
        
        <br />
        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">see events in calender</asp:LinkButton>
         </div>
               </div> 

                    </div>
                </div>
            </div>
</asp:Content>
