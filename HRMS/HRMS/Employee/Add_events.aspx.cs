﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace HRMS.Employee
{
    public partial class Add_events : System.Web.UI.Page
    {
        string objConfig = ConfigurationManager.ConnectionStrings["Dbconnection"].ToString();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            DateTime d = Convert.ToDateTime(TextBox1.Text);
            string desc = TextBox2.Text;
            SqlConnection con = new SqlConnection(objConfig);
            con.Open();
            SqlCommand cmd = new SqlCommand("insert into HRMS_Calender_Events values('" + d + "','" + desc + "') ", con);
            int x = cmd.ExecuteNonQuery();
            if (x == 0)
            {

                lbl.ForeColor = System.Drawing.Color.Red;
                lbl.Text = "There is no any row affected in the database";
            }
            else
            {
                lbl.ForeColor = System.Drawing.Color.GreenYellow;
                lbl.Text = x + " - Record is inserted successfully";
            }
        }
        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            TextBox1.Text = Calendar1.SelectedDate.ToShortDateString();
        }
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Dashboard/Dashboard.aspx");
        }
    }
}