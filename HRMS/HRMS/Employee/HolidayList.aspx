﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="HolidayList.aspx.cs" Inherits="HRMS.Administrator.HolidayList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        $('#sandbox-container Text').datepicker({
        });
    </script>
    <link href="../css/Administrator/HolidayList.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #ContentPlaceHolder1_actdate_container, #ContentPlaceHolder1_CalendarExtender2_popupDiv, #ContentPlaceHolder1_CalendarExtender3_popupDiv, #ContentPlaceHolder1_CalendarExtender1_container, #ContentPlaceHolder1_CalendarExtender2_container, #ContentPlaceHolder1_actdate_popupDiv, #ContentPlaceHolder1_CalendarExtender1_popupDiv
        {
            z-index: 10000;
        }

        .DateHide
        {
            display: none;
        }
    </style>
    <script type="text/javascript" lang="javascript">
        function ControlValid() {
            if (document.getElementById('<%=ddlYear.ClientID%>').options[document.getElementById('<%=ddlYear.ClientID%>').selectedIndex].value == "Select Year") {
                alert("Please Select Year..");
                document.getElementById('<%=ddlYear.ClientID%>').focus();
                return false;
            }
            if (document.getElementById('<%=txtDate.ClientID%>').value == "") {
                alert("Please Select Date..");

                document.getElementById('<%=txtDate.ClientID%>').focus();

                return false;
            }
            if (document.getElementById('<%=txtEventName.ClientID%>').value == "") {
                alert("Please Enter Event Name..");

                document.getElementById('<%=txtEventName.ClientID%>').focus();

                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="#">Administrator</a><i class="fa fa-angle-right"></i> </li>
                        <li><a href="HolidayList.aspx" class="current">Holiday List</a> </li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <i class="fa fa-th-list">Create Holiday List</i>
                        </header>
                        <div class="panel-body">
                            <div class="col-md-12 form-group">
                                <div class="col-md-2">
                                    <asp:Label ID="Label1" runat="server" Text="Label">Year</asp:Label>
                                </div>
                                <div class="col-md-2">
                                    <asp:DropDownList ID="ddlYear" runat="server" class="form-control"
                                        OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Text="Select Year" Value="Select Year"></asp:ListItem>
                                        <asp:ListItem Text="2017" Value="2017"></asp:ListItem>
                                        <asp:ListItem Text="2018" Value="2018"></asp:ListItem>
                                        <asp:ListItem Text="2019" Value="2019"></asp:ListItem>
                                        <asp:ListItem Text="2020" Value="2020"></asp:ListItem>
                                        <asp:ListItem Text="2021" Value="2021"></asp:ListItem>
                                        <asp:ListItem Text="2022" Value="2022"></asp:ListItem>
                                        <asp:ListItem Text="2023" Value="2023"></asp:ListItem>
                                        <asp:ListItem Text="2024" Value="2024"></asp:ListItem>
                                        <asp:ListItem Text="2025" Value="2025"></asp:ListItem>
                                        <asp:ListItem Text="2026" Value="2026"></asp:ListItem>
                                        <asp:ListItem Text="2027" Value="2027"></asp:ListItem>
                                        <asp:ListItem Text="2028" Value="2028"></asp:ListItem>
                                        <asp:ListItem Text="2029" Value="2029"></asp:ListItem>
                                        <asp:ListItem Text="2030" Value="2030"></asp:ListItem>
                                        <asp:ListItem Text="2031" Value="2031"></asp:ListItem>
                                        <asp:ListItem Text="2032" Value="2032"></asp:ListItem>
                                        <asp:ListItem Text="2033" Value="2033"></asp:ListItem>
                                        <asp:ListItem Text="2034" Value="2034"></asp:ListItem>
                                        <asp:ListItem Text="2035" Value="2035"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-12 form-group">
                                <div class="col-md-2">
                                    <asp:Label ID="Label2" runat="server" Text="Date"></asp:Label>
                                </div>
                                <div class="col-md-2">
                                    <asp:TextBox ID="txtDate" runat="server" placeholder="Ex:dd-MM-YYYY" class="form-control"
                                        MaxLength="10"></asp:TextBox>
                                    <cc1:CalendarExtender ID="actdate" PopupButtonID="imgPopup" runat="server" TargetControlID="txtDate"
                                        Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                    </cc1:CalendarExtender>
                                </div>
                            </div>
                            <div class="col-md-12 form-group">
                                <div class="col-md-2">
                                    Event Name
                                </div>
                                <div class="col-md-5">
                                    <asp:TextBox ID="txtEventName" runat="server" placeholder="Event Name" class="form-control"
                                        MaxLength="25"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-sm-12" style="text-align: center">
                                <asp:Button ID="btnSave" runat="server" Text="Submit" class="btn btn-primary" OnClientClick="return ControlValid()" OnClick="btnSave_Click" />
                                <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-primary" OnClick="btnUpdate_Click" />
                                <asp:Button ID="btnCancel" runat="server" Text="Reset" class="btn" OnClick="btnCancel_Click" />

                            </div>
                        </div>
                    </section>
                </div>
            </div>



            <asp:Button ID="btnAddressDetails" runat="server" Style="display: none;" />
            <cc1:ModalPopupExtender ID="mp1" runat="server" PopupControlID="Panel1" TargetControlID="btnAddressDetails"
                CancelControlID="btnClose" BackgroundCssClass="modalBackground">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" align="center" Style="display: none;">
                <div class="modal-header" runat="server" id="Popupheader">
                    <h3>Years<a class="close-modal" href="#" id="btnClose">&times;</a></h3>
                </div>
                <div class="modal-body">
                    <div class="panel-body">
                        <div class="col-sm-12" style="">
                            <div class="form-group">
                                <label for="textfield" class="control-label col-sm-1  lCYearID" style="">
                                    Year ID
                                </label>
                                <div class="col-sm-4 TCYearID" style="margin-left: 0px;">
                                    <asp:TextBox ID="txtYearID" runat="server" class="form-control" placeholder="Year ID"
                                        TextMode="SingleLine" MaxLength="10" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="textfield" class="control-label col-sm-1 lCYear" style="">
                                    Year <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                <div class="col-sm-4 TCYear" style="margin-left: 0px;">
                                    <asp:TextBox ID="txtYear" runat="server" class="form-control" placeholder="Year"
                                        TextMode="SingleLine" MaxLength="10"></asp:TextBox>
                                </div>
                            </div>
                            <div class="popbuttons" style="">
                                <asp:Button ID="btnNewAddDetails" class="btn btn-primary"
                                    runat="server" Text="Submit" />
                                <asp:Button ID="btnAddReset" runat="server" Text="Reset" class="btn" />
                            </div>
                        </div>



                    </div>
                    <!-- /.panel -->
                </div>
            </asp:Panel>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <i class="fa fa-th-list">Create Holiday List</i>
                        </header>
                        <div class="panel-body">
                            <div class="col-sm-12" style="display: block; margin-top: 2%;">
                                <asp:Panel ID="Panel2" runat="server" GroupingText="Holidays List" Style="border-bottom: 0px solid #8F8F8F;">
                                    <div class="col-sm-12" style="margin-bottom: 0.3%;">
                                        <div class="table-responsive row">
                                            <asp:GridView ID="grdHolidayEvent" class="table table-nomargin table-bordered CustomerAddress"
                                                runat="server" DataKeyNames="Holiday_Code"
                                                OnRowCommand="grdHolidayEvent_RowCommand"
                                                AutoGenerateColumns="False" OnRowDeleting="grdHolidayEvent_RowDeleting1">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtDate" runat="server" Text='<%#GetDate(Eval("Date"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Event Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtEventName" runat="server" Text='<%#Eval("Event_Name")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lbkEdit" runat="server" CommandName="Edit" CommandArgument='<%#Eval("Holiday_Code") %>' Style="color: gray; text-decoration: none; float: left; background-image: url('../img/file_edit.png'); height: 30px; width: 35px; background-position: left; background-repeat: no-repeat;"
                                                                OnClientClick="return confirm('Are you sure you want to Edit this Record ...?')" CausesValidation="false"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete" CommandArgument='<%#Eval("Holiday_Code") %>' Style="color: gray; text-decoration: none; float: left; background-image: url('../img/delete.png'); height: 30px; width: 35px; background-position: left; background-repeat: no-repeat;"
                                                                OnClientClick="return confirm('Are you sure you want to Remove this Record ...?')" CausesValidation="false"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
