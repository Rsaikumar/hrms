﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="DesignationMaster.aspx.cs" Inherits="HRMS.Employee.DesignationMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div id="main">
        <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i>
                    </li>
                    <li><a href="RoleMaster.aspx">Depatment Master</a><i class="fa fa-angle-right"></i> </li>
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                        <div class="box-title">
                            <h3>
                                <i class="fa fa-user"></i>Designation Master </h3>
                        </div>
                        <div class="box-content">
                            <div class="form-group col-md-4">
                                <label for="textfield" class="control-label col-sm-3" style="">
                               Id<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                <div class="col-sm-9" style="" id="Div2">
                                    <asp:TextBox ID="txtdesignationid" runat="server" placeholder=" Id" ReadOnly="true" class="form-control"
                                        MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="textfield" class="control-label col-sm-3" style="">
                                  Designation Name<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                <div class="col-sm-9" style="" id="Div1">
                                    <asp:TextBox ID="txtdesignation" runat="server" placeholder="Designation Name" class="form-control"
                                        MaxLength="30"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-12 text-center">
                                <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary" OnClick="btnSave_Click" /><%--OnClientClick="return fileUploadValidation()"--%>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</asp:Content>
