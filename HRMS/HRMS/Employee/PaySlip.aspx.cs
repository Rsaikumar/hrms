﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BAL;
using DAL;
using System.Globalization;
using System.Web.Services;
using System.Collections;





namespace HRMS.Employee
{
    public partial class Slip : System.Web.UI.Page
    {
       static ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Binddetails();
              //  ddlbind();
            }
        }

      

        public void Binddetails()
        {
            try
            {
                int empcode = Convert.ToInt32(Session["Emp_Code"]);

                var objEmPCodeDUP = (from itmes in ObjDAL.USP_gettingemployeenameandid(empcode)
                                     select itmes).ToList();

                //lblempname.InnerText = objEmPCodeDUP[0].empname;
                //lblempid.InnerText = objEmPCodeDUP[0].empid.ToString();



                var objDoj = (from item in ObjDAL.HRMSEmployeeMasterOfficialInformationDetails
                              where item.ID == Convert.ToInt32(empcode)
                              select item).ToList();
                if (objDoj.Count > 0)
                {
                    //lblempdoj.InnerText = (Convert.ToDateTime(objDoj[0].DateOfJoining)).ToString("dd-MM-yyyy");
                }



                var objHeaderInfo = (from header in ObjDAL.HRMSEmployeeMasterSalaryDetails
                                     where header.ID == Convert.ToInt32(empcode)
                                     select header).ToList();
                if (objHeaderInfo.Count > 0)
                {
                    //lblbasicamount.InnerText = Convert.ToDecimal(objHeaderInfo[0].BasicAmount).ToString();
                    // lblda.InnerText = Convert.ToDecimal(objHeaderInfo[0].DA).ToString();
                    // lblhra.InnerText = Convert.ToDecimal(objHeaderInfo[0].HRA).ToString();
                    //lblcca.InnerText = Convert.ToDecimal(objHeaderInfo[0].CCA).ToString();
                    // lblgrossearnings.InnerText = Convert.ToDecimal(objHeaderInfo[0].GrossAmount).ToString();

                    var Deduction = (from header in ObjDAL.HRMSEmployeeMasterDeductionsDetails
                                     where header.ID == Convert.ToInt32(empcode)
                                     select header).ToList();
                    if (Deduction.Count > 0)
                    {
                        decimal tds = Convert.ToDecimal(Deduction[0].TDSAmount);
                        decimal pf = Convert.ToDecimal(Deduction[0].PFAmount);
                        decimal esi = Convert.ToDecimal(Deduction[0].ESIAmount);
                        decimal pt = Convert.ToDecimal(Deduction[0].NetAmount);
                        decimal ga = Convert.ToDecimal(objHeaderInfo[0].GrossAmount);
                        decimal total = tds + pf + esi + pt;
                        decimal gross = ga - total;
                        //   lblgrossdeductions.InnerText = total.ToString();
                        //   lblpf.InnerText = pf.ToString();
                        // lblesi.InnerText = esi.ToString();
                        //  lbltds.InnerText = tds.ToString();
                        // lblptamt.InnerText = pt.ToString();
                        // lblnetpayable.InnerText = gross.ToString();
                    }

                    var objHeadInfo = (from header in ObjDAL.HRMSEmplyeeMasterPersonalIdentificationDetails
                                       where header.ID == Convert.ToInt32(empcode)
                                       select header).ToList();

                    if (objHeadInfo.Count > 0)
                    {
                        //   lblpanno.InnerText = objHeadInfo[0].PanNo;
                        ////   lblaccno.InnerText = objHeadInfo[0].AccountNumber;
                        //  lblbankname.InnerText = objHeadInfo[0].BankName;

                        var Objdesignation = (from header in ObjDAL.HRMSEmployeeMasterRegistrationDetails
                                              where header.ID == Convert.ToInt32(empcode)
                                              select header).ToList();

                        if (Objdesignation.Count > 0)
                        {
                            var Objofsr = (from header in ObjDAL.HRMSRoleMasters
                                           where header.RoleId == Objdesignation[0].UserSubRole
                                           select header).ToList();
                            if (Objofsr.Count > 0)
                            {
                                //      lbldesignation.InnerText = Objofsr[0].Sub_RoleShortName;
                            }
                        }

                    }
                    else
                    {

                    }

                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        public class PayslipReport
        {
            public ArrayList employedetails { get; set; }
            public ArrayList earnningdetails { get; set; }
            public ArrayList deductiondetails { get; set; }
        }

        [WebMethod]
        public static PayslipReport AnalysisReportData()
        {
            PayslipReport obj = new PayslipReport();
            obj.employedetails = GetData();
            obj.earnningdetails = GetEarningDetails();
            obj.deductiondetails = GetDeductionDetails();
         
            return obj;
        }



        public static ArrayList GetData()
        {

            ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();
            ArrayList SalaryDetails = new ArrayList();


            var empid = (from item in ObjDAL.HRMSEmployeeMasterRegistrationDetails
                         orderby item.ID ascending
                         select item).ToList();
            if (empid.Count > 0)
            {
                for (int j = 0; j < empid.Count; j++)
                {
                    List<string> EmpSalData = new List<string>();
                    int empcode = empid[j].ID;
                    var objEmPCodeDUP = (from itmes in ObjDAL.USP_gettingemployeenameandid(empcode)
                                         select itmes).ToList();
                    EmpSalData.Add(objEmPCodeDUP[0].empname);

                    EmpSalData.Add(Convert.ToString(empcode));

                    var objDoj = (from item in ObjDAL.HRMSEmployeeMasterOfficialInformationDetails
                                  where item.EmpId == Convert.ToString(empcode)
                                  select item).ToList();

                    EmpSalData.Add((Convert.ToDateTime(objDoj[0].DateOfJoining)).ToString("dd-MM-yyyy"));


                    var objHeaderInfo = (from header in ObjDAL.HRMSEmployeeMasterSalaryDetails
                                         where header.EmpId == Convert.ToString(empcode)
                                         select header).ToList();

                    EmpSalData.Add(Convert.ToDecimal(objHeaderInfo[0].BasicAmount).ToString());
                    EmpSalData.Add(Convert.ToDecimal(objHeaderInfo[0].GrossAmount).ToString());

                    var Deduction = (from header in ObjDAL.HRMSEmployeeMasterDeductionsDetails
                                     where header.EmpId == Convert.ToString(empcode)
                                     select header).ToList();

                    EmpSalData.Add(Convert.ToDecimal(Deduction[0].NetAmount).ToString());

                    //decimal pt = Convert.ToDecimal(Deduction[0].NetAmount);
                    //decimal ga = Convert.ToDecimal(objHeaderInfo[0].GrossAmount);

                    var Objdesignation = (from header in ObjDAL.HRMSEmployeeMasterRegistrationDetails
                                          where header.ID == Convert.ToInt32(empcode)
                                          select header).ToList();

                    var Objofsr = (from header in ObjDAL.HRMSRoleMasters
                                   where header.RoleId == Objdesignation[0].UserSubRole
                                   select header).ToList();

                    EmpSalData.Add(Objofsr[0].RoleName);



                    var objEarInfo = (from header in ObjDAL.EarningPercentageAmounts
                                      where header.EmpId == Convert.ToInt32(empcode)
                                      select header).ToList();
                    decimal EarTotal = 0;
                    if (objEarInfo.Count > 0)
                    {

                        for (int k = 0; k < objEarInfo.Count; k++)
                        {
                            decimal amount = Convert.ToDecimal(objEarInfo[k].Amount);
                            EarTotal = EarTotal + amount;
                        }

                    }
                    EmpSalData.Add(Convert.ToString(EarTotal));

                    var objdedInfo = (from header in ObjDAL.DeductionPercentageAmounts
                                      where header.EmpId == Convert.ToInt32(empcode)
                                      select header).ToList();
                    decimal DedTotal = 0;
                    if (objdedInfo.Count > 0)
                    {
                        for (int k = 0; k < objdedInfo.Count; k++)
                        {
                            decimal amount = Convert.ToDecimal(objdedInfo[k].Amount);
                            DedTotal = DedTotal + amount;
                        }
                    }
                    EmpSalData.Add(Convert.ToString(DedTotal));
                    SalaryDetails.Add(EmpSalData);
                }
            }

            return SalaryDetails;
        }

      

        public class ListModel
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public decimal Amount { get; set; }
        }

        public static ArrayList GetEarningDetails()
        {
            ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();
            ArrayList Details = new ArrayList();
            var empid = (from item in ObjDAL.HRMSEmployeeMasterRegistrationDetails
                         orderby item.ID ascending
                         select item).ToList();
            if (empid.Count > 0)
            {
                for (int j = 0; j < empid.Count; j++)
                {

                    List<string> EmpeardedData = new List<string>();
                    int empcode = empid[j].ID;
                    //var objEmPCodeDUP = (from itmes in ObjDAL.USP_gettingemployeenameandid(empcode)
                    //                     select itmes).ToList();

                    var objEarInfo = (from header in ObjDAL.EarningPercentageAmounts
                                      where header.EmpId == Convert.ToInt32(empcode)
                                      select header).ToList();
                    if (objEarInfo.Count > 0)
                    {

                        for (int i = 0; i < objEarInfo.Count; i++)
                        {
                            ListModel something = new ListModel();
                            EmpeardedData.Clear();
                            something.Id = Convert.ToInt32(objEarInfo[i].EmpId);
                            something.Name = (objEarInfo[i].Name.ToString());
                            something.Amount = Convert.ToDecimal(objEarInfo[i].Amount);
                            Details.Add(something);

                        }

                    }
                }
            }
            return Details;
        }

        public static ArrayList GetDeductionDetails()
        {
            ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();
            ArrayList Details = new ArrayList();
            var empid = (from item in ObjDAL.HRMSEmployeeMasterRegistrationDetails
                         orderby item.ID ascending
                         select item).ToList();
            if (empid.Count > 0)
            {
                for (int j = 0; j < empid.Count; j++)
                {

                    List<string> EmpeardedData = new List<string>();
                    int empcode = empid[j].ID;

                    var objdedInfo = (from header in ObjDAL.DeductionPercentageAmounts
                                      where header.EmpId == Convert.ToInt32(empcode)
                                      select header).ToList();
                    if (objdedInfo.Count > 0)
                    {

                        for (int i = 0; i < objdedInfo.Count; i++)
                        {
                            ListModel something = new ListModel();
                            EmpeardedData.Clear();
                            something.Id = Convert.ToInt32(objdedInfo[i].EmpId);
                            something.Name = (objdedInfo[i].Name.ToString());
                            something.Amount = Convert.ToDecimal(objdedInfo[i].Amount);
                            Details.Add(something);
                        }

                    }
                }
            }
            return Details;
        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            //int empcode = Convert.ToInt32(Session["Emp_Code"].ToString());
            //var Month = ddlmonth.SelectedItem.Value;
            //var Year = ddlyear.SelectedItem.Text;
            //var name = ddlempname.SelectedItem.Text;
        }



        //New code

        public class EmployeeInfo
        {
            public string Name { get; set; }
            public int MonthlyDays { get; set; }
            public int EmployeeId { get; set; }
            public int WorkingDays { get; set; }
            public int AbsentDays { get; set; }
            public int ApprovedLeaves { get; set; }
            public decimal BasicAmount { get; set; }
            public decimal NetAmount { get; set; }
            public decimal GrossAmount { get; set; }
            public decimal OT { get; set; }
            public string OThr { get; set; }
            public decimal TotalOTAmount { get; set; }
            public decimal LOP { get; set; }
            public decimal EarningTotal { get; set; }
            public decimal DeductionTotal { get; set; }
            public List<Earnings> earnings { get; set; }
            public List<Deduction> deductions { get; set; }
        }

        public class Earnings
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public decimal Amount { get; set; }
        }
        public class Deduction
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public decimal Amount { get; set; }
        }

        private static List<EmployeeInfo> GetEmployeesData(int depid, int month)
        {
            var employeepaySlip = new List<EmployeeInfo>();


            var employees = ObjDAL.HRMSEmployeeMasterRegistrationDetails.Where(x => x.UserSubRole == depid).ToList();
            if (employees != null && employees.Count > 0)
            {
                foreach (var employee in employees)
                {
                    var Earningsdata = new List<Earnings>();
                    var Deductiondata = new List<Deduction>();
                    var empDetails = new EmployeeInfo();
                    var employeename = (from itmes in ObjDAL.USP_gettingemployeenameandid(employee.ID)
                                        select itmes).FirstOrDefault();
                    empDetails.Name = employeename.empname;
                    empDetails.EmployeeId = employee.ID;
                    empDetails.MonthlyDays = DateTime.DaysInMonth(DateTime.Now.Year, month);

                    //Salary Details
                    var salaryDetails = ObjDAL.HRMSEmployeeMasterSalaryDetails.FirstOrDefault(x => x.EmpId == employee.ID.ToString());
                    empDetails.BasicAmount = salaryDetails != null ? Convert.ToDecimal(salaryDetails.BasicAmount) : 0;
                    empDetails.GrossAmount = salaryDetails != null ? Convert.ToDecimal(salaryDetails.GrossAmount) : 0;
                    var earningDetails = ObjDAL.HRMSEmployeeMasterDeductionsDetails.FirstOrDefault(x => x.EmpId == employee.ID.ToString());
                    empDetails.NetAmount = salaryDetails != null ? Convert.ToDecimal(earningDetails.NetAmount) : 0;

                    //Monthly Timesheet
                    decimal OTMinutes = 0;
                    var timeSheetData = ObjDAL.TimeSheets.Where(x => x.Date.Value.Year == DateTime.Now.Year &&
                                                                     x.Date.Value.Month == month &&
                                                                     x.EmpId == employee.ID).Distinct().ToList();

                    //Loop through time sheet and calculate the total OT hours of the employee
                    foreach (var timeSheet in timeSheetData)
                    {
                        var hours = timeSheet.OTHrs.Value.Hours;
                        var minutes = timeSheet.OTHrs.Value.Minutes;
                        OTMinutes += ((hours * 60) + minutes);
                    }
                    double Othrs = Convert.ToDouble(OTMinutes);
                    TimeSpan ts = TimeSpan.FromMinutes(Othrs);


                    decimal perdaySalary = (Convert.ToDecimal(earningDetails.NetAmount) / empDetails.MonthlyDays);
                    decimal perHourSalary = ((Convert.ToDecimal(earningDetails.NetAmount) / empDetails.MonthlyDays) / 8);
                    decimal perMinuteSalary = ((Convert.ToDecimal(earningDetails.NetAmount) / empDetails.MonthlyDays) / 480);
                    decimal totalOTAmount = OTMinutes * perMinuteSalary;//Total cost for Ot hours worked.
                    empDetails.OT = OTMinutes / 60;//Minutes converting to hours
                    empDetails.TotalOTAmount = Convert.ToDecimal(totalOTAmount.ToString("#.00"));
                    if (totalOTAmount > 0)
                    {

                        //empDetails.OThr = "(" + string.Format("{0} : {1}", ts.Hours, ts.Minutes) + "*" + perHourSalary.ToString("#.00") + "hrs)" + "=" + totalOTAmount.ToString("#.00"); }
                        empDetails.OThr = string.Format("{0} : {1}", ts.Hours, ts.Minutes) + " hrs";
                    }
                    else
                    {
                        empDetails.OThr = "0hrs";
                    }


                    //Loss of pay for absent days
                    var absentDays = ObjDAL.AttendanceLists.Where(x => x.Date.Value.Year == DateTime.Now.Year &&
                                                                       x.Date.Value.Month == month &&
                                                                       x.EmpId == employee.ID.ToString() &&
                                                                       x.Status.ToLower() == "Absent")
                                                                       .ToList().Count;
                    var leaveApplied = ObjDAL.HRMSLeaveApplications.Where(x => x.AppliedDate.Value.Year == DateTime.Now.Year &&
                                                                         x.AppliedDate.Value.Month == month &&
                                                                         x.EmpCode == employee.ID.ToString() &&
                                                                         x.LeaveStatus.ToLower() == "approved")
                                                                         .ToList().Count;
                    empDetails.AbsentDays = absentDays;
                    empDetails.WorkingDays = empDetails.MonthlyDays - absentDays;

                    empDetails.ApprovedLeaves = leaveApplied;
                    empDetails.LOP = Convert.ToDecimal(((absentDays - leaveApplied) * perdaySalary).ToString("#.00"));
                    empDetails.NetAmount = Convert.ToDecimal(((empDetails.NetAmount + empDetails.TotalOTAmount) - empDetails.LOP).ToString("#.00"));
                    //Earnings
                    Earningsdata.Clear();
                    var employeeEarnings = ObjDAL.EarningPercentageAmounts.Where(x => x.EmpId == employee.ID).Distinct().ToList();
                    decimal totalEarnings = 0;
                    employeeEarnings.ForEach(e => totalEarnings += Convert.ToDecimal(e.Amount));
                    empDetails.EarningTotal = Convert.ToDecimal((totalEarnings + totalOTAmount).ToString("#.00"));

                    employeeEarnings.ForEach(e => Earningsdata.Add(new Earnings { Amount = Convert.ToDecimal(e.Amount), Name = e.Name }));
                    empDetails.earnings = Earningsdata;



                    //Deductions
                    Deductiondata.Clear();
                    var employeeDeductions = ObjDAL.DeductionPercentageAmounts.Where(x => x.EmpId == employee.ID).Distinct().ToList();

                    decimal totalDeductions = 0;
                    employeeDeductions.ForEach(e => totalDeductions += Convert.ToDecimal(e.Amount));
                    empDetails.DeductionTotal = Convert.ToDecimal((empDetails.LOP + totalDeductions).ToString("#.00"));

                    employeeDeductions.ForEach(e => Deductiondata.Add(new Deduction { Amount = Convert.ToDecimal(e.Amount), Name = e.Name }));
                    empDetails.deductions = Deductiondata;


                    employeepaySlip.Add(empDetails);
                }
            }
            return employeepaySlip;
        }


    }
}

