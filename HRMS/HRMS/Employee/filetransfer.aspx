﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="filetransfer.aspx.cs" Inherits="HRMS.Employee.filetransfer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <div class="container">
    <div>
        <br />
        <br />

        from:<asp:TextBox ID="txtfrom" runat="server"></asp:TextBox>
            <br />
            to:<asp:TextBox ID="txtto" runat="server"></asp:TextBox>
            <br />
        <asp:FileUpload ID="FileUpload1" runat="server" />
        <asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="Upload" />
        <hr />
        <asp:GridView ID="GridView1" runat="server" HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White"
            RowStyle-BackColor="#A1DCF2" AlternatingRowStyle-BackColor="White" AlternatingRowStyle-ForeColor="#000"
            AutoGenerateColumns="False" EnableModelValidation="True" style="margin-top: 0px">
<AlternatingRowStyle BackColor="White" ForeColor="#000000"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField DataField="Fromuser" HeaderText="From" />
                <asp:BoundField DataField="Name" HeaderText="File Name" />
                <asp:BoundField DataField="Touser" HeaderText="To" />
                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkDownload" runat="server" Text="Download" OnClick="DownloadFile"
                            CommandArgument='<%# Eval("Id") %>'></asp:LinkButton>
                    </ItemTemplate>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
            </Columns>

<HeaderStyle BackColor="#3AC0F2" ForeColor="White"></HeaderStyle>

<RowStyle BackColor="#A1DCF2"></RowStyle>
        </asp:GridView>
    </div>
           </div>
</asp:Content>
