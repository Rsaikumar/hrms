﻿using DAL;
using BAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace HRMS.Employee
{
    public partial class EmployeeGroup : System.Web.UI.Page
    {
        // // // Object Creation For Database
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();
        EmployeeGroupMasterBAL objEmpGroupBAL = new EmployeeGroupMasterBAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["User_Type"].ToString() == "User")
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('You Don't have Permissions To Create..');", true);
                    Response.Redirect("~/Dashboard/Dashboard.aspx");
                }
                {
                    if (!IsPostBack)
                    {
                        if (Session["ClientRegistrationNo"] == null)
                        {
                            Response.Redirect("~/Default.aspx");
                        }
                        txtSearch.Focus();
                        SetInitialRow();
                        //Binding Group List
                        Bind_Grid();
                    }
                }
            }
        }
        /// <summary>
        /// Setting Initial Empty Row Data Into Group Details 
        /// </summary>
        /// <param name="sender"></param>
        public void SetInitialRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("Group_Code", typeof(string)));
            dt.Columns.Add(new DataColumn("Group_Name", typeof(string)));
            dt.Columns.Add(new DataColumn("Short_Name", typeof(string)));
            dt.Columns.Add(new DataColumn("Applicable_Heads", typeof(string)));

            dr = dt.NewRow();

            dr["Group_Code"] = string.Empty;
            dr["Group_Name"] = string.Empty;
            dr["Short_Name"] = string.Empty;
            dr["Applicable_Heads"] = string.Empty;
            dt.Rows.Add(dr);

            grdGroupList.DataSource = dt;
            grdGroupList.DataBind();

            int columncount = 7;
            grdGroupList.Rows[0].Cells.Clear();
            grdGroupList.Rows[0].Cells.Add(new TableCell());
            grdGroupList.Rows[0].Cells[0].ColumnSpan = columncount;
            grdGroupList.Rows[0].Cells[0].Text = "No Records Found";
            //  grdStorageLocation.HeaderStyle.HorizontalAlign = "center";
            grdGroupList.Rows[0].Cells[0].ForeColor = System.Drawing.Color.Red;
        }


        #region Control Events
        //Navigate to add Employee Group
        protected void lkbAddGroup_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Employee/AddEmployeGroup.aspx");
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Employee/AddEmployeGroup.aspx");
        }

        //Search Button event
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {

                string StrSearch = txtSearch.Text.Trim().ToLower();
                string CompanyName = Session["ClientRegistrationNo"].ToString().Trim().ToLower();
                List<HRMS_Group_Master> objGroupSearch = objEmpGroupBAL.GetData_search_GroupInfo(StrSearch, CompanyName);

                if (objGroupSearch.Count > 0)
                {
                    grdGroupList.DataSource = objGroupSearch;
                    grdGroupList.DataBind();
                }
                else
                {
                    SetInitialRow();
                }


            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.ToString() + "');", true);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Bind Grid Based on Group Master
        /// </summary>
        /// <param name="sender"></param>
        public void Bind_Grid()
        {
            try
            {
                string Cmpy = (string)Session["ClientRegistrationNo"];
                string Brnach = (string)Session["BranchID"];
                string UserType = (string)Session["User_Type"];
                string UserName = (string)Session["User_Name"];
                string Cration_Company = (string)Session["CreationCompany"]; //"000001";

                var objGroupInfo = (from grp in objDAL.HRMS_Group_Masters
                                    where grp.Creation_Company == Cmpy
                                    orderby grp.Group_Code descending
                                    select grp).ToList();

                if (objGroupInfo.Count > 0)
                {
                    grdGroupList.DataSource = objGroupInfo;
                    grdGroupList.DataBind();
                }
                else
                {
                    // // // // Grid view using setting initial rows displaying Empty Header Displying
                    SetInitialRow();
                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }
            finally
            {
            }
        }
        #endregion

        #region Grid Events
        /// <summary>
        /// Row Command Grid view  For Group Details
        /// </summary>

        // For Edit and Delete Button Events in grdGroupList grid
        protected void grdGroupList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string GroupCode = e.CommandArgument.ToString();
            if (e.CommandName == "Edit")
            {

                string path = "~/Employee/AddEmployeGroup.aspx";
                Session["GroupCode"] = GroupCode;
                Response.Redirect(path);

            }
            if (e.CommandName == "Delete")
            {

                string Creation_Company = Session["ClientRegistrationNo"].ToString();

                Delete_GroupDetails(GroupCode, Creation_Company);

            }


        }

        /// <summary>
        ///  The Following Method is used for Delete Grid records to Data From Group Master Table
        /// </summary>
        public void Delete_GroupDetails(string GroupCode, string Creation_Company)
        {
            try
            {
                HRMS_Group_Master a = objEmpGroupBAL.Delete_Group_Master(GroupCode, Session["ClientRegistrationNo"].ToString());

                if (a.Group_Code != null)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Header Details Deleted Successfully ');", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Header Details Already Existing In Header Page ');", true);
                }
                Bind_Grid();
                txtSearch.Focus();

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.ToString() + "');", true);
            }
            finally
            {
            }
        }

        //Sorting Group list Grid
        protected void grdGroupList_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        // Page index changing in Group list
        protected void grdGroupList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdGroupList.PageIndex = e.NewPageIndex;
            Bind_Grid();
        }

        //Row deleting event in group list grid
        protected void grdGroupList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }


        //Row Editing event in group list Grid
        protected void grdGroupList_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        #endregion
    }
}