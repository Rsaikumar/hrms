﻿using BAL;
using DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

namespace HRMS.Employee
{
    public partial class EmployeeDetailsList : System.Web.UI.Page
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string empcode = Convert.ToString(Session["EmployeeDetailsCode"]);

                bindgirdemployeedetails();
            }
        }
        public void bindgirdemployeedetails()
        {
            ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

            //getting session from login page 
            int roleid = Convert.ToInt32(Session["role"].ToString());
            int empid = Convert.ToInt32(Session["Emp_Code"]);
            //here employee list is filtered based on employee role 
            var employeelist= (from item in objDAL.HRMSEmployeeMasterRegistrationDetails
                               join per in objDAL.HRMSEmplyeeMasterPersonalDetails on item.ID equals Convert.ToInt32(per.EmpId)
                               join official in objDAL.HRMSEmployeeMasterOfficialInformationDetails on item.ID equals Convert.ToInt32(official.EmpId)
                               where item.CreatedBy == empid.ToString()
                               select new
                               {
                                   EmpId = item.ID,
                                   FirstName = per.FirstName,
                                   LastName=per.LastName,
                                   UserRole = item.RoleType,
                                   EmployeeStatus=item.EmployeeStatus,
                                   Gender=per.Gender,
                                   DateOfBirth=per.DateOfBirth

                               }).ToList();

            if (employeelist.Count>0)
            {
                gdvemplist.DataSource = employeelist;
                gdvemplist.DataBind();
            }
            else
            {

            }
        }
        protected void gdvemplist_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(gdvemplist, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = "View Employee Details";
                e.Row.Attributes["style"] = "cursor:pointer";
            }

            
        }

        protected void gdvemplist_SelectedIndexChanged(object sender, EventArgs e)
        {
            string Empid = gdvemplist.SelectedRow.Cells[0].Text;
            string path = "~/Employee/EmployeeDetailsView.aspx";
            Session["EmployeeDetailsCode"] = Empid;
            Response.Redirect(path);
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Dashboard/Dashboard.aspx");
        }

        //protected void gdvemplist_RowDeleting(object sender, GridViewDeleteEventArgs e)
        //{
        //    try
        //    {
        //        LinkButton lnkdelete = (LinkButton)e.CommandSource;
        //        int Sno = Convert.ToInt32(lnkdelete.CommandArgument);
        //        if (e.CommandName == "Delete")
        //        {
        //            var delete = (from header in objDAL.HRMSEmployeeMasterRegistrationDetails
        //                                    where header.ID == ID
        //                                    select header).FirstOrDefault();
        //            objDAL.HRMSEmployeeMasterRegistrationDetails.Remove(delete);
        //            objDAL.SaveChanges();
        //            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "", "alert('Deleted Successfully');", true);
        //            bindgirdemployeedetails();

        //        }
        //    }
        //    catch (Exception)
        //    {
        //        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "", "alert('Please Try Later');", true);
        //    }
        //}
    }
}