﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BAL;
using DAL;

namespace HRMS.Employee
{
    public partial class AddAddiction_Heads : System.Web.UI.Page
    {
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();
        AdditionsMasterBAL objAddictionBAL = new AdditionsMasterBAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_Type"].ToString() == "Employees")
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('You Don't have Permissions To Create..');", true);
                Response.Redirect("~/Dashboard/Dashboard.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    if (Session["ClientRegistrationNo"] == null)
                    {
                        Response.Redirect("~/Dashboard/Dashboard.aspx");
                    }

                    if (ddlCaluculation.SelectedValue == "Select")
                    {
                        txtPerOfBasic.ReadOnly = true;// txtPerOfBasic.Attributes.Add("readOnly", "true");
                    }
                    if ((string)Session["AdditionCode"] != null)
                    {
                        //  Request.QueryString.Clear();
                        string ClientRegistrationNo = Session["ClientRegistrationNo"].ToString();
                        string AdditionCode = (string)Session["AdditionCode"];
                        //string LoginID = Request.QueryString["id"];
                        //Edit_HeaderDetails(AdditionCode, ClientRegistrationNo);
                    }
                    else
                    {
                        btnUpdate.Visible = false;
                    }
                }
            }
        }



        #region Events
        //clearfields
        public void ClearFields()
        {
            txtID.Text = "";
            txtHeadName.Text = "";
            txtShortName.Text = "";
            ddlCaluculation.SelectedValue = "Select";
            txtPerOfBasic.Text = "";
            if (ddlCaluculation.SelectedValue == "Select")
            {
                txtPerOfBasic.ReadOnly = true;// txtPerOfBasic.Attributes.Add("readOnly", "true");
            }

        }



        //Save Event
        protected void btnSave_Click(object sender, EventArgs e)
        {
            Saving();
        }

        //Update Event
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Update_Method();
        }

        //Reset Event
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearFields();
        }
        #endregion


        #region Database Methods

        //Saving
        private void Saving()
        {
            try
            {
                objAddictionBAL.Addition_Code = "";
                objAddictionBAL.Head_Name = txtHeadName.Text;
                objAddictionBAL.Short_Name = txtShortName.Text;
                objAddictionBAL.Caluculation = ddlCaluculation.SelectedValue;
                objAddictionBAL.Per_Of_Basic = (txtPerOfBasic.Text == "") ? Convert.ToDecimal(0.0) : Convert.ToDecimal(txtPerOfBasic.Text);
                objAddictionBAL.Ot_Head_Names = ""; //(string)ViewState["txtDeducton"]
                objAddictionBAL.Ot_Day_Working_Hrs = Convert.ToDecimal("0");
                objAddictionBAL.Ot_Night_Working_Hrs = Convert.ToDecimal("0");
                objAddictionBAL.Ot_On_Weakly_Off = Convert.ToDecimal("0");
                objAddictionBAL.Ot_On_Holiday = Convert.ToDecimal("0");

                objAddictionBAL.Created_By = Session["User_Name"].ToString();
                objAddictionBAL.Created_Date = System.DateTime.Now;
                objAddictionBAL.Creation_Company = Session["ClientRegistrationNo"].ToString();
                objAddictionBAL.Modified_By = Session["User_Name"].ToString();
                objAddictionBAL.Modified_Date = System.DateTime.Now;
                objAddictionBAL.Parameter = 1;
                if (objAddictionBAL.Insert_HRMS_Additions_Master() != 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);
                    ClearFields();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
                }

            }
            catch (Exception ex)
            {

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            { }
        }


        // Editing Header Details
        //public void Edit_HeaderDetails(string additioncode, string CreationCompany)
        //{
        //    try
        //    {

        //        var objHeaderID = objAddictionBAL.Get_Addition_Header(additioncode, CreationCompany);
        //        if (objHeaderID.Count > 0)
        //        {
        //            txtID.Text = objHeaderID[0].Addition_Code.ToString();
        //            txtHeadName.Text = objHeaderID[0].Head_Name.ToString();
        //            //txtPassword.Attributes.Add("value", txtPassword.Text);
        //            txtShortName.Text = objHeaderID[0].Short_Name.ToString();
        //            //  txtDeducton.Text = "";
        //            ddlCaluculation.SelectedValue = objHeaderID[0].Caluculation.ToString();
        //            txtPerOfBasic.Text = objHeaderID[0].Per_Of_Basic.ToString();
        //            //  txtworking.Text= objHeaderID[0].Ot_Day_Working_Hrs.ToString();
        //            //  txtNightWorking.Text = objHeaderID[0].Ot_Night_Working_Hrs.ToString();
        //            // txtWeeklyOff.Text = objHeaderID[0].Ot_On_Weakly_Off.ToString();
        //            // txtHolidays.Text = objHeaderID[0].Ot_On_Holiday.ToString();



        //            btnSave.Visible = false;
        //            btnUpdate.Visible = true;
        //            Session["AdditionCode"] = null;


        //        }
        //        else
        //        {
        //            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('No Data Found ');", true);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string Error = ex.Message.ToString();
        //        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
        //    }
        //    finally
        //    {

        //    }
        //}

        //Updating Method
        public void Update_Method()
        {


            try
            {

                objAddictionBAL.Addition_Code = txtID.Text;
                objAddictionBAL.Head_Name = txtHeadName.Text;
                objAddictionBAL.Short_Name = txtShortName.Text;
                objAddictionBAL.Caluculation = ddlCaluculation.SelectedValue;
                objAddictionBAL.Per_Of_Basic = (txtPerOfBasic.Text == "") ? Convert.ToDecimal("0") : Convert.ToDecimal(txtPerOfBasic.Text);
                objAddictionBAL.Ot_Head_Names = "";
                objAddictionBAL.Ot_Day_Working_Hrs = Convert.ToDecimal("0");
                objAddictionBAL.Ot_Night_Working_Hrs = Convert.ToDecimal("0");
                objAddictionBAL.Ot_On_Weakly_Off = Convert.ToDecimal("0");
                objAddictionBAL.Ot_On_Holiday = Convert.ToDecimal("0");
                //objAddictionBAL.Login_Id = (txtLoginId.Text == "") ? "" : txtLoginId.Text;
                //objAddictionBAL.Password = (txtPassword.Text == "") ? "" : txtPassword.Text;
                //objAddictionBAL.User_Name = (txtEmployeeName.Text == "") ? "" : txtEmployeeName.Text;
                //ObjSCM_UserRegistration.E_Mail = (txtEmail.Text == "") ? "" : txtEmail.Text;
                //ObjSCM_UserRegistration.User_Type = (ddlUserType.SelectedItem.Value == "") ? "" : ddlUserType.SelectedItem.Value;
                //ObjSCM_UserRegistration.ClientRegistrationNo = (txtCompanyID.Text == "") ? "" : txtCompanyID.Text;

                //ObjSCM_UserRegistration.Status = (ddlStatus.SelectedItem.Value == "") ? "" : ddlStatus.SelectedItem.Value;

                //ObjSCM_UserRegistration.AllowCompanyListID = (StrModules1 == "") ? "" : StrModules1;




                objAddictionBAL.Creation_Company = Session["ClientRegistrationNo"].ToString();
                objAddictionBAL.Created_By = Session["User_Name"].ToString();
                objAddictionBAL.Created_Date = System.DateTime.Now;
                objAddictionBAL.Modified_By = Session["User_Name"].ToString();
                objAddictionBAL.Modified_Date = System.DateTime.Now;

                objAddictionBAL.Parameter = 2;


                if (objAddictionBAL.Insert_HRMS_Additions_Master() != 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Saved Successfully');", true);
                    ClearFields();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved');", true);
                }


            }
            catch (Exception ex)
            {

                //ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved ');", true);
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            {

            }

        }

        /// <summary>
        /// Head Name Text Change Event   Checking Duplication Data
        /// Created By Divya
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtHeadName_OnTextChanged(object sender, EventArgs e)
        {
            string CreationCompany = Session["ClientRegistrationNo"].ToString();
            // string gname = txtGroupName.Text.ToLower().Trim();
            string GroupName = (txtHeadName.Text.ToLower().Trim() == "") ? "" : txtHeadName.Text.ToLower().Trim();
            List<HRMS_Additions_Master> objitemgroupDUP = (from itmes in ObjDAL.GetTable<HRMS_Additions_Master>()
                                                           where itmes.Creation_Company == CreationCompany &&
                                                                              itmes.Head_Name.ToLower().Trim() == GroupName
                                                           select itmes).ToList<HRMS_Additions_Master>();
            if (objitemgroupDUP.Count > 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Product Group Allready Existed');", true);
                txtHeadName.Text = "";
                txtHeadName.Focus();
            }
            else
            {
                txtShortName.Focus();
            }
        }

        /// <summary>
        /// Short Name Text change Event Checking Duplication Data
        /// Created By Divya
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtShortName_OnTextChanged(object sender, EventArgs e)
        {
            string CreationCompany = Session["ClientRegistrationNo"].ToString();
            // string gname = txtGroupName.Text.ToLower().Trim();
            string GroupName = (txtHeadName.Text.ToLower().Trim() == "") ? "" : txtHeadName.Text.ToLower().Trim();
            string ShortName = (txtShortName.Text.ToLower().Trim() == "") ? "" : txtShortName.Text.ToLower().Trim();

            List<HRMS_Additions_Master> objitemgroupDUP = (from itmes in ObjDAL.GetTable<HRMS_Additions_Master>()
                                                           where itmes.Creation_Company == CreationCompany &&
                                                                              itmes.Head_Name.ToLower().Trim() == GroupName
                                                                              && itmes.Short_Name.ToLower().Trim() == ShortName
                                                           select itmes).ToList<HRMS_Additions_Master>();
            if (objitemgroupDUP.Count > 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Product Group Allready Existed');", true);
                txtShortName.Text = "";
                txtShortName.Focus();
            }
            else
            {
                ddlCaluculation.Focus();
            }
        }

        //DRop change Event For Basic Text Box readonly property 
        protected void ddlCaluculation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCaluculation.SelectedValue != "% Of Basic")
            {
                txtPerOfBasic.ReadOnly = true;// txtPerOfBasic.Attributes.Add("readOnly", "readOnly");
            }
            else
            {
                txtPerOfBasic.ReadOnly = false;//.Remove("readOnly", "readOnly");
                txtPerOfBasic.Focus();
            }
        }
        #endregion


    }
}