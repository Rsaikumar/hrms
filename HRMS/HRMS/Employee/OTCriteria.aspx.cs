﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


#region Private Using
using DAL;
using BAL;
using System.Configuration;
using System.Data;
using System.Reflection;
using System.Data.Linq.SqlClient;
// // // Name Space For BAL & DAL ClassLibrary
#endregion

namespace HRMS.Employee
{
    public partial class OTCriteria : System.Web.UI.Page
    {
        // // // Object Creation For Database
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

        OTCriteriaMaster objOTCriteriaBAL = new OTCriteriaMaster();


        // // // // Main Method

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["User_Type"].ToString() == "User")
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('You Don't have Permissions To Create..');", true);
                    Response.Redirect("~/Dashboard/Dashboard.aspx");
                }
                {
                    if (!IsPostBack)
                    {
                        if (Session["ClientRegistrationNo"] == null)
                        {
                            Response.Redirect("~/Default.aspx");
                        }
                        txtSearch.Focus();

                        //Bind User Registration
                        Bind_Grid();
                    }
                }
            }
        }

        #region Button Events

        // // // Navigation to User Registration Form For Created New User 
        protected void btnAdd_Click(object sender, EventArgs e)
        {

            Response.Redirect("~/Employee/AddOTCriteria.aspx");

        }

        /// <summary>
        /// Search For OT Criteria Details
        /// Created By Divya
        /// </summar
        /// 

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {

                string StrSearch = txtSearch.Text.ToString().ToLower().Trim();
                string CompanyName = Session["ClientRegistrationNo"].ToString().ToLower().Trim();
                List<HRMS_OT_Criteria_Master> objOTSearch = objOTCriteriaBAL.GetData_search_OTInfo(StrSearch, CompanyName);
                var distinctList = objOTSearch.GroupBy(x => x.OT_Code)
                         .Select(g => g.First())
                         .ToList();
                if (distinctList.Count > 0)
                {
                    grdOTMaster.DataSource = distinctList;
                    grdOTMaster.DataBind();
                }
                else
                {
                    SetInitialRow();
                }


            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.ToString() + "');", true);
            }
        }

        /// <summary>
        /// Navigation for OT Criteria create
        /// Created By Divya
        /// </summar
        /// 

        protected void lkbOTCriteria_Click(object sender, EventArgs e)
        {
            // if ((string)Session["User_Type"] == "SuperAdmin")
            //  {
            Response.Redirect("~/Employee/AddOTCriteria.aspx");
            // }

            // else
            // {
            // AdminPermissionAdd();
            // }
        }


        #endregion

        #region Methods

        /// <summary>
        /// Bind Grid Based on GrdOTMaster
        /// </summary>
        /// <param name="sender"></param>
        public void Bind_Grid()
        {
            try
            {
                string Cmpy = (string)Session["ClientRegistrationNo"];
                // string Brnach = (string)Session["BranchID"];
                string UserType = (string)Session["User_Type"];
                string UserName = (string)Session["User_Name"];
                string Cration_Company = (string)Session["CreationCompany"]; //"000001";

                var objOTInfo = (from header in objDAL.HRMS_OT_Criteria_Masters
                                 where header.Creation_Company == Cmpy
                                 orderby header.OT_Code descending
                                 select header).ToList();
                var distinctList = objOTInfo.GroupBy(x => x.OT_Code)
                         .Select(g => g.First())
                         .ToList();
                if (distinctList.Count > 0)
                {
                    grdOTMaster.DataSource = distinctList;
                    grdOTMaster.DataBind();
                }
                else
                {
                    // // // // Grid view using setting initial rows displaying Empty Header Displying
                    SetInitialRow();
                }

            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
            finally
            {
                txtSearch.Focus();
            }
        }


        /// <summary>
        /// Setting Initial Empty Row Data Into User Registration Details 
        /// Created By Divya
        /// </summary>
        /// <param name="sender"></param>
        public void SetInitialRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("OT_Code", typeof(string)));
            dt.Columns.Add(new DataColumn("Ot_Head_Names", typeof(string)));
            dr = dt.NewRow();

            dr["OT_Code"] = string.Empty;
            dr["Ot_Head_Names"] = string.Empty;

            dt.Rows.Add(dr);

            grdOTMaster.DataSource = dt;
            grdOTMaster.DataBind();
            int columncount = 7;
            grdOTMaster.Rows[0].Cells.Clear();
            grdOTMaster.Rows[0].Cells.Add(new TableCell());
            grdOTMaster.Rows[0].Cells[0].ColumnSpan = columncount;
            grdOTMaster.Rows[0].Cells[0].Text = "No Records Found";
            //  grdStorageLocation.HeaderStyle.HorizontalAlign = "center";
            grdOTMaster.Rows[0].Cells[0].ForeColor = System.Drawing.Color.Red;
        }



        #endregion

        #region User Registration Details Bind For Grid view


        /// <summary>
        /// Row Command Grid view  For UserRegistration Details
        /// </summar
        /// 

        protected void grdOTMaster_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string otCode = e.CommandArgument.ToString();
            if (e.CommandName == "Edit")
            {
                //string path = "AddUserRegistration.aspx?id=" + LoginID;
                string path = "~/Employee/AddOTCriteria.aspx";
                Session["otCode"] = otCode;
                Response.Redirect(path);
                //   AdminPermissionModify(LoginID);

            }
            if (e.CommandName == "Delete")
            {
                // AdminPermissionDelete(LoginID);
                string Creation_Company = Session["ClientRegistrationNo"].ToString();
                DeleteOTCriteria(otCode, Creation_Company);



            }


        }
        //delete method
        private void DeleteOTCriteria(string otCode, string Creation_Company)
        {
            try
            {
                List<HRMS_OT_Criteria_Master> a = objOTCriteriaBAL.Delete_HRMS_OT_Criteria_Master(otCode, Creation_Company);
                if (a.Count > 0)
                {

                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Ot Criteria Details Deleted Successfully ');", true);
                    Bind_Grid();
                    txtSearch.Focus();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Ot Criteria Details Already Existing In Products ');", true);
                }
                //  Bind_Grid();
                //  txtSearch.Focus();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            {
                //Bind_Grid();
                //
                txtSearch.Focus();
            }
        }
        /// <summary>
        ///  The Following Method is used for Delete Grid records to Data From User Registration  Table
        /// </summary>
        //public void DeleteOTCriteria(string AdditionCode, string Creation_Company)
        //{
        //    try
        //    {
        //        HRMS_Additions_Master a = objSCM_HeaderBAL.Delete_Addition_Header(AdditionCode, Session["ClientRegistrationNo"].ToString());

        //        if (a.Addition_Code != null)
        //        {
        //            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Header Details Deleted Successfully ');", true);
        //        }
        //        else
        //        {
        //            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Header Details Already Existing In Header Page ');", true);
        //        }
        //        Bind_Grid();
        //        txtSearch.Focus();

        //    }
        //    catch (Exception ex)
        //    {
        //        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.ToString() + "');", true);
        //    }
        //    finally
        //    {
        //    }
        //}



        //Child Grid bind
        public void grd2Bind(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowIndex != -1)
                {
                    string OtCode = grdOTMaster.DataKeys[e.Row.RowIndex].Value.ToString();
                    GridView grdstc = (GridView)e.Row.FindControl("grdOTChild");
                    String Creation_Company = (string)Session["ClientRegistrationNo"];
                    //string BranchID = Session["BranchID"].ToString();
                    List<HRMS_OT_Criteria_Master> objchild = (from ot in objDAL.HRMS_OT_Criteria_Masters
                                                              where ot.OT_Code == OtCode && ot.Creation_Company == Creation_Company
                                                              select ot).ToList();
                    grdstc.DataSource = objchild;
                    grdstc.DataBind();

                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
        }

        /// <summary>
        /// Paging For User Registration Details
        /// Created By Divya
        /// </summar
        /// 
        protected void grdOTMaster_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdOTMaster.PageIndex = e.NewPageIndex;
            Bind_Grid();
        }


        /// <summary>
        /// Row Delte Grid view  For UserRegistration
        /// Created By Divya
        /// </summar
        /// 
        protected void grdUserRegistration_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        /// <summary>
        /// Row Editing Grid view  For UserRegistration
        /// Created By Divya
        /// </summar
        /// 

        protected void grdUserRegistration_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        /// <summary>
        /// Row Sorting Grid view  For UserRegistration Details
        /// Created By Divya
        /// </summar
        /// 

        protected void grdOTMaster_OnSorting(object sender, GridViewSortEventArgs e)
        {
            //    string Sortdir = GetSortDirection(e.SortExpression);
            //    string SortExp = e.SortExpression;
            //    String Creation_Company = Session["ClientRegistrationNo"].ToString();
            //    List<Administrator_UserRegistration> list = objSCM_userRegistration.Get_UserRegistration(Creation_Company);


            //    if (Sortdir == "ASC")
            //    {
            //        list = Sort<Administrator_UserRegistration>(list, SortExp, SortDirection.Ascending);
            //    }
            //    else
            //    {
            //        list = Sort<Administrator_UserRegistration>(list, SortExp, SortDirection.Descending);
            //    }
            //    this.grdOTMaster.DataSource = list;
            //    this.grdOTMaster.DataBind();
            //}

            //private List<Administrator_UserRegistration> Sort<TKey>(List<Administrator_UserRegistration> list, string sortBy, SortDirection direction)
            //{
            //    PropertyInfo property = list.GetType().GetGenericArguments()[0].GetProperty(sortBy);
            //    if (direction == SortDirection.Ascending)
            //    {
            //        return list.OrderBy(e => property.GetValue(e, null)).ToList<Administrator_UserRegistration>();
            //    }
            //    else
            //    {
            //        return list.OrderByDescending(e => property.GetValue(e, null)).ToList<Administrator_UserRegistration>();
            //    }
        }

        ////  GetSortDirection Method it using PRoduct Grid Sorting  
        //private string GetSortDirection(string column)
        //{
        //    string sortDirection = "ASC";
        //    string sortExpression = ViewState["SortExpression"] as string;
        //    if (sortExpression != null)
        //    {
        //        if (sortExpression == column)
        //        {
        //            string lastDirection = ViewState["SortDirection"] as string;
        //            if ((lastDirection != null) && (lastDirection == "ASC"))
        //            {
        //                sortDirection = "DESC";
        //            }
        //        }
        //    }
        //    ViewState["SortDirection"] = sortDirection;
        //    ViewState["SortExpression"] = column;
        //    return sortDirection;
        //}

        #endregion






    }
}