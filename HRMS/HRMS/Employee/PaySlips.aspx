﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="PaySlips.aspx.cs" Inherits="HRMS.Employee.PaySlips" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
        }

        .auto-style2 {
            width: 130px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <%-- <script language="javascript">
        window.print();
</script>--%>
        <script type="text/javascript">
            function PrintPanel() {
                var panel = document.getElementById("<%=pnlContents.ClientID %>");
                var printWindow = window.open('', '', 'height=200,width=2000');
                printWindow.document.write('<html><head><title>DIV Contents</title>');
                printWindow.document.write('</head><body >');
                printWindow.document.write(panel.innerHTML);
                printWindow.document.write('</body></html>');
                printWindow.document.close();
                setTimeout(function () {
                    printWindow.print();
                }, 500);
                return false;
            }
        </script>


    </head>
    <body>
        <section id="main-content">
            <section class="wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="breadcrumbs-alt">
                            <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                            <li><a href="../Employee/PaySlips.aspx" class="current">PaySlips</a> </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <section class="panel">
                            <header class="panel-heading">
                                <i class="fa fa-th-list">PaySlips</i>
                            </header>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <div class="form-group col-md-6">
                                                <label for="textfield" class="control-label col-sm-5 lAEmpCode" style="">
                                                    Emp Code
                                                </label>
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txtEmpCode" runat="server" placeholder="Emp Code" MaxLength="50"
                                                        class="form-control" AutoPostBack="true" OnTextChanged="txtEmpCode_TextChanged"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="textfield" class="control-label col-sm-5">
                                                    Employee Name<span id="Span1" runat="server" style="color: Red; display: inline;">*</span></label>
                                                <div class="col-sm-7">
                                                    <asp:DropDownList ID="ddlEmpName" runat="server" class="form-control DropdownCss" TabIndex="1" AutoPostBack="True" OnSelectedIndexChanged="ddlEmpName_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 ACButtons" align="center">
                                            <asp:Button ID="btnSave" runat="server" Text="Show" class="btn btn-primary" OnClick="btnSave_Click" />
                                        </div>
                                        <div class="clearfix"></div>
                                        <div>
                                            <asp:Panel ID="pnlContents" runat="server">
                                                <div>
                                                    <div>
                                                        <table>
                                                            <tr>
                                                                <td class="auto-style1">Emp_Code:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblempcode" runat="server" Text=""></asp:Label>
                                                                </td>
                                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                                                                <td>Employee Name:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblempname" runat="server" Text=""></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>GPF NO :
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblgpfno" runat="server" Text=""></asp:Label>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>Designation:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lbldesignation" runat="server" Text=""></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>APGLI NO :
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblapglino" runat="server" Text=""></asp:Label>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>PAN NO :
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblpanno" runat="server" Text=""></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Bank A/C NO :
                                                                </td>
                                                                <td>

                                                                    <asp:Label ID="lblbankacno" runat="server" Text=""></asp:Label>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>Scale:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblscale" runat="server" Text=""></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Bank Name :
                                                                </td>
                                                                <td>

                                                                    <asp:Label ID="lblbankname" runat="server" Text=""></asp:Label>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                        </table>
                                                    </div>

                                                    <div>
                                                        <hr />

                                                        <table>
                                                            <tr>
                                                                <td>EARNINGS
                                                                </td>
                                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</td>
                                                                <td>DEDUCTIONS
                                                                </td>
                                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;</td>
                                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                                                                <td>RECOVERIES
                                                                </td>
                                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                                                                <td>DETAILS
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <hr />
                                                    </div>
                                                    <div>
                                                        <table>
                                                            <tr>
                                                                <td>Basic Pay :
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblbasicpay" runat="server" Text='<%# Eval("Basic") %>'></asp:Label>
                                                                </td>
                                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                                                                <td></td>
                                                                <td>TDS :
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lbltds" runat="server" Text='<%# Eval("APGLI")%>'></asp:Label>
                                                                </td>
                                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                                                                <td></td>
                                                                <td>HBA(P) :
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblhba" runat="server" Text=""></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>DA :
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblda" runat="server"></asp:Label>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>Provident Fund :
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblpf" runat="server"></asp:Label>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>GPFIV_LOAN :
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblgpfivloan" runat="server" Text=""></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>HRA :
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblhra" runat="server" Text='<%# Eval("HRA")%>'></asp:Label>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>ESI :
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblesi" runat="server" Text='<%# Eval("PTAX")%>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>CCA :
                                                                </td>
                                                                <td>

                                                                    <asp:Label ID="lblcca" runat="server" Text='<%# Eval("CCA")%>'></asp:Label>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>Professional Tax:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblptamt" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div>
                                                        <hr />
                                                        <table>
                                                            <tr>
                                                                <td>GROSSEARNINGS :
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblgrossamount" runat="server"></asp:Label>
                                                                </td>
                                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                                                                <td>DEDUCTIONS :
                                                                </td>

                                                                <td>
                                                                    <asp:Label ID="lbldeductions" runat="server"></asp:Label></td>
                                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                                                                <td>RECOVERIES
                                                                </td>
                                                                <td></td>
                                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                                                                <td>NET
                                                                </td>
                                                                <td></td>
                                                                <td>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                                                                <td class="auto-style2">NETPAYABLE:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblnetpayable" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>

                                                        </table>
                                                        <hr />

                                                    </div>
                                                    <div>
                                                        <table>
                                                            <tr>
                                                                <td>TOKEN NO:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lbltokenno" runat="server" Text=" "></asp:Label>
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                </td>
                                                                <td></td>
                                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                                                                <td>Bill Gross Amt Rs :
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblbillgrossamount" runat="server" Text=" "></asp:Label>
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                </td>
                                                                <td></td>
                                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                                                                <td>BILL DATE :
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblbilldate" runat="server" Text=""></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>TOKEN DATE:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lbltokendatae" runat="server" Text=" "></asp:Label>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>Bill Net Amt Rs :
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblbnars" runat="server" Text=" "></asp:Label>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>Deduct Amt Rs :
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lbldeductamtrs" runat="server" Text="" Style="text-align: center"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <hr />
                                                    </div>
                                                </div>
                                                <asp:Button ID="btnPrint" runat="server" Text="Print" align="center" OnClientClick="return PrintPanel();" />
                                            </asp:Panel>
                                        </div>




                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </section>
                    </div>







                </div>
                </div>
            </section>
            </div>
                </div>
        </section>
        </section>









    

                    <div class="box-content">
                    </div>
        </div>
            </div>
        </div>


        <br />
        <%-- <asp:button id="BtnPrint" runat="server" text="Print" onclientclick="javascript:CallPrint('bill');" xmlns:asp="#unknown" />--%>
        <%--<input type="button" value="Print" onclick="window.open('PaySlips.aspx')" />--%>
    </body>
</asp:Content>
