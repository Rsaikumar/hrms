﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BAL;
using DAL;
using System.Data.Linq.SqlClient;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using System.IO;

namespace HRMS.Employee
{
    public partial class pention : System.Web.UI.Page
    {
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();
        Leave_AllotmentBLL objLeaveAllotmentBLL = new Leave_AllotmentBLL();
        EmployeeOfficialInfoBAL objeoinfo = new EmployeeOfficialInfoBAL();
        EmployeeBAL objEmpBAL = new EmployeeBAL();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                employeenamebind();
                bindshowdetails();
                lblempcode.Visible = false;
                txtEmpCode.Visible = false;
            }
        }

        //namebinding

        public void employeenamebind()
        {
            int empcode = Convert.ToInt32(Session["Emp_Code"]);
            var bindname = (from item in ObjDAL.Usp_GettingEmployeeNameInPension()
                            where item.ID == empcode
                            select item).ToList();
            if (bindname.Count > 0)
            {
                txtempname.Text = bindname[0].empname;
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Emp Code is Not Valid');", true);
                txtEmpCode.Text = "";
                txtEmpCode.Focus();
                
            }
        }
       
        //savebutton
        public void bindshowdetails()
        {
            int empcode = Convert.ToInt32(Session["Emp_Code"]);
            var objHeaderInfo = (from header in ObjDAL.Usp_GettingEmployeeShowDetailsInPension(empcode)

                                 select header).ToList();
            if (objHeaderInfo.Count > 0)
            {
                txtdateofjoining.Text = Convert.ToDateTime(objHeaderInfo[0].DateOfJoining).ToString("dd-MM-yyyy");
                txtdateofbirth.Text = Convert.ToDateTime(objHeaderInfo[0].DateOfBirth).ToString("dd-MM-yyyy");
                int currentyear = Convert.ToInt32(DateTime.Now.Year.ToString());
                txtnoofyears.Text = currentyear.ToString();

                DateTime dt = Convert.ToDateTime(txtdateofbirth.Text);
                DateTime dtnew = dt.AddYears(60);
                string nowdate = dtnew.ToString("dd-MM-yyyy");
                txtretirement.Text = Convert.ToString(nowdate);
                if (txtdateofbirth.Text != " ")
                {
                    string mydate = txtdateofbirth.Text;
                    DateTime date = Convert.ToDateTime(mydate);
                    int y1 = date.Year;
                    if (Convert.ToInt32(txtnoofyears.Text) <= 28)
                    {
                        int i = 5;
                        double yy = (Math.Round(Convert.ToDouble(txtnoofyears.Text) + Convert.ToDouble(i)));
                        txtrule.Text = yy.ToString();
                    }
                    else
                    {
                        txtrule.Text = "33";

                    }
                }
            }
        }
    }
}