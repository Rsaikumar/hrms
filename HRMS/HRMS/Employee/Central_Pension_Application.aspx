﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="Central_Pension_Application.aspx.cs" Inherits="HRMS.Employee.pensionapplicationcentral" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script type="text/javascript">
         function PrintPanel() {
             var panel = document.getElementById("<%=pnlcontents.ClientID %>");
            var printWindow = window.open('', '', 'height=200,width=2000');
            printWindow.document.write('<html><head><title>DIV Contents</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
            return false;
        }
    </script>
    <style type="text/css">
        .auto-style1
        {
            width: 668px;
        }

        .auto-style2
        {
            width: 543px;
        }

        .auto-style3
        {
            width: 21px;
        }

        .auto-style5
        {
            width: 22px;
        }

        .auto-style6
        {
            width: 541px;
        }

        .auto-style8
        {
            width: 259px;
        }

        .auto-style10
        {
            width: 219px;
        }

        .auto-style11
        {
            width: 196px;
        }

        .auto-style12
        {
            width: 656px;
        }

        .auto-style13
        {
            width: 237px;
        }

        .auto-style14
        {
            width: 26px;
        }

        .auto-style15
        {
            width: 698px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="#">Pension</a><i class="fa fa-angle-right"></i> </li>
                        <li><a href="State_pension_application.aspx" class="current">Central Application</a> </li>
                    </ul>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <section class="panel" id="pnlcontents" runat="server">
                        <header class="panel-heading">
                            <i class="fa fa-th-list"> Central Pension Application Form</i>
                        </header>
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <h4 align="center">Form I-A</h4>
                                <h5 align="center">(See Rule 5(2), 12, 13(3), 14(1) AND 15(3))</h5>
                                <br />
                                <br />
                                <p>FORM OF APPLICATION FOR COMMUTATION OF FRACTION OF SUPERANNUATION PENSION WITHOUT MEDICAL EXAMINATION WHEN APPLICANT DESIRES THAT THE PAYMENT OF COMMUTED VALUE OF PENSION SHOULD BE AUTHORISED THROUGH THE PENSION PAYMENT ORDER</p>
                                <p>(To be submitted in duplicate at least three months before the date of retirement)</p>
                                <p>PART – I<br />
                                    <br />
                                    To
                                    <br />
                                    The Chief Secretary to the Government,<br />
                                    General Administration Department.
                                    <br />
                                    Telangana,  Hyderabad.
                                    <br />
                                </p>
                                <p>(Here indicate the designation and full address of the Head Office)</p>
                                <p>Subject:	Commutation of pension without medical examination.</p>
                                <p>Sir,I desire to commute a fraction on my pension in accordance with the provisions of the Central Civil Services (Commutation of pension) Rule, 1981. The necessary particulars are furnished below:-</p>
                                <div>
                                    <table>
                                        <tr>
                                            <td class="auto-style14">1</td>
                                            <td class="auto-style15">Name in Block Letters</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style14">2</td>
                                            <td class="auto-style15">Father’s name and else husband’s name in the case of female (Government servant)</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style14">3</td>
                                            <td class="auto-style15">Designation</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style14">4</td>
                                            <td class="auto-style15">Name of Office/Dept/Ministry in which employed</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style14">5</td>
                                            <td class="auto-style15">Date of Birth (by Christian Era)</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style14">6</td>
                                            <td class="auto-style15">Date of retirement on superannuation or on the expiry of extension in service granted   under F.R.56 (d)</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style14">7</td>
                                            <td class="auto-style15">Fraction of superannuation from pension proposed to be commuted.</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style14">8</td>
                                            <td class="auto-style15">Disbursing authority with pension is to be  drawn after retirement</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style14"></td>
                                            <td class="auto-style15">(a)Treasury/Sub-treasury (name and complete address of the treasury/ sub-treasury to be indicated)</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style14"></td>
                                            <td class="auto-style15">(b)	i) 	Branch of the nominated nationalized Bank with complete postal address</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style14"></td>
                                            <td class="auto-style15">ii) 	Bank account No. to which monthly pension is to be credited each month.</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style14"></td>
                                            <td class="auto-style15">(c)	Accounts Office of the Ministry/department/Office.</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style14">Place: Hyderabad</td>
                                            <td class="auto-style15"></td>
                                            <td>Signature</td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style14">Date:</td>
                                            <td class="auto-style15"></td>
                                            <td>Present Postal Address</td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style14">retirement</td>
                                            <td class="auto-style15"></td>
                                            <td>Postal Address after </td>
                                        </tr>
                                    </table>
                                </div>
                                <div>
                                    <h4 align="center">FORM I – A</h4>
                                    <br />

                                    <p>
                                        Note:<br />
                                        <br />
                                        The payment of commuted value of pension shall be made through the disbursing authority from which pension is to be drawn after retirement. It is not open to an applicant to draw the commuted value of pension from a disbursing authority other than the disbursing authority from which pension is to be drawn.<br />
                                        <br />
                                        * The applicant should indicate the fraction of the amount of monthly pension (Subject to a maximum of one-third thereof) which he desires to commute and not the amount in rupees
	<br />
                                        <br />
                                        * Score out which is not applicable
                                    </p>
                                    <br />
                                    <h4 align="center">PART – II</h4>
                                    <h5 align="center">(ACKNOWLEDGEMENT)</h5>
                                    <p>Received from <asp:TextBox ID="txtackrecfromname" runat="server" required></asp:TextBox> applicant in part – I of FORM I - A for on Commutation of a fraction  of pension without medical examination.</p>
                                    <table>
                                        <tr>
                                            <td class="auto-style1">Place: Hyderabad</td>
                                            <td>Signature</td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style1">Date:</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style1"></td>
                                            <td>(Head of Office)<br />
                                                Chief Secretary to Government.</td>
                                        </tr>
                                    </table>
                                    <br />
                                    <p>
                                        Note:<br />
                                        If the application has been received by the Head of Office before the expiry of three months before the retirement on superannuation, this acknowledgement should be detached from the Form and handed over to the applicant.  If the form has been received by post, it has to be acknowledged on the same day and acknowledgement sent under registered cover to the applicant.  In case it is received after the specified date, it should be accepted only if it has been put into the post on or before that date subject to the production of evidence to
that effect by the applicant.
                                    </p>
                                </div>
                                <h4 align="center">PART – III</h4>
                                <br />

                                <p>Forwarded to the Accounts Officer. O/o The Accountant General (A&E) A.P., Hyd (Here indicate the address and designation <asp:TextBox ID="txtackadddesignation" runat="server"></asp:TextBox> with the remarks that:-</p>
                                <br />
                                <p>
                                    i)	the particulars furnished by the applicant in the Part I have been verified and are correct.<br />
                                    <br />
                                    <br />
                                    ii)	the applicant is eligible to get a fraction of his pension commuted 
without medical examination; and    

                                    <br />
                                    <br />
                                    iii)	the commuted value of pension determined with reference to the 
Table applicable at the present comes to Rs.<asp:TextBox ID="txtcommutedvalue" runat="server" required></asp:TextBox> and
                                    <br />
                                    <br />
                                    iv)	the amount of residuary pension after commutation will be Rs.<asp:TextBox ID="txtamtofresiduary" runat="server" required></asp:TextBox>

                                    <br />
                                    <br />
                                    2.	The pension papers of the applicant completed in all respect were forwarded under this Ministry/Department/Office letter No. <asp:TextBox ID="txtapplicationletterno" runat="server"></asp:TextBox>
  dt.<asp:TextBox ID="txtapplicationdate" runat="server"></asp:TextBox> <ajaxToolKit:CalendarExtender ID="CalendarExtender1" PopupButtonID="imgPopup" runat="server" TargetControlID="txtapplicationdate" Format="dd-MM-yyyy" PopupPosition="BottomLeft"></ajaxToolKit:CalendarExtender>.  

                                    <br />
                                    <br />
                                    It is requested that the payment of commuted value of pension may be authorized through the Pension Payment Order which may be issued one month before the retirement of the applicant
                                    <br />
                                    <br />
                                    3.	The receipt of part I of this Form has been acknowledged in part II which has been forwarded separately to the applicant on <asp:TextBox ID="txtrecepiton" runat="server"></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarRenewalDate" PopupButtonID="imgPopup" runat="server"
                                                           TargetControlID="txtrecepiton" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                       </ajaxToolKit:CalendarExtender>
                                    <br />
                                    <br />
                                    4.	The commuted value of pension is debitable to Head of Account.<asp:TextBox ID="txtcommuteddebitedaccount" runat="server" required></asp:TextBox>
                                </p>
                                <br />
                                <table>
                                    <tr>
                                        <td class="auto-style1">Place: Hyderabad</td>
                                        <td>Signature</td>
                                    </tr>
                                    <tr>
                                        <td class="auto-style1">Date:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="auto-style1"></td>
                                        <td>(Head of Office)<br />
                                            Chief Secretary to Government.</td>
                                    </tr>
                                </table>
                                <div>
                                    <h4 align="center">FORM 7</h4>
                                    <h5 align="center">FORM FOR ASSESSING PENSION/FAMILY PENSION AND GRATUITY</h5>
                                    <h5 align="center">(To be sent in duplicate if payment is desired in a different circle of accounting unit)</h5>
                                    <h5 align="center">(See Rules 58, 60, 61(1) and (3) and 65(1))</h5>


                                    <h4 align="center">PART I</h4>
                                    <table>
                                        <tr>
                                            <td class="auto-style3">1</td>
                                            <td class="auto-style2">Name of the retiring Government employee</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3">2</td>
                                            <td class="auto-style2">Father’s / Husband’s</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3">3</td>
                                            <td class="auto-style2">Height</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2" class="auto-style3">4</td>
                                            <td rowspan="2" class="auto-style2">Marks of Identification</td>
                                            <td>i)</td>
                                        </tr>
                                        <tr>
                                            <td>ii)</td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3">6</td>
                                            <td class="auto-style2">Particulars of post held at the time of retirement:-<br />
                                                (a)	Name of the Office</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3"></td>
                                            <td class="auto-style2">(b)	Post Held</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3"></td>
                                            <td class="auto-style2">(c)	Whether the appointment mentioned above was under Government outside the Government on foreign service terms</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3">8</td>
                                            <td class="auto-style2">Whether declared substantive in any post under the Central Government</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3">9</td>
                                            <td class="auto-style2">Date of Beginning of Service</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3">10</td>
                                            <td class="auto-style2">Date of Ending of Service</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3">11</td>
                                            <td class="auto-style2">Cause of Ending of Service</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3"></td>
                                            <td class="auto-style2">(a)	Voluntary retirement on being Declared surplus (29)</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3"></td>
                                            <td class="auto-style2">(b)	Permanent absorption of public sector undertaking/autonomous body (Rule 37 – A)</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3"></td>
                                            <td class="auto-style2">(c)	Due to abolition of post (Rule 59)</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3"></td>
                                            <td class="auto-style2">(d)	Superannuation (Rule 35)</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3"></td>
                                            <td class="auto-style2">(e)	Invalidment on medical ground (Rule 38)</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3"></td>
                                            <td class="auto-style2">(f)	Voluntary/premature retirement </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3"></td>
                                            <td class="auto-style2">(g)	at the initiative of the Govt. Servant [under Rule 48, 48A and FR 56 (K)</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3"></td>
                                            <td class="auto-style2">(h)	Premature retirement at the initiative of the Govt. [Rule 48 or FR 56(K)</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3"></td>
                                            <td class="auto-style2">(i)	Compulsory retirement (Rule 40)</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3"></td>
                                            <td class="auto-style2">(j)Removal/dismissal from service(Rule 24 & 41)</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3"></td>
                                            <td class="auto-style2">(k)	Death</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3">12</td>
                                            <td class="auto-style2">In the case of compulsory retirement,the orders of the competent authority whether pension may be allowed at full rates or at reduced rates and in case of reduced rates, the percentage at which it is to be allowed</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3">13</td>
                                            <td class="auto-style2">In case of removal/dismissal from service  whether orders of competent authority have Been obtained for grant of compassionate  allowance and if so, at what rate</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3">14</td>
                                            <td class="auto-style2">Particulars relating to military service, if any:-<br />
                                                (a)Period of Military Service</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3"></td>
                                            <td class="auto-style2">(b)Terminal benefits drawn/being drawn for military service  </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3"></td>
                                            <td class="auto-style2">(c)	Whether opted for counting o0f military Service towards civil pension</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3"></td>
                                            <td class="auto-style2">(d)	If answer to (c) above is in the affirmative, Whether the terminal benefits have been Refunded</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3"></td>
                                            <td class="auto-style2">(e)	In case of ex-servicemen who are eligible for family pension under the Armed Forces Rules, whether opted to retain family pension under the Armed Forces Rules or to draw family pension under Civil rules</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3">15</td>
                                            <td class="auto-style2">Particulars relating to service in autonomous body, if any:-
                                                <br />
                                                (a)	Particulars of Service:</td>
                                            <td></td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td>Name of organization</td>
                                            <td>Post held</td>
                                            <td>Period From To </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </table>
                                    <table>

                                        <tr>
                                            <td class="auto-style5"></td>
                                            <td class="auto-style6">(b)Whether the above service is to be counted for pension</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style5"></td>
                                            <td class="auto-style6">(c)Whether the autonomous org. has discharged its pensionary liability to the Central Government.</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style5">16</td>
                                            <td class="auto-style6">Whether any department of judicial proceedings are pending against the retiring employee.</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style5">17</td>
                                            <td class="auto-style6">Qualifying Service:-<br />
                                                (a)	Details of omission, imperfection or deficiencies in the Service Book which have been ignored (under Rule 59 (1)(b) (iii)</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style5"></td>
                                            <td class="auto-style6">(b)	Period not counting as qualifying Service<br />
                                                (i)Boy Service (2nd provision to Rule 13)</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style5"></td>
                                            <td class="auto-style6">(ii)Extraordinary leave not counting as qualifying service (Rule 21)</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style5"></td>
                                            <td class="auto-style6">(iii)Periods of suspension not  treated as qualifying service (Rule 23)</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style5"></td>
                                            <td class="auto-style6">(iv)Interruptions in service (Rule 27 (i) (b) and Rule 28 (c))</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style5"></td>
                                            <td class="auto-style6">(v)	Periods of foreign Service with united Nations pension has been availed </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style5"></td>
                                            <td class="auto-style6">(vi)Any other period not treated as qualifying service (give details)</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style5"></td>
                                            <td class="auto-style6">(c)	Additions to qualify service<br />
                                                (i)	Military Service (Rule 19)</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style5"></td>
                                            <td class="auto-style6">(ii)	War Service (Rule 20)</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style5"></td>
                                            <td class="auto-style6">(iii)	Weightage under Rule 30</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style5"></td>
                                            <td class="auto-style6">(iv)Weightage of voluntary Retirement on being declared  Surplus (Rule 29)</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style5"></td>
                                            <td class="auto-style6">(d)	Net Qualifying Service</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style5"></td>
                                            <td class="auto-style6">(e)	Qualifying Service expressed in  terms of completed six monthly 
periods (Period of three months and 
over is treated as completed six 
monthly period)
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style5">18</td>
                                            <td class="auto-style6">Emoluments<br />
                                                (a)	Emoluments drawn during 10 
months proceeding retirement:-
                                            </td>
                                            <td></td>
                                        </tr>
                                    </table>
                                    <table border="1">
                                        <tr>
                                            <td class="auto-style8">FROM</td>
                                            <td class="auto-style10">TO</td>
                                            <td class="auto-style11">RATE OF PAY</td>
                                            <td>Amount</td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style8"></td>
                                            <td class="auto-style10"></td>
                                            <td class="auto-style11"></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style8"></td>
                                            <td class="auto-style10"></td>
                                            <td class="auto-style11"></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style8"></td>
                                            <td class="auto-style10"></td>
                                            <td class="auto-style11"></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style8"></td>
                                            <td class="auto-style10"></td>
                                            <td class="auto-style11">Total</td>
                                            <td></td>
                                        </tr>
                                    </table>
                                    <br />
                                    <table>
                                        <tr>
                                            <td></td>
                                            <td>(b)	If the officer was on Foreign service immediately proceeding retirement, the notional emoluments which he would have drawn under Government but for 
being on foreign service
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>(c)	Average emolument reckoned for
Pension
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>(d)	Emoluments reckoned for  retirement – gratuity/death-gratuity</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>(e)	Emoluments reckoned for family pension </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>19</td>
                                            <td>Date on which the retiring employee submitted his application for pension in Form 5</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>20</td>
                                            <td>Complete and up-to-date details of the family as given in Form – 3</td>
                                            <td></td>
                                        </tr>
                                    </table>
                                    <table border="1">
                                        <tr>
                                            <td>Sl.NO</td>
                                            <td>Name of the Member ofthe family</td>
                                            <td>Date of Birth</td>
                                            <td>Relation with the Government servant</td>
                                        </tr>
                                        <tr>
                                            <td>1.</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>2.</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>3.</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </table>

                                    <table>
                                        <tr>
                                            <td>21</td>
                                            <td>Whether nomination made for death 
gratuity/retirement-gratuity</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>22</td>
                                            <td>The date on which action initiated to 

                                                <br />
                                                (a)	Obtain the “no demand certificate” from the Directorate of Estates as
provided in Rule 59, and
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>(b)	assess the service and emoluments qualifying for pension
as provided in Rule 59, and
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>(c)	assess the Government dues other than the dues relating to the allotment of Government accommodation as provided in Rule 73 (1)</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>23</td>
                                            <td>Details of Government dues recoverable out of Gratuity:-<br />
                                                (a)	License fee for Government 
Accommodation (see sub-rules (2), (3) and (4) of Rule 72)</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>(b)	Dues referred to in Rule 73</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>24</td>
                                            <td>(a)	Proposed pension/service gratuity</td>
                                            <td>Limited to Rs.10,00,000.00
(Rupees Ten lakh only)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>(b)	Proposed dearness relief on pension (as on the date of retirement)</td>
                                            <td>Rs. 95,200.00</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>(c)	Date from which pension is to 
commence 
                                            </td>
                                            <td>01.07.2016</td>
                                        </tr>
                                        <tr>
                                            <td>25</td>
                                            <td>Date of Family Pension
                                                                                <br />
                                                (a)	Enhanced rate
                                            </td>
                                            <td>Rs. 80,000.00 X 0.5= Rs.40,000.00</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>(b)	Period for which family pension will be payable at enhanced rate</td>
                                            <td>5years or 65years age which ever is earlier in the event of death of pensioner</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>(c)	Ordinary Rate</td>
                                            <td>Rs.80,000.00 X 30/100=24,000.00 </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>(d)	Date from which ordinary rate of family Pension will be payable</td>
                                            <td>01.07.2021</td>
                                        </tr>
                                        <tr>
                                            <td>26</td>
                                            <td>Amount of retirement gratuity/death
Gratuity
                                            </td>
                                            <td>Rs. 10,00,000.00 in lumpsum</td>
                                        </tr>
                                        <tr>
                                            <td>27</td>
                                            <td>Commutation of Pension:
                                                <br />
                                                Whether simultaneously applied for 
Commutation of pension with the 
Pension application (applicable only in The case who retire on superannuation Pension
                                            </td>
                                            <td>Yes</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>(b)	The portion pension commuted</td>
                                            <td>40% of the pension (Rs.16,000.00)</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>(c)	Commuted value of pension</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>(d)	Amount of residuary pension after 
Deducting commuted pension
                                            </td>
                                            <td>Rs. 24,000.00</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>(e)	Date from which reduce pension is 
Payable
                                            </td>
                                            <td>01.07.2016 or from the next day of drawal  of commutation value of pension.
Andhra bank.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>28</td>
                                            <td>Name and address of Bank/ Pension 
Accounting Office from where pension is to Be drawn
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>29</td>
                                            <td>Head of Account to which pension and 
Gratuity are debitable
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>30</td>
                                            <td>Post-retirement address of the retiree</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>Signature of the Head of Office</td>
                                        </tr>
                                    </table>
                                </div>
                                <div>

                                    <h4 align="center">PART II</h4>

                                    <table>
                                        <tr>
                                            <td>1</td>
                                            <td>Date of receipt of pension papers by the Accounts Officer  from Head of  Office

                                            </td>
                                            <td>date</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Entitlement admitted:-

                                                <br />
                                                A 	Length of qualifying Pension

                                            </td>
                                            <td>No.of years ex.  36 years</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>B	Pension
	<br />
                                                (i)  	Class of pension

                                            </td>
                                            <td>IFS</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>(iii)	Date of commencement

                                            </td>
                                            <td>01.07.2016</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>C	Commutation of Pension
	<br />
                                                (i)	Commuted value of portion of 
pension commuted, if any

                                            </td>
                                            <td>40% (Rs.16,000.00)</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>(ii)	Residuary pension after
Commutation

                                            </td>
                                            <td>60% (Rs. 24,000.00)</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>(iii)	Date from which reduced pension is payable

                                            </td>
                                            <td>01.07.2016</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>(iv)	Date of restoration of commuted portion of pension subject to the pensioner continuing to live

                                            </td>
                                            <td>01.07.2031</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>D.	Retirement/Death Gratuity:-

                                                <br />
                                                (i)	Total Amount payable

                                            </td>
                                            <td>Rs.10,00,000.00 in          lumpsum</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>(ii)	Amount to be adjusted towards Government dues

                                            </td>
                                            <td>--Nil --</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>(iii)	Amount to be withheld for
adjustment of unassessed dues

                                            </td>
                                            <td>--Nil --</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>(iv)	Net amount to be released 
Immediately

                                            </td>
                                            <td>Rs.10,00,000.00</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>E	Family Pension
	<br />
                                                (i)	At enhanced rate

                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>(ii)	Period for which Family pension at Enhanced rate if payable

                                            </td>
                                            <td>5 years or 65 years of age which  ever is earlier in the event of death of pensioner</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>(iii)	At Normal rate

                                            </td>
                                            <td>Rs. 24,000.00</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Head of Account to which the amount of 
pension, retirement/death gratuity and 
family pension are to be debited
                                            </td>
                                            <td></td>
                                        </tr>

                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>Account Officer</td>
                                        </tr>
                                    </table>
                                </div>
                                <div>
                                    <h4 align="center">FORM – III</h4>
                                    <h5 align="center">(See Rule 54 (12)
                                    </h5>
                                    <h5 align="center">DETAILS OF FAMILY</h5>

                                    <p>
                                        Name of the Government Servant:

                                        <br />
                                        Designation:

                                        <br />
                                        Date of Birth :

                                        <br />
                                        Date of Appointment:

                                        <br />
                                        Details of the Members 
Of my Family * as on
                                                                    <br />
                                    </p>

                                    <table border="1">
                                        <tr>
                                            <td>Sl.No</td>
                                            <td>Name of the members
of  family *
                                            </td>
                                            <td>Date of
     Birth
                                            </td>
                                            <td>Relationship
   With the
   Officer
                                            </td>
                                            <td>Initials of  the Head of Office</td>
                                            <td>Remarks</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                        </tr>

                                    </table>
                                    <p>I hereby undertake to keep the above particulars up-to date by notifying to the Audit officer/Head of Office any addition of alteration.</p>

                                    <p>
                                        Place: 

                                        <br />
                                        Date: 
                                                                <br />

                                        Signature of Government Servant<br />


                                        *Family for this purpose means<br />


                                        a)	wife, in the case of a male Government Servant:
                                        <br />
                                        b)	husband, in the case of female Government Servant:
                                        <br />
                                        c)	Sons below eighteen years of age and unmarried daughters below twenty one years of age, including such son or daughter adopted legally before retirement.







                                        <br />
                                        <br />
                                        Note: Wife and Husband shall include respectively judicially separated wife and husband
                                    </p>
                                </div>
                                <div>

                                    <h4 align="center">FORM – I		</h4>
                                    <h5 align="center">(See Rule 53 (1)</h5>
                                    <h5 align="center">NOMINATION FOR DEATH-CUM RETIREMENT GRATUITY</h5>
                                    <br />
                                    <p>

                                        <br />
                                        When the Government Servant has a family and wishes to nominate one member, or more than one member thereof

                                        <br />
                                        I,<asp:TextBox ID="txtnominateename" runat="server" required></asp:TextBox>Hereby nominate the person/persons mentioned below who is/are member(s) of my family, and confer on him/them the right to receive the extent specified below, any gratuity the payment of which may be authorized by the Central Government in he event of my death, while in service and the right to receive on my death, to the extent specified below any gratuity which having become admissible to me on retirement may remain unpaid at my death:-
                                    </p>

                                </div>

                                <table border="1">
                                    <tr>
                                        <td colspan="3">Original Nominee (s)</td>
                                        <td colspan="3">*Alternate Nominee(s)</td>
                                    </tr>
                                    <tr>
                                        <td>Name & 
Address of 
Nominee/
Nominees
                                        </td>
                                        <td>Relation 
Ship with 
the 
Government
Servant
                                        </td>
                                        <td>Age </td>
                                        <td>Amount
Of Share 
of Gratuity
payable 
to each*
                                        </td>
                                        <td>Name, Address, Relationship and
age Of the persons if any, to whom
the right conferred on the nominee shall pass in the event of the nominee predeceasing
the Government Servants of the nominee dying after the death of the Government Servant before receiving payment
of gratuity
                                        </td>
                                        <td>Amount
Or share 
Of Gratuity payable
To each*
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>2</td>
                                        <td>3</td>
                                        <td>4</td>
                                        <td>5</td>
                                        <td>6</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>
                                <br />
                                <p>
                                    *This column should be filled in so as to cover the whole amount of the gratuity 
                                    <br />
                                    <br />
                                    **This amount/share of the gratuity shown in this column should cover the whole
    amount/share payable to the original nominee (s)
                                </p>
                                <br />

                                <div>
                                    <h5>Proforma for acknowledging receipt of the nomination form by the Head of Office/Accounts Officer</h5>
                                    <p>
                                        To, <asp:TextBox ID="txtofficername" runat="server"></asp:TextBox>

                                        <br />
                                        <br />
                                        <asp:TextBox ID="txtofficerdesignation" runat="server"></asp:TextBox>

                                        <br />
                                        <br />
                                        <asp:TextBox ID="txtofficeraddress" runat="server"></asp:TextBox>

                                        <br />
                                        <br />
                                        Sir,
                                        <br />
                                        <br />
                                        In acknowledging the receipt of your nomination dated he <asp:TextBox ID="txtnaminationdate" runat="server" required></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender2" PopupButtonID="imgPopup" runat="server"
                                                           TargetControlID="txtnaminationdate" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                       </ajaxToolKit:CalendarExtender>/ cancellation dated the <asp:TextBox ID="txtnominationcancellationdate" runat="server"></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender3" PopupButtonID="imgPopup" runat="server"
                                                           TargetControlID="txtnominationcancellationdate" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                       </ajaxToolKit:CalendarExtender>of the nomination made earlier in respect of gratuity in Form <asp:TextBox ID="txtformino" runat="server" required></asp:TextBox> I am to state that it has been duly placed on record.

                                        <br />
                                        Place: Hyderabad         		        Signature of Head of Office/Accounts Officer
                                        <br />
                                        Dated the 30.06.2007
                                        <br />
                                        Designation <asp:TextBox ID="txtformdesignation" runat="server" required></asp:TextBox>
                                                                   <br />
                                        Note:	The Government Servant is advised that it would be in the interest of his nominees if copies of the nominations and the related notices and  acknowledgements are kept in safe custody so that they may come into the possession of the beneficiaries in the event of his death
                                                                   <br />
                                        This nomination supersedes the nomination made by me earlier on <asp:TextBox ID="txtearliernominationdate" runat="server"></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender4" PopupButtonID="imgPopup" runat="server"
                                                           TargetControlID="txtearliernominationdate" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                       </ajaxToolKit:CalendarExtender>which stands cancelled.

                                        <br />
                                        <br />
                                        Notes:	(i)  The Government Servant shall draw lines across the blank space below
     the last entry to prevent the insertion of any name after he has signed.

                                        <br />
                                        <br />
                                        (ii) Strike out which is not applicable
     dated this <asp:TextBox ID="txtnotapplicantdate" runat="server" required></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender5" PopupButtonID="imgPopup" runat="server"
                                                           TargetControlID="txtnotapplicantdate" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                       </ajaxToolKit:CalendarExtender> day of <asp:TextBox ID="txtdayof" runat="server" required></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender6" PopupButtonID="imgPopup" runat="server"
                                                           TargetControlID="txtdayof" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                       </ajaxToolKit:CalendarExtender> 200  at Hyderabad.
                                        <br />
                                        <br />
                                        Witness to Signature

                                        <br />
                                        <br />
                                        1.<asp:TextBox ID="txtwitnesssignature1" runat="server"></asp:TextBox>

                                        <br />
                                        <br />
                                        2.<asp:TextBox ID="txtwitnesssignature2" runat="server"></asp:TextBox>

                                        <br />
                                        <br />
                                        3.<asp:TextBox ID="txtsignatureofgovt" runat="server"></asp:TextBox>		           Signature of Government Servant

                                        <br />
                                        <br />
                                        <h4 align="center">(To be filled in by he Head of Office/Accounts Officer)</h4>

                                        <p>
                                        </p>
                                        <p align="right">
                                            Signature of Head of Office/Accounts Officer
                                        </p>
                                        <p>
                                            Nomination by <asp:TextBox ID="txtnominationby" runat="server" > </asp:TextBox>. Date -0 -200
                                                                       <br />
                                            Designation:<asp:TextBox ID="txtnominationdesignation" runat="server" required></asp:TextBox>
                                                                       <br />
                                            Office of<asp:TextBox ID="txtofficeof" runat="server" required></asp:TextBox>
                                                                       <br />
                                            Designation:<asp:TextBox ID="txtdesinationofofficer" runat="server" ></asp:TextBox>
                                        </p>

                                        <p>
                                        </p>

                                    </p>


                                </div>
                                <div>

                                    <h4 align="center">FORM – IV</h4>
                                    <h5 align="center">(See Rule 55 (7))</h5>
                                    <h5 align="center">Nomination for family pension 1950</h5>

                                    <br />

                                    <br />
                                    <p>I <asp:TextBox ID="txtform4name" runat="server"></asp:TextBox>. hereby nominate the persons mentioned below who are members of my family to receive in the order shown below the family pension 1950 which may be granted by the central Government in the event of my death after completion of Ten Years qualifying service.</p>
                                    <table>
                                        <tr>
                                            <td>Name and address of nominee</td>
                                            <td>Relationship with the Government servant</td>
                                            <td>Age</td>
                                            <td>Whether married or unmarried</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>2</td>
                                            <td>3</td>
                                            <td>4</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </table>
                                    <p>This nomination supersedes the nomination made by me earlier on<asp:TextBox ID="txtform4nominationon" runat="server"></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender7" PopupButtonID="imgPopup" runat="server"
                                                           TargetControlID="txtform4nominationon" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                       </ajaxToolKit:CalendarExtender>which stands cancelled.</p>

                                    <h4>Proforma for acknowledging the receipt of the nominations
form by the Head of Office
                                    </h4>
                                    <p>
                                        To
                                        <br />
                                        <asp:TextBox ID="txtheadofficername" runat="server"></asp:TextBox>

                                        <br />
                                        <asp:TextBox ID="txtheadofficerdesignation" runat="server"></asp:TextBox>

                                        <br />
                                        <asp:TextBox ID="txtheadofficeraddress" runat="server"></asp:TextBox>
                                    </p>
                                    <p>
                                        Sir,
	In acknowledging the receipt of your nomination, dated the <asp:TextBox ID="txtform4nominationdate" runat="server"></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender8" PopupButtonID="imgPopup" runat="server"
                                                           TargetControlID="txtform4nominationdate" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                       </ajaxToolKit:CalendarExtender> / cancellation dated the <asp:TextBox ID="txtform4cancelleddate" runat="server"></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender9" PopupButtonID="imgPopup" runat="server"
                                                           TargetControlID="txtform4cancelleddate" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                       </ajaxToolKit:CalendarExtender>of  the nomination made earlier in respect of family pension 1950 in Form <asp:TextBox ID="txtform4no" runat="server"></asp:TextBox>, I am to state that it has been duly place on record.
                                    </p>
                                    <p>
                                        Place: Hyderabad&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Signature of Head of Office
                                        <br />
                                        Dated the <asp:TextBox ID="txtdatedthe" runat="server"></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender10" PopupButtonID="imgPopup" runat="server"
                                                           TargetControlID="txtdatedthe" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                       </ajaxToolKit:CalendarExtender>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Designation<asp:TextBox ID="txtofficerpost" runat="server"></asp:TextBox>
                                    </p>
                                    <p align="center">(To be filled in by the Head of Office)</p>

                                    <table>
                                        <tr>
                                            <td class="auto-style12">Nomination of Sri<asp:TextBox ID="txtnomineename" runat="server"></asp:TextBox></td>
                                            <td class="auto-style13">Signature of Head of Office</td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style12">Designation <asp:TextBox ID="txtnomineedesignation" runat="server"></asp:TextBox>   </td>
                                            <td class="auto-style13">Date <asp:TextBox ID="txttodaydate" runat="server"></asp:TextBox><ajaxToolKit:CalendarExtender ID="CalendarExtender11" PopupButtonID="imgPopup" runat="server"
                                                           TargetControlID="txttodaydate" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                       </ajaxToolKit:CalendarExtender></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style12">Office O/o the PCCF, A.P., Hyderabad
                                            </td>
                                            <td class="auto-style13">Designation <asp:TextBox ID="txtpresentdesignation" runat="server"></asp:TextBox></td>
                                        </tr>
                                    </table>
                                </div>

                                <div>
                                    <h4 align="center">FORM 5</h4>
                                    <p>
                                        See Rules 59(1) (c) and 61 (1)]<br />
                                        Particulars to be obtained by the Head of Office from the retiring Government Servant eight months before the date of his retirement
                                    </p>
                                    <table>
                                        <tr>
                                            <td>1</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td></td>
                                            <td></td>
                                        </tr>

                                    </table>
                                    <table>
                                        <tr>
                                            <td>Place: Hyderabad</td>
                                            <td>Signature</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>Designation</td>
                                        </tr>
                                        <tr>
                                            <td>Dated:	</td>
                                            <td>Ministry/Dept./Office</td>
                                        </tr>
                                    </table>
                                    <br />
                                    <p>
                                        *Two slips each bearing the Left hand thumb and finger impressions duly attested may be furnished by a person who is not literate enough to sign his name.  If such a Government Servant on account of physical disability is unable to give left had thumb and finger impressions he may give thumb and finger impressions of the right hand. Where the Government Servant has lot both the hand, he may give his toe impressions.  Impression should be duly attested by a gazetted Government servant.
                                        <br />
                                        <br />
                                        *Two copies of the passport size photograph of self only need be furnished
                                        <br />
                                        <br />
                                        (i) if the Government servant is governed by the Rule 54 of the Central Civil
     Services (Pension Rules, 1972 and is unmarried	or a widower of widow.
                                        <br />
                                        <br />
                                        (ii) if the Government servant is governed Rule 55 of the Central Civil Services
     (Pension) Rules; 1972.
                                        <br />
                                        <br />
                                        * Where it is not possible for a Government servant to submit a photograph 
    with his wife of her husband, he or she may submit separate photographs.
    The photographs shall be attested by the Head of Office.
                                        <br />
                                        <br />
                                        * Specify a few conspicuous marks, not less than two, if possible.
                                        <br />
                                        <br />
                                        * Any subsequent change of address should be notified to the Head of Office.
                                        <br />
                                        <br />
                                        *Applicable only where Rule 24 of the Central Civil service (Pension) Rules, 
1972 applies to the Government servant.
                                    </p>
                                    <p>Specimen Signatures of Sri/Smt./Kumari<br />
                                        <asp:TextBox ID="txtapplicantsignature" runat="server"></asp:TextBox><br />
                                        <asp:TextBox ID="txtapplicantdesignation" runat="server"></asp:TextBox></p>
                                    <br />

                                    <table>
                                        <tr>
                                            <td></td>
                                            <td>Specimen Signature of:</td>
                                            <td>Specimen Signature of:</td>
                                        </tr>
                                        <tr>
                                            <td>1.</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>2.</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>3.</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </table>
                                    <p>Certified that the above specimen signatures are those of</p>
                                    <p>
                                        Dated:        .   .200

                                        <br />
                                        Place:
                                    </p>
                                    <p align="right">
                                        Chief Accounts Officer.
                                                       <br />
                                        O/o Prl. Chief Conservator of Forests
                                    </p>
                                    <p>Photograph of Sri/Smt./Kumari </p>
                                    <br />
                                    <br />
                                    <br />
                                    <br />
                                    <br />
                                    <br />
                                    <br />
                                    <p>
                                        Certified that the above photograph is hat of Sri                    ¬¬¬¬¬¬¬¬                          .                                                           


                                        <br />
                                        Place: Hyderabad

                                        <br />
                                        Date:      .   . 200
                                    </p>
                                    <br />

                                    <p>JOINT PHOTOGRAPH WITH NOMINATION FOR FAMILY PENSION OR GRATUITY</p>
                                    <br />
                                    <br />
                                    <br />
                                    <br />
                                    <br />
                                    <br />
                                    <br />
                                    <br />

                                    <p>
                                        Signature of Pensioner:




                                        <br />
                                        Signature of Nominee:



                                        <br />
                                        Certified that the signature and the photographs are of those of 
                                    </p>
                                    <br />

                                    <p>
                                        1.  Name of the Pensioner: 

                                        <br />
                                        2. Name of the Nominee: 

                                        <br />
                                        Dated:    .   . 2008

                                        <br />
                                        Place: Hyderabad
                                    </p>
                                    <br />
                                    <p>
                                        PARTICULARS OF HEIGHT AND PERSONAL IDENTIFICATION MARKS OF 

                                        <br />
                                        SRI/Smt./Kumari<asp:TextBox ID="txtnameofpensioner" runat="server"></asp:TextBox>.
    <br />
                                        <asp:TextBox ID="txtpensionerdesignation" runat="server"></asp:TextBox>
                                    </p>
                                    <br />
                                    <p>
                                        1.  Height	                            	: 

                                        <br />
                                        2. Personal Identification Marks:
                                                                        <br />
                                        i) 

                                        <br />
                                        ii)
                                    </p>
                                    <p align="right">ATTESTED</p>

                                    <p>
                                        PARTICULARS OF HEIGHT AND PERSONAL IDENTIFICATION OF

                                        <asp:TextBox ID="txtpersonname" runat="server"></asp:TextBox>
                                    </p>
                                    <br />
                                    <p>
                                        1.  Height	                            	: 

                                        <br />
                                        2. Personal Identification Marks:
                                                                        <br />
                                        i) 

                                        <br />
                                        ii)
                                    </p>
                                    <p align="right">ATTESTED</p>
                                </div>

                            </div>
                        </div>
                    </section>
                </div>
                 
                <div class="col-md-12 text-center">
                     <asp:Button ID="btnPrint" runat="server" Text="Print" OnClientClick="return PrintPanel()" />
                            <asp:Button ID="btnsubmit" runat="server" Text="Save" OnClick="btnsubmit_Click" />
                        </div>
            </div>
        </section>
    </section>
</asp:Content>
