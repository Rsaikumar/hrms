﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Linq.SqlClient;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using System.Data;
using BAL;
using DAL;
using System.IO;

namespace HRMS.Employee
{
    public partial class SubRoleMaster : System.Web.UI.Page
    {
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                incrementcode();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                HRMS_Officer_Sub_Role role = new HRMS_Officer_Sub_Role();
                role.Sub_RoleID = Convert.ToInt32(txtsubroleid.Text);
                role.Sub_RoleShortName = txtsubroleshortname.Text;
                role.Sub_RoleName = txtsubroledescription.Text;
                role.CreatedBy = "";
                role.CreatedOn = DateTime.Now;
                ObjDAL.HRMS_Officer_Sub_Roles.InsertOnSubmit(role);
                ObjDAL.SubmitChanges();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Saved');", true);
                cleartext();
                incrementcode();
            }
            catch (Exception)
            {
            }

        }

        protected void cleartext()
        {
            txtsubroleshortname.Text = string.Empty;
            txtsubroledescription.Text = string.Empty;
        }

        protected void incrementcode()
        {
            var objHeaderInfo = (from header in ObjDAL.HRMS_Officer_Sub_Roles
                                 select header).ToList();
            int subroleid = Convert.ToInt32(objHeaderInfo.Count) + 1;
            txtsubroleid.Text = Convert.ToString(subroleid);
        }
    }
}