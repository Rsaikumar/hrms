﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Employee
{
    public partial class Registration : System.Web.UI.Page
    {
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            bindearningsgrid();
            binddeductionsgrid();
        }

        public void bindearningsgrid()
        {
            var earnings = (from item in ObjDAL.HRMSEarningAndDeductions
                            where item.PayslipType == "Earnings"
                            select item).ToList();


            if (earnings.Count > 0)
            {
                gvdearnings.DataSource = earnings;
                gvdearnings.DataBind();
            }
            else
            {
                gvdearnings.DataSource = earnings;
                gvdearnings.DataBind();


            }
        }


        public void binddeductionsgrid()
        {
            var deductions = (from item in ObjDAL.HRMSEarningAndDeductions
                              where item.PayslipType == "Deductions"
                              select item).ToList();


            if (deductions.Count > 0)
            {
                gvddeductions.DataSource = deductions;
                gvddeductions.DataBind();
            }
            else
            {
                gvddeductions.DataSource = deductions;
                gvddeductions.DataBind();


            }
        }
       

        //buttonPersonalNext
        protected void btnpersonal_Click2(object sender, EventArgs e)
        {
            addressinfo.Attributes.Add("class", "active");
            personalinfo.Attributes.Add("class", "");
            addressTabContent.Attributes.Add("class", "tab-pane active");
            personalTabContent.Attributes.Add("class", "tab-pane");
        }

        //buttonPrevious1
        protected void btnprevious1_Click(object sender, EventArgs e)
        {
            personalinfo.Attributes.Add("class", "active");
            addressinfo.Attributes.Add("class", "");
            personalTabContent.Attributes.Add("class", "tab-pane active");
            addressTabContent.Attributes.Add("class", "tab-pane");
        }

        //buttonAddressNext
        protected void btnaddressInfoNext_Click(object sender, EventArgs e)
        {
            officalinfo.Attributes.Add("class", "active");
            addressinfo.Attributes.Add("class", "");
            officalTabContent.Attributes.Add("class", "tab-pane active");
            addressTabContent.Attributes.Add("class", "tab-pane");
        }

        //buttonPreviousofficalInfo
        protected void btnPreviousOfficialInfo_Click(object sender, EventArgs e)
        {
            addressinfo.Attributes.Add("class", "active");
            officalinfo.Attributes.Add("class", "");
            addressTabContent.Attributes.Add("class", "tab-pane active");
            officalTabContent.Attributes.Add("class", "tab-pane");
        }

        //buttonNextOfficalInfo
        protected void btnNextOfficalInfo_Click(object sender, EventArgs e)
        {
            salaryinfo.Attributes.Add("class", "active");
            officalinfo.Attributes.Add("class", "");
            salaryTabContent.Attributes.Add("class", "tab-pane active");
            officalTabContent.Attributes.Add("class", "tab-pane");
        }

        //buttonPreviousSalaryInfo
        protected void btnPreviousSalaryInfo_Click(object sender, EventArgs e)
        {
            officalinfo.Attributes.Add("class", "active");
            salaryinfo.Attributes.Add("class", "");
            officalTabContent.Attributes.Add("class", "tab-pane active");
            salaryTabContent.Attributes.Add("class", "tab-pane");
        }

       
        protected void btnsubmit_Click1(object sender, EventArgs e)
        {
            personalinfo.Attributes.Add("class", "active");
            addressinfo.Attributes.Add("class", "");
            personalTabContent.Attributes.Add("class", "tab-pane active");
            addressTabContent.Attributes.Add("class", "tab-pane");
        }

        protected void btnnext2_Click(object sender, EventArgs e)
        {
            addressinfo.Attributes.Add("class", "active");
            personalinfo.Attributes.Add("class", "");
            addressTabContent.Attributes.Add("class", "tab-pane active");
            personalTabContent.Attributes.Add("class", "tab-pane");
        }

        protected void btntAddressPrevious1_Click(object sender, EventArgs e)
        {
            personalinfo.Attributes.Add("class", "active");
            addressinfo.Attributes.Add("class", "");
            personalTabContent.Attributes.Add("class", "tab-pane active");
            addressTabContent.Attributes.Add("class", "tab-pane");
        }

       
    }
}