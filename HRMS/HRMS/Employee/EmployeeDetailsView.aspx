﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="EmployeeDetailsView.aspx.cs" Inherits="HRMS.Employee.EmployeeDetailsView" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<link href="../css/Employee/Employee.css" rel="stylesheet" />--%>

    <%-- <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.js"></script>--%>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.js"></script>

    <script>
        $(document).ready(function () {
            $('#myTab a[href="#Location"]').tab('show');
        });
    </script>
    <style>
        label.fontweight-pm {
            font-weight: 200;
        }
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="EmployeeDetails.aspx" class="current">Employees Information</a> </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading text-left">
                            <div class="text-left">
                                <i class="fa fa-user">Employee Information</i>
                            </div>
                            <div class="text-right" style="visibility: hidden">
                                <asp:LinkButton ID="lkbedit" OnClick="lkbedit_Click1" runat="server" ForeColor="Green"><i class="fa fa-edit"> <b>Edit</b></i></asp:LinkButton>
                                <asp:LinkButton ID="lkbdelete" runat="server" ForeColor="Red" OnCommand="lkbdelete_Command" OnClientClick="javascript:return confirm('Are you sure you want to delete this class?');"><i class="fa fa-edit"> <b>Delete</b></i></asp:LinkButton>

                            </div>
                        </header>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="active">
                                        <a class="nav-link" data-toggle="tab" href="#Personal" role="tab"><b>Personal</b></a>
                                    </li>

                                </ul>
                                <div class="tab-content mt-2" style="margin-top: 20px">
                                    <div class="tab-pane active" id="Personal" role="tabpanel">

                                        <div class="col-md-12">
                                            <h5 class="sub-header"><b>Personal Infomation</b></h5>
                                            <hr />
                                        </div>
                                        <div class="form-group col-md-12" style="">
                                            <div class="row col-md-12">
                                                <div class="col-md-1">
                                                </div>
                                                <div class="col-md-2">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-5">

                                                    <div class=" form-group col-md-12" id="hideidemp" runat="server">
                                                        <label for="textfield" class="control-label col-sm-6" style="">
                                                            Emp Id<span style="color: #ff0000; font-size: 14px;"> </span>
                                                        </label>
                                                        <div class="col-sm-6" style="">
                                                            <asp:Label ID="lblempid" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label for="textfield" class="control-label col-sm-6" style="">
                                                            First Name<span style="color: #ff0000; font-size: 14px;"> </span>
                                                        </label>
                                                        <div class="col-sm-6" style="" id="">
                                                            <asp:Label ID="lblfisrtname" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label for="textfield" class="control-label col-sm-6" style="">
                                                            Last Name<span style="color: #ff0000; font-size: 14px;"> </span>
                                                        </label>
                                                        <div class="col-sm-6" style="" id="Div4">
                                                            <asp:Label ID="lbllastname" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-md-12">
                                                        <label for="textfield" class="control-label col-sm-6" style="">
                                                            Email</label>
                                                        <div class="col-sm-6" style="" id="Div36">
                                                            <asp:Label ID="lblemail" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-md-12">
                                                        <label for="textfield" class="control-label col-md-6" style="">
                                                            Date of Birth</label>
                                                        <div class="col-md-6" style="" id="Div10">
                                                            <asp:Label ID="lbldob" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label for="textfield" class="control-label col-sm-6">
                                                            Blood Group</label>
                                                        <div class="col-sm-6" style="" id="Div22">
                                                            <asp:Label ID="lblbloodgroup" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label for="textfield" class="control-label col-sm-6" style="">
                                                            Nationality</label>
                                                        <div class="col-sm-6" style="" id="Div24">
                                                            <asp:Label ID="lblnationality" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="form-group col-md-12">
                                                        <label for="textfield" class="control-label col-sm-6" style="">
                                                            Middle Name</label>
                                                        <div class="col-sm-6" style="" id="Div3">
                                                            <asp:Label ID="lblmiddlename" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>


                                                    <div class="form-group col-md-12">
                                                        <label for="textfield" class="control-label col-sm-6">
                                                            Gender</label>
                                                        <div class="col-sm-6" style="" id="Div21">
                                                            <asp:Label ID="lblgender" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label for="textfield" class="control-label col-sm-6" style="">
                                                            Mobile</label>
                                                        <div class="col-sm-6" style="" id="Div35">
                                                            <asp:Label ID="lblmobile" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label for="textfield" class="control-label col-sm-6">
                                                            Marital Status</label>
                                                        <div class="col-sm-6">
                                                            <asp:Label ID="lblmaritalstatus" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>


                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="col-md-12 text-center">
                                                        <div class="SliderLeft" style="">
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="">
                                                                    <asp:Image ID="userImg" Height="93px" Width="200px" runat="server" ImageUrl="~/img/administrator-icon.png" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <h5 class="sub-header"><b>Address Information</b></h5>
                                            <hr />
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group col-md-6">
                                                <label for="textfield" class="control-label col-sm-6" style="">
                                                    Address 1 <span style="color: #ff0000; font-size: 14px;"></span>
                                                </label>
                                                <div class="col-sm-6" style="" id="Div1">
                                                    <asp:Label ID="lbladdress1" runat="server" Text=""></asp:Label>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="textfield" class="control-label col-sm-6" style="">
                                                    Address 2</label>
                                                <div class="col-sm-6" style="" id="Div2">
                                                    <asp:Label ID="lbladdress2" runat="server" Text=""></asp:Label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group col-md-6">
                                                <label for="textfield" class="control-label col-sm-6" style="">
                                                    City<span style="color: #ff0000; font-size: 14px;"> </span>
                                                </label>
                                                <div class="col-sm-6" style="" id="Div11">
                                                    <asp:Label ID="lblcity" runat="server" Text=""></asp:Label>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="textfield" class="control-label col-sm-6" style="">
                                                    State</label>
                                                <div class="col-sm-6" style="" id="Div12">
                                                    <asp:Label ID="lblstate" runat="server" Text=""></asp:Label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group col-md-6">
                                                <label for="textfield" class="control-label col-sm-6" style="">
                                                    Postal Code<span style="color: #ff0000; font-size: 14px;"> </span>
                                                </label>
                                                <div class="col-sm-6" style="" id="Div13">
                                                    <asp:Label ID="lblpostalcode" runat="server" Text=""></asp:Label>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="textfield" class="control-label col-sm-6" style="">
                                                    Country</label>
                                                <div class="col-sm-6" style="" id="Div26">
                                                    <asp:Label ID="lblcountry" runat="server" Text=""></asp:Label>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="textfield" class="control-label col-sm-6" style="">
                                                    Address Proof<span style="color: #ff0000; font-size: 14px;"> </span>
                                                </label>
                                                <div class="col-sm-6" style="" id="Div7">
                                                    <asp:Label ID="lbladdresprof" runat="server" Text=""></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-12">
                                                    <h5 class="sub-header"><b>Official Infomation</b></h5>
                                                    <hr />
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-6" style="">
                                                            Date of Join</label>
                                                        <div class="col-sm-6" style="" id="Div58">
                                                            <asp:Label ID="lbldoj" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-6" style="">
                                                            Designation<span style="color: #ff0000; font-size: 14px;"></span>
                                                        </label>
                                                        <div class="col-sm-6" style="" id="Div65">
                                                            <asp:Label ID="lblusersubrole" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-6" style="">
                                                            Casual Leave<span style="color: #ff0000; font-size: 14px;"></span>
                                                        </label>
                                                        <div class="col-sm-6" style="" id="Div9">
                                                            <asp:Label ID="lblcasualleave" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-6" style="">
                                                            Earned Leave<span style="color: #ff0000; font-size: 14px;"></span>
                                                        </label>
                                                        <div class="col-sm-6" style="" id="Div14">
                                                            <asp:Label ID="lblearnedleave" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-6" style="">
                                                            Reporting<span style="color: #ff0000;"> </span>
                                                        </label>
                                                        <div class="col-sm-6" style="" id="Div66">
                                                            <asp:Label ID="lblreportingauthority" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="textfield" class="control-label col-sm-6" style="">
                                                            Department <span style="color: #ff0000; font-size: 14px;"></span>
                                                        </label>
                                                        <div class="col-sm-6" style="" id="Div5">
                                                            <asp:Label ID="lbluserrole" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 text-center">
                            <asp:Button ID="btncancel" runat="server" CssClass=" btn btn-primary" Text="Back" OnClick="btncancel_Click" />
                        </div>
                        </section>
                </div>
                </div>
        </section>
    </section>
</asp:Content>
