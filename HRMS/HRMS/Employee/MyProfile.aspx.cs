﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;

namespace HRMS.Employee
{
    public partial class MyProfile : System.Web.UI.Page
    {
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int id = Convert.ToInt32(Session["Emp_Code"]);
                Edit_Employee_Details(id);
                binddeductionsgrid();
                bindearningsgrid();
                hideidemp.Visible = false;
            }

        }
        public void bindearningsgrid()
        {
            int id = Convert.ToInt32(Session["Emp_Code"]);
            var earnings = (from item in ObjDAL.EarningPercentageAmounts
                            where item.EmpId == id
                            select item).ToList();


            if (earnings.Count > 0)
            {
                gvdearnings.DataSource = earnings;
                gvdearnings.DataBind();
            }
            else
            {
                gvdearnings.DataSource = earnings;
                gvdearnings.DataBind();


            }
        }


        public void binddeductionsgrid()
        {
            int id = Convert.ToInt32(Session["Emp_Code"]);

            var deductions = (from item in ObjDAL.DeductionPercentageAmounts
                              where item.EmpId == id
                              select item).ToList();


            if (deductions.Count > 0)
            {
                gvddeductions.DataSource = deductions;
                gvddeductions.DataBind();
            }
            else
            {
                gvddeductions.DataSource = deductions;
                gvddeductions.DataBind();


            }
        }

        protected void lkbdelete_Command(object sender, CommandEventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(lblempid.Text);
                String ID = Convert.ToString(lblempid.Text);

                var erd = (from s in ObjDAL.HRMSEmployeeMasterRegistrationDetails.Where(x => x.ID == id) select s).SingleOrDefault();


                var idendification = (from s in ObjDAL.HRMSEmplyeeMasterPersonalIdentificationDetails.Where(x => x.EmpId == Convert.ToString(id)) select s).SingleOrDefault();

                var reg = (from s in ObjDAL.HRMSEmplyeeMasterPersonalDetails.Where(x => x.EmpId == Convert.ToString(id)) select s).SingleOrDefault();

                var address = (from s in ObjDAL.HRMSEmployeeMasterAddressDetails.Where(x => x.EmpId == Convert.ToString(id)) select s).SingleOrDefault();
                ObjDAL.HRMSEmployeeMasterAddressDetails.DeleteOnSubmit(address);
                ObjDAL.SubmitChanges();


                var social = (from s in ObjDAL.HRMSEmployeeMasterSocialNetworkingDetails.Where(x => x.EmpId == Convert.ToString(id)) select s).SingleOrDefault();
                ObjDAL.HRMSEmployeeMasterSocialNetworkingDetails.DeleteOnSubmit(social);
                ObjDAL.SubmitChanges();


                var work = (from s in ObjDAL.HRMSEmployeeMasterWorkLocationDetails.Where(x => x.EmpId == Convert.ToString(id)) select s).SingleOrDefault();
                ObjDAL.HRMSEmployeeMasterWorkLocationDetails.DeleteOnSubmit(work);
                ObjDAL.SubmitChanges();


                var officialinfo = (from s in ObjDAL.HRMSEmployeeMasterOfficialInformationDetails.Where(x => x.EmpId == Convert.ToString(id)) select s).SingleOrDefault();
                ObjDAL.HRMSEmployeeMasterOfficialInformationDetails.DeleteOnSubmit(officialinfo);
                ObjDAL.SubmitChanges();


                var salary = (from s in ObjDAL.HRMSEmployeeMasterSalaryDetails.Where(x => x.EmpId == Convert.ToString(id)) select s).SingleOrDefault();
                ObjDAL.HRMSEmployeeMasterSalaryDetails.DeleteOnSubmit(salary);
                ObjDAL.SubmitChanges();

                var deduction = (from s in ObjDAL.HRMSEmployeeMasterDeductionsDetails.Where(x => x.EmpId == Convert.ToString(id)) select s).SingleOrDefault();
                ObjDAL.HRMSEmployeeMasterDeductionsDetails.DeleteOnSubmit(deduction);
                ObjDAL.SubmitChanges();

                ObjDAL.HRMSEmployeeMasterRegistrationDetails.DeleteOnSubmit(erd);
                ObjDAL.SubmitChanges();

                ObjDAL.HRMSEmplyeeMasterPersonalIdentificationDetails.DeleteOnSubmit(idendification);
                ObjDAL.SubmitChanges();

                ObjDAL.HRMSEmplyeeMasterPersonalDetails.DeleteOnSubmit(reg);
                ObjDAL.SubmitChanges();

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Successfully Removed Details');", true);
            }


            catch
            {
            }
        }
        protected void lkbedit_Click1(object sender, EventArgs e)
        {
            string AdditionCode = lblempid.Text;
            string path = "~/Employee/EmployeeDetailsMaster.aspx";
            Session["EmployeeDetailsCode"] = AdditionCode;
            Response.Redirect(path);
        }
        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Dashboard/Dashboard.aspx");
        }

        

        public void Edit_Employee_Details(int id)
        {

            try
            {
                int ID = Convert.ToInt32(id);
                List<HRMSEmployeeMasterRegistrationDetail> objEmpRegistration = (from master in ObjDAL.HRMSEmployeeMasterRegistrationDetails
                                                                                 where master.ID == ID
                                                                                 select master).ToList();

                if (objEmpRegistration.Count > 0)
                {
                    lblempid.Text = Convert.ToString(objEmpRegistration[0].ID);
                    lblusersubrole.Text = Convert.ToString(objEmpRegistration[0].RoleType);

                    List<HRMSDepartmentMaster> objrole = (from or in ObjDAL.HRMSDepartmentMasters
                                                          where or.DepartmentId == objEmpRegistration[0].UserRole
                                                          select or).ToList();

                    lbluserrole.Text = Convert.ToString(objrole[0].DepartmentName);


                    List<HRMSEmplyeeMasterPersonalDetail> objEmppersonaldetails = (from master in ObjDAL.HRMSEmplyeeMasterPersonalDetails
                                                                                   where master.EmpId ==Convert.ToInt32( ID).ToString()
                                                                                   select master).ToList();
                    lblfisrtname.Text = objEmppersonaldetails[0].FirstName;
                    lblmiddlename.Text = objEmppersonaldetails[0].MiddleName;
                    lbllastname.Text = objEmppersonaldetails[0].LastName;
                    lbldob.Text = Convert.ToDateTime(objEmppersonaldetails[0].DateOfBirth).ToString("yyyy/MM/dd");
                    lblmaritalstatus.Text = objEmppersonaldetails[0].MaritalStatus;
                    lblgender.Text = objEmppersonaldetails[0].Gender;
                    lblbloodgroup.Text = objEmppersonaldetails[0].BloodGroup;
                    lblnationality.Text = objEmppersonaldetails[0].Nationality;

                    List<HRMSEmplyeeMasterPersonalIdentificationDetail> objEmpidentification = (from master in ObjDAL.HRMSEmplyeeMasterPersonalIdentificationDetails
                                                                                                where master.EmpId == Convert.ToInt32(ID).ToString()
                                                                                                select master).ToList();


                    List<HRMSEmployeeLeave> objEmp = (from master in ObjDAL.HRMSEmployeeLeaves
                                                      where master.EmpId == ID
                                                      select master).ToList();
                    if (objEmp.Count > 0)
                    {
                        lblearnedleave.Text = Convert.ToString(objEmp[0].NoOfLeaves);
                        lblcasualleave.Text = Convert.ToString(objEmp[1].NoOfLeaves);
                    }
                    else {
                        lblearnedleave.Text ="NA" ;
                        lblcasualleave.Text = "NA";
                    }
                   

                    List<HRMSEmployeeMasterAddressDetail> objEmpaddress = (from master in ObjDAL.HRMSEmployeeMasterAddressDetails
                                                                           where master.EmpId == Convert.ToInt32(ID).ToString()
                                                                           select master).ToList();
                    lbladdress1.Text = objEmpaddress[0].PermanentAddress1;
                    lbladdress2.Text = objEmpaddress[0].PermanentAddress2;
                    lblcity.Text = objEmpaddress[0].PermanentCity;
                    lblstate.Text = objEmpaddress[0].PermanentState;
                    lblpostalcode.Text = Convert.ToInt32(objEmpaddress[0].PresentPostalCode).ToString();
                    lblcountry.Text = objEmpaddress[0].PermanentCountry;

                    lblmobile.Text = objEmpaddress[0].MobileNo;


                    List<HRMSEmployeeMasterSocialNetworkingDetail> objEmpnetworking = (from master in ObjDAL.HRMSEmployeeMasterSocialNetworkingDetails
                                                                                       where master.EmpId == Convert.ToInt32(ID).ToString()
                                                                                       select master).ToList();
                    lblemail.Text = objEmpnetworking[0].EmailId;


                    List<HRMSEmployeeMasterOfficialInformationDetail> objEmpofficialinfo = (from master in ObjDAL.HRMSEmployeeMasterOfficialInformationDetails
                                                                                            where master.EmpId == Convert.ToInt32(ID).ToString()
                                                                                            select master).ToList();
                    lbldoj.Text = Convert.ToDateTime(objEmpofficialinfo[0].DateOfJoining).ToString("yyyy/MM/dd");


                    var reporting = (from item in ObjDAL.USP_gettingemployeenameandid(Convert.ToInt32(objEmpofficialinfo[0].ReportingAuthority))
                                     select item).ToList();

                    lblreportingauthority.Text = reporting[0].empname;

                    List<HRMSEmployeeMasterSalaryDetail> objEmpsalaryinfo = (from master in ObjDAL.HRMSEmployeeMasterSalaryDetails
                                                                             where master.EmpId == Convert.ToInt32(ID).ToString()
                                                                             select master).ToList();

                    lblbasicamount.Text = Convert.ToString(objEmpsalaryinfo[0].BasicAmount);
                    lblgrossamount.Text = Convert.ToString(objEmpsalaryinfo[0].GrossAmount);

                    List<HRMSEmployeeMasterDeductionsDetail> objEmpdeduction = (from master in ObjDAL.HRMSEmployeeMasterDeductionsDetails
                                                                                where master.EmpId == Convert.ToInt32(ID).ToString()
                                                                                select master).ToList();
                    lblnetamount.Text = Convert.ToString(objEmpdeduction[0].NetAmount);


                    List<HRMSEmployeeMasterFacilitiesDetail> objEmpfacilities = (from master in ObjDAL.HRMSEmployeeMasterFacilitiesDetails
                                                                                 where master.EmpId == Convert.ToInt32(ID).ToString()
                                                                                 select master).ToList();

                 
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }
     
    }
}