﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="EmployeeAttendance.aspx.cs" Inherits="HRMS.Employee.EmployeeAttendance" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">

        function ValidationForDropDowns() {
            if (document.getElementById('<%=ddlempname.ClientID%>').options[document.getElementById('<%=ddlempname.ClientID%>').selectedIndex].value == "0") {
                alert("Please Select User Designation ");
                document.getElementById('<%=ddlempname.ClientID%>').focus();
                return false;
            }

            if (document.getElementById('<%=ddlmonth.ClientID%>').options[document.getElementById('<%=ddlmonth.ClientID%>').selectedIndex].value == "0") {
                alert("Please Select User Designation ");
                document.getElementById('<%=ddlmonth.ClientID%>').focus();
                return false;
            }

            if (document.getElementById('<%=ddlyear.ClientID%>').options[document.getElementById('<%=ddlyear.ClientID%>').selectedIndex].value == "0") {
                alert("Please Select Department ");
                document.getElementById('<%=ddlyear.ClientID%>').focus();
                return false;
            }

            return true;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        #ContentPlaceHolder1_actdate_container, #ContentPlaceHolder1_CalendarExtender2_popupDiv, #ContentPlaceHolder1_CalendarExtender3_popupDiv, #ContentPlaceHolder1_CalendarExtender1_container, #ContentPlaceHolder1_CalendarExtender2_container, #ContentPlaceHolder1_actdate_popupDiv, #ContentPlaceHolder1_CalendarExtender1_popupDiv {
            z-index: 10000;
        }

        .DateHide {
            display: none;
        }
    </style>

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="#">Attendance</a><i class="fa fa-angle-right"></i> </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="col-md-6 form-group">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label3" runat="server" Text="Label">Name</asp:Label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:DropDownList ID="ddlempname" runat="server" class="form-control"
                                         OnSelectedIndexChanged="ddlempname_SelectedIndexChanged"   AutoPostBack="true"   required >
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-6 form-group">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label1" runat="server" Text="Label">Month</asp:Label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:DropDownList ID="ddlmonth" runat="server" class="form-control"
                                            AutoPostBack="true">
                                            <asp:ListItem Text="Select Month" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="January" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="February" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="March" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="April" Value="4"></asp:ListItem>
                                            <asp:ListItem Text="May" Value="5"></asp:ListItem>
                                            <asp:ListItem Text="June" Value="6"></asp:ListItem>
                                            <asp:ListItem Text="July" Value="7"></asp:ListItem>
                                            <asp:ListItem Text="August" Value="8"></asp:ListItem>
                                            <asp:ListItem Text="september" Value="9"></asp:ListItem>
                                            <asp:ListItem Text="October" Value="10"></asp:ListItem>
                                            <asp:ListItem Text="November" Value="11"></asp:ListItem>
                                            <asp:ListItem Text="December" Value="12"></asp:ListItem>

                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label2" runat="server" Text="Label">Year</asp:Label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:DropDownList ID="ddlyear" runat="server" class="form-control"
                                         required   AutoPostBack="true">
                                            <asp:ListItem Text="Select Year" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="2017" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="2018" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="2019" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="2020" Value="4"></asp:ListItem>
                                            <asp:ListItem Text="2021" Value="5"></asp:ListItem>
                                            <asp:ListItem Text="2022" Value="6"></asp:ListItem>
                                            <asp:ListItem Text="2023" Value="7"></asp:ListItem>
                                            <asp:ListItem Text="2024" Value="8"></asp:ListItem>
                                            <asp:ListItem Text="2025" Value="9"></asp:ListItem>
                                            <asp:ListItem Text="2026" Value="10"></asp:ListItem>
                                            <asp:ListItem Text="2027" Value="11"></asp:ListItem>

                                        </asp:DropDownList>
                                    </div>
                                </div>
                                  <div class="col-md-12 text-center">
                                <asp:Button CssClass="btn btn-primary" ID="btnsearch" Text="Search" runat="server" OnClick="btnsearch_Click"/>
                                <asp:Button CssClass="btn btn-primary" ID="btnBacktoDashboard" Text="Back" runat="server" OnClick="btnBacktoDashboard_Click"/>

                                      </div>
                            </div>
                            <div class="col-md-12 ">
                                <div style="max-height: 515px; overflow: auto" class="table-responsive">
                                    <asp:GridView ID="gvdattendance" HeaderStyle-ForeColor="Black" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                        runat="server" Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%; overflow: scroll;" EmptyDataText="No records Found" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundField DataField="EmpId" HeaderText="Id" ItemStyle-Width="200" />
                                            <asp:BoundField DataField="Name" HeaderText="Employee Name" ItemStyle-Width="200" />
                                            <asp:BoundField DataField="Designation" HeaderText="Designation" ItemStyle-Width="200" />
                                            <asp:BoundField DataField="CreatedOn" HeaderText="Date" ItemStyle-Width="200" />
                                            <asp:BoundField DataField="Status" HeaderText="Status" ItemStyle-Width="200" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                             <div class="col-md-12 text-center">
                                <asp:Button CssClass="btn btn-primary" ID="btnback" Text="Back" runat="server" OnClick="btnback_Click" OnClientClick="return ValidationForDropDowns()"/>
                                      </div>
                        </div>
                    </section>
                </div>
            </div>

        </section>



    </section>
</asp:Content>
