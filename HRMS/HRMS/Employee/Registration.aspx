﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="HRMS.Employee.Registration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/Employee/Employee.css" rel="stylesheet" />

    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>

    <style type="text/css">
        .RadioButtonWidth label {
            margin-right: 30px;
        }
    </style>

    <style>
        .ajax__calendar .ajax__calendar_container {
            border: 1px solid #646464;
            background-color: #ffffff;
            color: #000000;
            z-index: 9999 !important;
            position: absolute !important;
        }

        .ajax__tab_default .ajax__tab_tab {
            text-align: left !important;
        }
    </style>
    <%--<script src="../Assist/Custom/RegistrationPage.js"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {

            $(".txtOnly").keypress(function (e) {
                var key = e.keyCode;
                if (key < 48 || (key > 57 && key < 65) || (key > 90 && key < 97) || key > 122 || (key >= 48 && key <= 57)) {
                    e.preventDefault();
                }
            });

            $(".txtno").keypress(function (e) {
                var key = e.keyCode;
                if (keyCode > 31 && (keyCode < 48 || keyCode > 57)) {
                    e.preventDefault();
                }
            });

            function username() {
                alert("please enter fields");
                return false;
            }
        });
    </script>
    <script type="text/javascript">
        function Validate() {

            var FirstName = document.getElementById('<%=txtFirstName.ClientID%>');
            var LastName = document.getElementById('<%=txtLastName.ClientID%>');
            var EmailId = document.getElementById('<%=txtemailid.ClientID%>');
            var PhoneNo = document.getElementById('<%=txtphoneno.ClientID%>');
            var DateOfBirth = document.getElementById('<%=txtDateOfBirth.ClientID%>');
            var Marital = document.getElementById('<%=ddlMaritalStatus.ClientID%>');

            //var txtDateOfBirth = document.getElementById('txtDateOfBirth');

            if (FirstName.value == '' || FirstName.value == null) {
                alert('please Enter First Name');
                return false;
            }

            if (LastName.value == '' || LastName.value == null) {
                alert('please Enter Last Name ');
                return false;
            }
            if (EmailId.value == '' || EmailId.value == null) {
                alert('please Enter EmailId ');
                return false;
            }
            if (PhoneNo.value == '' || PhoneNo.value == null) {
                alert('please Enter PhoneNo ');
                return false;
            }
            if (DateOfBirth.value == '' || DateOfBirth.value == null) {
                alert('please Enter DateOfBirth ');
                return false;
            }

            if (Marital.value == "") {
                //If the "Please Select" option is selected display error.
                alert("Please select an Marital Status!");
                return false;
            }
            return true;
        }
        function AddressValidate() {
            var Address1 = document.getElementById('<%=txtPAddress1.ClientID%>');
            var City = document.getElementById('<%=txtpcity.ClientID%>');
            var PostalCode = document.getElementById('<%=txtPPalost.ClientID%>');
            var Country = document.getElementById('<%=txtPAddress1.ClientID%>');
            var AddressProof = document.getElementById('<%=txtPAddress1.ClientID%>');


        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.js"></script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li><a href="../Dashboard/Dashboard.aspx"><i class="fa fa-home"></i>Dashboard</a> </li>
                        <li><a href="../Employee/EmployeeDetailsList.aspx" class="current">Employee Registration</a> </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">

                        <header class="panel-heading tab-bg-dark-navy-blue ">
                            <ul class="nav nav-tabs">
                                <li class="active" id="personalinfo" runat="server">
                                    <a data-toggle="tab" id="litab1" href="#personalTabContent" aria-expanded="false"><i class="fa fa-user">Personal Info</i></a>
                                </li>
                                <li class="" id="addressinfo" runat="server">
                                    <a data-toggle="tab" id="litab2" href="#addressTabContent" aria-expanded="false"><i class="fa fa-user">Address Info</i></a>
                                </li>
                                <li class="" id="officalinfo" runat="server">
                                    <a data-toggle="tab" href="#officalTabContent" aria-expanded="false"><i class="fa fa-user">Offical Info</i></a>
                                </li>
                                <li class="" id="salaryinfo" runat="server">
                                    <a data-toggle="tab" href="#salaryTabContent" aria-expanded="false"><i class="fa fa-user">Salary Info</i></a>
                                </li>
                            </ul>
                        </header>
                        <div class="panel-body">
                            <div class="tab-content">
                                <div id="personalTabContent" runat="server" class="tab-pane active">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5 style="color: rgb(15, 51, 224); font-size: 16px;"><b>Personal Infomation</b></h5>
                                            <hr />
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-4" style="">
                                                        First Name<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                    <div class="col-sm-8" style="" id="">
                                                        <asp:TextBox ID="txtFirstName" required runat="server" placeholder="First Name" class="form-control txtOnly"
                                                            MaxLength="50"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-4" style="">
                                                        Middle Name</label>
                                                    <div class="col-sm-8" style="" id="Div1">
                                                        <asp:TextBox ID="txtMiddleName" runat="server" placeholder="Middle Name" class="form-control txtOnly"
                                                            MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-4" style="">
                                                        Last Name<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                    <div class="col-sm-8" style="" id="Div2">
                                                        <asp:TextBox ID="txtLastName" runat="server" placeholder="Last Name" class="form-control txtOnly"
                                                            MaxLength="50"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-4" style="">
                                                        Email ID<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                    <div class="col-sm-8" style="" id="Div3">
                                                        <asp:TextBox ID="txtemailid" runat="server" placeholder=" Email ID" class="form-control"
                                                            MaxLength="50"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-4" style="">
                                                        Phone No<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                    <div class="col-sm-8" style="" id="Div4">
                                                        <asp:TextBox ID="txtphoneno" runat="server" placeholder=" Phone No" class="form-control"
                                                            MaxLength="50"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="textfield" class="control-label col-sm-4" style="">
                                                        Date of Birth<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtDateOfBirth" runat="server" placeholder="MM-dd-yyyy" class="form-control"
                                                            MaxLength="50"></asp:TextBox>
                                                        <ajaxToolKit:CalendarExtender ID="calendarDOB" PopupButtonID="imgPopup" runat="server"
                                                            TargetControlID="txtDateOfBirth" Format="dd-MM-yyyy" PopupPosition="BottomLeft">
                                                        </ajaxToolKit:CalendarExtender>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="textfield" class="control-label col-sm-4">
                                                        Gender<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                    <div class="col-sm-8">
                                                        <asp:RadioButtonList ID="rblGender" runat="server" CssClass="RadioButtonWidth" RepeatColumns="3" RepeatDirection="Horizontal">
                                                            <asp:ListItem Text="Female" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="Male" Value="2"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="textfield" class="control-label col-sm-4">
                                                        Blood Group</label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtBloodGroup" runat="server" required placeholder="Blood Group" class="form-control"
                                                            MaxLength="15"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="textfield" class="control-label col-sm-4">
                                                        Marital Status<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList ID="ddlMaritalStatus" runat="server" class="form-control">
                                                            <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                            <asp:ListItem Text="Single" Value="Single"></asp:ListItem>
                                                            <asp:ListItem Text="Married" Value="Married"></asp:ListItem>
                                                            <asp:ListItem Text="Widow" Value="Widow"></asp:ListItem>
                                                            <asp:ListItem Text="Divorced" Value="Divorced"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 text-right">
                                        <asp:Button ID="btnpersonal" runat="server" Text="Next" CssClass="btn btn-primary" OnClick="btnpersonal_Click2" OnClientClick="return Validate();" />
                                    </div>
                                </div>
                                <div id="addressTabContent" runat="server" class="tab-pane">
                                    <div class="col-md-12">
                                        <h5 style="color: rgb(15, 51, 224); font-size: 16px;"><b>Address Infomation</b></h5>
                                        <hr />
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                <ContentTemplate>
                                                    <div class="col-md-12">

                                                        <div class="clearfix"></div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-3" style="">
                                                                Address 1 <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                            <div class="col-sm-9" style="" id="Div28">
                                                                <asp:TextBox ID="txtPAddress1" runat="server" placeholder="Address 1" class="form-control"
                                                                    MaxLength="250" TextMode="SingleLine"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-3" style="">
                                                                Address 2</label>
                                                            <div class="col-sm-9" style="" id="Div29">
                                                                <asp:TextBox ID="txtPaddress2" runat="server" placeholder="Address 2" class="form-control"
                                                                    MaxLength="250" TextMode="SingleLine"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-3" style="">
                                                                City<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                            <div class="col-sm-9" style="" id="Div30">
                                                                <asp:TextBox ID="txtpcity" runat="server" placeholder="City" class="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-3" style="">
                                                                State</label>
                                                            <div class="col-sm-9" style="" id="Div31">
                                                                <asp:TextBox ID="txtPSate" runat="server" placeholder="State" class="form-control"
                                                                    MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-3" style="">
                                                                Postal Code<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                            <div class="col-sm-9" style="" id="Div32">
                                                                <asp:TextBox ID="txtPPalost" runat="server" placeholder="Postal Code" class="form-control"
                                                                    MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-3" style="">
                                                                Country<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                            <div class="col-sm-9" style="" id="Div33">
                                                                <asp:TextBox ID="txtpCountry" runat="server" placeholder="Country" class="form-control txtOnly"
                                                                    MaxLength="20" TextMode="SingleLine"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-5" style="">
                                                                Address Proof :<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                            <div class="col-sm-7" style="" id="Div5">
                                                                <span class="btn btn-default btn-file" id="Span1" runat="server" style="width: 100%"><span class="fileinput-new">Upload </span><span
                                                                    class="fileinput-exists"></span>
                                                                    <%--<input type="file" id="File4" runat="server" name="..." />--%>
                                                                    <asp:FileUpload ID="FileUpload1" runat="server" />
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <asp:Button ID="btntAddressPrevious1" runat="server" Text="Previous" CssClass="btn btn-primary" OnClick="btntAddressPrevious1_Click" OnClientClick="return AddressValidate();" />

                                        <%--    <asp:LinkButton ID="btnprevious1" Text="Previous" CssClass="btn btn-primary" runat="server" OnClick="btnprevious1_Click"></asp:LinkButton>--%>
                                        <asp:LinkButton ID="btnaddressInfoNext" Text="Next" CssClass="btn btn-primary" runat="server" OnClick="btnaddressInfoNext_Click"></asp:LinkButton>
                                    </div>
                                </div>
                                <div id="officalTabContent" runat="server" class="tab-pane">
                                    <div class="col-md-12">
                                        <h5 style="color: rgb(15, 51, 224); font-size: 16px;"><b>Offical Infomation</b></h5>
                                        <hr />
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <%--  <div class="col-md-12">
                                                <h5 style="color: rgb(15, 51, 224); font-size: 16px;"><b>Address Infomation</b></h5>
                                                <hr />
                                            </div>--%>

                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                <ContentTemplate>
                                                    <div class="col-md-12">

                                                        <div class="clearfix"></div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-4" style="">
                                                                Date of Join</label>
                                                            <div class="col-sm-8" style="" id="Div58">
                                                                <asp:TextBox ID="txtDateofJoin" runat="server" placeholder="Ex:MM-dd-yyyy" class="form-control"
                                                                    MaxLength="25" AutoPostBack="true"></asp:TextBox>
                                                                <ajaxToolKit:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="imgPopup"
                                                                    TargetControlID="txtDateofJoin" Format="dd-MM-yyyy" PopupPosition="BottomRight">
                                                                </ajaxToolKit:CalendarExtender>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 form-group">
                                                            <label for="textfield" class="control-label col-sm-4">
                                                                Designation <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                            <div class="col-sm-8">
                                                                <asp:DropDownList ID="ddlusersubrole" runat="server" class="form-control DropdownCss" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="form-group col-md-6">
                                                            <label for="textfield" class="control-label col-sm-4" style="">
                                                                Reporting Authority<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                            <div class="col-sm-8" style="" id="Div66">
                                                                <asp:DropDownList ID="ddlReportingAuthority" runat="server" class="form-control DropdownCss">
                                                                    <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                                    <asp:ListItem Text="HOFF" Value="1"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 form-group">
                                                            <label for="textfield" class="control-label col-sm-4">Department<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                            <div class="col-sm-8">
                                                                <asp:DropDownList ID="ddlUSerRole" runat="server" class="form-control DropdownCss">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <asp:LinkButton ID="btnPreviousOfficialInfo" Text="Previous" CssClass="btn btn-primary" runat="server" OnClick="btnPreviousOfficialInfo_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="btnNextOfficalInfo" Text="Next" CssClass="btn btn-primary" runat="server" OnClick="btnNextOfficalInfo_Click"></asp:LinkButton>


                                    </div>
                                </div>
                                <div id="salaryTabContent" runat="server" class="tab-pane">
                                    <div class="col-md-12">
                                        <h5 style="color: rgb(15, 51, 224); font-size: 16px;"><b>Salary Infomation</b></h5>
                                        <hr />
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="form-group col-md-6">
                                                <label for="textfield" class="control-label col-sm-4" style="">
                                                    Gross Amount<span style="color: #ff0000; font-size: 14px; display: none;">*</span></label>
                                                <div class="col-sm-8" style="" id="Div59">
                                                    <asp:TextBox ID="txtgrossamount" runat="server" AutoPostBack="true" placeholder="Gross Amount" class="form-control" MaxLength="50"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label for="textfield" class="control-label col-sm-4" style="">
                                                    Net Amount</label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="TextBox3" runat="server" ReadOnly="false" placeholder="Net Amount" class="form-control"
                                                        MaxLength="20"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <h5 style="color: rgb(15, 51, 224); font-size: 16px;"><b>Earnings And Deductions</b></h5>
                                                <hr />
                                            </div>
                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                <ContentTemplate>
                                                    <div class="col-md-6">

                                                        <div class="box-body table-responsive text-center ">
                                                            <div class=" form-group ">
                                                                <asp:GridView ID="gvdearnings" runat="server" AutoGenerateColumns="false">
                                                                    <FooterStyle BackColor="#ffffff" />
                                                                    <HeaderStyle CssClass="header" BackColor="#00677d" Font-Bold="True" ForeColor="#FFFFFF"></HeaderStyle>
                                                                    <PagerStyle CssClass="pager" BackColor="#ffffff" ForeColor="Black" HorizontalAlign="Right"></PagerStyle>
                                                                    <RowStyle CssClass="rows" BackColor="#ffffff"></RowStyle>
                                                                    <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True" />
                                                                    <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                                                    <SortedAscendingHeaderStyle BackColor="#848384" />
                                                                    <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                                                    <SortedDescendingHeaderStyle BackColor="#575357" />
                                                                    <AlternatingRowStyle BackColor="White" />
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="">
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chbboxdesignation" runat="server" AutoPostBack="true" CssClass="" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="Name" HeaderText="Name Of Earnings" ItemStyle-Width="150" />
                                                                        <asp:BoundField DataField="Value" HeaderText=" Percentage" ItemStyle-Width="150" />

                                                                        <asp:TemplateField HeaderText="Earnings Amount">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="txtelgibityamountlist" AutoPostBack="true" CssClass="form-control" runat="server">
                                                                                </asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <div class="row">
                                                <div class="col-md-6">

                                             <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                        <ContentTemplate>
                                                            <div class="col-md-12">
                                                                <div class="box-body table-responsive text-center ">
                                                                    <div class=" form-group ">
                                                                        <asp:GridView ID="gvddeductions" runat="server" AutoGenerateColumns="false">
                                                                            <FooterStyle BackColor="#ffffff" />
                                                                            <HeaderStyle CssClass="header" BackColor="#00677d" Font-Bold="True" ForeColor="#FFFFFF"></HeaderStyle>
                                                                            <PagerStyle CssClass="pager" BackColor="#ffffff" ForeColor="Black" HorizontalAlign="Right"></PagerStyle>
                                                                            <RowStyle CssClass="rows" BackColor="#ffffff"></RowStyle>
                                                                            <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True" />
                                                                            <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                                                            <SortedAscendingHeaderStyle BackColor="#848384" />
                                                                            <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                                                            <SortedDescendingHeaderStyle BackColor="#575357" />
                                                                            <AlternatingRowStyle BackColor="White" />
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="">
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="chbboxdesignation" runat="server" AutoPostBack="true" CssClass="" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="Name" HeaderText="Name Of Deductions" ItemStyle-Width="150" />
                                                                                <asp:BoundField DataField="Value" HeaderText=" Percentage" ItemStyle-Width="150" />

                                                                                <asp:TemplateField HeaderText="Deduction Amount">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="txtdeductions" AutoPostBack="true" CssClass="form-control" runat="server">
                                                                                        </asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>

                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="textfield" class="control-label col-sm-4" style="">
                                                        Basic Amount</label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtbasicamount" runat="server" ReadOnly="true" placeholder="Basic Amount" class="form-control"
                                                            MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <asp:LinkButton ID="btnPreviousSalaryInfo" Text="Previous" CssClass="btn btn-primary" runat="server" OnClick="btnPreviousSalaryInfo_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="btnSubmit" Text="Submit" CssClass="btn btn-primary" runat="server"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>





</asp:Content>
