﻿using DAL;
using BAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Employee
{
    public partial class AddEmployeGroup : System.Web.UI.Page
    {
        ERP_DatabaseDataContext objdb = new ERP_DatabaseDataContext();
        EmployeeGroupMasterBAL objEmpGroupMasterBAl = new EmployeeGroupMasterBAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_Type"].ToString() == "Employees")
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('You Don't have Permissions To Create..');", true);
                Response.Redirect("~/Dashboard/Dashboard.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    if (Session["ClientRegistrationNo"] == null)
                    {
                        Response.Redirect("~/Default.aspx");
                    }
                    //OTList();
                    if ((string)Session["GroupCode"] != null)
                    {

                        //  Request.QueryString.Clear();
                        string ClientRegistrationNo = Session["ClientRegistrationNo"].ToString();
                        string GroupCode = (string)Session["GroupCode"];
                        //string LoginID = Request.QueryString["id"];
                        Edit_GroupDetails(GroupCode, ClientRegistrationNo);
                    }
                    else
                    {
                        btnUpdate.Visible = false;
                    }
                }
            }
        }


        #region Control Events

        
        // checkbox list changed event
        protected void chlHeadsLists_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Create the list to store.
            List<String> StrList = new List<string>();
            // Loop through each item.
            foreach (ListItem item in chlHeadsLists.Items)
            {
                if (item.Selected)
                {
                    // If the item is selected, add the value to the list.
                    StrList.Add(item.Value);
                }
                else
                {
                    // Item is not selected, do something else.
                }
            }
            // Join the string together using the ; delimiter.
            String Str = String.Join(",", StrList.ToArray());
            ViewState["Applicable_Heads"] = Str;

        }

        //Saving Event
        protected void btnSave_Click(object sender, EventArgs e)
        {

            Saving();
        }

        //Clear Event
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearAllFields();
        }

        // Update button Click Event
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Update_Method();
        }

        // Group name text changed event
        protected void txtHeadName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtHeadName.Text != "")
                {
                    String Creation_Company = Session["ClientRegistrationNo"].ToString();
                    string groupname = txtHeadName.Text;
                    List<HRMS_Group_Master> ObjGroup = (from master in objdb.HRMS_Group_Masters
                                                        where master.Creation_Company == Creation_Company &&
                                                             master.Group_Name.ToLower().Trim() == groupname.ToLower().Trim()
                                                        select master).ToList();
                    if (ObjGroup.Count > 0)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Group Name Already Exist..');", true);
                        txtHeadName.Text = "";
                        txtHeadName.Focus();
                    }
                    else
                    {
                       
                        txtShortName.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }

        }

        // Short name text changed event
        protected void txtShortName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtShortName.Text != "")
                {
                    String Creation_Company = Session["ClientRegistrationNo"].ToString();
                    string shortname = txtShortName.Text;
                    List<HRMS_Group_Master> ObjGroup = (from master in objdb.HRMS_Group_Masters
                                                        where master.Creation_Company == Creation_Company &&
                                                             master.Short_Name.ToLower().Trim() == shortname.ToLower().Trim()
                                                        select master).ToList();
                    if (ObjGroup.Count > 0)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Short Name Already Exist..');", true);
                        txtShortName.Text = "";
                        txtShortName.Focus();
                    }
                    else
                    {
                        ddlGroup.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }

        }
        #endregion

        #region Methods
        // binding checkboxes in checkbox list based on ShortNames In Additions Master table
        //public void OTList()
        //{
        //    try
        //    {
        //        string Creation_Company = Session["ClientRegistrationNo"].ToString();
        //        //string Company_Name = Session["Creation_Company"].ToString();

        //        List<HRMS_Additions_Master> objShortInfoDetails = (from Short in objdb.HRMS_Additions_Masters
        //                                                           where Short.Creation_Company == Creation_Company
        //                                                           select Short).ToList();

        //        if (objShortInfoDetails.Count > 0)
        //        {

        //            chlHeadsLists.DataSource = objShortInfoDetails;

        //            chlHeadsLists.DataTextField = "Short_Name";
        //            chlHeadsLists.DataValueField = "Short_Name";
        //            chlHeadsLists.DataBind();

        //        }
        //        else
        //        {
        //            chlHeadsLists.Items.Insert(0, new ListItem(Creation_Company, Creation_Company)); //updated code
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        string Error = ex.ToString();
        //        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + Error + "');", true);
        //    }
        //    finally
        //    {
        //    }
        //}

        //Saving data in Group Master
        public void Saving()
        {
            try
            {
                string heads = (string)ViewState["Applicable_Heads"];
                objEmpGroupMasterBAl.Group_Code = "";
                objEmpGroupMasterBAl.Group_Name = txtHeadName.Text;
                objEmpGroupMasterBAl.Short_Name = txtShortName.Text;
                objEmpGroupMasterBAl.Group_type = ddlGroup.SelectedValue;
                objEmpGroupMasterBAl.Applicable_Heads = heads;
                objEmpGroupMasterBAl.Created_By = Session["User_Name"].ToString();
                objEmpGroupMasterBAl.Created_Date = System.DateTime.Now;
                objEmpGroupMasterBAl.Creation_Company = Session["ClientRegistrationNo"].ToString();
                objEmpGroupMasterBAl.Modified_By = Session["User_Name"].ToString();
                objEmpGroupMasterBAl.Modified_Date = System.DateTime.Now;
                objEmpGroupMasterBAl.Parameter = 1;
                if (objEmpGroupMasterBAl.Insert_Group_Master() != 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Data Saved Successfully');", true);
                    ClearAllFields();
                }
                else
                { }
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }
            finally
            { }
        }

        //Clear all controls in Employee Group
        private void ClearAllFields()
        {
            txtID.Text = "";
            txtHeadName.Text = "";
            txtShortName.Text = "";
            for (int items = 0; items < chlHeadsLists.Items.Count; items++)
            {
                chlHeadsLists.ClearSelection();
            }
            btnSave.Visible = true;
            btnUpdate.Visible = false;
        }

        // Editing Group Details
        public void Edit_GroupDetails(string groupcode, string CreationCompany)
        {
            try
            {

                var objGroupDetails = objEmpGroupMasterBAl.Get_Group_Details(groupcode, CreationCompany);
                if (objGroupDetails.Count > 0)
                {
                    txtID.Text = objGroupDetails[0].Group_Code.ToString();
                    txtHeadName.Text = objGroupDetails[0].Group_Name.ToString();
                    txtShortName.Text = objGroupDetails[0].Short_Name.ToString();
                    ddlGroup.SelectedValue = objGroupDetails[0].Group_type.ToString();
                    string shortnames = objGroupDetails[0].Applicable_Heads.ToString();
                    if (shortnames.Length > 1)
                    {
                        string[] items = shortnames.Split(',');
                        for (int i = 0; i < chlHeadsLists.Items.Count; i++)
                        {
                            for (int x = 0; x < items.Length; x++)
                            {

                                if (chlHeadsLists.Items[i].Text == items[x])
                                {
                                    chlHeadsLists.Items[i].Selected = true;
                                }

                            }
                        }
                    }

                    btnSave.Visible = false;
                    btnUpdate.Visible = true;
                    Session["GroupCode"] = null;


                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('No Data Found ');", true);
                }
            }
            catch (Exception ex)
            {
                string Error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(" + Error + ");", true);
            }
            finally
            {

            }
        }

        //Updating Method
        public void Update_Method()
        {


            try
            {
                List<String> StrList = new List<string>();
                // Loop through each item.
                foreach (ListItem item in chlHeadsLists.Items)
                {
                    if (item.Selected)
                    {
                        // If the item is selected, add the value to the list.
                        StrList.Add(item.Value);
                    }
                    else
                    {
                        // Item is not selected, do something else.
                    }
                }
                // Join the string together using the ; delimiter.
                String Str = String.Join(",", StrList.ToArray());

                objEmpGroupMasterBAl.Group_Code = txtID.Text;
                objEmpGroupMasterBAl.Group_Name = txtHeadName.Text;
                objEmpGroupMasterBAl.Short_Name = txtShortName.Text;
                objEmpGroupMasterBAl.Group_type = ddlGroup.SelectedValue;

                objEmpGroupMasterBAl.Applicable_Heads = Str;
                


                objEmpGroupMasterBAl.Creation_Company = Session["ClientRegistrationNo"].ToString();
                objEmpGroupMasterBAl.Created_By = Session["User_Name"].ToString();
                objEmpGroupMasterBAl.Created_Date = System.DateTime.Now;
                objEmpGroupMasterBAl.Modified_By = Session["User_Name"].ToString();
                objEmpGroupMasterBAl.Modified_Date = System.DateTime.Now;

                objEmpGroupMasterBAl.Parameter = 2;


                if (objEmpGroupMasterBAl.Insert_Group_Master() != 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Group Data Saved Successfully');", true);
                    ClearAllFields();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('User Registration Data Not Saved');", true);
                }


            }
            catch (Exception ex)
            {

                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Not Saved ');", true);
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.Message.ToString() + "');", true);
            }
            finally
            {

            }

        }
        #endregion
    }
}