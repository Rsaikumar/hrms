﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BAL;
using DAL;
using System.Globalization;

namespace HRMS.Employee
{
    public partial class SuspensionandPunishment : System.Web.UI.Page
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Id"] != null)
                {
                    int code  = Convert.ToInt32(Session["Id"]);
                    var employeename = (from item in objDAL.USP_gettingemployeenameandid(code)
                                        select item).ToList();
                    if (employeename.Count > 0)
                    {
                        txtempname.Text = employeename[0].empname;
                    }
                }
              
            }
            Bindingdetails();

        }

        public void Bindingdetails()
        {
            try
            {
                int code = Convert.ToInt32(Session["Id"]);
                List<HRMSEmployeeMasterRegistrationDetail> objEmpRegistration = (from master in objDAL.HRMSEmployeeMasterRegistrationDetails
                                                                                 where master.ID == code
                                                                                 select master).ToList();
                if (objEmpRegistration.Count > 0)
                {
                    List<HRMSRoleMaster> objsubrole = (from or in objDAL.HRMSRoleMasters
                                                              where or.RoleId == objEmpRegistration[0].UserSubRole
                                                              select or).ToList();

                    txtdesignation.Text = Convert.ToString(objsubrole[0].RoleName);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {

            try
            {
                int code = Convert.ToInt32(Session["Id"]);
                HRMSEmployeePunishmentandSuspension EPS = new HRMSEmployeePunishmentandSuspension();
                EPS.EmpId = code;
                EPS.Designation = txtdesignation.Text;
                EPS.Type = ddltype.SelectedItem.Text;
                EPS.Cause = txtcause.Text;
                EPS.Section = txtsection.Text;
                EPS.Duration = Convert.ToInt32(txtduration.Text);
                EPS.RejoiningDate = DateTime.ParseExact(txtrejoindate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                EPS.CreatedBy = Convert.ToString(Session["Emp_Code"]);
                EPS.CreatedOn = DateTime.Now;
                objDAL.HRMSEmployeePunishmentandSuspensions.InsertOnSubmit(EPS);
                objDAL.SubmitChanges();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Created Successfully');window.location ='../Employee/EmployeeDetailsView.aspx';", true);
            }
            catch (Exception ex)
            {
                var error = ex.Message.ToString();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Insert All Details');", true);
            }

        }

        public void RejoiningDate()
        {
            var TodayDate = Convert.ToDateTime(DateTime.Now);
            int NoofDays = Convert.ToInt32(txtduration.Text);
            TodayDate = TodayDate.AddDays(NoofDays);
            this.txtrejoindate.Text = Convert.ToDateTime(TodayDate.ToShortDateString()).ToString("dd-MM-yyyy");

           


        }

        protected void txtduration_TextChanged(object sender, EventArgs e)
        {
            RejoiningDate();
        }

        protected void lnkbackbtn_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Employee/EmployeeDetailsView.aspx");

        }
    }
}
