﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true"
     CodeBehind="HeaderList.aspx.cs" Inherits="HRMS.Employee.HeaderList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" lang="javascript">
    function Search() {
        if (document.getElementById('<%=txtSearch.ClientID%>').value == "") {
            alert("Search Box Can't be Empty ... ");
            document.getElementById('<%=txtSearch.ClientID%>').focus();
            return false;
        }
        return true;
    }
   
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
 
<div id="main">
    <div class="container-fluid">
        <div class="page-header">
            <div class="pull-left Search_Div" id="btnSearch_Div">
                <div class="Search_Icon">
                    <asp:TextBox ID="txtSearch" runat="server" placeholder="Search For Head List.."
                        class="form-control txtSearch"></asp:TextBox>
                </div>
            </div>
            <div class="pull-left">
               <asp:Button ID="btnSearch"   runat="server" class="search_Btn" OnClientClick="return Search()" OnClick="btnSearch_Click" />
            </div>
          
            <div class="pull-right">
                <ul class="stats">
                    <li>
                        <asp:Button ID="btnAdd" runat="server" class="add_Btn" OnClick="btnAdd_Click" ToolTip="Create Header Details" /></li>
                    <li>
                        <asp:Button ID="btnPdf" runat="server" class="PDF_Btn" ToolTip="Export Header Details To Pdf" /></li>
                    <li>
                        <asp:Button ID="btnExcel" runat="server" class="Excel_Btn" ToolTip="Export Header Details To Excel" /></li>
                </ul>
            </div>
        </div>
        <div class="breadcrumbs">
            <ul>
                <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i>
                </li>
                <li>
                        <a href="">Payroll</a><i class="fa fa-angle-right"></i>
                    </li>
                <li><a href="HeaderList.aspx">Header List</a> <i class="fa fa-angle-right"></i></li>
                <li>
                <asp:LinkButton Text="Add Header" id="lkbAddHeader" runat="server" OnClick="lkbAddHeader_Click"   ></asp:LinkButton>
                <%--<a href="AddUserRegistration.aspx">User Registration</a>--%> </li>
            </ul>
            <div class="close-bread">
                <a href="#"><i class="fa fa-times"></i></a>
            </div>
        </div>
         <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                    <div class="box-title">
                        <h3>
                            <i class="fa fa-th-list"></i>Header List</h3>
                    </div>
                    <div class="box-content">
                        <form action="#" class='form-horizontal form-bordered'>
                        <div class="col-sm-12" style="margin-bottom: 0.3%;">
                            <div class="row">
                                <asp:GridView ID="grdHeaderList" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                    Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%;" runat="server"
                                    AutoGenerateColumns="false" DataKeyNames="Addition_Code" EmptyDataText="No Records Found"
                                    ShowFooter="false" AllowPaging="true" 
                                     PageSize="15" AllowSorting="true" 
                                    onrowcommand="grdHeaderList_RowCommand" 
                                    onsorting="grdHeaderList_Sorting" 
                                    onpageindexchanging="grdHeaderList_PageIndexChanging" 
                                    onrowdeleting="grdHeaderList_RowDeleting" 
                                    onrowediting="grdHeaderList_RowEditing" >
                                    <AlternatingRowStyle BackColor="#EEEEEE" />
                                    <PagerStyle BackColor="#EEEEEE" ForeColor="Black" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#000065" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="S.No" SortExpression="">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Addition Code" >
                                            <ItemTemplate>
                                                <asp:Label ID="lblAdditionCode" runat="server" Text='<%#Eval("Addition_Code") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Head Name" >
                                            <ItemTemplate>
                                                <asp:Label ID="lblHeadName" runat="server" Text='<%#Eval("Head_Name") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Short Name" >
                                            <ItemTemplate>
                                                <asp:Label ID="lblShortName" runat="server" Text='<%#Eval("Short_Name") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Calculation" >
                                            <ItemTemplate>
                                                <asp:Label ID="lblCalculation" runat="server" Text='<%#Eval("Caluculation") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="% Basic" >
                                            <ItemTemplate>
                                                <asp:Label ID="lblPerOfBasic" runat="server" Text='<%#Eval("Per_Of_Basic") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                       
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbkEdit" runat="server" CommandName="Edit" Style="color: gray;
                                                    text-decoration: none; float: left; background-image: url('../img/file_edit.png');
                                                    height: 30px; width: 35px; background-position: left; background-repeat: no-repeat;"
                                                    OnClientClick="return confirm('Are you sure you want to Edit this Record ...?')" CausesValidation="false"
                                                    CommandArgument='<%#Eval("Addition_Code") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                       <%-- <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete" Style="color: gray;
                                                    text-decoration: none; float: left; background-image: url('../img/delete.png');
                                                    height: 30px; width: 35px; background-position: left; background-repeat: no-repeat;"
                                                    OnClientClick="return confirm('Are you sure you want to Remove this Record ...?')" CausesValidation="false"
                                                    CommandArgument='<%#Eval("Addition_Code") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                   
                                    </Columns>
                                </asp:GridView>
                            </div>
                    
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
     </ContentTemplate>
            </asp:UpdatePanel>
</asp:Content>
