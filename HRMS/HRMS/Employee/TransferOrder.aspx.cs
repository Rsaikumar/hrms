﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using System.Globalization;

namespace HRMS.Employee
{
    public partial class TransferOrder : System.Web.UI.Page
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindingemployeename();
                loctionblock();

            }
          
        }


        #region Methods
        public void bindingemployeename()
        {
            try
            {
                int roleid = Convert.ToInt32(Session["role"].ToString());
                int empid = Convert.ToInt32(Session["Emp_Code"]);

                var bindname = (from item in objDAL.usp_GetEmployeeNameAccordingRolesInTransferOrder(roleid, empid)

                                select item).ToList();

                if (bindname.Count() > 0)
                {

                    ddlemployeename.DataSource = bindname;
                    ddlemployeename.DataTextField = "empname";
                    ddlemployeename.DataValueField = "EmpId";
                    ddlemployeename.DataBind();
                    ddlemployeename.Items.Insert(0, new ListItem("---Select---", "0"));

                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        # region new

        public void Bind_Circle()
        {
            try
            {
                var objHeaderInfo = (from header in objDAL.Circles
                                     orderby header.Circlename ascending
                                     select new { circlename = header.Circlename, circlecd = header.Circlecd }).Distinct().ToList();
                if (objHeaderInfo.Count > 0)
                {
                    ddlnewlocationcircle.DataSource = objHeaderInfo;
                    ddlnewlocationcircle.DataTextField = "circlename";
                    ddlnewlocationcircle.DataValueField = "circlecd";
                    ddlnewlocationcircle.DataBind();
                    ddlnewlocationcircle.Items.Insert(0, new ListItem("---Select---", "0"));
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }

        }

        public void Bind_divisions()
        {
            try
            {
                var objHeaderInfo = (from header in objDAL.Divisions
                                     where header.Circlecd == ddlnewlocationcircle.SelectedValue
                                     orderby header.Divisionname ascending
                                     select new { divisionname = header.Divisionname, divisioncd = header.Divisioncd }).Distinct().ToList();
                if (objHeaderInfo.Count > 0)
                {

                    ddlnewdivisions.DataSource = objHeaderInfo;

                    ddlnewdivisions.DataTextField = "divisionname";
                    ddlnewdivisions.DataValueField = "divisioncd";
                    ddlnewdivisions.DataBind();
                    ddlnewdivisions.Items.Insert(0, new ListItem("---Select---", "0"));
                }
                else
                {

                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }

        }
        public void Bind_range()
        {
            try
            {
                var objHeaderInfo = (from header in objDAL.Ranges
                                     where header.Divisioncd == ddlnewdivisions.SelectedValue
                                     orderby header.Rangename ascending
                                     select new { Rangename = header.Rangename, Rangecd = header.Rangecd }).Distinct().ToList();
                if (objHeaderInfo.Count > 0)
                {
                    ddlnewrange.DataSource = objHeaderInfo;
                    ddlnewrange.DataTextField = "Rangename";
                    ddlnewrange.DataValueField = "Rangecd";
                    ddlnewrange.DataBind();
                    ddlnewrange.Items.Insert(0, new ListItem("---Select---", "0"));
                }
                else
                {

                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }

        }
        public void Bind_section()
        {
            try
            {
                var objHeaderInfo = (from header in objDAL.Sections
                                     where header.Rangecd == ddlnewrange.SelectedValue
                                     orderby header.Sectionname ascending
                                     select new { Sectionname = header.Sectionname, Sectioncd = header.Sectioncd }).Distinct().ToList();
                if (objHeaderInfo.Count > 0)
                {
                    ddlnewsection.DataSource = objHeaderInfo;
                    ddlnewsection.DataTextField = "Sectionname";
                    ddlnewsection.DataValueField = "Sectioncd";
                    ddlnewsection.DataBind();
                    ddlnewsection.Items.Insert(0, new ListItem("---Select---", "0"));
                }
                else
                {

                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }

        }
        public void Bind_beat()
        {
            try
            {


                var objHeaderInfo = (from header in objDAL.Beats
                                     where header.Sectioncd == ddlnewsection.SelectedValue
                                     orderby header.Beatname ascending
                                     select new { Beatname = header.Beatname, Beatcd = header.Beatcd }).Distinct().ToList();

                if (objHeaderInfo.Count > 0)
                {

                    ddlnewbeat.DataSource = objHeaderInfo;

                    ddlnewbeat.DataTextField = "Beatname";
                    ddlnewbeat.DataValueField = "Beatcd";
                    ddlnewbeat.DataBind();
                    ddlnewbeat.Items.Insert(0, new ListItem("---Select---", "0"));
                }
                else
                {

                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }

        }

#endregion

        public void loctionblock()
        {
            Bind_Circle();
            var objHeaderInfo = (from header in objDAL.HRMSEmployeeMasterWorkLocationDetails
                                 where header.ID ==Convert.ToInt32( Session["Emp_Code"].ToString())
                                 select header).ToList();

           // ddlnewlocationcircle.SelectedValue = objHeaderInfo[0].Circle.Trim();
            Bind_divisions();
          //  ddlnewdivisions.SelectedValue = objHeaderInfo[0].Division.Trim();
            Bind_range();
           // ddlnewrange.SelectedValue = objHeaderInfo[0].Range.Trim();
            Bind_section();
         //   ddlnewsection.SelectedValue = objHeaderInfo[0].Section.Trim();
            Bind_beat();
         //   ddlnewbeat.SelectedValue = objHeaderInfo[0].Beat.Trim();

            int role = Convert.ToInt32(Session["role"].ToString());
            if (role <= 4)
            {
                ddlnewlocationcircle.Enabled = true;
                ddlnewdivisions.Enabled = true;
                ddlnewrange.Enabled = true;
                ddlnewsection.Enabled = true;
                ddlnewbeat.Enabled = true;
            }
            else if (role == 5)
            {
                ddlnewlocationcircle.Enabled = false;
                ddlnewdivisions.Enabled = true;
                ddlnewrange.Enabled = true;
                ddlnewsection.Enabled = true;
                ddlnewbeat.Enabled = true;
            }
            else if (role == 6)
            {
                ddlnewlocationcircle.Enabled = false;
                ddlnewdivisions.Enabled = false;
                ddlnewrange.Enabled = true;
                ddlnewsection.Enabled = true;
                ddlnewbeat.Enabled = true;
            }
            else if (role == 7)
            {
                ddlnewlocationcircle.Enabled = false;
                ddlnewdivisions.Enabled = false;
                ddlnewrange.Enabled = true;
                ddlnewdivisions.Enabled = true;
                ddlnewbeat.Enabled = true;
            }
            else if (role == 8)
            {
                ddlnewlocationcircle.Enabled = false;
                ddlnewdivisions.Enabled = false;
                ddlnewrange.Enabled = false;
                ddlnewsection.Enabled = true;
                ddlnewbeat.Enabled = true;
            }
            else if (role == 9)
            {
                ddlnewlocationcircle.Enabled = false;
                ddlnewdivisions.Enabled = false;
                ddlnewrange.Enabled = false;
                ddlnewsection.Enabled = false;
                ddlnewbeat.Enabled = true;
            }

        }
        # endregion

        #region Events
        protected void ddlemployeename_SelectedIndexChanged1(object sender, EventArgs e)
        {
            try
            {

                int ID = Convert.ToInt32(Session["Emp_Code"]);
                List<HRMSEmployeeMasterRegistrationDetail> objdesignation = (from master in objDAL.HRMSEmployeeMasterRegistrationDetails
                                                                             where master.ID == Convert.ToInt32(ddlemployeename.SelectedValue)
                                                                             select master).ToList();


                if (objdesignation.Count > 0)
                {


                    List<HRMSRoleMaster> objsubrole = (from or in objDAL.HRMSRoleMasters
                                                              where or.RoleId == objdesignation[0].UserSubRole
                                                              select or).ToList();

                    txtdesignation.Text = Convert.ToString(objsubrole[0].RoleName);


                    var location = (from item in objDAL.HRMSEmployeeMasterWorkLocationDetails
                                    where item.ID == Convert.ToInt32(ddlemployeename.SelectedValue)
                                    select item).SingleOrDefault();


                    List<Beat> beatdetails = (from master in objDAL.Beats
                                              where master.Beatcd == location.Beat.Trim()
                                              select master).ToList();

                    txtbeat.Text = beatdetails[0].Beatname;

                    List<Section> sectiondetails = (from master in objDAL.Sections
                                                    where master.Sectioncd == location.Section.Trim()
                                                    select master).ToList();

                    txtsection.Text = sectiondetails[0].Sectionname;

                    List<Range> rangedetails = (from master in objDAL.Ranges
                                                where master.Rangecd == location.Range.Trim()
                                                select master).ToList();

                    txtrange.Text = rangedetails[0].Rangename;


                    List<Division> divisiondetails = (from master in objDAL.Divisions
                                                      where master.Divisioncd == location.Division.Trim()
                                                      select master).ToList();

                    txtdivisions.Text = divisiondetails[0].Divisionname;

                    List<Circle> circledetails = (from master in objDAL.Circles
                                                  where master.Circlecd == location.Circle.Trim()
                                                  select master).ToList();

                    txtcircle.Text = circledetails[0].Circlename;

                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void ddlnewlocationcircle_SelectedIndexChanged(object sender, EventArgs e)
        {
            Bind_divisions();
        }

        protected void ddlnewdivisions_SelectedIndexChanged(object sender, EventArgs e)
        {
             Bind_range();
        }

        protected void ddlnewrange_SelectedIndexChanged(object sender, EventArgs e)
        {
              Bind_section();
        }

        protected void ddlnewsection_SelectedIndexChanged(object sender, EventArgs e)
        {
            Bind_beat();
        }

        #endregion
        #region saving
        //saving

        protected void btnsave_Click1(object sender, EventArgs e)
        {
            try
            {
                HRMSEmployeeTransferHistory transferhistory = new HRMSEmployeeTransferHistory();
                transferhistory.EmpID = Convert.ToInt32(ddlemployeename.SelectedValue);
                transferhistory.TransferOrderDate = DateTime.ParseExact(txttransferorderdate.Text, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString();
                transferhistory.Circle = txtcircle.Text;
                transferhistory.Divisions = txtdivisions.Text;
                transferhistory.Section = txtsection.Text;
                transferhistory.Range = txtrange.Text;
                transferhistory.Beat = txtbeat.Text;
                transferhistory.CreatedBy = Convert.ToString(Session["Emp_Code"]);
                transferhistory.CreatedOn = DateTime.Now;
                transferhistory.ModifiedBy = "";
                objDAL.HRMSEmployeeTransferHistories.InsertOnSubmit(transferhistory);
            


                HRMSEmployeeTransferOrder transferorder = new HRMSEmployeeTransferOrder();

                transferorder.EmpID = Convert.ToInt32(ddlemployeename.SelectedValue);
                transferorder.TransferOrderDate = DateTime.ParseExact(txttransferorderdate.Text, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                transferorder.Designation = txtdesignation.Text;
                transferorder.OldLocationCircle = txtcircle.Text;
                transferorder.OldLocationDivision = txtdivisions.Text;
                transferorder.OldLocationRange = txtrange.Text;
                transferorder.OldLocationSection = txtsection.Text;
                transferorder.OldLocationBeat = txtbeat.Text;
                transferorder.NewLocationCircle = ddlnewlocationcircle.SelectedValue;
                transferorder.NewLocationDivision = ddlnewdivisions.SelectedValue;
                transferorder.NewLocationRange = ddlnewrange.SelectedValue;
                transferorder.NewLocationSection = ddlnewsection.SelectedValue;
                transferorder.NewLocationBeat = ddlnewbeat.SelectedValue;
                transferorder.CreatedBy = Convert.ToString(Session["Emp_Code"]);
                transferorder.CreatedOn = DateTime.Now;
                transferorder.ModifiedBy = "";
                objDAL.HRMSEmployeeTransferOrders.InsertOnSubmit(transferorder);
                objDAL.SubmitChanges();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('sucessfully Saved the Transfer Details');window.location ='../Dashboard/Dashboard.aspx';", true);

            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
        #endregion