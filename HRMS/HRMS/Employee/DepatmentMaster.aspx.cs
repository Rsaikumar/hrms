﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Employee
{
    public partial class DepatmentMaster : System.Web.UI.Page
    {
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            incrementcode();

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                HRMSDepartmentMaster role = new HRMSDepartmentMaster();
                role.DepartmentId = Convert.ToInt32(txtdepartid.Text);
                role.DepartmentName = txtdepartmentname.Text;
                role.CreatedBy = "";
                role.CreatedOn = DateTime.Now;
                ObjDAL.HRMSDepartmentMasters.InsertOnSubmit(role);
                ObjDAL.SubmitChanges();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Saved');", true);
                cleartext();
                incrementcode();
            }
            catch (Exception)
            {
            }
        }
        protected void cleartext()
        {
            txtdepartmentname.Text = string.Empty;
        }

        protected void incrementcode()
        {
            var objHeaderInfo = (from header in ObjDAL.HRMSDepartmentMasters
                                 select header).ToList();
            int subroleid = Convert.ToInt32(objHeaderInfo.Count) + 1;
            txtdepartid.Text = Convert.ToString(subroleid);
        }
    }
}