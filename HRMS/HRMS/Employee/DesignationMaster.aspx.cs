﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Employee
{
    public partial class DesignationMaster : System.Web.UI.Page
    {
        ERP_DatabaseDataContext ObjDAL = new ERP_DatabaseDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            incrementcode();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                HRMSRoleMaster role = new HRMSRoleMaster();
                role.RoleId = Convert.ToInt32(txtdesignationid.Text);
                role.RoleName = txtdesignation.Text;
                role.CreatedBy = "";
                role.CreatedOn = DateTime.Now;
                ObjDAL.HRMSRoleMasters.InsertOnSubmit(role);
                ObjDAL.SubmitChanges();
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Saved');", true);
                cleartext();
                incrementcode();
            }
            catch (Exception)
            {
            }
        }
        protected void cleartext()
        {
            txtdesignation.Text = string.Empty;
        }

        protected void incrementcode()
        {
            var objHeaderInfo = (from header in ObjDAL.HRMSRoleMasters
                                 select header).ToList();
            int subroleid = Convert.ToInt32(objHeaderInfo.Count) + 1;
            txtdesignationid.Text = Convert.ToString(subroleid);
        }
    }
}