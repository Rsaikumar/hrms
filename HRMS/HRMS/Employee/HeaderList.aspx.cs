﻿using BAL;
using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMS.Employee
{
    public partial class HeaderList : System.Web.UI.Page
    {
        // // // Object Creation For Database
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

        // // /// Object Creation For Item Group Domain
        AdditionsMasterBAL objSCM_HeaderBAL = new AdditionsMasterBAL();
        protected void Page_Load(object sender, EventArgs e)
         {
            if (!IsPostBack)
            {
                if (Session["User_Type"].ToString() == "User")
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('You Don't have Permissions To Create..');", true);
                    Response.Redirect("~/Dashboard/Dashboard.aspx");
                }
                {
                    if (!IsPostBack)
                    {
                        if (Session["ClientRegistrationNo"] == null)
                        {
                            Response.Redirect("~/Default.aspx");
                        }
                        txtSearch.Focus();

                        //Bind User Registration
                        //Bind_Grid();
                    }
                }
            }

        }


        #region Button Events

        // // // Navigation to User Registration Form For Created New User 
        protected void btnAdd_Click(object sender, EventArgs e)
        {

            Response.Redirect("~/Employee/AddAddiction_Heads.aspx");

        }

        /// <summary>
        /// Search For UserRegistration Details
        /// </summar
        /// 

        //protected void btnSearch_Click(object sender, EventArgs e)
        //{
        //    try
        //    {

        //        string StrSearch = txtSearch.Text.Trim().ToLower();
        //        string CompanyName = Session["ClientRegistrationNo"].ToString().Trim().ToLower();
        //        List<HRMS_Additions_Master> objHeaderSearch = objSCM_HeaderBAL.GetData_search_HeaderInfo(StrSearch, CompanyName);

        //        if (objHeaderSearch.Count > 0)
        //        {
        //            grdHeaderList.DataSource = objHeaderSearch;
        //            grdHeaderList.DataBind();
        //        }
        //        else
        //        {
        //            SetInitialRow();
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.ToString() + "');", true);
        //    }
        //}

        /// <summary>
        /// Navigation for User Header create
        /// </summar
        /// 
        protected void lkbAddHeader_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Employee/AddAddiction_Heads.aspx");
        }




        #endregion

        #region Methods

        /// <summary>
        /// Bind Grid Based on User Registration Details
        /// Created By Divya
        /// </summary>
        /// <param name="sender"></param>
        //public void Bind_Grid()
        //{
        //    try
        //    {
        //        string Cmpy = (string)Session["ClientRegistrationNo"];
        //        string Brnach = (string)Session["BranchID"];
        //        string UserType = (string)Session["User_Type"];
        //        string UserName = (string)Session["User_Name"];
        //        string Cration_Company = (string)Session["CreationCompany"]; //"000001";
        //        //if (UserType == "User")
        //        //{
        //        var objHeaderInfo = (from header in objDAL.HRMS_Additions_Masters
        //                             where header.Creation_Company == Cmpy
        //                             orderby header.Addition_Code descending
        //                             select header).ToList();
        //        // List<Administrator_UserRegistration> ObjuserInfo = objSCM_userRegistration.Get_userRegDetails(Session["ClientRegistrationNo"].ToString());
        //        if (objHeaderInfo.Count > 0)
        //        {
        //            grdHeaderList.DataSource = objHeaderInfo;
        //            grdHeaderList.DataBind();
        //        }
        //        else
        //        {
        //            // // // // Grid view using setting initial rows displaying Empty Header Displying
        //            SetInitialRow();
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        string msg = ex.Message.ToString();
        //    }
        //    finally
        //    {
        //    }
        //}


        /// <summary>
        /// Setting Initial Empty Row Data Into Header Details 
        /// </summary>
        /// <param name="sender"></param>
        public void SetInitialRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("Addition_Code", typeof(string)));
            dt.Columns.Add(new DataColumn("Head_Name", typeof(string)));
            dt.Columns.Add(new DataColumn("Short_Name", typeof(string)));
            dt.Columns.Add(new DataColumn("Caluculation", typeof(string)));
            dt.Columns.Add(new DataColumn("Per_Of_Basic", typeof(string)));
            dt.Columns.Add(new DataColumn("Ot_Head_Names", typeof(string)));
            dt.Columns.Add(new DataColumn("Ot_Day_Working_Hrs", typeof(string)));
            dt.Columns.Add(new DataColumn("Ot_Night_Working_Hrs", typeof(string)));
            dt.Columns.Add(new DataColumn("Ot_On_Weakly_Off", typeof(string)));
            dt.Columns.Add(new DataColumn("Ot_On_Holiday", typeof(string)));


            dr = dt.NewRow();

            dr["Addition_Code"] = string.Empty;
            dr["Head_Name"] = string.Empty;
            dr["Short_Name"] = string.Empty;
            dr["Caluculation"] = string.Empty;
            dr["Per_Of_Basic"] = string.Empty;
            dr["Ot_Head_Names"] = string.Empty;
            dr["Ot_Day_Working_Hrs"] = string.Empty;
            dr["Ot_Night_Working_Hrs"] = string.Empty;
            dr["Ot_On_Weakly_Off"] = string.Empty;
            dr["Ot_On_Holiday"] = string.Empty;
            dt.Rows.Add(dr);

            grdHeaderList.DataSource = dt;
            grdHeaderList.DataBind();

            int columncount = 7;
            grdHeaderList.Rows[0].Cells.Clear();
            grdHeaderList.Rows[0].Cells.Add(new TableCell());
            grdHeaderList.Rows[0].Cells[0].ColumnSpan = columncount;
            grdHeaderList.Rows[0].Cells[0].Text = "No Records Found";
            //  grdStorageLocation.HeaderStyle.HorizontalAlign = "center";
            grdHeaderList.Rows[0].Cells[0].ForeColor = System.Drawing.Color.Red;
        }

        /// <summary>
        ///  The Following Method is used for Delete Grid records to Data From User Registration  Table
        ///  Created By Divya
        /// </summary>
        //public void Delete_HeaderDetails(string AdditionCode, string Creation_Company)
        //{
        //    try
        //    {
        //        HRMS_Additions_Master a = objSCM_HeaderBAL.Delete_Addition_Header(AdditionCode, Session["ClientRegistrationNo"].ToString());

        //        if (a.Addition_Code != null)
        //        {
        //            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Header Details Deleted Successfully ');", true);
        //        }
        //        else
        //        {
        //            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Header Details Already Existing In Header Page ');", true);
        //        }
        //        Bind_Grid();
        //        txtSearch.Focus();

        //    }
        //    catch (Exception ex)
        //    {
        //        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('" + ex.ToString() + "');", true);
        //    }
        //    finally
        //    {
        //    }
        //}

        #endregion

        #region User Registration Details Bind For Grid view


        /// <summary>
        /// Row Command Grid view  For UserRegistration Details
        /// Created By Divya
        /// </summar
        /// 

        protected void grdHeaderList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string AdditionCode = e.CommandArgument.ToString();
            if (e.CommandName == "Edit")
            {

                string path = "~/Employee/AddAddiction_Heads.aspx";
                Session["AdditionCode"] = AdditionCode;
                Response.Redirect(path);

            }
            if (e.CommandName == "Delete")
            {

                string Creation_Company = Session["ClientRegistrationNo"].ToString();

                //Delete_HeaderDetails(AdditionCode, Creation_Company);

            }


        }


        /// <summary>
        /// Paging For User Registration Details
        /// </summar
        /// 
        protected void grdHeaderList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdHeaderList.PageIndex = e.NewPageIndex;
            //Bind_Grid();
        }


        /// <summary>
        /// Row Delte Grid view  For UserRegistration
        /// </summary>
        /// 
        protected void grdHeaderList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        /// <summary>
        /// Row Editing Grid view  For UserRegistration
        /// </summary>
        /// 

        protected void grdHeaderList_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        /// <summary>
        /// Row Sorting Grid view  For UserRegistration Details
        /// </summar
        /// 

        protected void grdHeaderList_Sorting(object sender, GridViewSortEventArgs e)
        {
            //string Sortdir = GetSortDirection(e.SortExpression);
            //string SortExp = e.SortExpression;
            //String Creation_Company = Session["ClientRegistrationNo"].ToString();
            //List<Administrator_UserRegistration> list = objSCM_userRegistration.Get_UserRegistration(Creation_Company);


            //if (Sortdir == "ASC")
            //{
            //    list = Sort<Administrator_UserRegistration>(list, SortExp, SortDirection.Ascending);
            //}
            //else
            //{
            //    list = Sort<Administrator_UserRegistration>(list, SortExp, SortDirection.Descending);
            //}
            //this.grdUserRegistration.DataSource = list;
            //this.grdUserRegistration.DataBind();
        }

        //private List<Administrator_UserRegistration> Sort<TKey>(List<Administrator_UserRegistration> list, string sortBy, SortDirection direction)
        //{
        //    PropertyInfo property = list.GetType().GetGenericArguments()[0].GetProperty(sortBy);
        //    if (direction == SortDirection.Ascending)
        //    {
        //        return list.OrderBy(e => property.GetValue(e, null)).ToList<Administrator_UserRegistration>();
        //    }
        //    else
        //    {
        //        return list.OrderByDescending(e => property.GetValue(e, null)).ToList<Administrator_UserRegistration>();
        //    }
        //}

        //  GetSortDirection Method it using PRoduct Grid Sorting  
        //private string GetSortDirection(string column)
        //{
        //    string sortDirection = "ASC";
        //    string sortExpression = ViewState["SortExpression"] as string;
        //    if (sortExpression != null)
        //    {
        //        if (sortExpression == column)
        //        {
        //            string lastDirection = ViewState["SortDirection"] as string;
        //            if ((lastDirection != null) && (lastDirection == "ASC"))
        //            {
        //                sortDirection = "DESC";
        //            }
        //        }
        //    }
        //    ViewState["SortDirection"] = sortDirection;
        //    ViewState["SortExpression"] = column;
        //    return sortDirection;
        //}

        #endregion



        #region Permissions

        // // // Add Functioanlity Permissions
        public void AdminPermissionAdd()
        {
            try
            {
                string Cmpy = (string)Session["ClientRegistrationNo"];
                string Brnach = (string)Session["BranchID"];
                string UserType = (string)Session["User_Type"];
                string UserName = (string)Session["User_Name"];
                var objUserPermissions = (from UserPerm in objDAL.Administrator_UserRegistrations
                                          where UserPerm.Branch_ID == Brnach
                                          && UserPerm.Login_Id == UserName && UserPerm.User_Type == UserType
                                          select UserPerm).ToList();
                if (objUserPermissions.Count > 0)
                {
                    string[] CmpName = objUserPermissions[0].AllowCompanyListID.Split(',');
                    int count = 0;

                    for (int i = 0; i < CmpName.Length; i++)
                    {
                        if (CmpName[i] == Cmpy)
                        {
                            string[] AllowCreate = objUserPermissions[0].Administrator_Create.Split(',');

                            for (int j = 0; j < AllowCreate.Length; j++)
                            {



                                string abc = AllowCreate[j];
                                switch (abc)
                                {
                                    case "User Registration":
                                        {
                                            count = count + 1;
                                            break;
                                        }

                                    case "":
                                        {
                                            //   count = count + 1;
                                            break;
                                        }


                                }

                            }

                        }

                    }
                    if (count > 0)
                    {
                        Response.Redirect("AddUserRegistration.aspx");
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Sorry You Do not have Permissions for Creating new User Registration  ..!');", true);
                    }
                }


            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        // // // Modify Functionality Permissions
        public void AdminPermissionModify(string strEdit)
        {
            try
            {
                string Cmpy = (string)Session["ClientRegistrationNo"];
                string Brnach = (string)Session["BranchID"];
                string UserType = (string)Session["User_Type"];
                string UserName = (string)Session["User_Name"];
                var objUserPermissions = (from UserPerm in objDAL.Administrator_UserRegistrations
                                          where UserPerm.Branch_ID == Brnach
                                          && UserPerm.Login_Id == UserName && UserPerm.User_Type == UserType
                                          select UserPerm).ToList();
                if (objUserPermissions.Count > 0)
                {
                    string[] CmpName = objUserPermissions[0].AllowCompanyListID.Split(',');
                    int count = 0;

                    for (int i = 0; i < CmpName.Length; i++)
                    {
                        if (CmpName[i] == Cmpy)
                        {
                            string[] AllowCreate = objUserPermissions[0].Administrator_Modify.Split(',');

                            for (int j = 0; j < AllowCreate.Length; j++)
                            {


                                string abc = AllowCreate[j];
                                switch (abc)
                                {
                                    case "User Registration":
                                        {
                                            count = count + 1;
                                            break;
                                        }

                                    case "":
                                        {
                                            // count = count + 1;
                                            break;
                                        }


                                }

                            }

                        }

                    }
                    if (count > 0)
                    {
                        string path = "AddUserRegistration.aspx?id=" + strEdit;
                        Response.Redirect(path);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Sorry You Do not have Permissions for Editing User Registration Details..!');", true);
                    }
                }


            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        // // // Delete Functionality Permissions
        public void AdminPermissionDelete(string strDelete)
        {
            //try
            //{
            //    string Cmpy = (string)Session["ClientRegistrationNo"];
            //    string Brnach = (string)Session["BranchID"];
            //    string UserType = (string)Session["User_Type"];
            //    string UserName = (string)Session["User_Name"];
            //    var objUserPermissions = (from UserPerm in objDAL.Administrator_UserRegistrations
            //                              where UserPerm.Branch_ID == Brnach
            //                              && UserPerm.Login_Id == UserName && UserPerm.User_Type == UserType
            //                              select UserPerm).ToList();
            //    if (objUserPermissions.Count > 0)
            //    {
            //        string[] CmpName = objUserPermissions[0].AllowCompanyListID.Split(',');
            //        int count = 0;

            //        for (int i = 0; i < CmpName.Length; i++)
            //        {
            //            if (CmpName[i] == Cmpy)
            //            {
            //                string[] AllowCreate = objUserPermissions[0].Administrator_Modify.Split(',');

            //                for (int j = 0; j < AllowCreate.Length; j++)
            //                {
            //                    //if (AllowCreate[j] == "Company Information")
            //                    //{
            //                    //    Response.Redirect("AddCompanyInformation.aspx");
            //                    //}



            //                    string abc = AllowCreate[j];
            //                    switch (abc)
            //                    {
            //                        case "User Registration":
            //                            {
            //                                count = count + 1;
            //                                break;
            //                            }

            //                        case "":
            //                            {
            //                                //count = count + 1;
            //                                break;
            //                            }


            //                    }

            //                }

            //            }

            //        }
            //        if (count > 0)
            //        {
            //            string Creation_Company = Session["ClientRegistrationNo"].ToString();

            //            Delete_UserRegistrationDetails(strDelete, Creation_Company);
            //        }
            //        else
            //        {
            //            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Sorry You Do not have Permissions For Deleteing User Registration Details..!');", true);
            //        }
            //    }


            //}
            //catch (Exception ex)
            //{
            //}
            //finally
            //{
            //}
        }
        #endregion

    }
}