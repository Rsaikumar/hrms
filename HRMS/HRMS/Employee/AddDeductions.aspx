﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Share/HRMSMaster.Master" AutoEventWireup="true" CodeBehind="AddDeductions.aspx.cs" Inherits="HRMS.Employee.AddDeductions" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/Employee/Deduction.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div class="container-fluid">
            <div class="breadcrumbs" style="margin-top: 2%;">
                <ul>
                    <li><a href="../Dashboard/Dashboard.aspx"><%= Session["CreationCompany"]  %></a> <i class="fa fa-angle-right"></i></li>
                    <li><a href="#">Payroll</a> <i class="fa fa-angle-right"></i></li>
                    <li><a href="../Employee/AddDeductions.aspx">Deductions</a> </li>
                </ul>
                <div class="close-bread">
                    <a href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-sm-12">

                            <div class="box box-bordered box-color" style="margin-bottom: 25px;">
                                <div class="box-title">
                                    <h3>
                                        <i class="fa fa-th-list"></i>Deductions</h3>
                                </div>
                                <div class="box-content">
                                    <form action="#" method="POST" class='form-horizontal form-bordered'>
                                        <div class="col-sm-12" style="">
                                            <asp:Panel ID="Panel4" runat="server" GroupingText="Pension Found (PF)" Style="border-bottom: 0px solid #8F8F8F;">
                                                <div class="col-sm-12" style="">
                                                    <div class="form-group">
                                                        <label for="textfield" class="control-label col-sm-1 lDPercentage" style="">
                                                            Percentage<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                        <div class="col-sm-2 tDPercentage" style="" id="">
                                                            <asp:TextBox ID="txtPercentage" runat="server" placeholder="Percentage" class="form-control"
                                                                MaxLength="50"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group" style="display: none;">
                                                        <label for="textfield" class="control-label col-sm-1 lDShortName" style="">
                                                            Short Name <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                        <div class="col-sm-2 tDShortName" style="">
                                                            <asp:TextBox ID="txtShortName" runat="server" placeholder="Short Name" MaxLength="50"
                                                                class="form-control" ReadOnly="true"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="textfield" class="control-label col-sm-1 lDLimitEmp" style="">
                                                            Employee Contribution Limit</label>
                                                        <div class="col-sm-2 tDLimitEmp" style="" id="">
                                                            <asp:TextBox ID="txtLimitforEmpContr" runat="server" placeholder="Limit For Emp Contr" class="form-control"
                                                                MaxLength="50"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-sm-12" style="">
                                                    <div class="form-group">
                                                        <label for="textfield" class="control-label col-sm-1 lDDeductOn" style="">
                                                            Deduct On <span style="color: #ff0000; font-size: 14px;">*</span></label>

                                                        <div class="col-sm-2 tDDeductOn3" style="" id="">
                                                            <asp:TextBox ID="txtDeducton" runat="server" placeholder="" class="form-control"
                                                                MaxLength="50"></asp:TextBox>
                                                        </div>
                                                        <div class="col-sm-2 bAYear" style="" id="Div8">
                                                            <asp:Button ID="btnPIDeduct" class="btn" Text="Select Head" runat="server" Style=""
                                                                OnClick="btnPIDeduct_Click" />
                                                        </div>

                                                    </div>
                                                </div>


                                            </asp:Panel>
                                        </div>

                                        <div class="col-sm-12" style="">
                                            <asp:Panel ID="Panel2" runat="server" GroupingText="ESI" Style="border-bottom: 0px solid #8F8F8F;">
                                                <div class="col-sm-12" style="">
                                                    <div class="form-group">
                                                        <label for="textfield" class="control-label col-sm-1 lDPercentage" style="">
                                                            Percentage<span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                        <div class="col-sm-2 tDPercentage" style="" id="">
                                                            <asp:TextBox ID="txtESIPercentage" runat="server" placeholder="Percentage" class="form-control"
                                                                MaxLength="50"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="form-group" style="display: none;">
                                                        <label for="textfield" class="control-label col-sm-1 lDLimitEmp" style="">
                                                            Employee Contribution Limit</label>
                                                        <div class="col-sm-2 tDLimitEmp" style="" id="">
                                                            <asp:TextBox ID="txtESIEmpLimit" runat="server" placeholder="Limit For Emp Contr" class="form-control"
                                                                MaxLength="50"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="textfield" class="control-label col-sm-1 lDDeductOn" style="">
                                                            Deduct On <span style="color: #ff0000; font-size: 14px;">*</span></label>

                                                        <div class="col-sm-2 tDDeductOn3" style="" id="">
                                                            <asp:TextBox ID="txtESIDeduct" runat="server" placeholder="" class="form-control"
                                                                MaxLength="50"></asp:TextBox>
                                                        </div>
                                                        <div class="col-sm-2 bAYear" style="" id="">
                                                            <asp:Button ID="btnESIDeduct" class="btn" Text="Select Head" runat="server" Style="" OnClick="btnESIDeduct_Click" />
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-sm-12" style="">
                                                </div>


                                            </asp:Panel>
                                        </div>

                                        <div class="col-sm-12" style="">
                                            <asp:Panel ID="Panel1" runat="server" GroupingText="Income Tax (TDS)" Style="border-bottom: 0px solid #8F8F8F;">
                                                <div class="col-sm-12" style="display: none;">
                                                    <div class="form-group">
                                                        <label for="textfield" class="control-label col-sm-1 lDIShortName" style="">
                                                            Short Name <span style="color: #ff0000; font-size: 14px;">*</span></label>
                                                        <div class="col-sm-2 tDIShortName" style="">
                                                            <asp:TextBox ID="txtInShortName" runat="server" placeholder="Short Name" MaxLength="50"
                                                                class="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12" style="display: none;">
                                                    <div class="form-group">
                                                        <label for="textfield" class="control-label col-sm-1 lDIDefineRange" style="">
                                                            Define Range <span style="color: #ff0000; font-size: 14px;">*</span></label>

                                                    </div>
                                                </div>
                                                <div class="col-sm-12" style="">
                                                    <div class="row">

                                                        <div class="col-sm-12" style="">
                                                            <asp:GridView ID="GrdDefineRange" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                                                Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%;" runat="server"
                                                                ShowFooter="true" AutoGenerateColumns="False">
                                                                <AlternatingRowStyle BackColor="#EEEEEE" />
                                                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                                <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                                <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                                <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                                <SortedDescendingHeaderStyle BackColor="#000065" />
                                                                <Columns>
                                                                    <%-- <asp:TemplateField HeaderText="Bank ID">
                                                                                <ItemTemplate>
                                                                                    <asp:TextBox ID="txtbankId" runat="server" placeholder="Bank ID" class="form-control"
                                                                                        MaxLength="10" Text='<%# Eval("Bank_ID")%>'></asp:TextBox>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>--%>
                                                                    <asp:TemplateField HeaderText="Income Slab(From)">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtIncomeslabFrom" runat="server" placeholder="Income slab" class="form-control"
                                                                                Text='<%# Eval("Define_Range_From")%>' MaxLength="30"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Income Slab(To)">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtIncomeslabTo" runat="server" placeholder="Income slab" class="form-control"
                                                                                Text='<%# Eval("Define_Range_To")%>' MaxLength="30"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField HeaderText="Percentage">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtDePercentage" runat="server" placeholder="Percentage" class="form-control"
                                                                                Text='<%# Eval("Define_Amount")%>' MaxLength="30"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="lbRemove" runat="server" OnClick="lbRemove_Click">Remove</asp:LinkButton>
                                                                        </ItemTemplate>
                                                                        <FooterStyle HorizontalAlign="Right" />
                                                                        <FooterTemplate>
                                                                            <asp:Button ID="btnAddNewRecord" runat="server" Text="Record" class="btn btn-primary"
                                                                                OnClick="btnAddNewRecord_Click" />
                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>

                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="textfield" class="control-label col-sm-1 lDDeductOn" style="">
                                                                    Deduct On <span style="color: #ff0000; font-size: 14px;">*</span></label>

                                                                <div class="col-sm-2 tDDeductOn3" style="" id="">
                                                                    <asp:TextBox ID="txtTaxDeduct" runat="server" placeholder="" class="form-control"
                                                                        MaxLength="50"></asp:TextBox>
                                                                </div>
                                                                <div class="col-sm-2 bAYear" style="" id="">
                                                                    <asp:Button ID="btnTaxDeduct" class="btn" Text="Select Head" runat="server" Style="" OnClick="btnTaxDeduct_Click" />
                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>


                                            </asp:Panel>
                                        </div>
                                        <div class="col-sm-12" style="">
                                            <asp:Panel ID="Panel3" runat="server" GroupingText="Professional Tax (PT)" Style="border-bottom: 0px solid #8F8F8F;">
                                                <div class="col-sm-12" style="">
                                                    <div class="row">

                                                        <div class="col-sm-12" style="">
                                                            <asp:GridView ID="grdPT" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                                                Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%;" runat="server"
                                                                ShowFooter="true" AutoGenerateColumns="False">
                                                                <AlternatingRowStyle BackColor="#EEEEEE" />
                                                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                                <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                                <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                                <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                                <SortedDescendingHeaderStyle BackColor="#000065" />
                                                                <Columns>

                                                                    <asp:TemplateField HeaderText="Income Slab(From)">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtIncomeslabPTFrom" runat="server" placeholder="Income slab" class="form-control"
                                                                                Text='<%# Eval("Define_Range_PT_From")%>' MaxLength="30"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Income Slab(To)">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtIncomeslabPTTo" runat="server" placeholder="Income slab" class="form-control"
                                                                                Text='<%# Eval("Define_Range_PT_To")%>' MaxLength="30"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Amount">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtAmount" runat="server" placeholder="Amount" class="form-control"
                                                                                Text='<%# Eval("Define_PT_Amount")%>' MaxLength="30"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="lbPTRemove" runat="server" OnClick="lbPTRemove_Click">Remove</asp:LinkButton>
                                                                        </ItemTemplate>
                                                                        <FooterStyle HorizontalAlign="Right" />
                                                                        <FooterTemplate>
                                                                            <asp:Button ID="btnPTAddNewRecord" runat="server" Text="Record" class="btn btn-primary"
                                                                                OnClick="btnPTAddNewRecord_Click" />
                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>

                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="textfield" class="control-label col-sm-1 lDDeductOn" style="">
                                                                    Deduct On <span style="color: #ff0000; font-size: 14px;">*</span></label>

                                                                <div class="col-sm-2 tDDeductOn3" style="" id="">
                                                                    <asp:TextBox ID="txtPTDeduct" runat="server" placeholder="" class="form-control"
                                                                        MaxLength="50"></asp:TextBox>
                                                                </div>
                                                                <div class="col-sm-2 bAYear" style="" id="">
                                                                    <asp:Button ID="btnPTDeduct" class="btn" Text="Select Head" runat="server" Style="" OnClick="btnPTDeduct_Click" />
                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                        <div class="col-sm-12" style="">
                                            <asp:Panel ID="Panel5" runat="server" GroupingText="Other" Style="border-bottom: 0px solid #8F8F8F;">
                                                <div class="col-sm-12" style="">
                                                    <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lALeaveApply" style="">
                                                Head Name:<span style="color: Red; display: inline;">*</span></label>
                                            <div class="col-sm-2 tALeaveApply" style="" id="Div4">
                                                <asp:DropDownList ID="ddlhdname" runat="server" class="form-control DropdownCss">
                                                    <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                    <asp:ListItem Text="APGLI" Value="APGLI"></asp:ListItem>
                                                    <asp:ListItem Text="GIS" Value="GIS"></asp:ListItem>
                                                    <asp:ListItem Text="GPF" Value="GPF"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                                    <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lALeaveApply" style="">
                                               Criteria:<span style="color: Red; display: inline;">*</span></label>
                                            <div class="col-sm-2 tALeaveApply" style="" id="Div1">
                                                 <asp:DropDownList ID="ddlCriteria" runat="server">
                                                                        <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                        <asp:ListItem Text="Fixed" Value="Fixed"></asp:ListItem>
                                                                        <asp:ListItem Text="% Of Basic" Value="% Of Basic"></asp:ListItem>
                                                                        <asp:ListItem Text="% Of Gross" Value="% Of Gross"></asp:ListItem>
                                                                        <asp:ListItem Text="On Selected Head" Value="On Selected Head"></asp:ListItem>
                                                                        <asp:ListItem Text="Range" Value="Range"></asp:ListItem>
                                                                    </asp:DropDownList>
                                            </div>
                                        </div>
                                                    <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lALeaveApply" style="">
                                                Salary Range:<span style="color: Red; display: inline;">*</span></label>
                                            <div class="col-sm-2 tALeaveApply" style="" id="Div2">
                                                <asp:DropDownList ID="ddlSalRange" runat="server" OnSelectedIndexChanged="ddlSalRange_SelectedIndexChanged" AutoPostBack="True">
                                                                        <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                        <asp:ListItem Text="5000-10000" Value="5000-10000"></asp:ListItem>
                                                                        <asp:ListItem Text="10001-15000" Value="10001-15000"></asp:ListItem>
                                                                        <asp:ListItem Text="15001-20000" Value="15001-20000"></asp:ListItem>
                                                                        <asp:ListItem Text="20001-25000" Value="20001-25000"></asp:ListItem>
                                                                        <asp:ListItem Text="25001-30000" Value="25001-30000"></asp:ListItem>
                                                                        <asp:ListItem Text="30001-40000" Value="30001-40000"></asp:ListItem>
                                                                        <asp:ListItem Text="40001-50000" Value="40001-50000"></asp:ListItem>
                                                                       <asp:ListItem Text="13000-16400" Value="13000-16400"></asp:ListItem>
                                                                        <asp:ListItem Text="16401-21230" Value="16401-21230"></asp:ListItem>
                                                                        <asp:ListItem Text="21231-28940" Value="21231-28940"></asp:ListItem>
                                                                        <asp:ListItem Text="28941-35120" Value="28941-35120"></asp:ListItem>
                                                                        <asp:ListItem Text="35121-48600" Value="35121-48600"></asp:ListItem>
                                                                        <asp:ListItem Text="48601+" Value="48601+"></asp:ListItem>
                                                                        
                                                                    </asp:DropDownList>
                                            </div>
                                        </div>
                                                    <div class="form-group">
                                            <label for="textfield" class="control-label col-sm-1 lALeaveApply" style="">
                                                Amount:<span style="color: Red; display: inline;">*</span></label>
                                            <div class="col-sm-2 tALeaveApply" style="" id="Div3">
                                                 <asp:TextBox ID="txtAmount" runat="server" placeholder="Amount" class="form-control"
                                                                         MaxLength="30"></asp:TextBox>
                                                
                                            </div>
                                        </div>
                                                    <asp:GridView ID="GrdOther" class="table table-hover table-nomargin table-bordered CustomerAddress"
                                                        Style="margin-bottom: 0.83%; margin-top: 0.83%; width: 100%;" runat="server"
                                                        ShowFooter="true" AutoGenerateColumns="False" OnRowDataBound="GrdOther_RowDataBound">
                                                        <AlternatingRowStyle BackColor="#EEEEEE" />
                                                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#ffffff" ForeColor="Black" />
                                                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                        <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                        <SortedDescendingHeaderStyle BackColor="#000065" />
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="Head Name">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtHead_Name" runat="server" placeholder="Head Name" class="form-control"
                                                                        Text='<%# Eval("Define_Range_Other_HeadName")%>' MaxLength="30"></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Criteria">
                                                                <ItemTemplate>
                                                                    <asp:DropDownList ID="ddlCriteria" runat="server">
                                                                        <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                        <asp:ListItem Text="Fixed" Value="Fixed"></asp:ListItem>
                                                                        <asp:ListItem Text="% Of Basic" Value="% Of Basic"></asp:ListItem>
                                                                        <asp:ListItem Text="% Of Gross" Value="% Of Gross"></asp:ListItem>
                                                                        <asp:ListItem Text="On Selected Head" Value="On Selected Head"></asp:ListItem>
                                                                        <asp:ListItem Text="Range" Value="Range"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:TextBox ID="txtCriteria" runat="server" placeholder="Criteria" class="form-control"
                                                                        Text='<%# Eval("Define_Range_Criteria")%>' MaxLength="30" Visible="false"></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Slary Range ">
                                                                <ItemTemplate>
                                                                    <asp:DropDownList ID="ddlSalRange" runat="server" OnSelectedIndexChanged="ddlSalRange_SelectedIndexChanged">
                                                                        <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                                                        <asp:ListItem Text="5000-10000" Value="5000-10000"></asp:ListItem>
                                                                        <asp:ListItem Text="10001-15000" Value="10001-15000"></asp:ListItem>
                                                                        <asp:ListItem Text="15001-20000" Value="15001-20000"></asp:ListItem>
                                                                        <asp:ListItem Text="20001-25000" Value="20001-25000"></asp:ListItem>
                                                                        <asp:ListItem Text="25001-30000" Value="25001-30000"></asp:ListItem>
                                                                        <asp:ListItem Text="30001-40000" Value="30001-40000"></asp:ListItem>
                                                                        <asp:ListItem Text="40001-50000" Value="40001-50000"></asp:ListItem>
                                                                       <asp:ListItem Text="13000-16400" Value="13000-16400"></asp:ListItem>
                                                                        <asp:ListItem Text="16401-21230" Value="16401-21230"></asp:ListItem>
                                                                        <asp:ListItem Text="21231-28940" Value="21231-28940"></asp:ListItem>
                                                                        <asp:ListItem Text="28941-35120" Value="28941-35120"></asp:ListItem>
                                                                        <asp:ListItem Text="35121-48600" Value="35121-48600"></asp:ListItem>
                                                                        <asp:ListItem Text="48601+" Value="48601+"></asp:ListItem>
                                                                        
                                                                    </asp:DropDownList>
                                                                    <asp:TextBox ID="txtSalAmount" runat="server" placeholder="Criteria" class="form-control"
                                                                        Text='<%# Eval("Salary_Range")%>' MaxLength="30" Visible="false"></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Amount">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtAmount" runat="server" placeholder="Amount" class="form-control"
                                                                        Text='<%# Eval("OtherAmount") %>' MaxLength="30"></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lbOTRemove" runat="server" OnClick="lbOTRemove_Click">Remove</asp:LinkButton>
                                                                </ItemTemplate>
                                                                <FooterStyle HorizontalAlign="Right" />
                                                                <FooterTemplate>
                                                                    <asp:Button ID="btnOTAddNewRecord" runat="server" Text="Record" class="btn btn-primary"
                                                                        OnClick="btnOTAddNewRecord_Click" />
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="textfield" class="control-label col-sm-1 lDDeductOn" style="">
                                                            Deduct On <span style="color: #ff0000; font-size: 14px;">*</span></label>

                                                        <div class="col-sm-2 tDDeductOn3" style="" id="">
                                                            <asp:TextBox ID="txtOtherDeduct" runat="server" placeholder="" class="form-control"
                                                                MaxLength="50"></asp:TextBox>
                                                        </div>
                                                        <div class="col-sm-2 bAYear" style="" id="">
                                                            <asp:Button ID="btnOtherDeduct" class="btn" Text="Select Head" runat="server" Style="" OnClick="btnOtherDeduct_Click" />
                                                        </div>

                                                    </div>

                                                </div>


                                            </asp:Panel>
                                        </div>
                                        <div class="form-actions col-sm-offset-2 col-sm-10 ACButtons">
                                            <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnUpdate" runat="server" Text="Save" class="btn btn-primary" OnClick="btnUpdate_Click" />
                                            <%--<asp:Button ID="btnCancel" runat="server" Text="Reset" class="btn" />--%>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <asp:Button ID="btnDeptDetails" runat="server" Style="display: none;" />
                    <ajaxToolKit:ModalPopupExtender ID="MPDept" runat="server" PopupControlID="PanelDept" TargetControlID="btnDeptDetails"
                        CancelControlID="btnCloseDept" BackgroundCssClass="modalBackground">
                    </ajaxToolKit:ModalPopupExtender>
                    <asp:Panel ID="PanelDept" runat="server" CssClass="modal1Popup" align="center" Style="display: none;">
                        <div class="modal-header" runat="server" id="Popupheader3">
                            <h3>Deduct On<a class="close-modal" href="#" id="btnCloseDept">&times;</a></h3>
                        </div>
                        <div class="modal-body">
                            <div class="panel-body">
                                <div class="col-sm-12" style="">
                                    <div class="form-group">
                                        <label for="textfield" class="control-label col-sm-1  lCDeptID" style="">
                                            Deduct On
                                        </label>
                                        <div class="col-sm-8 tAUCompanyID " style="">
                                            <asp:CheckBoxList ID="chlOTLists" runat="server" class="ListControl"
                                                RepeatDirection="vertical" RepeatLayout="Table" RepeatColumns="4"
                                                Width="100%">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>

                                    <div class="popbuttons" style="">
                                        <asp:Button ID="btnPFDedctSave" class="btn btn-primary" runat="server" Text="Submit" OnClick="btnPFDedctSave_Click" />
                                        <asp:Button ID="btnESIDedctSave" class="btn btn-primary" runat="server" Text="Submit" OnClick="btnESIDedctSave_Click" />
                                        <asp:Button ID="btnTDSDedctSave" class="btn btn-primary" runat="server" Text="Submit" OnClick="btnTDSDedctSave_Click" />
                                        <asp:Button ID="btnPTDedctSave" class="btn btn-primary" runat="server" Text="Submit" OnClick="btnPTDedctSave_Click" />
                                        <asp:Button ID="btnOtherDedctSave" class="btn btn-primary" runat="server" Text="Submit" OnClick="btnOtherDedctSave_Click" />
                                        <%--<asp:Button ID="btnDedctRest" runat="server" Text="Reset" class="btn" />--%>
                                    </div>
                                </div>
                            </div>
                            <!-- /.panel -->
                        </div>
                    </asp:Panel>


                </ContentTemplate>


            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
