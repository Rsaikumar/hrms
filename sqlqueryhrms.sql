USE [HRMS]
GO
/****** Object:  UserDefinedFunction [dbo].[Fn_autoincrement_Administrator_CompanyInfo]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Fn_autoincrement_Administrator_CompanyInfo] (@Creation_Company varchar(50))  
  
RETURNS varchar(100)  
  
as  
begin  
  
DECLARE @Result   VARCHAR(100)='',@newvalue VARCHAR(100)  
select  @newvalue = MAX(ClientRegistrationNo)  from Administrator_CompanyInfo WHere Creation_Company=@Creation_Company --order by ClientRegistrationNo desc  
select @newvalue=convert(varchar(27),Convert(int,Replace(ISNULL(@newvalue,'000000'),'',''))+1);  
select @Result=@Result + case len(@newvalue) when 1 then '00000'+@newvalue when 2 then '0000'+@newvalue when 3 then '000'+@newvalue when 4 then '00'+@newvalue when 5 then '0'+@newvalue else @newvalue end;  
return @Result  
end  





GO
/****** Object:  UserDefinedFunction [dbo].[Fn_autoincrement_HRMS_Additions_Master]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Fn_autoincrement_HRMS_Additions_Master] (@Creation_Company varchar(50))    
    
RETURNS varchar(100)    
    
as    
begin    
    
DECLARE @Result   VARCHAR(100)='',@newvalue VARCHAR(100)    
select @newvalue =MAX(Addition_Code) from [dbo].[HRMS_Additions_Master]  Where Creation_Company=@Creation_Company --order by Sno desc    
select @newvalue=convert(varchar(27),Convert(int,Replace(ISNULL(@newvalue,'000000'),'',''))+1);    
select @Result=@Result + case len(@newvalue) when 1 then '00000'+@newvalue when 2 then '0000'+@newvalue when 3 then '000'+@newvalue when 4 then '00'+@newvalue when 5 then '0'+@newvalue else @newvalue end;    
return @Result    
end




GO
/****** Object:  UserDefinedFunction [dbo].[Fn_autoincrement_HRMS_Bank_Master]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Fn_autoincrement_HRMS_Bank_Master] (@Creation_Company varchar(50))    
    
RETURNS varchar(100)    
    
as    
begin    
    
DECLARE @Result   VARCHAR(100)='',@newvalue VARCHAR(100)    
select @newvalue =MAX(Bank_Id) from [dbo].[HRMS_Bank_Master]  Where Creation_Company=@Creation_Company --order by Sno desc    
select @newvalue=convert(varchar(27),Convert(int,Replace(ISNULL(@newvalue,'000000'),'',''))+1);    
select @Result=@Result + case len(@newvalue) when 1 then '00000'+@newvalue when 2 then '0000'+@newvalue when 3 then '000'+@newvalue when 4 then '00'+@newvalue when 5 then '0'+@newvalue else @newvalue end;    
return @Result    
end




GO
/****** Object:  UserDefinedFunction [dbo].[Fn_autoincrement_HRMS_City_Master]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Fn_autoincrement_HRMS_City_Master] (@Creation_Company varchar(50))    
    
RETURNS varchar(100)    
    
as    
begin    
    
DECLARE @Result   VARCHAR(100)='',@newvalue VARCHAR(100)    
select @newvalue =MAX(City_Code) from HRMS_City_Master Where Creation_Company=@Creation_Company --order by Sno desc    
select @newvalue=convert(varchar(27),Convert(int,Replace(ISNULL(@newvalue,'000000'),'',''))+1);    
select @Result=@Result + case len(@newvalue) when 1 then '00000'+@newvalue when 2 then '0000'+@newvalue when 3 then '000'+@newvalue when 4 then '00'+@newvalue when 5 then '0'+@newvalue else @newvalue end;    
return @Result    
end





GO
/****** Object:  UserDefinedFunction [dbo].[Fn_autoincrement_HRMS_Deductions_IncomeTax_Details]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Fn_autoincrement_HRMS_Deductions_IncomeTax_Details] (@Creation_Company varchar(50))    
    
RETURNS varchar(100)    
    
as    
begin    
    
DECLARE @Result   VARCHAR(100)='',@newvalue VARCHAR(100)    
select @newvalue =MAX(PF_Code) from HRMS_Deductions_IncomeTax_Details Where Creation_Company=@Creation_Company --order by Sno desc    
select @newvalue=convert(varchar(27),Convert(int,Replace(ISNULL(@newvalue,'000000'),'',''))+1);    
select @Result=@Result + case len(@newvalue) when 1 then '00000'+@newvalue when 2 then '0000'+@newvalue when 3 then '000'+@newvalue when 4 then '00'+@newvalue when 5 then '0'+@newvalue else @newvalue end;    
return @Result    
end




GO
/****** Object:  UserDefinedFunction [dbo].[Fn_autoincrement_HRMS_Deductions_Other_Details]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Fn_autoincrement_HRMS_Deductions_Other_Details] (@Creation_Company varchar(50))    
    
RETURNS varchar(100)    
    
as    
begin    
    
DECLARE @Result   VARCHAR(100)='',@newvalue VARCHAR(100)    
select @newvalue =MAX(PF_Code) from HRMS_Deductions_Other_Details Where Creation_Company=@Creation_Company --order by Sno desc    
select @newvalue=convert(varchar(27),Convert(int,Replace(ISNULL(@newvalue,'000000'),'',''))+1);    
select @Result=@Result + case len(@newvalue) when 1 then '00000'+@newvalue when 2 then '0000'+@newvalue when 3 then '000'+@newvalue when 4 then '00'+@newvalue when 5 then '0'+@newvalue else @newvalue end;    
return @Result    
end




GO
/****** Object:  UserDefinedFunction [dbo].[Fn_autoincrement_HRMS_Deductions_PensionFund_Details]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Fn_autoincrement_HRMS_Deductions_PensionFund_Details] (@Creation_Company varchar(50))    
    
RETURNS varchar(100)    
    
as    
begin    
    
DECLARE @Result   VARCHAR(100)='',@newvalue VARCHAR(100)    
select @newvalue =MAX(PF_Code) from HRMS_Deductions_PensionFund_Details Where Creation_Company=@Creation_Company --order by Sno desc    
select @newvalue=convert(varchar(27),Convert(int,Replace(ISNULL(@newvalue,'000000'),'',''))+1);    
select @Result=@Result + case len(@newvalue) when 1 then '00000'+@newvalue when 2 then '0000'+@newvalue when 3 then '000'+@newvalue when 4 then '00'+@newvalue when 5 then '0'+@newvalue else @newvalue end;    
return @Result    
end




GO
/****** Object:  UserDefinedFunction [dbo].[Fn_autoincrement_HRMS_Deductions_ProfessionalTax_PT_Details]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[Fn_autoincrement_HRMS_Deductions_ProfessionalTax_PT_Details] (@Creation_Company varchar(50))    
    
RETURNS varchar(100)    
    
as    
begin    
    
DECLARE @Result   VARCHAR(100)='',@newvalue VARCHAR(100)    
select @newvalue =MAX(PF_Code) from [dbo].[HRMS_Deductions_ProfessionalTax_PT_Details]  Where Creation_Company=@Creation_Company --order by Sno desc    
select @newvalue=convert(varchar(27),Convert(int,Replace(ISNULL(@newvalue,'000000'),'',''))+1);    
select @Result=@Result + case len(@newvalue) when 1 then '00000'+@newvalue when 2 then '0000'+@newvalue when 3 then '000'+@newvalue when 4 then '00'+@newvalue when 5 then '0'+@newvalue else @newvalue end;    
return @Result    
end





GO
/****** Object:  UserDefinedFunction [dbo].[Fn_autoincrement_HRMS_Deductions_ProfessionalTax_PT_Details_1]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Fn_autoincrement_HRMS_Deductions_ProfessionalTax_PT_Details_1] (@Creation_Company varchar(50))    
    
RETURNS varchar(100)    
    
as    
begin    
    
DECLARE @Result   VARCHAR(100)='',@newvalue VARCHAR(100)    
select @newvalue =MAX(PF_Code) from HRMS_Deductions_ProfessionalTax_PT_Details Where Creation_Company=@Creation_Company --order by Sno desc    
select @newvalue=convert(varchar(27),Convert(int,Replace(ISNULL(@newvalue,'000000'),'',''))+1);    
select @Result=@Result + case len(@newvalue) when 1 then '00000'+@newvalue when 2 then '0000'+@newvalue when 3 then '000'+@newvalue when 4 then '00'+@newvalue when 5 then '0'+@newvalue else @newvalue end;    
return @Result    
end




GO
/****** Object:  UserDefinedFunction [dbo].[Fn_autoincrement_HRMS_Department_Master]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Fn_autoincrement_HRMS_Department_Master] (@Creation_Company varchar(50))    
    
RETURNS varchar(100)    
    
as    
begin    
    
DECLARE @Result   VARCHAR(100)='',@newvalue VARCHAR(100)    
select @newvalue =MAX(Department_Code) from [dbo].[HRMS_Department_Master] Where Creation_Company=@Creation_Company --order by Sno desc    
select @newvalue=convert(varchar(27),Convert(int,Replace(ISNULL(@newvalue,'000000'),'',''))+1);    
select @Result=@Result + case len(@newvalue) when 1 then '00000'+@newvalue when 2 then '0000'+@newvalue when 3 then '000'+@newvalue when 4 then '00'+@newvalue when 5 then '0'+@newvalue else @newvalue end;    
return @Result    
end





GO
/****** Object:  UserDefinedFunction [dbo].[Fn_autoincrement_HRMS_Employee_Master_PersonalDetails]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[Fn_autoincrement_HRMS_Employee_Master_PersonalDetails] (@Creation_Company varchar(50))  
  
RETURNS varchar(100)  
  
as  
begin  
  
DECLARE @Result   VARCHAR(100)='',@newvalue VARCHAR(100)  
select  @newvalue = MAX(Emp_Code)  from [dbo].[HRMS_Employee_Master_PersonalDetails] WHere Creation_Company=@Creation_Company --order by ClientRegistrationNo desc  
select @newvalue=convert(varchar(27),Convert(int,Replace(ISNULL(@newvalue,'000000'),'',''))+1);  
select @Result=@Result + case len(@newvalue) when 1 then '00000'+@newvalue when 2 then '0000'+@newvalue when 3 then '000'+@newvalue when 4 then '00'+@newvalue when 5 then '0'+@newvalue else @newvalue end;  
return @Result  
end




GO
/****** Object:  UserDefinedFunction [dbo].[Fn_autoincrement_HRMS_Employee_Work_Locations]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Fn_autoincrement_HRMS_Employee_Work_Locations] (@Creation_Company varchar(50))    
    
RETURNS varchar(100)    
    
as    
begin    
    
DECLARE @Result   VARCHAR(100)='',@newvalue VARCHAR(100)    
select @newvalue =MAX(Location_Id) from [dbo].[HRMS_Employee_Work_Locations] Where Creation_Company=@Creation_Company --order by Sno desc    
select @newvalue=convert(varchar(27),Convert(int,Replace(ISNULL(@newvalue,'000000'),'',''))+1);    
select @Result=@Result + case len(@newvalue) when 1 then '00000'+@newvalue when 2 then '0000'+@newvalue when 3 then '000'+@newvalue when 4 then '00'+@newvalue when 5 then '0'+@newvalue else @newvalue end;    
return @Result    
end




GO
/****** Object:  UserDefinedFunction [dbo].[Fn_autoincrement_HRMS_Group_Master]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Fn_autoincrement_HRMS_Group_Master] (@Creation_Company varchar(50))    
    
RETURNS varchar(100)    
    
as    
begin    
    
DECLARE @Result   VARCHAR(100)='',@newvalue VARCHAR(100)    
select @newvalue =MAX(Group_Code) from [dbo].[HRMS_Group_Master]  Where Creation_Company=@Creation_Company --order by Sno desc    
select @newvalue=convert(varchar(27),Convert(int,Replace(ISNULL(@newvalue,'000000'),'',''))+1);    
select @Result=@Result + case len(@newvalue) when 1 then '00000'+@newvalue when 2 then '0000'+@newvalue when 3 then '000'+@newvalue when 4 then '00'+@newvalue when 5 then '0'+@newvalue else @newvalue end;    
return @Result    
end





GO
/****** Object:  UserDefinedFunction [dbo].[Fn_autoincrement_HRMS_Holiday_Master]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Fn_autoincrement_HRMS_Holiday_Master] (@Creation_Company varchar(50))    
    
RETURNS varchar(100)    
    
as    
begin    
    
DECLARE @Result   VARCHAR(100)='',@newvalue VARCHAR(100)    
select @newvalue =MAX(Holiday_Code) from HRMS_Holiday_Master Where Creation_Company=@Creation_Company --order by Sno desc    
select @newvalue=convert(varchar(27),Convert(int,Replace(ISNULL(@newvalue,'000000'),'',''))+1);    
select @Result=@Result + case len(@newvalue) when 1 then '00000'+@newvalue when 2 then '0000'+@newvalue when 3 then '000'+@newvalue when 4 then '00'+@newvalue when 5 then '0'+@newvalue else @newvalue end;    
return @Result    
end




GO
/****** Object:  UserDefinedFunction [dbo].[Fn_autoincrement_HRMS_JobType_Master]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Fn_autoincrement_HRMS_JobType_Master] (@Creation_Company varchar(50))    
    
RETURNS varchar(100)    
    
as    
begin    
    
DECLARE @Result   VARCHAR(100)='',@newvalue VARCHAR(100)    
select @newvalue =MAX(Job_Id) from [dbo].[HRMS_JobType_Master]  Where Creation_Company=@Creation_Company --order by Sno desc    
select @newvalue=convert(varchar(27),Convert(int,Replace(ISNULL(@newvalue,'000000'),'',''))+1);    
select @Result=@Result + case len(@newvalue) when 1 then '00000'+@newvalue when 2 then '0000'+@newvalue when 3 then '000'+@newvalue when 4 then '00'+@newvalue when 5 then '0'+@newvalue else @newvalue end;    
return @Result    
end




GO
/****** Object:  UserDefinedFunction [dbo].[Fn_autoincrement_HRMS_Leave_Application]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[Fn_autoincrement_HRMS_Leave_Application] (@Creation_Company varchar(50))    
    
RETURNS varchar(100)    
    
as    
begin    
    
DECLARE @Result   VARCHAR(100)='',@newvalue VARCHAR(100)    
select @newvalue =MAX(Leave_Code) from HRMS_Leave_Application Where Creation_Company=@Creation_Company --order by Sno desc    
select @newvalue=convert(varchar(27),Convert(int,Replace(ISNULL(@newvalue,'000000'),'',''))+1);    
select @Result=@Result + case len(@newvalue) when 1 then '00000'+@newvalue when 2 then '0000'+@newvalue when 3 then '000'+@newvalue when 4 then '00'+@newvalue when 5 then '0'+@newvalue else @newvalue end;    
return @Result    
end





GO
/****** Object:  UserDefinedFunction [dbo].[Fn_autoincrement_HRMS_Leave_Master]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[Fn_autoincrement_HRMS_Leave_Master] (@Creation_Company varchar(50))    
    
RETURNS varchar(100)    
    
as    
begin    
    
DECLARE @Result   VARCHAR(100)='',@newvalue VARCHAR(100)    
select @newvalue =MAX(Leave_Code) from HRMS_Leave_Master Where Creation_Company=@Creation_Company --order by Sno desc    
select @newvalue=convert(varchar(27),Convert(int,Replace(ISNULL(@newvalue,'000000'),'',''))+1);    
select @Result=@Result + case len(@newvalue) when 1 then '00000'+@newvalue when 2 then '0000'+@newvalue when 3 then '000'+@newvalue when 4 then '00'+@newvalue when 5 then '0'+@newvalue else @newvalue end;    
return @Result    
end





GO
/****** Object:  UserDefinedFunction [dbo].[Fn_autoincrement_HRMS_OT_Criteria_Master]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Fn_autoincrement_HRMS_OT_Criteria_Master] (@Creation_Company varchar(50))    
    
RETURNS varchar(100)    
    
as    
begin    
    
DECLARE @Result   VARCHAR(100)='',@newvalue VARCHAR(100)    
select @newvalue =MAX(OT_Code) from [dbo].[HRMS_OT_Criteria_Master]  Where Creation_Company=@Creation_Company --order by Sno desc    
select @newvalue=convert(varchar(27),Convert(int,Replace(ISNULL(@newvalue,'000000'),'',''))+1);    
select @Result=@Result + case len(@newvalue) when 1 then '00000'+@newvalue when 2 then '0000'+@newvalue when 3 then '000'+@newvalue when 4 then '00'+@newvalue when 5 then '0'+@newvalue else @newvalue end;    
return @Result    
end




GO
/****** Object:  Table [dbo].[Administrator_CompanyInfo]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Administrator_CompanyInfo](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ClientRegistrationNo] [varchar](10) NOT NULL,
	[Company_Name] [varchar](150) NULL,
	[Address1] [varchar](250) NULL,
	[Address2] [varchar](250) NULL,
	[State] [varchar](20) NULL,
	[City] [varchar](20) NULL,
	[Pincode] [varchar](10) NULL,
	[Phone_No] [varchar](25) NULL,
	[E_Mail] [varchar](25) NULL,
	[Web_URL] [varchar](25) NULL,
	[ECC_No] [varchar](15) NULL,
	[TIN_No] [varchar](15) NULL,
	[CE_Range] [varchar](15) NULL,
	[CE_Division] [varchar](15) NULL,
	[Fax_No] [varchar](25) NULL,
	[CE_Commissionarate] [varchar](15) NULL,
	[CST_No] [varchar](15) NULL,
	[PF_Code] [varchar](20) NULL,
	[PF_Region] [varchar](50) NULL,
	[PF_Office] [varchar](50) NULL,
	[ESI_Reg_Code] [varchar](20) NULL,
	[ESI_Branch] [varchar](50) NULL,
	[PT_Reg_No] [varchar](30) NULL,
	[PAN_No] [varchar](20) NULL,
	[Weak_Off] [varchar](50) NULL,
	[Status] [varchar](10) NULL,
	[Creation_Company] [varchar](200) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_Administrator_CompanyInfo] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Administrator_CompanyInfo_BankDetails]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Administrator_CompanyInfo_BankDetails](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ClientRegistrationNo] [varchar](20) NULL,
	[Bank_Name] [varchar](50) NULL,
	[Account_Number] [varchar](20) NULL,
	[Branch] [varchar](50) NULL,
	[Creation_Company] [varchar](50) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Administrator_UserRegistration]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Administrator_UserRegistration](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Login_Id] [varchar](20) NOT NULL,
	[Password] [varchar](20) NULL,
	[User_Name] [varchar](20) NULL,
	[E_Mail] [varchar](50) NULL,
	[User_Type] [varchar](10) NULL,
	[AllowCompanyListID] [varchar](250) NULL,
	[AllowCompanyList] [varchar](250) NULL,
	[ClientRegistrationNo] [varchar](10) NULL,
	[Form_Name] [varchar](50) NULL,
	[Modules] [varchar](200) NULL,
	[Status] [varchar](10) NULL,
	[Administrator_Create] [varchar](250) NULL,
	[Administrator_Modify] [varchar](250) NULL,
	[Administrator_Approve] [varchar](250) NULL,
	[Administrator_View] [varchar](250) NULL,
	[Master_Create] [varchar](250) NULL,
	[Master_Modify] [varchar](250) NULL,
	[Master_Approve] [varchar](250) NULL,
	[Master_View] [varchar](250) NULL,
	[Transcation_Create] [varchar](250) NULL,
	[Transcation_Modify] [varchar](250) NULL,
	[Transcation_Approve] [varchar](250) NULL,
	[Transcation_View] [varchar](250) NULL,
	[Reports_Create] [varchar](250) NULL,
	[Reports_Modify] [varchar](250) NULL,
	[Reports_Approve] [varchar](250) NULL,
	[Reports_View] [varchar](250) NULL,
	[Add_Create] [varchar](10) NULL,
	[Modify] [varchar](10) NULL,
	[Rec_Delete] [varchar](10) NULL,
	[Approve_Authorize] [varchar](10) NULL,
	[View_Print] [varchar](10) NULL,
	[Creation_Company] [varchar](50) NOT NULL,
	[Created_By] [varchar](30) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](30) NULL,
	[Modified_Date] [datetime] NULL,
	[Branch_ID] [varchar](10) NULL,
 CONSTRAINT [PK__Administ__A7230C9ED1DC672A] PRIMARY KEY CLUSTERED 
(
	[Login_Id] ASC,
	[Creation_Company] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[beat_mst]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[beat_mst](
	[beat_cd] [nvarchar](10) NOT NULL,
	[beat_name] [nvarchar](40) NOT NULL,
	[section_cd] [nvarchar](8) NOT NULL,
	[c_date] [datetime] NULL,
	[m_date] [datetime] NULL,
	[old_beat_cd] [nvarchar](10) NULL,
	[created_by] [nvarchar](max) NULL,
	[tcs_cd] [nchar](10) NULL,
 CONSTRAINT [beat_mst_new_pkey] PRIMARY KEY NONCLUSTERED 
(
	[beat_cd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[calenderevents]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[calenderevents](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EventDate] [date] NULL,
	[EventDesc] [varchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[circle_mst]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[circle_mst](
	[circle_cd] [nvarchar](2) NOT NULL,
	[circle_name] [nvarchar](30) NOT NULL,
	[circle_flag] [nvarchar](10) NULL,
	[c_date] [datetime] NULL,
	[m_date] [datetime] NULL,
	[zone_cd] [nvarchar](10) NULL,
	[flag_BL] [char](5) NULL,
	[flag_FP] [nvarchar](2) NULL,
	[flag_FM] [nvarchar](2) NULL,
	[flag_IT] [nvarchar](2) NULL,
	[flag_State] [nvarchar](1) NULL,
	[hh_flag] [nvarchar](2) NULL,
 CONSTRAINT [circle_mst_New_pkey] PRIMARY KEY NONCLUSTERED 
(
	[circle_cd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[dist_div_map]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dist_div_map](
	[distric_cd] [nvarchar](2) NOT NULL,
	[distric_name] [nvarchar](40) NOT NULL,
	[div_cd] [nvarchar](4) NOT NULL,
	[div_name] [nvarchar](30) NULL,
	[map_dist_div_cd] [nvarchar](6) NOT NULL,
	[c_date] [datetime] NULL,
	[m_date] [datetime] NULL,
 CONSTRAINT [dist_div_map_New_pkey] PRIMARY KEY NONCLUSTERED 
(
	[map_dist_div_cd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[district_mst]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[district_mst](
	[distric_cd] [nvarchar](2) NOT NULL,
	[distric_name] [nvarchar](40) NOT NULL,
	[c_date] [datetime] NULL,
	[tcs_cd] [nvarchar](10) NULL,
 CONSTRAINT [district_mst_pkey1] PRIMARY KEY NONCLUSTERED 
(
	[distric_cd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[division_mst]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[division_mst](
	[div_cd] [nvarchar](4) NOT NULL,
	[div_name] [nvarchar](30) NULL,
	[div_flag] [nvarchar](5) NULL,
	[c_date] [datetime] NULL,
	[m_date] [datetime] NULL,
	[circle_cd] [nvarchar](2) NULL,
	[flag_BL] [char](5) NULL,
	[flag_FP] [nvarchar](2) NULL,
	[flag_State] [nvarchar](1) NULL,
	[H_Flag] [nvarchar](1) NULL,
 CONSTRAINT [division_mst_New_pkey] PRIMARY KEY NONCLUSTERED 
(
	[div_cd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[employee_detail_information]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[employee_detail_information](
	[sno] [int] IDENTITY(1,1) NOT NULL,
	[Emp_Code] [varchar](max) NULL,
	[First_Name] [varchar](max) NULL,
	[Middle_Name] [varchar](max) NULL,
	[Last_Name] [varchar](max) NULL,
	[User_Role] [varchar](max) NULL,
	[User_Name] [varchar](max) NULL,
	[password] [varchar](max) NULL,
	[Status] [varchar](max) NULL,
	[circle] [varchar](max) NULL,
	[district] [varchar](max) NULL,
	[Divisions] [varchar](max) NULL,
	[date_of_birth] [date] NULL,
	[age] [int] NULL,
	[marital_status] [varchar](max) NULL,
	[gender] [varchar](max) NULL,
	[Blood_group] [varchar](max) NULL,
	[Religion] [varchar](max) NULL,
	[Nationality] [varchar](max) NULL,
	[adrressone] [varchar](max) NULL,
	[addresstwo] [varchar](max) NULL,
	[city] [varchar](max) NULL,
	[state] [varchar](max) NULL,
	[postalcode] [varchar](max) NULL,
	[country] [varchar](max) NULL,
	[padrressone] [varchar](max) NULL,
	[paddresstwo] [varchar](max) NULL,
	[pcity] [varchar](max) NULL,
	[pstate] [varchar](max) NULL,
	[ppostalcode] [varchar](max) NULL,
	[pcountry] [varchar](max) NULL,
	[Email] [varchar](max) NULL,
	[Alternate_email] [varchar](max) NULL,
	[skype_ID] [varchar](max) NULL,
	[Linkedin] [varchar](max) NULL,
	[Driving_Licence_No] [varchar](max) NULL,
	[Type] [varchar](max) NULL,
	[licence_expiry_date] [date] NULL,
	[adhar_no] [varchar](max) NULL,
	[pan_no] [varchar](max) NULL,
	[passport_no] [varchar](max) NULL,
	[isssued_date] [date] NULL,
	[expiry_date] [date] NULL,
	[bank_name] [varchar](max) NULL,
	[account_no] [varchar](max) NULL,
	[health_insurence_policy_no] [varchar](max) NULL,
	[sum_insured] [varchar](max) NULL,
	[Renewal_date] [date] NULL,
	[date_of_joining] [date] NULL,
	[designation] [varchar](max) NULL,
	[employe_grade] [varchar](max) NULL,
	[department_tsfd] [varchar](max) NULL,
	[reporting_authority] [varchar](max) NULL,
	[location] [varchar](max) NULL,
	[work_type] [varchar](max) NULL,
	[recruit] [varchar](max) NULL,
	[working_in_shifts] [varchar](max) NULL,
	[basic_amount] [decimal](18, 0) NULL,
	[gross_amount] [decimal](18, 0) NULL,
	[da] [decimal](18, 0) NULL,
	[hra] [decimal](18, 0) NULL,
	[cca] [decimal](18, 0) NULL,
	[pf] [varchar](max) NULL,
	[pf_code] [varchar](max) NULL,
	[pf_amount] [decimal](18, 0) NULL,
	[esi] [varchar](max) NULL,
	[esi_code] [varchar](max) NULL,
	[esi_amount] [decimal](18, 0) NULL,
	[eesi_amount] [decimal](18, 0) NULL,
	[esi_dispensary] [decimal](18, 0) NULL,
	[pt] [varchar](max) NULL,
	[tds] [varchar](max) NULL,
	[accomdation] [varchar](max) NULL,
	[four_wheeler] [varchar](max) NULL,
	[two_wheeler] [varchar](max) NULL,
	[laptop] [varchar](max) NULL,
	[mobile_phone] [varchar](max) NULL,
	[other_facilities] [varchar](max) NULL,
	[cretaed_date] [date] NULL,
	[modified_date] [date] NULL,
	[modified_by] [varchar](max) NULL,
	[created_by] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Additions_Master]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Additions_Master](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Addition_Code] [varchar](20) NULL,
	[Head_Name] [varchar](50) NULL,
	[Short_Name] [varchar](30) NULL,
	[Caluculation] [varchar](30) NULL,
	[Per_Of_Basic] [decimal](18, 2) NULL,
	[Ot_Head_Names] [varchar](100) NULL,
	[Ot_Day_Working_Hrs] [decimal](18, 2) NULL,
	[Ot_Night_Working_Hrs] [decimal](18, 2) NULL,
	[Ot_On_Weakly_Off] [decimal](18, 2) NULL,
	[Ot_On_Holiday] [decimal](18, 2) NULL,
	[Creation_Company] [varchar](200) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_HRMS_Deductions_Master] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Bank_Master]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Bank_Master](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Bank_Id] [varchar](10) NULL,
	[Bank_Name] [varchar](50) NULL,
	[Creation_Company] [varchar](50) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_HRMS_Bank_Master] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Calender_Events]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Calender_Events](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EventDate] [date] NULL,
	[EventDesc] [varchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_City_Master]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_City_Master](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[City_Code] [varchar](20) NULL,
	[City_Name] [varchar](50) NULL,
	[State_Name] [varchar](50) NULL,
	[Creation_Company] [varchar](50) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
	[Country] [varchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Deductions_IncomeTax_Details]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Deductions_IncomeTax_Details](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PF_Code] [varchar](20) NULL,
	[Define_Range_From] [decimal](18, 2) NULL,
	[Define_Range_To] [decimal](18, 2) NULL,
	[Define_Amount] [decimal](18, 2) NULL,
	[Deduct_On_Heads] [varchar](50) NULL,
	[Creation_Company] [varchar](200) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_HRMS_Deductions_IncomeTax_Details] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Deductions_Other_Details]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Deductions_Other_Details](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PF_Code] [varchar](20) NULL,
	[Define_Range_Other_HeadName] [varchar](50) NULL,
	[Define_Range_Criteria] [varchar](50) NULL,
	[Define_Amount] [decimal](18, 2) NULL,
	[Deduct_On_Other_Heads] [varchar](50) NULL,
	[Salary_Range] [varchar](20) NULL,
	[Creation_Company] [varchar](200) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_HRMS_Deductions_Other_Details] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Deductions_PensionFund_Details]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Deductions_PensionFund_Details](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PF_Code] [varchar](20) NOT NULL,
	[PF_Percentage] [decimal](18, 2) NULL,
	[PF_Emp_Contribution] [decimal](18, 2) NULL,
	[PF_Deduct] [varchar](150) NULL,
	[ESI_Percentage] [decimal](18, 2) NULL,
	[ESI_Deduct] [varchar](150) NULL,
	[Creation_Company] [varchar](200) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_HRMS_Deductions_PensionFund_Details] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Deductions_ProfessionalTax_PT_Details]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Deductions_ProfessionalTax_PT_Details](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PF_Code] [varchar](20) NULL,
	[Define_Range_PT_From] [decimal](18, 2) NULL,
	[Define_Range_PT_To] [decimal](18, 2) NULL,
	[Define_PT_Amount] [decimal](18, 2) NULL,
	[Deduct_On_PT_Heads] [varchar](50) NULL,
	[Creation_Company] [varchar](200) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_HRMS_Deductions_ProfessionalTax_PT_Details] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Department_Master]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Department_Master](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Department_Code] [varchar](20) NULL,
	[Department_Name] [varchar](50) NULL,
	[Creation_Company] [varchar](50) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_HRMS_Department_Master] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Employee_Documents]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Employee_Documents](
	[Sno] [int] IDENTITY(1,1) NOT NULL,
	[Sent_By] [varchar](50) NULL,
	[Received_By] [varchar](50) NULL,
	[Document] [varbinary](max) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Employee_Increments]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Employee_Increments](
	[Sno] [int] IDENTITY(1,1) NOT NULL,
	[Emp_code] [varchar](50) NULL,
	[Emp_Name] [varchar](50) NULL,
	[Department] [varchar](50) NULL,
	[Designation] [varchar](50) NULL,
	[pay_scale] [varchar](50) NULL,
	[Basic_salary] [money] NULL,
	[Approved_By] [varchar](50) NULL,
	[Creation_Company] [varchar](50) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [varchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Employee_Leave_Details]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Employee_Leave_Details](
	[Sno] [numeric](18, 0) NOT NULL,
	[Emp_Code] [varchar](20) NULL,
	[Emp_Name] [varchar](50) NULL,
	[Job_Type] [varchar](30) NULL,
	[Leave_From] [datetime] NULL,
	[Leave_To] [datetime] NULL,
	[Leave_Hrs] [time](7) NULL,
	[Leave_Code] [varchar](20) NULL,
	[Leave_Name] [varchar](20) NULL,
	[System_Hrs] [time](7) NULL,
	[Working_Hrs] [time](7) NULL,
	[BT_Reason] [varchar](200) NULL,
	[Bt_Expance] [varchar](30) NULL,
	[Bt_Place] [varchar](100) NULL,
	[Creation_Company] [varchar](50) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_HRMS_Employee_Leave_Details] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Employee_Master_Attendance]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Employee_Master_Attendance](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Emp_Code] [varchar](20) NULL,
	[Emp_Name] [varchar](100) NULL,
	[In_Time] [datetime] NULL,
	[Out_Time] [datetime] NULL,
	[Creation_Company] [varchar](50) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_HRMS_Employee_Master_Attendance] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Employee_Master_Educational_Certification_Details]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Employee_Master_Educational_Certification_Details](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Emp_Code] [varchar](20) NULL,
	[Qualification_Certification] [varchar](50) NULL,
	[Institute_University] [varchar](100) NULL,
	[Year] [varchar](10) NULL,
	[Percentage] [decimal](18, 2) NULL,
	[Attach_File] [varchar](50) NULL,
	[Creation_Company] [varchar](50) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_HRMS_Employee_Master_Educational_Skill_Details] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Employee_Master_Educational_Skill_Details]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Employee_Master_Educational_Skill_Details](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Emp_Code] [varchar](20) NULL,
	[Skill_Name] [varchar](50) NULL,
	[Skill_Level] [varchar](10) NULL,
	[Description] [varchar](100) NULL,
	[Creation_Company] [varchar](50) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_HRMS_Employee_Master_Educational_Skill_Details_1] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Employee_Master_Employee_Deductions]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Employee_Master_Employee_Deductions](
	[SNo] [int] IDENTITY(1,1) NOT NULL,
	[Emp_Code] [varchar](20) NULL,
	[Head] [varchar](50) NULL,
	[Amount] [decimal](18, 2) NULL,
	[Frequency] [varchar](50) NULL,
	[Creation_Company] [varchar](50) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_HRMS_Employee_Master_Employee_Deductions_1] PRIMARY KEY CLUSTERED 
(
	[SNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Employee_Master_Employee_Heads_Child]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Employee_Master_Employee_Heads_Child](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Addition_Code] [varchar](20) NULL,
	[Emp_Code] [varchar](20) NULL,
	[Head] [varchar](50) NULL,
	[Amount] [decimal](18, 2) NULL,
	[Frequency] [varchar](50) NULL,
	[Creation_Company] [varchar](50) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_HRMS_Employee_Master_Employee_Deductions] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Employee_Master_FamilyInformation]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Employee_Master_FamilyInformation](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Emp_Code] [varchar](20) NULL,
	[Member_Name] [varchar](50) NULL,
	[Member_Relation] [varchar](30) NULL,
	[Member_DOB] [datetime] NULL,
	[Member_Occupation] [varchar](50) NULL,
	[Member_ContactNo] [varchar](15) NULL,
	[Member_Dependent] [varchar](10) NULL,
	[Member_Nominee] [varchar](10) NULL,
	[Member_Emergency_Contact] [varchar](50) NULL,
	[Creation_Company] [varchar](50) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_HRMS_Employee_Master_FamilyInformation] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Employee_Master_Key_PerformanceDetails]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Employee_Master_Key_PerformanceDetails](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Emp_Code] [varchar](20) NULL,
	[Objective_Goal] [varchar](50) NULL,
	[Measurement] [varchar](50) NULL,
	[Monitoring_Frequency] [varchar](50) NULL,
	[Current_Level] [varchar](50) NULL,
	[Creation_Company] [varchar](50) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_HRMS_Employee_Master_Key_PerformanceDetails] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Employee_Master_Leaves_Information]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Employee_Master_Leaves_Information](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Emp_Code] [varchar](20) NULL,
	[Leave_Name] [varchar](50) NULL,
	[Eligible_To_Aail] [varchar](10) NULL,
	[Opening_Bal] [decimal](18, 2) NULL,
	[This_Year_Allotment] [decimal](18, 2) NULL,
	[Availed_As_On_Date] [decimal](18, 2) NULL,
	[Encashed_This_Year] [decimal](18, 2) NULL,
	[Balance_Available] [decimal](18, 2) NULL,
	[Creation_Company] [varchar](50) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_HRMS_Employee_Master_Leaves_Information] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Employee_Master_Loans_Information]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Employee_Master_Loans_Information](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Emp_Code] [varchar](20) NULL,
	[Loan_Type] [varchar](50) NULL,
	[Loan_Amount] [decimal](18, 2) NULL,
	[Deduct_Per_Month] [decimal](18, 2) NULL,
	[Balance_As_On_Date] [decimal](18, 2) NULL,
	[Creation_Company] [varchar](50) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_HRMS_Employee_Master_Loans_Information_1] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Employee_Master_Official_Information]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Employee_Master_Official_Information](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Emp_Code] [varchar](20) NULL,
	[Date_Of_Join] [date] NULL,
	[Designation] [varchar](30) NULL,
	[Employee_Grade] [varchar](30) NULL,
	[Department] [varchar](30) NULL,
	[Location] [varchar](50) NULL,
	[Reporting_Authority] [varchar](50) NULL,
	[Working_Shifts] [varchar](10) NULL,
	[PF] [varchar](10) NULL,
	[PF_Code] [varchar](20) NULL,
	[ESI] [varchar](10) NULL,
	[ESI_Code] [varchar](20) NULL,
	[ESI_Dispensary] [varchar](50) NULL,
	[Profisional_Tax] [varchar](10) NULL,
	[TDS] [varchar](10) NULL,
	[Quarter_Accommodation] [varchar](10) NULL,
	[Four_Wheeler] [varchar](10) NULL,
	[Two_Wheeler] [varchar](10) NULL,
	[Laptop] [varchar](10) NULL,
	[Mobile_Phone] [varchar](10) NULL,
	[Other_Facilities] [varchar](50) NULL,
	[Creation_Company] [varchar](50) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
	[Work_Type] [varchar](50) NULL,
	[Recruit] [varchar](50) NULL,
 CONSTRAINT [PK_HRMS_Employee_Master_Official_Information] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Employee_Master_PersonalDetails]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Employee_Master_PersonalDetails](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Emp_Code] [varchar](20) NULL,
	[Emp_First_Name] [varchar](50) NULL,
	[Emp_Middle_Name] [varchar](50) NULL,
	[Emp_Last_Name] [varchar](50) NULL,
	[Emp_Roll] [varchar](50) NULL,
	[User_Name] [varchar](50) NULL,
	[Password] [varchar](30) NULL,
	[Emp_DOB] [date] NULL,
	[Emp_Age] [decimal](18, 0) NULL,
	[Date_Of_Anniversary] [date] NULL,
	[Gender] [varchar](20) NULL,
	[Blood_Group] [varchar](20) NULL,
	[Blood_Donor] [varchar](10) NULL,
	[Marital_Status] [varchar](20) NULL,
	[Nationality] [varchar](20) NULL,
	[Religion] [varchar](50) NULL,
	[Languages_Known] [varchar](80) NULL,
	[Hobbies] [varchar](200) NULL,
	[Curricular_Activities] [varchar](50) NULL,
	[Driving_Licenece_Number] [varchar](20) NULL,
	[Licence_Type] [varchar](20) NULL,
	[Licence_Exp_Date] [datetime] NULL,
	[Licence_Attachment] [varchar](50) NULL,
	[Aadhar_Number] [varchar](20) NULL,
	[Aadhar_Attachment] [varchar](20) NULL,
	[Pan_Number] [varchar](20) NULL,
	[Pan_Attachment] [varchar](20) NULL,
	[Passport_Number] [varchar](20) NULL,
	[Issued_Date] [date] NULL,
	[Exp_Date] [date] NULL,
	[Passport_Attachment] [varchar](20) NULL,
	[Bank_Name] [varchar](50) NULL,
	[Account_Number] [varchar](20) NULL,
	[Health_Insurance_Poly_No] [varchar](20) NULL,
	[Sum_Insured] [varchar](20) NULL,
	[Renewal_Date] [date] NULL,
	[Present_Address1] [varchar](100) NULL,
	[Present_Address2] [varchar](100) NULL,
	[Present_City] [varchar](50) NULL,
	[Present_State] [varchar](50) NULL,
	[Present_Pincode] [varchar](10) NULL,
	[Present_Country] [varchar](50) NULL,
	[Permanent_Address1] [varchar](100) NULL,
	[Permanent_Address2] [varchar](100) NULL,
	[Permanent_City] [varchar](50) NULL,
	[Permanent_State] [varchar](50) NULL,
	[Permanent_Pincode] [varchar](10) NULL,
	[Permanent_Country] [varchar](50) NULL,
	[Permanent_Mobile] [varchar](13) NULL,
	[Permanent_Email] [varchar](30) NULL,
	[Alternative_Email] [varchar](30) NULL,
	[Skype_Id] [varchar](30) NULL,
	[Linkedin_Id] [varchar](30) NULL,
	[Emp_Image] [varchar](30) NULL,
	[Emp_Status] [varchar](20) NULL,
	[Creation_Company] [varchar](50) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
	[Resigned_Date] [date] NULL,
	[Educate] [varchar](50) NULL,
	[PresentAsPrev] [varchar](10) NULL,
	[Telephone] [varchar](20) NULL,
 CONSTRAINT [PK_HRMS_Employee_Master_PersonalDetails] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Employee_Master_Professional_Experience]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Employee_Master_Professional_Experience](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Emp_Code] [varchar](20) NULL,
	[Emp_Name] [varchar](50) NULL,
	[Emp_Designation] [varchar](50) NULL,
	[Period_From] [date] NULL,
	[Period_To] [date] NULL,
	[Total_Duration] [varchar](20) NULL,
	[Attach_File] [varchar](50) NULL,
	[Creation_Company] [varchar](50) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_HRMS_Employee_Master_Professional_Experience] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Employee_Official_Info_Head_Details]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Employee_Official_Info_Head_Details](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Emp_Code] [varchar](20) NULL,
	[Heads] [varchar](20) NULL,
	[Amount] [decimal](18, 2) NULL,
	[Basic_Amount] [decimal](18, 2) NULL,
	[Gross_Amount] [decimal](18, 2) NULL,
	[DA] [decimal](18, 2) NULL,
	[HRA] [decimal](18, 2) NULL,
	[CCA] [decimal](18, 2) NULL,
	[OA] [decimal](18, 2) NULL,
	[PF] [decimal](18, 2) NULL,
	[EESI] [decimal](18, 2) NULL,
	[ESI] [decimal](18, 2) NULL,
	[PTAX] [decimal](18, 2) NULL,
	[TDS] [decimal](18, 2) NULL,
	[APGLI] [decimal](18, 2) NULL,
	[GIS] [decimal](18, 2) NULL,
	[HBA] [decimal](18, 2) NULL,
	[MCA] [decimal](18, 2) NULL,
	[BA] [decimal](18, 2) NULL,
	[LOAN] [decimal](18, 2) NULL,
	[OTHERS] [decimal](18, 2) NULL,
	[NET] [decimal](18, 2) NULL,
	[Criteria] [varchar](50) NULL,
	[Salary_Range] [varchar](50) NULL,
	[Creation_Company] [varchar](50) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
	[MONTHS] [varchar](50) NULL,
 CONSTRAINT [PK__HRMS_Emp__CA1FE4646D0D32F4] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Employee_Ot_Details]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Employee_Ot_Details](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Ot_Date] [datetime] NULL,
	[Emp_Code] [varchar](20) NULL,
	[Emp_Name] [varchar](50) NULL,
	[Job_Type] [varchar](30) NULL,
	[Department_Code] [varchar](20) NULL,
	[Department_Name] [varchar](50) NULL,
	[Start_Time] [datetime] NULL,
	[End_Time] [datetime] NULL,
	[Ot_Hrs] [time](7) NULL,
	[Note] [varchar](100) NULL,
	[Creation_Company] [varchar](50) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_HRMS_Employee_Ot_Details] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Employee_Work_Locations]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Employee_Work_Locations](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Location_Id] [varchar](20) NULL,
	[Location_Name] [varchar](50) NULL,
	[Creation_Company] [varchar](50) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_HRMS_Employee_Work_Locations] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Group_Master]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Group_Master](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Group_Code] [varchar](20) NULL,
	[Group_Name] [varchar](50) NULL,
	[Short_Name] [varchar](50) NULL,
	[Group_type] [varchar](50) NULL,
	[Applicable_Heads] [varchar](150) NULL,
	[Creation_Company] [varchar](50) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_HRMS_Group_Master] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Head_Master]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Head_Master](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Head_Code] [varchar](10) NULL,
	[Head_Name] [varchar](50) NULL,
	[Group_Code] [varchar](20) NULL,
	[Creation_Company] [varchar](200) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_HRMS_Head_Master] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Holiday_Master]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Holiday_Master](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Holiday_Code] [varchar](20) NULL,
	[Year] [varchar](10) NULL,
	[Date] [datetime] NULL,
	[Event_Name] [varchar](50) NULL,
	[Creation_Company] [varchar](10) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_HRMS_Holiday_Master] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_JobType_Master]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_JobType_Master](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Job_Id] [varchar](10) NULL,
	[Job_Name] [varchar](50) NULL,
	[Creation_Company] [varchar](50) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_HRMS_JobType_Master] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Leave_Allotment]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Leave_Allotment](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Emp_Code] [varchar](50) NULL,
	[Emp_Name] [varchar](50) NULL,
	[Select_Year] [varchar](30) NULL,
	[New_Opening_Balance] [varchar](30) NULL,
	[Leave_Name] [varchar](100) NULL,
	[Noof_Leavs] [decimal](18, 2) NULL,
	[Creation_Company] [varchar](200) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_HRMS_Leave_Allotment] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Leave_Application]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Leave_Application](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Leave_Code] [varchar](20) NULL,
	[Emp_Code] [varchar](50) NULL,
	[Emp_Name] [varchar](50) NULL,
	[Department] [varchar](30) NULL,
	[Designation] [varchar](30) NULL,
	[Reporting_Authority] [varchar](50) NULL,
	[TypeOfLeaveApple] [varchar](30) NULL,
	[Balances_Available] [decimal](18, 2) NULL,
	[LeaveFrom] [datetime] NULL,
	[LeaveTo] [datetime] NULL,
	[Noof_Days] [decimal](18, 2) NULL,
	[Leave_Status] [varchar](50) NULL,
	[Creation_Company] [varchar](200) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
	[Cerification_Upload_Path] [varchar](100) NULL,
 CONSTRAINT [PK_HRMS_Leave_Application] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Leave_Master]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Leave_Master](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Leave_Code] [varchar](20) NULL,
	[Leave_Name] [varchar](50) NULL,
	[Short_Name] [varchar](30) NULL,
	[Monthly] [varchar](30) NULL,
	[Monthly_Limit] [decimal](18, 2) NULL,
	[Encashment] [varchar](50) NULL,
	[Balnces_Transfer] [varchar](50) NULL,
	[Encashment_Balance] [decimal](18, 2) NULL,
	[Allotment] [varchar](50) NULL,
	[Noof_leaves_Per_Year] [decimal](18, 2) NULL,
	[Creation_Company] [varchar](200) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
	[Authority1] [varchar](50) NULL,
	[UserName1] [varchar](50) NULL,
	[Authority2] [varchar](50) NULL,
	[UserName2] [varchar](50) NULL,
	[TwoStage] [varchar](50) NULL,
	[Need_Certification] [varchar](20) NULL,
	[Period] [varchar](50) NULL,
	[No_Of_Times] [decimal](18, 2) NULL,
	[No_Of_Days] [decimal](18, 2) NULL,
	[Authority3] [varchar](50) NULL,
	[UserName3] [varchar](50) NULL,
 CONSTRAINT [PK_HRMS_Leave_Master] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Loan_Application]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Loan_Application](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Loan_Code] [varchar](50) NULL,
	[Emp_Code] [varchar](50) NULL,
	[Emp_Name] [varchar](50) NULL,
	[Department] [varchar](50) NULL,
	[Designation] [varchar](50) NULL,
	[Reporting_Authority] [varchar](50) NULL,
	[Type_Of_Loan_Apply] [varchar](50) NULL,
	[Basic_Amount] [decimal](18, 2) NULL,
	[Loan_amount_Required] [decimal](18, 2) NULL,
	[Eligibility_Amount] [decimal](18, 2) NULL,
	[Rate_Of_Interest] [decimal](18, 2) NULL,
	[tenure] [int] NULL,
	[Creation_Company] [varchar](50) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [varchar](50) NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [varchar](50) NULL,
	[Status] [varchar](50) NULL,
 CONSTRAINT [PK_HRMS_Loan_Application] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Loan_Master]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Loan_Master](
	[Sno] [int] IDENTITY(1,1) NOT NULL,
	[Loan_Code] [varchar](50) NULL,
	[Loan_Name] [varchar](50) NULL,
	[Short_Name] [varchar](50) NULL,
	[Creation_Company] [varchar](50) NULL,
	[Created_By] [varchar](50) NULL,
	[Allotment] [varchar](50) NULL,
	[Created_Date] [date] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [date] NULL,
	[Authority1] [varchar](50) NULL,
	[UserName1] [varchar](50) NULL,
	[Authority2] [varchar](50) NULL,
	[UserName2] [varchar](50) NULL,
	[Period] [varchar](50) NULL,
	[Need_Certification] [varchar](50) NULL,
	[No_Of_Times] [decimal](18, 0) NULL,
	[Authority3] [varchar](50) NULL,
	[UserName3] [varchar](50) NULL,
 CONSTRAINT [PK_HRMS_Loan_Master] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_OT_Criteria_Master]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_OT_Criteria_Master](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[OT_Code] [varchar](20) NULL,
	[Ot_Head_Names] [varchar](100) NULL,
	[Ot_Criteria] [varchar](100) NULL,
	[Ot_Rate] [decimal](18, 2) NULL,
	[Creation_Company] [varchar](200) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_pension_central]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_pension_central](
	[Sno] [int] IDENTITY(1,1) NOT NULL,
	[Status] [varchar](max) NULL,
	[PensionNo] [varchar](max) NULL,
	[empid] [varchar](250) NOT NULL,
	[empname] [varchar](max) NULL,
	[fathername] [varchar](max) NULL,
	[designation] [varchar](max) NULL,
	[department] [varchar](max) NULL,
	[DOB] [date] NULL,
	[retirment] [date] NULL,
	[seven] [varchar](max) NULL,
	[eighta] [varchar](max) NULL,
	[eightb] [varchar](max) NULL,
	[bankname] [varchar](max) NULL,
	[acno] [varchar](250) NULL,
	[acoffice] [varchar](max) NULL,
	[fathernaame] [varchar](max) NULL,
	[height] [varchar](max) NULL,
	[partonesix] [varchar](max) NULL,
	[partoneseven] [varchar](max) NULL,
	[partonesevena] [varchar](max) NULL,
	[partonesevenb] [varchar](max) NULL,
	[partonesevenc] [varchar](max) NULL,
	[partoneeight] [varchar](max) NULL,
	[servicebegining] [date] NULL,
	[endingservice] [date] NULL,
	[levena] [varchar](max) NULL,
	[levenb] [varchar](max) NULL,
	[levenc] [varchar](max) NULL,
	[levend] [varchar](max) NULL,
	[levene] [varchar](max) NULL,
	[levenf] [varchar](max) NULL,
	[leveng] [varchar](max) NULL,
	[levenh] [varchar](max) NULL,
	[leveni] [varchar](max) NULL,
	[levenj] [varchar](max) NULL,
	[levenk] [varchar](max) NULL,
	[tweleve] [varchar](max) NULL,
	[thirten] [varchar](max) NULL,
	[forteena] [varchar](max) NULL,
	[forteenb] [varchar](max) NULL,
	[forteenc] [varchar](max) NULL,
	[forteend] [varchar](max) NULL,
	[fifteena] [varchar](max) NULL,
	[fiteenbfrom] [varchar](max) NULL,
	[fifteenbto] [varchar](max) NULL,
	[fifteencfrom] [varchar](max) NULL,
	[fifteencto] [varchar](max) NULL,
	[sixteen] [varchar](max) NULL,
	[sixteenafrom] [varchar](max) NULL,
	[sixteenato] [varchar](max) NULL,
	[sixteenbfrom] [varchar](max) NULL,
	[sixteenbto] [varchar](max) NULL,
	[sixteenonefrom] [varchar](max) NULL,
	[sixteenoneto] [varchar](max) NULL,
	[sixteentwofrom] [varchar](max) NULL,
	[sixteentwoto] [varchar](max) NULL,
	[sixteenthreefrom] [varchar](max) NULL,
	[sixteenthreeto] [varchar](max) NULL,
	[sixteenforefrom] [varchar](max) NULL,
	[sixteenforeto] [varchar](max) NULL,
	[sixteenfivefrom] [varchar](max) NULL,
	[sixteenfiveto] [varchar](max) NULL,
	[sixteensixfrom] [varchar](max) NULL,
	[sixteensixto] [varchar](max) NULL,
	[sixteenconefrom] [varchar](max) NULL,
	[sixteenconetwo] [varchar](max) NULL,
	[sixteenctwofrom] [varchar](max) NULL,
	[sixteenctwotwo] [varchar](max) NULL,
	[sixteencthreefrom] [varchar](max) NULL,
	[sixteencthreetwo] [varchar](max) NULL,
	[sixteencforefrom] [varchar](max) NULL,
	[sixteencforetwo] [varchar](max) NULL,
	[sixteend] [varchar](max) NULL,
	[sixteene] [varchar](max) NULL,
	[seventeenfrom] [varchar](max) NULL,
	[seventeento] [varchar](max) NULL,
	[seventeenrateofpay] [money] NULL,
	[seventeenaamount] [money] NULL,
	[seventeenatotal] [money] NULL,
	[seventeenb] [varchar](max) NULL,
	[seventeenc] [varchar](max) NULL,
	[seventeend] [varchar](max) NULL,
	[seventeene] [varchar](max) NULL,
	[seventeeene] [varchar](max) NULL,
	[deathorretirement] [varchar](max) NULL,
	[twentytwoa] [varchar](max) NULL,
	[twentytwob] [varchar](max) NULL,
	[twentytwoc] [varchar](max) NULL,
	[twentythreea] [varchar](max) NULL,
	[twentythreeb] [varchar](max) NULL,
	[twentiforea] [varchar](max) NULL,
	[twentiforeb] [varchar](max) NULL,
	[twentiforec] [date] NULL,
	[twentyfive] [varchar](max) NULL,
	[twentysix] [varchar](max) NULL,
	[twentysevena] [varchar](max) NULL,
	[twentysevenb] [varchar](max) NULL,
	[twentysevenc] [varchar](max) NULL,
	[twentysevend] [varchar](max) NULL,
	[twentysevene] [varchar](max) NULL,
	[bankaddress] [varchar](max) NULL,
	[dateofreciptpensionpaper] [date] NULL,
	[lengthofQP] [varchar](max) NULL,
	[classOfPension] [varchar](max) NULL,
	[Amountofmonthlypension] [money] NULL,
	[DateofCommencement] [date] NULL,
	[parttwoone] [varchar](max) NULL,
	[parttwotwo] [varchar](max) NULL,
	[parttwothree] [date] NULL,
	[parttwofore] [date] NULL,
	[parttwodone] [varchar](max) NULL,
	[parttwodtwo] [varchar](max) NULL,
	[parttwodthree] [varchar](max) NULL,
	[parttwodfore] [varchar](max) NULL,
	[parttwoeone] [varchar](max) NULL,
	[parttwoetwo] [varchar](max) NULL,
	[parttwoethree] [varchar](max) NULL,
	[parttwothreee] [varchar](max) NULL,
	[dateofappointment] [date] NULL,
	[place] [varchar](max) NULL,
	[dateall] [date] NULL,
	[Creation_Company] [varchar](max) NULL,
	[Created_By] [varchar](max) NULL,
	[Created_Date] [date] NULL,
	[Modified_By] [varchar](max) NULL,
	[Modified_Date] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[empid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_pension_state]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_pension_state](
	[Sno] [int] IDENTITY(1,1) NOT NULL,
	[Status] [varchar](max) NULL,
	[pensionno] [varchar](max) NULL,
	[empid] [varchar](250) NOT NULL,
	[empname] [varchar](max) NULL,
	[postheld] [varchar](max) NULL,
	[NOA] [varchar](max) NULL,
	[permanentaddress] [varchar](max) NULL,
	[Addressafterretirment] [varchar](max) NULL,
	[copa] [varchar](max) NULL,
	[copb] [varchar](max) NULL,
	[nameoPDA] [varchar](max) NULL,
	[bankname] [varchar](max) NULL,
	[bankacno] [varchar](50) NULL,
	[Fathername] [varchar](max) NULL,
	[DOB] [date] NULL,
	[DOES] [date] NULL,
	[designation] [varchar](max) NULL,
	[eightb] [varchar](max) NULL,
	[eightc] [varchar](max) NULL,
	[totalservice] [varchar](max) NULL,
	[tena] [varchar](max) NULL,
	[tenb] [varchar](max) NULL,
	[tenc] [varchar](max) NULL,
	[tend] [varchar](max) NULL,
	[tene] [varchar](max) NULL,
	[NQS] [varchar](max) NULL,
	[weightage] [varchar](max) NULL,
	[TotalQSerive] [varchar](max) NULL,
	[LPDRAWN] [money] NULL,
	[servicepensiongravity] [money] NULL,
	[seventiena] [money] NULL,
	[seventienb] [money] NULL,
	[eightenafrom] [date] NULL,
	[eightenatilldeath] [varchar](max) NULL,
	[eightenbfrom] [date] NULL,
	[eightenbto] [date] NULL,
	[eightencfrom] [date] NULL,
	[eightencto] [varchar](max) NULL,
	[ninetenap] [money] NULL,
	[ninetenai] [money] NULL,
	[ninetenatotal] [money] NULL,
	[ninetenbp] [money] NULL,
	[ninetenbi] [money] NULL,
	[ninetenbtotal] [money] NULL,
	[ninetencp] [money] NULL,
	[ninetenci] [money] NULL,
	[ninetenctotal] [money] NULL,
	[ninetendp] [money] NULL,
	[ninetendi] [money] NULL,
	[ninetendtotal] [money] NULL,
	[ninetenep] [money] NULL,
	[ninetenei] [money] NULL,
	[ninetenetotal] [money] NULL,
	[ninetenfp] [money] NULL,
	[ninetenfi] [money] NULL,
	[ninetenftotal] [money] NULL,
	[ninetengp] [money] NULL,
	[ninetengi] [money] NULL,
	[ninetengtotal] [money] NULL,
	[ninetenhp] [money] NULL,
	[ninetenhi] [money] NULL,
	[ninetenhtotal] [money] NULL,
	[ninetenip] [money] NULL,
	[ninetenii] [money] NULL,
	[ninetenitotal] [money] NULL,
	[ninetenjp] [money] NULL,
	[ninetenji] [money] NULL,
	[ninetenjtotal] [money] NULL,
	[ninetenkp] [money] NULL,
	[ninetenki] [money] NULL,
	[ninetenktotal] [money] NULL,
	[ninetentotal] [money] NULL,
	[retiringgravity] [money] NULL,
	[commutating] [money] NULL,
	[nominiesname] [varchar](max) NULL,
	[personalidentifications] [varchar](max) NULL,
	[Place] [varchar](max) NULL,
	[datall] [date] NULL,
	[Creation_Company] [varchar](max) NULL,
	[Created_By] [varchar](max) NULL,
	[Created_Date] [date] NULL,
	[Modified_By] [varchar](max) NULL,
	[Modified_Date] [date] NULL,
 CONSTRAINT [PK__HRMS_pen__AF4CE865634EBE90] PRIMARY KEY CLUSTERED 
(
	[empid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_pension_state_familydetails]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_pension_state_familydetails](
	[Sno] [int] IDENTITY(1,1) NOT NULL,
	[empid] [varchar](250) NULL,
	[family_member] [varchar](max) NULL,
	[dateofbirth] [date] NULL,
	[Relation] [varchar](max) NULL,
	[maritial_status] [varchar](max) NULL,
	[employedORnot] [varchar](max) NULL,
	[Creation_Company] [varchar](max) NULL,
	[Created_By] [varchar](max) NULL,
	[Created_Date] [date] NULL,
	[Modified_By] [varchar](max) NULL,
	[Modified_Date] [date] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Recruit_Master]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Recruit_Master](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Recruit_Code] [varchar](20) NULL,
	[Recruit_Name] [varchar](50) NULL,
	[Creation_Company] [varchar](50) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Shift_Setup]    Script Date: 09/23/2017 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Shift_Setup](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Shift_Code] [varchar](20) NULL,
	[Shift_Name] [varchar](30) NULL,
	[Sign] [varchar](30) NULL,
	[A_Start_Time] [time](7) NULL,
	[A_Early_Ot_Limit] [time](7) NULL,
	[A_Invalid_Limit] [time](7) NULL,
	[A_Early_Limit] [time](7) NULL,
	[A_Delay_Limit] [time](7) NULL,
	[A_Late_Limit] [time](7) NULL,
	[A_Crossday] [varchar](20) NULL,
	[A_First_Punch] [varchar](10) NULL,
	[A_Off_Time] [time](7) NULL,
	[A_Leave_Early_Limit] [time](7) NULL,
	[A_Second_Early_Limit] [time](7) NULL,
	[A_Second_Delay_Limit] [time](7) NULL,
	[A_Second_Invalid_Limit] [time](7) NULL,
	[A_Ot_Delay_Limit] [time](7) NULL,
	[A_Second_Crossday] [varchar](20) NULL,
	[A_Second_Punch] [varchar](10) NULL,
	[First_Working_Hrs] [time](7) NULL,
	[B_Start_Time] [time](7) NULL,
	[B_Early_Ot_Limit] [time](7) NULL,
	[B_Invalid_Limit] [time](7) NULL,
	[B_Early_Limit] [time](7) NULL,
	[B_Delay_Limit] [time](7) NULL,
	[B_Late_Limit] [time](7) NULL,
	[B_Crossday] [varchar](20) NULL,
	[B_First_Punch] [varchar](10) NULL,
	[B_Off_Time] [time](7) NULL,
	[B_Leave_Early_Limit] [time](7) NULL,
	[B_Second_Early_Limit] [time](7) NULL,
	[B_Second_Delay_Limit] [time](7) NULL,
	[B_Second_Invalid_Limit] [time](7) NULL,
	[B_Ot_Delay_Limit] [time](7) NULL,
	[B_Second_Crossday] [varchar](20) NULL,
	[B_Second_Punch] [varchar](10) NULL,
	[Second_Working_Hrs] [time](7) NULL,
	[C_Start_Time] [time](7) NULL,
	[C_Early_Ot_Limit] [time](7) NULL,
	[C_Invalid_Limit] [time](7) NULL,
	[C_Early_Limit] [time](7) NULL,
	[C_Delay_Limit] [time](7) NULL,
	[C_Late_Limit] [time](7) NULL,
	[C_Crossday] [varchar](20) NULL,
	[C_First_Punch] [varchar](10) NULL,
	[C_Off_Time] [time](7) NULL,
	[C_Leave_Early_Limit] [time](7) NULL,
	[C_Second_Early_Limit] [time](7) NULL,
	[C_Second_Delay_Limit] [time](7) NULL,
	[C_Second_Invalid_Limit] [time](7) NULL,
	[C_Ot_Delay_Limit] [time](7) NULL,
	[C_Second_Crossday] [varchar](20) NULL,
	[C_Second_Punch] [varchar](10) NULL,
	[Third_Working_Hrs] [time](7) NULL,
	[Break_Before_Ot] [time](7) NULL,
	[Early_Limit] [time](7) NULL,
	[Ot_Delay_Limit] [time](7) NULL,
	[Max_Ot_Hrs] [time](7) NULL,
	[Creation_Company] [varchar](20) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_HRMS_Shift_Setup] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRMS_Year_Master]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRMS_Year_Master](
	[Sno] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Year_Id] [varchar](10) NOT NULL,
	[Year] [varchar](10) NULL,
	[Creation_Company] [varchar](10) NOT NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_HRMS_Year_Master] PRIMARY KEY CLUSTERED 
(
	[Year_Id] ASC,
	[Creation_Company] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[increments]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[increments](
	[sno] [int] IDENTITY(1,1) NOT NULL,
	[circle] [varchar](max) NULL,
	[distric] [varchar](max) NULL,
	[sub_distric] [varchar](max) NULL,
	[Employee_ID] [varchar](max) NULL,
	[Name_Of_Circle] [varchar](max) NULL,
	[Employee_Name] [varchar](max) NULL,
	[dob] [date] NULL,
	[Designation] [varchar](max) NULL,
	[Date_Of_entry_Service] [date] NULL,
	[Date_retirement_death] [date] NULL,
	[Due_date_increment] [date] NULL,
	[date_relesed_increment] [date] NULL,
	[due_leave] [varchar](max) NULL,
	[under_suspension] [varchar](max) NULL,
	[due_non_regulirisation] [varchar](max) NULL,
	[due_non_declaration_probation] [varchar](max) NULL,
	[due_punishment] [varchar](max) NULL,
	[eol] [varchar](max) NULL,
	[promotion] [varchar](max) NULL,
	[suspension] [varchar](max) NULL,
	[sgp] [varchar](max) NULL,
	[sapp_1a] [varchar](max) NULL,
	[sapp_1b] [varchar](max) NULL,
	[sapp_2] [varchar](max) NULL,
	[transfer] [varchar](max) NULL,
	[employe_promotion] [varchar](max) NULL,
	[appt] [varchar](max) NULL,
	[retired] [varchar](max) NULL,
	[death] [varchar](max) NULL,
	[created_date] [date] NULL,
	[created_by] [varchar](max) NULL,
	[modified_date] [date] NULL,
	[modified_by] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[pension_form]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pension_form](
	[sno] [int] IDENTITY(1,1) NOT NULL,
	[circle] [varchar](max) NULL,
	[distric] [varchar](max) NULL,
	[sub_distric] [varchar](max) NULL,
	[Employee_ID] [varchar](max) NULL,
	[Name_Of_Circle] [varchar](max) NULL,
	[Employee_Name] [varchar](max) NULL,
	[Designation] [varchar](max) NULL,
	[DOb] [date] NULL,
	[date_of_entry_service] [date] NULL,
	[date_of_receipt_pps] [date] NULL,
	[date_sending_auterisation] [date] NULL,
	[date_recipt_auterisation] [date] NULL,
	[reason_not_sending_autherisation] [varchar](max) NULL,
	[Date_retirement_death] [date] NULL,
	[no_papers_received] [int] NULL,
	[no_papers_balance] [int] NULL,
	[forwarding_pension_papers] [varchar](max) NULL,
	[receipt_pps] [varchar](max) NULL,
	[process_hod] [varchar](max) NULL,
	[sanction_authority] [varchar](max) NULL,
	[forward_ag] [varchar](max) NULL,
	[pvr_recd] [varchar](max) NULL,
	[autherization_recrd] [varchar](max) NULL,
	[date_of_forwarding_pension_to_ag] [date] NULL,
	[date_pvr] [date] NULL,
	[sanction_order_psa] [varchar](max) NULL,
	[autherization_issued] [varchar](max) NULL,
	[resaons_for_pending_non_pps] [varchar](max) NULL,
	[resaons_for_pending_non_finalisation] [varchar](max) NULL,
	[resaons_for_pending_non_furnishing] [varchar](max) NULL,
	[resaons_for_pending_non_furnishing_ndc] [varchar](max) NULL,
	[resaons_for_pending_non_finalisation_crs] [varchar](max) NULL,
	[pending_whome] [varchar](max) NULL,
	[due_to_disc_cases] [varchar](max) NULL,
	[due_to_acb_cases] [varchar](max) NULL,
	[vig] [varchar](max) NULL,
	[criminal_cases] [varchar](max) NULL,
	[non_finalisation_ndc] [varchar](max) NULL,
	[death_employee] [varchar](max) NULL,
	[forward_ag_autherization] [varchar](max) NULL,
	[autherisation_recd] [varchar](max) NULL,
	[created_date] [date] NULL,
	[created_by] [varchar](max) NULL,
	[modified_date] [date] NULL,
	[modified_by] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[range_mst]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[range_mst](
	[range_cd] [nvarchar](6) NOT NULL,
	[range_name] [nvarchar](30) NULL,
	[range_flag] [nvarchar](5) NULL,
	[c_date] [datetime] NULL,
	[m_date] [datetime] NULL,
	[div_cd] [nvarchar](4) NULL,
	[created] [nvarchar](50) NULL,
	[old_range_cd] [nvarchar](6) NULL,
 CONSTRAINT [range_mst_New_pkey] PRIMARY KEY NONCLUSTERED 
(
	[range_cd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SCM_Administrator_UserPermission]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SCM_Administrator_UserPermission](
	[SNo] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Administrator] [varchar](50) NULL,
	[Masters] [varchar](50) NULL,
	[Transactions] [varchar](50) NULL,
	[Reports] [varchar](50) NULL,
	[Login_Id] [varchar](20) NULL,
	[User_Type] [varchar](10) NULL,
	[Creation_Company] [varchar](50) NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
 CONSTRAINT [PK_SCM_Administrator_UserPermission] PRIMARY KEY CLUSTERED 
(
	[SNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[section_mst]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[section_mst](
	[section_cd] [nvarchar](8) NOT NULL,
	[section_name] [nvarchar](40) NOT NULL,
	[section_flag] [nvarchar](5) NULL,
	[c_date] [datetime] NULL,
	[m_date] [datetime] NULL,
	[range_cd] [nvarchar](6) NULL,
	[old_section_cd] [nvarchar](8) NULL,
	[created_by] [nvarchar](50) NULL,
 CONSTRAINT [section_mst_new_pkey] PRIMARY KEY NONCLUSTERED 
(
	[section_cd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblFiles]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblFiles](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[ContentType] [nvarchar](250) NULL,
	[Data] [varbinary](max) NULL,
	[Fromuser] [varchar](50) NULL,
	[Touser] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[dist_div_map] ADD  DEFAULT (getdate()) FOR [c_date]
GO
ALTER TABLE [dbo].[dist_div_map] ADD  DEFAULT (getdate()) FOR [m_date]
GO
ALTER TABLE [dbo].[beat_mst]  WITH NOCHECK ADD  CONSTRAINT [beat_mst_new_section_cd_fkey] FOREIGN KEY([section_cd])
REFERENCES [dbo].[section_mst] ([section_cd])
GO
ALTER TABLE [dbo].[beat_mst] NOCHECK CONSTRAINT [beat_mst_new_section_cd_fkey]
GO
ALTER TABLE [dbo].[HRMS_pension_state_familydetails]  WITH CHECK ADD  CONSTRAINT [FK__HRMS_pens__empid__662B2B3B] FOREIGN KEY([empid])
REFERENCES [dbo].[HRMS_pension_state] ([empid])
GO
ALTER TABLE [dbo].[HRMS_pension_state_familydetails] CHECK CONSTRAINT [FK__HRMS_pens__empid__662B2B3B]
GO
/****** Object:  StoredProcedure [dbo].[sp_beats]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
   Create procedure [dbo].[sp_beats](@parameter nvarchar(50))
 as
 begin
 
  select * FROM [dbo].[beat_mst] where section_cd like @parameter + '%'
  end
GO
/****** Object:  StoredProcedure [dbo].[sp_Divides]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE procedure [dbo].[sp_Divides](@parameter nvarchar(50))
 as
 begin
 
  select * FROM [dbo].[range_mst] where div_cd like @parameter + '%'
  end
GO
/****** Object:  StoredProcedure [dbo].[sp_Emp_Personal_Official_Department_Data]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[sp_Emp_Personal_Official_Department_Data]
@Department_Code varchar(20),
@Creation_Company varchar(50)
AS
BEGIN
select  p.Emp_Code,(p.Emp_First_Name+''+p.Emp_Middle_Name+''+p.Emp_Last_Name) as 'Name',p.Gender,          
p.Emp_Status,f.Employee_Grade,f.Designation,f.Department,f.Location,f.Date_Of_Join,ed.Department_Name from [dbo].[HRMS_Employee_Master_PersonalDetails] p            
left join [dbo].[HRMS_Employee_Master_Official_Information] f            
on f.Emp_Code=p.Emp_Code            
left join [dbo].[HRMS_Employee_Master_Professional_Experience] Pf             
on pf.Emp_Code=f.Emp_Code left join [dbo].[HRMS_Department_Master] ed on f.Department = ed.Department_Code           
where  p.Creation_Company=@creation_Company and not p.Emp_Status in('Resigned','Retired') and f.Department=@Department_Code


END



GO
/****** Object:  StoredProcedure [dbo].[sp_Get_All_Deductions_Page_Data]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_Get_All_Deductions_Page_Data]  
@PF_Code varchar(20),  
@Creation_Company varchar(20)  
AS  
BEGIN  
SELECT PF.*,IT.*,PT.*,ot.Deduct_On_Other_Heads,OT.Define_Range_Other_HeadName  
,OT.Define_Range_Criteria,OT.Define_Amount as 'OtherAmount',OT.Salary_Range FROM [dbo].[HRMS_Deductions_PensionFund_Details] PF  
LEFT JOIN [dbo].[HRMS_Deductions_IncomeTax_Details] IT  
on PF.PF_Code=IT.PF_Code left join [dbo].[HRMS_Deductions_ProfessionalTax_PT_Details] PT  
on PT.PF_Code=IT.PF_Code left join [dbo].[HRMS_Deductions_Other_Details] OT on   
OT.PF_Code=PT.PF_Code  
  
where PF.PF_Code=@PF_Code and PF.Creation_Company=@Creation_Company  
  
END  



GO
/****** Object:  StoredProcedure [dbo].[Sp_Get_User_RegistrationBasedOnLoginId]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[Sp_Get_User_RegistrationBasedOnLoginId]            
            
@Creation_Company varchar(10),      
@Login_Id varchar(20) ,     
@User_Type varchar(10)     
As            
Begin            
           
  if @User_Type='1'    
  BEGIN     
  Select*from Administrator_UserRegistration where Creation_Company=@Creation_Company  and ( Login_Id=@Login_Id or User_Type='0')    
  END    
  else    
  BEGIN    
  Select*from Administrator_UserRegistration where Creation_Company=@Creation_Company and Login_Id=@Login_Id    
  End    
END




GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Count_HRMS_Employee_Master_Attendance]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Sp_HRMS_Count_HRMS_Employee_Master_Attendance]        
        
@Emp_Code VARCHAR(20),        
 
@Creation_Company VARCHAR(50)

AS        
BEGIN        

  select isnull(count(In_Time),0) as 'IN_Time Count',isnull(count(Out_Time),0) 'Out_Time Count',isnull(count(In_Time),0)-isnull(count(Out_Time),0) as 'IN_OUT Count'
 from 
 HRMS_Employee_Master_Attendance 
 where Creation_Company=@Creation_Company and Emp_Code=@Emp_Code and CONVERT(varchar(10),Created_Date,105)=CONVERT(varchar(10),GETDATE(),105)
 
END





GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Employee_Count_Details]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[Sp_HRMS_Employee_Count_Details]
@Creation_Company varchar(50)
AS
BEGIN
SELECT Count([Emp_Code]) AS 'Total_EMP', ISNULL(MALE.MALE_Count,0) as 'MALE_Count',
ISNULL( FEMALE.FEMALE_Count,0) AS 'FEMALE_Count',
ISNULL( NEW.New_Count,0) AS 'New_Count',
ISNULL(Resigned.Resigned,0) AS 'Resigned'
 FROM [dbo].[HRMS_Employee_Master_PersonalDetails] EMP

LEFT JOIN 

(SELECT ISNULL(COUNT([Emp_Code]),0) AS 'MALE_Count',Creation_Company  FROM [dbo].[HRMS_Employee_Master_PersonalDetails]

WHERE [Gender]='Male'

GROUP BY Creation_Company) MALE

ON EMP.Creation_Company=MALE.Creation_Company

LEFT JOIN

(SELECT ISNULL(COUNT([Emp_Code]),0) AS 'FEMALE_Count',Creation_Company   FROM [dbo].[HRMS_Employee_Master_PersonalDetails]

WHERE [Gender]='Female'
GROUP BY Creation_Company) FEMALE

ON EMP.Creation_Company=FEMALE.Creation_Company

LEFT JOIN 
(SELECT ISNULL(COUNT([Emp_Code]),0) AS 'New_Count',Creation_Company   
FROM [dbo].[HRMS_Employee_Master_PersonalDetails]
WHERE  [Created_Date] BETWEEN GETDATE()-90 AND GETDATE()
GROUP BY Creation_Company) NEW
ON EMP.Creation_Company=NEW.Creation_Company

LEFT JOIN

(SELECT ISNULL(COUNT([Emp_Code]),0) AS 'Resigned',Creation_Company   FROM [dbo].[HRMS_Employee_Master_PersonalDetails]

WHERE Emp_Status='Resigned'
GROUP BY Creation_Company) Resigned

ON EMP.Creation_Company=Resigned.Creation_Company

WHERE EMP.Creation_Company=@Creation_Company

GROUP BY  MALE.MALE_Count, FEMALE.FEMALE_Count,NEW.New_Count,Resigned.Resigned

END




GO
/****** Object:  StoredProcedure [dbo].[sp_HRMS_Employee_Personal_Official_GridData]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_HRMS_Employee_Personal_Official_GridData]         
      
@creation_Company varchar(50)        
AS          
BEGIN          
select  p.Emp_Code,(p.Emp_First_Name+''+p.Emp_Middle_Name+''+p.Emp_Last_Name) as 'Name',p.Gender,        
p.Emp_Status,f.Employee_Grade,f.Designation,f.Department,f.Location,f.Date_Of_Join,ed.Department_Name from [dbo].[HRMS_Employee_Master_PersonalDetails] p          
left join [dbo].[HRMS_Employee_Master_Official_Information] f          
on f.Emp_Code=p.Emp_Code          
left join [dbo].[HRMS_Employee_Master_Professional_Experience] Pf           
on pf.Emp_Code=f.Emp_Code left join [dbo].[HRMS_Department_Master] ed on f.Department = ed.Department_Code         
where  p.Creation_Company=@creation_Company and not p.Emp_Status in('Resigned','Retired')      
END




GO
/****** Object:  StoredProcedure [dbo].[sp_HRMS_Employee_Personal_Official_Search]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_HRMS_Employee_Personal_Official_Search]       
@search varchar(30) ,      
@creation_Company varchar(50)        
AS          
BEGIN          
select  p.Emp_Code,(p.Emp_First_Name+''+p.Emp_Middle_Name+''+p.Emp_Last_Name) as 'Name',p.Gender,        
p.Emp_Status,f.Employee_Grade,f.Designation,f.Date_Of_Join,f.Department,Dept.Department_Name from [dbo].[HRMS_Employee_Master_PersonalDetails] p          
left join [dbo].[HRMS_Employee_Master_Official_Information] f          
on f.Emp_Code=p.Emp_Code          
left join [dbo].[HRMS_Employee_Master_Professional_Experience] Pf           
on pf.Emp_Code=f.Emp_Code left join [dbo].[HRMS_Department_Master] Dept on f.Department=Dept.Department_Code
where p.Creation_Company=@creation_Company and   
p.Emp_Code like @search or           
p.Emp_First_Name like @search or           
p.Emp_Last_Name like @search or          
p.Emp_Middle_Name like @search or            
p.Gender like @search or           
p.Emp_Status like @search or           
f.Employee_Grade like @search or          
f.Designation like @search  or    
f.Date_Of_Join like @search or    
f.Department like @search and not p.Emp_Status in('Resigned','Retired')      
END  




GO
/****** Object:  StoredProcedure [dbo].[sp_HRMS_Employee_Personal_OTSettings_GridData]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_HRMS_Employee_Personal_OTSettings_GridData]             
@Emp_Code varchar(20),   
@creation_Company varchar(50)        
AS              
BEGIN              
select  p.Emp_Code,(p.Emp_First_Name+''+p.Emp_Middle_Name+''+p.Emp_Last_Name) as 'Name',p.Gender,            
p.Emp_Status,f.Employee_Grade,f.Designation,f.Department,f.Location,f.Date_Of_Join,ed.Department_Name from [dbo].[HRMS_Employee_Master_PersonalDetails] p              
left join [dbo].[HRMS_Employee_Master_Official_Information] f              
on f.Emp_Code=p.Emp_Code              
left join [dbo].[HRMS_Employee_Master_Professional_Experience] Pf               
on pf.Emp_Code=f.Emp_Code left join [dbo].[HRMS_Department_Master] ed on f.Department = ed.Department_Code             
where  p.Creation_Company=@creation_Company and p.Emp_Code=@Emp_Code and not p.Emp_Status in('Resigned','Retired')          
END 



GO
/****** Object:  StoredProcedure [dbo].[sp_HRMS_Employee_Shift_Setup_Rpt]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_HRMS_Employee_Shift_Setup_Rpt]
@Department_Code varchar(20),
@Creation_Company varchar(50),
@Emp_Code varchar(20),
@Emp_Status varchar(20),
@Parameter int
AS
BEGIN
IF(@Parameter=1)    
BEGIN  
select  p.Emp_Code,(p.Emp_First_Name+''+p.Emp_Middle_Name+''+p.Emp_Last_Name) as 'Name',        
f.Designation,ed.Department_Name from [dbo].[HRMS_Employee_Master_PersonalDetails] p            
left join [dbo].[HRMS_Employee_Master_Official_Information] f            
on f.Emp_Code=p.Emp_Code            
left join [dbo].[HRMS_Employee_Master_Professional_Experience] Pf             
on pf.Emp_Code=f.Emp_Code left join [dbo].[HRMS_Department_Master] ed on f.Department = ed.Department_Code 

where p.Creation_Company= @Creation_Company and p.Emp_Status=@Emp_Status
END

IF(@Parameter=2)    
BEGIN  
select  p.Emp_Code,(p.Emp_First_Name+''+p.Emp_Middle_Name+''+p.Emp_Last_Name) as 'Name',        
f.Designation,ed.Department_Name from [dbo].[HRMS_Employee_Master_PersonalDetails] p            
left join [dbo].[HRMS_Employee_Master_Official_Information] f            
on f.Emp_Code=p.Emp_Code            
left join [dbo].[HRMS_Employee_Master_Professional_Experience] Pf             
on pf.Emp_Code=f.Emp_Code left join [dbo].[HRMS_Department_Master] ed on f.Department = ed.Department_Code 

where p.Creation_Company= @Creation_Company and ed.Department_Code=@Department_Code
END


IF(@Parameter=3)    
BEGIN
select  p.Emp_Code,(p.Emp_First_Name+''+p.Emp_Middle_Name+''+p.Emp_Last_Name) as 'Name',        
f.Designation,ed.Department_Name from [dbo].[HRMS_Employee_Master_PersonalDetails] p            
left join [dbo].[HRMS_Employee_Master_Official_Information] f            
on f.Emp_Code=p.Emp_Code            
left join [dbo].[HRMS_Employee_Master_Professional_Experience] Pf             
on pf.Emp_Code=f.Emp_Code left join [dbo].[HRMS_Department_Master] ed on f.Department = ed.Department_Code 

where p.Creation_Company= @Creation_Company and p.Emp_Code=@Emp_Code
END
IF(@Parameter=4)    
BEGIN  
select  p.Emp_Code,(p.Emp_First_Name+''+p.Emp_Middle_Name+''+p.Emp_Last_Name) as 'Name',        
f.Designation,ed.Department_Name from [dbo].[HRMS_Employee_Master_PersonalDetails] p            
left join [dbo].[HRMS_Employee_Master_Official_Information] f            
on f.Emp_Code=p.Emp_Code            
left join [dbo].[HRMS_Employee_Master_Professional_Experience] Pf             
on pf.Emp_Code=f.Emp_Code left join [dbo].[HRMS_Department_Master] ed on f.Department = ed.Department_Code 

where p.Creation_Company= @Creation_Company and p.Emp_Code=@Emp_Code and ed.Department_Code=@Department_Code
END
END



GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Emppoyee_Leave_Details]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[Sp_HRMS_Emppoyee_Leave_Details]
@Year varchar(10),
@Emp_Code varchar(20),
@Department varchar(50),
@Creation_Company varchar(50),
@Parameter int
AS
BEGIN

IF(@Parameter=1)
BEGIN
SELECT HLA.Emp_Code,HLA.Emp_Name, HLM.Leave_Code,HLA.Select_Year,HLA.Noof_Leavs,
HLM.Leave_Name,HLM.Short_Name ,ISNULL(HLAPP.Applied_Leves,0) AS 'Used_Leaves', HLAPP.Department, 
HLA.Noof_Leavs-ISNULL(HLAPP.Applied_Leves,0) AS 'Balance_Leaves'
FROM HRMS_Leave_Allotment HLA
LEFT JOIN [dbo].[HRMS_Leave_Master] HLM
ON HLA.Leave_Name=HLM.Short_Name AND HLA.Creation_Company=HLM.Creation_Company
LEFT JOIN 
(SELECT ISNULL(SUM(Noof_Days),0) AS 'Applied_Leves' ,TypeOfLeaveApple,Emp_Code,Creation_Company,   Department,
DATEPART(yyyy,LeaveFrom) AS Leave_Year
FROM  HRMS_Leave_Application
WHERE Leave_Status='Approval'
GROUP BY TypeOfLeaveApple ,Emp_Code,Creation_Company,Department,LeaveFrom) HLAPP
ON HLA.Emp_Code= HLAPP.Emp_Code AND  HLM.Leave_Code=HLAPP.TypeOfLeaveApple 
AND HLA.Creation_Company=HLAPP.Creation_Company AND HLA.Select_Year=HLAPP.Leave_Year

WHERE HLA.Select_Year=@Year AND HLA.Creation_Company=@Creation_Company

END

IF(@Parameter=2)
BEGIN
SELECT HLA.Emp_Code,HLA.Emp_Name, HLM.Leave_Code,HLA.Select_Year,HLA.Noof_Leavs,
HLM.Leave_Name,HLM.Short_Name ,ISNULL(HLAPP.Applied_Leves,0) AS 'Used_Leaves', HLAPP.Department, 
HLA.Noof_Leavs-ISNULL(HLAPP.Applied_Leves,0) AS 'Balance_Leaves'
FROM HRMS_Leave_Allotment HLA
LEFT JOIN [dbo].[HRMS_Leave_Master] HLM
ON HLA.Leave_Name=HLM.Short_Name AND HLA.Creation_Company=HLM.Creation_Company
LEFT JOIN 
(SELECT ISNULL(SUM(Noof_Days),0) AS 'Applied_Leves' ,TypeOfLeaveApple,Emp_Code,Creation_Company,   Department,
DATEPART(yyyy,LeaveFrom) AS Leave_Year
FROM  HRMS_Leave_Application
WHERE Leave_Status='Approval'
GROUP BY TypeOfLeaveApple ,Emp_Code,Creation_Company,Department,LeaveFrom) HLAPP
ON HLA.Emp_Code= HLAPP.Emp_Code AND  HLM.Leave_Code=HLAPP.TypeOfLeaveApple 
AND HLA.Creation_Company=HLAPP.Creation_Company AND HLA.Select_Year=HLAPP.Leave_Year

WHERE HLA.Select_Year=@Year AND HLA.Creation_Company=@Creation_Company AND HLA.Emp_Code=@Emp_Code

END

IF(@Parameter=3)
BEGIN
SELECT HLA.Emp_Code,HLA.Emp_Name, HLM.Leave_Code,HLA.Select_Year,HLA.Noof_Leavs,
HLM.Leave_Name,HLM.Short_Name ,ISNULL(HLAPP.Applied_Leves,0) AS 'Used_Leaves', HLAPP.Department, 
HLA.Noof_Leavs-ISNULL(HLAPP.Applied_Leves,0) AS 'Balance_Leaves'
FROM HRMS_Leave_Allotment HLA
LEFT JOIN [dbo].[HRMS_Leave_Master] HLM
ON HLA.Leave_Name=HLM.Short_Name AND HLA.Creation_Company=HLM.Creation_Company
LEFT JOIN 
(SELECT ISNULL(SUM(Noof_Days),0) AS 'Applied_Leves' ,TypeOfLeaveApple,Emp_Code,Creation_Company,   Department,
DATEPART(yyyy,LeaveFrom) AS Leave_Year
FROM  HRMS_Leave_Application
WHERE Leave_Status='Approval'
GROUP BY TypeOfLeaveApple ,Emp_Code,Creation_Company,Department,LeaveFrom) HLAPP
ON HLA.Emp_Code= HLAPP.Emp_Code AND  HLM.Leave_Code=HLAPP.TypeOfLeaveApple 
AND HLA.Creation_Company=HLAPP.Creation_Company AND HLA.Select_Year=HLAPP.Leave_Year

WHERE HLA.Select_Year=@Year AND HLA.Creation_Company=@Creation_Company AND HLAPP.Department=@Department

END

END




GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Additions_Master]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Additions_Master]        
        
@Addition_Code VARCHAR(20),        
@Head_Name VARCHAR(50), 
@Short_Name	varchar(30)	,
@Caluculation	varchar(30)	,
@Per_Of_Basic	decimal(18, 2)	,
@Ot_Head_Names	varchar(100)	,
@Ot_Day_Working_Hrs	decimal(18, 2),
@Ot_Night_Working_Hrs	decimal(18, 2),
@Ot_On_Weakly_Off	decimal(18, 2),
@Ot_On_Holiday	decimal(18, 2)	,
@Creation_Company VARCHAR(50),        
@Created_By VARCHAR(50),        
@Created_Date datetime,        
@Modified_By VARCHAR(50),        
@Modified_Date datetime,        
        
@Parameter int         
         
        
        
        
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
    -- Insert statements for procedure here        
 IF(@Parameter=1)        
         
 BEGIN        
 INSERT INTO [dbo].[HRMS_Additions_Master]    
 (        
           
  Addition_Code,Head_Name,Short_Name,Caluculation,Per_Of_Basic,Ot_Head_Names,
Ot_Day_Working_Hrs,Ot_Night_Working_Hrs	,Ot_On_Weakly_Off,Ot_On_Holiday	,

    Creation_Company,Created_By,Created_Date        
          
 )        
 VALUES        
 (        
          
         
    [dbo].[Fn_autoincrement_HRMS_Additions_Master] (@Creation_Company) ,        
 @Head_Name ,@Short_Name,@Caluculation	,@Per_Of_Basic	,@Ot_Head_Names,
@Ot_Day_Working_Hrs	,@Ot_Night_Working_Hrs,@Ot_On_Weakly_Off	,@Ot_On_Holiday	,
   @Creation_Company,@Created_By,@Created_Date        
          
        
 )        
 END        
  -- Update statements for procedure here        
 IF(@Parameter=2)        
         
 BEGIN        
 Update [dbo].[HRMS_Additions_Master]  SET         
         
         
      Head_Name=@Head_Name ,Short_Name=@Short_Name,Caluculation=@Caluculation,
	  Per_Of_Basic=@Per_Of_Basic,Ot_Head_Names=@Ot_Head_Names,
Ot_Day_Working_Hrs=@Ot_Day_Working_Hrs,Ot_Night_Working_Hrs=@Ot_Night_Working_Hrs,
Ot_On_Weakly_Off=@Ot_On_Weakly_Off,Ot_On_Holiday=@Ot_On_Holiday	,
 
  Modified_By=@Modified_By,Modified_Date=@Modified_Date        
 Where         
         Addition_Code=@Addition_Code and Creation_Company=@Creation_Company        
 END        
         
 ---- Select Statement For Procedure Here        
 IF(@Parameter=3)        
         
 BEGIN        
 Select * from [dbo].[HRMS_Additions_Master]      
 Where         
 Creation_Company=@Creation_Company        
         
 END        
         
END





GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Bank_Master]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Bank_Master]        
        
@Bank_Id VARCHAR(20),        
@Bank_Name VARCHAR(50),      
@Creation_Company VARCHAR(50),        
@Created_By VARCHAR(50),        
@Created_Date datetime,        
@Modified_By VARCHAR(50),        
@Modified_Date datetime,        
        
@Parameter int         
         
        
        
        
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
    -- Insert statements for procedure here        
 IF(@Parameter=1)        
         
 BEGIN        
 INSERT INTO [dbo].[HRMS_Bank_Master]    
 (        
           
   Bank_Id,Bank_Name,  
    Creation_Company,Created_By,Created_Date        
          
 )        
 VALUES        
 (        
          
         
    [dbo].[Fn_autoincrement_HRMS_Bank_Master] (@Creation_Company) ,        
 @Bank_Name,     
   @Creation_Company,@Created_By,@Created_Date        
          
        
 )        
 END        
  -- Update statements for procedure here        
 IF(@Parameter=2)        
         
 BEGIN        
 Update [dbo].[HRMS_Bank_Master]  SET         
         
         
      Bank_Name=@Bank_Name,
  Modified_By=@Modified_By,Modified_Date=@Modified_Date        
 Where         
         Bank_Id=@Bank_Id and Creation_Company=@Creation_Company        
 END        
         
 ---- Select Statement For Procedure Here        
 IF(@Parameter=3)        
         
 BEGIN        
 Select * from [dbo].[HRMS_Bank_Master]      
 Where         
 Creation_Company=@Creation_Company        
         
 END        
         
END




GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_City_Master]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_City_Master]        
        
@City_Code VARCHAR(20),        
@City_Name VARCHAR(50),      
@State_Name VARCHAR(50),
@Country    VARCHAR(50),
@Creation_Company VARCHAR(50),        
@Created_By VARCHAR(50),        
@Created_Date datetime,        
@Modified_By VARCHAR(50),        
@Modified_Date datetime,        
        
@Parameter int         
         
        
        
        
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
    -- Insert statements for procedure here        
 IF(@Parameter=1)        
         
 BEGIN        
 INSERT INTO [dbo].[HRMS_City_Master]      
 (        
           
   City_Code,City_Name,State_Name,       
    Creation_Company,Created_By,Created_Date,Country       
          
 )        
 VALUES        
 (        
          
         
    [dbo].[Fn_autoincrement_HRMS_City_Master] (@Creation_Company) ,        
  @City_Name,@State_Name,     
   @Creation_Company,@Created_By,@Created_Date ,@Country       
          
        
 )        
 END        
  -- Update statements for procedure here        
 IF(@Parameter=2)        
         
 BEGIN        
 Update [dbo].[HRMS_City_Master] SET         
         
         
     City_Name=@City_Name,State_Name=@State_Name,  
  Modified_By=@Modified_By,Modified_Date=@Modified_Date ,Country=@Country       
 Where         
         City_Code=@City_Code and Creation_Company=@Creation_Company        
 END        
         
 ---- Select Statement For Procedure Here        
 IF(@Parameter=3)        
         
 BEGIN        
 Select * from [dbo].[HRMS_City_Master]      
 Where         
 Creation_Company=@Creation_Company        
         
 END        
         
END




GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Deductions_IncomeTax_Details]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Deductions_IncomeTax_Details]        
@PF_Code	varchar(20),
@Define_Range_From decimal(18,2),
@Define_Range_To	decimal(18,2),
@Define_Amount	decimal(18,2),
@Deduct_On_Heads	varchar(50),
@Creation_Company VARCHAR(50),        
@Created_By VARCHAR(50),        
@Created_Date datetime,        
@Modified_By VARCHAR(50),        
@Modified_Date datetime,        
        
@Parameter int         
         
        
        
        
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
    -- Insert statements for procedure here        
 IF(@Parameter=1)        
         
 BEGIN  
 DECLARE @PF varchar(10)       
   select @PF=MAX(PF_Code) from HRMS_Deductions_PensionFund_Details --order by SNo desc 
       
 INSERT INTO [dbo].[HRMS_Deductions_IncomeTax_Details]    
 (        
           
   PF_Code,Define_Range_From,Define_Range_To,
   Define_Amount,Deduct_On_Heads,Creation_Company,Created_By,Created_Date        
          
 )        
 VALUES        
 (  @PF,      
   @Define_Range_From,@Define_Range_To,@Define_Amount,
  @Deduct_On_Heads,@Creation_Company,@Created_By,@Created_Date        
  )        
 END        
  -- Update statements for procedure here        
 IF(@Parameter=2)        
         
 BEGIN        
 Update [dbo].[HRMS_Deductions_IncomeTax_Details]  SET         
         
	Define_Range_From=@Define_Range_From,Define_Range_To=@Define_Range_To,
   Define_Amount=@Define_Amount,Deduct_On_Heads=@Deduct_On_Heads,
   Modified_By=@Modified_By,Modified_Date=@Modified_Date        
 Where         
         PF_Code=@PF_Code and Creation_Company=@Creation_Company        
 END        
         
 ---- Select Statement For Procedure Here        
 IF(@Parameter=3)        
         
 BEGIN        
 Select * from [dbo].[HRMS_Deductions_IncomeTax_Details]      
 Where         
 Creation_Company=@Creation_Company        
         
 END        
         
END




GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Deductions_Other_Details]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Deductions_Other_Details]        
@PF_Code	varchar(20),
@Define_Range_Other_HeadName	varchar(50),
@Define_Range_Criteria	varchar(50),
@Define_Amount	decimal(18,2),
@Deduct_On_Other_Heads	varchar(50),
@Salary_Range varchar(20),
@Creation_Company VARCHAR(50),        
@Created_By VARCHAR(50),        
@Created_Date datetime,        
@Modified_By VARCHAR(50),        
@Modified_Date datetime,        
        
@Parameter int         
         
        
        
        
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
    -- Insert statements for procedure here        
 IF(@Parameter=1)        
         
 BEGIN   
  DECLARE @PF varchar(10)       
   select @PF=MAX(PF_Code) from HRMS_Deductions_PensionFund_Details --order by SNo desc 

      
 INSERT INTO [dbo].[HRMS_Deductions_Other_Details]    
 (        
           
   PF_Code,Define_Range_Other_HeadName,Define_Range_Criteria,Define_Amount,
Deduct_On_Other_Heads,Salary_Range,Creation_Company,Created_By,Created_Date        
          
 )        
 VALUES        
 (  @PF,      
   @Define_Range_Other_HeadName,@Define_Range_Criteria,@Define_Amount,
@Deduct_On_Other_Heads,@Salary_Range,@Creation_Company,@Created_By,@Created_Date        
  )        
 END        
  -- Update statements for procedure here        
 IF(@Parameter=2)        
         
 BEGIN        
 Update [dbo].[HRMS_Deductions_Other_Details]  SET         
         
	Define_Range_Other_HeadName=@Define_Range_Other_HeadName,
	Define_Range_Criteria=@Define_Range_Criteria,Define_Amount=@Define_Amount,
Deduct_On_Other_Heads=@Deduct_On_Other_Heads,Salary_Range=@Salary_Range,Modified_By=@Modified_By,Modified_Date=@Modified_Date        
 Where         
         PF_Code=@PF_Code and Creation_Company=@Creation_Company        
 END        
         
 ---- Select Statement For Procedure Here        
 IF(@Parameter=3)        
         
 BEGIN        
 Select * from [dbo].[HRMS_Deductions_Other_Details]      
 Where         
 Creation_Company=@Creation_Company        
         
 END        
         
END





GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Deductions_PensionFund_Details]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Deductions_PensionFund_Details]

@Short_Name	varchar(30),
@Limit_For_Emp_Contribution	decimal(18, 2),
@Percentage	decimal(18, 2),
@Deduct_On	varchar(100),

@Creation_Company VARCHAR(50),        
@Created_By VARCHAR(50),        
@Created_Date datetime,        
@Modified_By VARCHAR(50),        
@Modified_Date datetime,                
@Parameter int         

        
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
    -- Insert statements for procedure here        
 IF(@Parameter=1)        
         
 BEGIN        
 INSERT INTO [dbo].[HRMS_Deductions_PensionFund_Details]
 (  

   Short_Name,Limit_For_Emp_Contribution,Percentage,Deduct_On,
    Creation_Company,Created_By,Created_Date                  
 )        
 VALUES        
 (       
    @Short_Name,@Limit_For_Emp_Contribution,@Percentage,@Deduct_On,
    @Creation_Company,@Created_By,@Created_Date        

 )        
 END        
  -- Update statements for procedure here        
 IF(@Parameter=2)        
         
 BEGIN        
 Update [dbo].[HRMS_Deductions_PensionFund_Details]  SET         
         
     Short_Name=@Short_Name,Limit_For_Emp_Contribution=@Limit_For_Emp_Contribution,Percentage=@Percentage,
	 Deduct_On=@Deduct_On,
  Modified_By=@Modified_By,Modified_Date=@Modified_Date        
 Where         
         Short_Name=@Short_Name and Creation_Company=@Creation_Company        
 END        
         
 ---- Select Statement For Procedure Here        
 IF(@Parameter=3)        
         
 BEGIN        
 Select * from [dbo].[HRMS_Deductions_PensionFund_Details] 
 Where         
 Creation_Company=@Creation_Company        
         
 END        
         
END





GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Deductions_PensionFund_Details_1]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Deductions_PensionFund_Details_1]        
@PF_Code	varchar(20),
@PF_Percentage decimal(18,2),
@PF_Emp_Contribution decimal(18,2),
@PF_Deduct	varchar(150),
@ESI_Percentage	decimal(18,2),
@ESI_Deduct	varchar(150),
@Creation_Company VARCHAR(50),        
@Created_By VARCHAR(50),        
@Created_Date datetime,        
@Modified_By VARCHAR(50),        
@Modified_Date datetime,        
        
@Parameter int         
         
        
        
        
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
    -- Insert statements for procedure here        
 IF(@Parameter=1)        
         
 BEGIN        
 INSERT INTO [dbo].[HRMS_Deductions_PensionFund_Details]    
 (        
           
   PF_Code,PF_Percentage,
PF_Emp_Contribution,
PF_Deduct,
ESI_Percentage,
ESI_Deduct,Creation_Company,Created_By,Created_Date        
          
 )        
 VALUES        
 (  [dbo].[Fn_autoincrement_HRMS_Deductions_PensionFund_Details] (@Creation_Company),      
   @PF_Percentage,@PF_Emp_Contribution,@PF_Deduct,@ESI_Percentage,
@ESI_Deduct,@Creation_Company,@Created_By,@Created_Date        
  )        
 END        
  -- Update statements for procedure here        
 IF(@Parameter=2)        
         
 BEGIN        
 Update [dbo].[HRMS_Deductions_PensionFund_Details]  SET         
         
	PF_Percentage=@PF_Percentage,PF_Emp_Contribution=@PF_Emp_Contribution,
PF_Deduct=@PF_Deduct,ESI_Percentage=@ESI_Percentage,ESI_Deduct=@ESI_Deduct,
   Modified_By=@Modified_By,Modified_Date=@Modified_Date        
 Where         
         PF_Code=@PF_Code and Creation_Company=@Creation_Company        
 END        
         
 ---- Select Statement For Procedure Here        
 IF(@Parameter=3)        
         
 BEGIN        
 Select * from [dbo].[HRMS_Deductions_PensionFund_Details]      
 Where         
 Creation_Company=@Creation_Company        
         
 END        
         
END




GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Deductions_ProfessionalTax_PT_Details]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Deductions_ProfessionalTax_PT_Details]        
@PF_Code	varchar(20),
@Define_Range_PT_From	decimal(18,2),
@Define_Range_PT_To	decimal(18,2),
@Define_PT_Amount	decimal(18,2),
@Deduct_On_PT_Heads	varchar(50),
@Creation_Company VARCHAR(50),        
@Created_By VARCHAR(50),        
@Created_Date datetime,        
@Modified_By VARCHAR(50),        
@Modified_Date datetime,        
        
@Parameter int         
         
        
        
        
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
    -- Insert statements for procedure here        
 IF(@Parameter=1)        
         
 BEGIN  
 DECLARE @PF varchar(10)       
   select @PF=MAX(PF_Code) from HRMS_Deductions_PensionFund_Details --order by SNo desc 
       
 INSERT INTO [dbo].[HRMS_Deductions_ProfessionalTax_PT_Details]    
 (        
           
   PF_Code,Define_Range_PT_From,Define_Range_PT_To,Define_PT_Amount,
Deduct_On_PT_Heads,Creation_Company,Created_By,Created_Date        
          
 )        
 VALUES        
 (  @PF,      
   @Define_Range_PT_From,@Define_Range_PT_To,@Define_PT_Amount,
@Deduct_On_PT_Heads,@Creation_Company,@Created_By,@Created_Date        
  )        
 END        
  -- Update statements for procedure here        
 IF(@Parameter=2)        
         
 BEGIN        
 Update [dbo].[HRMS_Deductions_ProfessionalTax_PT_Details]  SET         
         
	Define_Range_PT_From=@Define_Range_PT_From,Define_Range_PT_To=@Define_Range_PT_To
	,Define_PT_Amount=@Define_PT_Amount,Deduct_On_PT_Heads=@Deduct_On_PT_Heads,
   Modified_By=@Modified_By,Modified_Date=@Modified_Date        
 Where         
         PF_Code=@PF_Code and Creation_Company=@Creation_Company        
 END        
         
 ---- Select Statement For Procedure Here        
 IF(@Parameter=3)        
         
 BEGIN        
 Select * from [dbo].[HRMS_Deductions_ProfessionalTax_PT_Details]      
 Where         
 Creation_Company=@Creation_Company        
         
 END        
         
END




GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Department_Master]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Department_Master]        
        
@Department_Code VARCHAR(20),        
@Department_Name VARCHAR(50),      
@Creation_Company VARCHAR(50),        
@Created_By VARCHAR(50),        
@Created_Date datetime,        
@Modified_By VARCHAR(50),        
@Modified_Date datetime,        
        
@Parameter int         
         
        
        
        
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
    -- Insert statements for procedure here        
 IF(@Parameter=1)        
         
 BEGIN        
 INSERT INTO [dbo].[HRMS_Department_Master]    
 (        
           
   Department_Code,Department_Name,  
    Creation_Company,Created_By,Created_Date        
          
 )        
 VALUES        
 (        
          
         @Department_Code,
    ---[dbo].[Fn_autoincrement_HRMS_Department_Master] (@Creation_Company) ,        
 @Department_Name,     
   @Creation_Company,@Created_By,@Created_Date        
          
        
 )        
 END        
  -- Update statements for procedure here        
 IF(@Parameter=2)        
         
 BEGIN        
 Update [dbo].[HRMS_Department_Master]  SET         
         
         
      Department_Name=@Department_Name,
  Modified_By=@Modified_By,Modified_Date=@Modified_Date        
 Where         
         Department_Code=@Department_Code and Creation_Company=@Creation_Company        
 END        
         
 ---- Select Statement For Procedure Here        
 IF(@Parameter=3)        
         
 BEGIN        
 Select * from [dbo].[HRMS_Department_Master]      
 Where         
 Creation_Company=@Creation_Company        
         
 END        
         
END





GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Leave_Details]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Leave_Details]

@Emp_Code	varchar(20),
@Emp_Name	varchar(50),
@Job_Type varchar(30),
@Leave_From	datetime,
@Leave_To	datetime,
@Leave_Hrs	time(7),
@Leave_Code	varchar(20),
@Leave_Name	varchar(20),
@System_Hrs	time(7),
@Working_Hrs time(7),
@BT_Reason	varchar(200),
@Bt_Expance	varchar(30),
@Bt_Place	varchar(100),
@Creation_Company	varchar(50),
@Created_By	varchar(50),
@Created_Date	datetime,
@Modified_By	varchar(50),
@Modified_Date	datetime,
@Parameter int 


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	if(@Parameter=1)

	BEGIN

	INSERT INTO HRMS_Employee_Leave_Details
	(
		Emp_Code,Emp_Name,Job_Type,Leave_From,Leave_To,Leave_Hrs,Leave_Code,Leave_Name,System_Hrs,Working_Hrs,
		BT_Reason,Bt_Expance,Bt_Place,Creation_Company,Created_By,Created_Date	
	)
	VALUES
	(
	    @Emp_Code,@Emp_Name,@Job_Type,@Leave_From,@Leave_To,@Leave_Hrs,@Leave_Code,@Leave_Name,@System_Hrs,@Working_Hrs,
		@BT_Reason,@Bt_Expance,@Bt_Place,@Creation_Company,@Created_By,@Created_Date

	)

	END

	-----Update Table Based On Employee Code

	if(@Parameter=2)

	BEGIN

	UPDATE HRMS_Employee_Leave_Details SET 
	Emp_Name=@Emp_Name,Job_Type=@Job_Type,Leave_From=@Leave_From,Leave_To=@Leave_To,Leave_Hrs=@Leave_Hrs,Leave_Code=@Leave_Code,
	Leave_Name=@Leave_Name,System_Hrs=@System_Hrs,Working_Hrs=@Working_Hrs,
		BT_Reason=@BT_Reason,Bt_Expance=@Bt_Expance,Bt_Place=@Bt_Place,Modified_By=@Modified_By,Modified_Date=@Modified_Date

		WHERE Emp_Code=@Emp_Code and Creation_Company=@Creation_Company


	END


	------ Select The Data Based On Company Wise

	if(@Parameter=3)
	BEGIN

	Select *from HRMS_Employee_Leave_Details WHERE Creation_Company=@Creation_Company

	END


END




GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_Attendance]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_Attendance]        
        
@Emp_Code VARCHAR(20),        
@Emp_Name VARCHAR(100),  
@In_Time datetime,
@Out_Time datetime,    
@Creation_Company VARCHAR(50),        
@Created_By VARCHAR(50),        
@Created_Date datetime,        
@Modified_By VARCHAR(50),        
@Modified_Date datetime,        
        
@Parameter int         
         
        
        
        
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
    -- Insert statements for procedure here        
 IF(@Parameter=1)        
         
 BEGIN        
 INSERT INTO [dbo].[HRMS_Employee_Master_Attendance]    
 (        
           
  Emp_Code ,        
Emp_Name ,  
In_Time ,
    Creation_Company,Created_By,Created_Date        
          
 )        
 VALUES        
 (        
          
         @Emp_Code ,        
@Emp_Name ,  
@In_Time ,

   @Creation_Company,@Created_By,@Created_Date        
          
        
 )        
 END        
  -- Update statements for procedure here        
 IF(@Parameter=2)        
         
 BEGIN        
 Update [dbo].[HRMS_Employee_Master_Attendance]  SET         
         
         
         

Out_Time=@Out_Time,
  Modified_By=@Modified_By,Modified_Date=@Modified_Date        
 Where         
          Emp_Code=@Emp_Code  and Creation_Company=@Creation_Company        
 END        
         
 ---- Select Statement For Procedure Here        
 IF(@Parameter=3)        
         
 BEGIN        
 Select * from [dbo].[HRMS_Employee_Master_Attendance]      
 Where         
 Creation_Company=@Creation_Company        
         
 END        
         
END





GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_ContactDetails]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_ContactDetails]        
        
@Emp_Code varchar(20),
@Present_Address1 varchar(100),
@Present_Address2 varchar(100),
@Present_City varchar(50),
@Present_State	varchar(50),
@Present_Pincode varchar(10),
@Present_Country varchar(50),
@Permanent_Address1	varchar(100),
@Permanent_Address2	varchar(100),
@Permanent_City	varchar(50),
@Permanent_State varchar(50),
@Permanent_Pincode	varchar(10),
@Permanent_Country	varchar(50),
@Permanent_Mobile	varchar(13),
@Permanent_Email	varchar(30),
@Alternative_Email	varchar(30),
@Skype_Id	varchar(30),
@Linkedin_Id varchar(30),

@Creation_Company VARCHAR(50),        
@Created_By VARCHAR(50),        
@Created_Date datetime,        
@Modified_By VARCHAR(50),        
@Modified_Date datetime,        
        
@Parameter int         
         
        
        
        
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
    -- Insert statements for procedure here        
 IF(@Parameter=1)        
         
 BEGIN        
 INSERT INTO [dbo].[HRMS_Employee_Master_ContactDetails] (        
           
  Emp_Code,Present_Address1,
Present_Address2,
Present_City,
Present_State,
Present_Pincode,
Present_Country,
Permanent_Address1,
Permanent_Address2,
Permanent_City,
Permanent_State,
Permanent_Pincode,
Permanent_Country,
Permanent_Mobile,
Permanent_Email,
Alternative_Email,
Skype_Id,
Linkedin_Id,
    Creation_Company,Created_By,Created_Date        
          
 )        
 VALUES        
 (        
          
         
    @Emp_Code,@Present_Address1,
@Present_Address2,
@Present_City,
@Present_State,
@Present_Pincode,
@Present_Country,
@Permanent_Address1,
@Permanent_Address2,
@Permanent_City,
@Permanent_State,
@Permanent_Pincode,
@Permanent_Country,
@Permanent_Mobile,
@Permanent_Email,
@Alternative_Email,
@Skype_Id,
@Linkedin_Id,
   @Creation_Company,@Created_By,@Created_Date        
          
        
 )        
 END        
  -- Update statements for procedure here        
 IF(@Parameter=2)        
         
 BEGIN        
 Update [dbo].[HRMS_Employee_Master_ContactDetails]  SET         
         
         
     Present_Address1=@Present_Address1,
Present_Address2=@Present_Address2,
Present_City=@Present_City,
Present_State=@Present_State,
Present_Pincode=@Present_Pincode,
Present_Country=@Present_Country,
Permanent_Address1=@Permanent_Address1,
Permanent_Address2=@Permanent_Address2,
Permanent_City=@Permanent_City,
Permanent_State=@Permanent_State,
Permanent_Pincode=@Permanent_Pincode,
Permanent_Country=@Permanent_Country,
Permanent_Mobile=@Permanent_Mobile,
Permanent_Email=@Permanent_Email,
Alternative_Email=@Alternative_Email,
Skype_Id=@Skype_Id,
Linkedin_Id=@Linkedin_Id,
  Modified_By=@Modified_By,Modified_Date=@Modified_Date        
 Where         
         Emp_Code=@Emp_Code and Creation_Company=@Creation_Company        
 END        
         
 ---- Select Statement For Procedure Here        
 IF(@Parameter=3)        
         
 BEGIN        
 Select * from [dbo].[HRMS_Employee_Master_ContactDetails]  
 Where         
 Creation_Company=@Creation_Company        
         
 END        
         
END





GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_Educational_Certification_Details]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_Educational_Certification_Details]        
        
@Emp_Code varchar(20),
@Qualification_Certification	varchar(50),
@Institute_University	varchar(100),
@Year	varchar(10),
@Percentage	decimal(18, 2),
@Attach_File	varchar(50),

@Creation_Company VARCHAR(50),        
@Created_By VARCHAR(50),        
@Created_Date datetime,        
@Modified_By VARCHAR(50),        
@Modified_Date datetime,        
        
@Parameter int         
         
        
        
        
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
    -- Insert statements for procedure here        
 IF(@Parameter=1)        
         
 BEGIN        
 INSERT INTO [dbo].[HRMS_Employee_Master_Educational_Certification_Details]
           (
  Emp_Code, Qualification_Certification,Institute_University,Year,
Percentage,Attach_File,
    Creation_Company,Created_By,Created_Date        
          
 )        
 VALUES        
 (       
    @Emp_Code,@Qualification_Certification,@Institute_University,@Year,@Percentage,@Attach_File,
   @Creation_Company,@Created_By,@Created_Date
 )        
 END        
  -- Update statements for procedure here        
 IF(@Parameter=2)        
         
 BEGIN        
 Update [dbo].[HRMS_Employee_Master_Educational_Certification_Details]  SET         
         
     Qualification_Certification=@Qualification_Certification,Institute_University=@Institute_University,Year=@Year,
Percentage=@Percentage,Attach_File=@Attach_File,    
       Modified_By=@Modified_By,Modified_Date=@Modified_Date        
 Where         
         Emp_Code=@Emp_Code and Creation_Company=@Creation_Company        
 END        
         
 ---- Select Statement For Procedure Here        
 IF(@Parameter=3)        
         
 BEGIN        
 Select * from [dbo].[HRMS_Employee_Master_Educational_Certification_Details]
 Where         
 Creation_Company=@Creation_Company        
         
 END        
         
END





GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_Educational_Skill_Details]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_Educational_Skill_Details]        
        
@Emp_Code varchar(20),
@Skill_Name	varchar(50),
@Skill_Level	varchar(10),
@Description	varchar(100),

@Creation_Company VARCHAR(50),        
@Created_By VARCHAR(50),        
@Created_Date datetime,        
@Modified_By VARCHAR(50),        
@Modified_Date datetime,        
        
@Parameter int         
         
        
        
        
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
    -- Insert statements for procedure here        
 IF(@Parameter=1)        
         
 BEGIN        
 INSERT INTO [dbo].[HRMS_Employee_Master_Educational_Skill_Details]
           (
  Emp_Code, Skill_Name,Skill_Level,Description,
    Creation_Company,Created_By,Created_Date        
          
 )        
 VALUES        
 (       
    @Emp_Code,@Skill_Name,@Skill_Level,@Description,
   @Creation_Company,@Created_By,@Created_Date
 )        
 END        
  -- Update statements for procedure here        
 IF(@Parameter=2)        
         
 BEGIN        
 Update [dbo].[HRMS_Employee_Master_Educational_Skill_Details]
  SET         
     Skill_Name=@Skill_Name,Skill_Level=@Skill_Level,Description=@Description,
       Modified_By=@Modified_By,Modified_Date=@Modified_Date        
 Where         
         Emp_Code=@Emp_Code and Creation_Company=@Creation_Company        
 END        
         
 ---- Select Statement For Procedure Here        
 IF(@Parameter=3)        
         
 BEGIN        
 Select * from [dbo].[HRMS_Employee_Master_Educational_Skill_Details]
 Where         
 Creation_Company=@Creation_Company        
         
 END        
         
END





GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_Employee_Deductions]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_Employee_Deductions]
        
@Emp_Code varchar(20),
@Head	varchar(50),
@Amount	decimal(18, 2),
@Frequency	varchar(50),

@Creation_Company VARCHAR(50),        
@Created_By VARCHAR(50),        
@Created_Date datetime,        
@Modified_By VARCHAR(50),        
@Modified_Date datetime,        
        
@Parameter int    
        
        
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
    -- Insert statements for procedure here        
 IF(@Parameter=1)        
         
 BEGIN        
 INSERT INTO [dbo].[HRMS_Employee_Master_Employee_Deductions]
 (
     Emp_Code,Head,Amount,Frequency,
	 
	 Creation_Company,Created_By,Created_Date       
          
 )        
 VALUES        
 (       
    @Emp_Code,@Head,@Amount,@Frequency,
    @Creation_Company,@Created_By,@Created_Date
 )        
 END        
  -- Update statements for procedure here        
 IF(@Parameter=2)        
         
 BEGIN        
 Update [dbo].[HRMS_Employee_Master_Employee_Deductions]  SET
         
       Head=@Head,Amount=@Amount,Frequency=@Frequency,
       Modified_By=@Modified_By,Modified_Date=@Modified_Date        
 Where         
         Emp_Code=@Emp_Code and Creation_Company=@Creation_Company        
 END        
         
 ---- Select Statement For Procedure Here        
 IF(@Parameter=3)        
         
 BEGIN    
     
 Select * from [dbo].[HRMS_Employee_Master_Employee_Deductions]
	 Where         
	 Creation_Company=@Creation_Company        
         
 END        
         
END





GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_FamilyInformation]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_FamilyInformation] 
        
@Emp_Code varchar(20),

@Member_Name	varchar(50),
@Member_Relation	varchar(30),
@Member_DOB	datetime,
@Member_Occupation	varchar(50),
@Member_ContactNo	varchar(15),
@Member_Dependent	varchar(10),
@Member_Nominee	varchar(10),
@Member_Emergency_Contact	varchar(50),

@Creation_Company VARCHAR(50),        
@Created_By VARCHAR(50),        
@Created_Date datetime,        
@Modified_By VARCHAR(50),        
@Modified_Date datetime,        
        
@Parameter int     
         
        
        
        
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
    -- Insert statements for procedure here        
 IF(@Parameter=1)        
         
 BEGIN        
 INSERT INTO [dbo].[HRMS_Employee_Master_FamilyInformation]
 (
    Emp_Code,Member_Name,Member_Relation, Member_DOB,Member_Occupation,
    Member_ContactNo,Member_Dependent,Member_Nominee,Member_Emergency_Contact,

    Creation_Company,Created_By,Created_Date        
          
 )        
 VALUES        
 (       
    @Emp_Code,  @Member_Name,@Member_Relation, @Member_DOB,@Member_Occupation,
    @Member_ContactNo,@Member_Dependent,@Member_Nominee,@Member_Emergency_Contact,

   @Creation_Company,@Created_By,@Created_Date
 )        
 END        
  -- Update statements for procedure here        
 IF(@Parameter=2)        
         
 BEGIN        
 Update [dbo].[HRMS_Employee_Master_FamilyInformation]
  SET         
    Member_Name=@Member_Name,Member_Relation=@Member_Relation, Member_DOB=@Member_DOB,
	Member_Occupation=@Member_Occupation, Member_ContactNo=@Member_ContactNo,Member_Dependent=@Member_Dependent,
	Member_Nominee=@Member_Nominee,Member_Emergency_Contact=@Member_Emergency_Contact,
       Modified_By=@Modified_By,Modified_Date=@Modified_Date        
 Where         
         Emp_Code=@Emp_Code and Creation_Company=@Creation_Company        
 END        
         
 ---- Select Statement For Procedure Here        
 IF(@Parameter=3)        
         
 BEGIN        
 Select * from [dbo].[HRMS_Employee_Master_FamilyInformation]
 Where         
 Creation_Company=@Creation_Company        
         
 END        
         
END





GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_Key_PerformanceDetails]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_Key_PerformanceDetails] 
        
@Emp_Code varchar(20),

@Objective_Goal	varchar(50),
@Measurement	varchar(50),
@Monitoring_Frequency	varchar(50),
@Current_Level	varchar(50),

@Creation_Company VARCHAR(50),        
@Created_By VARCHAR(50),        
@Created_Date datetime,        
@Modified_By VARCHAR(50),        
@Modified_Date datetime,        
        
@Parameter int     
         
        
        
        
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
    -- Insert statements for procedure here        
 IF(@Parameter=1)        
         
 BEGIN        
 INSERT INTO [dbo].[HRMS_Employee_Master_Key_PerformanceDetails]
 (
    Emp_Code,
	Objective_Goal,Measurement,Monitoring_Frequency,
Current_Level,

    Creation_Company,Created_By,Created_Date        
          
 )        
 VALUES        
 (       
    @Emp_Code, @Objective_Goal,@Measurement,@Monitoring_Frequency,
    @Current_Level,

   @Creation_Company,@Created_By,@Created_Date
 )        
 END        
  -- Update statements for procedure here        
 IF(@Parameter=2)        
         
 BEGIN        
 Update [dbo].[HRMS_Employee_Master_Key_PerformanceDetails]
  SET
  Objective_Goal=@Objective_Goal,Measurement=@Measurement,Monitoring_Frequency=@Monitoring_Frequency,
Current_Level =@Current_Level     ,
       Modified_By=@Modified_By,Modified_Date=@Modified_Date        
 Where         
         Emp_Code=@Emp_Code and Creation_Company=@Creation_Company        
 END        
         
 ---- Select Statement For Procedure Here        
 IF(@Parameter=3)        
         
 BEGIN        
 Select * from [dbo].[HRMS_Employee_Master_Key_PerformanceDetails]
 Where         
 Creation_Company=@Creation_Company        
         
 END        
         
END





GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_Leaves_Information]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_Leaves_Information] 
        
@Emp_Code varchar(20),

@Leave_Name	varchar(50),
@Eligible_To_Aail	varchar(10),
@Opening_Bal	decimal(18, 2),
@This_Year_Allotment	decimal(18, 2),
@Availed_As_On_Date	decimal(18, 2),
@Encashed_This_Year	decimal(18, 2),
@Balance_Available	decimal(18, 2),

@Creation_Company VARCHAR(50),        
@Created_By VARCHAR(50),        
@Created_Date datetime,        
@Modified_By VARCHAR(50),        
@Modified_Date datetime,        
        
@Parameter int     
         
        
        
        
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
    -- Insert statements for procedure here        
 IF(@Parameter=1)        
         
 BEGIN        
 INSERT INTO [dbo].[HRMS_Employee_Master_Leaves_Information]
           (
  Emp_Code, Leave_Name,Eligible_To_Aail,Opening_Bal,This_Year_Allotment,Availed_As_On_Date,
Encashed_This_Year,Balance_Available,
    Creation_Company,Created_By,Created_Date        
          
 )        
 VALUES        
 (       
    @Emp_Code, @Leave_Name,@Eligible_To_Aail,@Opening_Bal,@This_Year_Allotment,@Availed_As_On_Date,
@Encashed_This_Year,@Balance_Available,

   @Creation_Company,@Created_By,@Created_Date
 )        
 END        
  -- Update statements for procedure here        
 IF(@Parameter=2)        
         
 BEGIN        
 Update [dbo].[HRMS_Employee_Master_Leaves_Information]
  SET         
      Leave_Name=@Leave_Name, Eligible_To_Aail=@Eligible_To_Aail,Opening_Bal=@Opening_Bal,
	  This_Year_Allotment=@This_Year_Allotment,Availed_As_On_Date=@Availed_As_On_Date,
Encashed_This_Year=@Encashed_This_Year,Balance_Available=@Balance_Available,
       Modified_By=@Modified_By,Modified_Date=@Modified_Date        
 Where         
         Emp_Code=@Emp_Code and Creation_Company=@Creation_Company        
 END        
         
 ---- Select Statement For Procedure Here        
 IF(@Parameter=3)        
         
 BEGIN        
 Select * from [dbo].[HRMS_Employee_Master_Leaves_Information]
 Where         
 Creation_Company=@Creation_Company        
         
 END        
         
END





GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_Loans_Information]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_Loans_Information] 
        
@Emp_Code varchar(20),

@Loan_Type	varchar(50),
@Loan_Amount	decimal(18, 2),
@Deduct_Per_Month	decimal(18, 2),
@Balance_As_On_Date	decimal(18, 2),

@Creation_Company VARCHAR(50),        
@Created_By VARCHAR(50),        
@Created_Date datetime,        
@Modified_By VARCHAR(50),        
@Modified_Date datetime,        
        
@Parameter int     
         
        
        
        
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
    -- Insert statements for procedure here        
 IF(@Parameter=1)        
         
 BEGIN        
 INSERT INTO [dbo].[HRMS_Employee_Master_Loans_Information]
 (
  Emp_Code, Loan_Type,Loan_Amount,Deduct_Per_Month,Balance_As_On_Date,
    Creation_Company,Created_By,Created_Date        
          
 )        
 VALUES        
 (       
    @Emp_Code, @Loan_Type,@Loan_Amount,@Deduct_Per_Month,@Balance_As_On_Date,
   @Creation_Company,@Created_By,@Created_Date
 )        
 END        
  -- Update statements for procedure here        
 IF(@Parameter=2)        
         
 BEGIN        
 Update [dbo].[HRMS_Employee_Master_Loans_Information]
  SET         
     Loan_Type=@Loan_Type,Loan_Amount=@Loan_Amount,Deduct_Per_Month=@Deduct_Per_Month,Balance_As_On_Date=@Balance_As_On_Date,
       Modified_By=@Modified_By,Modified_Date=@Modified_Date        
 Where         
         Emp_Code=@Emp_Code and Creation_Company=@Creation_Company        
 END        
         
 ---- Select Statement For Procedure Here        
 IF(@Parameter=3)        
         
 BEGIN        
 Select * from [dbo].[HRMS_Employee_Master_Loans_Information]
 Where         
 Creation_Company=@Creation_Company        
         
 END        
         
END





GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_Official_Information]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_Official_Information]  
          
@Emp_Code varchar(20),  
@Date_Of_Join datetime,  
@Designation varchar(30),  
@Employee_Grade varchar(30),  
@Department varchar(30),  
@Location varchar(50),
@Reporting_Authority varchar(50),  
@Working_Shifts varchar(10),  
@PF varchar(10),  
@PF_Code varchar(20),  
@ESI varchar(10),  
@ESI_Code varchar(20),  
@ESI_Dispensary varchar(50),  
@Profisional_Tax varchar(10),  
@TDS varchar(10),  
@Quarter_Accommodation varchar(10),  
@Four_Wheeler varchar(10),  
@Two_Wheeler varchar(10),  
@Laptop varchar(10),  
@Mobile_Phone varchar(10),  
@Other_Facilities varchar(50),  
  
@Creation_Company VARCHAR(50),          
@Created_By VARCHAR(50),          
@Created_Date datetime,          
@Modified_By VARCHAR(50),          
@Modified_Date datetime,
@Work_Type varchar(50),
@Recruit  varchar(50),      
          
@Parameter int           
           
          
          
          
AS          
BEGIN          
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON;          
          
    -- Insert statements for procedure here          
 IF(@Parameter=1)          
           
 BEGIN          
 INSERT INTO [dbo].[HRMS_Employee_Master_Official_Information]  
 (  
     Emp_Code, Date_Of_Join,Designation,Employee_Grade,Department,Location,  
 Reporting_Authority,Working_Shifts,PF,PF_Code,ESI,  
 ESI_Code,ESI_Dispensary,Profisional_Tax,TDS,Quarter_Accommodation,  
 Four_Wheeler,Two_Wheeler,Laptop,Mobile_Phone,Other_Facilities,  
  Work_Type,Recruit,
    Creation_Company,Created_By,Created_Date         
            
 )          
 VALUES          
 (         
    @Emp_Code,@Date_Of_Join,@Designation,@Employee_Grade,@Department,@Location,  
 @Reporting_Authority,@Working_Shifts,@PF,@PF_Code,@ESI,  
 @ESI_Code,@ESI_Dispensary,@Profisional_Tax,@TDS,@Quarter_Accommodation,  
 @Four_Wheeler,@Two_Wheeler,@Laptop,@Mobile_Phone,@Other_Facilities,  
 @Work_Type,@Recruit,@Creation_Company,@Created_By,@Created_Date  
 )          
 END          
  -- Update statements for procedure here          
 IF(@Parameter=2)          
           
 BEGIN          
 Update [dbo].[HRMS_Employee_Master_Official_Information]  SET  
           
     Date_Of_Join=@Date_Of_Join,Designation=@Designation,Employee_Grade=@Employee_Grade,Department=@Department,  
	 Location=@Location,
 Reporting_Authority=@Reporting_Authority,Working_Shifts=@Working_Shifts,PF=@PF,PF_Code=@PF_Code,ESI=@ESI,  
 ESI_Code=@ESI_Code,ESI_Dispensary=@ESI_Dispensary,Profisional_Tax=@Profisional_Tax,TDS=@TDS,  
 Quarter_Accommodation=@Quarter_Accommodation,Four_Wheeler=@Four_Wheeler,Two_Wheeler=@Two_Wheeler,  
 Laptop=@Laptop,Mobile_Phone=@Mobile_Phone,Other_Facilities=@Other_Facilities,Work_Type=@Work_Type,  
      @Recruit=Recruit,Modified_By=@Modified_By,Modified_Date=@Modified_Date          
 Where           
         Emp_Code=@Emp_Code and Creation_Company=@Creation_Company          
 END          
           
 ---- Select Statement For Procedure Here          
 IF(@Parameter=3)          
           
 BEGIN      
       
 Select * from [dbo].[HRMS_Employee_Master_Official_Information]  
  Where           
  Creation_Company=@Creation_Company          
           
 END          
           
END




GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_PersonalDetails]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_PersonalDetails]                
                
@Emp_Code VARCHAR(20),       
@Emp_Image VARCHAR(200),               
@Emp_First_Name VARCHAR(50),              
@Emp_Middle_Name varchar(50),        
@Emp_Last_Name varchar(50),        
@Emp_Roll varchar(50),     
@Emp_Status varchar(20),    
@User_Name varchar(50),        
@Password varchar(30),        
@Emp_DOB datetime,         
@Emp_Age decimal(18,0),         
@Date_Of_Anniversary datetime,        
@Gender varchar(20),        
@Blood_Group varchar(20),        
@Blood_Donor varchar(10),        
@Marital_Status varchar(20),        
@Nationality varchar(20),        
@Religion varchar(50),        
@Languages_Known varchar(80),        
@Hobbies varchar(200),        
@Curricular_Activities varchar(50),        
        
@Driving_Licenece_Number varchar(20),        
@Licence_Type varchar(20),        
@Licence_Exp_Date datetime,        
@Licence_Attachment varchar(50),        
@Aadhar_Number varchar(20),        
@Aadhar_Attachment varchar(20),        
@Pan_Number varchar(20),        
@Pan_Attachment varchar(20),        
@Passport_Number varchar(20),        
@Issued_Date datetime,        
@Exp_Date datetime,        
@Passport_Attachment varchar(20),        
@Bank_Name varchar(50),        
@Account_Number varchar(20),        
@Health_Insurance_Poly_No varchar(20),        
@Sum_Insured varchar(20),        
@Renewal_Date datetime,        
        
----Contact Details        
        
@Present_Address1 varchar(100),        
@Present_Address2 varchar(100),        
@Present_City varchar(50),        
@Present_State varchar(50),        
@Present_Pincode varchar(10),        
@Present_Country varchar(50),        
@Permanent_Address1 varchar(100),        
@Permanent_Address2 varchar(100),
@PresentAsPrev  varchar(10),        
@Permanent_City varchar(50),        
@Permanent_State varchar(50),        
@Permanent_Pincode varchar(10),        
@Permanent_Country varchar(50),        
@Permanent_Mobile varchar(13),        
@Permanent_Email varchar(30),        
@Alternative_Email varchar(30),        
@Skype_Id varchar(30),        
@Linkedin_Id varchar(30),        
        
----End Of Contact Details        
@Creation_Company VARCHAR(50),                
@Created_By VARCHAR(50),                
@Created_Date datetime,                
@Modified_By VARCHAR(50),                
@Modified_Date datetime,                
    
   --Additonal Data  
@Resigned_Date datetime,  
@Educate varchar(50),  
@Telephone varchar(20),         
@Parameter int        
                
AS                
BEGIN                
 -- SET NOCOUNT ON added to prevent extra result sets from                
 -- interfering with SELECT statements.                
 SET NOCOUNT ON;                
                
    -- Insert statements for procedure here                
 IF(@Parameter=1)                
                 
 BEGIN                
 INSERT INTO [dbo].[HRMS_Employee_Master_PersonalDetails]            
 (                           
  Emp_Code,Emp_Image,Emp_First_Name,Emp_Middle_Name,Emp_Last_Name,Emp_Roll,Emp_Status,        
  User_Name,Password,Emp_DOB,Emp_Age,Date_Of_Anniversary,Gender,        
 Blood_Group,Blood_Donor,Marital_Status,Nationality,Religion,Languages_Known,Hobbies,        
 Curricular_Activities,        
        
 Driving_Licenece_Number,Licence_Type,Licence_Exp_Date,Licence_Attachment,Aadhar_Number,Aadhar_Attachment,        
 Pan_Number,Pan_Attachment,Passport_Number,Issued_Date,Exp_Date,Passport_Attachment,        
 Bank_Name,Account_Number,Health_Insurance_Poly_No,Sum_Insured,Renewal_Date,        
 ---Contact Details        
 Present_Address1,Present_Address2,Present_City,Present_State,Present_Pincode,Present_Country,        
 Permanent_Address1,Permanent_Address2,Permanent_City,Permanent_State,Permanent_Pincode,Permanent_Country,        
 Permanent_Mobile,Permanent_Email,Alternative_Email,Skype_Id,Linkedin_Id,PresentAsPrev,        
---End Of Contact Details        
        
Creation_Company,Created_By,Created_Date,Resigned_Date,  
Educate,Telephone                 
                  
 )                 VALUES                
 (                 
                 
 --   [dbo].[Fn_autoincrement_HRMS_Employee_Master_PersonalDetails] (@Creation_Company) ,  
 @Emp_Code, @Emp_Image,                
    @Emp_First_Name,@Emp_Middle_Name,@Emp_Last_Name,@Emp_Roll,@Emp_Status,        
    @User_Name,@Password,@Emp_DOB,@Emp_Age,@Date_Of_Anniversary,@Gender,        
 @Blood_Group,@Blood_Donor,@Marital_Status,@Nationality,@Religion,@Languages_Known,@Hobbies,        
 @Curricular_Activities,        
            
 @Driving_Licenece_Number,@Licence_Type,@Licence_Exp_Date,@Licence_Attachment,@Aadhar_Number,@Aadhar_Attachment,        
 @Pan_Number,@Pan_Attachment,@Passport_Number,@Issued_Date,@Exp_Date,@Passport_Attachment,        
 @Bank_Name,@Account_Number,@Health_Insurance_Poly_No,@Sum_Insured,@Renewal_Date,        
        
 ---Contact Details        
 @Present_Address1,@Present_Address2,@Present_City,@Present_State,@Present_Pincode,@Present_Country,        
 @Permanent_Address1,@Permanent_Address2,@Permanent_City,@Permanent_State,@Permanent_Pincode,@Permanent_Country,        
 @Permanent_Mobile,@Permanent_Email,@Alternative_Email,@Skype_Id,@Linkedin_Id,@PresentAsPrev,        
        
----End Of Contact Details        
        
   @Creation_Company,@Created_By,@Created_Date                
   ,@Resigned_Date ,  
@Educate,@Telephone                
                
 )                
 END                
  -- Update statements for procedure here                
 IF(@Parameter=2)                
                 
 BEGIN                
 Update [dbo].[HRMS_Employee_Master_PersonalDetails]  SET         
                 
Emp_Image=@Emp_Image, Emp_First_Name=@Emp_First_Name,Emp_Middle_Name=@Emp_Middle_Name,Emp_Last_Name=@Emp_Last_Name,Emp_Roll=@Emp_Roll,        
Emp_Status=@Emp_Status ,User_Name=@User_Name,Password=@Password,Emp_DOB=@Emp_DOB,Emp_Age=@Emp_Age,Date_Of_Anniversary=@Date_Of_Anniversary,Gender=@Gender,        
 Blood_Group=@Blood_Group,Blood_Donor=@Blood_Donor,Marital_Status=@Marital_Status,Nationality=@Nationality,Religion=@Religion,Languages_Known=@Languages_Known,Hobbies=@Hobbies,        
 Curricular_Activities=@Curricular_Activities,        
        
 Driving_Licenece_Number=@Driving_Licenece_Number,Licence_Type=@Licence_Type,        
 Licence_Exp_Date=@Licence_Exp_Date,Licence_Attachment=@Licence_Attachment,Aadhar_Number=@Aadhar_Number,        
 Aadhar_Attachment=@Aadhar_Attachment,Pan_Number=@Pan_Number,Pan_Attachment=@Pan_Attachment,        
 Passport_Number=@Passport_Number,Issued_Date=@Issued_Date,Exp_Date=@Exp_Date,Passport_Attachment=@Passport_Attachment,        
 Bank_Name=@Bank_Name,Account_Number=@Account_Number,Health_Insurance_Poly_No=@Health_Insurance_Poly_No,        
 Sum_Insured=@Sum_Insured,Renewal_Date=@Renewal_Date,        
        
-----Contact  Details        
     Present_Address1=@Present_Address1,Present_Address2=@Present_Address2,Present_City=@Present_City,        
  Present_State=@Present_State,Present_Pincode=@Present_Pincode,Present_Country=@Present_Country,        
 Permanent_Address1=@Permanent_Address1,Permanent_Address2=@Permanent_Address2,Permanent_City=@Permanent_City,        
 Permanent_State=@Permanent_State,Permanent_Pincode=@Permanent_Pincode,Permanent_Country=@Permanent_Country,        
 Permanent_Mobile=@Permanent_Mobile,Permanent_Email=@Permanent_Email,Alternative_Email=@Alternative_Email,        
 Skype_Id=@Skype_Id,Linkedin_Id=@Linkedin_Id,PresentAsPrev=@PresentAsPrev,        
--End Of Contact Details        
        
        
  Modified_By=@Modified_By,Modified_Date=@Modified_Date ,     
  @Resigned_Date=@Resigned_Date,Telephone=@Telephone,  
Educate=@Educate          
 Where                 
         Emp_Code=@Emp_Code and Creation_Company=@Creation_Company                
 END                
                 
 ---- Select Statement For Procedure Here                
 IF(@Parameter=3)                
                 
 BEGIN                
 Select * from [dbo].[HRMS_Employee_Master_PersonalDetails]              
 Where                 
 Creation_Company=@Creation_Company                
                 
 END                                 
END




GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_Professional_Experience]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_Professional_Experience] 
        
@Emp_Code varchar(20),

@Emp_Name	varchar(50),
@Emp_Designation	varchar(50),
@Period_From	datetime,
@Period_To	datetime,
@Total_Duration	varchar(20),
@Attach_File	varchar(50),

@Creation_Company VARCHAR(50),        
@Created_By VARCHAR(50),        
@Created_Date datetime,        
@Modified_By VARCHAR(50),        
@Modified_Date datetime,        
        
@Parameter int     
         
        
        
        
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
    -- Insert statements for procedure here        
 IF(@Parameter=1)        
         
 BEGIN        
 INSERT INTO [dbo].[HRMS_Employee_Master_Professional_Experience]
 (
  Emp_Code, Emp_Name,Emp_Designation,Period_From,Period_To,Total_Duration,
Attach_File,    Creation_Company,Created_By,Created_Date        
          
 )        
 VALUES        
 (       
    @Emp_Code, @Emp_Name,@Emp_Designation,@Period_From,@Period_To,@Total_Duration,
@Attach_File,   @Creation_Company,@Created_By,@Created_Date
 )        
 END        
  -- Update statements for procedure here        
 IF(@Parameter=2)        
         
 BEGIN        
 Update [dbo].[HRMS_Employee_Master_Professional_Experience]
  SET         
     Emp_Name=@Emp_Name,Emp_Designation=@Emp_Designation,Period_From=@Period_From,
	 Period_To=@Period_To,Total_Duration=@Total_Duration,Attach_File=@Attach_File,
       Modified_By=@Modified_By,Modified_Date=@Modified_Date        
 Where         
         Emp_Code=@Emp_Code and Creation_Company=@Creation_Company        
 END        
         
 ---- Select Statement For Procedure Here        
 IF(@Parameter=3)        
         
 BEGIN        
 Select * from [dbo].[HRMS_Employee_Master_Professional_Experience]
 Where         
 Creation_Company=@Creation_Company        
         
 END        
         
END





GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Official_Info_Head_Details]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Official_Info_Head_Details]        
        
@Emp_Code VARCHAR (20),
@Heads VARCHAR (50),
@Amount  DECIMAL (18, 2) ,
@Basic_Amount  DECIMAL (18, 2),
@Gross_Amount  DECIMAL (18, 2),
@DA DECIMAL(18,2),
@HRA DECIMAL(18,2),
@CCA DECIMAL(18,2),

@OA DECIMAL(18,2),
@PF DECIMAL(18,2),
@EESI DECIMAL(18,2),
@ESI DECIMAL(18,2),
@PTAX DECIMAL(18,2),
@TDS DECIMAL(18,2),
@APGLI DECIMAL(18,2),
@GIS DECIMAL(18,2),
@HBA DECIMAL(18,2),
@MCA DECIMAL(18,2),
@BA DECIMAL(18,2),
@LOAN DECIMAL(18,2),
@OTHERS DECIMAL(18,2),
@NET DECIMAL(18,2),
@Criteria VARCHAR(50),
@Salary_Range VARCHAR(50),
@Creation_Company VARCHAR(50),        
@Created_By VARCHAR(50),        
@Created_Date datetime,        
@Modified_By VARCHAR(50),        
@Modified_Date datetime, 
@MONTHS VARCHAR(50),   
        
@Parameter int         
         
        
        
        
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
    -- Insert statements for procedure here        
 IF(@Parameter=1)        
         
 BEGIN        
 INSERT INTO [dbo].[HRMS_Employee_Official_Info_Head_Details]  
 (  
 Emp_Code,Heads,Amount,Basic_Amount,Gross_Amount,DA,HRA,CCA,OA,PF,EESI,ESI,PTAX,TDS,APGLI,GIS,HBA,MCA,BA,LOAN,OTHERS,NET,Criteria,Salary_Range,
 Creation_Company,Created_By,Created_Date,MONTHS      
          
 )        
 VALUES        
 (        
          
         
  @Emp_Code,@Heads,@Amount,@Basic_Amount,@Gross_Amount,@DA,@HRA,@CCA,@OA,@PF,@EESI,@ESI,@PTAX,@TDS,@APGLI,@GIS,@HBA,@MCA,@BA,@LOAN,@OTHERS,@NET,@Criteria,@Salary_Range,
 @Creation_Company,@Created_By,@Created_Date,@MONTHS     
          
        
 )        
 END        
  -- Update statements for procedure here        
 IF(@Parameter=2)        
         
 BEGIN        
 Update [dbo].[HRMS_Employee_Official_Info_Head_Details]  SET         
         
      Heads=@Heads,
	  Amount=@Amount,
	  Basic_Amount=@Basic_Amount,
	  Gross_Amount=@Gross_Amount, 
	  DA=@DA,
	  HRA=@HRA,
	  CCA=@CCA,
	  OA=@OA,
	  PF=@PF,
	  EESI=@EESI,
	  ESI=@ESI,
	  PTAX=@PTAX,
	  TDS=@TDS,
	  APGLI=@APGLI,
	  GIS=@GIS,
	  HBA=@HBA,
	  MCA=@MCA,
	  BA=@BA,
	  LOAN=@LOAN,
	  OTHERS=@OTHERS,
	  NET=@NET,
	  Criteria=@Criteria,
	  Salary_Range=@Salary_Range,
	  MONTHS=@MONTHS,
   
  Modified_By=@Modified_By,Modified_Date=@Modified_Date       
 Where         
         Emp_Code=@Emp_Code and Creation_Company=@Creation_Company        
 END        
         
 ---- Select Statement For Procedure Here        
 IF(@Parameter=3)        
         
 BEGIN        
 Select * from [dbo].[HRMS_Employee_Official_Info_Head_Details]      
 Where         
 Creation_Company=@Creation_Company        
         
 END        
         
END



GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Ot_Details]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Ot_Details]        
        
@Ot_Date DATETIME ,
@Emp_Code VARCHAR (20),
@Emp_Name VARCHAR (50),
@Job_Type VARCHAR (30),
@Department_Code  VARCHAR (20),
@Department_Name  VARCHAR (50),
@Start_Time DATETIME,
@End_Time DATETIME,
@Ot_Hrs time(7),
@Note VARCHAR (100) ,
   
@Creation_Company VARCHAR(50),        
@Created_By VARCHAR(50),        
@Created_Date datetime,        
@Modified_By VARCHAR(50),        
@Modified_Date datetime,        
        
@Parameter int         
         
        
        
        
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
    -- Insert statements for procedure here        
 IF(@Parameter=1)        
         
 BEGIN        
 INSERT INTO [dbo].[HRMS_Employee_Ot_Details]      
 (        
           
   Ot_Date,Emp_Code,Emp_Name,Job_Type,Department_Code,Department_Name,Start_Time,End_Time,Ot_Hrs,Note,       
    Creation_Company,Created_By,Created_Date
          
 )        
 VALUES        
 (        
          
         
    @Ot_Date,
@Emp_Code,@Emp_Name,@Job_Type,
@Department_Code,
@Department_Name,
@Start_Time,
@End_Time,
@Ot_Hrs,
@Note,  
   @Creation_Company,@Created_By,@Created_Date    
          
        
 )        
 END        
  -- Update statements for procedure here        
 IF(@Parameter=2)        
         
 BEGIN        
 Update [dbo].[HRMS_Employee_Ot_Details] SET         
         
         
    Emp_Code=@Emp_Code,Emp_Name=@Emp_Name,Job_Type=@Job_Type,Department_Name=@Department_Name,Start_Time=@Start_Time,
End_Time=@End_Time,
Ot_Hrs=@Ot_Hrs,
Note=@Note,   
  Modified_By=@Modified_By,Modified_Date=@Modified_Date       
 Where         
        Department_Code=@Department_Code and Creation_Company=@Creation_Company        
 END        
         
 ---- Select Statement For Procedure Here        
 IF(@Parameter=3)        
         
 BEGIN        
 Select * from [dbo].[HRMS_Employee_Ot_Details]      
 Where         
 Creation_Company=@Creation_Company        
         
 END        
         
END




GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Work_Locations]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Employee_Work_Locations]  
          
@Location_Id	varchar(20),
@Location_Name	varchar(50),
  
@Creation_Company VARCHAR(50),          
@Created_By VARCHAR(50),          
@Created_Date datetime,          
@Modified_By VARCHAR(50),          
@Modified_Date datetime,
--@Work_Type varchar(50),
--@Recruit  varchar(50),      
          
@Parameter int           
           
          
          
          
AS          
BEGIN          
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON;          
          
    -- Insert statements for procedure here          
 IF(@Parameter=1)          
           
 BEGIN          
 INSERT INTO [dbo].[HRMS_Employee_Work_Locations]
 (  
    Location_Id,Location_Name,
    Creation_Company,Created_By,Created_Date      
            
 )          
 VALUES          
 (  
    [dbo].[Fn_autoincrement_HRMS_Employee_Work_Locations] (@Creation_Company),
	@Location_Name,       
    @Creation_Company,@Created_By,@Created_Date  
 )          
 END          
  -- Update statements for procedure here          
 IF(@Parameter=2)          
           
 BEGIN          
 Update [dbo].[HRMS_Employee_Work_Locations]  SET  
     
	      Location_Name=@Location_Name, 
     Modified_By=@Modified_By,Modified_Date=@Modified_Date          
 Where           
         Location_Id=@Location_Id and Creation_Company=@Creation_Company          
 END          
           
 ---- Select Statement For Procedure Here          
 IF(@Parameter=3)          
           
 BEGIN      
       
 Select * from [dbo].[HRMS_Employee_Work_Locations]  
  Where           
  Creation_Company=@Creation_Company          
           
 END          
           
END




GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Group_Master]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Group_Master]          
          
@Group_Code VARCHAR(20),          
@Group_Name VARCHAR(50),   
@Short_Name  VARCHAR (50), 
@Group_type  VARCHAR(50),
@Applicable_Heads varchar(150),     
@Creation_Company VARCHAR(50),          
@Created_By VARCHAR(50),          
@Created_Date datetime,          
@Modified_By VARCHAR(50),          
@Modified_Date datetime,          
          
@Parameter int           
           
          
          
          
AS          
BEGIN          
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON;          
          
    -- Insert statements for procedure here          
 IF(@Parameter=1)          
           
 BEGIN          
 INSERT INTO [dbo].[HRMS_Group_Master]      
 (          
             
   Group_Code,Group_Name,Short_Name,Group_type,Applicable_Heads,    
    Creation_Company,Created_By,Created_Date          
            
 )          
 VALUES          
 (          
            
        --  @Group_Code, 
   [dbo].[Fn_autoincrement_HRMS_Group_Master] (@Creation_Company) ,          
 @Group_Name,@Short_Name,@Group_type,@Applicable_Heads,       
   @Creation_Company,@Created_By,@Created_Date          
            
          
 )          
 END          
  -- Update statements for procedure here          
 IF(@Parameter=2)          
           
 BEGIN          
 Update [dbo].[HRMS_Group_Master]  SET           
           
           
      Group_Name=@Group_Name,Short_Name=@Short_Name,Group_type=@Group_type,Applicable_Heads=@Applicable_Heads,  
  Modified_By=@Modified_By,Modified_Date=@Modified_Date          
 Where           
         Group_Code=@Group_Code and Creation_Company=@Creation_Company          
 END          
           
 ---- Select Statement For Procedure Here          
 IF(@Parameter=3)          
           
 BEGIN          
 Select * from [dbo].[HRMS_Group_Master]        
 Where           
 Creation_Company=@Creation_Company          
           
 END          
           
END





GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Holiday_Master]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Holiday_Master]        
    	@Holiday_Code	varchar(20),
    
@Year	varchar(10),
@Date	datetime,
@Event_Name	varchar(50),
@Creation_Company VARCHAR(50),        
@Created_By VARCHAR(50),        
@Created_Date datetime,        
@Modified_By VARCHAR(50),        
@Modified_Date datetime,        
        
@Parameter int         
         
        
        
        
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
    -- Insert statements for procedure here        
 IF(@Parameter=1)        
         
 BEGIN        
 INSERT INTO [dbo].[HRMS_Holiday_Master]    
 (        
           
   Holiday_Code,Year,Date,Event_Name,Creation_Company,Created_By,Created_Date        
          
 )        
 VALUES        
 (  [dbo].[Fn_autoincrement_HRMS_Holiday_Master] (@Creation_Company),      
  @Year,@Date,@Event_Name,@Creation_Company,@Created_By,@Created_Date        
  )        
 END        
  -- Update statements for procedure here        
 IF(@Parameter=2)        
         
 BEGIN        
 Update [dbo].[HRMS_Holiday_Master]  SET         
         Year=@Year ,
         
      Date=@Date,Event_Name=@Event_Name,
  Modified_By=@Modified_By,Modified_Date=@Modified_Date        
 Where         
         Holiday_Code=@Holiday_Code and Creation_Company=@Creation_Company        
 END        
         
 ---- Select Statement For Procedure Here        
 IF(@Parameter=3)        
         
 BEGIN        
 Select * from [dbo].[HRMS_Holiday_Master]      
 Where         
 Creation_Company=@Creation_Company        
         
 END        
         
END




GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_JobType_Master]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_JobType_Master]          
          
@Job_Id VARCHAR(20),        
@Job_Name VARCHAR(50),        
@Creation_Company VARCHAR(50),          
@Created_By VARCHAR(50),          
@Created_Date datetime,          
@Modified_By VARCHAR(50),          
@Modified_Date datetime,          
          
@Parameter int           
           
          
          
          
AS          
BEGIN          
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON;          
          
    -- Insert statements for procedure here          
 IF(@Parameter=1)          
           
 BEGIN          
 INSERT INTO [dbo].[HRMS_JobType_Master]      
 (          
             
    Job_Id,Job_Name,    
    Creation_Company,Created_By,Created_Date          
            
 )          
 VALUES          
 (          
            
           
     @Job_Id,          
 @Job_Name,       
   @Creation_Company,@Created_By,@Created_Date          
            
          
 )          
 END          
  -- Update statements for procedure here          
 IF(@Parameter=2)          
           
 BEGIN          
 Update [dbo].[HRMS_JobType_Master]  SET           
           
           
      Job_Name=@Job_Name,  
  Modified_By=@Modified_By,Modified_Date=@Modified_Date          
 Where           
         Job_Id=@Job_Id and Creation_Company=@Creation_Company          
 END          
           
 ---- Select Statement For Procedure Here          
 IF(@Parameter=3)          
           
 BEGIN          
 Select * from [dbo].[HRMS_JobType_Master]        
 Where           
 Creation_Company=@Creation_Company          
           
 END          
           
END




GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Leave_Allotment]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Leave_Allotment]          
          
@Emp_Code varchar(50),  
@Emp_Name varchar(50),  
@Select_Year varchar(30),  
@New_Opening_Balance varchar(30),  
@Leave_Name varchar(100),  
@Noof_Leavs decimal(18,2),  
  
@Creation_Company VARCHAR(50),          
@Created_By VARCHAR(50),          
@Created_Date datetime,          
@Modified_By VARCHAR(50),          
@Modified_Date datetime,          
          
@Parameter int          
           
          
          
          
AS          
BEGIN          
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON;          
          
    -- Insert statements for procedure here          
 IF(@Parameter=1)          
           
 BEGIN          
 INSERT INTO [dbo].[HRMS_Leave_Allotment]  
           (  
  Emp_Code, Emp_Name,Select_Year,New_Opening_Balance,  
Leave_Name,Noof_Leavs,  
    Creation_Company,Created_By,Created_Date          
            
 )          
 VALUES          
 (         
   @Emp_Code, @Emp_Name,@Select_Year,@New_Opening_Balance,  
@Leave_Name,@Noof_Leavs,  
    @Creation_Company,@Created_By,@Created_Date  
 )          
 END 
   
 if(@Parameter=4) 
  BEGIN 

    DELETE  [dbo].[HRMS_Leave_Allotment]   WHERE  Emp_Code=@Emp_Code and Creation_Company=@Creation_Company
    END 
  -- Update statements for procedure here          
 IF(@Parameter=2)          
           
 BEGIN  
  
 
-- DELETE  HRMS_Leave_Allotment WHERE  Emp_Code=@Emp_Code and Creation_Company=@Creation_Company
 
-- INSERT INTO [dbo].[HRMS_Leave_Allotment]  
--           (  
--  Emp_Code, Emp_Name,Select_Year,New_Opening_Balance,  
--Leave_Name,Noof_Leavs,  
--    Creation_Company,Created_By,Created_Date,Modified_By,    Modified_Date 
            
-- )          
-- VALUES          
-- (         
--   @Emp_Code, @Emp_Name,@Select_Year,@New_Opening_Balance,  
--@Leave_Name,@Noof_Leavs,  
--    @Creation_Company,@Created_By,@Created_Date  ,@Modified_By,    @Modified_Date 
-- )       
        
 Update [dbo].[HRMS_Leave_Allotment]  SET           
           
     Emp_Code=@Emp_Code,Emp_Name=@Emp_Name,Select_Year=@Select_Year,  
New_Opening_Balance=@New_Opening_Balance,Leave_Name=@Leave_Name,Noof_Leavs=@Noof_Leavs,      
       Modified_By=@Modified_By,Modified_Date=@Modified_Date          
 Where           
         Emp_Code=@Emp_Code and Creation_Company=@Creation_Company          
 END          
           
 ---- Select Statement For Procedure Here          
 IF(@Parameter=3)          
           
 BEGIN          
 Select * from [dbo].[HRMS_Leave_Allotment]  
 Where           
 Creation_Company=@Creation_Company          
           
 END          
           
END




GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Leave_Application]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Leave_Application]          
@Sno int,        
@Leave_Code VARCHAR (20),  
@Emp_Code   VARCHAR (50),  
@Emp_Name  VARCHAR (50),  
@Department VARCHAR (30),  
@Designation VARCHAR (30),  
@Reporting_Authority  varchar(50),  
@TypeOfLeaveApple VARCHAR (30),  
@Balances_Available DECIMAL (18, 2),  
@LeaveFrom  DATETIME,  
@LeaveTo  DATETIME,  
@Noof_Days DECIMAL (18, 2),  
@Leave_Status VARCHAR (50),     
@Creation_Company VARCHAR(50),          
@Created_By VARCHAR(50),          
@Created_Date datetime,          
@Modified_By VARCHAR(50),          
@Modified_Date datetime,          
          
@Parameter int  ,  
@Cerification_Upload_Path varchar(100)         
           
          
          
          
AS          
BEGIN          
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON;          
          
    -- Insert statements for procedure here          
 IF(@Parameter=1)          
           
 BEGIN          
 INSERT INTO [dbo].[HRMS_Leave_Application]      
 (          
   Leave_Code ,  
Emp_Code ,  
Emp_Name  ,  
Department ,  
Designation ,  
Reporting_Authority ,  
TypeOfLeaveApple ,  
Balances_Available ,  
LeaveFrom ,  
LeaveTo ,  
Noof_Days ,  
Leave_Status ,  
    Creation_Company,Created_By,Created_Date  ,  
 Cerification_Upload_Path        
            
 )          
 VALUES          
 (          
            
           
    --[dbo].[Fn_autoincrement_HRMS_Leave_Application] (@Creation_Company) , 
	@Leave_Code,         
  @Emp_Code ,  
@Emp_Name  ,  
@Department ,  
@Designation ,  
@Reporting_Authority ,  
@TypeOfLeaveApple ,  
@Balances_Available ,  
@LeaveFrom ,  
@LeaveTo ,  
@Noof_Days ,  
@Leave_Status ,     
   @Creation_Company,@Created_By,@Created_Date ,  
   @Cerification_Upload_Path         
            
          
 )          
 END          
  -- Update statements for procedure here          
 IF(@Parameter=2)          
           
 BEGIN          
 Update [dbo].[HRMS_Leave_Application]  SET           
           
           
     Emp_Code=@Emp_Code ,  
Emp_Name=@Emp_Name  ,  
Department=@Department,  
Designation=@Designation ,  
Reporting_Authority=@Reporting_Authority ,  
TypeOfLeaveApple=@TypeOfLeaveApple ,  
Balances_Available=@Balances_Available ,  
LeaveFrom=@LeaveFrom ,  
LeaveTo=@LeaveTo,  
Noof_Days=@Noof_Days ,  
Leave_Status=@Leave_Status ,  
  Modified_By=@Modified_By,Modified_Date=@Modified_Date  ,  
  Cerification_Upload_Path=@Cerification_Upload_Path  
          
 Where           
         Sno=@Sno 
 END          
           
 ---- Select Statement For Procedure Here          
 IF(@Parameter=3)          
           
 BEGIN          
 Select * from [dbo].[HRMS_Leave_Application]        
 Where           
 Creation_Company=@Creation_Company          
           
 END          
           
END




GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Leave_Master]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Leave_Master]          
          
 @Leave_Code VARCHAR (20),  
@Leave_Name VARCHAR (50),  
 @Short_Name VARCHAR (30),  
@Monthly  VARCHAR (30),  
 @Monthly_Limit  DECIMAL (18, 2) ,  
@Encashment VARCHAR (50),  
@Balnces_Transfer  VARCHAR (50),  
 @Encashment_Balance  DECIMAL (18, 2),  
@Allotment varchar(50),  
@Noof_leaves_Per_Year  DECIMAL (18, 2),  
@Need_Certification varchar(20),  
@TwoStage varchar(50),  
@authority1 varchar(50) ,  
@username1 varchar(50),    
@authority2 varchar(50),   
@username2 varchar(50),    
@Creation_Company VARCHAR(50),          
@Created_By VARCHAR(50),          
@Created_Date datetime,          
@Modified_By VARCHAR(50),          
@Modified_Date datetime, 
@Period varchar(50),
@No_Of_Times decimal(18,2),
@No_Of_Days decimal(18,2),
@Authority3 varchar(50),
@UserName3 varchar(50),         
          
@Parameter int           
           
          
          
          
AS          
BEGIN          
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON;          
          
    -- Insert statements for procedure here          
 IF(@Parameter=1)          
           
 BEGIN          
 INSERT INTO [dbo].[HRMS_Leave_Master]      
 (          
    Leave_Code,         
   Leave_Name ,  
 Short_Name ,  
Monthly  ,  
 Monthly_Limit ,  
Encashment,  
Balnces_Transfer ,  
 Encashment_Balance ,  
Allotment ,  
Noof_leaves_Per_Year,TwoStage,Need_Certification,UserName1,Authority1,UserName2,Authority2,  
    Creation_Company,Created_By,Created_Date,Period,No_Of_Times,No_Of_Days,
	 Authority3,UserName3         
            
 )          
 VALUES          
 (          
            
           
    [dbo].[Fn_autoincrement_HRMS_Leave_Master] (@Creation_Company) ,          
  @Leave_Name ,  
 @Short_Name ,  
@Monthly  ,  
@Monthly_Limit ,  
@Encashment,  
@Balnces_Transfer ,  
@Encashment_Balance ,  
@Allotment ,  
@Noof_leaves_Per_Year,   
@TwoStage ,  
@Need_Certification,  
@authority1  ,  
@username1 ,    
@authority2,   
@username2 ,        
@Creation_Company,
@Created_By,
@Created_Date,
@Period,
@No_Of_Times,
@No_Of_Days,
@Authority3,
@UserName3          
            
          
 )          
 END          
  -- Update statements for procedure here          
 IF(@Parameter=2)          
           
 BEGIN          
 Update [dbo].[HRMS_Leave_Master]  SET           
           
           
      Leave_Name =@Leave_Name,  
 Short_Name =@Short_Name,  
Monthly =@Monthly ,  
 Monthly_Limit =@Monthly_Limit,  
Encashment=@Encashment,  
Balnces_Transfer =@Balnces_Transfer,  
 Encashment_Balance =@Encashment_Balance,  
Allotment =@Allotment,  
Noof_leaves_Per_Year=@Noof_leaves_Per_Year,  
Modified_By=@Modified_By,Modified_Date=@Modified_Date ,    
TwoStage = @TwoStage ,  
Authority1=@authority1  ,  
UserName1=@username1 ,    
Authority2=@authority2,   
UserName2=@username2,  
Need_Certification=@Need_Certification,
Period=@Period,No_Of_Times=@No_Of_Times,
No_Of_Days=@No_Of_Days,Authority3=@Authority3,
UserName3=@UserName3   
       
 Where           
         Leave_Code=@Leave_Code and Creation_Company=@Creation_Company          
 END          
           
 ---- Select Statement For Procedure Here          
 IF(@Parameter=3)          
           
 BEGIN          
 Select * from [dbo].[HRMS_Leave_Master]        
 Where           
 Creation_Company=@Creation_Company          
           
 END          
           
END




GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Loan_Application]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Loan_Application]          
@Sno int,
@Loan_Code varchar(50),  
@Emp_Code   VARCHAR (50),  
@Emp_Name  VARCHAR (50),  
@Department VARCHAR (30),  
@Designation VARCHAR (30),  
@Reporting_Authority  varchar(50),  
@Type_Of_Loan_Apply VARCHAR (30),  
@Basic_Amount DECIMAL (18, 2),  
@Loan_amount_Required  DECIMAL (18, 2),  
@Eligibility_Amount DECIMAL (18, 2),  
@Rate_Of_Interest DECIMAL (18, 2), 
@tenure INT, 
@Status VARCHAR (50),     
@Creation_Company VARCHAR(50),          
@Created_By VARCHAR(50),          
@Created_Date datetime,          
@Modified_By VARCHAR(50),          
@Modified_Date datetime,          
          
@Parameter int         
           
          
          
          
AS          
BEGIN          
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON;          
          
    -- Insert statements for procedure here          
 IF(@Parameter=1)          
           
 BEGIN          
 INSERT INTO [dbo].[HRMS_LOAN_Application]      
 ( 
 Loan_Code,          
Emp_Code ,  
Emp_Name  ,  
Department ,  
Designation ,  
Reporting_Authority ,  
Type_Of_Loan_Apply,  
Basic_Amount,  
Loan_amount_Required ,  
Eligibility_Amount,
Status,
    Creation_Company,Created_By,Created_Date
            
 )          
 VALUES          
 (          
            
           
    --[dbo].[Fn_autoincrement_HRMS_Leave_Application] (@Creation_Company) , 
    @Loan_Code,
@Emp_Code ,  
@Emp_Name  ,  
@Department ,  
@Designation ,  
@Reporting_Authority ,  
@Type_Of_Loan_Apply,  
@Basic_Amount,  
@Loan_amount_Required ,  
@Eligibility_Amount,
@Status,
    @Creation_Company,
    @Created_By,
    @Created_Date
                 
            
          
 )          
 END          
  -- Update statements for procedure here          
 IF(@Parameter=2)          
           
 BEGIN          
 Update [dbo].[HRMS_loan_Application]  SET           
           
     Loan_Code=@Loan_Code,      
     Emp_Code=@Emp_Code ,  
Emp_Name=@Emp_Name  ,  
Department=@Department,  
Designation=@Designation ,  
Reporting_Authority=@Reporting_Authority ,  
Type_Of_Loan_Apply=@Type_Of_Loan_Apply,  
Basic_Amount=@Basic_Amount,  
Loan_amount_Required=@Loan_amount_Required ,  
Eligibility_Amount=@Eligibility_Amount,  
Rate_Of_Interest=@Rate_Of_Interest ,
Status=@Status,  
tenure=@tenure,
  Modified_By=@Modified_By,
  Modified_Date=@Modified_Date 
          
 Where           
         Sno=@Sno 
 END          
           
 ---- Select Statement For Procedure Here          
 IF(@Parameter=3)          
           
 BEGIN          
 Select * from [dbo].[HRMS_Loan_Application]        
 Where           
 Creation_Company=@Creation_Company          
           
 END          
           
END




GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Loan_Master]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Loan_Master]          
          
 @Loan_Code VARCHAR (20),  
@Loan_Name VARCHAR (50),  
 @Short_Name VARCHAR (30), 
 @Allotment varchar(50),  
@Need_Certification varchar(20),
  
@authority1 varchar(50) ,  
@username1 varchar(50),    
@authority2 varchar(50),   
@username2 varchar(50),    
@Creation_Company VARCHAR(50),          
@Created_By VARCHAR(50),          
@Created_Date datetime,          
@Modified_By VARCHAR(50),          
@Modified_Date datetime, 
@Period varchar(50),
@No_Of_Times decimal(18,2),

@Authority3 varchar(50),
@UserName3 varchar(50),         
          
@Parameter int           
           
          
          
          
AS          
BEGIN          
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON;          
          
    -- Insert statements for procedure here          
 IF(@Parameter=1)          
           
 BEGIN          
 INSERT INTO [dbo].[HRMS_Loan_Master]      
 (          
   Loan_Code,         
   Loan_Name,  
 Short_Name,   
Allotment, 
Need_Certification,
UserName1,
Authority1,
UserName2,
Authority2,  
Creation_Company,
Created_By,
Created_Date,
Modified_By,
 Modified_Date,
Period,
No_Of_Times,
Authority3,
UserName3        
            
 )          
 VALUES          
 (                
    [dbo].[Fn_autoincrement_HRMS_Loan_Master] (@Creation_Company) ,
    @Loan_Code,        
  @Loan_Name ,  
 @Short_Name ,  
@Allotment ,  
@Need_Certification,  
@authority1  ,  
@username1 ,    
@authority2,   
@username2 ,        
@Creation_Company,
@Created_By,
@Created_Date,
@Modified_By,
 @Modified_Date,
@Period,
@No_Of_Times,

@Authority3,
@UserName3          
            
          
 )          
 END          
  -- Update statements for procedure here          
 IF(@Parameter=2)          
           
 BEGIN          
 Update [dbo].[HRMS_Loan_Master]  SET           
           
           
      Loan_Name =@Loan_Name,  
 Short_Name =@Short_Name,  
Allotment =@Allotment,  
Modified_By=@Modified_By,
Modified_Date=@Modified_Date ,    
Authority1=@authority1  ,  
UserName1=@username1 ,    
Authority2=@authority2,   
UserName2=@username2,  
Need_Certification=@Need_Certification,
Period=@Period,
No_Of_Times=@No_Of_Times,
Authority3=@Authority3,
UserName3=@UserName3   
       
 Where           
         Loan_Code=@Loan_Code and Creation_Company=@Creation_Company          
 END          
           
 ---- Select Statement For Procedure Here          
 IF(@Parameter=3)          
           
 BEGIN          
 Select * from [dbo].[HRMS_Loan_Master]        
 Where           
 Creation_Company=@Creation_Company          
           
 END          
           
END




GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_OT_Criteria_Master]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_OT_Criteria_Master]        
@OT_Code VARCHAR(20),        
@Ot_Head_Names VARCHAR(50), 

@Ot_Criteria	varchar(100),

@Ot_Rate	decimal(18, 2),
@Creation_Company VARCHAR(50),        
@Created_By VARCHAR(50),        
@Created_Date datetime,        
@Modified_By VARCHAR(50),        
@Modified_Date datetime,        
        
@Parameter int         
         
        
        
        
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
    -- Insert statements for procedure here        
 IF(@Parameter=1)        
         
 BEGIN        
 INSERT INTO [dbo].[HRMS_OT_Criteria_Master]    
 (        
           
  OT_Code,Ot_Head_Names,
Ot_Criteria,Ot_Rate	,

    Creation_Company,Created_By,Created_Date        
          
 )        
 VALUES        
 (        
          
         
    @OT_Code ,        
 @Ot_Head_Names ,
@Ot_Criteria	,@Ot_Rate,
   @Creation_Company,@Created_By,@Created_Date        
          
        
 )        
 END        
  -- Update statements for procedure here        
 IF(@Parameter=2)        
         
 BEGIN        
 Update [dbo].[HRMS_OT_Criteria_Master]  SET         
         
         
    
	  Ot_Head_Names=@Ot_Head_Names,
Ot_Criteria=@Ot_Criteria,Ot_Rate=@Ot_Rate,
 
  Modified_By=@Modified_By,Modified_Date=@Modified_Date        
 Where         
         OT_Code=@OT_Code and Creation_Company=@Creation_Company        
 END        
         
 ---- Select Statement For Procedure Here        
 IF(@Parameter=3)        
         
 BEGIN        
 Select * from [dbo].[HRMS_OT_Criteria_Master]      
 Where         
 Creation_Company=@Creation_Company        
         
 END        
         
END




GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Recruit_Master]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Recruit_Master]        
        
@Recruit_Code VARCHAR(20),        
@Recruit_Name VARCHAR(50),      
@Creation_Company VARCHAR(50),        
@Created_By VARCHAR(50),        
@Created_Date datetime,        
@Modified_By VARCHAR(50),        
@Modified_Date datetime,        
        
@Parameter int         
         
        
        
        
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
    -- Insert statements for procedure here        
 IF(@Parameter=1)        
         
 BEGIN        
 INSERT INTO [dbo].[HRMS_Recruit_Master]    
 (        
           
   Recruit_Code,Recruit_Name,  
    Creation_Company,Created_By,Created_Date        
          
 )        
 VALUES        
 (        
          
         @Recruit_Code,
    ---[dbo].[Fn_autoincrement_HRMS_Department_Master] (@Creation_Company) ,        
 @Recruit_Name,     
   @Creation_Company,@Created_By,@Created_Date        
          
        
 )        
 END        
  -- Update statements for procedure here        
 IF(@Parameter=2)        
         
 BEGIN        
 Update [dbo].[HRMS_Recruit_Master]  SET         
         
         
      Recruit_Name=@Recruit_Name,
  Modified_By=@Modified_By,Modified_Date=@Modified_Date        
 Where         
         Recruit_Code=@Recruit_Code and Creation_Company=@Creation_Company        
 END        
         
 ---- Select Statement For Procedure Here        
 IF(@Parameter=3)        
         
 BEGIN        
 Select * from [dbo].[HRMS_Recruit_Master]      
 Where         
 Creation_Company=@Creation_Company        
         
 END        
         
END





GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Shift_Setup]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_Insert_Select_Update_HRMS_Shift_Setup]        
        
@Shift_Code varchar(20),
@Shift_Name	varchar(30),
@Sign	varchar(30),
@A_Start_Time	time(7),
@A_Early_Ot_Limit	time(7),
@A_Invalid_Limit	time(7),
@A_Early_Limit	time(7),
@A_Delay_Limit	time(7),
@A_Late_Limit	time(7),
@A_Crossday	varchar(20),
@A_First_Punch	varchar(10),
@A_Off_Time	time(7),
@A_Leave_Early_Limit	time(7),
@A_Second_Early_Limit	time(7),
@A_Second_Delay_Limit	time(7),
@A_Second_Invalid_Limit	time(7),
@A_Ot_Delay_Limit	time(7),
@A_Second_Crossday	varchar(20),
@A_Second_Punch	varchar(10),
@First_Working_Hrs	time(7),
@B_Start_Time	time(7),
@B_Early_Ot_Limit	time(7),
@B_Invalid_Limit	time(7),
@B_Early_Limit	time(7),
@B_Delay_Limit	time(7),
@B_Late_Limit	time(7),
@B_Crossday	varchar(20),
@B_First_Punch	varchar(10),
@B_Off_Time	time(7),
@B_Leave_Early_Limit	time(7),
@B_Second_Early_Limit	time(7),
@B_Second_Delay_Limit	time(7),
@B_Second_Invalid_Limit	time(7),
@B_Ot_Delay_Limit	time(7),
@B_Second_Crossday	varchar(20),
@B_Second_Punch	varchar(10),
@Second_Working_Hrs	time(7),
@C_Start_Time	time(7),
@C_Early_Ot_Limit	time(7),
@C_Invalid_Limit	time(7),
@C_Early_Limit	time(7),
@C_Delay_Limit	time(7),
@C_Late_Limit	time(7),
@C_Crossday	varchar(20),
@C_First_Punch	varchar(10),
@C_Off_Time	time(7),
@C_Leave_Early_Limit	time(7),
@C_Second_Early_Limit	time(7),
@C_Second_Delay_Limit	time(7),
@C_Second_Invalid_Limit	time(7),
@C_Ot_Delay_Limit	time(7),
@C_Second_Crossday	varchar(20),
@C_Second_Punch	varchar(10),
@Third_Working_Hrs	time(7),
@Break_Before_Ot	time(7),
@Early_Limit	time(7),
@Ot_Delay_Limit	time(7),
@Max_Ot_Hrs	time(7),

@Creation_Company VARCHAR(50),        
@Created_By VARCHAR(50),        
@Created_Date datetime,        
@Modified_By VARCHAR(50),        
@Modified_Date datetime,        
        
@Parameter int         
         
        
        
        
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
    -- Insert statements for procedure here        
 IF(@Parameter=1)        
         
 BEGIN        
 INSERT INTO HRMS_Shift_Setup (        
           
  Shift_Code,
Shift_Name,
Sign,
A_Start_Time,
A_Early_Ot_Limit,
A_Invalid_Limit,
A_Early_Limit,
A_Delay_Limit,
A_Late_Limit,
A_Crossday,
A_First_Punch,
A_Off_Time,
A_Leave_Early_Limit,
A_Second_Early_Limit,
A_Second_Delay_Limit,
A_Second_Invalid_Limit,
A_Ot_Delay_Limit,
A_Second_Crossday,
A_Second_Punch,
First_Working_Hrs,
B_Start_Time,
B_Early_Ot_Limit,
B_Invalid_Limit,
B_Early_Limit,
B_Delay_Limit,
B_Late_Limit,
B_Crossday,
B_First_Punch,
B_Off_Time,
B_Leave_Early_Limit,
B_Second_Early_Limit,
B_Second_Delay_Limit,
B_Second_Invalid_Limit,
B_Ot_Delay_Limit,
B_Second_Crossday,
B_Second_Punch,
Second_Working_Hrs,
C_Start_Time,
C_Early_Ot_Limit,
C_Invalid_Limit,
C_Early_Limit,
C_Delay_Limit,
C_Late_Limit,
C_Crossday,
C_First_Punch,
C_Off_Time,
C_Leave_Early_Limit,
C_Second_Early_Limit,
C_Second_Delay_Limit,
C_Second_Invalid_Limit,
C_Ot_Delay_Limit,
C_Second_Crossday,
C_Second_Punch,
Third_Working_Hrs,
Break_Before_Ot,
Early_Limit,
Ot_Delay_Limit,
Max_Ot_Hrs,
    Creation_Company,Created_By,Created_Date        
          
 )        
 VALUES        
 (        
          
@Shift_Code,
@Shift_Name,
@Sign,
@A_Start_Time,
@A_Early_Ot_Limit,
@A_Invalid_Limit,
@A_Early_Limit,
@A_Delay_Limit,
@A_Late_Limit,
@A_Crossday,
@A_First_Punch,
@A_Off_Time,
@A_Leave_Early_Limit,
@A_Second_Early_Limit,
@A_Second_Delay_Limit,
@A_Second_Invalid_Limit,
@A_Ot_Delay_Limit,
@A_Second_Crossday,
@A_Second_Punch,
@First_Working_Hrs,
@B_Start_Time,
@B_Early_Ot_Limit,
@B_Invalid_Limit,
@B_Early_Limit,
@B_Delay_Limit,
@B_Late_Limit,
@B_Crossday,
@B_First_Punch,
@B_Off_Time,
@B_Leave_Early_Limit,
@B_Second_Early_Limit,
@B_Second_Delay_Limit,
@B_Second_Invalid_Limit,
@B_Ot_Delay_Limit,
@B_Second_Crossday,
@B_Second_Punch,
@Second_Working_Hrs,
@C_Start_Time,
@C_Early_Ot_Limit,
@C_Invalid_Limit,
@C_Early_Limit,
@C_Delay_Limit,
@C_Late_Limit,
@C_Crossday,
@C_First_Punch,
@C_Off_Time,
@C_Leave_Early_Limit,
@C_Second_Early_Limit,
@C_Second_Delay_Limit,
@C_Second_Invalid_Limit,
@C_Ot_Delay_Limit,
@C_Second_Crossday,
@C_Second_Punch,
@Third_Working_Hrs,
@Break_Before_Ot,
@Early_Limit,
@Ot_Delay_Limit,
@Max_Ot_Hrs,
@Creation_Company,@Created_By,@Created_Date        
          
        
 )        
 END        
  -- Update statements for procedure here        
 IF(@Parameter=2)        
         
 BEGIN        
 Update HRMS_Shift_Setup  SET         
Shift_Name=@Shift_Name,
Sign=@Sign,
A_Start_Time=@A_Start_Time,
A_Early_Ot_Limit=@A_Early_Ot_Limit,
A_Invalid_Limit=@A_Invalid_Limit,
A_Early_Limit=@A_Early_Limit,
A_Delay_Limit=@A_Delay_Limit,
A_Late_Limit=@A_Late_Limit,
A_Crossday=@A_Crossday,
A_First_Punch=@A_First_Punch,
A_Off_Time=@A_Off_Time,
A_Leave_Early_Limit=@A_Leave_Early_Limit,
A_Second_Early_Limit=@A_Second_Early_Limit,
A_Second_Delay_Limit=@A_Second_Delay_Limit,
A_Second_Invalid_Limit=@A_Second_Invalid_Limit,
A_Ot_Delay_Limit=@A_Ot_Delay_Limit,
A_Second_Crossday=@A_Second_Crossday,
A_Second_Punch=@A_Second_Punch,
First_Working_Hrs=@First_Working_Hrs,
B_Start_Time=@B_Start_Time,
B_Early_Ot_Limit=@B_Early_Ot_Limit,
B_Invalid_Limit=@B_Invalid_Limit,
B_Early_Limit=@B_Early_Limit,
B_Delay_Limit=@B_Delay_Limit,
B_Late_Limit=@B_Late_Limit,
B_Crossday=@B_Crossday,
B_First_Punch=@B_First_Punch,
B_Off_Time=@B_Off_Time,
B_Leave_Early_Limit=@B_Leave_Early_Limit,
B_Second_Early_Limit=@B_Second_Early_Limit,
B_Second_Delay_Limit=@B_Second_Delay_Limit,
B_Second_Invalid_Limit=@B_Second_Invalid_Limit,
B_Ot_Delay_Limit=@B_Ot_Delay_Limit,
B_Second_Crossday=@B_Second_Crossday,
B_Second_Punch=@B_Second_Punch,
Second_Working_Hrs=@Second_Working_Hrs,
C_Start_Time=@C_Start_Time,
C_Early_Ot_Limit=@C_Early_Ot_Limit,
C_Invalid_Limit=@C_Invalid_Limit,
C_Early_Limit=@C_Early_Limit,
C_Delay_Limit=@C_Delay_Limit,
C_Late_Limit=@C_Late_Limit,
C_Crossday=@C_Crossday,
C_First_Punch=@C_First_Punch,
C_Off_Time=@C_Off_Time,
C_Leave_Early_Limit=@C_Leave_Early_Limit,
C_Second_Early_Limit=@C_Second_Early_Limit,
C_Second_Delay_Limit=@C_Second_Delay_Limit,
C_Second_Invalid_Limit=@C_Second_Invalid_Limit,
C_Ot_Delay_Limit=@C_Ot_Delay_Limit,
C_Second_Crossday=@C_Second_Crossday,
C_Second_Punch=@C_Second_Punch,
Third_Working_Hrs=@Third_Working_Hrs,
Break_Before_Ot=@Break_Before_Ot,
Early_Limit=@Early_Limit,
Ot_Delay_Limit=@Ot_Delay_Limit,
Max_Ot_Hrs=@Max_Ot_Hrs,
  Modified_By=@Modified_By,Modified_Date=@Modified_Date        
 Where         
         Shift_Code=@Shift_Code and Creation_Company=@Creation_Company        
 END        
         
 ---- Select Statement For Procedure Here        
 IF(@Parameter=3)        
         
 BEGIN        
 Select * from HRMS_Shift_Setup
 Where         
 Creation_Company=@Creation_Company        
         
 END        
         
END



GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_LEAVE_DATA_BIND_EMP]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PRoc [dbo].[Sp_HRMS_LEAVE_DATA_BIND_EMP]  
@Creation_Company varchar(50),    

@Emp_Code varchar(20)    

AS    

BEGIN    

    

SELECT  EMP.Emp_Last_Name + ' ' + EMP.Emp_First_Name + ' ' + EMP.Emp_Middle_Name AS 'Emp_name',    

ISNULL(LM.Total_Leaves,0) AS 'Total_Leaves', LM.Leave_Name,    

EOI.Department,EOI.Designation,EOI.Reporting_Authority,    

ISNULL(LAP.Applied_Leaves,0) AS 'Applied_Leaves',    

ISNULL(LM.Total_Leaves,0)-ISNULL(LAP.Applied_Leaves,0) AS 'Balance_Leaves',    

    

EMP.Emp_Code FROM HRMS_Employee_Master_PersonalDetails EMP    

    

    

LEFT JOIN HRMS_Employee_Master_Official_Information HOI    

    

ON  EMP.Creation_Company = HOI.Creation_Company AND EMP.Emp_Code = HOI.Emp_Code    

    

left join HRMS_Employee_Master_Official_Information EOI    

    

on  EMP.Creation_Company = EOI.Creation_Company AND EMP.Emp_Code = EOI.Emp_Code    

    

    

    

LEFT JOIN    

(    

SELECT  ISNULL(SUM(Noof_Leavs),0) AS 'Total_Leaves', 
Select_Year,Leave_Name, Emp_Code,Creation_Company    

    

FROM HRMS_Leave_Allotment    

    

WHERE Select_Year=convert(varchar(10), DATEPART(yyyy,GETDATE()))     

    

GROUP BY  Select_Year, Emp_Code,Creation_Company,Leave_Name ) LM    

    

ON  EMP.Creation_Company = LM.Creation_Company AND EMP.Emp_Code = LM.Emp_Code   
LEFT JOIN     
(SELECT SUM(Noof_Days) AS 'Applied_Leaves',Emp_Code,Leave_Code,Creation_Company,LeaveFrom    

FROM HRMS_Leave_Application    

WHERE Leave_Status='Approval' AND DATEPART(yyyy,LeaveFrom)=DATEPART(yyyy,GETDATE())     

GROUP BY Emp_Code,Leave_Code,Creation_Company,LeaveFrom) LAP    

    

ON EMP.Creation_Company = LAP.Creation_Company AND EMP.Emp_Code = LAP.Emp_Code    

    

    

WHERE   EMP.Creation_Company = @Creation_Company AND EMP.Emp_Code = @Emp_Code    

    

END 



GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_OT_Criteria_Master_OTCode]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_HRMS_OT_Criteria_Master_OTCode]        
        
@Creation_Company VARCHAR(50)     
         
        
        
        
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
    DECLARE @Result   VARCHAR(100)='',@newvalue VARCHAR(100)    
select @newvalue =MAX(OT_Code) from [dbo].[HRMS_OT_Criteria_Master]  Where Creation_Company=@Creation_Company --order by Sno desc    
select @newvalue=convert(varchar(27),Convert(int,Replace(ISNULL(@newvalue,'000000'),'',''))+1);    
select @Result=@Result + case len(@newvalue) when 1 then '00000'+@newvalue when 2 then '0000'+@newvalue when 3 then '000'+@newvalue when 4 then '00'+@newvalue when 5 then '0'+@newvalue else @newvalue end;    
SELECT @Result  AS 'OTCode'    

END




GO
/****** Object:  StoredProcedure [dbo].[Sp_HRMS_Select_HRMS_Employee_Master_Attendance]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[Sp_HRMS_Select_HRMS_Employee_Master_Attendance]        
        
@Emp_Code VARCHAR(20),        
 
@Creation_Company VARCHAR(50)  
 

AS        
BEGIN        
 
-- Declare @Emp_Code VARCHAR(20)='TG022'      
 
--Declare @Creation_Company VARCHAR(50) ='000002'

  select *
 from 
 HRMS_Employee_Master_Attendance 
 where Creation_Company=@Creation_Company and Emp_Code=@Emp_Code and CONVERT(varchar(10),Created_Date,105)=CONVERT(varchar(10),GETDATE(),105)
 

 

END





GO
/****** Object:  StoredProcedure [dbo].[sp_insert_HRMS_pension_Central]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_insert_HRMS_pension_Central] (
@Sno int,
@Status varchar(max),
@PensionNo varchar(max),
@empid varchar(250),
@empname varchar(max),
@fathername varchar(max),
@designation varchar(max),
@department varchar(max),
@DOB date,
@retirment date,
@seven varchar(max),
@eighta varchar(max),
@eightb varchar(max),
@bankname varchar(max),
@acno varchar(250),
@acoffice varchar(max),
@fathernaame varchar(max),
@height varchar(max),
@partonesix varchar(max),
@partoneseven varchar(max),
@partonesevena varchar(max),
@partonesevenb varchar(max),
@partonesevenc varchar(max),
@partoneeight varchar(max),
@servicebegining date,
@endingservice date,
@levena varchar(max),
@levenb varchar(max),
@levenc varchar(max),
@levend varchar(max),
@levene varchar(max),
@levenf varchar(max),
@leveng varchar(max),
@levenh varchar(max),
@leveni varchar(max),
@levenj varchar(max),
@levenk varchar(max),
@tweleve varchar(max),
@thirten varchar(max),
@forteena varchar(max),
@forteenb varchar(max),
@forteenc varchar(max),
@forteend varchar(max),
@fifteena varchar(max),
@fiteenbfrom varchar(max),
@fifteenbto varchar(max),
@fifteencfrom varchar(max),
@fifteencto varchar(max),
@sixteen varchar(max),
@sixteenafrom varchar(max),
@sixteenato varchar(max),
@sixteenbfrom varchar(max),
@sixteenbto varchar(max),
@sixteenonefrom varchar(max),
@sixteenoneto varchar(max),
@sixteentwofrom varchar(max),
@sixteentwoto varchar(max),
@sixteenthreefrom varchar(max),
@sixteenthreeto varchar(max),
@sixteenforefrom varchar(max),
@sixteenforeto varchar(max),
@sixteenfivefrom varchar(max),
@sixteenfiveto varchar(max),
@sixteensixfrom varchar(max),
@sixteensixto varchar(max),
@sixteenconefrom varchar(max),
@sixteenconetwo varchar(max),
@sixteenctwofrom varchar(max),
@sixteenctwotwo varchar(max),
@sixteencthreefrom varchar(max),
@sixteencthreetwo varchar(max),
@sixteencforefrom varchar(max),
@sixteencforetwo varchar(max),
@sixteend varchar(max),
@sixteene varchar(max),
@seventeenfrom varchar(max),
@seventeento varchar(max),
@seventeenrateofpay money,
@seventeenaamount money,
@seventeenatotal money,
@seventeenb varchar(max),
@seventeenc varchar(max),
@seventeend varchar(max),
@seventeene varchar(max),
@seventeeene varchar(max),
@deathorretirement varchar(max),
@twentytwoa varchar(max),
@twentytwob varchar(max),
@twentytwoc varchar(max),
@twentythreea varchar(max),
@twentythreeb varchar(max),
@twentiforea varchar(max),
@twentiforeb varchar(max),
@twentiforec date,
@twentyfive varchar(max),
@twentysix varchar(max),
@twentysevena varchar(max),
@twentysevenb varchar(max),
@twentysevenc varchar(max),
@twentysevend varchar(max),
@twentysevene varchar(max),
@bankaddress varchar(max),
@dateofreciptpensionpaper date,
@lengthofQP varchar(max),
@classOfPension varchar(max),
@Amountofmonthlypension Money,
@DateofCommencement Date,
@parttwoone varchar(max),
@parttwotwo varchar(max),
@parttwothree date,
@parttwofore date,
@parttwodone varchar(max),
@parttwodtwo varchar(max),
@parttwodthree varchar(max),
@parttwodfore varchar(max),
@parttwoeone varchar(max),
@parttwoetwo varchar(max),
@parttwoethree varchar(max),
@parttwothreee varchar(max),
@dateofappointment date,
@place varchar(max),
@dateall date,
@Creation_Company varchar(max),
@Created_By varchar(max),
@Created_Date date,
@Modified_By varchar(max),
@Modified_Date date,
@parameter int)
as
begin
if(@parameter=1)

begin
insert into HRMS_pension_central
(Status,PensionNo,empid,empname,fathername,designation,department,DOB,retirment,seven,eighta,eightb,bankname,acno ,acoffice ,fathernaame ,height ,partonesix ,partoneseven ,partonesevena ,partonesevenb,partonesevenc ,partoneeight,servicebegining ,endingservice,levena ,levenb ,levenc ,levend ,levene ,levenf ,leveng ,levenh ,leveni ,levenj ,levenk ,tweleve ,thirten ,forteena ,forteenb ,forteenc ,forteend ,fifteena,fiteenbfrom,fifteenbto ,fifteencfrom ,fifteencto ,sixteen ,sixteenafrom ,sixteenato ,sixteenbfrom ,sixteenbto ,sixteenonefrom ,sixteenoneto,sixteentwofrom ,sixteentwoto ,sixteenthreefrom ,sixteenthreeto ,sixteenforefrom ,sixteenforeto ,sixteenfivefrom ,sixteenfiveto ,sixteensixfrom,sixteensixto ,sixteenconefrom ,sixteenconetwo ,sixteenctwofrom ,sixteenctwotwo ,sixteencthreefrom ,sixteencthreetwo ,sixteencforefrom ,sixteencforetwo ,sixteend ,sixteene ,seventeenfrom ,seventeento ,seventeenrateofpay ,seventeenaamount ,seventeenatotal,seventeenb ,seventeenc ,seventeend ,seventeene ,seventeeene ,deathorretirement ,twentytwoa ,twentytwob ,twentytwoc ,twentythreea ,twentythreeb ,twentiforea ,twentiforeb ,twentiforec ,twentyfive ,twentysix ,twentysevena,twentysevenb ,twentysevenc ,twentysevend ,twentysevene ,bankaddress ,dateofreciptpensionpaper,lengthofQP ,classOfPension ,Amountofmonthlypension ,DateofCommencement ,parttwoone ,parttwotwo ,parttwothree,parttwofore ,parttwodone ,parttwodtwo ,parttwodthree ,parttwodfore ,parttwoeone ,parttwoetwo,parttwoethree,parttwothreee ,dateofappointment ,place ,dateall ,Creation_Company ,Created_By ,Created_Date ,Modified_By ,Modified_Date)
values
(
@Status ,
@PensionNo ,
@empid ,
@empname ,
@fathername ,
@designation,
@department,
@DOB ,
@retirment ,
@seven ,
@eighta ,
@eightb ,
@bankname ,
@acno ,
@acoffice ,
@fathernaame ,
@height ,
@partonesix ,
@partoneseven,
@partonesevena ,
@partonesevenb ,
@partonesevenc ,
@partoneeight ,
@servicebegining ,
@endingservice ,
@levena,
@levenb ,
@levenc ,
@levend ,
@levene,
@levenf ,
@leveng ,
@levenh ,
@leveni,
@levenj,
@levenk ,
@tweleve,
@thirten ,
@forteena ,
@forteenb ,
@forteenc ,
@forteend ,
@fifteena,
@fiteenbfrom,
@fifteenbto,
@fifteencfrom,
@fifteencto ,
@sixteen ,
@sixteenafrom ,
@sixteenato ,
@sixteenbfrom,
@sixteenbto ,
@sixteenonefrom ,
@sixteenoneto ,
@sixteentwofrom ,
@sixteentwoto,
@sixteenthreefrom ,
@sixteenthreeto,
@sixteenforefrom ,
@sixteenforeto,
@sixteenfivefrom ,
@sixteenfiveto ,
@sixteensixfrom,
@sixteensixto ,
@sixteenconefrom,
@sixteenconetwo,
@sixteenctwofrom ,
@sixteenctwotwo,
@sixteencthreefrom,
@sixteencthreetwo,
@sixteencforefrom,
@sixteencforetwo,
@sixteend,
@sixteene ,
@seventeenfrom ,
@seventeento ,
@seventeenrateofpay,
@seventeenaamount,
@seventeenatotal,
@seventeenb,
@seventeenc,
@seventeend,
@seventeene,
@seventeeene,
@deathorretirement,
@twentytwoa,
@twentytwob,
@twentytwoc,
@twentythreea,
@twentythreeb,
@twentiforea ,
@twentiforeb,
@twentiforec,
@twentyfive,
@twentysix ,
@twentysevena,
@twentysevenb,
@twentysevenc,
@twentysevend,
@twentysevene,
@bankaddress ,
@dateofreciptpensionpaper ,
@lengthofQP ,
@classOfPension,
@Amountofmonthlypension,
@DateofCommencement,
@parttwoone ,
@parttwotwo ,
@parttwothree,
@parttwofore,
@parttwodone,
@parttwodtwo,
@parttwodthree,
@parttwodfore,
@parttwoeone,
@parttwoetwo,
@parttwoethree,
@parttwothreee,
@dateofappointment,
@place,
@dateall,
@Creation_Company,
@Created_By,
@Created_Date,
@Modified_By,
@Modified_Date
)
end
end

GO
/****** Object:  StoredProcedure [dbo].[sp_insert_HRMS_pension_state]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_insert_HRMS_pension_state](
@Sno int,
@pensionno varchar(max),
@Status varchar(max),
@empid varchar(250),
@empname varchar(max),
@postheld varchar(max),
@NOA varchar(max),
@permanentaddress varchar(max),
@Addressafterretirment varchar(max),
@copa varchar(max),
@copb varchar(max),
@nameoPDA varchar(max),
@bankname varchar(max),
@bankacno varchar(max),
@Fathername varchar(max),
@DOB date,
@DOES date,
@designation varchar(max),
@eightb varchar(max),
@eightc varchar(max),
@totalservice varchar(max),
@tena varchar(max),
@tenb varchar(max),
@tenc varchar(max),
@tend varchar(max),
@tene varchar(max),
@NQS varchar(max),
@weightage varchar(max),
@TotalQSerive varchar(max),
@LPDRAWN money,
@servicepensiongravity money,
@seventiena money,
@seventienb money,
@eightenafrom date,
@eightenatilldeath varchar(max),
@eightenbfrom date,
@eightenbto date,
@eightencfrom date,
@eightencto varchar(max),
@ninetenap money,
@ninetenai money,
@ninetenatotal money,
@ninetenbp money,
@ninetenbi money,
@ninetenbtotal money,
@ninetencp money,
@ninetenci money,
@ninetenctotal money,
@ninetendp money,
@ninetendi money,
@ninetendtotal money,
@ninetenep money,
@ninetenei money,
@ninetenetotal money,
@ninetenfp money,
@ninetenfi money,
@ninetenftotal money,
@ninetengp money,
@ninetengi money,
@ninetengtotal money,
@ninetenhp money,
@ninetenhi money,
@ninetenhtotal money,
@ninetenip money,
@ninetenii money,
@ninetenitotal money,
@ninetenjp money,
@ninetenji money,
@ninetenjtotal money,
@ninetenkp money,
@ninetenki money,
@ninetenktotal money,
@ninetentotal money,
@retiringgravity money,
@commutating money,

@nominiesname varchar(max),
@personalidentifications varchar(max),
@Place varchar(max),
@datall date,
@Creation_Company varchar(max),
@Created_By varchar(max),
@Created_Date date,
@Modified_By varchar(max),
@Modified_Date date,
@parameter int)
as
begin
if(@parameter=1)

begin
insert into HRMS_pension_state

([Status],empid,empname,postheld,NOA,permanentaddress,Addressafterretirment,copa,copb,nameoPDA,bankname,bankacno,Fathername,DOB,DOES,designation,eightb,eightc,totalservice,tena,tenb,tenc,tend,tene,NQS,weightage,TotalQSerive,LPDRAWN,servicepensiongravity,seventiena,seventienb,eightenafrom,eightenatilldeath,eightenbfrom,eightenbto,eightencfrom,eightencto,ninetenap,ninetenai,ninetenatotal,ninetenbp,ninetenbi,ninetenbtotal,ninetencp,ninetenci,ninetenctotal,ninetendp,ninetendi,ninetendtotal,ninetenep,ninetenei,ninetenetotal,ninetenfp,ninetenfi,ninetenftotal,ninetengp,ninetengi,ninetengtotal,ninetenhp,ninetenhi,ninetenhtotal,ninetenip,ninetenii,ninetenitotal,ninetenjp,ninetenji,ninetenjtotal,ninetenkp,ninetenki,ninetenktotal,ninetentotal,retiringgravity,commutating,nominiesname,personalidentifications,Place,datall,Creation_Company,Created_By,
Created_Date,
Modified_By,
Modified_Date)
values
(@Status,
@empid,
@empname,
@postheld,
@NOA,
@permanentaddress,
@Addressafterretirment,
@copa,
@copb,
@nameoPDA,
@bankname,
@bankacno,
@Fathername,
@DOB,
@DOES,
@designation,
@eightb,
@eightc,
@totalservice,
@tena,
@tenb,
@tenc,
@tend,
@tene,
@NQS,
@weightage,
@TotalQSerive,
@LPDRAWN,
@servicepensiongravity,
@seventiena,
@seventienb,
@eightenafrom,
@eightenatilldeath,
@eightenbfrom,
@eightenbto,
@eightencfrom,
@eightencto,
@ninetenap,
@ninetenai,
@ninetenatotal,
@ninetenbp,
@ninetenbi,
@ninetenbtotal,
@ninetencp,
@ninetenci,
@ninetenctotal,
@ninetendp,
@ninetendi,
@ninetendtotal,
@ninetenep,
@ninetenei,
@ninetenetotal,
@ninetenfp,
@ninetenfi,
@ninetenftotal,
@ninetengp,
@ninetengi,
@ninetengtotal,
@ninetenhp,
@ninetenhi,
@ninetenhtotal,
@ninetenip,
@ninetenii,
@ninetenitotal,
@ninetenjp,
@ninetenji,
@ninetenjtotal,
@ninetenkp,
@ninetenki,
@ninetenktotal,
@ninetentotal,
@retiringgravity,
@commutating,

@nominiesname,
@personalidentifications,
@Place,
@datall,
@Creation_Company,
@Created_By,
@Created_Date,
@Modified_By,
@Modified_Date
)
end
end

GO
/****** Object:  StoredProcedure [dbo].[Sp_Insert_Select_Update_Administrator_CompanyInfo]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_Insert_Select_Update_Administrator_CompanyInfo]

@ClientRegistrationNo varchar(10),
@Company_Name  varchar(150),
@Address1  varchar(250),
@Address2 varchar(250),
@State varchar(20),
@City varchar(20),
@Pincode varchar(6),
@Phone_No varchar(25),
@E_Mail varchar(25),
@Web_URL varchar(25),
@ECC_No varchar(15),
@TIN_No varchar(15),
@CE_Range varchar(15),
@CE_Division varchar(15),
@Fax_No varchar(25),
@CE_Commissionarate varchar(15),
@CST_No varchar(15),
@PF_Code	varchar(20),
@PF_Region varchar(50),
@PF_Office varchar(50),
@ESI_Reg_Code varchar(20),
@ESI_Branch varchar(50),
@PT_Reg_No varchar(30),
@PAN_No	varchar(20),
@Weak_Off varchar(50),
@Status varchar(10),
@Creation_Company varchar(200),
@Created_By varchar(50),
@Created_Date datetime,
@Modified_By  varchar(50),
@Modified_Date datetime,
@Parameter int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	If(@Parameter=1)
	BEGIN

	INsert INTO Administrator_CompanyInfo
	(
		ClientRegistrationNo,Company_Name,Address1,Address2,State,City,Pincode,Phone_No,E_Mail,Web_URL,
		ECC_No,TIN_No,CE_Range,CE_Division,Fax_No,CE_Commissionarate,CST_No,
		PF_Code,PF_Region,PF_Office,ESI_Reg_Code,ESI_Branch,PT_Reg_No,PAN_No,Weak_Off,
		Status,Creation_Company,Created_By,Created_Date
	)

	VALUES
	(
		[dbo].[Fn_autoincrement_Administrator_CompanyInfo](@Creation_Company) ,@Company_Name,@Address1,@Address2,@State,@City,@Pincode,
		@Phone_No,@E_Mail,@Web_URL,	@ECC_No,@TIN_No,@CE_Range,@CE_Division,@Fax_No,@CE_Commissionarate,@CST_No,
		@PF_Code,@PF_Region,@PF_Office,@ESI_Reg_Code,@ESI_Branch,@PT_Reg_No,@PAN_No,@Weak_Off,	@Status,
		@Creation_Company,@Created_By,@Created_Date
	)

	END

	---- Update statements for procedure here

	if(@Parameter=2)

	BEGIN

	UPDATE Administrator_CompanyInfo SET  Company_Name=@Company_Name,
	Address1=@Address1,Address2=@Address2,State=@State,City=@City,Pincode=@Pincode,Phone_No=@Phone_No,E_Mail=@E_Mail,
	Web_URL=@Web_URL,ECC_No=@ECC_No,TIN_No=@TIN_No,CE_Range=@CE_Range,CE_Division=@CE_Division,Fax_No=@Fax_No,
	CE_Commissionarate=@CE_Commissionarate,CST_No=@CST_No,	
	PF_Code=@PF_Code,PF_Region=@PF_Region,PF_Office=@PF_Office,ESI_Reg_Code=@ESI_Reg_Code,ESI_Branch=@ESI_Branch,
	PT_Reg_No=@PT_Reg_No,PAN_No=@PAN_No,Weak_Off=@Weak_Off,	
	Status=@Status,Modified_By=@Modified_By,Modified_Date=@Modified_Date
	WHERE Creation_Company=@Creation_Company and ClientRegistrationNo=@ClientRegistrationNo

	END

	if(@Parameter=3)

	Begin

	Select *From Administrator_CompanyInfo WHERE Creation_Company=@Creation_Company 

	END


END






GO
/****** Object:  StoredProcedure [dbo].[Sp_Insert_Select_Update_Administrator_CompanyInfo_BankDetails]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_Insert_Select_Update_Administrator_CompanyInfo_BankDetails]

@ClientRegistrationNo varchar(10),
@Bank_Name	varchar(50),
@Account_Number	varchar(20),
@Branch	varchar(50),

@Creation_Company varchar(200),
@Created_By varchar(50),
@Created_Date datetime,
@Modified_By  varchar(50),
@Modified_Date datetime,
@Parameter int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	If(@Parameter=1)
	BEGIN

	DECLARE @Report_NoAI varchar(10)       
   select @Report_NoAI=MAX(ClientRegistrationNo) from Administrator_CompanyInfo --order by SNo desc  


	INsert INTO [dbo].[Administrator_CompanyInfo_BankDetails]
	(
		ClientRegistrationNo,Bank_Name,Account_Number,Branch,
		Creation_Company,Created_By,Created_Date
	)

	VALUES
	(
		@Report_NoAI,@Bank_Name,@Account_Number,@Branch,
		@Creation_Company,@Created_By,@Created_Date
	)

	END

	---- Update statements for procedure here

	if(@Parameter=2)

	BEGIN

	UPDATE [dbo].[Administrator_CompanyInfo_BankDetails] SET   
	Bank_Name=@Bank_Name,Account_Number=@Account_Number,Branch=@Branch,
	Modified_By=@Modified_By,Modified_Date=@Modified_Date
	WHERE Creation_Company=@Creation_Company and ClientRegistrationNo=@ClientRegistrationNo

	END

	if(@Parameter=3)

	Begin

	Select *From [dbo].[Administrator_CompanyInfo_BankDetails] WHERE Creation_Company=@Creation_Company 

	END
	 -- Insert statements for procedure here
	If(@Parameter=4)
	BEGIN

--	DECLARE @Report_NoAI varchar(10)       
  -- select @Report_NoAI=MAX(ClientRegistrationNo) from Administrator_CompanyInfo --order by SNo desc  


	INsert INTO [dbo].[Administrator_CompanyInfo_BankDetails]
	(
		ClientRegistrationNo,Bank_Name,Account_Number,Branch,
		Creation_Company,Created_By,Created_Date
	)

	VALUES
	(
		@ClientRegistrationNo,@Bank_Name,@Account_Number,@Branch,
		@Creation_Company,@Created_By,@Created_Date
	)

	END


END




GO
/****** Object:  StoredProcedure [dbo].[Sp_Insert_Select_Update_Administrator_UserRegistration]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_Insert_Select_Update_Administrator_UserRegistration]  
  
@Login_Id varchar(20),  
@Password  varchar(20),  
@User_Name varchar(20),  
@E_Mail  varchar(50),  
@User_Type  varchar(10),  
  
@AllowCompanyListID varchar(250),  
@AllowCompanyList varchar(250),  
  
@ClientRegistrationNo  varchar(10),  
@Modules  varchar(200),  
@Status  varchar(10),  
  
@Administrator_Create varchar(250),  
@Administrator_Modify varchar(250),  
@Administrator_Approve varchar(250),  
@Administrator_View varchar(250),  
@Master_Create varchar(250),  
@Master_Modify varchar(250),  
@Master_Approve varchar(250),  
@Master_View varchar(250),  
@Transcation_Create varchar(250),  
@Transcation_Modify varchar(250),  
@Transcation_Approve varchar(250),  
@Transcation_View varchar(250),  
@Reports_Create varchar(250),  
@Reports_Modify varchar(250),  
@Reports_Approve varchar(250),  
@Reports_View varchar(250),  
  
@Add_Create varchar(10),  
@Modify varchar(10),  
@Rec_Delete varchar(10),  
@Approve_Authorize varchar(10),  
@View_Print varchar(10) ,  
@Creation_Company  varchar(200),  
@Created_By varchar(30),  
@Created_Date datetime,  
@Modified_By  varchar(50),  
@Modified_Date datetime,  
@Parameter int  
  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
 If(@Parameter=1)  
 BEGIN  
  
 INsert INTO Administrator_UserRegistration  
 (  
  Login_Id,Password,User_Name,E_Mail,User_Type,  
   AllowCompanyListID,AllowCompanyList,    
  ClientRegistrationNo,Modules,Status,  
  
  Administrator_Create,Administrator_Modify,Administrator_Approve,Administrator_View,  
  Master_Create,Master_Modify,Master_Approve,Master_View,Transcation_Create,Transcation_Modify,  
  Transcation_Approve,Transcation_View,Reports_Create,Reports_Modify,Reports_Approve,Reports_View,  
  Add_Create,Modify,Rec_Delete,Approve_Authorize,View_Print,  
  Creation_Company,  
  Created_By,Created_Date  
 )  
  
 VALUES  
 (  
  @Login_Id,@Password,@User_Name,@E_Mail,@User_Type, @AllowCompanyListID,@AllowCompanyList,    
    
  @ClientRegistrationNo,@Modules,@Status,  
  
  @Administrator_Create,@Administrator_Modify,@Administrator_Approve,@Administrator_View,  
  @Master_Create,@Master_Modify,@Master_Approve,@Master_View,@Transcation_Create,@Transcation_Modify,  
  @Transcation_Approve,@Transcation_View,@Reports_Create,@Reports_Modify,@Reports_Approve,@Reports_View,  
  
  @Add_Create,@Modify,@Rec_Delete,@Approve_Authorize,@View_Print,  
  @Creation_Company,  
  @Created_By,@Created_Date  
 )  
  
 END  
  
 ---- Update statements for procedure here  
  
 if(@Parameter=2)  
  
 BEGIN  
  
 UPDATE [dbo].[Administrator_UserRegistration] SET  User_Name=@User_Name,  
 E_Mail=@E_Mail,User_Type=@User_Type,  
 AllowCompanyListID=@AllowCompanyListID,AllowCompanyList=@AllowCompanyList,   
   
 ClientRegistrationNo=@ClientRegistrationNo,Modules=@Modules,Status=@Status,  
  
 Administrator_Create=@Administrator_Create,Administrator_Modify=@Administrator_Modify,Administrator_Approve=@Administrator_Approve,  
 Administrator_View=@Administrator_View,Master_Create=@Master_Create,Master_Modify=@Master_Modify,Master_Approve=@Master_Approve,  
 Master_View=@Master_View,Transcation_Create=@Transcation_Create,Transcation_Modify=@Transcation_Modify,  
  Transcation_Approve=@Transcation_Approve,Transcation_View=@Transcation_View,Reports_Create=@Reports_Create,  
  Reports_Modify=@Reports_Modify,Reports_Approve=@Reports_Approve,Reports_View=@Reports_View,  
  
  
 Add_Create=@Add_Create,Modify=@Modify,Rec_Delete=@Rec_Delete,Approve_Authorize=@Approve_Authorize,  
 View_Print=@View_Print, Modified_By=@Modified_By,Modified_Date=@Modified_Date   
 where Creation_Company=@Creation_Company and Login_Id=@Login_Id  
  
 END  
  
 if(@Parameter=3)  
  
 Begin  
  
 Select *From Administrator_UserRegistration WHERE Creation_Company=@Creation_Company   
  
 END  
  
  
END



GO
/****** Object:  StoredProcedure [dbo].[Sp_SCM_Change_Password]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_SCM_Change_Password]      
@Login_Id varchar(20),      
@Password varchar(20),    
@Modified_Date datetime,  
@Creation_Company varchar(50)  
          
             
AS                    
BEGIN                    
         
 update 
      SCM_Administrator_UserRegistration SET     
       Password=@Password,  
    Modified_Date=@Modified_Date  
     WHERE Login_Id =@Login_Id  and  Creation_Company=@Creation_Company        
                    
 END




GO
/****** Object:  StoredProcedure [dbo].[sp_Sections]    Script Date: 09/23/2017 10:42:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  Create procedure [dbo].[sp_Sections](@parameter nvarchar(50))
 as
 begin
 
  select * FROM [dbo].[section_mst] where range_cd like @parameter + '%'
  end
GO
