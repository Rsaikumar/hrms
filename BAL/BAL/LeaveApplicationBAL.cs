﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data.Linq.SqlClient;

namespace BAL
{
    public class LeaveApplicationBAL
    {

        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();
        public int ID { get; set; }
        public string EmpCode { get; set; }

        public string EmpName { get; set; }

        public string ReportingAuthority { get; set; }
        public string TypeOfLeaveApple { get; set; }
        public DateTime LeaveFrom { get; set; }

        public DateTime LeaveTo { get; set; }
        public decimal NoofDays { get; set; }

        public string LeaveStatus { get; set; }

        public DateTime AppliedDate { get; set; }

        public string CreationCompany { get; set; }


        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime ModifiedDate { get; set; }

        public string CerificationPath { get; set; }

        public int Parameter { get; set; }

        // Inserting Leave Application
        public int Insert_HRMS_Leave_Application()
        {
            try
            {
                objDAL.Usp_HRMSInsertSelectUpdateHRMSLeaveApplication(ID,EmpCode, ReportingAuthority, TypeOfLeaveApple,LeaveFrom,
                    LeaveTo, NoofDays, LeaveStatus, AppliedDate, CreationCompany, CreatedBy, CreatedDate, ModifiedBy,
                  ModifiedDate, Parameter, CerificationPath);
                return 1;
            }

            catch (Exception ex)
            {
                string msg = ex.Message;
                return 0;
            }
        }
        // Edit Leave Application
       

        
       

       










    }
}
