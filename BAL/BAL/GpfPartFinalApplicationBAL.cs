﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;

namespace BAL
{
   public class GpfPartFinalApplicationBAL
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();
        public int ID { get; set; }
        public string NameOfSubscriber { get; set; }
        public string Designation { get; set; }
        public string Pay { get; set; }
        public string NameOfProvidentFund { get; set; }
        public decimal BalanceOn { get; set; }
        public decimal SecondPartFinalDrawl { get; set; }
        public decimal AlreadyTakenAmount { get; set; }
        public decimal BalanceOfTakenAmount { get; set; }
        public decimal RequiredAmount { get; set; }
        public decimal AmountRequiredHouse { get; set; }
        public string Purpose { get; set; }
        public string ForPurchasing { get; set; }
        public string ForConstructing { get; set; }
        public string ForRedemption { get; set; }
        public string ForMakingAdditions { get; set; }
        public string ForHouseSite { get; set; }
        public string ForRepaymet { get; set; }
        public string LoanUnder { get; set; }
        public string HigherEducation { get; set; }
        public string RelationWithdrawal { get; set; }
        public string Institution { get; set; }
        public string SchoolYears { get; set; }
        public DateTime WithdrawalCurrentYear { get; set; }
        public DateTime DateWithdrawal { get; set; }
        public string MarriageBetrothal { get; set; }
        public string RelationDependent { get; set; }
        public string PresentWithDrawal { get; set; }

        public DateTime ActualDate { get; set; }
        public decimal MedicalExpenses { get; set; }
        public decimal TotalService { get; set; }
        public DateTime PeriodOfService { get; set; }
        public DateTime DateSuperannuation { get; set; }
        public decimal ActualCost { get; set; }
        public decimal AnticipatedCost { get; set; }
        public decimal AnticipatedCostOfAdditions { get; set; }
        public string ForRedemptionHouse { get; set; }
        public string LoanTakendateAmount { get; set; }
        public decimal ExpensesRequired { get; set; }
        public decimal AmountMarriage { get; set; }
        public decimal AmountHouseSites { get; set; }
        public decimal AmountProvidentFund { get; set; }
        public int DesignationNo { get; set; }
        public DateTime DatedThe { get; set; }
        public string Particularsverified { get; set; }
        public int RecordNo { get; set; }
        public DateTime Date { get; set; }
        public string ReportingAuthority { get; set;}
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int parameter { get; set; }

        public int InsertHRMSGpfPartFinalApplication()
        {
            try
            {
                objDAL.Usp_HRMSSelectUpdateGPFPartFinalApplication(ID, NameOfSubscriber, Designation,  Pay,  NameOfProvidentFund,  BalanceOn,
      SecondPartFinalDrawl, AlreadyTakenAmount, BalanceOfTakenAmount, RequiredAmount, AmountRequiredHouse, Purpose, ForPurchasing, ForConstructing, ForRedemption,
      ForMakingAdditions,ForHouseSite, ForRepaymet, LoanUnder ,HigherEducation, RelationWithdrawal, Institution, SchoolYears, WithdrawalCurrentYear,
      DateWithdrawal, MarriageBetrothal,RelationDependent, PresentWithDrawal,  ActualDate,  MedicalExpenses,  TotalService, PeriodOfService,
      DateSuperannuation, ActualCost, AnticipatedCost, AnticipatedCostOfAdditions, ForRedemptionHouse, LoanTakendateAmount, ExpensesRequired, AmountMarriage,  AmountHouseSites,
      AmountProvidentFund,  DesignationNo,  DatedThe, Particularsverified, RecordNo,  Date, ReportingAuthority,
      CreatedBy, CreatedOn, ModifiedBy, ModifiedOn,parameter );
                return 1;
            }

            catch (Exception ex)
            {
                string msg = ex.Message;
                return 0;
            }
        }

    }
}

    
   