﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data.Linq.SqlClient;
namespace BAL
{
    public class JobTypeBAL
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

        public int Sno { get; set; }
        public string Job_Id  { get; set; }
        public string Job_Name { get; set; }
        public string Creation_Company { get; set; }
        public string Created_By { get; set; }
        public DateTime Created_Date { get; set; }

        public string Modified_By { get; set; }
        public DateTime Modified_Date { get; set; }
        public int Parameter { get; set; }

        public int Insert_JobType_Master()
        {
            try
            {
                objDAL.Sp_HRMS_Insert_Select_Update_HRMS_JobType_Master(Job_Id
                    , Job_Name, Creation_Company, Created_By, Created_Date, Modified_By, Modified_Date, Parameter);
                return 1;
            }

            catch (Exception ex)
            {
                string msg = ex.Message;
                return 0;
            }
        }

        public List<HRMS_JobType_Master> GetData_JobTypeMaster(string Creation_Company)
        {
            try
            {
                List<HRMS_JobType_Master> ObjGroup = (from master in objDAL.HRMS_JobType_Masters
                                                      where master.Creation_Company == Creation_Company
                                                    select master).ToList();
                return ObjGroup;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return new List<HRMS_JobType_Master>();
            }
        }
    }
}
