﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data.Linq.SqlClient;

namespace BAL
{
    public class HolidaysListBAL
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();
        public string Holiday_Code{ get; set; }
        public string Year { get; set; }
        public DateTime Date { get; set; }
        public string Event_Name { get; set; }
        public string Creation_Company { get; set; }
        public string Created_By { get; set; }
        public DateTime Created_Date { get; set; }
        public string Modified_By { get; set; }
        public DateTime Modified_Date { get; set; }
        public int Parameter { get; set; }


        #region Database Methods
        // Inserting Header Details
        public int Insert_HRMS_Holiday_Master()
        {
            try
            {
                //objDAL.Sp_HRMS_Insert_Select_Update_HRMS_Holiday_Master(Holiday_Code,Year, Date, Event_Name, Creation_Company, Created_By, Created_Date, Modified_By,
                //  Modified_Date, Parameter);
                return 1;
            }

            catch (Exception ex)
            {
                string msg = ex.Message;
                return 0;
            }
        }


      
        // Delete Method
        public HRMS_Holiday_Master Delete_Holiday_Master(string Holiday_Code, string Creation_Company)
        {

            try
            {
                HRMS_Holiday_Master SUM = (from ST in objDAL.HRMS_Holiday_Masters
                                           where ST.Creation_Company == Creation_Company 
                                         select ST).FirstOrDefault();
                objDAL.HRMS_Holiday_Masters.DeleteOnSubmit(SUM);
                objDAL.SubmitChanges();

                return SUM;

            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
                return new HRMS_Holiday_Master();
            }
        }

        // Edit Group Details
        public List<HRMS_Holiday_Master> Get_Holiday_Details(string Holiday_Code, string Creation_Company)
        {
            try
            {
                List<HRMS_Holiday_Master> objGroup = (from ST in objDAL.HRMS_Holiday_Masters
                                                      where ST.Creation_Company == Creation_Company
                                                    select ST).ToList();
                return objGroup;
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
                return new List<HRMS_Holiday_Master>();
            }
        }
        #endregion
    }
}
