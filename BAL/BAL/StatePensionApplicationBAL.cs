﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;

namespace BAL
{
   public class StatePensionApplicationBAL
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

        public int ID { get; set; }
        public string Circle { get; set; }
        public string District { get; set; }
        public string SubDistrict { get; set; }
        public string EmployeeName { get; set; }
        public string ForwardServiceName { get; set; }
        public DateTime ExpiredWhileService { get; set; }
        public string Receipt { get; set; }
        public string CopyToName { get; set; }
        public string CopyAddress { get; set; }
        public string GovernmentServiceName { get; set; }
        public DateTime PostHeld { get; set; }
        public string PermanentAddress { get; set; }
        public string AddressAfterRetirement { get; set; }
        public string PensionDisbusingAutority { get; set; }
        public string BankName { get; set; }
        public string BankAccount { get; set; }
        public DateTime ApplicationOfPension { get; set; }
        public string LateName { get; set; }
        public string LateGuardianShip { get; set; }
        public DateTime DateOfEntering { get; set; }
        public DateTime DateOfRetirement { get; set; }
        public decimal FamilyPension { get; set; }
        public decimal NormalFamilyPension { get; set; }
        public decimal HousePrinciple { get; set; }
        public decimal HouseInterest { get; set; }
        public decimal HouseTotal { get; set; }
        public decimal MotorPrinciple { get; set; }
        public decimal MotorInterest { get; set; }
        public decimal MotorTotal { get; set; }
        public decimal MarriagePrinciple { get; set; }
        public decimal MarriageInterest { get; set; }
        public decimal MarriageTotal { get; set; }
        public decimal FestivalPrinciple { get; set; }
        public decimal FestivalInterest { get; set; }
        public decimal FestivalTotal { get; set; }
        public decimal EducationPrinciple { get; set; }
        public decimal EducationInterest { get; set; }
        public decimal EducationTotal { get; set; }
        public decimal ComputerPrinciple { get; set; }
        public decimal ComputerInterest { get; set; }
        public decimal ComputerTotal { get; set; }
        public DateTime NominationDate { get; set; }
        public DateTime DateThis { get; set; }
        public DateTime DateOf { get; set; }
        public string Year { get; set; }
        public string Place { get; set; }
        public string FromName { get; set; }
        public string FromAddress { get; set; }
        public string AuthorityName { get; set; }
        public string AuthorityAddress { get; set; }
        public string RetiringBenfitsName { get; set; }
        public string Designation { get; set; }
        public string PensionApplicationName { get; set; }
        public DateTime RetiredOn { get; set; }
        public string AsDesignationName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }

        public int Parameter { get; set; }




        public int InsertHRMSStatePensionApplication()
        {
            try
            {
                objDAL.Usp_HRMSInsertSelectUpdateStatePensionApplicationForm(ID, Circle, District, SubDistrict, EmployeeName, ForwardServiceName, ExpiredWhileService,
    Receipt, CopyToName, CopyAddress, GovernmentServiceName, PostHeld, PermanentAddress, AddressAfterRetirement, PensionDisbusingAutority, BankName,
    BankAccount, ApplicationOfPension, LateName, LateGuardianShip, DateOfEntering, DateOfRetirement, FamilyPension, NormalFamilyPension, HousePrinciple,
    HouseInterest, HouseTotal, MotorPrinciple, MotorInterest, MotorTotal, MarriagePrinciple, MarriageInterest, MarriageTotal, FestivalPrinciple, FestivalInterest,
    FestivalTotal, EducationPrinciple, EducationInterest, EducationTotal, ComputerPrinciple, ComputerInterest, ComputerTotal, NominationDate, DateThis,
    DateOf, Year, Place, FromName, FromAddress, AuthorityName, AuthorityAddress, RetiringBenfitsName, Designation, PensionApplicationName, RetiredOn, AsDesignationName,
    CreatedBy, CreatedOn, ModifiedBy, ModifiedOn,Parameter );

                return 1;
            }

            catch (Exception ex)
            {
                string msg = ex.Message;
                return 0;
            }
        }
    }

    }

