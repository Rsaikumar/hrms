﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data.Linq.SqlClient;

namespace BAL
{
    public class LoanapplicationBAL
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

        public int ID { get; set; }
        
        public string EmpCode { get; set; }

        
       
        public string ReportingAuthority { get; set; }
        public string TypeOfLoanApply { get; set; }
        public decimal BasicAmount { get; set; }
        public decimal LoanAmountRequired { get; set; }
        public decimal EligibilityAmount { get; set; }

        public decimal RateOfInterest { get; set; }
        public int Tenure { get; set; }
        public DateTime AppliedDate { get; set; }
        public string Status { get; set; }
       
        public string CreationCompany { get; set; }
      

        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime ModifiedDate { get; set; }

        public int Parameter { get; set; }

        // Inserting Leave Application...

        public int InsertHRMSLoanApplication()
        {
            try
            {
                objDAL.Usp_HRMSInsertSelectUpdateHRMSLoanApplication(ID, EmpCode,  ReportingAuthority, TypeOfLoanApply, BasicAmount, LoanAmountRequired, EligibilityAmount, RateOfInterest, Tenure,AppliedDate, Status, CreationCompany, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, Parameter);
                
                return 1;
            }

            catch (Exception ex)
            {
                string msg = ex.Message;
                return 0;
            }
        }
    }
}