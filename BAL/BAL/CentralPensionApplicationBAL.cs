﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;

namespace BAL
{
   public class CentralPensionApplicationBAL
    {
       ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

        public int ID { get; set; }
        public string ReceivedFrom { get; set; }
        public string AccountGeneral { get; set; }
        public decimal CommutatedPensionValue { get; set; }
        public decimal AmountOfResiduary { get; set; }
        public int ApplicationLetterNo { get; set; }
        public DateTime ApplicatioDate { get; set; }
        public DateTime ReceiptPartII { get; set; }
        public int CommutedDebitAccount { get; set; }
        public string NominateeName { get; set; }
        public string OfficerName { get; set; }
        public string OfficerDesignation { get; set; }
        public string OfficerAddress { get; set; }
        public DateTime Nomination { get; set; }
        public DateTime NominationCancel { get; set; }
        public int Form1Number { get; set; }
        public string Form1Designation { get; set; }
        public DateTime EarlierNominationOn { get; set; }
        public DateTime NotApplicant { get; set; }
        public DateTime DayOf { get; set; }
        public string WitnessSignature1 { get; set; }
        public string WitnessSignature2 { get; set; }
        public string SignatureOfGovServant { get; set; }
        public string NominatedBy { get; set; }
        public string NominatedDesignation { get; set; }
        public string OfficeOfTheOfficer { get; set; }
        public string DesignationOfTheOfficer { get; set; }
        public string FormIVName { get; set; }
        public DateTime FormIVEarlierNominationOn { get; set; }
        public string HeadOfficerName { get; set; }
        public string HeadOfficerDesignation { get; set; }
        public string HeadOfficerAddress { get; set; }
        public DateTime FormIVNominationDate { get; set; }
        public DateTime FormIVNominationCancelDate { get; set; }
        public int FormIVNumber { get; set; }
        public DateTime DatedThe { get; set; }
        public string OfficerPost { get; set; }
        public string NomineeName { get; set; }
        public string NomineeDesignation { get; set; }
        public DateTime TodayDate { get; set; }
        public string PresentDesignation { get; set; }
        public string ApplicantSignture { get; set; }
        public string ApplicantDesignation { get; set; }
        public string NameOfPensioner { get; set; }
        public string PensionerDesignation { get; set; }
        public string PersonName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }

        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int Parameter { get; set; }

        public int InsertHRMSCentralPensionApplication()
        {
            try
            {
                objDAL.Usp_HRMSSelectUpdateCentralPensionApplication(ID, ReceivedFrom, AccountGeneral, CommutatedPensionValue, AmountOfResiduary, ApplicationLetterNo,
     ApplicatioDate, ReceiptPartII,CommutedDebitAccount,NominateeName,OfficerName, OfficerDesignation,OfficerAddress,Nomination,NominationCancel,Form1Number,Form1Designation,
     EarlierNominationOn,NotApplicant,DayOf,WitnessSignature1, WitnessSignature2,SignatureOfGovServant,NominatedBy,NominatedDesignation,OfficeOfTheOfficer,DesignationOfTheOfficer,FormIVName,
     FormIVEarlierNominationOn, HeadOfficerName,HeadOfficerDesignation,HeadOfficerAddress,FormIVNominationDate,FormIVNominationCancelDate,FormIVNumber,DatedThe,
     OfficerPost,NomineeName,NomineeDesignation, TodayDate,  PresentDesignation,  ApplicantSignture, ApplicantDesignation, NameOfPensioner, PensionerDesignation,
     PersonName, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn, Parameter);

                return 1;
            }

            catch (Exception ex)
            {
                string msg = ex.Message;
                return 0;
            }
        }

    }
    }

