﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BAL
{
    public class DeductionsBAL
    {
        // IncomeTax Details table
        public string PF_Code { set; get; }
        public decimal Define_Amount { set; get; }
        public decimal Define_Range_From { set; get; }
        public decimal Define_Range_To { set; get; }
        public string Deduct_On_Heads { set; get; }

        //Other Details Table
        public string Define_Range_Other_HeadName { set; get; }
        public string Define_Range_Criteria { set; get; }
        //public decimal Define_Amount { set; get; }
        public string Deduct_On_Other_Heads { set; get; }

        //Pension Fund Details
        public decimal PF_Percentage { set; get; }
        public decimal PF_Emp_Contribution { set; get; }
        public string PF_Deduct { set; get; }
        public decimal ESI_Percentage { set; get; }
        public string ESI_Deduct { set; get; }

        //Pension Fund Details
        public decimal Define_Range_PT_From { set; get; }
        public decimal Define_Range_PT_To { set; get; }
        public decimal Define_PT_Amount { set; get; }
        public string Deduct_On_PT_Heads { set; get; }
        public string Salary_Range { set; get; }


        public string Creation_Company { set; get; }
        public string Created_By { set; get; }
        public DateTime Created_Date { set; get; }
        public string Modified_By { set; get; }
        public DateTime Modified_Date { set; get; }

        public int Parameter { get; set; }


    }
}
