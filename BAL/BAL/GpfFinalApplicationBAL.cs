﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;

namespace BAL
{
   public class GpfFinalApplicationBAL
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

        public int ID { get; set; }
        public string NameOfSubscriber { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string DesignationandOffice { get; set; }
        public string GPFAccountNo { get; set; }
        public string ResidentialAddress { get; set; }
        public string SlipEnclosed { get; set; }
        public DateTime DateOfRetirement { get; set; }
        public string WorkedLastYears { get; set; }
        public string TreasuryPayment { get; set; }
        public string Documents { get; set; }
        public string PersonalMarks { get; set; }
        public string ToSpecimen { get; set; }
        public string ThumbImpression { get; set; }
        public string CauseOfDeath { get; set; }
        public DateTime DateOfDeath { get; set; }
        public string ReligionOfDeceased { get; set; }
        public string Place { get; set; }
        public DateTime Date { get; set; }
        public string PayMonthOf { get; set; }
        public string OfficeBillNo { get; set; }
        public DateTime Dated { get; set; }
        public decimal ForRupee { get; set; }
        public decimal Rupees { get; set; }

        public string CashVoucherNo { get; set; }
        public string OfName { get; set; }
        public decimal GPFSubscription { get; set; }

        public decimal RefundAdvance { get; set; }
        public decimal AmountPartFinalI { get; set; }
        public DateTime AmountPartFinaldate { get; set; }
        public string AmountPartFinalVoucherNo { get; set; }
        public decimal AmountPartFinalII { get; set; }
        public DateTime AmountPartFinaldateII { get; set; }
        public string AmountPartFinalIIVoucherNo { get; set; }
        public string Station { get; set; }
        public string ReportingAuthority { get; set;}
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int Parameter { get; set; }

        public int InsertHRMSGpfFinalApplication()
        {
            try
            {
                objDAL.Usp_HRMSSelectUpdateGPFFinalApplication(ID, NameOfSubscriber, DateOfBirth, DesignationandOffice, GPFAccountNo, ResidentialAddress,
      SlipEnclosed, DateOfRetirement, WorkedLastYears, TreasuryPayment, Documents, PersonalMarks, ToSpecimen, ThumbImpression, CauseOfDeath,
      DateOfDeath, ReligionOfDeceased, Place, Date, PayMonthOf, OfficeBillNo, Dated, ForRupee, Rupees, CashVoucherNo, OfName,
      GPFSubscription, RefundAdvance, AmountPartFinalI, AmountPartFinaldate, AmountPartFinalVoucherNo, AmountPartFinalII,
      AmountPartFinaldateII, AmountPartFinalIIVoucherNo, Station, ReportingAuthority, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn, Parameter);

                return 1;
            }

            catch (Exception ex)
            {
                string msg = ex.Message;
                return 0;
            }
        }
    }
}
