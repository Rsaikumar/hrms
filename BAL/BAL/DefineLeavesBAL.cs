﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DAL;
using System.Data.Linq.SqlClient;

namespace BAL
{
    public class DefineLeavesBAL
    {

        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

        public int Sno { get; set; }
        public string LeaveCode { get; set; }
        public string LeaveName { get; set; }

        public string ShortName { get; set; }

        public string Monthly { get; set; }

        public decimal Monthly_Limit { get; set; }

        public string Encashment { get; set; }
        public string Balnces_Transfer { get; set; }
        public decimal Encashment_Balance { get; set; }
        public string Allotment { get; set; }

        public decimal Noof_leaves_Per_Year { get; set; }

        public string twoStage { get; set; }
        public string Authority1 { get; set; }

        public string Username1 { get; set; }

        public string Authority2 { get; set; }

        public string Username2 { get; set; }


        public string Need_Certification { get; set; }

        public string Period { get; set; }

        public decimal No_Of_Times { get; set; }

        public decimal No_Of_Days { get; set; }

        public string Authority3 { get; set; }


        public string UserName3 { get; set; }

        public string Creation_Company { get; set; }

        public string Created_By { get; set; }

        public DateTime Created_Date { get; set; }

        public string Modified_By { get; set; }

        public DateTime Modified_Date { get; set; }

        public int Parameter { get; set; }

        public int Insert_HRMS_Leaves_Master()
        {
            try
            {
                //objDAL.Sp_HRMS_Insert_Select_Update_HRMS_Leave_Master(Leave_Code, Leave_Name, Short_Name, Monthly
                //    , Monthly_Limit, Encashment, Balnces_Transfer, Encashment_Balance, Allotment,
                //    Noof_leaves_Per_Year, Need_Certification,twoStage, Authority1, Username1, Authority2,Username2, Creation_Company, Created_By, Created_Date, Modified_By,
                //  Modified_Date, Period, No_Of_Times, No_Of_Days, Authority3, UserName3, Parameter);

                return 1;
            }

            catch (Exception ex)
            {
                string msg = ex.Message;
                return 0;
            }
        }
        public List<HRMSLeaveMaster> Get_Leave(string LeaveCode, string CreationCompany)
        {
            try
            {
                List<HRMSLeaveMaster> objLeaves = (from ST in objDAL.HRMSLeaveMasters
                                                   where ST.CreationCompany == CreationCompany
                                                   select ST).ToList();
                return objLeaves;
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
                return new List<HRMSLeaveMaster>();
            }
        }

        public List<HRMSLeaveMaster> GetData_search_LeaveInfo(string SearchFiled, string CreationCompany)
        {
            try
            {
                string query = "%" + SearchFiled + "%";
                List<HRMSLeaveMaster> ObjHeadInfo = (from HeaderInfo in objDAL.HRMSLeaveMasters
                                                     where HeaderInfo.CreationCompany.ToLower().Trim() == Creation_Company.ToLower().Trim() &&
                                          SqlMethods.Like(HeaderInfo.ID.ToString(), query.ToLower().Trim()) ||
                                           SqlMethods.Like(HeaderInfo.LeaveName.ToLower().Trim(), query.ToLower().Trim()) ||
                                            SqlMethods.Like(HeaderInfo.ShortName.ToLower().Trim(), query.ToLower().Trim())

                                                     select HeaderInfo).ToList();
                return ObjHeadInfo;
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
                return new List<HRMSLeaveMaster>();
            }
        }



    }
}
