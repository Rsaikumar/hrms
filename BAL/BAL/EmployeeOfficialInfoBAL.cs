﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;

namespace BAL
{
    public class EmployeeOfficialInfoBAL
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

        public string Emp_Code { get; set; }
        public string Heads { get; set; }
        public decimal Amount { get; set; }
        public decimal Basic_Amount { get; set; }
        public decimal Gross_Amount { get; set; }
        public decimal DA { get; set; }
        public decimal HRA { get; set; }
        public decimal CCA { get; set; }
        public decimal OA { get; set; }
        public decimal PF { get; set; }
        public decimal EESI { get; set; }
        public decimal ESI { get; set; }
        public decimal PTAX { get; set; }
        public decimal TDS { get; set; }
        public decimal APGLI { get; set; }
        public decimal GIS { get; set; }
        public decimal HBA { get; set; }
        public decimal MCA { get; set; }
        public decimal BA { get; set; }
        public decimal LOAN { get; set; }
        public decimal OTHERS { get; set; }
        public decimal NET { get; set; }
        public string Criteria { get; set; }
        public string Salary_Range { get; set; }
        public string Bank_Name { get; set; }
        public string Account_Number { get; set; }
        public string Pan_Number { get; set; }
        //public string Created_By { get; set; }
        //public string Created_By { get; set; }
        //public string Created_By { get; set; }

        public string Creation_Company { get; set; }
        public string Created_By { get; set; }

        public DateTime Created_Date { get; set; }

        public string Modified_By { get; set; }

        public DateTime Modified_Date { get; set; }

        public int Parameter { get; set; }

        #region Insert Method


        public int Insert_Employeedetailsinfo()
        {
            try
            {
               // objDAL.Sp_HRMS_Insert_Select_Update_HRMS_Employee_Official_Info_Head_Details(Emp_Code, Heads, Amount, Basic_Amount, Gross_Amount, DA, HRA, CCA, OA, PF, EESI, ESI, PTAX, TDS, APGLI, GIS, HBA, MCA, BA, LOAN, OTHERS, NET, Criteria, Salary_Range, Creation_Company, Created_By, Created_Date, Modified_By, Modified_Date, Parameter);


                return 1;
            }
            catch (Exception Ex)
            {
                Ex.ToString();
                return 0;
            }
        }
        #endregion
    }
}