﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data.Linq.SqlClient;


namespace BAL
{
    public class DefineLoansBAL
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();
        public int ID { get; set; }
       
        public string LoanName { get; set; }

        public string ShortName { get; set; }
        public string Allotment { get; set; }
       
          public string CreationCompany { get; set; }

       

        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime ModifiedDate { get; set; }

        public int Parameter { get; set; }

        public int InsertHRMSLoansMaster()
        {
            try
            {
                //objDAL.Sp_HRMS_Insert_Select_Update_HRMS_Loan_Master(Loan_Code, Loan_Name, Short_Name, Allotment, Need_Certification, Authority1, Username1,
                //    Authority2, Username2, Creation_Company, Created_By, Created_Date, Modified_By, Modified_Date, Period, No_Of_Times, Authority3, UserName3, Parameter);
                
                objDAL.Usp_HRMSInsertSelectUpdateHRMSLoanMaster(LoanName,ShortName, Allotment,CreationCompany,
                      CreatedBy, CreatedDate, ModifiedBy,
                  ModifiedDate, Parameter);

                return 1;
            }

            catch (Exception ex)
            {
                string msg = ex.Message;
                return 0;
            }
        }
        public List<HRMSLoanMaster> Get_Loan(string LoanCode, string CreationCompany)
        {
            try
            {
                List<HRMSLoanMaster> objLoan = (from ST in objDAL.HRMSLoanMasters
                                                  where ST.CreationCompany == CreationCompany
                                                  select ST).ToList();
                return objLoan;
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
                return new List<HRMSLoanMaster>();
            }
        }
    }
}
