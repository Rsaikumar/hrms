﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;

namespace BAL
{
    public class Employee_Location_BAL
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

        public string Location_Id { get; set; }
        public string Location_Name { get; set; }

        //public string Created_By { get; set; }
        //public string Created_By { get; set; }
        //public string Created_By { get; set; }

        public string Creation_Company { get; set; }
        public string Created_By { get; set; }

        public DateTime Created_Date { get; set; }

        public string Modified_By { get; set; }

        public DateTime Modified_Date { get; set; }

        public int Parameter { get; set; }

        #region Insert Method

        public int Insert_EmpLocations()
        {
            try
            {

                objDAL.Sp_HRMS_Insert_Select_Update_HRMS_Employee_Work_Locations(Location_Id, Location_Name, Creation_Company, Created_By, Created_Date,
                    Modified_By, Modified_Date, Parameter);
                return 1;
            }
            catch (Exception Ex)
            {
                Ex.ToString();
                return 0;
            }
        }


        public List<HRMS_Employee_Work_Location> GetData_LocationMaster(string Creation_Company)
        {
            try
            {
                List<HRMS_Employee_Work_Location> ObjGroup = (from master in objDAL.HRMS_Employee_Work_Locations
                                                              where master.Creation_Company == Creation_Company
                                                              select master).ToList();
                return ObjGroup;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return new List<HRMS_Employee_Work_Location>();
            }
        }
        #endregion
    }
}
