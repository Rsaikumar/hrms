﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#region Private Using
using DAL;
#endregion
namespace BAL
{
    public class HRMS_ALL_Get_Methods
    {
        ERP_DatabaseDataContext objDb = new ERP_DatabaseDataContext();
        /// <summary>
        ///  The Following Method is used for Getting The Data Into Customer Details
        ///  Created By Divya
        /// </summary>
        #region Company Information
        /// <summary>
        ///  This Method Is used For Gtting Single Record For Company Information Master Table
        /// </summary>
        /// <param name="QuotNo"></param>
        /// <param name="Creation_Company"></param>
        /// <returns></returns>
        public List<Administrator_CompanyInfo> Get_AdminiStrator_Companyinformation(string ClientRegNo, string Creation_Company)
        {
            try
            {
                List<Administrator_CompanyInfo> objQuotFollow = (from ST in objDb.Administrator_CompanyInfos where ST.ClientRegistrationNo == ClientRegNo 
                                                                // && ST.Creation_Company == Creation_Company
                                                                 select ST).ToList();
                return objQuotFollow;
            }
            catch (Exception ex)
            {
                return new List<Administrator_CompanyInfo>();
            }
        }

        /// <summary>
        ///  This Method Is used For Gtting Single Record For Company Information Bank Details Table
        /// </summary>
        /// <param name="QuotNo"></param>
        /// <param name="Creation_Company"></param>
        /// <returns></returns>
        public List<Administrator_CompanyInfo_BankDetail> Get_AdminiStrator_Company_Bankinformation(string ClientRegNo, string Creation_Company)
        {
            try
            {
                List<Administrator_CompanyInfo_BankDetail> objQuotFollow = (from ST in objDb.Administrator_CompanyInfo_BankDetails where ST.ClientRegistrationNo == ClientRegNo && ST.Creation_Company == Creation_Company select ST).ToList();
                return objQuotFollow;
            }
            catch (Exception ex)
            {
                return new List<Administrator_CompanyInfo_BankDetail>();
            }
        }
        #endregion

    }
}
