﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data.Linq.SqlClient;
namespace BAL
{
    public class EmployeeGroupMasterBAL
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

        public int Sno { get; set; }
        public string Group_Code { get; set; }
        public string Group_Name { get; set; }

        public string Short_Name { get; set; }
        public string Group_type { get; set; }
        public string Applicable_Heads { get; set; }

        public string Creation_Company { get; set; }
        public string Created_By { get; set; }
        public DateTime Created_Date { get; set; }

        public string Modified_By { get; set; }
        public DateTime Modified_Date { get; set; }
        public int Parameter { get; set; }


        #region methods
        public int Insert_Group_Master()
        {
            try
            {
                objDAL.Sp_HRMS_Insert_Select_Update_HRMS_Group_Master(Group_Code
                    , Group_Name, Short_Name, Group_type, Applicable_Heads, Creation_Company, Created_By, Created_Date, Modified_By, Modified_Date, Parameter);
                return 1;
            }

            catch (Exception ex)
            {
                string msg = ex.Message;
                return 0;
            }
        }

        public List<HRMS_Group_Master> GetData_GroupMaster(string Creation_Company)
        {
            try
            {
                List<HRMS_Group_Master> ObjGroup = (from master in objDAL.HRMS_Group_Masters
                                                    where master.Creation_Company == Creation_Company
                                                         select master).ToList();
                return ObjGroup;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return new List<HRMS_Group_Master>();
            }
        }

        // Edit Group Details
        public List<HRMS_Group_Master> Get_Group_Details(string Group_Code, string Creation_Company)
        {
            try
            {
                List<HRMS_Group_Master> objGroup = (from ST in objDAL.HRMS_Group_Masters
                                                     where ST.Group_Code == Group_Code && ST.Creation_Company == Creation_Company
                                                         select ST).ToList();
                return objGroup;
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
                return new List<HRMS_Group_Master>();
            }
        }

        // Delete Method
        public HRMS_Group_Master Delete_Group_Master(string Group_Code, string Creation_Company)
        {

            try
            {
                HRMS_Group_Master SUM = (from ST in objDAL.HRMS_Group_Masters
                                         where ST.Creation_Company == Creation_Company && ST.Group_Code == Group_Code
                                         select ST).FirstOrDefault();
                objDAL.HRMS_Group_Masters.DeleteOnSubmit(SUM);
                objDAL.SubmitChanges();

                return SUM;

            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
                return new HRMS_Group_Master();
            }
        }

        /// <summary>
        ///  The Following Method is used for Search(Company Name) The Data Into HRMS_Group_Master Table
        /// </summary>
        /// <param name="ObjUnit"></param>

        public List<HRMS_Group_Master> GetData_search_GroupInfo(string SearchFiled, string Creation_Company)
        {
            try
            {
                string query = "%" + SearchFiled + "%";
                List<HRMS_Group_Master> ObjGroupInfo = (from GroupInfo in objDAL.HRMS_Group_Masters
                                                       where GroupInfo.Creation_Company.ToLower().Trim() == Creation_Company.ToLower().Trim() &&
                                                SqlMethods.Like(GroupInfo.Group_Code.ToLower().Trim(), query.ToLower().Trim()) ||
                                                 SqlMethods.Like(GroupInfo.Group_Name.ToLower().Trim(), query.ToLower().Trim()) ||
                                                  SqlMethods.Like(GroupInfo.Short_Name.ToLower().Trim(), query.ToLower().Trim()) ||
                                                  SqlMethods.Like(GroupInfo.Group_type.ToLower().Trim(), query.ToLower().Trim()) ||
                                                   SqlMethods.Like(GroupInfo.Applicable_Heads.ToLower().Trim(), query.ToLower().Trim()) 

                                                           select GroupInfo).ToList();
                return ObjGroupInfo;
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
                return new List<HRMS_Group_Master>();
            }
        }

        #endregion

    }
}
