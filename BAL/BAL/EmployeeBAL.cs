﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq.SqlClient;

using DAL;

namespace BAL
{
    public class EmployeeBAL
    {

        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

        #region Common Fields For All Employee Tables

        public int Sno { get; set; }
        public string Emp_Code { get; set; }
        public string Creation_Company { get; set; }

        public string Created_By { get; set; }

        public DateTime Created_Date { get; set; }

        public string Modified_By { get; set; }

        public DateTime Modified_Date { get; set; }

        public int EmpParameter { get; set; }


        public int Parameter { get; set; }
        #endregion

        #region Common Field for EducationalCertificationDetails & ProfessionalExperience
        public string Attach_File { get; set; }
        #endregion

        #region Employee Personal Details

        public string Emp_First_Name { get; set; }
        public string Emp_Middle_Name { get; set; }
        public string Emp_Last_Name { get; set; }
        public string Emp_Image { get; set; }

        public string Emp_Roll { get; set; }

        public string User_Name { get; set; }

        public string Password { get; set; }

        public DateTime Emp_DOB { get; set; }

        public decimal Emp_Age { get; set; }
        public DateTime Date_Of_Anniversary { get; set; }

        public string Gender { get; set; }

        public string Blood_Group { get; set; }

        public string Blood_Donor { get; set; }

        public string Marital_Status { get; set; }

        public string Nationality { get; set; }
        public string Religion { get; set; }

        public string Hobbies { get; set; }

        public string Languages_Known { get; set; }


        public string Curricular_Activities { get; set; }

        public string Emp_Status { get; set; }

        public DateTime Resigned_Date { get; set; }

        public string Educate { get; set; }

        public string Telephone { get; set; }

        #endregion

        #region Emp Personal Identification Data

        public string Driving_Licenece_Number { get; set; }
        public string Licence_Type { get; set; }

        public DateTime Licence_Exp_Date { get; set; }

        public string Licence_Attachment { get; set; }

        public string Aadhar_Number { get; set; }

        public string Aadhar_Attachment { get; set; }

        public string Pan_Number { get; set; }
        public string Pan_Attachment { get; set; }

        public string Passport_Number { get; set; }

        public DateTime Issued_Date { get; set; }

        public DateTime Exp_Date { get; set; }

        public string Passport_Attachment { get; set; }

        public string Bank_Name { get; set; }
        public string Account_Number { get; set; }

        public string Health_Insurance_Poly_No { get; set; }

        public string Sum_Insured { get; set; }


        public DateTime Renewal_Date { get; set; }


        #endregion

        #region Employee Contact Details

        public string Present_Address1 { get; set; }
        public string Present_Address2 { get; set; }
        public string Present_City { get; set; }

        public string PresentAsPrev { get; set; }
        public string Present_State { get; set; }

        public string Present_Pincode { get; set; }

        public string Present_Country { get; set; }

        public string Permanent_Address1 { get; set; }

        public string Permanent_Address2 { get; set; }
        public string Permanent_City { get; set; }

        public string Permanent_State { get; set; }

        public string Permanent_Pincode { get; set; }

        public string Permanent_Country { get; set; }

        public string Permanent_Mobile { get; set; }

        public string Permanent_Email { get; set; }
        public string Alternative_Email { get; set; }

        public string Skype_Id { get; set; }

        public string Linkedin_Id { get; set; }



        #endregion

        #region Employee Family Information

        public string Member_Name { get; set; }
        public string Member_Relation { get; set; }
        public DateTime Member_DOB { get; set; }

        public string Member_Occupation { get; set; }

        public string Member_ContactNo { get; set; }

        public string Member_Dependent { get; set; }

        public string Member_Nominee { get; set; }

        public string Member_Emergency_Contact { get; set; }
        #endregion

        #region Emp Educational Certification Details

        public string Qualification_Certification { get; set; }
        public string Institute_University { get; set; }

        public string Year { get; set; }

        public decimal Percentage { get; set; }

        #endregion

        #region Emp Educational Skill Details

        public string Skill_Name { get; set; }
        public string Skill_Level { get; set; }

        public string Description { get; set; }

        #endregion

        #region Emp Key Performance Details
        public string Objective_Goal { get; set; }
        public string Measurement { get; set; }
        public string Monitoring_Frequency { get; set; }
        public string Current_Level { get; set; }

        #endregion

        #region Emp Leaves Information

        public string Leave_Name { get; set; }
        public string Eligible_To_Aail { get; set; }

        public decimal Opening_Bal { get; set; }

        public decimal This_Year_Allotment { get; set; }

        public decimal Availed_As_On_Date { get; set; }

        public decimal Encashed_This_Year { get; set; }

        public decimal Balance_Available { get; set; }

        #endregion

        #region Emp Loans Information
        public string Loan_Type { get; set; }

        public decimal Loan_Amount { get; set; }

        public decimal Deduct_Per_Month { get; set; }

        public decimal Balance_As_On_Date { get; set; }

        #endregion

        #region Emp Professional Experience

        public string Emp_Name { get; set; }
        public string Emp_Designation { get; set; }
        public DateTime Period_From { get; set; }
        public DateTime Period_To { get; set; }
        public string Total_Duration { get; set; }
        #endregion

        #region Employee Official Details

        public string OfficialEmpCode { get; set; }
        public DateTime Date_Of_Join { get; set; }
        public string Desgnation { get; set; }
        public string Employee_Grade { get; set; }
        public string Department { get; set; }
        public string Reporting_Authority { get; set; }
        public string Working_Shifts { get; set; }
        public string PF { get; set; }
        public string PF_Code { get; set; }
        public string ESI { get; set; }
        public string ESI_Code { get; set; }
        public string ESI_Dispensary { get; set; }
        public string Profisional_Tax { get; set; }
        public string TDS { get; set; }
        public string Quarter_Accommodation { get; set; }
        public string Four_Wheeler { get; set; }
        public string Two_Wheeler { get; set; }
        public string Laptop { get; set; }
        public string Mobile_Phone { get; set; }
        public string Other_Facilities { get; set; }

        public int OfficialParameter { get; set; }

        public string Head { get; set; }
        public decimal Amount { get; set; }
        public string Frequency { get; set; }

        public decimal Basic_Amount { get; set; }
        public decimal Gross_Amount { get; set; }
        public decimal DA { get; set; }
        public decimal HRA { get; set; }
        public decimal CCA { get; set; }
        public string WorkType { get; set; }

        public string Recruit { get; set; }
        public string Location { get; set; }

        #endregion


        #region Methods
        // insertion for Personal Details table
        public int Insert_Employee_Details()
        {
            try
            {
                objDAL.Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_PersonalDetails(Emp_Code, Emp_Image, Emp_First_Name, Emp_Middle_Name,
                   Emp_Last_Name, Emp_Roll, Emp_Status, User_Name, Password, Emp_DOB, Emp_Age, Date_Of_Anniversary, Gender, Blood_Group,
                   Blood_Donor, Marital_Status, Nationality, Religion, Languages_Known, Hobbies, Curricular_Activities,
                   Driving_Licenece_Number, Licence_Type, Licence_Exp_Date, Licence_Attachment, Aadhar_Number,
                   Aadhar_Attachment, Pan_Number, Pan_Attachment, Passport_Number, Issued_Date, Exp_Date,
                   Passport_Attachment, Bank_Name, Account_Number, Health_Insurance_Poly_No, Sum_Insured,
                   Renewal_Date, Present_Address1, Present_Address2, Present_City, Present_State,
                   Present_Pincode, Present_Country, Permanent_Address1, Permanent_Address2, PresentAsPrev, Permanent_City, Permanent_State,
                  Permanent_Pincode, Permanent_Country, Permanent_Mobile, Permanent_Email, Alternative_Email,
                  Skype_Id, Linkedin_Id, Creation_Company, Created_By, Created_Date, Modified_By,
                  Modified_Date, Resigned_Date, Educate, Telephone, Parameter);
                return 1;
            }

            catch (Exception ex)
            {
                string msg = ex.Message;
                return 0;
            }
        }

        // insertion for Employee Family table
        public int Insert_Employee_Family_Information()
        {
            try
            {
                objDAL.Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_FamilyInformation(Emp_Code, Member_Name, Member_Relation,
                    Member_DOB, Member_Occupation, Member_ContactNo, Member_Dependent, Member_Nominee, Member_Emergency_Contact, Creation_Company, Created_By, Created_Date, Modified_By,
                  Modified_Date, Parameter);
                return 1;
            }

            catch (Exception ex)
            {
                string msg = ex.Message;
                return 0;
            }
        }

        // insertion for Key Performance table
        public int Insert_Employee_KeyPer_Information()
        {
            try
            {
                objDAL.Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_Key_PerformanceDetails(Emp_Code, Objective_Goal, Measurement, Monitoring_Frequency, Current_Level, Creation_Company, Created_By, Created_Date, Modified_By,
                  Modified_Date, Parameter);
                return 1;
            }

            catch (Exception ex)
            {
                string msg = ex.Message;
                return 0;
            }
        }


        // loan information insertion method
        public int Insert_Employee_Loan_Details()
        {
            try
            {
                objDAL.Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_Loans_Information(Emp_Code, Loan_Type, Loan_Amount,
                   Deduct_Per_Month, Balance_As_On_Date, Creation_Company, Created_By, Created_Date, Modified_By,
                  Modified_Date, Parameter);
                return 1;
            }

            catch (Exception ex)
            {
                string msg = ex.Message;
                return 0;
            }
        }

        // leave information insertion method
        public int Insert_Employee_Leave_Details()
        {
            try
            {
                objDAL.Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_Leaves_Information(Emp_Code, Leave_Name,
                    Eligible_To_Aail, Opening_Bal, This_Year_Allotment, Availed_As_On_Date,
                    Encashed_This_Year, Balance_Available, Creation_Company, Created_By, Created_Date, Modified_By,
                  Modified_Date, Parameter);
                return 1;
            }

            catch (Exception ex)
            {
                string msg = ex.Message;
                return 0;
            }
        }
        #endregion



        #region Method For Professional (Divya)

        public int Insert_Update_ProfessionalExperience()
        {
            try
            {
                objDAL.Sp_HRMS_Insert_Select_Update_HRMS_Employee_Master_Professional_Experience(Emp_Code,
                    Emp_Name, Emp_Designation, Period_From, Period_To, Total_Duration, Attach_File, Creation_Company, Created_By, Created_Date, Modified_By,
                  Modified_Date, Parameter);
                return 1;
            }

            catch (Exception ex)
            {
                string msg = ex.Message;
                return 0;
            }
        }
        #endregion



        #region Searching For Employee
        public object GetData_search_GroupInfo(string SearchFiled, string Creation_Company)
        {
            try
            {
                string query = "%" + SearchFiled + "%";
                var qry = objDAL.sp_HRMS_Employee_Personal_Official_Search(query, Creation_Company).ToList();
                return qry;
                //List<HRMS_Group_Master> ObjGroupInfo = (from GroupInfo in objDAL.HRMS_Group_Masters
                //                                        where GroupInfo.Creation_Company.ToLower().Trim() == Creation_Company.ToLower().Trim() &&
                //                                 SqlMethods.Like(GroupInfo.Group_Code.ToLower().Trim(), query.ToLower().Trim()) ||
                //                                  SqlMethods.Like(GroupInfo.Group_Name.ToLower().Trim(), query.ToLower().Trim()) ||
                //                                   SqlMethods.Like(GroupInfo.Short_Name.ToLower().Trim(), query.ToLower().Trim()) ||
                //                                    SqlMethods.Like(GroupInfo.Applicable_Heads.ToLower().Trim(), query.ToLower().Trim())

                //                                        select GroupInfo).ToList();

                //var qry = (from personal in objDAL.HRMS_Employee_Master_PersonalDetails
                //           join offical in objDAL.HRMS_Employee_Master_Official_Informations
                //           on personal.Emp_Code equals offical.Emp_Code
                //           where offical.Creation_Company.Trim().ToLower() == Creation_Company
                //           where personal.Creation_Company.Trim().ToLower() == Creation_Company &&
                //           SqlMethods.Like(personal.Emp_Code.ToLower().Trim(), query.ToLower().Trim()) ||
                //           SqlMethods.Like(personal.Emp_First_Name.ToLower().Trim(), query.ToLower().Trim()) ||
                //           SqlMethods.Like(personal.Emp_Middle_Name.ToLower().Trim(), query.ToLower().Trim()) ||
                //           SqlMethods.Like(personal.Emp_Last_Name.ToLower().Trim(), query.ToLower().Trim()) ||
                //            SqlMethods.Like(offical.Employee_Grade.ToLower().Trim(), query.ToLower().Trim()) ||
                //            SqlMethods.Like(offical.Designation.ToLower().Trim(), query.ToLower().Trim()) ||
                //            SqlMethods.Like(personal.Gender.ToLower().Trim(), query.ToLower().Trim()) ||
                //            SqlMethods.Like(personal.Emp_Status.ToLower().Trim(), query.ToLower().Trim())
                //            //select personal).ToList();
                //              select new
                //            {
                //                personal.Emp_Code,
                //                Name = personal.Emp_First_Name + "" + personal.Emp_Middle_Name + "" + personal.Emp_Last_Name,
                //                // Date=GetDate(personal.Emp_DOB),
                //                offical.Employee_Grade,
                //                offical.Designation,
                //                personal.Gender,
                //                personal.Emp_Status
                //            }).Distinct().ToList();


            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
                return new List<HRMS_Group_Master>();
            }
        }

        #endregion


        #region Edit Employee Details based on Employee Code in employee list page
        //Employee details based on employee code in employee list grid
        public List<HRMS_Employee_Master_PersonalDetail> Get_Employee_Details(string Emp_Code, string Creation_Company)
        {
            try
            {
                List<HRMS_Employee_Master_PersonalDetail> objEmpDetails = (from empPerson in objDAL.HRMS_Employee_Master_PersonalDetails
                                                                           where empPerson.Emp_Code == Emp_Code &&
                                                                           empPerson.Creation_Company == Creation_Company
                                                                           select empPerson).ToList();
                return objEmpDetails;
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
                return new List<HRMS_Employee_Master_PersonalDetail>();
            }
        }
        //get family details based on empcode and creation company
        public List<HRMS_Employee_Master_FamilyInformation> Get_Employee_Family_Details(string Emp_Code, string Creation_Company)
        {
            try
            {
                List<HRMS_Employee_Master_FamilyInformation> objEmpDetails = (from empPerson in objDAL.HRMS_Employee_Master_FamilyInformations
                                                                              where empPerson.Emp_Code == Emp_Code &&
                                                                           empPerson.Creation_Company == Creation_Company
                                                                              select empPerson).ToList();
                return objEmpDetails;
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
                return new List<HRMS_Employee_Master_FamilyInformation>();
            }
        }

        //get Professional details based on empcode and creation company
        public List<HRMS_Employee_Master_Professional_Experience> Get_Employee_Professional_Details(string Emp_Code, string Creation_Company)
        {
            try
            {
                List<HRMS_Employee_Master_Professional_Experience> objEmpDetails = (from empPerson in objDAL.HRMS_Employee_Master_Professional_Experiences
                                                                                    where empPerson.Emp_Code == Emp_Code &&
                                                                           empPerson.Creation_Company == Creation_Company
                                                                                    select empPerson).ToList();
                return objEmpDetails;
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
                return new List<HRMS_Employee_Master_Professional_Experience>();
            }
        }

        //get Professional details based on empcode and creation company
        public List<HRMS_Employee_Master_Official_Information> Get_Employee_Official_Details(string Emp_Code, string Creation_Company)
        {
            try
            {
                List<HRMS_Employee_Master_Official_Information> objEmpDetails = (from empPerson in objDAL.HRMS_Employee_Master_Official_Informations
                                                                                 where empPerson.Emp_Code == Emp_Code &&
                                                                        empPerson.Creation_Company == Creation_Company
                                                                                 select empPerson).ToList();
                return objEmpDetails;
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
                return new List<HRMS_Employee_Master_Official_Information>();
            }
        }

        //get Eductional cretification details based on empcode and creation company
        public List<HRMS_Employee_Master_Educational_Certification_Detail> Get_Employee_Eductional_Cer_Details(string Emp_Code, string Creation_Company)
        {
            try
            {
                List<HRMS_Employee_Master_Educational_Certification_Detail> objEmpDetails = (from empPerson in objDAL.HRMS_Employee_Master_Educational_Certification_Details
                                                                                             where empPerson.Emp_Code == Emp_Code &&
                                                                           empPerson.Creation_Company == Creation_Company
                                                                                             select empPerson).ToList();
                return objEmpDetails;
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
                return new List<HRMS_Employee_Master_Educational_Certification_Detail>();
            }
        }

        //get Eductional Skill details based on empcode and creation company
        public List<HRMS_Employee_Master_Educational_Skill_Detail> Get_Employee_Eductional_Skill_Details(string Emp_Code, string Creation_Company)
        {
            try
            {
                List<HRMS_Employee_Master_Educational_Skill_Detail> objEmpDetails = (from empPerson in objDAL.HRMS_Employee_Master_Educational_Skill_Details
                                                                                     where empPerson.Emp_Code == Emp_Code &&
                                                                           empPerson.Creation_Company == Creation_Company
                                                                                     select empPerson).ToList();
                return objEmpDetails;
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
                return new List<HRMS_Employee_Master_Educational_Skill_Detail>();
            }
        }
        #endregion
    }
}
