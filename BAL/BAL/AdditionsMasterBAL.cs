﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DAL;
using System.Data.Linq.SqlClient;

namespace BAL
{
    public class AdditionsMasterBAL
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();
        public int Sno { get; set; }
        public string Addition_Code { get; set; }
        public string Head_Name { get; set; }

        public string Short_Name { get; set; }

        public string Caluculation { get; set; }

        public decimal Per_Of_Basic { get; set; }

        public string Ot_Head_Names { get; set; }
        public decimal Ot_Day_Working_Hrs { get; set; }
        public decimal Ot_Night_Working_Hrs { get; set; }
        public decimal Ot_On_Weakly_Off { get; set; }

        public decimal Ot_On_Holiday { get; set; }
        public string Creation_Company { get; set; }

        public string Created_By { get; set; }

        public DateTime Created_Date { get; set; }

        public string Modified_By { get; set; }

        public DateTime Modified_Date { get; set; }
        
        public int Parameter { get; set; }

        #region methods
        // Inserting Header Details
        public int Insert_HRMS_Additions_Master()
        {
            try
            {
                objDAL.Sp_HRMS_Insert_Select_Update_HRMS_Additions_Master(Addition_Code,Head_Name,Short_Name,Caluculation
                    ,Per_Of_Basic,Ot_Head_Names,Ot_Day_Working_Hrs,Ot_Night_Working_Hrs,Ot_On_Weakly_Off,
                    Ot_On_Holiday, Creation_Company, Created_By, Created_Date, Modified_By,
                  Modified_Date, Parameter);
                return 1;
            }

            catch (Exception ex)
            {
                string msg = ex.Message;
                return 0;
            }
        }
        // Edit header Details
        //public List<HRMS_Additions_Master> Get_Addition_Header(string Addition_Code, string Creation_Company)
        //{
        //    try
        //    {
        //        List<HRMS_Additions_Master> objHeader = (from ST in objDAL.HRMS_Additions_Masters
        //                                                    where ST.Addition_Code == Addition_Code && ST.Creation_Company == Creation_Company select ST).ToList();
        //        return objHeader;
        //    }
        //    catch (Exception ex)
        //    {
        //        string msg = ex.Message.ToString();
        //        return new List<HRMS_Additions_Master>();
        //    }
        //}

        //// Delete Method
        //public HRMS_Additions_Master Delete_Addition_Header(string Addition_Code, string Creation_Company)
        //{

        //    try
        //    {
        //        HRMS_Additions_Master SUM = (from ST in objDAL.HRMS_Additions_Masters
        //                                              where ST.Creation_Company == Creation_Company && ST.Addition_Code == Addition_Code
        //                                              select ST).FirstOrDefault();
        //        objDAL.HRMS_Additions_Masters.DeleteOnSubmit(SUM);
        //        objDAL.SubmitChanges();



        //        return SUM;

        //    }
        //    catch (Exception ex)
        //    {
        //        string msg = ex.Message.ToString();
        //        return new HRMS_Additions_Master();
        //    }
        //}

        ///// <summary>
        /////  The Following Method is used for Search(Company Name) The Data Into HRMS_Additions_Master Table
        ///// </summary>
        ///// <param name="ObjUnit"></param>

        //public List<HRMS_Additions_Master> GetData_search_HeaderInfo(string SearchFiled, string Creation_Company)
        //{
        //    try
        //    {
        //      string  query = "%" + SearchFiled + "%";
        //        List<HRMS_Additions_Master> ObjHeadInfo = (from HeaderInfo in objDAL.HRMS_Additions_Masters
        //                                                            where HeaderInfo.Creation_Company.ToLower().Trim() == Creation_Company.ToLower().Trim() &&
        //                                                 SqlMethods.Like(HeaderInfo.Addition_Code.ToLower().Trim(), query.ToLower().Trim()) ||
        //                                                  SqlMethods.Like(HeaderInfo.Head_Name.ToLower().Trim(), query.ToLower().Trim()) ||
        //                                                   SqlMethods.Like(HeaderInfo.Short_Name.ToLower().Trim(), query.ToLower().Trim()) ||
        //                                                    SqlMethods.Like(HeaderInfo.Caluculation.ToLower().Trim(), query.ToLower().Trim()) ||
        //                                                     SqlMethods.Like(HeaderInfo.Per_Of_Basic.ToString().ToLower().Trim(), query.ToLower().Trim()) 
                                                         
        //                                                            select HeaderInfo).ToList();
        //        return ObjHeadInfo;
        //    }
        //    catch (Exception ex)
        //    {
        //        string msg = ex.Message.ToString();
        //        return new List<HRMS_Additions_Master>();
        //    }
        //}
        #endregion
    }



}
