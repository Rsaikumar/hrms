﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
namespace BAL
{
   public class GpfTemporaryApplicationBAL
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

        public int ID { get; set; }
        public string NameOfSubscriber { get; set; }
        public int AccountNo { get; set; }
        public string Designation { get; set; }
        public string Pay { get; set; }
        public DateTime DateOfApplication { get; set; }
        public decimal AmountTaken { get; set; }
        public decimal AmountRequired { get; set; }
        public string PurposeRequired { get; set; }
        public decimal AmountRepaid { get; set; }
        public string TemporaryWithdrawl { get; set; }
        public string ReportingAuthority { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }

        public DateTime ModifiedOn { get; set; }
        public int Parameter { get; set; }

        public int InsertHRMSGpfTempoaryApplication()
        {
            try
            {
                objDAL.Usp_HRMSSelectUpdateGPFTemporaryApplication(ID, NameOfSubscriber, AccountNo, Pay, Designation,
     DateOfApplication, AmountTaken, AmountRequired, PurposeRequired, AmountRepaid, TemporaryWithdrawl, ReportingAuthority, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn, Parameter);

                return 1;
            }

            catch (Exception ex)
            {
                string msg = ex.Message;
                return 0;
            }
        }
    }
}
