﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Linq;

#region Private Using
using DAL;
using System.Data.Linq.SqlClient;
#endregion

namespace BAL
{
    public class Administrator_User_RegistrationBAL
    {
        // // // Object Creation For Database
        ERP_DatabaseDataContext objDb = new ERP_DatabaseDataContext();


        String query = "%%";

        #region Public Property
        /// <summary>
        ///  Domain names Creation
        ///  Created By Rambabu
        /// </summary>

        public int Parameter { set; get; }

        public string Login_Id { set; get; }
        public string Password { set; get; }

        public string User_Name { set; get; }

        public string AllowCompanyListID { set; get; }
        public string AllowCompanyList { set; get; }

        public string E_Mail { set; get; }
        public string User_Type { set; get; }
        public string ClientRegistrationNo { set; get; }
        public string Modules { set; get; }
        public string Status { set; get; }

        public string Administrator_Create { set; get; }
        public string Administrator_Modify { set; get; }
        public string Administrator_Approve { set; get; }
        public string Administrator_View { set; get; }
        public string Master_Create { set; get; }
        public string Master_Modify { set; get; }
        public string Master_Approve { set; get; }
        public string Master_View { set; get; }
        public string Transcation_Create { set; get; }
        public string Transcation_Modify { set; get; }
        public string Transcation_Approve { set; get; }
        public string Transcation_View { set; get; }
        public string Reports_Create { set; get; }
        public string Reports_Modify { set; get; }
        public string Reports_Approve { set; get; }
        public string Reports_View { set; get; }


        // // // Newly Adding Domains(29-12-2015)
        public string Add_Create { set; get; }
        public string Modify { set; get; }
        public string Rec_Delete { set; get; }
        public string Approve_Authorize { set; get; }
        public string View_Print { set; get; }


        public string Creation_Company { set; get; }
        public string Created_By { set; get; }
        public DateTime Created_Date { set; get; }
        public string Modified_By { set; get; }
        public DateTime Modified_Date { set; get; }

        #endregion

        #region Methods

        /// <summary>
        ///  The Following Method is used for Insert The Data Into Units Of Measurements Table
        ///  Created By Rambabu
        /// </summary>
        /// <param name="ObjUnit"></param>

        public int Insert_Administrator_UserRegistration()
        {
            try
            {
                objDb.Sp_Insert_Select_Update_Administrator_UserRegistration(
                    Login_Id, Password, User_Name,
                    E_Mail, User_Type,
                    AllowCompanyListID, AllowCompanyList, 
                    ClientRegistrationNo, Modules, Status, Administrator_Create, Administrator_Modify, Administrator_Approve, Administrator_View,
  Master_Create, Master_Modify, Master_Approve, Master_View, Transcation_Create, Transcation_Modify,
  Transcation_Approve, Transcation_View, Reports_Create, Reports_Modify, Reports_Approve, Reports_View,  
                    Add_Create, Modify, Rec_Delete, Approve_Authorize
                    , View_Print, Creation_Company, Created_By,
                    Created_Date, Modified_By, Modified_Date, Parameter);

                return 1;
            }

            catch (Exception EX)
            {
                return 0;
            }
        }
        public List<Administrator_UserRegistration> Get_UserRegistration(string Creation_Company)
        {
            try
            {
                List<Administrator_UserRegistration> objuser = (from AdUser in objDb.Administrator_UserRegistrations where AdUser.ClientRegistrationNo == Creation_Company select AdUser).ToList();
                return objuser;
            }
            catch (Exception EX)
            {
                return new List<Administrator_UserRegistration>();
            }
        }

        public List<Administrator_UserRegistration> Get_Sale_Login(string Login_Id, string Creation_Company)
        {
            try
            {
                List<Administrator_UserRegistration> objSaleLogin = (from ST in objDb.Administrator_UserRegistrations where ST.Login_Id == Login_Id && ST.ClientRegistrationNo == Creation_Company select ST).ToList();
                return objSaleLogin;
            }
            catch (Exception ex)
            {
                return new List<Administrator_UserRegistration>();
            }
        }

        public object Get_UserTypes_Based_Login(string Login_Id, string Creation_Company, string status)
        {
            try
            {
                return objDb.Sp_Get_User_RegistrationBasedOnLoginId(Creation_Company, Login_Id, status);
            }
            catch (Exception ex)
            {
                return new List<Administrator_UserRegistration>();
            }
        }


       

        public Administrator_UserRegistration Delete_UserDetails(string LoginID, string Creation_Company)
        {

            try
            {
                Administrator_UserRegistration SUM = (from ST in objDb.Administrator_UserRegistrations
                                                      where ST.Creation_Company == Creation_Company && ST.Login_Id == LoginID
                                                          select ST).FirstOrDefault();
                objDb.Administrator_UserRegistrations.DeleteOnSubmit(SUM);
                objDb.SubmitChanges();

                return SUM;

            }
            catch (Exception ex)
            {
                return new Administrator_UserRegistration();
            }
        }


        /// <summary>
        ///  The Following Method is used for Search(Company Name) The Data Into Customer Table
        ///  Created By Divya
        /// </summary>
        /// <param name="ObjUnit"></param>

        public List<Administrator_UserRegistration> GetData_search_UserRegistration(string SearchFiled, string Creation_Company)
        {
            try
            {
                query = "%" + SearchFiled + "%";
                List<Administrator_UserRegistration> ObjUserInfo = (from UserInfo in objDb.Administrator_UserRegistrations
                                                                    where UserInfo.Creation_Company == Creation_Company &&
                                                         SqlMethods.Like(UserInfo.ClientRegistrationNo, query) ||
                                                          SqlMethods.Like(UserInfo.Password, query) ||
                                                           SqlMethods.Like(UserInfo.User_Name, query) ||
                                                            SqlMethods.Like(UserInfo.E_Mail, query) ||
                                                             SqlMethods.Like(UserInfo.Status, query) ||
                                                         SqlMethods.Like(UserInfo.Login_Id, query)
                                                                        select UserInfo).ToList();
                return ObjUserInfo;
            }
            catch (Exception ex)
            {
                return new List<Administrator_UserRegistration>();
            }
        }
        //public void Delete_User(string Login_Id, string Creation_Company)
        //{
        //    try
        //    {

        //        Administrator_UserRegistration aduser = (from ST in objDb.Administrator_UserRegistrations where ST.Creation_Company == Creation_Company && ST.Login_Id == Login_Id select ST).FirstOrDefault();
        //        objDb.Administrator_UserRegistrations.DeleteOnSubmit(aduser);
        //        objDb.SubmitChanges();
        //    }
        //    catch
        //    {

        //    }
        //}
        #endregion



        #region User Based Company name get

        public List<Administrator_UserRegistration> Get_CompanyDetails(string LoginID)
        {
            try
            {
                List<Administrator_UserRegistration> objLogin = (from ST in objDb.Administrator_UserRegistrations
                                                                     where ST.Login_Id == Login_Id
                                                                     select ST).ToList();
                return objLogin;
            }
            catch (Exception ex)
            {
                return new List<Administrator_UserRegistration>();
            }
        }
        #endregion
    }
}
