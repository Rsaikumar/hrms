﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data.Linq.SqlClient;

namespace BAL
{
    public class OTCriteriaMaster
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();
        public string OT_Code { get; set; }

        public string Ot_Head_Names { get; set; }

        public string Ot_Criteria { get; set; }

        public decimal Ot_Rate { get; set; }
        public string Creation_Company { get; set; }

        public string Created_By { get; set; }

        public DateTime Created_Date { get; set; }

        public string Modified_By { get; set; }

        public DateTime Modified_Date { get; set; }

        public int Parameter { get; set; }

        #region Methods
        // Inserting Header Details
        public int Insert_HRMS_OT_Criteria_Master()
        {
            try
            {
                objDAL.Sp_HRMS_Insert_Select_Update_HRMS_OT_Criteria_Master(OT_Code,
                   Ot_Head_Names, Ot_Criteria, Ot_Rate, Creation_Company, Created_By, Created_Date, Modified_By,
                  Modified_Date, Parameter);
                return 1;
            }

            catch (Exception ex)
            {
                string msg = ex.Message;
                return 0;
            }
        }

        // Edit header Details
        public List<HRMS_OT_Criteria_Master> Edit_OT_Criteria(string otcode, string Creation_Company)
        {
            try
            {
                List<HRMS_OT_Criteria_Master> objotdetails = (from ST in objDAL.HRMS_OT_Criteria_Masters
                                                              where ST.OT_Code == otcode && ST.Creation_Company == Creation_Company
                                                              select ST).ToList();
                return objotdetails;
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
                return new List<HRMS_OT_Criteria_Master>();
            }
        }


        // Delete Method
        public List<HRMS_OT_Criteria_Master> Delete_HRMS_OT_Criteria_Master(string OT_Code, string Creation_Company)
        {

            try
            {
               List<HRMS_OT_Criteria_Master> SUM = (from ST in objDAL.HRMS_OT_Criteria_Masters
                                               where ST.Creation_Company == Creation_Company && ST.OT_Code == OT_Code
                                             select ST).ToList();
                objDAL.HRMS_OT_Criteria_Masters.DeleteAllOnSubmit(SUM);
                objDAL.SubmitChanges();

                return SUM;

            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
                return new List<HRMS_OT_Criteria_Master>();
            }
        }



        /// <summary>
        ///  The Following Method is used for Search The Data Into HRMS_OT_Criteria_Master Table
        /// </summary>
        /// <param name="ObjUnit"></param>

        public List<HRMS_OT_Criteria_Master> GetData_search_OTInfo(string SearchFiled, string Creation_Company)
        {
            try
            {
                string query = "%" + SearchFiled + "%";
                List<HRMS_OT_Criteria_Master> ObjHeadInfo = (from HeaderInfo in objDAL.HRMS_OT_Criteria_Masters
                                                             where HeaderInfo.Creation_Company.ToLower().Trim() == Creation_Company.ToLower().Trim() &&
                                                SqlMethods.Like(HeaderInfo.OT_Code.ToLower().Trim(), query.ToLower().Trim()) ||
                                                 SqlMethods.Like(HeaderInfo.Ot_Criteria.ToLower().Trim(), query.ToLower().Trim()) ||
                                                  SqlMethods.Like(HeaderInfo.Ot_Rate.ToString().ToLower().Trim(), query.ToLower().Trim()) ||
                                                   SqlMethods.Like(HeaderInfo.Ot_Head_Names.ToLower().Trim(), query.ToLower().Trim()) 

                                                           select HeaderInfo).ToList();
                return ObjHeadInfo;
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
                return new List<HRMS_OT_Criteria_Master>();
            }
        }
        #endregion
    }
}
