﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data.Linq.SqlClient;

namespace BAL
{
    public class Leave_AllotmentBLL
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();
        public string EmpCode { get; set; }

        public string SelectYear { get; set; }

        public string NewOpeningBalance { get; set; }
        public string LeaveName { get; set; }

        public decimal NoofLeavs { get; set; }
        public string CreationCompany { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int Parameter { get; set; }



        #region Database Methods
        // Inserting Header Details
        public int Insert_HRMS_Leave_Allotment()
        {
            try
            {
                objDAL.Usp_HRMSInsertSelectUpdateHRMSLeaveAllotment(EmpCode,  SelectYear, NewOpeningBalance, LeaveName, NoofLeavs,CreationCompany, CreatedBy, CreatedDate, ModifiedBy,
                  ModifiedDate, Parameter);
                return 1;
            }

            catch (Exception ex)
            {
                string msg = ex.Message;
                return 0;
            }
        }
        #endregion
    }
}
