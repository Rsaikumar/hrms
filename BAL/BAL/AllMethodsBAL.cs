﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DAL;

namespace BAL
{
    public class AllMethodsBAL
    {
        ERP_DatabaseDataContext objDAL = new ERP_DatabaseDataContext();

        #region Allmethods
        //User Login Status
        public List<Administrator_UserRegistration> Get_Sale_LoginUser(string Login_Id, string password, string creationcomp)
        {
            try
            {
                List<Administrator_UserRegistration> objSaleLogin = (from ST in objDAL.Administrator_UserRegistrations where ST.Login_Id == Login_Id && ST.Password == password && ST.Creation_Company == creationcomp select ST).ToList();
                return objSaleLogin;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return new List<Administrator_UserRegistration>();
            }
        }

        //Employee Login Status
        public List<HRMS_Employee_Master_PersonalDetail> Get_Emp_LoginUser(string Login_Id, string password, string creationcomp)
        {
            try
            {
                List<HRMS_Employee_Master_PersonalDetail> objEmpLogin = (from ST in objDAL.HRMS_Employee_Master_PersonalDetails where ST.User_Name == Login_Id && ST.Password == password && ST.Creation_Company == creationcomp select ST).ToList();
                return objEmpLogin;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return new List<HRMS_Employee_Master_PersonalDetail>();
            }
        }

        #endregion



    }
}
