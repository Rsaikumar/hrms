﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#region Private Using
using DAL;
using System.Data.Linq.SqlClient;
#endregion

namespace BAL
{
    public class Administrator_CompanyInfoBAL
    {
        // // // Object Creation For Database
        ERP_DatabaseDataContext objDb = new ERP_DatabaseDataContext();

        String query = "%%";

        #region Public Property
        /// <summary>
        ///  Domain names Creation
        ///  Created By Anusha
        /// </summary>

        public int Parameter { set; get; }
        public string ClientRegistrationNo { set; get; }
        public string Company_Name { set; get; }

        public string Address1 { set; get; }
        public string Address2 { set; get; }
        public string State { set; get; }
        public string City { set; get; }
        public string Pincode { set; get; }
        public string Phone_No { set; get; }
        public string E_Mail { set; get; }
        public string Web_URL { set; get; }
        public string ECC_No { set; get; }
        public string TIN_No { set; get; }

        public string CE_Range { set; get; }
        public string CE_Division { set; get; }
        public string Fax_No { set; get; }
        public string CE_Commissionarate { set; get; }
        public string CST_No { set; get; }
        public string Status { set; get; }

        public string Creation_Company { set; get; }
        public string Created_By { set; get; }
        public DateTime Created_Date { set; get; }
        public string Modified_By { set; get; }
        public DateTime Modified_Date { set; get; }

        // // // // Newly Added Fields
        public string PF_Code { set; get; }
        public string PF_Region { set; get; }
        public string PF_Office { set; get; }
        public string ESI_Reg_Code { set; get; }
        public string ESI_Branch { set; get; }
        public string PT_Reg_No { set; get; }
        public string PAN_No { set; get; }
        public string Weak_Off { set; get; }

        // // /Bank Details

        public string Bank_Name { set; get; }
        public string Account_Number { set; get; }
        public string Branch { set; get; }


        #endregion

        #region Methods

        /// <summary>
        ///  The Following Method is used for Insert The Data Into Units Of Measurements Table
        ///  Created By Rambabu
        /// </summary>
        /// <param name="ObjUnit"></param>

        public int Insert_Administrator_Company_Info()
        {
            try
            {
                objDb.Sp_Insert_Select_Update_Administrator_CompanyInfo(ClientRegistrationNo, Company_Name, Address1, Address2,
                State, City, Pincode, Phone_No, E_Mail, Web_URL, ECC_No, TIN_No, CE_Range, CE_Division, Fax_No, CE_Commissionarate, CST_No,
                PF_Code, PF_Region, PF_Office, ESI_Reg_Code, ESI_Branch, PT_Reg_No, PAN_No, Weak_Off,
                Status, Creation_Company, Created_By, Created_Date, Modified_By, Modified_Date, Parameter);

                return 1;
            }

            catch (Exception EX)
            {
                return 0;
            }
        }



        #endregion

        #region Get Methods
        // //Get the Total Data of Tax Setup
        // // Created By Anusha
        public List<Administrator_CompanyInfo> GetData_CompanyInfo(string Creation_Company)
        {
            try
            {
                List<Administrator_CompanyInfo> ObjCompanyInfo = (from CI in objDb.Administrator_CompanyInfos
                                                                  where CI.ClientRegistrationNo == Creation_Company || CI.Creation_Company == Creation_Company
                                                                  select CI).ToList();
                return ObjCompanyInfo;
            }
            catch (Exception ex)
            {
                return new List<Administrator_CompanyInfo>();
            }
        }


        /// <summary>
        ///  The Following Method is used for Search(Company Name) The Data Into Customer Table
        ///  Created By Divya
        /// </summary>
        /// <param name="ObjUnit"></param>

        public List<Administrator_CompanyInfo> GetData_search_CompanyInfo(string SearchFiled, string Creation_Company)
        {
            try
            {
                query = "%" + SearchFiled + "%";
                List<Administrator_CompanyInfo> ObjUserInfo = (from UserInfo in objDb.Administrator_CompanyInfos
                                                                    where UserInfo.Creation_Company == Creation_Company &&
                                                         SqlMethods.Like(UserInfo.ClientRegistrationNo, query) ||
                                                          SqlMethods.Like(UserInfo.CST_No, query) ||
                                                           SqlMethods.Like(UserInfo.Address1, query) ||
                                                            SqlMethods.Like(UserInfo.Address2, query) ||
                                                             SqlMethods.Like(UserInfo.CE_Commissionarate, query) ||
                                                         SqlMethods.Like(UserInfo.CE_Division, query) ||
                                                          SqlMethods.Like(UserInfo.CE_Range, query) ||
                                                            SqlMethods.Like(UserInfo.City, query) ||
                                                             SqlMethods.Like(UserInfo.State, query) ||
                                                         SqlMethods.Like(UserInfo.Status, query) ||
                                                          SqlMethods.Like(UserInfo.TIN_No, query) ||
                                                            SqlMethods.Like(UserInfo.Company_Name, query) ||
                                                             SqlMethods.Like(UserInfo.ECC_No, query) ||
                                                         SqlMethods.Like(UserInfo.ESI_Branch, query)   ||
                                                            SqlMethods.Like(UserInfo.ESI_Reg_Code, query) ||
                                                             SqlMethods.Like(UserInfo.E_Mail, query) ||
                                                         SqlMethods.Like(UserInfo.Fax_No, query) ||
                                                          SqlMethods.Like(UserInfo.PAN_No, query) ||
                                                            SqlMethods.Like(UserInfo.PF_Code, query) ||
                                                             SqlMethods.Like(UserInfo.PF_Office, query) ||
                                                         SqlMethods.Like(UserInfo.PF_Region, query)  ||
                                                            SqlMethods.Like(UserInfo.Phone_No, query) ||
                                                             SqlMethods.Like(UserInfo.Pincode, query) ||
                                                         SqlMethods.Like(UserInfo.PT_Reg_No, query) ||
                                                          SqlMethods.Like(UserInfo.Weak_Off, query) ||
                                                            SqlMethods.Like(UserInfo.Web_URL, query) 
                                                               select UserInfo).ToList();
                return ObjUserInfo;
            }
            catch (Exception ex)
            {
                return new List<Administrator_CompanyInfo>();
            }
        }
        #endregion

        #region Delete Methods
        // // Created By Hema Prasad

        public void Delete_CompanyInfo(string TaxCode, string Creation_Company)
        {

            try
            {

                Administrator_CompanyInfo CI = (from ACI in objDb.Administrator_CompanyInfos where ACI.Creation_Company == Creation_Company && ACI.ClientRegistrationNo == ClientRegistrationNo select ACI).FirstOrDefault();
                objDb.Administrator_CompanyInfos.DeleteOnSubmit(CI);
                objDb.SubmitChanges();

            }
            catch (Exception ex)
            {

            }
        }

        //Divya


        public Administrator_CompanyInfo Delete_CompanyDetails(string ClientRegistrationNo, string Creation_Company)
        {

            try
            {
                Administrator_CompanyInfo SUM = (from ST in objDb.Administrator_CompanyInfos
                                                 where ST.Creation_Company == Creation_Company && ST.ClientRegistrationNo == ClientRegistrationNo
                                                 select ST).FirstOrDefault();
                objDb.Administrator_CompanyInfos.DeleteOnSubmit(SUM);
                objDb.SubmitChanges();

                return SUM;

            }
            catch (Exception ex)
            {
                return new Administrator_CompanyInfo();
            }
        }

        #endregion
    }
}
